package com.guillaumevdn.supremeshops.module.rentable;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.util.RentPeriod;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePatternAvailability;

public interface Rentable {

	String getId();
	boolean isRented();
	UserInfo getCurrentOwner();
	String getDisplayName();
	int getPastPaidRents();
	int getPaidRents();
	boolean isRentable();
	long getLastCheckRent();
	boolean isAwaitingForceUnrent();
	List<TradeObject> getRentPrice();
	RentPeriod getRentPeriod();
	int getRentPeriodStreakLimit();
	int getRentPeriodStreakLimitDelay();
	CPConditions getRentConditions();
	boolean areRentConditionsValid(Player player, boolean errorMessage);
	String getUnrentedParticlePatternId();
	ParticlePattern getUnrentedParticlePattern();
	ParticlePatternAvailability getRentableParticlePatternAvailability();
	Object[] getMessageReplacers(boolean withDetails, boolean fromModifiersCalculation, Player parser);
	boolean isCurrentOwnerAdmin();

	void addRentPrice(TradeObject priceObject);
	void removeRentPrice(TradeObject priceObject);
	void addRentCondition(Condition condition);
	void removeRentCondition(Condition condition);
	void changeRentable(boolean rentable);
	void changeRentPeriod(RentPeriod period);
	void changeRentPeriodStreakLimit(int limit);
	void changeRentPeriodStreakLimitDelay(int delay);
	void changeUnrentedParticlePattern(String particlePatternId);
	void changePaidRents(int paidRents);
	void changeRent(UserInfo newOwner, int alreadyPaidForRents, boolean push);
	void checkToUpdateRent(boolean push);
	void updateRent(boolean push);
	void openGUI(Merchant fromMerchant, Player player, GUI fromGUI, int fromGUIPageIndex);

	void pushAsync();

}
