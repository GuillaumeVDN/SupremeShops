package com.guillaumevdn.supremeshops.module.modifier;

import java.util.Collection;
import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.messenger.Replacer;
import com.guillaumevdn.gcorelegacy.lib.parseable.ContainerParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPEnumList;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPString;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.Utils.CalculationError;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.MainBoard;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;
import com.guillaumevdn.supremeshops.util.parseable.primitive.PPObjectTypeList;

public class Modifier extends ContainerParseable {

	// base
	private CPConditions conditions = addComponent(new CPConditions("conditions", this, false, 0, EditorGUI.ICON_CONDITION, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_MODIFIER_CONDITIONSLORE.getLines()));
	private PPEnumList<ObjectSide> targetObjectSides = addComponent(new PPEnumList<ObjectSide>("target_object_sides", this, Utils.emptyList(), ObjectSide.class, "object side", false, 1, EditorGUI.ICON_TECHNICAL, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_MODIFIER_TARGETOBJECTSIDESLORE.getLines()));
	private PPObjectTypeList targetObjectTypes = addComponent(new PPObjectTypeList("target_object_types", this, Utils.emptyList(), false, 2, EditorGUI.ICON_TECHNICAL, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_MODIFIER_TARGETOBJECTTYPESLORE.getLines()));
	private PPString formula = addComponent(new PPString("formula", this, "{amount} * 1.001", true, 3, EditorGUI.ICON_TECHNICAL, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_MODIFIER_FORMULALORE.getLines()));

	public Modifier(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "modifier", mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public CPConditions getConditions() {
		return conditions;
	}

	public PPEnumList<ObjectSide> getTargetObjectSides() {
		return targetObjectSides;
	}

	public List<ObjectSide> getTargetObjectSides(Player parser) {
		return targetObjectSides.getParsedValue(parser);
	}

	public PPObjectTypeList getTargetObjectTypes() {
		return targetObjectTypes;
	}

	public List<ObjectType> getTargetObjectTypes(Player parser) {
		return targetObjectTypes.getParsedValue(parser);
	}

	public PPString getFormula() {
		return formula;
	}

	public String getFormula(Player parser) {
		return formula.getParsedValue(parser);
	}

	// methods
	public boolean shouldApply(TradeObject object, Player parser) {
		List<ObjectSide> sides = getTargetObjectSides(parser);
		List<ObjectType> types = getTargetObjectTypes(parser);
		return ((sides == null || sides.isEmpty()) || sides.contains(object.getSide())) && ((types == null || types.isEmpty()) || types.contains(object.getType()));
	}

	// static methods
	public static double applyModifiers(TradeObject object, Shop shop, double amount, int trades, Collection<Modifier> modifiers, Player parser) {
		for (Modifier modifier : modifiers) {
			if (modifier.shouldApply(object, parser)) {
				String rawFormula = null;
				String parsedFormula = null;
				try {
					rawFormula = modifier.getFormula(parser);
					parsedFormula = new Replacer("{amount}", Utils.round5(amount), "{trades}", trades, "{total_server_trades}", SupremeShops.inst().getData().getBoard().get(MainBoard.KEY_MISC_SUCCESSFUL_TRADES, 0d), (shop == null ? null : shop.getMessageReplacers(true, true, parser))).apply(rawFormula);
					amount = Utils.calculateExpression(parsedFormula);
				} catch (Throwable exception) {
					if (!(exception instanceof CalculationError)) exception.printStackTrace();
					SupremeShops.inst().error("Couldn't apply modifier " + modifier.getId() + (shop != null ? " for shop " + shop.getId() : "") + " to object " + object.getId() + " (" + object.getType().getId() + ") (formula '" + rawFormula + "', parsed formula '" + parsedFormula + "') : " + exception.getMessage());
				}
			}
		}
		return Math.abs(amount);
	}

	// clone
	protected Modifier() {
	}

	@Override
	public Modifier clone() {
		return (Modifier) super.clone();
	}

}
