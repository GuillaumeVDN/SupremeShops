package com.guillaumevdn.supremeshops.module;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.Perm;
import com.guillaumevdn.gcorelegacy.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ConfigData;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.versioncompat.sound.Sound;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.adminshop.AdminShopData;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.gui.SSGui;
import com.guillaumevdn.supremeshops.module.itemvalue.ItemValueManager;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchantEquipment;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchantSkin;
import com.guillaumevdn.supremeshops.module.modifier.Modifier;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;
import com.guillaumevdn.supremeshops.module.trigger.TriggerType;
import com.guillaumevdn.supremeshops.util.ClickType;
import com.guillaumevdn.supremeshops.util.CreationTax;
import com.guillaumevdn.supremeshops.util.ListItem;
import com.guillaumevdn.supremeshops.util.WagePeriod;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePatternAvailability;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticleScript;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticleScriptDecoder;

public class ModuleManager {

	// misc
	private Map<String, ObjectType> objectTypes = new HashMap<String, ObjectType>();
	private Map<String, ConditionType> conditionTypes = new HashMap<String, ConditionType>();
	private Map<String, TriggerType> triggerTypes = new HashMap<String, TriggerType>();

	@Deprecated
	public Map<String, ObjectType> getObjectTypes() {
		return objectTypes;
	}

	@Deprecated
	public Map<String, ConditionType> getConditionTypes() {
		return conditionTypes;
	}

	@Deprecated
	public Map<String, TriggerType> getTriggerTypes() {
		return triggerTypes;
	}

	// misc settings
	private boolean splitUnstackableItems = false;
	private double itemDurabilityMinPercentage = 90d;
	private int inactivityTimeDestroy = 0;
	private WagePeriod wagePeriod = null;
	private Map<String, ParticlePattern> particlePatterns = new HashMap<String, ParticlePattern>();
	private Map<String, NpcMerchantSkin> merchantSkins = new HashMap<String, NpcMerchantSkin>();
	private Map<String, NpcMerchantEquipment> merchantEquipments = new HashMap<String, NpcMerchantEquipment>();
	private Map<String, CreationTax> shopCreationTaxes = new HashMap<String, CreationTax>();
	private Map<String, CreationTax> merchantCreationTaxes = new HashMap<String, CreationTax>();
	private Map<String, AdminShopData> adminShops = new HashMap<String, AdminShopData>();
	private Map<String, Trigger> triggers = new HashMap<String, Trigger>();
	private Map<Class<? extends Trigger>, List<? extends Trigger>> triggersByClass = new HashMap<Class<? extends Trigger>, List<? extends Trigger>>();

	public boolean getSplitUnstackableItems() {
		return splitUnstackableItems;
	}

	public double getItemDurabilityMinPercentage() {
		return itemDurabilityMinPercentage;
	}

	public int getInactivityTimeDestroy() {
		return inactivityTimeDestroy;
	}

	public Map<String, ParticlePattern> getParticlePatterns() {
		return particlePatterns;
	}

	public WagePeriod getWagePeriod() {
		return wagePeriod;
	}

	public ParticlePattern getParticlePattern(String id) {
		return id != null ? particlePatterns.get(id.toLowerCase()) : null;
	}

	public Map<String, NpcMerchantSkin> getMerchantSkins() {
		return merchantSkins;
	}

	public NpcMerchantSkin getMerchantSkin(String id) {
		return id != null ? merchantSkins.get(id.toLowerCase()) : null;
	}

	public Map<String, NpcMerchantEquipment> getMerchantEquipments() {
		return merchantEquipments;
	}

	public NpcMerchantEquipment getMerchantEquipment(String id) {
		return id != null ? merchantEquipments.get(id.toLowerCase()) : null;
	}

	public Map<String, CreationTax> getShopCreationTaxes() {
		return shopCreationTaxes;
	}

	public CreationTax getShopCreationTax(String id) {
		return id != null ? shopCreationTaxes.get(id.toLowerCase()) : null;
	}

	public Map<String, CreationTax> getMerchantCreationTaxes() {
		return merchantCreationTaxes;
	}

	public CreationTax getMerchantCreationTax(String id) {
		return id != null ? merchantCreationTaxes.get(id.toLowerCase()) : null;
	}

	public Map<String, AdminShopData> getAdminShops() {
		return adminShops;
	}

	public AdminShopData getAdminShop(String id) {
		return id == null ? null : adminShops.get(id.toLowerCase());
	}

	public Map<String, Trigger> getTriggers() {
		return triggers;
	}

	public Trigger getTrigger(String id) {
		return id == null ? null : triggers.get(id);
	}

	public Map<Class<? extends Trigger>, List<? extends Trigger>> getTriggersByClass() {
		return triggersByClass;
	}

	public <T extends Trigger> List<T> getTriggersByClass(Class<T> clazz) {
		List<? extends Trigger> list = triggersByClass.get(clazz);
		return list == null ? Utils.emptyList(clazz) : (List<T>) list;
	}

	// shop controls
	private ClickType previewClick = null;
	private boolean previewClickSneak = false;

	public ClickType getPreviewClick() {
		return previewClick;
	}

	public boolean getPreviewClickSneak() {
		return previewClickSneak;
	}

	// permission
	private Map<Mat, Perm> shopBlocks = new HashMap<Mat, Perm>();
	private Map<Mat, Perm> merchantBlocks = new HashMap<Mat, Perm>();
	private List<Integer> maxShops = new ArrayList<Integer>();
	private Map<Integer, Perm> maxShopsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxBlockShops = new ArrayList<Integer>();
	private Map<Integer, Perm> maxBlockShopsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxSignShops = new ArrayList<Integer>();
	private Map<Integer, Perm> maxSignShopsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxGuiShops = new ArrayList<Integer>();
	private Map<Integer, Perm> maxGuiShopsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxMerchantShops = new ArrayList<Integer>();
	private Map<Integer, Perm> maxMerchantShopsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxShopManagers = new ArrayList<Integer>();
	private Map<Integer, Perm> maxShopManagersPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxShopItems = new ArrayList<Integer>();
	private Map<Integer, Perm> maxShopItemsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxShopXpLevels = new ArrayList<Integer>();
	private Map<Integer, Perm> maxShopXpLevelsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxShopPlayerPointsPoints = new ArrayList<Integer>();
	private Map<Integer, Perm> maxShopPlayerPointsPointsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxShopTokenEnchantTokens = new ArrayList<Integer>();
	private Map<Integer, Perm> maxShopTokenEnchantTokensPermissions = new HashMap<Integer, Perm>();
	private List<Double> maxShopMoney = new ArrayList<Double>();
	private Map<Double, Perm> maxShopMoneyPermissions = new HashMap<Double, Perm>();
	private List<Double> maxObjectMoney = new ArrayList<Double>();
	private Map<Double, Perm> maxObjectMoneyPermissions = new HashMap<Double, Perm>();
	private List<Integer> maxRemoteShops = new ArrayList<Integer>();
	private Map<Integer, Perm> maxRemoteShopsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxRentedShops = new ArrayList<Integer>();
	private Map<Integer, Perm> maxRentedShopsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxRentedMerchants = new ArrayList<Integer>();
	private Map<Integer, Perm> maxRentedMerchantsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxShopTrades = new ArrayList<Integer>();
	private Map<Integer, Perm> maxShopTradesPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxShopTradesConditions = new ArrayList<Integer>();
	private Map<Integer, Perm> maxShopTradesConditionsPermissions = new HashMap<Integer, Perm>();
	private List<ParticleScript> onbuyParticleScripts = new ArrayList<ParticleScript>();
	private Map<ParticleScript, Perm> onbuyParticleScriptsPermissions = new HashMap<ParticleScript, Perm>();
	private List<Integer> maxMerchants = new ArrayList<Integer>();
	private Map<Integer, Perm> maxMerchantsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxBlockMerchants = new ArrayList<Integer>();
	private Map<Integer, Perm> maxBlockMerchantsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxSignMerchants = new ArrayList<Integer>();
	private Map<Integer, Perm> maxSignMerchantsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxMerchantsLinkedShops = new ArrayList<Integer>();
	private Map<Integer, Perm> maxMerchantsLinkedShopsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxMerchantsCreatedShops = new ArrayList<Integer>();
	private Map<Integer, Perm> maxMerchantsCreatedShopsPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxRemoteMerchants = new ArrayList<Integer>();
	private Map<Integer, Perm> maxRemoteMerchantsPermissions = new HashMap<>();
	private List<Integer> maxMerchantManagers = new ArrayList<Integer>();
	private Map<Integer, Perm> maxMerchantManagersPermissions = new HashMap<Integer, Perm>();
	private List<Integer> maxMerchantInteractConditions = new ArrayList<Integer>();
	private Map<Integer, Perm> maxMerchantInteractConditionsPermissions = new HashMap<Integer, Perm>();

	public boolean canUseShopBlock(Player player, Mat mat) {
		if (!shopBlocks.containsKey(mat)) {
			return false;
		}
		Perm perm = shopBlocks.get(mat);
		return perm == null || perm.has(player);
	}

	public int getMaxShops(Player player) {
		Integer last = 0;
		for (Integer max : maxShops) {
			Perm perm = maxShopsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxBlockShops(Player player) {
		Integer last = 0;
		for (Integer max : maxBlockShops) {
			Perm perm = maxBlockShopsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxSignShops(Player player) {
		Integer last = 0;
		for (Integer max : maxSignShops) {
			Perm perm = maxSignShopsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxGuiShops(Player player) {
		Integer last = 0;
		for (Integer max : maxGuiShops) {
			Perm perm = maxGuiShopsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxMerchantShops(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxMerchantShops) {
			Perm perm = maxMerchantShopsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxShopItems(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxShopItems) {
			Perm perm = maxShopItemsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxShopXpLevels(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxShopXpLevels) {
			Perm perm = maxShopXpLevelsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxShopPlayerPointsPoints(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxShopPlayerPointsPoints) {
			Perm perm = maxShopPlayerPointsPointsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxShopTokenEnchantTokens(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxShopTokenEnchantTokens) {
			Perm perm = maxShopTokenEnchantTokensPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public double getMaxShopMoney(OfflinePlayer player) {
		Double last = 0d;
		for (Double max : maxShopMoney) {
			Perm perm = maxShopMoneyPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public double getMaxObjectVaultMoney(OfflinePlayer player) {
		Double last = 0d;
		for (Double max : maxObjectMoney) {
			Perm perm = maxObjectMoneyPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxShopManagers(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxShopManagers) {
			Perm perm = maxShopManagersPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxRemoteShops(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxRemoteShops) {
			Perm perm = maxRemoteShopsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxRentedShops(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxRentedShops) {
			Perm perm = maxRentedShopsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxRentedMerchants(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxRentedMerchants) {
			Perm perm = maxRentedMerchantsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxShopTrades(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxShopTrades) {
			Perm perm = maxShopTradesPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxShopTradesConditions(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxShopTradesConditions) {
			Perm perm = maxShopTradesConditionsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public ParticleScript getOnBuyParticleScript(Player player) {
		ParticleScript last = null;
		for (ParticleScript pattern : onbuyParticleScripts) {
			Perm perm = onbuyParticleScriptsPermissions.get(pattern);
			if (perm == null || perm.has(player)) {
				last = pattern;
			}
		}
		return last;
	}

	public boolean canUseMerchantBlock(Player player, Mat mat) {
		if (!merchantBlocks.containsKey(mat)) {
			return false;
		}
		Perm perm = merchantBlocks.get(mat);
		return perm == null || perm.has(player);
	}

	public int getMaxMerchants(Player player) {
		Integer last = 0;
		for (Integer max : maxMerchants) {
			Perm perm = maxMerchantsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxBlockMerchants(Player player) {
		Integer last = 0;
		for (Integer max : maxBlockMerchants) {
			Perm perm = maxBlockMerchantsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxSignMerchants(Player player) {
		Integer last = 0;
		for (Integer max : maxSignMerchants) {
			Perm perm = maxSignMerchantsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxMerchantsCreatedShops(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxMerchantsCreatedShops) {
			Perm perm = maxMerchantsCreatedShopsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxMerchantsLinkedShops(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxMerchantsLinkedShops) {
			Perm perm = maxMerchantsLinkedShopsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxRemoteMerchants(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxRemoteMerchants) {
			Perm perm = maxRemoteMerchantsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxMerchantManagers(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxMerchantManagers) {
			Perm perm = maxMerchantManagersPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	public int getMaxMerchantInteractConditions(OfflinePlayer player) {
		Integer last = 0;
		for (Integer max : maxMerchantInteractConditions) {
			Perm perm = maxMerchantInteractConditionsPermissions.get(max);
			if (perm == null || perm.has(player)) {
				last = max;
			}
		}
		return last;
	}

	// conditions and modifiers
	private Map<String, CPConditions> shopExistenceConditions = new HashMap<String, CPConditions>();
	private Map<String, CPConditions> shopDestructionConditions = new HashMap<String, CPConditions>();
	private Map<String, CPConditions> merchantExistenceConditions = new HashMap<String, CPConditions>();
	private Map<String, CPConditions> merchantDestructionConditions = new HashMap<String, CPConditions>();
	private Map<String, Modifier> tradeModifiers = new HashMap<String, Modifier>();
	private Map<String, Modifier> tradeModifiersTaxes = new HashMap<String, Modifier>();

	public Map<String, CPConditions> getShopExistenceConditions() {
		return shopExistenceConditions;
	}

	public CPConditions getShopExistenceCondition(String id) {
		return id == null ? null : shopExistenceConditions.get(id.toLowerCase());
	}

	public boolean canShopExist(Player player, Shop shop, boolean errorMessage) {
		for (CPConditions conditions : shopExistenceConditions.values()) {
			if (!conditions.isValid(player, shop, errorMessage)) {
				return false;
			}
		}
		return true;
	}

	public Map<String, CPConditions> getShopDestructionConditions() {
		return shopDestructionConditions;
	}

	public CPConditions getShopDestructionCondition(String id) {
		return id == null ? null : shopDestructionConditions.get(id.toLowerCase());
	}

	public boolean canShopBeDestroyed(Player player, Shop shop, boolean sendErrorMessage) {
		for (CPConditions conditions : shopDestructionConditions.values()) {
			if (!conditions.isValid(player, shop, sendErrorMessage)) {
				return false;
			}
		}
		return true;
	}

	public Map<String, CPConditions> getMerchantExistenceConditions() {
		return merchantExistenceConditions;
	}

	public CPConditions getMerchantExistenceCondition(String id) {
		return id == null ? null : merchantExistenceConditions.get(id.toLowerCase());
	}

	public boolean canMerchantExist(Player player, Merchant merchant, boolean sendErrorMessage) {
		for (CPConditions conditions : merchantExistenceConditions.values()) {
			if (!conditions.isValid(player, merchant, sendErrorMessage)) {
				return false;
			}
		}
		return true;
	}

	public Map<String, CPConditions> getMerchantDestructionConditions() {
		return merchantDestructionConditions;
	}

	public CPConditions getMerchantDestructionCondition(String id) {
		return id == null ? null : merchantDestructionConditions.get(id.toLowerCase());
	}

	public boolean canMerchantBeDestroyed(Player player, Merchant merchant, boolean sendErrorMessage) {
		for (CPConditions conditions : merchantDestructionConditions.values()) {
			if (!conditions.isValid(player, merchant, sendErrorMessage)) {
				return false;
			}
		}
		return true;
	}

	public Map<String, Modifier> getTradeModifiers() {
		return tradeModifiers;
	}

	public Modifier getTradeModifier(String id) {
		return id == null ? null : tradeModifiers.get(id.toLowerCase());
	}

	public Map<String, Modifier> getTradeModifiersTaxes() {
		return tradeModifiersTaxes;
	}

	public Modifier getTradeModifierTax(String id) {
		return id == null ? null : tradeModifiersTaxes.get(id.toLowerCase());
	}

	// items
	private ItemValueManager itemValueManager = new ItemValueManager();
	private List<ListItem> itemBlacklist = new ArrayList<ListItem>();
	private List<ListItem> itemWhitelist = new ArrayList<ListItem>();
	private boolean enableItemBlacklist = false;
	private boolean enableItemWhitelist = false;

	public ItemValueManager getItemValueManager() {
		return itemValueManager;
	}

	public boolean getEnableItemBlacklist() {
		return enableItemBlacklist;
	}

	public boolean getEnableItemWhitelist() {
		return enableItemWhitelist;
	}

	public List<ListItem> getItemBlacklist() {
		return itemBlacklist;
	}

	public List<ListItem> getItemWhitelist() {
		return itemWhitelist;
	}

	// sound
	private Sound guiClickSound;
	private Sound guiClickErrorSound;
	private Sound shopRentSound;
	private Sound playerTradeSound;
	private Sound otherPlayerTradeSound;
	private Sound shopTradeSound;
	private Sound stockAddSound;
	private Sound stockWithdrawSound;
	private Sound objectAddSound;
	private Sound unbalancedTradeSoundGaining;
	private Sound unbalancedTradeSoundLosing;

	public Sound getGuiClickSound() {
		return guiClickSound;
	}

	public Sound getGuiClickErrorSound() {
		return guiClickErrorSound;
	}

	public Sound getShopRentSound() {
		return shopRentSound;
	}

	public Sound getPlayerTradeSound() {
		return playerTradeSound;
	}

	public Sound getOtherPlayerTradeSound() {
		return otherPlayerTradeSound;
	}

	public Sound getShopTradeSound() {
		return shopTradeSound;
	}

	public Sound getStockAddSound() {
		return stockAddSound;
	}

	public Sound getStockWithdrawSound() {
		return stockWithdrawSound;
	}

	public Sound getObjectAddSound() {
		return objectAddSound;
	}

	public Sound getUnbalancedTradeSoundGaining() {
		return unbalancedTradeSoundGaining;
	}

	public Sound getUnbalancedTradeSoundLosing() {
		return unbalancedTradeSoundLosing;
	}

	// gui and preview
	private ItemData backItem = null;
	private int previewGuiSize = 54;
	private List<Integer> previewGuiTakenItemSlots = new ArrayList<>();
	private List<Integer> previewGuiGivenItemSlots = new ArrayList<>();
	private ItemData previewGuiShopInfo = null;
	private ItemData previewGuiRentShopInfo = null;
	private ItemData previewGuiAdminShopInfo = null;
	private ItemData previewGuiMerchantInfo = null;
	private ItemData previewGuiRentMerchantInfo = null;
	private ItemData previewGuiAdminMerchantInfo = null;
	private ItemData previewGuiTradeItem = null;
	private ItemData previewGuiEditAdminRentItem = null;
	private ItemData previewGuiEditItem = null;
	private ItemData previewGuiBackItem = null;
	private ItemData previewGuiSelectMaxTrades = null;
	private ItemData previewGuiTradeAmountModifyItemNegative = null;
	private ItemData previewGuiTradeAmountModifyItemPositive = null;
	private Map<Integer, Integer> previewGuiTradeAmountModifiers = new HashMap<>();
	private List<ItemData> previewGuiContent = new ArrayList<ItemData>();
	private ItemData previewGuiItemVaultMoney = null;
	private ItemData previewGuiItemXpLevel = null;
	private ItemData previewGuiItemPlayerPointsPoints = null;
	private ItemData previewGuiItemTokenEnchantTokens = null;
	private ItemData previewGuiItemShop = null;
	private ItemData previewGuiItemMerchant = null;

	private int rentableManagementGuiSize = 54;
	private int rentableManagementGuiSlotInfo = 4;
	private int rentableManagementGuiSlotRentPrice = 10;
	private int rentableManagementGuiSlotRentPeriod = 11;
	private int rentableManagementGuiSlotRentPeriodStreakLimit = 12;
	private int rentableManagementGuiSlotRentPeriodStreakLimitDelay = 13;
	private int rentableManagementGuiSlotRentConditions = 15;
	private int rentableManagementGuiSlotUnrentedParticlePattern = 28;
	private int rentableManagementGuiSlotRentableToggle = 32;
	private int rentableManagementGuiSlotForceStopRenting = 43;
	private int rentableManagementGuiSlotDestroy = 49;
	private int rentableManagementGuiSlotBack = 52;

	private int shopManagementGuiSize = 54;
	private int shopManagementSlotInfo = 4;
	private int shopManagementSlotGuiTakenObjects = 10;
	private int shopManagementSlotGuiGivenObjects = 11;
	private int shopManagementSlotsGuiTradesLimit = 14;
	private int shopManagementSlotGuiTradeConditions = 15;
	private int shopManagementSlotGuiManagers = 16;
	private int shopManagementSlotGuiDisplayName = 28;
	private int shopManagementSlotGuiParticlePatterns = 29;
	private int shopManagementSlotGuiCloseToggle = 32;
	private int shopManagementSlotGuiRemoteToggle = 33;
	private int shopManagementSlotGuiDisplayItemsToggle = 34;
	private int shopManagementSlotGuiAdminStockToggle = 31;
	private int shopManagementSlotGuiRent = 43;
	private int shopManagementSlotGuiDestroy = 49;
	private int shopManagementSlotGuiStopRenting = 49;
	private int shopManagementSlotGuiBack = 52;

	private int merchantManagementGuiSize = 54;
	private int merchantManagementSlotInfo = 4;
	private int merchantManagementSlotGuiShops = 11;
	private int merchantManagementSlotsGuiTradesLimit = 14;
	private int merchantManagementSlotGuiInteractConditions = 15;
	private int merchantManagementSlotGuiManagers = 16;
	private int merchantManagementSlotGuiDisplayName = 28;
	private int merchantManagementSlotGuiParticlePatterns = 29;
	private int merchantManagementSlotGuiSkins = 30;
	private int merchantManagementSlotGuiEquipments = 31;
	private int merchantManagementSlotGuiStatus = 32;
	private int merchantManagementSlotGuiCloseToggle = 33;
	private int merchantManagementSlotGuiRemoteToggle = 34;
	private int merchantManagementSlotGuiRent = 43;
	private int merchantManagementSlotGuiDestroy = 49;
	private int merchantManagementSlotGuiStopRenting = 49;
	private int merchantManagementSlotGuiBack = 52;

	private String mainGuiId;
	private Map<String, SSGui> guis = new HashMap<String, SSGui>();

	public int getPreviewGuiSize() {
		return previewGuiSize;
	}

	public List<Integer> getPreviewGuiTakenItemSlots() {
		return previewGuiTakenItemSlots;
	}

	public List<Integer> getPreviewGuiGivenItemSlots() {
		return previewGuiGivenItemSlots;
	}

	public ItemData getBackItem() {
		return backItem;
	}

	public ItemData getPreviewGuiShopInfo() {
		return previewGuiShopInfo;
	}

	public ItemData getPreviewGuiRentShopInfo() {
		return previewGuiRentShopInfo;
	}

	public ItemData getPreviewGuiAdminShopInfo() {
		return previewGuiAdminShopInfo;
	}

	public ItemData getPreviewGuiMerchantInfo() {
		return previewGuiMerchantInfo;
	}

	public ItemData getPreviewGuiRentMerchantInfo() {
		return previewGuiRentMerchantInfo;
	}

	public ItemData getPreviewGuiAdminMerchantInfo() {
		return previewGuiAdminMerchantInfo;
	}

	public ItemData getPreviewGuiTradeItem() {
		return previewGuiTradeItem;
	}

	public ItemData getPreviewGuiEditItem() {
		return previewGuiEditItem;
	}

	public ItemData getPreviewGuiEditAdminRentItem() {
		return previewGuiEditAdminRentItem;
	}

	public ItemData getPreviewGuiBackItem() {
		return previewGuiBackItem;
	}

	public ItemData getPreviewGuiSelectMaxTrades() {
		return previewGuiSelectMaxTrades;
	}

	public ItemData getPreviewGuiTradeAmountModifyItemNegative() {
		return previewGuiTradeAmountModifyItemNegative;
	}

	public ItemData getPreviewGuiTradeAmountModifyItemPositive() {
		return previewGuiTradeAmountModifyItemPositive;
	}

	public Map<Integer, Integer> getPreviewGuiTradeAmountModifiers() {
		return previewGuiTradeAmountModifiers;
	}

	public List<ItemData> getPreviewGuiContent() {
		return previewGuiContent;
	}

	public ItemData getPreviewGuiItemVaultMoney() {
		return previewGuiItemVaultMoney;
	}

	public ItemData getPreviewGuiItemPlayerPointsPoints() {
		return previewGuiItemPlayerPointsPoints;
	}

	public ItemData getPreviewGuiItemTokenEnchantTokens() {
		return previewGuiItemTokenEnchantTokens;
	}

	public ItemData getPreviewGuiItemXpLevel() {
		return previewGuiItemXpLevel;
	}

	public ItemData getPreviewGuiItemShop() {
		return previewGuiItemShop;
	}

	public ItemData getPreviewGuiItemMerchant() {
		return previewGuiItemMerchant;
	}

	// shop management GUI
	public int getShopManagementGuiSize() {
		return shopManagementGuiSize;
	}

	public int getShopManagementSlotInfo() {
		return shopManagementSlotInfo;
	}

	public int getShopManagementSlotGuiDisplayName() {
		return shopManagementSlotGuiDisplayName;
	}

	public int getShopManagementSlotGuiParticlePatterns() {
		return shopManagementSlotGuiParticlePatterns;
	}

	public int getShopManagementSlotsGuiTradesLimit() {
		return shopManagementSlotsGuiTradesLimit;
	}

	public int getShopManagementSlotGuiTradeConditions() {
		return shopManagementSlotGuiTradeConditions;
	}

	public int getShopManagementSlotGuiManagers() {
		return shopManagementSlotGuiManagers;
	}

	public int getShopManagementSlotGuiCloseToggle() {
		return shopManagementSlotGuiCloseToggle;
	}

	public int getShopManagementSlotGuiRemoteToggle() {
		return shopManagementSlotGuiRemoteToggle;
	}

	public int getShopManagementSlotGuiDisplayItemsToggle() {
		return shopManagementSlotGuiDisplayItemsToggle;
	}

	public int getShopManagementSlotGuiAdminStockToggle() {
		return shopManagementSlotGuiAdminStockToggle;
	}

	public int getShopManagementSlotGuiTakenObjects() {
		return shopManagementSlotGuiTakenObjects;
	}

	public int getShopManagementSlotGuiGivenObjects() {
		return shopManagementSlotGuiGivenObjects;
	}

	public int getShopManagementSlotGuiRent() {
		return shopManagementSlotGuiRent;
	}

	public int getShopManagementSlotGuiDestroy() {
		return shopManagementSlotGuiDestroy;
	}

	public int getShopManagementSlotGuiStopRenting() {
		return shopManagementSlotGuiStopRenting;
	}

	public int getShopManagementSlotGuiBack() {
		return shopManagementSlotGuiBack;
	}

	// merchant management GUI
	public int getMerchantManagementGuiSize() {
		return merchantManagementGuiSize;
	}

	public int getMerchantManagementSlotInfo() {
		return merchantManagementSlotInfo;
	}

	public int getMerchantManagementSlotGuiDisplayName() {
		return merchantManagementSlotGuiDisplayName;
	}

	public int getMerchantManagementSlotGuiParticlePatterns() {
		return merchantManagementSlotGuiParticlePatterns;
	}

	public int getMerchantManagementSlotsGuiTradesLimit() {
		return merchantManagementSlotsGuiTradesLimit;
	}

	public int getMerchantManagementSlotGuiInteractConditions() {
		return merchantManagementSlotGuiInteractConditions;
	}

	public int getMerchantManagementSlotGuiManagers() {
		return merchantManagementSlotGuiManagers;
	}

	public int getMerchantManagementSlotGuiCloseToggle() {
		return merchantManagementSlotGuiCloseToggle;
	}

	public int getMerchantManagementSlotGuiRemoteToggle() {
		return merchantManagementSlotGuiRemoteToggle;
	}

	public int getMerchantManagementSlotGuiShops() {
		return merchantManagementSlotGuiShops;
	}

	public int getMerchantManagementSlotGuiDestroy() {
		return merchantManagementSlotGuiDestroy;
	}

	public int getMerchantManagementSlotGuiSkins() {
		return merchantManagementSlotGuiSkins;
	}

	public int getMerchantManagementSlotGuiEquipments() {
		return merchantManagementSlotGuiEquipments;
	}

	public int getMerchantManagementSlotGuiStatus() {
		return merchantManagementSlotGuiStatus;
	}

	public int getMerchantManagementSlotGuiRent() {
		return merchantManagementSlotGuiRent;
	}

	public int getMerchantManagementSlotGuiStopRenting() {
		return merchantManagementSlotGuiStopRenting;
	}

	public int getMerchantManagementSlotGuiBack() {
		return merchantManagementSlotGuiBack;
	}

	// rentable
	public int getRentableManagementGuiSize() {
		return rentableManagementGuiSize;
	}

	public int getRentableManagementGuiSlotInfo() {
		return rentableManagementGuiSlotInfo;
	}

	public int getRentableManagementGuiSlotRentPrice() {
		return rentableManagementGuiSlotRentPrice;
	}

	public int getRentableManagementGuiSlotRentPeriod() {
		return rentableManagementGuiSlotRentPeriod;
	}

	public int getRentableManagementGuiSlotRentPeriodStreakLimit() {
		return rentableManagementGuiSlotRentPeriodStreakLimit;
	}

	public int getRentableManagementGuiSlotRentPeriodStreakLimitDelay() {
		return rentableManagementGuiSlotRentPeriodStreakLimitDelay;
	}

	public int getRentableManagementGuiSlotRentConditions() {
		return rentableManagementGuiSlotRentConditions;
	}

	public int getRentableManagementGuiSlotUnrentedParticlePattern() {
		return rentableManagementGuiSlotUnrentedParticlePattern;
	}

	public int getRentableManagementGuiSlotRentableToggle() {
		return rentableManagementGuiSlotRentableToggle;
	}

	public int getRentableManagementGuiSlotForceStopRenting() {
		return rentableManagementGuiSlotForceStopRenting;
	}

	public int getRentableManagementGuiSlotDestroy() {
		return rentableManagementGuiSlotDestroy;
	}

	public int getRentableManagementGuiSlotBack() {
		return rentableManagementGuiSlotBack;
	}

	// misc
	public String getMainGuiId() {
		return mainGuiId;
	}

	public Map<String, SSGui> getGuis() {
		return guis;
	}

	public SSGui getGui(String id) {
		return id == null ? null : guis.get(id.toLowerCase());
	}

	// methods
	public void reloadConfig() {
		// settings
		previewClick = SupremeShops.inst().getConfiguration().getEnumValue("preview_click.type", ClickType.class, (String) null);
		previewClickSneak = SupremeShops.inst().getConfiguration().getBoolean("preview_click.sneak", false);
		splitUnstackableItems = SupremeShops.inst().getConfiguration().getBoolean("split_unstackable_items", false);
		itemDurabilityMinPercentage = SupremeShops.inst().getConfiguration().getDouble("item_durability_min_percentage", 100d);
		if (itemDurabilityMinPercentage < 0d) itemDurabilityMinPercentage = 0d;
		else if (itemDurabilityMinPercentage > 100d) itemDurabilityMinPercentage = 100d;
		inactivityTimeDestroy = SupremeShops.inst().getConfiguration().getInt("inactivity_time_destroy", 0);
		wagePeriod = SupremeShops.inst().getConfiguration().getEnumValue("managers_wage_period", WagePeriod.class, WagePeriod.MONTHLY.name());

		// settings : sounds
		guiClickSound = SupremeShops.inst().getConfiguration().getEnumValue("sounds.gui_click", Sound.class, (Sound) null);
		guiClickErrorSound = SupremeShops.inst().getConfiguration().getEnumValue("sounds.gui_click_error", Sound.class, (Sound) null);
		shopRentSound = SupremeShops.inst().getConfiguration().getEnumValue("sounds.shop_rent_sound", Sound.class, (Sound) null);
		playerTradeSound = SupremeShops.inst().getConfiguration().getEnumValue("sounds.player_trade", Sound.class, (Sound) null);
		otherPlayerTradeSound = SupremeShops.inst().getConfiguration().getEnumValue("sounds.other_player_trade", Sound.class, (Sound) null);
		shopTradeSound = SupremeShops.inst().getConfiguration().getEnumValue("sounds.shop_trade", Sound.class, (Sound) null);
		stockAddSound = SupremeShops.inst().getConfiguration().getEnumValue("sounds.stock_add", Sound.class, (Sound) null);
		stockWithdrawSound = SupremeShops.inst().getConfiguration().getEnumValue("sounds.stock_withdraw", Sound.class, (Sound) null);
		objectAddSound = SupremeShops.inst().getConfiguration().getEnumValue("sounds.object_add", Sound.class, (Sound) null);
		unbalancedTradeSoundGaining = SupremeShops.inst().getConfiguration().getEnumValue("sounds.unbalanced_trade_gaining", Sound.class, (Sound) null);
		unbalancedTradeSoundLosing = SupremeShops.inst().getConfiguration().getEnumValue("sounds.unbalanced_trade_loosing", Sound.class, (Sound) null);

		// settings : shop blocks
		shopBlocks.clear();
		for (String raw : SupremeShops.inst().getConfiguration().getList("shop_blocks", Utils.emptyList())) {
			String[] split = raw.contains(",") ? raw.split(",") : new String[] { raw };
			Mat mat = Mat.valueOf(split[0].toUpperCase());
			if (mat == null) {
				SupremeShops.inst().error("Couldn't load shop block " + raw + " (unknown GCoreLegacy material " + split[0] + ")");
				continue;
			}
			if (shopBlocks.containsKey(mat)) {
				SupremeShops.inst().error("Couldn't load shop block " + raw + " (duplicate material " + split[0] + ")");
				continue;
			}
			shopBlocks.put(mat, split.length >= 2 ? new Perm(null, split[1]) : null);
		}
		if (!shopBlocks.isEmpty()) SupremeShops.inst().success("Loaded " + shopBlocks.size() + " shop block" + Utils.getPlural(shopBlocks.size()) + " : " + Utils.asNiceString(shopBlocks.keySet(), true));

		// settings : merchant blocks
		merchantBlocks.clear();
		for (String raw : SupremeShops.inst().getConfiguration().getList("merchant_blocks", Utils.emptyList())) {
			String[] split = raw.contains(",") ? raw.split(",") : new String[] { raw };
			Mat mat = Mat.valueOf(split[0].toUpperCase());
			if (mat == null) {
				SupremeShops.inst().error("Couldn't load merchant block " + raw + " (unknown GCoreLegacy material " + split[0] + ")");
				continue;
			}
			if (merchantBlocks.containsKey(mat)) {
				SupremeShops.inst().error("Couldn't load merchant block " + raw + " (duplicate material " + split[0] + ")");
				continue;
			}
			merchantBlocks.put(mat, split.length >= 2 ? new Perm(null, split[1]) : null);
		}
		if (!merchantBlocks.isEmpty()) SupremeShops.inst().success("Loaded " + merchantBlocks.size() + " merchant block" + Utils.getPlural(merchantBlocks.size()) + " : " + Utils.asNiceString(merchantBlocks.keySet(), true));

		// settings : max
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_shops", maxShops, maxShopsPermissions, "max shop", "max shops");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_block_shops", maxBlockShops, maxBlockShopsPermissions, "max block shop", "max block shops");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_sign_shops", maxSignShops, maxSignShopsPermissions, "max sign shop", "max sign shops");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_gui_shops", maxGuiShops, maxGuiShopsPermissions, "max gui shop", "max gui shops");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_merchant_shops", maxMerchantShops, maxMerchantShopsPermissions, "max merchant shop", "max merchant shops");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_shop_items", maxShopItems, maxShopItemsPermissions, "max shop item", "max shop items");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_shop_playerpoints_points", maxShopPlayerPointsPoints, maxShopPlayerPointsPointsPermissions, "max shop PlayerPoints point", "max shop PlayerPoints points");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_shop_tokenenchant_tokens", maxShopTokenEnchantTokens, maxShopTokenEnchantTokensPermissions, "max shop TokenEnchant token", "max shop TokenEnchant tokens");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_shop_xp_level", maxShopXpLevels, maxShopXpLevelsPermissions, "max shop xp level", "max shop xp levels");
		loadPermissibleListDouble(SupremeShops.inst().getConfiguration(), "max_shop_money", maxShopMoney, maxShopMoneyPermissions, "max shop money", "max shop money");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_shop_managers", maxShopManagers, maxShopManagersPermissions, "max shop manager", "max shop managers");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_remote_shops", maxRemoteShops, maxRemoteShopsPermissions, "max remote shop", "max remote shops");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_shop_trades", maxShopTrades, maxShopTradesPermissions, "max shop trade", "max shop trades");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_shop_trades_conditions", maxShopTradesConditions, maxShopTradesConditionsPermissions, "max shop trade conditions", "max shop trade conditions");
		loadPermissibleListDouble(SupremeShops.inst().getConfiguration(), "max_vault_money_object", maxObjectMoney, maxObjectMoneyPermissions, "max object vault money", "max objects vault money");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_rented_shops", maxRentedShops, maxRentedShopsPermissions, "max rented shop", "max rented shops");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_merchants", maxMerchants, maxMerchantsPermissions, "max merchant", "max merchants");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_block_merchants", maxBlockMerchants, maxBlockMerchantsPermissions, "max block merchant", "max block merchants");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_sign_merchants", maxSignMerchants, maxSignMerchantsPermissions, "max sign merchant", "max sign merchants");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_merchants_created_shops", maxMerchantsCreatedShops, maxMerchantsCreatedShopsPermissions, "max merchant created shop", "max merchant created shops");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_merchants_linked_shops", maxMerchantsLinkedShops, maxMerchantsLinkedShopsPermissions, "max merchant linked  shop", "max merchant linked shops");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_remote_merchants", maxRemoteMerchants, maxRemoteMerchantsPermissions, "max remote merchant", "max remote merchants");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_merchant_managers", maxMerchantManagers, maxMerchantManagersPermissions, "max merchant manager", "max merchant managers");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_merchant_interact_conditions", maxMerchantInteractConditions, maxMerchantInteractConditionsPermissions, "max merchant interact conditions", "max merchant interact conditions");
		loadPermissibleListInteger(SupremeShops.inst().getConfiguration(), "max_rented_merchants", maxRentedMerchants, maxRentedMerchantsPermissions, "max rented merchant", "max rented merchants");

		// settings : on buy particle scripts
		onbuyParticleScripts.clear();
		onbuyParticleScriptsPermissions.clear();
		for (String raw : SupremeShops.inst().getConfiguration().getList("onbuy_particle_scripts", Utils.emptyList())) {
			String[] split = raw.contains(",") ? raw.split(",") : new String[] { raw };
			// no file
			String file = split[0];
			File scriptFile = new File(SupremeShops.inst().getDataFolder() + "/particle_patterns/scripts/" + file);
			if (!scriptFile.exists()) {
				SupremeShops.inst().error("Couldn't load on buy particle script " + raw + " (unknown script)");
				continue;
			}
			// load script
			ParticleScript script;
			try {
				ParticleScriptDecoder decoder = new ParticleScriptDecoder(scriptFile);
				decoder.decode();
				script = decoder.getScript();
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Couldn't load on buy particle script " + raw + " (unknown error while decoding script)");
				continue;
			}
			// already exists
			if (onbuyParticleScripts.contains(script)) {
				SupremeShops.inst().error("Couldn't load maximum shops " + raw + " (duplicate pattern " + script.getId() + ")");
				continue;
			}
			// add
			onbuyParticleScripts.add(script);
			onbuyParticleScriptsPermissions.put(script, split.length >= 2 ? new Perm(null, split[1]) : null);
		}
		if (!onbuyParticleScripts.isEmpty()) SupremeShops.inst().success("Loaded " + onbuyParticleScripts.size() + " on buy particle script" + Utils.getPlural(onbuyParticleScripts.size()) + " : " + Utils.asNiceString(onbuyParticleScripts, true));

		// gui
		mainGuiId = SupremeShops.inst().getConfiguration().getString("main_gui", "main_gui");
		backItem = SupremeShops.inst().getConfiguration().getItem("back_item_gui");
		previewGuiSize = SupremeShops.inst().getConfiguration().getInt("gui.preview_size", 54);
		previewGuiShopInfo = SupremeShops.inst().getConfiguration().getItem("gui.shop_info");
		previewGuiRentShopInfo = SupremeShops.inst().getConfiguration().getItem("gui.rent_shop_info");
		previewGuiAdminShopInfo = SupremeShops.inst().getConfiguration().getItem("gui.admin_shop_info");
		previewGuiMerchantInfo = SupremeShops.inst().getConfiguration().getItem("gui.merchant_info");
		previewGuiRentMerchantInfo = SupremeShops.inst().getConfiguration().getItem("gui.rent_merchant_info");
		previewGuiAdminMerchantInfo = SupremeShops.inst().getConfiguration().getItem("gui.admin_merchant_info");
		previewGuiTradeItem = SupremeShops.inst().getConfiguration().getItem("gui.trade_item");
		previewGuiEditItem = SupremeShops.inst().getConfiguration().getItem("gui.edit_item");
		previewGuiEditAdminRentItem = SupremeShops.inst().getConfiguration().getItem("gui.edit_admin_rent_item");
		previewGuiBackItem = SupremeShops.inst().getConfiguration().getItem("gui.back_item");
		previewGuiSelectMaxTrades = SupremeShops.inst().getConfiguration().getItem("gui.select_max_trades_item");
		previewGuiTradeAmountModifyItemNegative = SupremeShops.inst().getConfiguration().getItem("gui.trade_amount_modify_item_negative");
		previewGuiTradeAmountModifyItemPositive = SupremeShops.inst().getConfiguration().getItem("gui.trade_amount_modify_item_positive");
		previewGuiTradeAmountModifiers.clear();
		int i = -1;
		for (String raw : SupremeShops.inst().getConfiguration().getList("gui.trade_amount_modify", Utils.emptyList())) {
			++i;
			String[] split = raw.trim().split(",");
			if (split.length == 1) {// not updated yet ; TODO remove
				Integer nb = Utils.integerOrNull(split[0]);
				if (nb != null) {
					previewGuiTradeAmountModifiers.put(48 - i, -nb);
					previewGuiTradeAmountModifiers.put(50 + i, nb);
				}
			} else {
				Integer slot = Utils.integerOrNull(split[0]);
				Integer nb = Utils.integerOrNull(split[1]);
				if (slot != null && nb != null) {
					previewGuiTradeAmountModifiers.put(slot, nb);
				}
			}
		}
		previewGuiTakenItemSlots.clear();
		for (String raw : SupremeShops.inst().getConfiguration().getList("gui.preview_taken_items_slots", Utils.asList("9", "10", "11", "12", "18", "19", "20", "21", "27", "28", "29", "30"))) {
			Integer slot = Utils.integerOrNull(raw);
			if (slot != null) {
				previewGuiTakenItemSlots.add(slot);
			}
		}
		previewGuiGivenItemSlots.clear();
		for (String raw : SupremeShops.inst().getConfiguration().getList("gui.preview_given_items_slots", Utils.asList("14", "15", "16", "17", "23", "24", "25", "26", "32", "33", "34", "35"))) {
			Integer slot = Utils.integerOrNull(raw);
			if (slot != null) {
				previewGuiGivenItemSlots.add(slot);
			}
		}
		previewGuiContent = SupremeShops.inst().getConfiguration().getItems("gui.content");
		previewGuiItemVaultMoney = SupremeShops.inst().getConfiguration().getItem("gui.item_vault_money");
		previewGuiItemPlayerPointsPoints = SupremeShops.inst().getConfiguration().getItem("gui.playerpoints_points");
		previewGuiItemTokenEnchantTokens = SupremeShops.inst().getConfiguration().getItem("gui.tokenenchant_tokens");
		previewGuiItemXpLevel = SupremeShops.inst().getConfiguration().getItem("gui.item_xp_level");
		previewGuiItemShop = SupremeShops.inst().getConfiguration().getItem("gui.item_shop");
		previewGuiItemMerchant = SupremeShops.inst().getConfiguration().getItem("gui.item_merchant");

		shopManagementGuiSize = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_gui_size", 54);
		shopManagementSlotInfo = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.info", 4);
		shopManagementSlotGuiTakenObjects = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.taken_objects", 10);
		shopManagementSlotGuiGivenObjects = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.given_objects", 11);
		shopManagementSlotsGuiTradesLimit = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.trades_limit", 14);
		shopManagementSlotGuiTradeConditions = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.trade_conditions", 15);
		shopManagementSlotGuiManagers = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.managers", 16);
		shopManagementSlotGuiDisplayName = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.display_name", 28);
		shopManagementSlotGuiParticlePatterns = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.particle_patterns", 29);
		shopManagementSlotGuiCloseToggle = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.close_toggle", 32);
		shopManagementSlotGuiRemoteToggle = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.remote_toggle", 33);
		shopManagementSlotGuiDisplayItemsToggle = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.display_items_toggle", 34);
		shopManagementSlotGuiAdminStockToggle = SupremeShops.inst().getConfiguration().getInt("gui.shop_management.slots.admin_stock_toggle", 31);
		shopManagementSlotGuiRent = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.rent", 43);
		shopManagementSlotGuiDestroy = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.destroy", 49);
		shopManagementSlotGuiStopRenting = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.stop_renting", 49);
		shopManagementSlotGuiBack = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.back", 52);

		merchantManagementGuiSize = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_gui_size", 54);
		merchantManagementSlotInfo = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.info", 4);
		merchantManagementSlotGuiShops = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.shops", 11);
		merchantManagementSlotsGuiTradesLimit = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.trades_limit", 14);
		merchantManagementSlotGuiInteractConditions = SupremeShops.inst().getConfiguration().getInt("gui.shop_management_slots.interact_conditions", 15);
		merchantManagementSlotGuiManagers = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.managers", 16);
		merchantManagementSlotGuiDisplayName = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.display_name", 28);
		merchantManagementSlotGuiParticlePatterns = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.particle_patterns", 29);
		merchantManagementSlotGuiSkins = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.skins", 30);
		merchantManagementSlotGuiEquipments = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.equipments", 31);
		merchantManagementSlotGuiStatus = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.status", 32);
		merchantManagementSlotGuiCloseToggle = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.close_toggle", 33);
		merchantManagementSlotGuiRemoteToggle = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.remote_toggle", 34);
		merchantManagementSlotGuiRent = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.rent", 43);
		merchantManagementSlotGuiDestroy = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.destroy", 49);
		merchantManagementSlotGuiStopRenting = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.stop_renting", 49);
		merchantManagementSlotGuiBack = SupremeShops.inst().getConfiguration().getInt("gui.merchant_management_slots.back", 52);

		rentableManagementGuiSize = SupremeShops.inst().getConfiguration().getInt("gui.rentable_management_gui_size", 54);
		rentableManagementGuiSlotInfo = SupremeShops.inst().getConfiguration().getInt("gui.rentable_management_slots.info", 4);
		rentableManagementGuiSlotRentPrice = SupremeShops.inst().getConfiguration().getInt("gui.rentable_management_slots.rent_price", 10);
		rentableManagementGuiSlotRentPeriod = SupremeShops.inst().getConfiguration().getInt("gui.rentable_management_slots.rent_period", 11);
		rentableManagementGuiSlotRentPeriodStreakLimit = SupremeShops.inst().getConfiguration().getInt("gui.rentable_management_slots.rent_period_streak_limit", 12);
		rentableManagementGuiSlotRentPeriodStreakLimitDelay = SupremeShops.inst().getConfiguration().getInt("gui.rentable_management_slots.rent_period_streak_limit_delay", 13);
		rentableManagementGuiSlotRentConditions = SupremeShops.inst().getConfiguration().getInt("gui.rentable_management_slots.rent_conditions", 15);
		rentableManagementGuiSlotUnrentedParticlePattern = SupremeShops.inst().getConfiguration().getInt("gui.rentable_management_slots.unrented_particle_pattern", 29);
		rentableManagementGuiSlotRentableToggle = SupremeShops.inst().getConfiguration().getInt("gui.rentable_management_slots.rentable_toggle", 32);
		rentableManagementGuiSlotForceStopRenting = SupremeShops.inst().getConfiguration().getInt("gui.rentable_management_slots.force_stop_renting", 43);
		rentableManagementGuiSlotDestroy = SupremeShops.inst().getConfiguration().getInt("gui.rentable_management_slots.destroy", 49);
		rentableManagementGuiSlotBack = SupremeShops.inst().getConfiguration().getInt("gui.rentable_management_slots.back", 52);

		// particle patterns
		particlePatterns.clear();
		YMLConfiguration patterns = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/particle_patterns/registration.yml"), "particle_patterns/registration.yml", true, true);
		for (String id : patterns.getKeysForSection("patterns", false)) {
			// load script
			File scriptFile = new File(SupremeShops.inst().getDataFolder() + "/particle_patterns/scripts/" + patterns.getString("patterns." + id + ".script", null));
			ParticleScript script;
			try {
				ParticleScriptDecoder decoder = new ParticleScriptDecoder(scriptFile);
				decoder.decode();
				script = decoder.getScript();
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Couldn't load particle pattern " + id + ", unknown error while decoding script");
				continue;
			}
			// load settings
			String name = patterns.getStringFormatted("patterns." + id + ".name", null);
			if (name == null) {
				SupremeShops.inst().error("Couldn't load particle pattern " + id + ", missing setting 'name'");
				continue;
			}
			ParticlePatternAvailability availability = patterns.getEnumValue("patterns." + id + ".availability", ParticlePatternAvailability.class, (ParticlePatternAvailability) null);
			if (availability == null) {
				SupremeShops.inst().error("Couldn't load particle pattern " + id + ", missing valid setting 'availability'");
				continue;
			}
			ItemData icon = patterns.getItem("patterns." + id + ".icon");
			if (icon == null) {
				SupremeShops.inst().error("Couldn't load particle pattern " + id + ", missing setting 'icon'");
				continue;
			}
			String rawPermission = patterns.getString("patterns." + id + ".permission", null);
			Perm permission = rawPermission != null && !rawPermission.isEmpty() ? new Perm(null, rawPermission) : null;
			id = id.toLowerCase();
			particlePatterns.put(id, new ParticlePattern(id, name, permission, availability, icon, script));
		}
		if (!particlePatterns.isEmpty()) SupremeShops.inst().success("Loaded " + particlePatterns.size() + " particle pattern" + Utils.getPlural(particlePatterns.size()) + " : " + Utils.asNiceString(particlePatterns.keySet(), true));

		// merchant skins
		merchantSkins.clear();
		YMLConfiguration merchantSkinsConfig = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/merchants/skins.yml"), "merchants/skins.yml", true, true);
		for (String id : merchantSkinsConfig.getKeysForSection("merchant_skins", false)) {
			String data = merchantSkinsConfig.getStringFormatted("merchant_skins." + id + ".data", null);
			if (data == null) {
				SupremeShops.inst().error("Couldn't load merchant skin " + id + ", missing setting 'data'");
				continue;
			}
			String signature = merchantSkinsConfig.getStringFormatted("merchant_skins." + id + ".signature", null);
			if (signature == null) {
				SupremeShops.inst().error("Couldn't load merchant skin " + id + ", missing setting 'signature'");
				continue;
			}
			ItemData icon = merchantSkinsConfig.getItem("merchant_skins." + id + ".icon");
			if (icon == null) {
				SupremeShops.inst().error("Couldn't load merchant skin " + id + ", missing setting 'icon'");
				continue;
			}
			String rawPermission = merchantSkinsConfig.getString("merchant_skins." + id + ".permission", null);
			Perm permission = rawPermission != null && !rawPermission.isEmpty() ? new Perm(null, rawPermission) : null;
			id = id.toLowerCase();
			merchantSkins.put(id, new NpcMerchantSkin(id, icon, permission, data, signature));
		}
		if (!merchantSkins.isEmpty()) SupremeShops.inst().success("Loaded " + merchantSkins.size() + " merchant skin" + Utils.getPlural(merchantSkins.size()) + " : " + Utils.asNiceString(merchantSkins.keySet(), true));

		// merchant equipments
		merchantEquipments.clear();
		YMLConfiguration merchantEquipmentsConfig = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/merchants/equipments.yml"), "merchants/equipments.yml", true, true);
		for (String id : merchantEquipmentsConfig.getKeysForSection("merchant_equipments", false)) {
			ItemData helmet = merchantEquipmentsConfig.getItem("merchant_equipments." + id + ".helmet", null);
			ItemData chestplate = merchantEquipmentsConfig.getItem("merchant_equipments." + id + ".chestplate", null);
			ItemData leggings = merchantEquipmentsConfig.getItem("merchant_equipments." + id + ".leggings", null);
			ItemData boots = merchantEquipmentsConfig.getItem("merchant_equipments." + id + ".boots", null);
			ItemData mainHand = merchantEquipmentsConfig.getItem("merchant_equipments." + id + ".main_hand", null);
			ItemData offHand = merchantEquipmentsConfig.getItem("merchant_equipments." + id + ".off_hand", null);
			ItemData icon = merchantEquipmentsConfig.getItem("merchant_equipments." + id + ".icon");
			if (icon == null) {
				SupremeShops.inst().error("Couldn't load merchant equipment " + id + ", missing setting 'icon'");
				continue;
			}
			String rawPermission = merchantEquipmentsConfig.getString("merchant_equipments." + id + ".permission", null);
			Perm permission = rawPermission != null && !rawPermission.isEmpty() ? new Perm(null, rawPermission) : null;
			id = id.toLowerCase();
			merchantEquipments.put(id, new NpcMerchantEquipment(id, icon, permission, helmet, chestplate, leggings, boots, mainHand, offHand));
		}
		if (!merchantEquipments.isEmpty()) SupremeShops.inst().success("Loaded " + merchantEquipments.size() + " merchant equipment" + Utils.getPlural(merchantEquipments.size()) + " : " + Utils.asNiceString(merchantEquipments.keySet(), true));

		// settings : item blacklist
		itemBlacklist.clear();
		YMLConfiguration blacklistConfig = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/items/blacklist.yml"), "items/blacklist.yml", true, true);
		enableItemBlacklist = blacklistConfig.getBoolean("enable", false);
		for (String key : blacklistConfig.getKeysForSection("list", false)) {
			ItemData item = blacklistConfig.getItem("list." + key);
			boolean exactMatch = blacklistConfig.getBoolean("list." + key + ".exact_match", true);
			boolean durabilityCheck = blacklistConfig.getBoolean("list." + key + ".durability_check", true);
			itemBlacklist.add(new ListItem(item, exactMatch, durabilityCheck));
		}
		if (!itemBlacklist.isEmpty()) SupremeShops.inst().success("Loaded " + itemBlacklist.size() + " blacklisted item" + Utils.getPlural(itemBlacklist.size()) + " : " + Utils.asNiceString(itemBlacklist, true));

		// settings : item whitelist
		itemWhitelist.clear();
		YMLConfiguration whitelistConfig = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/items/whitelist.yml"), "items/whitelist.yml", true, true);
		enableItemBlacklist = whitelistConfig.getBoolean("enable", false);
		for (String key : whitelistConfig.getKeysForSection("list", false)) {
			ItemData item = whitelistConfig.getItem("list." + key);
			boolean exactMatch = whitelistConfig.getBoolean("list." + key + ".exact_match", true);
			boolean durabilityCheck = whitelistConfig.getBoolean("list." + key + ".durability_check", true);
			itemWhitelist.add(new ListItem(item, exactMatch, durabilityCheck));
		}
		if (!itemWhitelist.isEmpty()) SupremeShops.inst().success("Loaded " + itemWhitelist.size() + " whitelisted item" + Utils.getPlural(itemWhitelist.size()) + " : " + Utils.asNiceString(itemWhitelist, true));

		// settings : shop existence conditions
		shopExistenceConditions.clear();
		YMLConfiguration shopExistenceConditionsConfig = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/existence_conditions.yml"), "shops/existence_conditions.yml", true, true);
		for (String id : shopExistenceConditionsConfig.getKeysForSection("conditions", false)) {
			try {
				ConfigData data = new ConfigData(SupremeShops.inst(), "shop existence conditions " + id, shopExistenceConditionsConfig, "conditions." + id);
				CPConditions conditions = new CPConditions(id, null, false, -1, EditorGUI.ICON_CONDITION, null);
				conditions.load(data);
				shopExistenceConditions.put(id.toLowerCase(), conditions);
				if (conditions.hasErrors()) {
					SupremeShops.inst().warning("Loaded shop existence conditions '" + id + "' with errors, it might not work as intended, plz fix");
				}
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Couldn't load shop existence conditions '" + id + "'");
			}
		}
		if (!shopExistenceConditions.isEmpty()) SupremeShops.inst().success("Loaded " + shopExistenceConditions.size() + " shop existence condition" + Utils.getPlural(shopExistenceConditions.size()) + " : " + Utils.asNiceString(shopExistenceConditions.keySet(), true));

		// settings : shop destruction conditions
		shopDestructionConditions.clear();
		YMLConfiguration shopDestructionConditionsConfig = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/destruction_conditions.yml"), "shops/destruction_conditions.yml", true, true);
		for (String id : shopDestructionConditionsConfig.getKeysForSection("conditions", false)) {
			try {
				ConfigData data = new ConfigData(SupremeShops.inst(), "shop destruction conditions " + id, shopDestructionConditionsConfig, "conditions." + id);
				CPConditions conditions = new CPConditions(id, null, false, -1, EditorGUI.ICON_CONDITION, null);
				conditions.load(data);
				shopDestructionConditions.put(id.toLowerCase(), conditions);
				if (conditions.hasErrors()) {
					SupremeShops.inst().warning("Loaded shop destruction conditions '" + id + "' with errors, it might not work as intended, plz fix");
				}
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Couldn't load shop destruction conditions '" + id + "'");
			}
		}
		if (!shopDestructionConditions.isEmpty()) SupremeShops.inst().success("Loaded " + shopDestructionConditions.size() + " shop destruction condition" + Utils.getPlural(shopDestructionConditions.size()) + " : " + Utils.asNiceString(shopDestructionConditions.keySet(), true));

		// settings : merchant existence conditions
		merchantExistenceConditions.clear();
		YMLConfiguration merchantExistenceConditionsConfig = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/merchants/existence_conditions.yml"), "merchants/existence_conditions.yml", true, true);
		for (String id : merchantExistenceConditionsConfig.getKeysForSection("merchant_existence_conditions", false)) {
			try {
				ConfigData data = new ConfigData(SupremeShops.inst(), "merchant existence conditions " + id, merchantExistenceConditionsConfig, "merchant_existence_conditions." + id);
				CPConditions conditions = new CPConditions(id, null, false, -1, EditorGUI.ICON_CONDITION, null);
				conditions.load(data);
				merchantExistenceConditions.put(id.toLowerCase(), conditions);
				if (conditions.hasErrors()) {
					SupremeShops.inst().warning("Loaded merchant existence conditions '" + id + "' with errors, it might not work as intended, plz fix");
				}
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Couldn't load merchant existence conditions '" + id + "'");
			}
		}
		if (!merchantExistenceConditions.isEmpty()) SupremeShops.inst().success("Loaded " + merchantExistenceConditions.size() + " merchant existence condition" + Utils.getPlural(merchantExistenceConditions.size()) + " : " + Utils.asNiceString(merchantExistenceConditions.keySet(), true));

		// settings : merchant destruction conditions
		merchantDestructionConditions.clear();
		YMLConfiguration merchantDestructionConditionsConfig = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/merchants/destruction_conditions.yml"), "merchants/destruction_conditions.yml", true, true);
		for (String id : merchantDestructionConditionsConfig.getKeysForSection("conditions", false)) {
			try {
				ConfigData data = new ConfigData(SupremeShops.inst(), "merchant destruction conditions " + id, merchantDestructionConditionsConfig, "conditions." + id);
				CPConditions conditions = new CPConditions(id, null, false, -1, EditorGUI.ICON_CONDITION, null);
				conditions.load(data);
				merchantDestructionConditions.put(id.toLowerCase(), conditions);
				if (conditions.hasErrors()) {
					SupremeShops.inst().warning("Loaded merchant destruction conditions '" + id + "' with errors, it might not work as intended, plz fix");
				}
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Couldn't load merchant destruction conditions '" + id + "'");
			}
		}
		if (!merchantDestructionConditions.isEmpty()) SupremeShops.inst().success("Loaded " + merchantDestructionConditions.size() + " merchant destruction condition" + Utils.getPlural(merchantDestructionConditions.size()) + " : " + Utils.asNiceString(merchantDestructionConditions.keySet(), true));

		// settings : shop creation taxes
		shopCreationTaxes.clear();
		for (String id : SupremeShops.inst().getConfiguration().getKeysForSection("shop_creation_taxes", false)) {
			try {
				ConfigData data = new ConfigData(SupremeShops.inst(), "shop creation tax " + id, SupremeShops.inst().getConfiguration(), "shop_creation_taxes." + id);
				CreationTax tax = new CreationTax(id, null, false, -1, EditorGUI.ICON_TECHNICAL, null);
				tax.load(data);
				shopCreationTaxes.put(id.toLowerCase(), tax);
				if (tax.hasErrors()) {
					SupremeShops.inst().warning("Loaded shop creation tax '" + id + "' with errors, it might not work as intended, plz fix");
				}
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Couldn't load shop creation tax '" + id + "'");
			}
		}
		if (!shopCreationTaxes.isEmpty()) SupremeShops.inst().success("Loaded " + shopCreationTaxes.size() + " shop creation tax" + Utils.getPlural("es", shopCreationTaxes.size()) + " : " + Utils.asNiceString(shopCreationTaxes.keySet(), true));

		// settings : merchant creation taxes
		merchantCreationTaxes.clear();
		for (String id : SupremeShops.inst().getConfiguration().getKeysForSection("merchant_creation_taxes", false)) {
			try {
				ConfigData data = new ConfigData(SupremeShops.inst(), "merchant creation tax " + id, SupremeShops.inst().getConfiguration(), "merchant_creation_taxes." + id);
				CreationTax tax = new CreationTax(id, null, false, -1, EditorGUI.ICON_TECHNICAL, null);
				tax.load(data);
				merchantCreationTaxes.put(id.toLowerCase(), tax);
				if (tax.hasErrors()) {
					SupremeShops.inst().warning("Loaded merchant creation tax '" + id + "' with errors, it might not work as intended, plz fix");
				}
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Couldn't load merchant creation tax '" + id + "'");
			}
		}
		if (!merchantCreationTaxes.isEmpty()) SupremeShops.inst().success("Loaded " + merchantCreationTaxes.size() + " merchant creation tax" + Utils.getPlural("es", merchantCreationTaxes.size()) + " : " + Utils.asNiceString(merchantCreationTaxes.keySet(), true));

		// settings : trade modifiers
		tradeModifiers.clear();
		YMLConfiguration tradeModifiersConfig = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/trade_modifiers.yml"), "shops/trade_modifiers.yml", true, true);
		for (String id : tradeModifiersConfig.getKeysForSection("modifiers", false)) {
			try {
				ConfigData data = new ConfigData(SupremeShops.inst(), "trade modifier " + id, tradeModifiersConfig, "modifiers." + id);
				Modifier modifier = new Modifier(id, null, false, -1, EditorGUI.ICON_MODIFIER, null);
				modifier.load(data);
				tradeModifiers.put(id.toLowerCase(), modifier);
				if (modifier.hasErrors()) {
					SupremeShops.inst().warning("Loaded trade modifier '" + id + "' with errors, it might not work as intended, plz fix");
				}
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Couldn't load trade modifier '" + id + "'");
			}
		}
		if (!tradeModifiers.isEmpty()) SupremeShops.inst().success("Loaded " + tradeModifiers.size() + " trade modifier" + Utils.getPlural(tradeModifiers.size()) + " : " + Utils.asNiceString(tradeModifiers.keySet(), true));

		// settings : trade modifiers taxes
		tradeModifiersTaxes.clear();
		YMLConfiguration tradeModifiersTaxesConfig = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/trade_modifiers_taxes.yml"), "shops/trade_modifiers_taxes.yml", true, true);
		for (String id : tradeModifiersTaxesConfig.getKeysForSection("modifiers", false)) {
			try {
				ConfigData data = new ConfigData(SupremeShops.inst(), "trade modifier tax " + id, tradeModifiersTaxesConfig, "modifiers." + id);
				Modifier modifier = new Modifier(id, null, false, -1, EditorGUI.ICON_MODIFIER, null);
				modifier.load(data);
				tradeModifiersTaxes.put(id.toLowerCase(), modifier);
				if (modifier.hasErrors()) {
					SupremeShops.inst().warning("Loaded trade modifier tax '" + id + "' with errors, it might not work as intended, plz fix");
				}
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Couldn't load trade modifier tax '" + id + "'");
			}
		}
		if (!tradeModifiersTaxes.isEmpty()) SupremeShops.inst().success("Loaded " + tradeModifiersTaxes.size() + " trade modifier tax" + Utils.getPlural("es", tradeModifiersTaxes.size()) + " : " + Utils.asNiceString(tradeModifiersTaxes.keySet(), true));

		// settings : GUIs
		guis.clear();
		File guisFolder = new File(SupremeShops.inst().getDataFolder() + "/guis/");
		if (guisFolder.exists() && guisFolder.isDirectory()) {
			for (File file : guisFolder.listFiles()) {
				String id = file.getName().toLowerCase();
				if (id.endsWith(".yml")) {
					id = id.substring(0, id.length() - 4);
					try {
						ConfigData data = new ConfigData(SupremeShops.inst(), "gui " + id, new YMLConfiguration(SupremeShops.inst(), file, null, false, true), "");
						SSGui gui = new SSGui(id, file, null, false, -1, EditorGUI.ICON_GUI, null);
						gui.load(data);
						guis.put(id, gui);
						if (gui.hasErrors()) {
							SupremeShops.inst().warning("Loaded GUI '" + id + "' with errors, it might not work as intended, plz fix");
						}
					} catch (Throwable exception) {
						if (exception instanceof InvalidSettingError) {
							SupremeShops.inst().error("Couldn't load GUI " + id + " : " + exception.getMessage());
						} else {
							exception.printStackTrace();
							SupremeShops.inst().error("Couldn't load GUI " + id + " (unknown error)");
						}
					}
				}
			}
		}
		if (!guis.isEmpty()) SupremeShops.inst().success("Loaded " + guis.size() + " gui" + Utils.getPlural(guis.size()) + " : " + Utils.asNiceString(guis.keySet(), true));

		// settings : admin shops
		adminShops.clear();
		File adminShopsFolder = new File(SupremeShops.inst().getDataFolder() + "/shops/admin/shops/");
		if (adminShopsFolder.exists() && adminShopsFolder.isDirectory()) {
			for (File file : adminShopsFolder.listFiles()) {
				String id = file.getName().toLowerCase();
				if (id.endsWith(".yml")) {
					id = id.substring(0, id.length() - 4);
					try {
						ConfigData data = new ConfigData(SupremeShops.inst(), "admin shop " + id, new YMLConfiguration(SupremeShops.inst(), file, null, false, true), "");
						AdminShopData adminShop = new AdminShopData(id, file, null, false, -1, EditorGUI.ICON_OBJECT, null);
						adminShop.load(data);
						adminShops.put(id, adminShop);
						if (adminShop.hasErrors()) {
							SupremeShops.inst().warning("Loaded admin shop '" + id + "' with errors, it might not work as intended, plz fix");
						}
					} catch (Throwable exception) {
						if (exception instanceof InvalidSettingError) {
							SupremeShops.inst().error("Couldn't load admin shop " + id + " : " + exception.getMessage());
						} else {
							exception.printStackTrace();
							SupremeShops.inst().error("Couldn't load admin shop " + id + " (unknown error)");
						}
					}
				}
			}
		}
		if (!adminShops.isEmpty()) SupremeShops.inst().success("Loaded " + adminShops.size() + " admin shop" + Utils.getPlural(adminShops.size()) + " : " + Utils.asNiceString(adminShops.keySet(), true));

		// settings : triggers
		triggers.clear();
		YMLConfiguration triggersConfig = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/triggers.yml"), "triggers.yml", true, true);
		for (String id : triggersConfig.getKeysForSection("triggers", false)) {
			try {
				ConfigData data = new ConfigData(SupremeShops.inst(), "trigger" + id, triggersConfig, "triggers." + id);
				Trigger trigger = Trigger.load(id, null, data, false, -1, EditorGUI.ICON_TRIGGER, null);
				if (trigger != null) {
					triggers.put(id.toLowerCase(), trigger);
					addTriggerToByClassList(trigger.getClass(), trigger);
					if (trigger.hasErrors()) {
						SupremeShops.inst().warning("Loaded trigger '" + id + "' with errors, it might not work as intended, plz fix");
					}
				}
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Couldn't load trigger '" + id + "'");
			}
		}
		if (!triggers.isEmpty()) SupremeShops.inst().success("Loaded " + triggers.size() + " trigger" + Utils.getPlural(triggers.size()) + " : " + Utils.asNiceString(triggers.keySet(), true));

		// item value manager
		itemValueManager.reloadConfig();
	}

	private <T extends Trigger> List<T> addTriggerToByClassList(Class<T> clazz, Trigger trigger) {
		List<T> list = (List<T>) triggersByClass.get(clazz);
		if (list == null) triggersByClass.put(clazz, list = new ArrayList<T>());
		list.add((T) trigger);
		return list;
	}

	private void loadPermissibleListInteger(YMLConfiguration config, String path, List<Integer> list, Map<Integer, Perm> perms, String logName, String logNamePlural) {
		list.clear();
		perms.clear();
		for (String raw : SupremeShops.inst().getConfiguration().getList(path, Utils.emptyList())) {
			String[] split = raw.contains(",") ? raw.split(",") : new String[] { raw };
			Integer amount = Utils.integerOrNull(split[0]);
			if (amount == null) {
				SupremeShops.inst().error("Couldn't load maximum " + logNamePlural + " " + raw + " (invalid number " + split[0] + ")");
				continue;
			}
			if (list.contains(amount)) {
				SupremeShops.inst().error("Couldn't load maximum " + logNamePlural + " " + raw + " (duplicate amount " + split[0] + ")");
				continue;
			}
			list.add(amount);
			perms.put(amount, split.length >= 2 ? new Perm(null, split[1]) : null);
		}
		if (!list.isEmpty()) SupremeShops.inst().success("Loaded " + list.size() + " " + logName + " setting" + Utils.getPlural(list.size()) + " : " + Utils.asNiceString(list, true));
	}

	private void loadPermissibleListDouble(YMLConfiguration config, String path, List<Double> list, Map<Double, Perm> perms, String logName, String logNamePlural) {
		list.clear();
		perms.clear();
		for (String raw : SupremeShops.inst().getConfiguration().getList(path, Utils.emptyList())) {
			String[] split = raw.contains(",") ? raw.split(",") : new String[] { raw };
			Double amount = Utils.doubleOrNull(split[0]);
			if (amount == null) {
				SupremeShops.inst().error("Couldn't load maximum " + logNamePlural + " " + raw + " (invalid number " + split[0] + ")");
				continue;
			}
			if (list.contains(amount)) {
				SupremeShops.inst().error("Couldn't load maximum " + logNamePlural + " " + raw + " (duplicate amount " + split[0] + ")");
				continue;
			}
			list.add(amount);
			perms.put(amount, split.length >= 2 ? new Perm(null, split[1]) : null);
		}
		if (!list.isEmpty()) SupremeShops.inst().success("Loaded " + list.size() + " " + logName + " setting" + Utils.getPlural(list.size()) + " : " + Utils.asNiceString(list, true));
	}

}
