package com.guillaumevdn.supremeshops.module.itemvalue;

import java.math.BigDecimal;
import java.util.Map;

import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.Utils.CalculationError;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.MainBoard;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;

public class PerMinuteModifiersApplicationTask extends BukkitRunnable {

	@Override
	public void run() {
		// affect all known values
		Map<String, Double> values = SupremeShops.inst().getData().getBoard().getAll(MainBoard.KEY_ITEMVALUE_);
		for (String key : values.keySet()) {
			// get modifier
			String modifier = null;
			if (key.startsWith(MainBoard.KEY_ITEMVALUE_GENERIC_GIVING_)) {
				if (SupremeShops.inst().getModuleManager().getItemValueManager().getGenericConfig() != null) {
					modifier = SupremeShops.inst().getModuleManager().getItemValueManager().getGenericConfig().getPerMinuteModifiers().get(ObjectSide.GIVING);
				}
			} else if (key.startsWith(MainBoard.KEY_ITEMVALUE_GENERIC_TAKING_)) {
				if (SupremeShops.inst().getModuleManager().getItemValueManager().getGenericConfig() != null) {
					modifier = SupremeShops.inst().getModuleManager().getItemValueManager().getGenericConfig().getPerMinuteModifiers().get(ObjectSide.TAKING);
				}
			} else if (key.startsWith(MainBoard.KEY_ITEMVALUE_SPECIFIC_GIVING_)) {
				ItemValueConfig config = SupremeShops.inst().getModuleManager().getItemValueManager().getSpecificConfigsById().get(key.substring(MainBoard.KEY_ITEMVALUE_SPECIFIC_GIVING_.length()));
				if (config != null) {
					modifier = config.getPerMinuteModifiers().get(ObjectSide.GIVING);
					if (modifier == null && SupremeShops.inst().getModuleManager().getItemValueManager().getGenericConfig() != null) {
						modifier = SupremeShops.inst().getModuleManager().getItemValueManager().getGenericConfig().getPerMinuteModifiers().get(ObjectSide.GIVING);
					}
				}
			} else if (key.startsWith(MainBoard.KEY_ITEMVALUE_SPECIFIC_TAKING_)) {
				ItemValueConfig config = SupremeShops.inst().getModuleManager().getItemValueManager().getSpecificConfigsById().get(key.substring(MainBoard.KEY_ITEMVALUE_SPECIFIC_TAKING_.length()));
				if (config != null) {
					modifier = config.getPerMinuteModifiers().get(ObjectSide.TAKING);
					if (modifier == null && SupremeShops.inst().getModuleManager().getItemValueManager().getGenericConfig() != null) {
						modifier = SupremeShops.inst().getModuleManager().getItemValueManager().getGenericConfig().getPerMinuteModifiers().get(ObjectSide.TAKING);
					}
				}
			}
			// no modifier or is empty, skip
			if (modifier == null || modifier.isEmpty()) {
				continue;
			}
			// apply modifier
			String formula = modifier.toLowerCase().replace("{value}", BigDecimal.valueOf(values.get(key)).toPlainString());
			try {
				SupremeShops.inst().getData().getBoard().set(key, Utils.calculateExpression(formula));
			} catch (Throwable exception) {
				if (!(exception instanceof CalculationError)) exception.printStackTrace();
				SupremeShops.inst().error("Couldn't apply per minute modifier to key " + key + " (formula is " + formula + ") : " + exception.getMessage());
			}
		}
	}

}
