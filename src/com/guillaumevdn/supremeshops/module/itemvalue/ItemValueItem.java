package com.guillaumevdn.supremeshops.module.itemvalue;

import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;

public class ItemValueItem {

	// base
	private String key;
	private ItemData item;
	private boolean checkDurability, exactMatch;

	public ItemValueItem(String key, ItemData item, boolean checkDurability, boolean exactMatch) {
		this.item = item;
		this.checkDurability = checkDurability;
	}

	// get
	public String getKey() {
		return key;
	}

	public ItemData getItem() {
		return item;
	}

	public boolean getCheckDurability() {
		return checkDurability;
	}

	public boolean getExactMatch() {
		return exactMatch;
	}

	// overriden methods
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((key == null) ? 0 : key.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemValueItem other = (ItemValueItem) obj;
		if (key == null) {
            return other.key == null;
		} else return key.equals(other.key);
    }

}
