package com.guillaumevdn.supremeshops.module.itemvalue;

import java.io.File;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.Pair;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.Utils.CalculationError;
import com.guillaumevdn.gcorelegacy.lib.versioncompat.sound.Sound;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.MainBoard;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.shop.ShopType;

public class ItemValueManager {

	// base
	private Map<ShopType, ItemValueSetting> shopsSettings = new HashMap<ShopType, ItemValueSetting>();
	private ItemValueSetting adminSetting = ItemValueSetting.ENABLED;
	private ItemValueConfig genericConfig = null;
	private Map<String, ItemValueConfig> specificConfigsById = new HashMap<String, ItemValueConfig>();
	private Map<Mat, Map<ItemValueItem, ItemValueConfig>> specificConfigs = new HashMap<Mat, Map<ItemValueItem, ItemValueConfig>>();
	private Map<Enchantment, Map<Integer, Double>> enchantsMultipliers = new HashMap<Enchantment, Map<Integer, Double>>();
	private double notifyUnbalancedDiff = 25d;
	private boolean notifyUnbalancedPlayerTrades = true;

	// get
	public Map<ShopType, ItemValueSetting> getShopsSettings() {
		return shopsSettings;
	}

	public ItemValueSetting getShopSetting(Shop shop) {
		if (shop.isCurrentOwnerAdmin()) {
			return adminSetting;
		}
		ItemValueSetting setting = shopsSettings.get(shop.getType());
		return setting != null ? setting : ItemValueSetting.DISABLED;
	}

	public ItemValueSetting getAdminSetting() {
		return adminSetting;
	}

	public ItemValueConfig getGenericConfig() {
		return genericConfig;
	}

	public Map<String, ItemValueConfig> getSpecificConfigsById() {
		return specificConfigsById;
	}

	public Map<Mat, Map<ItemValueItem, ItemValueConfig>> getSpecificConfigs() {
		return specificConfigs;
	}

	public Map<Enchantment, Map<Integer, Double>> getEnchantsMultipliers() {
		return enchantsMultipliers;
	}

	public double getNotifyUnbalancedDiff() {
		return notifyUnbalancedDiff;
	}

	public boolean getNotifyUnbalancedPlayerTrades() {
		return notifyUnbalancedPlayerTrades;
	}

	// methods
	public void reloadConfig() {
		// clear
		shopsSettings.clear();
		genericConfig = null;
		specificConfigsById.clear();
		specificConfigs.clear();
		enchantsMultipliers.clear();
		// laod config
		YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/items/values.yml"), "items/values.yml", true, true);
		// setting
		adminSetting = config.getBoolean("admin_setting", true) ? ItemValueSetting.ENABLED : ItemValueSetting.DISABLED;
		for (ShopType shopType : ShopType.values()) {
			ItemValueSetting value = config.getEnumValue("shop_settings." + shopType.name(), ItemValueSetting.class, (ItemValueSetting) null);
			shopsSettings.put(shopType, value != null ? value : ItemValueSetting.DISABLED);
		}
		// misc
		notifyUnbalancedDiff = config.getDouble("notify_unbalanced_diff", 25d);
		notifyUnbalancedPlayerTrades = config.getBoolean("notify_unbalanced_player_trades", true);
		// generic config
		genericConfig = loadConfig("generic", config, "generic_config");
		if (genericConfig != null) SupremeShops.inst().success("Loaded generic item values config");
		// specific items
		int loadedSpecific = 0;
		for (String key : config.getKeysForSection("specific_configs", false)) {
			// invalid key
			if (key.equalsIgnoreCase("generic")) {
				SupremeShops.inst().warning("Key '" + key + "' is not allowed in specific item values config");
				continue;
			}
			// load item
			ItemValueItem valueItem = loadItem(key.toLowerCase(), config, "specific_configs." + key);
			if (valueItem != null) {
				// load config
				ItemValueConfig valueConfig = loadConfig(key.toLowerCase(), config, "specific_configs." + key);
				if (config != null) {
					// add item and config
					Map<ItemValueItem, ItemValueConfig> map = specificConfigs.get(valueItem.getItem().getType());
					if (map == null) specificConfigs.put(valueItem.getItem().getType(), map = new HashMap<ItemValueItem, ItemValueConfig>());
					map.put(valueItem, valueConfig);
					// add by id
					specificConfigsById.put(key.toLowerCase(), valueConfig);
				}
			}
		}
		if (loadedSpecific > 0) SupremeShops.inst().success("Loaded " + loadedSpecific + " specific item value config" + Utils.getPlural(loadedSpecific));
		// enchants
		for (String rawEnchant : config.getKeysForSection("enchant_multipliers", false)) {
			Enchantment enchant = Utils.enchantmentOrNull(rawEnchant);
			if (enchant != null) {
				Map<Integer, Double> levels = new HashMap<Integer, Double>();
				for (String rawLevel : config.getKeysForSection("enchant_multipliers." + rawEnchant, false)) {
					try {
						levels.put(Integer.parseInt(rawLevel), config.getDouble("enchant_multipliers." + rawEnchant + "." + rawLevel, 1D));
					} catch (Throwable ignored) {}
				}
				enchantsMultipliers.put(enchant, levels);
			}
		}
		if (!enchantsMultipliers.isEmpty()) SupremeShops.inst().success("Loaded " + enchantsMultipliers.size() + " item value enchant multipliers" + Utils.getPlural(enchantsMultipliers.size()));
	}

	private ItemValueConfig loadConfig(String key, YMLConfiguration config, String path) {
		try {
			ItemValueConfig result = new ItemValueConfig(key);
			for (String rawSide : config.getKeysForSection(path + ".values", false)) {
				// unknown side
				ObjectSide side = Utils.valueOfOrNull(ObjectSide.class, rawSide.toUpperCase());
				if (side == null) {
					throw new InvalidSettingError("unknown side " + rawSide + " for settings");
				}
				// invalid value
				String subPath = path + ".values." + rawSide;
				double defaultValue = config.getDouble(subPath + ".default_value", 0d);
				if (defaultValue < 0d) throw new InvalidSettingError("default_value must be at least 0 (0 = take the generic setting)");
				double minValue = config.getDouble(subPath + ".min_value", -1d);
				double maxValue = config.getDouble(subPath + ".max_value", -1d);
				String postTradeModifier = config.getString(subPath + ".post_trade_modifier", null);
				String postOppositeTradeModifier = config.getString(subPath + ".post_opposite_trade_modifier", null);
				String perMinuteModifier = config.getString(subPath + ".per_minute_modifier", null);
				// set values
				if (defaultValue > 0d) result.getDefaultValues().put(side, defaultValue);
				if (minValue > 0d) result.getMinValues().put(side, minValue);
				if (maxValue > 0d) result.getMaxValues().put(side, maxValue);
				if (postTradeModifier != null) result.getPostTradeModifiers().put(side, postTradeModifier);
				if (postOppositeTradeModifier != null) result.getPostOppositeTradeModifiers().put(side, postOppositeTradeModifier);
				if (perMinuteModifier != null) result.getPerMinuteModifiers().put(side, perMinuteModifier);
			}
			return result;
		} catch (Throwable exception) {
			if (!(exception instanceof InvalidSettingError)) exception.printStackTrace();
			SupremeShops.inst().error("Couldn't load setting at path '" + path + "' in /items/values.yml : " + exception.getMessage());
			return null;
		}
	}

	private ItemValueItem loadItem(String key, YMLConfiguration config, String path) {
		try {
			ItemData item = config.getItem(path);
			if (item == null) throw new InvalidSettingError("no valid item found");
			if (item.getType() == null || !item.getType().exists()) throw new InvalidSettingError("item type doesn't exist (if it's a custom item type, enable 'allow_custom_materials' in GCoreLegacy/config.yml)");
			boolean checkDurability = config.getBoolean(path + ".check_durability", false);
			boolean exactMatch = config.getBoolean(path + ".exact_match", false);
			return new ItemValueItem(key, item, checkDurability, exactMatch);
		} catch (Throwable exception) {
			if (!(exception instanceof InvalidSettingError)) exception.printStackTrace();
			SupremeShops.inst().error("Couldn't load setting at path '" + path + "' in /items/values.yml : " + exception.getMessage());
			return null;
		}
	}

	public void notifyUnbalancedItemsValue(Player player1, Player player2, double value1, double value2) {
		Sound unbalancedTradeSoundLosing = SupremeShops.inst().getModuleManager().getUnbalancedTradeSoundLosing();
		Sound unbalancedTradeSoundGaining = SupremeShops.inst().getModuleManager().getUnbalancedTradeSoundGaining();
		if (value1 > value2) {
			if (100d - (value1 / value2 * 100d) > notifyUnbalancedDiff) {
				if (player1 != null) {
					SSLocale.MSG_SUPREMESHOPS_NOTIFYUNBALANCEDLOOSINGMONEY.send(player1, "{money}", Utils.round(value1 - value2));
					if (unbalancedTradeSoundLosing != null) unbalancedTradeSoundLosing.play(player1);
				}
				if (player2 != null) {
					SSLocale.MSG_SUPREMESHOPS_NOTIFYUNBALANCEDGAININGMONEY.send(player2, "{money}", Utils.round(value1 - value2));
					if (unbalancedTradeSoundGaining != null) unbalancedTradeSoundGaining.play(player2);
				}
			}
		} else if (value2 > value1) {
			if (100d - (value2 / value1 * 100d) > notifyUnbalancedDiff) {
				if (player2 != null) {
					SSLocale.MSG_SUPREMESHOPS_NOTIFYUNBALANCEDLOOSINGMONEY.send(player2, "{money}", Utils.round(value2 - value1));
					if (unbalancedTradeSoundLosing != null) unbalancedTradeSoundLosing.play(player2);
				}
				if (player1 != null) {
					SSLocale.MSG_SUPREMESHOPS_NOTIFYUNBALANCEDGAININGMONEY.send(player1, "{money}", Utils.round(value2 - value1));
					if (unbalancedTradeSoundGaining != null) unbalancedTradeSoundGaining.play(player1);
				}
			}
		}
	}

	public Pair<ItemValueConfig, ItemValueItem> getConfigFromItem(ItemData item, ObjectSide itemSide) {
		// look for specific
		ItemValueConfig match = null;
		ItemValueItem matchItem = null;
		Map<ItemValueItem, ItemValueConfig> mapForType = specificConfigs.get(item.getType());
		if (mapForType != null) {
			ItemValueConfig lastNonExactMatch = null;
			ItemValueItem lastNonExactMatchItem = null;
			for (ItemValueItem valueItem : mapForType.keySet()) {
				if (valueItem.getItem().isSimilar(item.getItemStack(), valueItem.getCheckDurability(), valueItem.getExactMatch(), false)) {
					if (valueItem.getExactMatch()) {
						match = mapForType.get(valueItem);
						matchItem = valueItem;
						break;
					} else {
						lastNonExactMatch = mapForType.get(valueItem);
						lastNonExactMatchItem = valueItem;
					}
				}
			}
			if (match == null) {
				match = lastNonExactMatch;
				matchItem = lastNonExactMatchItem;
			}
		}
		// done
		return match == null ? null : new Pair<ItemValueConfig, ItemValueItem>(match, matchItem);
	}

	public Pair<String, Double> getCurrentItemValue(ItemData item, ObjectSide itemSide, ItemValueConfig configMatch) {
		// has specific match
		if (configMatch != null) {
			// has value
			String boardKey = (itemSide.equals(ObjectSide.GIVING) ? MainBoard.KEY_ITEMVALUE_SPECIFIC_GIVING_ : (itemSide.equals(ObjectSide.TAKING) ? MainBoard.KEY_ITEMVALUE_SPECIFIC_TAKING_ : MainBoard.KEY_ITEMVALUE_SPECIFIC_ + itemSide.name().toLowerCase() + ":")) + configMatch.getKey().toLowerCase();
			Double value = SupremeShops.inst().getData().getBoard().get(boardKey);
			if (value != null) {
				return new Pair<String, Double>(boardKey, value);
			}
			// has default match value
			value = configMatch.getDefaultValues().get(itemSide);
			if (value != null) {
				// initialize match value and use it
				SupremeShops.inst().getData().getBoard().set(boardKey, value);
				return new Pair<String, Double>(boardKey, value);
			}
			// no default match or generic value
			if (genericConfig == null) {
				return null;
			}
			value = genericConfig.getDefaultValues().get(itemSide);
			if (value == null) {
				return null;
			}
			// initialize generic value and use it
			SupremeShops.inst().getData().getBoard().set(boardKey, value);
			return new Pair<String, Double>(boardKey, value);
		}
		// no specific match
		else {
			// has generic value
			String boardKey = (itemSide.equals(ObjectSide.GIVING) ? MainBoard.KEY_ITEMVALUE_GENERIC_GIVING_ : (itemSide.equals(ObjectSide.TAKING) ? MainBoard.KEY_ITEMVALUE_GENERIC_TAKING_ : MainBoard.KEY_ITEMVALUE_GENERIC_ + itemSide.name().toLowerCase() + ":")) + item.getType().getModernName().toLowerCase();
			Double value = SupremeShops.inst().getData().getBoard().get(boardKey);
			if (value != null) {
				return new Pair<String, Double>(boardKey, value);
			}
			// no default generic value
			if (genericConfig == null) {
				return null;
			}
			value = genericConfig.getDefaultValues().get(itemSide);
			if (value == null) {
				return null;
			}
			// initialize generic value and use it
			SupremeShops.inst().getData().getBoard().set(boardKey, value);
			return new Pair<String, Double>(boardKey, value);
		}
	}

	public double calculateItemValue(ItemData item, ObjectSide itemSide, boolean multiplyForAmount) {
		// look for match
		Pair<ItemValueConfig, ItemValueItem> match = getConfigFromItem(item, itemSide);
		// has no current value
		Pair<String, Double> matchValue = getCurrentItemValue(item, itemSide, match == null ? null : match.getA());
		if (matchValue == null) {
			return -1d;
		}
		// return value
		return multiplyValueWithNonExistingEnchants(item, match == null ? null : match.getB(), matchValue.getB(), multiplyForAmount);
	}

	public double multiplyValueWithNonExistingEnchants(ItemData item, ItemValueItem itemValue, double value, boolean multiplyForAmount) {
		for (Enchantment enchantment : item.getEnchants().keySet()) {
			// skip
			if (!enchantsMultipliers.containsKey(enchantment) || (itemValue != null && itemValue.getItem().getEnchants().containsKey(enchantment))) {
				continue;
			}
			// get multiplier
			Map<Integer, Double> levels = enchantsMultipliers.get(enchantment);
			Integer itemLevel = item.getEnchants().get(enchantment);
			Integer highest = 0;
			for (Integer level : levels.keySet()) {
				if (level <= itemLevel && level > highest) {
					level = highest;
				}
			}
			// multiply
			if (levels.containsKey(highest)) {
				value *= levels.get(highest);
			}
		}
		return multiplyForAmount ? value * ((double) item.getAmount()) : value;
	}

	public void applyPostTradeModifier(ItemData item, ObjectSide itemSide, int trades) {
		// no config and no generic config
		Pair<ItemValueConfig, ItemValueItem> match = getConfigFromItem(item, itemSide);
		if (match == null && genericConfig == null) {
			return;
		}
		// has no current value
		Pair<String, Double> matchValue = getCurrentItemValue(item, itemSide, match == null ? null : match.getA());
		if (matchValue == null) {
			return;
		}
		// get modifier
		String modifier = null;
		if (match != null) {
			modifier = match.getA().getPostTradeModifiers().get(itemSide);
			if (modifier == null && genericConfig != null) {
				modifier = genericConfig.getPostTradeModifiers().get(itemSide);
			}
		} else {
			if (modifier == null && genericConfig != null) {
				modifier = genericConfig.getPostTradeModifiers().get(itemSide);
			}
		}
		// no modifier or is empty, skip
		if (modifier == null || modifier.isEmpty()) {
			return;
		}
		// get min/max values
		Double min = match.getA().getMinValues().get(itemSide);
		if (min == null && genericConfig != null) {
			min = genericConfig.getMinValues().get(itemSide);
		}
		Double max = match.getA().getMaxValues().get(itemSide);
		if (max == null && genericConfig != null) {
			max = genericConfig.getMaxValues().get(itemSide);
		}
		// apply modifier
		String formula = modifier.toLowerCase().replace("{value}", BigDecimal.valueOf(matchValue.getB()).toPlainString()).replace("{trades}", BigDecimal.valueOf(trades).toPlainString());
		try {
			double value = Utils.calculateExpression(formula);
			if (min != null && value < min) {
				value = min;
			} else if (max != null && value > max) {
				value = max;
			}
			SupremeShops.inst().getData().getBoard().set(matchValue.getA(), value);
		} catch (Throwable exception) {
			if (!(exception instanceof CalculationError)) exception.printStackTrace();
			SupremeShops.inst().error("Couldn't apply post trade modifier to key " + matchValue.getA() + " (formula is " + formula + ") : " + exception.getMessage());
		}
	}

	public void applyPostOppositeTradeModifier(ItemData item, ObjectSide itemSide, int trades) {
		// no config and no generic config
		ObjectSide oppositeSide = itemSide.equals(ObjectSide.GIVING) ? ObjectSide.TAKING : ObjectSide.GIVING;
		Pair<ItemValueConfig, ItemValueItem> match = getConfigFromItem(item, oppositeSide);
		if (match == null && genericConfig == null) {
			return;
		}
		// has no current value
		Pair<String, Double> matchValue = getCurrentItemValue(item, oppositeSide, match == null ? null : match.getA());
		if (matchValue == null) {
			return;
		}
		// get modifier
		String modifier = null;
		if (match != null) {
			modifier = match.getA().getPostOppositeTradeModifiers().get(oppositeSide);
			if (modifier == null && genericConfig != null) {
				modifier = genericConfig.getPostOppositeTradeModifiers().get(oppositeSide);
			}
		} else {
			if (modifier == null && genericConfig != null) {
				modifier = genericConfig.getPostOppositeTradeModifiers().get(oppositeSide);
			}
		}
		// no modifier or is empty, skip
		if (modifier == null || modifier.isEmpty()) {
			return;
		}
		// apply modifier
		String formula = modifier.toLowerCase().replace("{value}", BigDecimal.valueOf(matchValue.getB()).toPlainString()).replace("{trades}", BigDecimal.valueOf(trades).toPlainString());
		try {
			SupremeShops.inst().getData().getBoard().set(matchValue.getA(), Utils.calculateExpression(formula));
		} catch (Throwable exception) {
			if (!(exception instanceof CalculationError)) exception.printStackTrace();
			SupremeShops.inst().error("Couldn't apply post opposite trade modifier to key " + matchValue.getA() + " (formula is " + formula + ") : " + exception.getMessage());
		}
	}

	public boolean resetItemValue(ItemData item, ObjectSide itemSide) {
		// look for match
		Pair<ItemValueConfig, ItemValueItem> match = getConfigFromItem(item, itemSide);
		// has specific match
		ItemValueConfig configMatch = match != null ? match.getA() : null;
		if (configMatch != null) {
			// has value
			String boardKey = (itemSide.equals(ObjectSide.GIVING) ? MainBoard.KEY_ITEMVALUE_SPECIFIC_GIVING_ : (itemSide.equals(ObjectSide.TAKING) ? MainBoard.KEY_ITEMVALUE_SPECIFIC_TAKING_ : MainBoard.KEY_ITEMVALUE_SPECIFIC_ + itemSide.name().toLowerCase() + ":")) + configMatch.getKey().toLowerCase();
			Double value = SupremeShops.inst().getData().getBoard().get(boardKey);
			if (value != null) {
				SupremeShops.inst().getData().getBoard().remove(boardKey);
				return true;
			}
			// nothing to reset
			return false;
		}
		// no specific match
		else {
			// has generic value
			String boardKey = (itemSide.equals(ObjectSide.GIVING) ? MainBoard.KEY_ITEMVALUE_GENERIC_GIVING_ : (itemSide.equals(ObjectSide.TAKING) ? MainBoard.KEY_ITEMVALUE_GENERIC_TAKING_ : MainBoard.KEY_ITEMVALUE_GENERIC_ + itemSide.name().toLowerCase() + ":")) + item.getType().getModernName().toLowerCase();
			Double value = SupremeShops.inst().getData().getBoard().get(boardKey);
			if (value != null) {
				SupremeShops.inst().getData().getBoard().remove(boardKey);
				return true;
			}
			// nothing to reset
			return false;
		}

	}

}
