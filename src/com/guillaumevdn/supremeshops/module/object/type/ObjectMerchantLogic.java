package com.guillaumevdn.supremeshops.module.object.type;

import java.io.IOException;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.npc.Npc;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.NpcInput;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.merchant.MerchantSelectionGUI;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.object.ObjectLogic;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.playertrade.PlayerTrade;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ObjectMerchantLogic extends ObjectLogic<ObjectMerchant> {

	// base
	public ObjectMerchantLogic() {
		super(SSLocale.MSG_SUPREMESHOPS_PLAYERTRADECREATEMERCHANTADDED);
	}

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ObjectMerchant value) throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectMerchant readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id, ObjectSide side, boolean canEditSide) throws IOException {
		throw new UnsupportedOperationException();
	}

	// add procedure
	@Override
	public boolean canAddToShop(Player player, Shop shop, ObjectSide side) {
		return false;
	}

	@Override
	public double getShopLimit(OfflinePlayer player) {
		return 0d;
	}

	@Override
	public boolean canAddToManageableManagerWage(Player player, Manageable manageable, UserInfo manager) {
		return false;
	}

	@Override
	public boolean canAddToPlayerTrade(Player player, PlayerTrade trade) {
		return SSPerm.SUPREMESHOPS_CREATE_OBJECT_MERCHANT.has(player);
	}

	@Override
	public boolean canAddToRentableRentPrice(Player player, Rentable rentable) {
		return false;
	}

	@Override
	public void startCreateProcedure(final Player player, boolean sendInputMessage, final ObjectSide side, final GUI fromGUI, boolean isAdminShop, final ObjectLogic<ObjectMerchant>.DoneCallback callback) {
		if (sendInputMessage) SSLocale.MSG_SUPREMESHOPS_PLAYERTRADECREATEMERCHANTINPUT.send(player);
		// open selection GUI, but they're free to close it
		new MerchantSelectionGUI(new UserInfo(player), false, player, false, fromGUI, 0) {
			@Override
			protected void onSelect(Merchant merchant) {
				// remove npc input
				GCoreLegacy.inst().getNpcInputs().remove(player);
				// not his merchant
				if (merchant.getCurrentOwner() == null ? !SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player) : !merchant.getCurrentOwner().equals(new UserInfo(player))) {
					SSLocale.MSG_SUPREMESHOPS_MERCHANTNOTYOURS.send(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// can't transfer rent ownership
				if (merchant instanceof Rentable && ((Rentable) merchant).isRented()) {
					SSLocale.MSG_SUPREMESHOPS_MERCHANTCANTTRANSFERRENTED.send(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// create object
				ObjectMerchant object = new ObjectMerchant(UUID.randomUUID().toString().replace("-", "").substring(0, 5), side, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
				object.getMerchantId().setValue(Utils.asList(merchant.getId()));
				// callback
				callback.callback(object, 1);
			}
		}.open(player);
		// add npc input
		GCoreLegacy.inst().getNpcInputs().put(player, new NpcInput() {
			@Override
			public void onChoose(Player player, Npc value) {
				// invalid npc
				Merchant merchant = SupremeShops.inst().getGeneralManager().getMerchantByNpc(value);
				if (merchant == null) {
					SSLocale.MSG_SUPREMESHOPS_PLAYERTRADECREATEMERCHANTINVALID.send(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// not his merchant
				if (merchant.getCurrentOwner() == null ? !SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player) : !merchant.getCurrentOwner().equals(new UserInfo(player))) {
					SSLocale.MSG_SUPREMESHOPS_MERCHANTNOTYOURS.send(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// can't transfer rent ownership
				if (merchant instanceof Rentable && ((Rentable) merchant).isRented()) {
					SSLocale.MSG_SUPREMESHOPS_MERCHANTCANTTRANSFERRENTED.send(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// create object
				ObjectMerchant object = new ObjectMerchant(UUID.randomUUID().toString().replace("-", "").substring(0, 5), side, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
				object.getMerchantId().setValue(Utils.asList(merchant.getId()));
				// callback
				callback.callback(object, 1);
			}
		});
	}

}
