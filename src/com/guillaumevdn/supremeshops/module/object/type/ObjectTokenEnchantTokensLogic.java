package com.guillaumevdn.supremeshops.module.object.type;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Map;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.ChatInput;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.itemvalue.ItemValueSetting;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.object.ObjectLogic;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.playertrade.PlayerTrade;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ObjectTokenEnchantTokensLogic extends ObjectLogic<ObjectTokenEnchantTokens> {

	// base
	public ObjectTokenEnchantTokensLogic() {
		super(SSLocale.MSG_SUPREMESHOPS_CREATETOKENENCHANTTOKENSADDED);
	}

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ObjectTokenEnchantTokens value) throws IOException {
		JsonUtils.writeJsonValue(out, "amount", value.getAmount().getValue());
	}

	@Override
	public ObjectTokenEnchantTokens readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id, ObjectSide side, boolean canEditSide) throws IOException {
		ObjectTokenEnchantTokens object = new ObjectTokenEnchantTokens(id, side, canEditSide, parent, false, -1, EditorGUI.ICON_OBJECT, null);
		object.getAmount().setValue(JsonUtils.readJsonValue(in));
		return object;
	}

	// add procedure
	@Override
	public boolean canAddToShop(Player player, Shop shop, ObjectSide side) {
		if (!(SSPerm.SUPREMESHOPS_CREATE_OBJECT_TOKENENCHANT_TOKENS.has(player) && shop.getObjects(null, Utils.asList(ObjectType.TOKENENCHANT_TOKENS)).isEmpty())) {
			return false;
		}
		if (SupremeShops.inst().getModuleManager().getItemValueManager().getShopSetting(shop).equals(ItemValueSetting.ENABLED)) {
			return shop.getObjects(null, Utils.asList(ObjectType.VAULT_MONEY)).isEmpty() || shop.getObjects(null, Utils.asList(ObjectType.ITEM)).isEmpty();
		}
		return true;
	}

	@Override
	public double getShopLimit(OfflinePlayer player) {
		return SupremeShops.inst().getModuleManager().getMaxShopTokenEnchantTokens(player);
	}

	@Override
	public boolean canAddToManageableManagerWage(Player player, Manageable manageable, UserInfo manager) {
		if (!SSPerm.SUPREMESHOPS_CREATE_OBJECT_TOKENENCHANT_TOKENS.has(player)) return false;
		Map<TradeObject, Double> wage = manageable.getManagerWage(manager);
		if (wage != null) {
			for (TradeObject object : wage.keySet()) {
				if (object.getType().equals(ObjectType.TOKENENCHANT_TOKENS)) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public boolean canAddToPlayerTrade(Player player, PlayerTrade trade) {
		return SSPerm.SUPREMESHOPS_CREATE_OBJECT_TOKENENCHANT_TOKENS.has(player);
	}

	@Override
	public boolean canAddToRentableRentPrice(Player player, Rentable rentable) {
		return true;
	}

	@Override
	public void startCreateProcedure(final Player player, boolean sendInputMessage, final ObjectSide side, final GUI fromGUI, boolean isAdminShop, final ObjectLogic<ObjectTokenEnchantTokens>.DoneCallback callback) {
		// enter amount in chat
		player.closeInventory();
		if (sendInputMessage) SSLocale.MSG_SUPREMESHOPS_CREATETOKENSINPUT.send(player);
		GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
			@Override
			public void onChat(Player player, String value) {
				// cancel
				if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// not a number
				Integer amount = Utils.integerOrNull(value);
				if (amount == null || amount <= 0 || amount > Integer.MAX_VALUE) {
					GLocale.MSG_GENERIC_COMMAND_INVALIDINTPARAM.send(player, "{plugin}", SupremeShops.inst().getName(), "{parameter}", value);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// create object
				ObjectTokenEnchantTokens object = new ObjectTokenEnchantTokens(UUID.randomUUID().toString().replace("-", "").substring(0, 5), side, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
				object.getAmount().setValue(Utils.asList("" + BigDecimal.valueOf((long) amount).toPlainString()));
				// callback
				callback.callback(object, amount);
			}
		});
	}

}
