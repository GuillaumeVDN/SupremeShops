package com.guillaumevdn.supremeshops.module.object.type;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPDouble;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.shop.management.RestockVaultMoneyAmountSelectionGUI;
import com.guillaumevdn.supremeshops.module.itemvalue.ItemValueSetting;
import com.guillaumevdn.supremeshops.module.modifier.Modifier;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ObjectVaultMoney extends TradeObject {

	// base
	private PPDouble customAmount = addComponent(new PPDouble("custom_amount", this, "0", 0d, Double.MAX_VALUE, false, getCanEditSide() ? 2 : 1, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_MONEYAMOUNT.getLines()));

	public ObjectVaultMoney(String id, ObjectSide side, boolean canEditSide, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ObjectType.VAULT_MONEY, side, canEditSide, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPDouble getCustomAmount() {
		return customAmount;
	}

	@Override
	public double getCustomAmount(Player parser) {
		return customAmount.getParsedValue(parser);
	}

	// methods
	@Override
	public ItemData getPreviewItem(double amount, Player parser) {
		ItemData item = SupremeShops.inst().getModuleManager().getPreviewGuiItemVaultMoney();
		return new ItemData(item.getId(), item.getSlot(), item.getItemStack("{money}", Utils.round5(amount)));
	}

	@Override
	public String describeForAmount(double amount, Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_MONEYFORMAT.getLine("{amount}", Utils.round5(amount));
	}

	@Override
	public double calculateAmountForTrades(Shop shop, int trades, Collection<Modifier> potentialModifiers, Player parser) {
		// calculate the amount of money needed for the opposite side for one trade
		double amount = -1d;
		if (SupremeShops.inst().getModuleManager().getItemValueManager().getShopSetting(shop).equals(ItemValueSetting.ENABLED)) {
			// get items to calculate
			List<ObjectItem> itemObjects = new ArrayList<ObjectItem>();
			ObjectSide oppositeSide = getSide().equals(ObjectSide.GIVING) ? ObjectSide.TAKING : ObjectSide.GIVING;
			for (TradeObject object : shop.getObjects(Utils.asList(oppositeSide), null)) {
				if (object instanceof ObjectItem) {
					itemObjects.add((ObjectItem) object);
				} else {// we can only calculate cost of items and nothing else so just return the regular money amount
					itemObjects.clear();
					break;
				}
			}
			// calculate value of items
			if (!itemObjects.isEmpty()) {
				amount = 0d;
				for (ObjectItem object : itemObjects) {
					ItemData item = object.getItem(parser).clone();
					item.setAmount((int) object.calculateAmountForTrades(shop, trades, potentialModifiers, parser));
					amount += SupremeShops.inst().getModuleManager().getItemValueManager().calculateItemValue(item, oppositeSide, true);
				}
			}
		}
		if (amount == -1d) {
			amount = getCustomAmount(parser) * ((double) trades);
		}
		// apply modifiers
		amount = Modifier.applyModifiers(this, shop, amount, trades, potentialModifiers, parser);
		// done
		return amount <= 0d ? 0.01d : amount;
	}

	@Override
	public double getPlayerStock(Player player) {
		return GCoreLegacy.inst().getEconomyHandler().get(player);
	}

	@Override
	public boolean has(Player player, double amount) {
		return ! (amount <= 0d) && GCoreLegacy.inst().getEconomyHandler().get(player) >= amount;
	}

	@Override
	public void sendHasntMessage(Player player, double amount) {
		SSLocale.MSG_SUPREMESHOPS_HASNTMESSAGE_VAULTMONEY.send(player, "{money}", Utils.round5(amount));
	}

	@Override
	public void take(Player player, double amount) {
		if (amount > 0d) {
			GCoreLegacy.inst().getEconomyHandler().take(player, amount);
			SSLocale.MSG_SUPREMESHOPS_OBJECTTAKE_VAULTMONEY.send(player, "{money}", describeForAmount(amount, player));
		}
	}

	@Override
	public void give(Player player, double amount) {
		if (amount > 0d) {
			GCoreLegacy.inst().getEconomyHandler().give(player, amount);
			SSLocale.MSG_SUPREMESHOPS_OBJECTGIVE_VAULTMONEY.send(player, "{money}", describeForAmount(amount, player));
		}
	}

	@Override
	public void startRestock(final Player player, final GUI fromGUI, final ObjectRestockLogic logic) {
		// set amount
		new RestockVaultMoneyAmountSelectionGUI(fromGUI, 0) {
			@Override
			protected OfflinePlayer getOwner() {
				return logic.getCurrentOwner();
			}

			@Override
			protected double getTotalMoneyInStock() {
				return logic.getTotalObjectStock(Utils.asList(ObjectType.VAULT_MONEY), true);
			}

			@Override
			protected void onSelect(Player player, double amount) {
				// doesn't have
				if (GCoreLegacy.inst().getEconomyHandler().get(player) < amount) {
					SSLocale.MSG_SUPREMESHOPS_RESTOCKVAULTMONEYDONTHAVE.send(player, "{amount}", Utils.round5(amount));
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// restock
				restockNoCheck(player, fromGUI, amount, logic);
			}
		}.open(player);
	}

	@Override
	public void restockNoCheck(final Player player, final GUI fromGUI, final double amount, final ObjectRestockLogic logic) {
		// take money
		GCoreLegacy.inst().getEconomyHandler().take(player, amount);
		// add stock
		logic.addStockAmount(amount);
		// sound
		if (SupremeShops.inst().getModuleManager().getStockAddSound() != null) {
			SupremeShops.inst().getModuleManager().getStockAddSound().play(player);
		}
		// send message and open from GUI
		SSLocale.MSG_SUPREMESHOPS_RESTOCKVAULTMONEY.send(player, "{amount}", Utils.round5(amount));
		if (fromGUI != null) fromGUI.open(player);
	}

	@Override
	public double restockMax(Player player, GUI fromGUI, ObjectRestockLogic logic) {
		// get amount
		double amount = GCoreLegacy.inst().getEconomyHandler().get(player);
		// too much money in the shop already
		OfflinePlayer owner = logic.getCurrentOwner();
		double current = logic.getTotalObjectStock(Utils.asList(ObjectType.VAULT_MONEY), true);
		double limit = owner == null ? Double.MAX_VALUE /* technically we shouldn't be with an admin shop at this point but meh we never know */
				: SupremeShops.inst().getModuleManager().getMaxShopMoney(owner);
		if (current + amount > limit) {
			amount = limit - current;
			SSLocale.MSG_SUPREMESHOPS_RESTOCKVALUESELECTSTOCKLIMITREACHED.send(player);
		}
		// restock eventually
		if (amount > 0) {
			restockNoCheck(player, fromGUI, amount, logic);
		}
		return amount;
	}

	@Override
	public double withdrawAsMuchStockAsPossible(Player player, Shop shop) {
		double remainingStock = shop.getObjectStock(this);
		if (remainingStock > 0d) {
			// withdraw
			shop.changeModifyObjectStock(this, -remainingStock);
			GCoreLegacy.inst().getEconomyHandler().give(player, remainingStock);
			// notify
			if (remainingStock > 0d) {
				SSLocale.MSG_SUPREMESHOPS_WITHDRAWVAULTMONEY.send(player, "{amount}", Utils.round5(remainingStock));
				// sound
				if (SupremeShops.inst().getModuleManager().getStockWithdrawSound() != null) {
					SupremeShops.inst().getModuleManager().getStockWithdrawSound().play(player);
				}
			}
			return remainingStock;
		}
		return 0d;
	}

	@Override
	public void forceWithdrawAll(OfflinePlayer player, Shop shop) {
		double remainingStock = shop.getObjectStock(this);
		if (remainingStock <= 0) return;
		GCoreLegacy.inst().getEconomyHandler().give(player, remainingStock);
		shop.changeModifyObjectStock(this, -remainingStock);
		// sound
		if (SupremeShops.inst().getModuleManager().getStockWithdrawSound() != null && player.isOnline()) {
			SupremeShops.inst().getModuleManager().getStockWithdrawSound().play(player.getPlayer());
		}
	}

	@Override
	public void forceWithdraw(OfflinePlayer player, double amount) {
		if (amount <= 0) return;
		GCoreLegacy.inst().getEconomyHandler().give(player, amount);
		// sound
		if (SupremeShops.inst().getModuleManager().getStockWithdrawSound() != null && player.isOnline()) {
			SupremeShops.inst().getModuleManager().getStockWithdrawSound().play(player.getPlayer());
		}
	}

	// clone
	protected ObjectVaultMoney() {
		super();
	}

}
