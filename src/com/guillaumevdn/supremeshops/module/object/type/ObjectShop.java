package com.guillaumevdn.supremeshops.module.object.type;

import java.util.Collection;
import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPString;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.modifier.Modifier;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.Locatable;

public class ObjectShop extends TradeObject {

	// base
	private PPString shopId = addComponent(new PPString("shop_id", this, null, true, getCanEditSide() ? 2 : 1, EditorGUI.ICON_STRING, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_SHOPID.getLines()));

	public ObjectShop(String id, ObjectSide side, boolean canEditSide, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ObjectType.SHOP, side, canEditSide, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPString getShopId() {
		return shopId;
	}

	public String getShopId(Player parser) {
		return shopId.getParsedValue(parser);
	}

	public Shop getShop(Player parser) {
		return SupremeShops.inst().getData().getShops().getElement(getShopId(parser));
	}

	// methods
	@Override
	public ItemData getPreviewItem(double amount, Player parser) {
		ItemData item = SupremeShops.inst().getModuleManager().getPreviewGuiItemShop();
		Shop shop = getShop(parser);
		return new ItemData(item.getId(), item.getSlot(), item.getItemStack("{name}", shop.getDisplayName(), "{coords}", shop instanceof Locatable ? ((Locatable) shop).getBlock().toString() : ""));
	}

	@Override
	public String describeForAmount(double amount, Player parser) {
		return "shop " + getShop(parser).getDisplayName();
	}

	@Override
	public double getCustomAmount(Player parser) {
		throw new UnsupportedOperationException();
	}

	@Override
	public double calculateAmountForTrades(Shop shop, int trades, Collection<Modifier> potentialModifiers, Player parser) {
		throw new UnsupportedOperationException();
	}

	@Override
	public double getPlayerStock(Player player) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean has(Player player, double amount) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void sendHasntMessage(Player player, double amount) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void take(Player player, double amount) {
		getShop(player).changeActualOwner(null);
	}

	@Override
	public void give(Player player, double amount) {
		getShop(player).changeActualOwner(new UserInfo(player));
	}

	@Override
	public void startRestock(Player player, GUI fromGUI, ObjectRestockLogic logic) {
	}

	@Override
	public void restockNoCheck(Player player, GUI fromGUI, double amount, ObjectRestockLogic logic) {
	}

	@Override
	public double restockMax(Player player, GUI fromGUI, ObjectRestockLogic logic) {
		return 0d;
	}

	@Override
	public double withdrawAsMuchStockAsPossible(Player player, Shop inShop) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void forceWithdrawAll(OfflinePlayer player, Shop inShop) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public void forceWithdraw(OfflinePlayer player, double amount) {
		throw new UnsupportedOperationException();
	}

	// clone
	protected ObjectShop() {
		super();
	}

}
