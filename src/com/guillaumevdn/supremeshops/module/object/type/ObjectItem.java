package com.guillaumevdn.supremeshops.module.object.type;

import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.container.CPEnchantment;
import com.guillaumevdn.gcorelegacy.lib.parseable.container.CPItem;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.shop.management.RestockItemAmountSelectionGUI;
import com.guillaumevdn.supremeshops.module.modifier.Modifier;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.Locatable;

public class ObjectItem extends TradeObject {

	// base
	private CPItem item = addComponent(new CPItem("item", this, true, getCanEditSide() ? 2 : 1, EditorGUI.ICON_ITEM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_ITEM.getLines()));

	public ObjectItem(String id, ObjectSide side, boolean canEditSide, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ObjectType.ITEM, side, canEditSide, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public CPItem getItem() {
		return item;
	}

	public ItemData getItem(Player parser) {
		return item.getParsedValue(parser);
	}

	// methods
	@Override
	public ItemData getPreviewItem(double amount, Player parser) {
		ItemData clone = getItem(parser).clone();
		clone.setAmount((int) amount);
		return clone;
	}

	@Override
	public String describeForAmount(double amount, Player parser) {
		String name = item.getName(parser);
		String description = Utils.round5(amount) + "x " + (name != null ? "\"" + name + "\"" : item.getType(parser));
		for (CPEnchantment enchant : item.getEnchants().getElements().values()) {
			description += " (" + enchant.getType(parser).getName() + " " + enchant.getLevel(parser) + ")";
		}
		return description;
	}

	@Override
	public double getCustomAmount(Player parser) {
		return (double) item.getAmount(parser);
	}

	@Override
	public double calculateAmountForTrades(Shop shopModifier, int trades, Collection<Modifier> potentialModifiers, Player parser) {
		// get amount for trades
		double amount = trades * item.getAmount(parser);
		// apply modifiers
		amount = Modifier.applyModifiers(this, shopModifier, amount, trades, potentialModifiers, parser);
		// done
		amount = Math.floor(amount);
		return (int) (amount < 1d ? 1 : amount);
	}

	@Override
	public double getPlayerStock(Player player) {
		ItemData parsed = item.getParsedValue(player);
		return Math.floor(parsed.count(player.getInventory(), false, true, SupremeShops.inst().getModuleManager().getItemDurabilityMinPercentage()));
	}

	@Override
	public boolean has(Player player, double amount) {
		ItemData parsed = item.getParsedValue(player);
		return parsed.contains(player.getInventory(), (int) Math.floor(amount), false, true, SupremeShops.inst().getModuleManager().getItemDurabilityMinPercentage());
	}

	@Override
	public void sendHasntMessage(Player player, double amount) {
		SSLocale.MSG_SUPREMESHOPS_HASNTMESSAGE_ITEM.send(player, "{item}", describeForAmount(amount, player));
	}

	@Override
	public void take(Player player, double amount) {
		item.getParsedValue(player).remove(player.getInventory(), (int) amount, false, true, SupremeShops.inst().getModuleManager().getItemDurabilityMinPercentage());
		player.updateInventory();
		SSLocale.MSG_SUPREMESHOPS_OBJECTTAKE_ITEM.send(player, "{item}", describeForAmount(amount, player));
	}

	@Override
	public void give(Player player, double amount) {
		ItemData itemData = item.getParsedValue(player).cloneWithAmount((int) amount);
		Item drop = null;
		for (int splitAmount : itemData.getSplitAmounts(SupremeShops.inst().getModuleManager().getSplitUnstackableItems() ? itemData.getItemStack().getMaxStackSize() : 64)) {
			if (drop != null) {
				drop.getItemStack().setAmount(drop.getItemStack().getAmount() + splitAmount);
			} else {
				drop = itemData.give(player, player.getEyeLocation(), splitAmount, true, splitAmount);
			}
		}
		SSLocale.MSG_SUPREMESHOPS_OBJECTGIVE_ITEM.send(player, "{item}", describeForAmount(amount, player));
	}

	@Override
	public void startRestock(final Player player, final GUI fromGUI, final ObjectRestockLogic logic) {
		// set amount
		new RestockItemAmountSelectionGUI(item.getParsedValue(player), fromGUI, 0) {
			@Override
			protected OfflinePlayer getCurrentOwner() {
				return logic.getCurrentOwner();
			}

			@Override
			protected int getTotalItemsInStock() {
				return (int) logic.getTotalObjectStock(Utils.asList(ObjectType.ITEM), true);
			}

			@Override
			protected void onSelect(Player player, int amount) {
				// doesn't have
				if (item.getParsedValue(player).count(player.getInventory()) < amount) {
					SSLocale.MSG_SUPREMESHOPS_RESTOCKITEMDONTHAVE.send(player, "{item}", describeForAmount(amount, player));
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// restock
				restockNoCheck(player, fromGUI, amount, logic);
			}
		}.open(player);
	}

	@Override
	public void restockNoCheck(Player player, GUI fromGUI, double amount, ObjectRestockLogic logic) {
		// take stacks
		item.getParsedValue(player).remove(player.getInventory(), (int) amount, true, true, SupremeShops.inst().getModuleManager().getItemDurabilityMinPercentage());
		player.updateInventory();
		// add stock
		logic.addStockAmount(amount);
		// sound
		if (SupremeShops.inst().getModuleManager().getStockAddSound() != null) {
			SupremeShops.inst().getModuleManager().getStockAddSound().play(player);
		}
		// send message and open from GUI
		SSLocale.MSG_SUPREMESHOPS_RESTOCKITEM.send(player, "{item}", describeForAmount(amount, player));
		if (fromGUI != null) fromGUI.open(player);
	}

	@Override
	public double restockMax(Player player, GUI fromGUI, ObjectRestockLogic logic) {
		// get amount
		int amount = Integer.MAX_VALUE;
		// more than the player has
		int inInv = item.getParsedValue(player).count(player.getInventory(), false, true, SupremeShops.inst().getModuleManager().getItemDurabilityMinPercentage());
		if (amount > inInv) {
			amount = inInv;
			SSLocale.MSG_SUPREMESHOPS_RESTOCKVALUESELECTDONTHAVE.send(player);
		}
		// too many items in the shop already
		int current = (int) logic.getTotalObjectStock(Utils.asList(ObjectType.ITEM), true);
		OfflinePlayer owner = logic.getCurrentOwner();
		int limit = owner == null ? Integer.MAX_VALUE /* technically we shouldn't be with an admin shop at this point but meh we never know */
				: SupremeShops.inst().getModuleManager().getMaxShopItems(owner);
		if (current + amount > limit) {
			amount = limit - current;
			SSLocale.MSG_SUPREMESHOPS_RESTOCKVALUESELECTSTOCKLIMITREACHED.send(player);
		}
		// restock eventually
		if (amount > 0) {
			restockNoCheck(player, fromGUI, amount, logic);
		}
		return amount;
	}

	@Override
	public double withdrawAsMuchStockAsPossible(Player player, Shop shop) {
		// see how much we can withdraw
		int remainingStock = (int) shop.getObjectStock(this);
		int toWithdraw = item.getParsedValue(player).countFreeFor(player.getInventory(), false, true, SupremeShops.inst().getModuleManager().getItemDurabilityMinPercentage());
		if (toWithdraw > remainingStock) toWithdraw = remainingStock;
		if (toWithdraw > 0) {
			// withdraw
			item.getParsedValue(player).give(player, player.getEyeLocation(), toWithdraw, true, SupremeShops.inst().getModuleManager().getSplitUnstackableItems() ? item.getType(player).getCurrentMaterial().getMaxStackSize() : 64);
			// change stock
			shop.changeModifyObjectStock(this, (double) -toWithdraw);
			// done
			if (toWithdraw > 0) {
				SSLocale.MSG_SUPREMESHOPS_WITHDRAWITEM.send(player, "{item}", describeForAmount(toWithdraw, player));
				// sound
				if (SupremeShops.inst().getModuleManager().getStockWithdrawSound() != null) {
					SupremeShops.inst().getModuleManager().getStockWithdrawSound().play(player);
				}
			}
		}
		return toWithdraw;
	}

	@Override
	public void forceWithdrawAll(OfflinePlayer player, Shop shop) {
		int remainingStock = (int) shop.getObjectStock(this);
		if (remainingStock <= 0) return;
		// online
		Location dropLocation = shop instanceof Locatable ? ((Locatable) shop).getBlock().toLocation().add(0.5d, 1d, 0.5d) : (player.isOnline() ? player.getPlayer().getLocation() : (player.getBedSpawnLocation() != null ? player.getBedSpawnLocation() : new Location(Bukkit.getWorlds().get(0), 0, 100, 0)));
		if (player.isOnline()) {
			Player onlinePlayer = player.getPlayer();
			// withdraw
			int toWithdraw = remainingStock;
			remainingStock = 0;
			Item ent = item.getParsedValue(onlinePlayer).give(onlinePlayer, onlinePlayer.getEyeLocation(), toWithdraw, true, SupremeShops.inst().getModuleManager().getSplitUnstackableItems() ? item.getType(onlinePlayer).getCurrentMaterial().getMaxStackSize() : 64);
			// sound
			if (SupremeShops.inst().getModuleManager().getStockWithdrawSound() != null) {
				SupremeShops.inst().getModuleManager().getStockWithdrawSound().play(onlinePlayer);
			}
			// done
			if (ent != null) {
				SSLocale.MSG_SUPREMESHOPS_FORCEWITHDRAWITEM.send(onlinePlayer, "{item}", describeForAmount(ent.getItemStack().getAmount(), onlinePlayer), "{location}", Utils.serializeWXYZLocation(dropLocation));
			}
		}
		// offline
		else {
			ItemData it = item.getParsedValue(null).clone();
			it.setAmount(remainingStock);
			dropLocation.getWorld().dropItem(dropLocation, it.getItemStack());
			remainingStock = 0;
		}
	}

	@Override
	public void forceWithdraw(OfflinePlayer player, double amount) {
		if (amount <= 0) return;
		// online
		Location dropLocation = player.isOnline() ? player.getPlayer().getLocation() : (player.getBedSpawnLocation() != null ? player.getBedSpawnLocation() : new Location(Bukkit.getWorlds().get(0), 0, 100, 0));
		if (player.isOnline()) {
			Player onlinePlayer = player.getPlayer();
			// withdraw
			int toWithdraw = (int) amount;
			Item ent = item.getParsedValue(onlinePlayer).give(onlinePlayer, onlinePlayer.getEyeLocation(), toWithdraw, true, SupremeShops.inst().getModuleManager().getSplitUnstackableItems() ? item.getType(onlinePlayer).getCurrentMaterial().getMaxStackSize() : 64);
			// sound
			if (SupremeShops.inst().getModuleManager().getStockWithdrawSound() != null) {
				SupremeShops.inst().getModuleManager().getStockWithdrawSound().play(onlinePlayer);
			}
			// done
			if (ent != null) {
				SSLocale.MSG_SUPREMESHOPS_FORCEWITHDRAWITEM.send(onlinePlayer, "{item}", describeForAmount(ent.getItemStack().getAmount(), onlinePlayer), "{location}", Utils.serializeWXYZLocation(dropLocation));
			}
		}
		// offline
		else {
			ItemData it = item.getParsedValue(null).clone();
			it.setAmount((int) amount);
			dropLocation.getWorld().dropItem(dropLocation, it.getItemStack());
		}
	}

	// clone
	protected ObjectItem() {
		super();
	}

}
