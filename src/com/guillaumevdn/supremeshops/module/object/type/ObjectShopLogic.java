package com.guillaumevdn.supremeshops.module.object.type;

import java.io.IOException;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.LocationInput;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.shop.ShopSelectionGUI;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.object.ObjectLogic;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.playertrade.PlayerTrade;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ObjectShopLogic extends ObjectLogic<ObjectShop> {

	// base
	public ObjectShopLogic() {
		super(SSLocale.MSG_SUPREMESHOPS_PLAYERTRADECREATESHOPADDED);
	}


	@Override
	public void writeJsonProperties(JsonWriter out, ObjectShop value) throws IOException {
		throw new UnsupportedOperationException();
	}

	@Override
	public ObjectShop readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id, ObjectSide side, boolean canEditSide) throws IOException {
		throw new UnsupportedOperationException();
	}

	// add procedure
	@Override
	public boolean canAddToShop(Player player, Shop shop, ObjectSide side) {
		return false;
	}

	@Override
	public double getShopLimit(OfflinePlayer player) {
		return 0d;
	}

	@Override
	public boolean canAddToManageableManagerWage(Player player, Manageable manageable, UserInfo manager) {
		return false;
	}

	@Override
	public boolean canAddToPlayerTrade(Player player, PlayerTrade trade) {
		return SSPerm.SUPREMESHOPS_CREATE_OBJECT_SHOP.has(player);
	}

	@Override
	public boolean canAddToRentableRentPrice(Player player, Rentable rentable) {
		return false;
	}

	@Override
	public void startCreateProcedure(final Player player, boolean sendInputMessage, final ObjectSide side, final GUI fromGUI, boolean isAdminShop, final ObjectLogic<ObjectShop>.DoneCallback callback) {
		if (sendInputMessage) SSLocale.MSG_SUPREMESHOPS_PLAYERTRADECREATESHOPINPUT.send(player);
		// open selection GUI, but they're free to close it
		new ShopSelectionGUI(new UserInfo(player), false, player, false, fromGUI, 0) {
			@Override
			protected void onSelect(Shop shop) {
				// remove location input
				GCoreLegacy.inst().getLocationInputs().remove(player);
				// not his shop
				if (shop.getCurrentOwner() == null ? !SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player) : !shop.getCurrentOwner().equals(new UserInfo(player))) {
					SSLocale.MSG_SUPREMESHOPS_SHOPNOTYOURS.send(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// can't transfer rent ownership
				if (shop instanceof Rentable && ((Rentable) shop).isRented()) {
					SSLocale.MSG_SUPREMESHOPS_SHOPCANTTRANSFERRENTED.send(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// create object
				ObjectShop object = new ObjectShop(UUID.randomUUID().toString().replace("-", "").substring(0, 5), side, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
				object.getShopId().setValue(Utils.asList(shop.getId()));
				// callback
				callback.callback(object, 1);
			}
		}.open(player);
		// add location input
		GCoreLegacy.inst().getLocationInputs().put(player, new LocationInput() {
			@Override
			public void onChoose(Player player, Location value) {
				// invalid shop
				Shop shop = null;
				if (value != null) {
					BlockCoords coords = new BlockCoords(value);
					shop = SupremeShops.inst().getData().getShops().getPhysicalElementLinkedToBlock(coords);
					if (shop != null && (shop.isCurrentOwnerAdmin() ? !SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player) : !shop.getCurrentOwner().equals(new UserInfo(player)))) shop = null;
				}
				if (shop == null) {
					SSLocale.MSG_SUPREMESHOPS_PLAYERTRADECREATESHOPINVALID.send(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// not his shop
				if (shop.getCurrentOwner() == null ? !SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player) : !shop.getCurrentOwner().equals(new UserInfo(player))) {
					SSLocale.MSG_SUPREMESHOPS_SHOPNOTYOURS.send(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// can't transfer rent ownership
				if (shop instanceof Rentable && ((Rentable) shop).isRented()) {
					SSLocale.MSG_SUPREMESHOPS_SHOPCANTTRANSFERRENTED.send(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// create object
				ObjectShop object = new ObjectShop(UUID.randomUUID().toString().replace("-", "").substring(0, 5), side, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
				object.getShopId().setValue(Utils.asList(shop.getId()));
				// callback
				callback.callback(object, 1);
			}
		});
	}

}
