package com.guillaumevdn.supremeshops.module.object.type;

import java.io.IOException;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.ItemInput;
import com.guillaumevdn.gcorelegacy.lib.versioncompat.Compat;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonToken;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.itemvalue.ItemValueSetting;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.object.ObjectLogic;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.playertrade.PlayerTrade;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ObjectItemLogic extends ObjectLogic<ObjectItem> {

	// base
	public ObjectItemLogic() {
		super(SSLocale.MSG_SUPREMESHOPS_CREATEITEMADDED);
	}

	// json
	private ItemData.GsonAdapter itemAdapter = new ItemData.GsonAdapter();

	@Override
	public void writeJsonProperties(JsonWriter out, ObjectItem value) throws IOException {
		out.name("item");
		itemAdapter.write(out, value.getItem(null));
	}

	@Override
	public ObjectItem readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id, ObjectSide side, boolean canEditSide) throws IOException {
		ObjectItem object = new ObjectItem(id, side, canEditSide, parent, false, -1, EditorGUI.ICON_OBJECT, null);
		in.nextName();
		object.getItem().replace(in.peek().equals(JsonToken.STRING) /* old */? itemAdapter.fromJson(in.nextString()) : itemAdapter.read(in), null);
		return object;
	}

	// add procedure
	@Override
	public boolean canAddToShop(Player player, Shop shop, ObjectSide side) {
		if (!(SSPerm.SUPREMESHOPS_CREATE_OBJECT_ITEM.has(player))) {
			return false;
		}
		if (SupremeShops.inst().getModuleManager().getItemValueManager().getShopSetting(shop).equals(ItemValueSetting.ENABLED)) {
			if (!shop.getObjects(null, Utils.asList(ObjectType.VAULT_MONEY)).isEmpty()) {
				for (TradeObject object : shop.getObjects(null, null)) {
					if (!object.getType().equals(ObjectType.VAULT_MONEY) && !object.getType().equals(ObjectType.ITEM)) {
						return false;
					}
				}
			}
		}
		return true;
	}

	@Override
	public double getShopLimit(OfflinePlayer player) {
		return SupremeShops.inst().getModuleManager().getMaxShopItems(player);
	}

	@Override
	public boolean canAddToManageableManagerWage(Player player, Manageable manageable, UserInfo manager) {
		return SSPerm.SUPREMESHOPS_CREATE_OBJECT_ITEM.has(player);
	}

	@Override
	public boolean canAddToPlayerTrade(Player player, PlayerTrade trade) {
		return SSPerm.SUPREMESHOPS_CREATE_OBJECT_ITEM.has(player);
	}

	@Override
	public boolean canAddToRentableRentPrice(Player player, Rentable rentable) {
		return true;
	}

	@Override
	public void startCreateProcedure(final Player player, boolean sendInputMessage, final ObjectSide side, final GUI fromGUI, boolean isAdminShop, final ObjectLogic<ObjectItem>.DoneCallback onDone) {
		player.closeInventory();
		if (sendInputMessage) SSLocale.MSG_SUPREMESHOPS_PLAYERTRADECREATEITEMINPUT.send(player);
		GCoreLegacy.inst().getItemInputs().put(player, new ItemInput() {
			@Override
			public void onChoose(Player player, ItemStack value) {
				// invalid item
				Mat valueType = Mat.fromItem(value);
				if (value == null || valueType.isAir()) {
					SSLocale.MSG_SUPREMESHOPS_PLAYERTRADECREATEITEMINVALID.send(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// not admin, breakable, not unbreakable and not enough durability
				if (!isAdminShop && valueType.isDamageable() && !Compat.INSTANCE.isUnbreakable(value)) {
					if (100d - (((double) value.getDurability()) / ((double) value.getType().getMaxDurability()) * 100d) < SupremeShops.inst().getModuleManager().getItemDurabilityMinPercentage()) {
						SSLocale.MSG_SUPREMESHOPS_CREATEITEMINVALIDDURABILITY.send(player);
						if (fromGUI != null) fromGUI.open(player);
						return;
					}
				}
				// create object
				ObjectItem object = new ObjectItem(UUID.randomUUID().toString().replace("-", "").substring(0, 5), side, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
				ItemData itemData = new ItemData(value);
				object.getItem().replace(itemData, player);
				// callback
				onDone.callback(object, value.getAmount());
			}
		});
	}
	
}
