package com.guillaumevdn.supremeshops.module.object.type;

import java.util.Collection;
import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.shop.management.RestockTokenEnchantTokensAmountSelectionGUI;
import com.guillaumevdn.supremeshops.module.modifier.Modifier;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.vk2gpz.tokenenchant.TokenEnchant;

public class ObjectTokenEnchantTokens extends TradeObject {

	// base
	private PPInteger amount = addComponent(new PPInteger("amount", this, "0", 0, Integer.MAX_VALUE, false, getCanEditSide() ? 2 : 1, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_TOKENENCHANTTOKENSAMOUNT.getLines()));

	public ObjectTokenEnchantTokens(String id, ObjectSide side, boolean canEditSide, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ObjectType.TOKENENCHANT_TOKENS, side, canEditSide, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPInteger getAmount() {
		return amount;
	}

	public Integer getAmount(Player parser) {
		return amount.getParsedValue(parser);
	}

	// methods
	@Override
	public ItemData getPreviewItem(double amount, Player parser) {
		ItemData item = SupremeShops.inst().getModuleManager().getPreviewGuiItemTokenEnchantTokens();
		return new ItemData(item.getId(), item.getSlot(), item.getItemStack("{amount}", Utils.round5(amount)));
	}

	@Override
	public String describeForAmount(double amount, Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_TOKENENCHANTTOKENSFORMAT.getLine("{amount}", Utils.round5(amount));
	}

	@Override
	public double getCustomAmount(Player parser) {
		return getAmount(parser);
	}

	@Override
	public double calculateAmountForTrades(Shop shop, int trades, Collection<Modifier> potentialModifiers, Player parser) {
		double amount = Modifier.applyModifiers(this, shop, (double) getAmount(parser), trades, potentialModifiers, parser);
		return (amount < 1d ? 1d : amount) * ((double) trades);
	}

	@Override
	public double getPlayerStock(Player player) {
		return ((TokenEnchant) Utils.getPlugin("TokenEnchant")).getTokens(player);
	}

	@Override
	public boolean has(Player player, double amount) {
		return ! (amount <= 0d) && ((TokenEnchant) Utils.getPlugin("TokenEnchant")).getTokens(player) >= amount;
	}

	@Override
	public void sendHasntMessage(Player player, double amount) {
		SSLocale.MSG_SUPREMESHOPS_HASNTMESSAGE_TOKENENCHANTTOKENS.send(player, "{amount}", Utils.round5(amount));
	}

	@Override
	public void take(Player player, double amount) {
		if (amount > 0d) {
			((TokenEnchant) Utils.getPlugin("TokenEnchant")).addTokens(player, (int) amount);
			SSLocale.MSG_SUPREMESHOPS_OBJECTTAKE_TOKENENCHANTTOKENS.send(player, "{amount}", describeForAmount(amount, player));
		}
	}

	@Override
	public void give(Player player, double amount) {
		if (amount > 0d) {
			((TokenEnchant) Utils.getPlugin("TokenEnchant")).removeTokens(player, (int) amount);
			SSLocale.MSG_SUPREMESHOPS_OBJECTGIVE_TOKENENCHANTTOKENS.send(player, "{amount}", describeForAmount(amount, player));
		}
	}

	@Override
	public void startRestock(final Player player, final GUI fromGUI, final ObjectRestockLogic logic) {
		// set amount
		new RestockTokenEnchantTokensAmountSelectionGUI(fromGUI, 0) {
			@Override
			protected OfflinePlayer getOwner() {
				return logic.getCurrentOwner();
			}

			@Override
			protected int getTotalTokenEnchantTokensInStock() {
				return (int) logic.getTotalObjectStock(Utils.asList(ObjectType.TOKENENCHANT_TOKENS), true);
			}

			@Override
			protected void onSelect(Player player, int amount) {
				// doesn't have
				if (((TokenEnchant) Utils.getPlugin("TokenEnchant")).getTokens(player) < amount) {
					SSLocale.MSG_SUPREMESHOPS_RESTOCKTOKENENCHANTTOKENSDONTHAVE.send(player, "{amount}", Utils.round5(amount));
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// restock
				restockNoCheck(player, fromGUI, amount, logic);
			}
		}.open(player);
	}

	@Override
	public void restockNoCheck(final Player player, final GUI fromGUI, final double amount, final ObjectRestockLogic logic) {
		// take level
		((TokenEnchant) Utils.getPlugin("TokenEnchant")).removeTokens(player, (int) amount);
		// add stock
		logic.addStockAmount(amount);
		// sound
		if (SupremeShops.inst().getModuleManager().getStockAddSound() != null) {
			SupremeShops.inst().getModuleManager().getStockAddSound().play(player);
		}
		// send message and open from GUI
		SSLocale.MSG_SUPREMESHOPS_RESTOCKTOKENENCHANTTOKENS.send(player, "{amount}", Utils.round5(amount));
		if (fromGUI != null) fromGUI.open(player);
	}

	@Override
	public double restockMax(Player player, GUI fromGUI, ObjectRestockLogic logic) {
		// get amount
		int amount = (int) ((TokenEnchant) Utils.getPlugin("TokenEnchant")).getTokens(player);
		// too much money in the shop already
		OfflinePlayer owner = logic.getCurrentOwner();
		int current = (int) logic.getTotalObjectStock(Utils.asList(ObjectType.TOKENENCHANT_TOKENS), true);
		int limit = owner == null ? Integer.MAX_VALUE : SupremeShops.inst().getModuleManager().getMaxShopTokenEnchantTokens(owner);
		if (current + amount > limit) {
			amount = limit - current;
			SSLocale.MSG_SUPREMESHOPS_RESTOCKVALUESELECTSTOCKLIMITREACHED.send(player);
		}
		// restock eventually
		if (amount > 0) {
			restockNoCheck(player, fromGUI, amount, logic);
		}
		return amount;
	}

	@Override
	public double withdrawAsMuchStockAsPossible(Player player, Shop shop) {
		double remainingStock = shop.getObjectStock(this);
		if (remainingStock > 0d) {
			// withdraw
			shop.changeModifyObjectStock(this, -remainingStock);
			((TokenEnchant) Utils.getPlugin("TokenEnchant")).addTokens(player, (int) remainingStock);
			// notify
			if (remainingStock > 0d) {
				SSLocale.MSG_SUPREMESHOPS_WITHDRAWTOKENENCHANTTOKENS.send(player, "{amount}", Utils.round5(remainingStock));
				// sound
				if (SupremeShops.inst().getModuleManager().getStockWithdrawSound() != null) {
					SupremeShops.inst().getModuleManager().getStockWithdrawSound().play(player);
				}
			}
			return remainingStock;
		}
		return 0d;
	}

	@Override
	public void forceWithdrawAll(OfflinePlayer player, Shop shop) {
		double remainingStock = shop.getObjectStock(this);
		if (remainingStock <= 0) return;
		((TokenEnchant) Utils.getPlugin("TokenEnchant")).addTokens(player, (int) remainingStock);
		shop.changeModifyObjectStock(this, -remainingStock);
		// sound
		if (SupremeShops.inst().getModuleManager().getStockWithdrawSound() != null && player.isOnline()) {
			SupremeShops.inst().getModuleManager().getStockWithdrawSound().play(player.getPlayer());
		}
	}

	@Override
	public void forceWithdraw(OfflinePlayer player, double amount) {
		if (amount <= 0) return;
		((TokenEnchant) Utils.getPlugin("TokenEnchant")).addTokens(player, (int) amount);
		// sound
		if (SupremeShops.inst().getModuleManager().getStockWithdrawSound() != null && player.isOnline()) {
			SupremeShops.inst().getModuleManager().getStockWithdrawSound().play(player.getPlayer());
		}
	}

	// clone
	protected ObjectTokenEnchantTokens() {
		super();
	}

}
