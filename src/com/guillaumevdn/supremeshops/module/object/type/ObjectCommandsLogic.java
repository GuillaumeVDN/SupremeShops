package com.guillaumevdn.supremeshops.module.object.type;

import java.io.IOException;
import java.util.List;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.ChatInput;
import com.guillaumevdn.gcorelegacy.lib.util.input.ItemInput;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonToken;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.object.ObjectLogic;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.type.ObjectCommands.ExecutionSide;
import com.guillaumevdn.supremeshops.module.playertrade.PlayerTrade;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ObjectCommandsLogic extends ObjectLogic<ObjectCommands> {

	// base
	public ObjectCommandsLogic() {
		super(SSLocale.MSG_SUPREMESHOPS_CREATEITEMADDED);
	}

	// json
	private ItemData.GsonAdapter itemAdapter = new ItemData.GsonAdapter();

	@Override
	public void writeJsonProperties(JsonWriter out, ObjectCommands value) throws IOException {
		JsonUtils.writeJsonValue(out, "commands", value.getCommands().getValue());
		out.name("preview_item");
		itemAdapter.write(out, value.getPreviewItem(null));
	}

	@Override
	public ObjectCommands readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id, ObjectSide side, boolean canEditSide) throws IOException {
		ObjectCommands object = new ObjectCommands(id, side, canEditSide, parent, false, -1, EditorGUI.ICON_OBJECT, null);
		object.getCommands().setValue(JsonUtils.readJsonValue(in));
		in.nextName();
		object.getPreviewItem().replace(in.peek().equals(JsonToken.STRING) /* old */? itemAdapter.fromJson(in.nextString()) : itemAdapter.read(in), null);
		return object;
	}

	// add procedure
	@Override
	public boolean canAddToShop(Player player, Shop shop, ObjectSide side) {
		return SSPerm.SUPREMESHOPS_CREATE_OBJECT_COMMANDS.has(player);
	}

	@Override
	public double getShopLimit(OfflinePlayer player) {
		return Double.MAX_VALUE;
	}

	@Override
	public boolean canAddToManageableManagerWage(Player player, Manageable manageable, UserInfo manager) {
		return SSPerm.SUPREMESHOPS_CREATE_OBJECT_COMMANDS.has(player);
	}

	@Override
	public boolean canAddToPlayerTrade(Player player, PlayerTrade trade) {
		return SSPerm.SUPREMESHOPS_CREATE_OBJECT_COMMANDS.has(player);
	}

	@Override
	public boolean canAddToRentableRentPrice(Player player, Rentable rentable) {
		return true;
	}

	@Override
	public void startCreateProcedure(final Player player, boolean sendInputMessage, final ObjectSide side, final GUI fromGUI, boolean isAdminShop, final ObjectLogic<ObjectCommands>.DoneCallback onDone) {
		if (sendInputMessage) SSLocale.MSG_SUPREMESHOPS_CREATECOMMANDSPREVIEWITEMINPUT.send(player);
		player.closeInventory();
		GCoreLegacy.inst().getItemInputs().put(player, new ItemInput() {
			@Override
			public void onChoose(final Player player, final ItemStack previewItem) {
				// invalid item or not allowed
				if (previewItem == null || Mat.fromItem(previewItem).isAir()) {
					SSLocale.MSG_SUPREMESHOPS_CREATEITEMINVALID.send(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// ask for execution side
				askEnumValue(player, SSLocale.MSG_SUPREMESHOPS_CREATECOMMANDSEXECUTIONSIDEINPUT, fromGUI, ExecutionSide.class, new EnumSelectionCallback<ExecutionSide>() {
					@Override
					public void onSelect(ExecutionSide executionSide) {
						// start asking for commands
						keepAskingCommands(player, Utils.emptyList(), executionSide, new ItemData("preview", previewItem), side, fromGUI, onDone);
					}
				});
			}
		});
	}

	private void keepAskingCommands(final Player player, final List<String> commands, final ExecutionSide executionSide, final ItemData previewItem, final ObjectSide side, final GUI fromGUI, final DoneCallback onDone) {
		SSLocale.MSG_SUPREMESHOPS_CREATECOMMANDSINPUT.send(player, "{amount}", commands.size());
		GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
			@Override
			public void onChat(Player player, String value) {
				// cancel
				String unformat = Utils.unformat(value.trim().toLowerCase());
				if (unformat.equals("cancel")) {
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// stop
				if (unformat.equals("stop")) {
					// empty
					if (commands.isEmpty()) {
						SSLocale.MSG_SUPREMESHOPS_CREATECOMMANDSEMPTY.send(player);
						if (fromGUI != null) fromGUI.open(player);
						return;
					}
					// create object
					ObjectCommands object = new ObjectCommands(UUID.randomUUID().toString().replace("-", "").substring(0, 5), side, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
					object.getCommands().setValue(commands);
					object.getExecutionSide().setValue(Utils.asList(executionSide.name()));
					object.getPreviewItem().replace(previewItem, player);
					// callback
					onDone.callback(object, 1);
					return;
				}
				// add value and keep asking
				commands.add(value);
				keepAskingCommands(player, commands, executionSide, previewItem, side, fromGUI, onDone);
			}
		});
	}

}
