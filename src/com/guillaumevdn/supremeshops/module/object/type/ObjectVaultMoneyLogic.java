package com.guillaumevdn.supremeshops.module.object.type;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Map;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.ChatInput;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.display.items.ItemsDisplayable;
import com.guillaumevdn.supremeshops.module.itemvalue.ItemValueSetting;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.object.ObjectLogic;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.playertrade.PlayerTrade;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.shop.fake.FakeShop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ObjectVaultMoneyLogic extends ObjectLogic<ObjectVaultMoney> {

	// base
	public ObjectVaultMoneyLogic() {
		super(SSLocale.MSG_SUPREMESHOPS_CREATEVAULTMONEYADDED);
	}

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ObjectVaultMoney value) throws IOException {
		JsonUtils.writeJsonValue(out, "custom_amount", value.getCustomAmount().getValue());
	}

	@Override
	public ObjectVaultMoney readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id, ObjectSide side, boolean canEditSide) throws IOException {
		ObjectVaultMoney object = new ObjectVaultMoney(id, side, canEditSide, parent, false, -1, EditorGUI.ICON_OBJECT, null);
		object.getCustomAmount().setValue(JsonUtils.readJsonValue(in));
		return object;
	}

	// add procedure
	@Override
	public boolean canAddToShop(Player player, Shop shop, ObjectSide side) {
		if (!(GCoreLegacy.inst().getEconomyHandler().getUtils() != null && SSPerm.SUPREMESHOPS_CREATE_OBJECT_VAULT_MONEY.has(player) && shop.getObjects(null, Utils.asList(ObjectType.VAULT_MONEY)).isEmpty())) {
			return false;
		}
		if (SupremeShops.inst().getModuleManager().getItemValueManager().getShopSetting(shop).equals(ItemValueSetting.ENABLED)) {
			int itemCount = 0;
			int otherCount = 0;
			for (TradeObject object : shop.getObjects(null, null)) {
				if (object.getType().equals(ObjectType.ITEM)) {
					++itemCount;
				} else {
					++otherCount;
				}
			}
			return itemCount == 0 || otherCount == 0;
		}
		return true;
	}

	@Override
	public double getShopLimit(OfflinePlayer player) {
		return SupremeShops.inst().getModuleManager().getMaxShopMoney(player);
	}

	@Override
	public boolean canAddToManageableManagerWage(Player player, Manageable manageable, UserInfo manager) {
		if (!SSPerm.SUPREMESHOPS_CREATE_OBJECT_VAULT_MONEY.has(player)) return false;
		Map<TradeObject, Double> wage = manageable.getManagerWage(manager);
		if (wage != null) {
			for (TradeObject object : wage.keySet()) {
				if (object.getType().equals(ObjectType.VAULT_MONEY)) {
					return false;
				}
			}
		}
		return true;
	}

	@Override
	public boolean canAddToPlayerTrade(Player player, PlayerTrade trade) {
		return SSPerm.SUPREMESHOPS_CREATE_OBJECT_VAULT_MONEY.has(player);
	}

	@Override
	public boolean canAddToRentableRentPrice(Player player, Rentable rentable) {
		return true;
	}

	@Override
	public void attemptToAddToShop(final Player player, final Shop shop, final ObjectSide side, final GUI fromGUI) {
		// can't add
		if (!canAddToShop(player, shop, side)) {
			throw new UnsupportedOperationException();
		}
		// recommend money eventually
		Collection<TradeObject> oppositeItemObjects = null;
		boolean sendMessage = true;
		if (SupremeShops.inst().getModuleManager().getItemValueManager().getShopSetting(shop).equals(ItemValueSetting.RECOMMENDED) && (oppositeItemObjects = shop.getObjects(Utils.asList(side.equals(ObjectSide.GIVING) ? ObjectSide.TAKING : ObjectSide.GIVING), Utils.asList(ObjectType.ITEM))).size() != 0) {
			double value = 0d;
			for (TradeObject object : oppositeItemObjects) {
				ItemData item = ((ObjectItem) object).getItem(player).clone();
				item.setAmount((int) object.calculateAmountForTrades(shop, 1, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), player));
				value += SupremeShops.inst().getModuleManager().getItemValueManager().calculateItemValue(item, object.getSide(), true);
			}
			SSLocale.MSG_SUPREMESHOPS_CREATEVAULTMONEYRECOMMENDEDINPUT.send(player, "{recommended}", Utils.round5(value));
			sendMessage = false;
		}
		// ask procedure
		startCreateProcedure(player, sendMessage, side, fromGUI, false, new DoneCallback() {
			@Override
			public void callback(ObjectVaultMoney object, double amount) {
				// not allowed
				Shop fake = shop.asFake();
				if (fake != null) {
					((FakeShop) fake).withObject(object, 0d);
					if (!SupremeShops.inst().getModuleManager().canShopExist(player, fake, true)) {
						if (fromGUI != null) fromGUI.open(player);
						return;
					}
				}
				// add object
				shop.addObject(object);
				// remove display items (will be re-added soon anyway)
				if (shop instanceof ItemsDisplayable) {
					if (SupremeShops.inst().getGeneralManager().getItemsDisplayableManager() != null) {
						SupremeShops.inst().getGeneralManager().getItemsDisplayableManager().stop((ItemsDisplayable) shop);
					}
				}
				// sound
				if (SupremeShops.inst().getModuleManager().getObjectAddSound() != null) {
					SupremeShops.inst().getModuleManager().getObjectAddSound().play(player);
				}
				// send message and open from GUI
				getAddedMessage().send(player, "{amount}", Utils.round5(amount));
				if (fromGUI != null) fromGUI.open(player);
				// log
				SupremeShops.inst().pluginLog(shop, null, null, player, null, "Added " + side.name() + " object " + object.getId() + " (" + object.describeForAmount(amount, player) + ")");
				// notify unbalanced trade
				if (SupremeShops.inst().getModuleManager().getItemValueManager().getShopSetting(shop).equals(ItemValueSetting.RECOMMENDED)) {
					double shopGiving = 0d, shopTaking = 0d;
					for (TradeObject giving : shop.getObjects(Utils.asList(ObjectSide.GIVING), Utils.asList(ObjectType.VAULT_MONEY, ObjectType.ITEM))) {
						if (giving instanceof ObjectVaultMoney) {
							shopGiving += giving.calculateAmountForTrades(shop, 1, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), player);
						} else if (giving instanceof ObjectItem) {
							shopGiving += SupremeShops.inst().getModuleManager().getItemValueManager().calculateItemValue(((ObjectItem) giving).getItem(player), giving.getSide(), true);
						}
					}
					for (TradeObject taking : shop.getObjects(Utils.asList(ObjectSide.TAKING), Utils.asList(ObjectType.VAULT_MONEY, ObjectType.ITEM))) {
						if (taking instanceof ObjectVaultMoney) {
							shopTaking += taking.calculateAmountForTrades(shop, 1, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), player);
						} else if (taking instanceof ObjectItem) {
							shopTaking += SupremeShops.inst().getModuleManager().getItemValueManager().calculateItemValue(((ObjectItem) taking).getItem(player), taking.getSide(), true);
						}
					}
					SupremeShops.inst().getModuleManager().getItemValueManager().notifyUnbalancedItemsValue(player, null, shopGiving, shopTaking);
				}
			}
		});
	}

	@Override
	public void startCreateProcedure(final Player player, boolean sendInputMessage, final ObjectSide side, final GUI fromGUI, boolean isAdminShop, final ObjectLogic<ObjectVaultMoney>.DoneCallback callback) {
		// enter amount in chat
		player.closeInventory();
		if (sendInputMessage) SSLocale.MSG_SUPREMESHOPS_CREATEVAULTMONEYINPUT.send(player);
		GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
			@Override
			public void onChat(Player player, String value) {
				// cancel
				if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// not a number
				Double amount = Utils.doubleOrNull(value);
				if (amount == null || amount <= 0d || amount > Double.MAX_VALUE || Double.isNaN(amount) || Double.isInfinite(amount)) {
					GLocale.MSG_GENERIC_COMMAND_INVALIDDOUBLEPARAM.send(player, "{plugin}", SupremeShops.inst().getName(), "{parameter}", value);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// create object
				ObjectVaultMoney object = new ObjectVaultMoney(UUID.randomUUID().toString().replace("-", "").substring(0, 5), side, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
				object.getCustomAmount().setValue(Utils.asList("" + BigDecimal.valueOf(amount).toPlainString()));
				// callback
				callback.callback(object, amount);
			}
		});
	}

}
