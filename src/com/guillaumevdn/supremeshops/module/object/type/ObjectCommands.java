package com.guillaumevdn.supremeshops.module.object.type;

import java.util.Collection;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.container.CPItem;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPEnum;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPStringList;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.module.modifier.Modifier;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ObjectCommands extends TradeObject {

	// base
	private PPStringList commands = addComponent(new PPStringList("commands", this, Utils.emptyList(), true, getCanEditSide() ? 2 : 1, EditorGUI.ICON_TECHNICAL, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_COMMANDS.getLines()));
	private CPItem previewItem = addComponent(new CPItem("preview_item", this, true, getCanEditSide() ? 3 : 2, EditorGUI.ICON_ITEM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_PREVIEWITEM.getLines()));
	private PPEnum<ExecutionSide> executionSide = addComponent(new PPEnum<ExecutionSide>("execution_side", this, ExecutionSide.SERVER.name(), ExecutionSide.class, "execution side", false, getCanEditSide() ? 4 : 3, EditorGUI.ICON_ENUM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_COMMANDSEXECUTIONSIDE.getLines()));
	
	public ObjectCommands(String id, ObjectSide side, boolean canEditSide, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ObjectType.COMMANDS, side, canEditSide, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPStringList getCommands() {
		return commands;
	}

	public List<String> getCommands(Player parser) {
		return commands.getParsedValue(parser);
	}

	public CPItem getPreviewItem() {
		return previewItem;
	}

	public ItemData getPreviewItem(Player parser) {
		return previewItem.getParsedValue(parser);
	}
	
	public PPEnum<ExecutionSide> getExecutionSide() {
		return executionSide;
	}
	
	public ExecutionSide getExecutionSide(Player parser) {
		return executionSide.getParsedValue(parser);
	}

	// methods
	public void perform(Player player, int amount) {
		boolean asPlayer = ExecutionSide.PLAYER.equals(getExecutionSide(player));
		for (String command : getCommands(player)) {
			command = command.replace("{player}", player.getName());
			for (int i = 0; i < amount; ++i) {
				Bukkit.dispatchCommand(asPlayer ? player : Bukkit.getConsoleSender(), command);
			}
		}
	}

	// overriden methods
	@Override
	public ItemData getPreviewItem(double amount, Player parser) {
		ItemData clone = getPreviewItem(parser).clone();
		clone.setAmount((int) amount);
		return clone;
	}

	@Override
	public String describeForAmount(double amount, Player parser) {
		return Utils.round5(amount) + "x " + previewItem.getName(parser);
	}

	@Override
	public double getCustomAmount(Player parser) {
		return 1d;
	}

	@Override
	public double calculateAmountForTrades(Shop shop, int trades, Collection<Modifier> potentialModifiers, Player parser) {
		return trades;
	}

	@Override
	public double getPlayerStock(Player player) {
		return Integer.MAX_VALUE;
	}

	@Override
	public void startRestock(Player player, GUI fromGUI, ObjectRestockLogic callback) {
	}

	@Override
	public void restockNoCheck(Player player, GUI fromGUI, double amount, ObjectRestockLogic callback) {
	}

	@Override
	public double restockMax(Player player, GUI fromGUI, ObjectRestockLogic logic) {
		return 0d;
	}

	@Override
	public boolean has(Player player, double amount) {
		return true;
	}

	@Override
	public void sendHasntMessage(Player player, double amount) {
	}

	@Override
	public void take(Player player, double amount) {
		perform(player, (int) amount);
	}

	@Override
	public void give(Player player, double amount) {
		perform(player, (int) amount);
	}

	@Override
	public double withdrawAsMuchStockAsPossible(Player player, Shop shop) {
		return 0;
	}

	@Override
	public void forceWithdrawAll(OfflinePlayer player, Shop shop) {
	}
	
	@Override
	public void forceWithdraw(OfflinePlayer player, double amount) {
	}

	public static enum ExecutionSide {
		PLAYER, SERVER
	}

	// clone
	protected ObjectCommands() {
		super();
	}

}

