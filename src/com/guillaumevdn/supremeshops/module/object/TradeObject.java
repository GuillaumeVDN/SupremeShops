package com.guillaumevdn.supremeshops.module.object;

import java.util.Collection;
import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ConfigData;
import com.guillaumevdn.gcorelegacy.lib.parseable.ContainerParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.ParseableContainment;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorItem;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.ModifCallback;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.module.modifier.Modifier;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public abstract class TradeObject extends ContainerParseable {

	// base
	private ObjectType type;
	private ObjectSide side;
	private boolean canEditSide;

	public TradeObject(String id, ObjectType type, ObjectSide side, boolean canEditSide, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "trade object", mandatory, editorSlot, editorIcon, editorDescription);
		this.type = type;
		this.side = side;
		this.canEditSide = canEditSide;
	}

	// get
	public ObjectType getType() {
		return type;
	}

	public ObjectSide getSide() {
		return side;
	}

	public boolean getCanEditSide() {
		return canEditSide;
	}

	// abstract
	public abstract ItemData getPreviewItem(double amount, Player parser);
	public abstract String describeForAmount(double amount, Player parser);

	// amount
	public abstract double getCustomAmount(Player parser);
	public abstract double calculateAmountForTrades(Shop shop, int trades, Collection<Modifier> potentialModifiers, Player parser);
	public abstract double getPlayerStock(Player player);

	public int getTradesForStock(double stock, double amount) {
		return amount <= 0d || stock <= 0d ? 0 : (int) Math.floor(stock / amount);
	}

	// stock
	public abstract void startRestock(Player player, GUI fromGUI, ObjectRestockLogic logic);
	public abstract void restockNoCheck(Player player, GUI fromGUI, double amount, ObjectRestockLogic logic);
	public abstract double restockMax(Player player, GUI fromGUI, ObjectRestockLogic logic);

	public interface ObjectRestockLogic {
		OfflinePlayer getCurrentOwner();
		double getTotalObjectStock(List<ObjectType> types, boolean withManagersWages);
		void addStockAmount(double amount);
	}

	// has/take/give
	public abstract boolean has(Player player, double amount);
	public abstract void sendHasntMessage(Player player, double amount);
	public abstract void take(Player player, double amount);
	public abstract void give(Player player, double amount);
	public abstract double withdrawAsMuchStockAsPossible(Player player, Shop shop);
	public abstract void forceWithdrawAll(OfflinePlayer player, Shop shop);
	public abstract void forceWithdraw(OfflinePlayer player, double amount);

	// clone
	protected TradeObject() {
		super();
	}

	@Override
	public TradeObject clone() {
		TradeObject clone = (TradeObject) super.clone();
		clone.type = type;
		return clone;
	}

	@Override
	public <T extends Parseable> T cloneAs(Class<T> clazz) {
		TradeObject clone = (TradeObject) super.cloneAs(clazz);
		clone.type = type;
		return (T) clone;
	}

	// save
	@Override
	public void save(ConfigData data) {
		super.save(data);
		if (data != null) {
			data.getConfig().set(data.getPath() + ".type", type.getId());
			data.getConfig().set(data.getPath() + ".side", side.name());
		}
	}

	// editor
	@Override
	public List<String> describe(int depth) {
		List<String> desc = super.describe(depth);
		String spaces = Utils.copyString(" ", depth + 1);
		desc.add(1, spaces + " §6> type : §e" + type.getId());
		return desc;
	}

	@Override
	protected void fillEditor(final EditorGUI gui, Player player, final ModifCallback onModif) {
		// add type selector
		gui.setRegularItem(new EditorItem("object_type", 0, Mat.ENDER_CHEST, "§6type", SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_TYPELORE.getLines()) {
			@Override
			protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
				// create sub GUI
				String name = Utils.getNewInventoryName(gui.getName(), "type");
				EditorGUI sub = new EditorGUI(getLastData().getPlugin(), gui, name, 9, GUI.SLOTS_0_TO_7) {
					private EditorGUI subThis = this;
					@Override
					protected void fill() {
						// current
						EditorGUI.fillItemCurrent(subThis, player, "type", Utils.asList(type.getId()), null, "quest object type", isMandatory(), getEditorIcon(), 0, onModif);
						// select
						setRegularItem(new EditorItem("control_item_select", 2, Mat.ENDER_CHEST, GLocale.GUI_GENERIC_EDITORENUMSELECT.getLine(), GLocale.GUI_GENERIC_EDITORENUMSELECTLORE.getLines()) {
							@Override
							protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
								// selection gui
								EditorGUI subSelection = new EditorGUI(getLastData().getPlugin(), gui, Utils.getNewInventoryName(gui.getName(), "Select"), 54, GUI.SLOTS_0_TO_44) {
									@Override
									protected void fill() {
										// add values
										for (final ObjectType val : Utils.asSortedList(ObjectType.values(), Utils.objectSorter)) {
											final String valName = val.getId();
											setRegularItem(new EditorItem("value_" + valName, -1, Mat.ENDER_CHEST, "§6" + valName, null) {
												@Override
												protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
													TradeObject curr = TradeObject.this;
													final TradeObject result = val.createNew(curr.getId(), curr.getSide(), curr.getCanEditSide(), curr.getParent(), curr.getLastData().clone(), false, curr.isMandatory(), curr.getEditorSlot(), curr.getEditorIcon(), curr.getEditorDescription());
													// error loading
													if (result == null) {
														player.sendMessage("§cAn unknown error occured, check the console for more details.");
														gui.open(player);
													}
													// success
													else {
														// replace result
														if (Utils.instanceOf(TradeObject.this.getParent(), ParseableContainment.class)) {
															((ParseableContainment<TradeObject>) TradeObject.this.getParent()).replaceContaining(result);
														}
														onModif.callback(result, subThis, player);
														// create result GUI
														String name = Utils.getNewInventoryName(gui.getName(), result.getId());
														EditorGUI comp = new EditorGUI(result.getLastData().getPlugin(), gui.getParent(), name, result.getEditorSize(), result.getEditorRegularSlots()) {
															private EditorGUI compThis = this;
															@Override
															protected void fill() {
																result.fillEditor(compThis, player, onModif);
																// back item (do it there after filling the other GUI because otherwise it doesn't appear somehow)
																compThis.setPersistentItem(new EditorItem("control_item_back", result.getEditorBackSlot(), Mat.ARROW, GLocale.GUI_GENERIC_EDITORITEMBACK.getLine(), null) {
																	@Override
																	protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
																		gui.getParent().open(player);
																	}
																});
															}
														};
														// open it
														comp.open(player);
													}
												}
											});
										}
										// back item
										setPersistentItem(new EditorItem("control_item_back", 52, Mat.ARROW, GLocale.GUI_GENERIC_EDITORITEMBACK.getLine(), null) {
											@Override
											protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
												gui.open(player);
											}
										});
									}
								};
								// open it
								subSelection.open(player);
							}
						});
						// back item
						setPersistentItem(new EditorItem("control_item_back", 8, Mat.ARROW, GLocale.GUI_GENERIC_EDITORITEMBACK.getLine(), null) {
							@Override
							protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
								gui.open(player);
							}
						});
					}
				};
				// open it
				sub.open(player);
			}
		});
		// add side selector
		if (getCanEditSide()) {
			gui.setRegularItem(new EditorItem("object_side", 1, Mat.ENDER_CHEST, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_SIDELORE.getLine(), null) {
				@Override
				protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
					// selection gui
					EditorGUI sub = new EditorGUI(getLastData().getPlugin(), gui, Utils.getNewInventoryName(gui.getName(), "Select"), 54, GUI.SLOTS_0_TO_44) {
						@Override
						protected void fill() {
							// add values
							for (final ObjectSide val : ObjectSide.values()) {
								final String valName = val.name();
								setRegularItem(new EditorItem("value_" + valName, -1, Mat.ENDER_CHEST, "§6" + valName, null) {
									@Override
									protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
										// replace value
										TradeObject.this.side = val;
										onModif.callback(null, gui, player);
										// re-fill and open
										gui.open(player);
									}
								});
							}
							// back item
							setPersistentItem(new EditorItem("control_item_back", 52, Mat.ARROW, GLocale.GUI_GENERIC_EDITORITEMBACK.getLine(), null) {
								@Override
								protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
									gui.open(player);
								}
							});
						}
					};
					// open it
					sub.open(player);
				}
			});
		}
		// super
		super.fillEditor(gui, player, onModif);
	}

	// static methods
	/**
	 * Load an element from a configuration
	 * @return the loaded trade object, or null if couldn't instantiate it for some reason
	 */
	public static TradeObject load(String id, ObjectSide side, boolean canEditSide, Parseable parent, ConfigData data, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		// compact
		ObjectType type = null;
		// missing type setting
		data.setContains(data.getConfig().contains(data.getPath() + ".type"));
		if (!data.contains()) {
			(parent == null || parent.getLastData() == null ? data : parent.getLastData()).log("missing primitive setting 'type' (must be a trade object type)");
			return null;
		}
		// invalid type
		type = ObjectType.valueOf(data.getConfig().getString(data.getPath() + ".type", ""));
		if (type == null) {
			(parent == null || parent.getLastData() == null ? data : parent.getLastData()).log("invalid primitive setting 'type' (must be a trade object type)");
			return null;
		}
		// create
		return type.createNew(id, side, canEditSide, parent, data, true, mandatory, editorSlot, editorIcon, editorDescription);
	}

}
