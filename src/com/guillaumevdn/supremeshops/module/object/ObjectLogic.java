package com.guillaumevdn.supremeshops.module.object;

import java.io.IOException;
import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.messenger.Text;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.Wrapper;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.display.items.ItemsDisplayable;
import com.guillaumevdn.supremeshops.module.itemvalue.ItemValueSetting;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.object.type.ObjectItem;
import com.guillaumevdn.supremeshops.module.object.type.ObjectVaultMoney;
import com.guillaumevdn.supremeshops.module.playertrade.PlayerTrade;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.shop.fake.FakeShop;

// logic agent
public abstract class ObjectLogic<T extends TradeObject> {

	// base
	private Text addedMessage;

	public ObjectLogic(Text addedMessage) {
		this.addedMessage = addedMessage;
	}

	// get
	public Text getAddedMessage() {
		return addedMessage;
	}

	// json
	public abstract void writeJsonProperties(JsonWriter out, T value) throws IOException;
	public abstract T readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id, ObjectSide side, boolean canEditSide) throws IOException;

	// add procedure
	public abstract boolean canAddToShop(Player player, Shop shop, ObjectSide side);
	public abstract double getShopLimit(OfflinePlayer player);
	public abstract boolean canAddToManageableManagerWage(Player player, Manageable manageable, UserInfo manager);
	public abstract boolean canAddToRentableRentPrice(Player player, Rentable rentable);
	public abstract boolean canAddToPlayerTrade(Player player, PlayerTrade trade);
	public abstract void startCreateProcedure(Player player, boolean sendInputMessage, ObjectSide side, GUI fromGUI, boolean isAdminShop, DoneCallback callback);

	public abstract class DoneCallback {
		public abstract void callback(T object, double amount);
	}

	public void attemptToAddToShop(final Player player, final Shop shop, final ObjectSide side, final GUI fromGUI) throws UnsupportedOperationException {
		// can't add
		if (!canAddToShop(player, shop, side)) {
			throw new UnsupportedOperationException();
		}
		// ask procedure
		startCreateProcedure(player, true, side, fromGUI, shop.isCurrentOwnerAdmin(), new DoneCallback() {
			@Override
			public void callback(T object, double amount) {
				// too much
				double limit = shop.isCurrentOwnerAdmin() ? Double.MAX_VALUE : getShopLimit(player);
				if (amount > limit) {
					SSLocale.MSG_SUPREMESHOPS_STOCKLIMITREACHED.send(player, "{limit}", Utils.round5(limit));
					if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
					}
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// not allowed
				Shop fake = shop.asFake();
				if (fake != null) {
					((FakeShop) fake).withObject(object, 0d);
					if (!SupremeShops.inst().getModuleManager().canShopExist(player, fake, true)) {
						if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
						}
						if (fromGUI != null) fromGUI.open(player);
						return;
					}
				}
				// add object
				shop.addObject(object);
				// remove display items (will be re-added soon anyway)
				if (shop instanceof ItemsDisplayable) {
					if (SupremeShops.inst().getGeneralManager().getItemsDisplayableManager() != null) {
						SupremeShops.inst().getGeneralManager().getItemsDisplayableManager().stop((ItemsDisplayable) shop);
					}
				}
				// sound
				if (SupremeShops.inst().getModuleManager().getObjectAddSound() != null) {
					SupremeShops.inst().getModuleManager().getObjectAddSound().play(player);
				}
				// send message and open from GUI
				addedMessage.send(player, "{amount}", Utils.round5(amount));
				if (fromGUI != null) fromGUI.open(player);
				// log
				SupremeShops.inst().pluginLog(shop, null, null, player, null, "Added " + side.name() + " object " + object.getId() + " (" + object.describeForAmount(amount, player) + ")");
				// notify unbalanced trade
				if (SupremeShops.inst().getModuleManager().getItemValueManager().getShopSetting(shop).equals(ItemValueSetting.RECOMMENDED)) {
					double shopGiving = 0d, shopTaking = 0d;
					for (TradeObject giving : shop.getObjects(Utils.asList(ObjectSide.GIVING), Utils.asList(ObjectType.VAULT_MONEY, ObjectType.ITEM))) {
						if (giving instanceof ObjectVaultMoney) {
							shopGiving += giving.calculateAmountForTrades(shop, 1, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), player);
						} else if (giving instanceof ObjectItem) {
							shopGiving += SupremeShops.inst().getModuleManager().getItemValueManager().calculateItemValue(((ObjectItem) giving).getItem(player), giving.getSide(), true);
						}
					}
					for (TradeObject taking : shop.getObjects(Utils.asList(ObjectSide.TAKING), Utils.asList(ObjectType.VAULT_MONEY, ObjectType.ITEM))) {
						if (taking instanceof ObjectVaultMoney) {
							shopTaking += taking.calculateAmountForTrades(shop, 1, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), player);
						} else if (taking instanceof ObjectItem) {
							shopTaking += SupremeShops.inst().getModuleManager().getItemValueManager().calculateItemValue(((ObjectItem) taking).getItem(player), taking.getSide(), true);
						}
					}
					SupremeShops.inst().getModuleManager().getItemValueManager().notifyUnbalancedItemsValue(player, null, shopGiving, shopTaking);
				}
			}
		});
	}

	public void attemptToAddToManageableManagerWage(final Player player, final Manageable manageable, final UserInfo manager, final GUI fromGUI) {
		// can't add
		if (!canAddToManageableManagerWage(player, manageable, manager)) {
			throw new UnsupportedOperationException();
		}
		// ask procedure
		startCreateProcedure(player, true, ObjectSide.GIVING, fromGUI, manageable.isCurrentOwnerAdmin(), new DoneCallback() {
			@Override
			public void callback(T object, double amount) {
				// add object
				manageable.addToManagerWage(manager, object, 0d);
				// sound
				if (SupremeShops.inst().getModuleManager().getObjectAddSound() != null) {
					SupremeShops.inst().getModuleManager().getObjectAddSound().play(player);
				}
				// send message and open from GUI
				addedMessage.send(player, "{amount}", Utils.round5(amount));
				if (fromGUI != null) fromGUI.open(player);
				// log
				SupremeShops.inst().pluginLog(manageable, player, null, "Added object " + object.getId() + " (" + object.describeForAmount(amount, player) + ") to wage of manager " + manager.toStringName());
			}
		});
	}

	public void attemptToAddToRentableRentPrice(final Player player, final Rentable rentable, final GUI fromGUI) {
		// can't add
		if (!canAddToRentableRentPrice(player, rentable)) {
			throw new UnsupportedOperationException();
		}
		// ask procedure
		startCreateProcedure(player, true, ObjectSide.TAKING, fromGUI, rentable.isCurrentOwnerAdmin(), new DoneCallback() {
			@Override
			public void callback(T object, double amount) {
				// add object
				rentable.addRentPrice(object);
				// sound
				if (SupremeShops.inst().getModuleManager().getObjectAddSound() != null) {
					SupremeShops.inst().getModuleManager().getObjectAddSound().play(player);
				}
				// send message and open from GUI
				addedMessage.send(player, "{amount}", Utils.round5(amount));
				if (fromGUI != null) fromGUI.open(player);
				// log
				SupremeShops.inst().pluginLog(rentable, player, null, "Added rent price object " + object.getId() + " (" + object.describeForAmount(amount, player) + ")");
			}
		});
	}

	public void attemptToAddToPlayerTrade(final Player player, final PlayerTrade trade, final GUI fromGUI) {
		// can't add
		if (!canAddToPlayerTrade(player, trade)) {
			throw new UnsupportedOperationException();
		}
		// ask procedure
		startCreateProcedure(player, true, ObjectSide.TAKING, fromGUI, false, new DoneCallback() {
			@Override
			public void callback(T object, double amount) {
				// trade is locked already
				if (trade.isLocked()) {
					SSLocale.MSG_SUPREMESHOPS_PLAYERTRADELOCKED.send(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// don't have
				if (!object.has(player, amount)) {
					object.sendHasntMessage(player, amount);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// take and add
				object.take(player, amount);
				trade.addOffering(player, object);
				// sound
				if (SupremeShops.inst().getModuleManager().getObjectAddSound() != null) {
					SupremeShops.inst().getModuleManager().getObjectAddSound().play(player);
				}
				// send message and open from GUI
				addedMessage.send(player, "{amount}", Utils.round5(amount));
				if (fromGUI != null) fromGUI.open(player);
				// log
				SupremeShops.inst().pluginLog(null, null, null, player, "{SAFE TRADE WITH " + (trade.getPlayer1().equals(player) ? trade.getPlayer2().getName() : trade.getPlayer1().getName()) + "}", "Added object " + object.getId() + " (" + object.describeForAmount(amount, player) + ")");
				// notify unbalanced trade
				if (SupremeShops.inst().getModuleManager().getItemValueManager().getNotifyUnbalancedPlayerTrades()) {
					double value1 = 0d, value2 = 0d;
					for (TradeObject obj : trade.getOffering1()) {
						if (obj instanceof ObjectVaultMoney) {
							value1 += obj.getCustomAmount(trade.getPlayer2());
						} else if (obj instanceof ObjectItem) {
							value1 += SupremeShops.inst().getModuleManager().getItemValueManager().calculateItemValue(((ObjectItem) obj).getItem(trade.getPlayer2()), obj.getSide(), true);
						}
					}
					for (TradeObject obj : trade.getOffering2()) {
						if (obj instanceof ObjectVaultMoney) {
							value1 += obj.getCustomAmount(trade.getPlayer2());
						} else if (obj instanceof ObjectItem) {
							value1 += SupremeShops.inst().getModuleManager().getItemValueManager().calculateItemValue(((ObjectItem) obj).getItem(trade.getPlayer2()), obj.getSide(), true);
						}
					}
					SupremeShops.inst().getModuleManager().getItemValueManager().notifyUnbalancedItemsValue(player, null, value1, value2);
				}
			}
		});
	}

	// utils
	public static <T extends Enum<T>> void askEnumValue(final Player player, final Text message, final GUI fromGUI, final Class<T> enumClazz, final EnumSelectionCallback<T> callback) {
		message.send(player);
		final Wrapper<Boolean> selected = new Wrapper<Boolean>(false);
		GUI gui = new GUI(SupremeShops.inst(), "Select a value", Utils.getInventorySize(enumClazz.getEnumConstants().length), GUI.SLOTS_0_TO_44) {
			@Override
			protected boolean onClose(List<Player> players, int pageIndex) {
				if (!selected.getValue() && fromGUI != null) {
					fromGUI.open(player);
				}
				return false;
			}
		};
		for (final T value : enumClazz.getEnumConstants()) {
			gui.setRegularItem(new ClickeableItem(new ItemData("value_" + value.name(), -1, EditorGUI.ICON_ENUM, 1, Utils.capitalizeUnderscores(value.name()), null)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					selected.setValue(true);
					player.closeInventory();
					callback.onSelect(value);
				}
			});
		}
		gui.open(player);
	}

	public interface EnumSelectionCallback<T extends Enum<T>> {
		void onSelect(T value);
	}

}
