package com.guillaumevdn.supremeshops.module.object;

public enum ObjectSide {

	TAKING,
	GIVING

}
