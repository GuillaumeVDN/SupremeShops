package com.guillaumevdn.supremeshops.module.merchant;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.data.DataElement;
import com.guillaumevdn.gcorelegacy.lib.data.mysql.Query;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.WeekDay;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.merchant.MerchantDestroyedEvent;
import com.guillaumevdn.supremeshops.api.event.merchant.MerchantPaidManagerWageEvent;
import com.guillaumevdn.supremeshops.api.event.merchant.MerchantUpdatedEvent;
import com.guillaumevdn.supremeshops.api.event.merchant.MerchantUpdatedInteractConditionsEvent;
import com.guillaumevdn.supremeshops.api.event.merchant.MerchantUpdatedManagerEvent;
import com.guillaumevdn.supremeshops.api.event.merchant.MerchantUpdatedShopEvent;
import com.guillaumevdn.supremeshops.data.MerchantBoard;
import com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.data.SSUser;
import com.guillaumevdn.supremeshops.gui.merchant.FilledMerchantGui;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.manageable.MerchantManagementPermission;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.MerchantShop;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.DestroyCause;
import com.guillaumevdn.supremeshops.util.MerchantsSorter;
import com.guillaumevdn.supremeshops.util.MerchantsSorter.SortCriteria;
import com.guillaumevdn.supremeshops.util.WagePeriod;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;

public abstract class Merchant extends DataElement implements Manageable<MerchantManagementPermission> {

	// base
	private MerchantType type;
	private String id;
	private UserInfo owner;
	private String displayName;
	private List<String> shopsIds = new ArrayList<String>();
	private CPConditions interactConditions = new CPConditions("interactConditions", null, false, -1, null, null);
	private boolean open = false;
	private boolean remote = false;
	private int tradesLimit = -1;
	private Map<UserInfo, Set<MerchantManagementPermission>> managers = new HashMap<UserInfo, Set<MerchantManagementPermission>>();
	private Map<UserInfo, Map<TradeObject, Double>> managersWages = new HashMap<UserInfo, Map<TradeObject, Double>>();
	private Map<UserInfo, Long> lastPaidManagersWages = new HashMap<UserInfo, Long>();
	private Map<UserInfo, Integer> lateManagersWages = new HashMap<UserInfo, Integer>();
	private Map<UserInfo, Integer> buyers = new HashMap<UserInfo, Integer>();

	protected Merchant(String id, MerchantType type) {
		this.id = id;
		this.type = type;
	}

	protected Merchant(String id, MerchantType type, UserInfo owner) {
		this.id = id;
		this.displayName = id;
		this.type = type;
		this.owner = owner;
	}

	// get
	public MerchantType getType() {
		return type;
	}

	public String getId() {
		return id;
	}

	public UserInfo getActualOwner() {
		return owner;
	}

	public boolean isActualOwnerAdmin() {
		return owner == null;
	}

	public UserInfo getCurrentOwner() {
		return getActualOwner();
	}

	public OfflinePlayer getCurrentOwnerPlayer() {
		UserInfo owner = getCurrentOwner();
		return owner == null ? null : owner.toOfflinePlayer();
	}

	public boolean isCurrentOwnerAdmin() {
		return isActualOwnerAdmin();
	}

	public String getDisplayName() {
		return displayName;
	}

	public List<String> getShopsIds() {
		return Collections.unmodifiableList(shopsIds);
	}

	public boolean hasShop(Shop shop) {
		return shop != null && shopsIds.contains(shop.getId().toLowerCase());
	}

	public List<Shop> getShops(Collection<? extends Class<?>> types, boolean mustBeOpen) {
		List<Shop> shops = new ArrayList<Shop>();
		for (String shopId : shopsIds) {
			Shop shop = SupremeShops.inst().getData().getShops().getElement(shopId);
			if (shop != null && ((types == null || types.isEmpty()) || Utils.hasOneSuper(shop.getClass(), types)) && (! mustBeOpen || shop.isOpen())) {
				shops.add(shop);
			}
		}
		return Collections.unmodifiableList(shops);
	}

	public CPConditions getInteractConditions() {
		return interactConditions;
	}

	public Collection<Condition> getInteractConditions(List<ConditionType> types) {
		List<Condition> result = new ArrayList<Condition>();
		for (Condition condition : interactConditions.getConditions().getElements().values()) {
			if ((types == null || types.isEmpty()) || types.contains(condition.getType())) {
				result.add(condition);
			}
		}
		return result;
	}

	public Map<UserInfo, Integer> getBuyers() {
		return Collections.unmodifiableMap(buyers);
	}

	public int getBuyerTrades(UserInfo user) {
		Integer trades = buyers.get(user);
		return trades == null ? 0 : trades;
	}

	public int getBuyersTrades() {
		int trades = 0;
		for (UserInfo user : buyers.keySet()) {
			trades += buyers.get(user);
		}
		return trades;
	}

	public boolean isOpen() {
		return open;
	}

	public boolean ensureOpenOrError(Player player) {
		if (!open) {
			SSLocale.MSG_SUPREMESHOPS_MERCHANTCLOSED.send(player);
		}
		return open;
	}

	public boolean isRemote() {
		return remote;
	}

	public int getTradesLimit() {
		return tradesLimit;
	}

	// manageable
	@Override
	public List<MerchantManagementPermission> getAllPermissions() {
		return Utils.asList(MerchantManagementPermission.values());
	}

	@Override
	public Map<UserInfo, Set<MerchantManagementPermission>> getManagers() {
		return Collections.unmodifiableMap(managers);
	}

	@Override
	public Map<UserInfo, Map<TradeObject, Double>> getManagersWages() {
		return Collections.unmodifiableMap(managersWages);
	}

	@Override
	public Map<UserInfo, Long> getLastPaidManagersWages() {
		return Collections.unmodifiableMap(lastPaidManagersWages);
	}

	@Override
	public Map<UserInfo, Integer> getLateManagersWages() {
		return Collections.unmodifiableMap(lateManagersWages);
	}

	// set
	protected void setActualOwner(UserInfo owner) {
		this.owner = owner;
	}

	protected void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	protected void setShopsIds(List<String> shopsIds) {
		this.shopsIds = shopsIds;
	}

	protected void clearShopsIds() {
		shopsIds.clear();
	}

	protected void setInteractConditions(CPConditions interactConditions) {
		this.interactConditions = interactConditions;
	}

	protected void setOpen(boolean open) {
		this.open = open;
	}

	protected void setRemote(boolean remote) {
		this.remote = remote;
	}

	protected void setTradesLimit(int tradesLimit) {
		this.tradesLimit = tradesLimit;
	}

	protected void setManagers(Map<UserInfo, Set<MerchantManagementPermission>> managers) {
		this.managers = managers;
	}

	protected void setManagersWages(Map<UserInfo, Map<TradeObject, Double>> managersWages) {
		this.managersWages = managersWages;
	}

	protected void setLastPaidManagersWages(Map<UserInfo, Long> lastPaidManagersWages) {
		this.lastPaidManagersWages = lastPaidManagersWages;
	}

	protected void setLateManagersWages(Map<UserInfo, Integer> lateManagersWages) {
		this.lateManagersWages = lateManagersWages;
	}

	protected void setNotManager(UserInfo manager) {
		managers.remove(manager);
		managersWages.remove(manager);
		lastPaidManagersWages.remove(manager);
		lateManagersWages.remove(manager);
	}

	protected void clearAllManagers() {
		managers.clear();
		managersWages.clear();
		lastPaidManagersWages.clear();
		lateManagersWages.clear();
	}

	protected void setBuyers(Map<UserInfo, Integer> buyers) {
		this.buyers = buyers;
	}

	// methods
	@Override
	public Set<MerchantManagementPermission> getManagementPermissions(OfflinePlayer player) {
		return getManagementPermissions(new UserInfo(player));
	}

	@Override
	public Set<MerchantManagementPermission> getManagementPermissions(UserInfo player) {
		if (managers.containsKey(player)) {// is manager
			return Collections.unmodifiableSet(managers.get(player));
		} else if ((getCurrentOwner() == null && SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player.toOfflinePlayer())) || player.equals(getCurrentOwner())) {// is owner or admin
			return Collections.unmodifiableSet(Utils.asSet(MerchantManagementPermission.values()));
		}
		return Collections.unmodifiableSet(new HashSet<MerchantManagementPermission>());// no permissions
	}

	@Override
	public boolean hasManagementPermission(Player player, MerchantManagementPermission permission) {
		return getManagementPermissions(player).contains(permission);
	}

	@Override
	public Long getLastPaidManagerWage(UserInfo manager) {
		Long last = lastPaidManagersWages.get(manager);
		return last == null ? 0L : last;
	}

	@Override
	public Integer getLateManagerWages(UserInfo manager) {
		Integer late = lateManagersWages.get(manager);
		return late == null ? 0 : late;
	}

	@Override
	public Map<TradeObject, Double> getManagerWage(UserInfo manager) {
		Map<TradeObject, Double> wage = managersWages.get(manager);
		return Collections.unmodifiableMap(wage == null ? new HashMap<TradeObject, Double>() : wage);
	}

	@Override
	public void changeModifyManagerWageStock(UserInfo manager, TradeObject object, double delta) {
		Map<TradeObject, Double> wage = managersWages.get(manager);
		wage.put(object, (wage.containsKey(object) ? wage.get(object) : 0) + delta);
		pushAsync();
	}

	@Override
	public double getTotalObjectStockInManagersWages(List<ObjectType> types) {
		// wages
		double amount = 0d;
		for (Map<TradeObject, Double> wage : managersWages.values()) {
			for (TradeObject object : wage.keySet()) {
				if ((types == null || types.isEmpty()) || types.contains(object.getType())) {
					amount += wage.get(object).intValue();
				}
			}
		}
		// done
		return amount;
	}

	public boolean areInteractConditionsValid(Player player, boolean errorMessage) {
		return interactConditions.isValid(player, this, errorMessage);
	}

	/**
	 * @return a list of shops to destroy if the "push" parameter is false, or null
	 */
	public List<Shop> destroy(DestroyCause cause, boolean push) {
		// close GUIs
		SupremeShops.inst().getGeneralManager().closeRelatedGUIs(this, false);
		// destroy merchant shops
		List<Shop> shopsToRemove = getShops(Utils.asList(MerchantShop.class), false);
		if (push) {
			for (Shop shop : shopsToRemove) {
				try {
					shop.destroy(DestroyCause.MERCHANT_DESTROYED, true);
				} catch (Throwable exception) {
					exception.printStackTrace();
					SupremeShops.inst().error("Couldn't destroy shop " + shop.getId() + " belonging to merchant " + getId());
				}
			}
		}
		// delete merchant
		SupremeShops.inst().getData().getMerchants().delete(this);
		// event
		Bukkit.getPluginManager().callEvent(new MerchantDestroyedEvent(this, cause));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, "Destroyed merchant (" + cause.name() + ")");
		// return
		return push ? null : shopsToRemove;
	}

	public Object[] getMessageReplacers(boolean withDetails, boolean fromModifiersCalculation, Player parser) {
		List<Object> replacers = buildMessageReplacers(withDetails, fromModifiersCalculation, parser);
		return replacers.toArray(new Object[replacers.size()]);
	}

	protected List<Object> buildMessageReplacers(boolean withDetails, boolean fromModifiersCalculation, Player parser) {
		List<Object> replacers = new ArrayList<Object>();
		// type
		replacers.add("{type}");
		replacers.add(type.getName());
		// owner
		replacers.add("{owner}");
		replacers.add(owner != null ? owner.toOfflinePlayer().getName() : "[admin]");
		// open
		replacers.add("{name}");
		replacers.add(displayName);
		// open
		replacers.add("{merchant_open}");
		replacers.add(open ? SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTOPEN.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTCLOSED.getLine());
		// trade count and trades limit
		int tradesCount = parser == null ? 0 : SSUser.get(parser).getMerchantTradeCount(this);
		replacers.add("{trades_count}");
		replacers.add(tradesCount);
		replacers.add("{trades_limit}");
		replacers.add(tradesLimit > 0 ? "/" + tradesLimit : "");
		replacers.add("{trades_count_plural}");
		replacers.add(Utils.getPlural(tradesCount));
		// stats
		if (withDetails) {
			// successful trades
			replacers.add("{successful_trades}");
			replacers.add(getBuyersTrades());
			// unique buyers
			replacers.add("{unique_buyers}");
			replacers.add(buyers.size());
		}
		// rankings
		if (withDetails) {
			// merchant ranking (server)
			List<Merchant> sortedServerMerchants = new MerchantsSorter(SupremeShops.inst().getData().getMerchants().getElements(owner, null, false, ElementRemotePolicy.MIGHT_BE), SortCriteria.BY_RANKING_SERVER).getSortedList();
			replacers.add("{merchant_server_ranking}");
			replacers.add(sortedServerMerchants.indexOf(this) + 1);
			replacers.add("{server_merchants}");
			replacers.add(sortedServerMerchants.size());
			// merchant ranking (seller)
			List<Merchant> sortedSellerMerchants = new MerchantsSorter(SupremeShops.inst().getData().getMerchants().getElements(owner, null, false, ElementRemotePolicy.MIGHT_BE), SortCriteria.BY_RANKING_SELLER).getSortedList();
			replacers.add("{merchant_owner_ranking}");
			replacers.add(sortedSellerMerchants.indexOf(this) + 1);
			replacers.add("{owner_merchants}");
			replacers.add(sortedSellerMerchants.size());
			// seller ranking
			Map<UserInfo, List<Merchant>> merchantsByOwner = SupremeShops.inst().getData().getMerchants().getAllByOwners(true);
			final Map<UserInfo, Integer> ownerSuccessfulTrades = new HashMap<UserInfo, Integer>();
			for (UserInfo own : merchantsByOwner.keySet()) {
				int trades = 0;
				for (Merchant merch : merchantsByOwner.get(own)) {
					trades += merch.getBuyersTrades();
				}
				ownerSuccessfulTrades.put(own, trades);
			}
			List<UserInfo> allOwners = Utils.asList(merchantsByOwner.keySet());
			Utils.sortList(allOwners, new Comparator<UserInfo>() {
				@Override
				public int compare(UserInfo o1, UserInfo o2) {
					return Integer.compare(ownerSuccessfulTrades.get(o1), ownerSuccessfulTrades.get(o2));
				}
			});
			replacers.add("{owner_ranking}");
			replacers.add(allOwners.indexOf(owner) + 1);
			replacers.add("{server_owners}");
			replacers.add(allOwners.size());
		}
		// return
		return replacers;
	}

	// methods : managers
	@Override
	public void addManager(UserInfo manager) {
		if (!managers.containsKey(manager)) {
			managers.put(manager, new HashSet<MerchantManagementPermission>());
			Bukkit.getPluginManager().callEvent(new MerchantUpdatedManagerEvent(this, manager, null, Utils.emptyList(MerchantManagementPermission.class), MerchantUpdatedManagerEvent.Operation.ADD_MANAGER));
			pushAsync();
		}
	}

	@Override
	public void addToManagerWage(UserInfo manager, TradeObject wageObject, Double stock) {
		Map<TradeObject, Double> wage = managersWages.get(manager);
		if (wage == null) managersWages.put(manager, wage = new HashMap<TradeObject, Double>());
		wage.put(wageObject, stock);
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedManagerEvent(this, manager, wageObject, Utils.emptyList(MerchantManagementPermission.class),MerchantUpdatedManagerEvent.Operation.ADD_WAGE));
		pushAsync();
	}

	@Override
	public void removeManager(UserInfo manager) {
		if (managers.remove(manager) != null) {
			pushAsync();
			// close inventory ; knowing that he's fired, that filthy manager might already be committing an obnoxious FELONY ! ;-;
			Player player = manager.toPlayer();
			if (player != null) player.closeInventory();
			// event
			Bukkit.getPluginManager().callEvent(new MerchantUpdatedManagerEvent(this, manager, null, Utils.emptyList(MerchantManagementPermission.class),MerchantUpdatedManagerEvent.Operation.REMOVE_MANAGER));
		}
	}

	@Override
	public void removeManagerWage(UserInfo manager, TradeObject wageObject) {
		Map<TradeObject, Double> wage = managersWages.get(manager);
		if (wage != null && wage.remove(wageObject) != null) {
			Bukkit.getPluginManager().callEvent(new MerchantUpdatedManagerEvent(this, manager, wageObject, Utils.emptyList(MerchantManagementPermission.class),MerchantUpdatedManagerEvent.Operation.REMOVE_WAGE));
			pushAsync();
		}
	}

	@Override
	public void changeManagementPermission(Player manager, MerchantManagementPermission permission, boolean has) {
		changeManagementPermission(new UserInfo(manager), permission, has);
	}

	@Override
	public void changeManagementPermission(UserInfo manager, MerchantManagementPermission permission, boolean has) {
		// get permissions
		Set<MerchantManagementPermission> permissions = managers.get(manager);
		if (permissions == null) managers.put(manager, permissions = new HashSet<MerchantManagementPermission>());
		// set permission
		if (has) {
			if (permissions.add(permission)) {
				Bukkit.getPluginManager().callEvent(new MerchantUpdatedManagerEvent(this, manager, null, Utils.asList(permission), MerchantUpdatedManagerEvent.Operation.ADD_PERMISSION));
				pushAsync();
			}
		} else {
			if (permissions.remove(permission)) {
				Bukkit.getPluginManager().callEvent(new MerchantUpdatedManagerEvent(this, manager, null, Utils.asList(permission), MerchantUpdatedManagerEvent.Operation.REMOVE_PERMISSION));
				pushAsync();
			}
		}
	}

	@Override
	public void changeManagementPermissions(Player player, Set<MerchantManagementPermission> permissions) {
		changeManagementPermissions(new UserInfo(player), permissions);
	}

	@Override
	public void changeManagementPermissions(UserInfo manager, Set<MerchantManagementPermission> permissions) {
		managers.put(manager, permissions = new HashSet<MerchantManagementPermission>());
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedManagerEvent(this, manager, null, Utils.asList(permissions), MerchantUpdatedManagerEvent.Operation.UPDATE_PERMISSIONS));
		pushAsync();
	}

	@Override
	public void changeLateManagerWages(UserInfo manager, int late, boolean push) {
		lateManagersWages.put(manager, late);
		if (push) pushAsync();
	}

	public static final long SECOND_MILLIS = 1000L;
	public static final long MINUTE_MILLIS = 60L * SECOND_MILLIS;
	public static final long HOUR_MILLIS = 60L * MINUTE_MILLIS;
	public static final long DAY_MILLIS = 24L * HOUR_MILLIS;
	public static final long WEEK_MILLIS = 7L * DAY_MILLIS;

	@Override
	public int checkToPayWages(boolean push) {
		// check managers
		int paid = 0;
		for (UserInfo manager : managers.keySet()) {
			if (checkToPayWage(manager, false)) {
				++paid;
			}
		}
		// push
		if (paid > 0 && push) {
			pushAsync();
		}
		// return
		return paid;
	}

	@Override
	public boolean checkToPayWage(UserInfo manager, boolean push) {
		long lastPaid = getLastPaidManagerWage(manager);
		// daily
		if (WagePeriod.DAILY.equals(SupremeShops.inst().getModuleManager().getWagePeriod())) {
			// get current period start
			Calendar calendar = GCoreLegacy.inst().getCalendarInstance();
			long hour = calendar.get(Calendar.HOUR_OF_DAY);
			long minute = calendar.get(Calendar.MINUTE);
			long second = calendar.get(Calendar.SECOND);
			long millis = calendar.get(Calendar.MILLISECOND);
			long start = System.currentTimeMillis() - hour * HOUR_MILLIS - minute * MINUTE_MILLIS - second * SECOND_MILLIS - millis;
			// if it wasn't today, pay
			if (lastPaid < start) {
				return payWage(manager, push);
			}
		}
		// weekly
		else if (WagePeriod.WEEKLY.equals(SupremeShops.inst().getModuleManager().getWagePeriod())) {
			// get current period start
			Calendar calendar = GCoreLegacy.inst().getCalendarInstance();
			long day = WeekDay.getCurrent().ordinal();
			long hour = calendar.get(Calendar.HOUR_OF_DAY);
			long minute = calendar.get(Calendar.MINUTE);
			long second = calendar.get(Calendar.SECOND);
			long millis = calendar.get(Calendar.MILLISECOND);
			long start = System.currentTimeMillis() - day * DAY_MILLIS - hour * HOUR_MILLIS - minute * MINUTE_MILLIS - second * SECOND_MILLIS - millis;
			// if it wasn't this week, pay
			if (lastPaid < start) {
				return payWage(manager, push);
			}
		}
		// monthly
		if (WagePeriod.MONTHLY.equals(SupremeShops.inst().getModuleManager().getWagePeriod())) {
			// get current period start
			Calendar calendar = GCoreLegacy.inst().getCalendarInstance();
			long day = calendar.get(Calendar.DAY_OF_MONTH);
			long hour = calendar.get(Calendar.HOUR_OF_DAY);
			long minute = calendar.get(Calendar.MINUTE);
			long second = calendar.get(Calendar.SECOND);
			long millis = calendar.get(Calendar.MILLISECOND);
			long start = System.currentTimeMillis() - day * DAY_MILLIS - hour * HOUR_MILLIS - minute * MINUTE_MILLIS - second * SECOND_MILLIS - millis;
			// if it wasn't this month, pay
			if (lastPaid < start) {
				return payWage(manager, push);
			}
		}
		// nothing to pay
		return false;
	}

	@Override
	public int checkToPayLateWages(boolean push) {
		// check managers
		int paid = 0;
		for (UserInfo manager : managers.keySet()) {
			if (checkToPayLateWages(manager, false)) {
				++paid;
			}
		}
		// push
		if (paid > 0 && push) {
			pushAsync();
		}
		// return
		return paid;
	}

	@Override
	public boolean checkToPayLateWages(UserInfo manager, boolean push) {
		// offline
		Player managerPlayer = manager.toPlayer();
		if (managerPlayer == null) {
			return false;
		}
		// see how late we are
		int late = getLateManagerWages(manager);
		if (late == 0) {
			return false;
		}
		// no wage
		Map<TradeObject, Double> wage = getManagerWage(manager);
		if (wage.isEmpty()) {
			return false;
		}
		// see how many complete wages we can pay
		int canPay = late;
		for (TradeObject object : wage.keySet()) {
			int can = object.getTradesForStock(wage.get(object), object.getCustomAmount(null));
			if (can < canPay) canPay = can;
		}
		// can at least pay one
		if (canPay > 0) {
			// pay
			for (TradeObject object : wage.keySet()) {
				object.give(managerPlayer, canPay);
			}
			late -= canPay;
			lateManagersWages.put(manager, late);
			SSLocale.MSG_SUPREMESHOPS_MERCHANTLATEWAGERECEIVE.send(managerPlayer, "{player}", managerPlayer.getName(), "{name}", getDisplayName(), "{count}", late, "{plural}", Utils.getPlural(late));
			// event
			Bukkit.getPluginManager().callEvent(new MerchantPaidManagerWageEvent(this, manager, canPay));
			// push
			if (push) pushAsync();
		}
		return true;
	}

	@Override
	public boolean payWage(UserInfo manager, boolean push) {
		// offline
		Player managerPlayer = manager.toPlayer();
		if (managerPlayer == null) return false;
		// no wage
		Map<TradeObject, Double> wage = getManagerWage(manager);
		if (wage.isEmpty()) {
			return false;
		}
		// can't pay the complete wage
		for (TradeObject object : wage.keySet()) {
			if (object.getTradesForStock(wage.get(object), object.getCustomAmount(null)) <= 0) {
				int late = getLateManagerWages(manager) + 1;
				lateManagersWages.put(manager, late);
				// notify manager and owner
				SSLocale.MSG_SUPREMESHOPS_MERCHANTOWNERCANTPAYMANAGEMENT.send(managerPlayer, "{player}", managerPlayer.getName(), "{name}", getDisplayName(), "{count}", late, "{plural}", Utils.getPlural(late));
				Player ownerPlayer = getCurrentOwner().toPlayer();
				if (ownerPlayer != null) SSLocale.MSG_SUPREMESHOPS_MERCHANTCANTPAYMANAGER.send(ownerPlayer, "{player}", ownerPlayer.getName(), "{name}", getDisplayName(), "{count}", late, "{plural}", Utils.getPlural(late));
				return false;
			}
		}
		// pay
		for (TradeObject object : wage.keySet()) {
			object.give(managerPlayer, object.getCustomAmount(null));
		}
		SSLocale.MSG_SUPREMESHOPS_WAGERECEIVE.send(managerPlayer, "{player}", managerPlayer.getName(), "{name}", getDisplayName());
		// event
		Bukkit.getPluginManager().callEvent(new MerchantPaidManagerWageEvent(this, manager, 0));
		// push
		if (push) pushAsync();
		return true;
	}

	// methods : change
	public void changeActualOwner(UserInfo owner) {
		// set owner
		setNotManager(owner);
		setActualOwner(owner);
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_ACTUAL_OWNER));
		// save
		pushAsync();
	}

	public void changeDisplayName(String displayName) {
		// set display name
		setDisplayName(displayName);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_DISPLAY_NAME));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, "Set display name to " + displayName);
	}

	public void changeTradesLimit(int limit) {
		// set limit
		setTradesLimit(limit);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_TRADES_LIMIT));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, "Set trades limit to " + limit);
	}

	public void addBuyerTrades(UserInfo user, int trades) {
		// set trades
		buyers.put(user, getBuyerTrades(user) + trades);
		pushAsync();
	}

	// methods : shops
	public void removeInteractCondition(Condition condition) {
		if (interactConditions.getConditions().removeElement(condition.getId()) != null) {
			Bukkit.getPluginManager().callEvent(new MerchantUpdatedInteractConditionsEvent(this, condition, MerchantUpdatedInteractConditionsEvent.Operation.REMOVE_CONDITION));
			pushAsync();
		}
	}

	public boolean addShop(Shop shop) {
		return addShop(shop, true);
	}

	public boolean addShop(Shop shop, boolean push) {
		String shopId = shop.getId().toLowerCase();
		if (!shopsIds.contains(shopId)) {
			shopsIds.add(shopId);
			Bukkit.getPluginManager().callEvent(new MerchantUpdatedShopEvent(this,shop, MerchantUpdatedShopEvent.Operation.ADD_SHOP));
			if (push) pushAsync();
			SupremeShops.inst().pluginLog(null, this, null, null, null, "Added shop " + shop.getId());
			return true;
		}
		return false;
	}

	public boolean removeShop(Shop shop, boolean push) {
		String shopId = shop.getId().toLowerCase();
		if (shopsIds.remove(shopId)) {
			Bukkit.getPluginManager().callEvent(new MerchantUpdatedShopEvent(this,shop, MerchantUpdatedShopEvent.Operation.REMOVE_SHOP));
			if (push) pushAsync();
			SupremeShops.inst().pluginLog(null, this, null, null, null, "Removed shop " + shop.getId());
			return true;
		}
		return false;
	}

	public void addInteractCondition(Condition condition) {
		if (interactConditions.getConditions().getElement(condition.getId()) == null) {
			interactConditions.getConditions().addElement(condition);
			Bukkit.getPluginManager().callEvent(new MerchantUpdatedInteractConditionsEvent(this, condition, MerchantUpdatedInteractConditionsEvent.Operation.ADD_CONDITION));
			pushAsync();
		}
	}

	public void changeOpen(boolean open) {
		// set open
		setOpen(open);
		pushAsync();
		// close GUIs
		if (!open) {
			SupremeShops.inst().getGeneralManager().closeRelatedGUIs(this, true);
		}
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.TOGGLE_OPEN));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, (open ? "Opened" : "Closed"));
	}

	public void changeRemote(boolean remote) {
		// set remote
		setRemote(remote);
		pushAsync();
		// close GUIs
		if (!remote) {
			SupremeShops.inst().getGeneralManager().closeRelatedGUIs(this, true);
		}
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.TOGGLE_REMOTE));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, (remote ? "Enabled remote" : "Disabled remote"));
	}

	// methods : GUI
	public FilledMerchantGui buildGui(Player player, GUI fromGUI, int fromGUIPageIndex) {
		return new FilledMerchantGui(this, player, displayName, fromGUI, fromGUIPageIndex);
	}

	// fake
	public abstract Merchant asFake();

	// overriden
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Merchant other = (Merchant) obj;
		if (id == null) {
			return other.id == null;
		} else return id.equals(other.id);
	}

	// board and id
	@Override
	protected MerchantBoard getBoard() {
		return SupremeShops.inst().getData().getMerchants();
	}

	@Override
	public String getDataId() {
		return getId();
	}

	public abstract Class<?> getJsonDataClass();
	public abstract void readJsonData(Object jsonData);
	public abstract Object writeJsonData();

	// data
	private transient boolean loadError = false;

	public boolean hasLoadError() {
		return loadError;
	}

	@Override
	public final void jsonPull() {
		// not saved
		if (getJsonDataClass() == null) return;
		// pull
		loadError = true;
		File file = getBoard().getJsonFile(this);
		if (file.exists()) {
			Object data = Utils.loadFromGson(getJsonDataClass(), file, true, SupremeShops.GSON);
			if (data != null) {
				readJsonData(data);
				loadError = false;
			}
		}
	}

	@Override
	protected final void jsonPush() {
		// not saved
		if (getJsonDataClass() == null) return;
		// push
		File file = getBoard().getJsonFile(this);
		Utils.saveToGson(writeJsonData(), file, SupremeShops.GSON);
	}

	@Override
	protected final void jsonDelete() {
		// not saved
		if (getJsonDataClass() == null) return;
		// delete
		File file = getBoard().getJsonFile(this);
		if (file.exists()) {
			file.delete();
		}
	}

	// MYSQL
	@Override
	public final void mysqlPull(ResultSet set) throws SQLException {
		// not saved
		if (getJsonDataClass() == null) return;
		// pull
		loadError = true;
		Object data = SupremeShops.GSON.fromJson(set.getString("data"), getJsonDataClass());
		if (data != null) {
			readJsonData(data);
			loadError = false;
		}
	}

	protected final Query getMySQLPullQuery() {
		// not saved
		if (getJsonDataClass() == null) return new Query();
		// build query
		return new Query("SELECT * FROM `" + getBoard().getMySQLTable() + "` WHERE `id`=?;", getDataId());
	}

	protected final Query getMySQLPushQuery() {
		// not saved
		if (getJsonDataClass() == null) return new Query();
		// build query
		return new Query("REPLACE INTO `" + getBoard().getMySQLTable() + "`(`id`,`type`,`owner`,`data`) VALUES(?,?,?,?);",
				getDataId(), type.name(), owner == null ? "null" : owner.toString(), SupremeShops.GSON.toJson(writeJsonData()));
	}

	protected final Query getMySQLDeleteQuery() {
		// not saved
		if (getJsonDataClass() == null) return new Query();
		// build query
		return new Query("DELETE FROM `" + getBoard().getMySQLTable() + "` WHERE `id`=?;", getDataId());
	}

}
