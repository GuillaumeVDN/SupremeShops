package com.guillaumevdn.supremeshops.module.merchant.fake;

import java.util.List;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.data.DataManager.Callback;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.data.MerchantBoard;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.merchant.RentableBlockMerchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.DestroyCause;

public class FakeRentableBlockMerchant extends RentableBlockMerchant implements FakeMerchant {

	// base
	public FakeRentableBlockMerchant(RentableBlockMerchant merchant) {
		super(merchant.getId(), merchant.getBlock(), merchant.getSign());
		setDisplayName(merchant.getDisplayName());
		setShopsIds(merchant.getShopsIds());
		setInteractConditions(merchant.getInteractConditions());
		setOpen(merchant.isOpen());
		setRemote(merchant.isRemote());
		setParticlePatternId(merchant.getParticlePatternId());
		setBuyers(merchant.getBuyers());
		setManagers(merchant.getManagers());
		setManagersWages(merchant.getManagersWages());
		setLastPaidManagersWages(merchant.getLastPaidManagersWages());
		setLateManagersWages(merchant.getLateManagersWages());
		setCurrentOwner(merchant.getCurrentOwner());
		setPastPaidRents(merchant.getPastPaidRents());
		setPaidRents(merchant.getPaidRents());
		setLastCheckRent(merchant.getLastCheckRent());
		setAwaitingForceUnrent(merchant.isAwaitingForceUnrent());
		setRentable(merchant.isRentable());
		setRentPrice(merchant.getRentPrice());
		setRentPeriod(merchant.getRentPeriod());
		setRentPeriodStreakLimit(merchant.getRentPeriodStreakLimit());
		setRentPeriodStreakLimitDelay(merchant.getRentPeriodStreakLimitDelay());
		setRentConditions(merchant.getRentConditions());
		setUnrentedParticlePatternId(merchant.getUnrentedParticlePatternId());
	}

	// fake merchant
	@Override
	public void withShop(Shop shop) {
		List<String> shopsIds = Utils.asList(getShopsIds());
		shopsIds.add(shop.getId());
		setShopsIds(shopsIds);
	}

	@Override
	public void withInteractCondition(Condition condition) {
		getInteractConditions().getConditions().addElement(condition);
	}

	// override
	@Override
	public boolean addShop(Shop shop) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeDisplayName(String displayName) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeOpen(boolean open) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeActualOwner(UserInfo owner) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeParticlePattern(String particlePatternId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeRemote(boolean remote) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteAsync() {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public List<Shop> destroy(DestroyCause cause, boolean push) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected MerchantBoard getBoard() {
		throw new UnsupportedOperationException();
	}

	@Override
	public String getDataId() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void pullAsync() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void pullAsync(Callback callback) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void push(boolean async) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean removeShop(Shop shop, boolean push) {
		throw new UnsupportedOperationException();
	}

}
