package com.guillaumevdn.supremeshops.module.merchant.fake;

import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public interface FakeMerchant {

	void withShop(Shop shop);
	void withInteractCondition(Condition condition);

}
