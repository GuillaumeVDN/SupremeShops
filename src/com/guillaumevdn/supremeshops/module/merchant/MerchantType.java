package com.guillaumevdn.supremeshops.module.merchant;

import com.guillaumevdn.supremeshops.SSLocaleMisc;

public enum MerchantType {

	// values
	BLOCK(BlockMerchant.class, "block", SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTTYPE_BLOCK.getLine()),
	NPC(NpcMerchant.class, "npc", SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTTYPE_NPC.getLine()),
	RENTABLE_BLOCK(RentableBlockMerchant.class, "rentable_block", SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTTYPE_RENTABLEBLOCK.getLine()),
	RENTABLE_NPC(RentableNpcMerchant.class, "rentable_npc", SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTTYPE_RENTABLENPC.getLine()),
	RENTABLE_SIGN(RentableSignMerchant.class, "rentable_sign", SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTTYPE_RENTABLESIGN.getLine()),
	SIGN(SignMerchant.class, "sign", SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTTYPE_SIGN.getLine())
	;

	// base
	private Class<? extends Merchant> merchantClass;
	private String diskDataFolder;
	private String name;

	private MerchantType(Class<? extends Merchant> merchantClass, String diskDataFolder, String name) {
		this.merchantClass = merchantClass;
		this.diskDataFolder = diskDataFolder;
		this.name = name;
	}

	// get
	public Class<? extends Merchant> getMerchantClass() {
		return merchantClass;
	}

	public String getDiskDataFolder() {
		if (diskDataFolder == null) throw new UnsupportedOperationException("merchant type " + name() + " can't be saved on disk");
		return diskDataFolder;
	}

	public boolean mustSaveData() {
		return diskDataFolder != null;
	}

	public String getName() {
		return name;
	}

}
