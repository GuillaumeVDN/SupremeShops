package com.guillaumevdn.supremeshops.module.merchant;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.npc.Npc;
import com.guillaumevdn.gcorelegacy.lib.npc.NpcStatus;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.merchant.MerchantUpdatedEvent;
import com.guillaumevdn.supremeshops.module.display.particles.SingleParticlesDisplayable;
import com.guillaumevdn.supremeshops.module.manageable.MerchantManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.fake.FakeNpcMerchant;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.particlepatternassignable.ParticlePatternAssignable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.DestroyCause;
import com.guillaumevdn.supremeshops.util.Locatable;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePatternAvailability;

public class NpcMerchant extends Merchant implements Locatable, SingleParticlesDisplayable, ParticlePatternAssignable {

	// static base
	public static final int NPC_ID_BASE = 71352;

	// base
	private Location location;
	private String particlePatternId = null;
	private String skinId = null;
	private String equipmentId = null;
	private List<NpcStatus> status = new ArrayList<NpcStatus>();

	private NpcMerchant(String id) {
		super(id, MerchantType.NPC);
	}

	protected NpcMerchant(String id, MerchantType type) {
		super(id, type);
	}

	public NpcMerchant(String id, UserInfo owner, Location location) {
		this(id, MerchantType.NPC, owner, location);
	}

	protected NpcMerchant(String id, MerchantType type, UserInfo owner, Location location) {
		super(id, type, owner);
		this.location = location;
	}

	// get
	@Override
	public World getWorld() {
		return location.getWorld();
	}
	
	@Override
	public BlockCoords getBlock() {
		return new BlockCoords(location);
	}

	@Override
	public Location getLocation() {
		return location;
	}

	public String getSkinId() {
		return skinId;
	}

	public String getEquipmentId() {
		return equipmentId;
	}

	public List<NpcStatus> getStatus() {
		return Collections.unmodifiableList(status);
	}

	public ItemData[] getNpcItems() {
		ItemData[] items = new ItemData[6];
		NpcMerchantEquipment equipment = SupremeShops.inst().getModuleManager().getMerchantEquipment(equipmentId);
		if (equipment != null) {
			items[0] = equipment.getMainHand();
			items[1] = equipment.getOffHand();
			items[2] = equipment.getBoots();
			items[3] = equipment.getLeggings();
			items[4] = equipment.getChestplate();
			items[5] = equipment.getHelmet();
		}
		return items;
	}

	// particle pattern
	@Override
	public String getParticlePatternId() {
		return particlePatternId;
	}

	@Override
	public ParticlePattern getParticlePattern() {
		return particlePatternId == null ? null : SupremeShops.inst().getModuleManager().getParticlePattern(particlePatternId);
	}

	@Override
	public List<ParticlePattern> getAvailableParticlePatterns(Player player) {
		List<ParticlePattern> patterns = new ArrayList<ParticlePattern>();
		for (ParticlePattern pattern : SupremeShops.inst().getModuleManager().getParticlePatterns().values()) {
			if (pattern.getAvailability().equals(ParticlePatternAvailability.ENTITY) && (isCurrentOwnerAdmin() || (pattern.getPermission() == null || pattern.getPermission().has(getCurrentOwner().toOfflinePlayer())))) {
				patterns.add(pattern);
			}
		}
		return patterns;
	}

	// particles displayable
	@Override
	public boolean mustDisplayParticles() {
		return isOpen();
	}

	@Override
	public ParticlePattern getCurrentParticlePattern() {
		return getParticlePattern();
	}

	@Override
	public boolean areCurrentParticlesConditionsValid(Player player) {
		return areInteractConditionsValid(player, false);
	}

	@Override
	public Location getParticlesBase() {
		return getLocation();
	}

	// set
	protected void setLocation(Location location) {
		this.location = location;
	}

	protected void setParticlePatternId(String particlePatternId) {
		this.particlePatternId = particlePatternId;
	}

	protected void setSkinId(String skinId) {
		this.skinId = skinId;
	}

	protected void setEquipmentId(String equipmentId) {
		this.equipmentId = equipmentId;
	}

	protected void setStatus(List<NpcStatus> status) {
		this.status = status;
	}

	// methods
	public Npc getNpc(Player player) {
		return GCoreLegacy.inst().getNpcManager().getNpc(player, SupremeShops.inst().getGeneralManager().getMerchantNpcId(this));
	}
	
	@Override
	public List<Shop> destroy(DestroyCause cause, boolean push) {
		// remove NPCs
		SupremeShops.inst().getGeneralManager().removeMerchantNpc(this);
		// destroy
		return super.destroy(cause, push);
	}

	@Override
	protected List<Object> buildMessageReplacers(boolean withDetails, boolean fromModifiersCalculation, Player parser) {
		List<Object> replacers = super.buildMessageReplacers(withDetails, fromModifiersCalculation, parser);
		// location
		replacers.add("{location}");
		replacers.add(location.toString());
		// return
		return replacers;
	}

	// methods : change
	@Override
	public void changeDisplayName(String displayName) {
		// change name
		super.changeDisplayName(displayName);
		// respawn NPC
		SupremeShops.inst().getGeneralManager().removeMerchantNpc(this);
		SupremeShops.inst().getGeneralManager().spawnMerchantNpcs(this);
	}

	// methods : shops
	@Override
	public void changeParticlePattern(String particlePatternId) {
		// set id
		setParticlePatternId(particlePatternId);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_PARTICLE_PATTERN));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, "Set particle pattern to" + particlePatternId);
	}

	public void changeSkin(String id) {
		// set id
		setSkinId(id);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_SKIN));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, "Set skin to " + id);
		// update display
		SupremeShops.inst().getGeneralManager().removeMerchantNpc(this);
		SupremeShops.inst().getGeneralManager().spawnMerchantNpcs(this);
	}

	public void changeEquipment(String id) {
		// set id
		setEquipmentId(id);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_EQUIPMENT));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, "Set equipment to " + id);
		// update display
		SupremeShops.inst().getGeneralManager().removeMerchantNpc(this);
		SupremeShops.inst().getGeneralManager().spawnMerchantNpcs(this);
	}

	public void changeStatus(List<NpcStatus> status) {
		// set
		setStatus(status);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new MerchantUpdatedEvent(this, MerchantUpdatedEvent.Operation.SET_STATUS));
		// log
		SupremeShops.inst().pluginLog(null, this, null, null, null, "Set status to " + Utils.asNiceString(status, true));
		// update display
		SupremeShops.inst().getGeneralManager().removeMerchantNpc(this);
		SupremeShops.inst().getGeneralManager().spawnMerchantNpcs(this);
	}

	// fake
	@Override
	public NpcMerchant asFake() {
		return new FakeNpcMerchant(this);
	}

	// data class
	public static final class NpcMerchantJsonData {

		private final UserInfo owner;
		private final String displayName;
		private final List<String> shopsIds;
		private final CPConditions interactConditions;
		private final boolean open;
		private final boolean remote;
		private final int tradesLimit;
		private final Map<UserInfo, Set<MerchantManagementPermission>> managers;
		private final Map<UserInfo, Map<TradeObject, Double>> managersWages;
		private final Map<UserInfo, Long> lastPaidManagersWages;
		private final Map<UserInfo, Integer> lateManagersWages;
		private final Map<UserInfo, Integer> buyers;
		private final Location location;
		private final String particlePatternId;
		private final String equipmentId;
		private final String skinId;
		private final List<NpcStatus> status;

		private NpcMerchantJsonData(NpcMerchant merchant) {
			this.owner = merchant.getActualOwner();
			this.displayName = merchant.getDisplayName();
			this.shopsIds = merchant.getShopsIds();
			this.interactConditions = merchant.getInteractConditions();
			this.open = merchant.isOpen();
			this.remote = merchant.isRemote();
			this.tradesLimit = merchant.getTradesLimit();
			this.managers = merchant.getManagers();
			this.managersWages = merchant.getManagersWages();
			this.lastPaidManagersWages = merchant.getLastPaidManagersWages();
			this.lateManagersWages = merchant.getLateManagersWages();
			this.buyers = merchant.getBuyers();
			this.location = merchant.location;
			this.particlePatternId = merchant.particlePatternId;
			this.skinId = merchant.skinId;
			this.equipmentId = merchant.equipmentId;
			this.status = merchant.status;
		}

	}

	public Class<?> getJsonDataClass() {
		return NpcMerchantJsonData.class;
	}

	public void readJsonData(Object jsonData) {
		// invalid class
		if (!Utils.instanceOf(jsonData, getJsonDataClass())) {
			throw new IllegalArgumentException("data class is " + jsonData.getClass() + ", expected is " + getJsonDataClass().getName());
		}
		// read data
		NpcMerchantJsonData data = (NpcMerchantJsonData) jsonData;
		setActualOwner(data.owner);
		setDisplayName(data.displayName);
		setShopsIds(data.shopsIds);
		setInteractConditions(data.interactConditions);
		setOpen(data.open);
		setRemote(data.remote);
		setTradesLimit(data.tradesLimit);
		setManagers(data.managers);
		setManagersWages(data.managersWages);
		setLastPaidManagersWages(data.lastPaidManagersWages);
		setLateManagersWages(data.lateManagersWages);
		setBuyers(data.buyers);
		setLocation(data.location);
		setParticlePatternId(data.particlePatternId);
		setSkinId(data.skinId);
		setEquipmentId(data.equipmentId);
		setStatus(data.status);
	}

	public Object writeJsonData() {
		return new NpcMerchantJsonData(this);
	}

}
