package com.guillaumevdn.supremeshops.module.adminshop;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ContainerParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPString;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.AdminShop;
import com.guillaumevdn.supremeshops.util.DestroyCause;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;
import com.guillaumevdn.supremeshops.util.parseable.list.LPTradeObject;

public class AdminShopDataTrade extends ContainerParseable {

	// base
	private PPString name = addComponent(new PPString("name", this, null, true, 0, EditorGUI.ICON_STRING, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADENAME.getLines()));
	private LPTradeObject objects = addComponent(new LPTradeObject("objects", this, false, 1, EditorGUI.ICON_OBJECT, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADEOBJECTS.getLines()));
	private CPConditions conditions = addComponent(new CPConditions("conditions", this, false, 2, EditorGUI.ICON_CONDITION, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADECONDITIONS.getLines()));
	private PPInteger tradesLimit = addComponent(new PPInteger("trades_limit", this, "-1", -1, Integer.MAX_VALUE, false, 3, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADESLIMIT.getLines()));
	private AdminShop shop = null;

	public AdminShopDataTrade(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "admin shop trade", mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPString getName() {
		return name;
	}

	public String getName(Player parser) {
		return name.getParsedValue(parser);
	}

	public LPTradeObject getObjects() {
		return objects;
	}

	public List<TradeObject> getObjects(List<ObjectSide> sides, List<ObjectType> types) {
		List<TradeObject> result = new ArrayList<TradeObject>();
		for (TradeObject object : objects.getElements().values()) {
			if (((sides == null || sides.isEmpty()) || sides.contains(object.getSide())) || ((types == null || types.isEmpty()) || types.contains(object.getType()))) {
				result.add(object);
			}
		}
		return result;
	}

	public CPConditions getConditions() {
		return conditions;
	}

	public PPInteger getTradesLimit() {
		return tradesLimit;
	}

	public Integer getTradesLimit(Player parser) {
		return tradesLimit.getParsedValue(parser);
	}

	public AdminShop getShop() {
		if (shop == null) {
			reloadShop();
		}
		return shop;
	}

	// methods
	public void reloadShop() {
		// destroy previous
		if (shop != null) {
			shop.destroy(DestroyCause.RELOAD_ADMIN_SHOP, true);
		}
		// create new shop
		Integer limit = tradesLimit.getParsedValue(null);
		shop = new AdminShop("adminshop_" + (getParent() != null && getParent().getParent() != null ? getParent().getParent().getId() : "electronlibre") + "_" + getId(), limit == null ? -1 : limit);
		// set objects
		for (TradeObject object : objects.getElements().values()) {
			shop.addObject(object);
		}
		// set conditions
		shop.setTradeConditions(conditions);
	}

}
