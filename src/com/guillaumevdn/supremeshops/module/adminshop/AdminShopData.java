package com.guillaumevdn.supremeshops.module.adminshop;

import java.io.File;
import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ContainerParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.util.parseable.list.LPAdminShopDataTrade;

public class AdminShopData extends ContainerParseable {

	// base
	private File file;
	private LPAdminShopDataTrade trades = addComponent(new LPAdminShopDataTrade("trades", this, true, 0, EditorGUI.ICON_OBJECT, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADES.getLines()));

	public AdminShopData(String id, File file, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "admin shop", mandatory, editorSlot, editorIcon, editorDescription);
		this.file = file;
	}

	// get
	public File getFile() {
		return file;
	}

	public LPAdminShopDataTrade getTrades() {
		return trades;
	}

}
