package com.guillaumevdn.supremeshops.module.gui;

import java.io.File;
import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ContainerParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPString;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.util.parseable.list.LPESGuiItem;

public class SSGui extends ContainerParseable {

	// base
	private File file = null;
	private PPString name = addComponent(new PPString("name", this, null, true, 0, EditorGUI.ICON_STRING, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GUI_NAMELORE.getLines()));
	private PPInteger size = addComponent(new PPInteger("size", this, "9", 9, 54, false, 1, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GUI_SIZELORE.getLines()));
	private LPESGuiItem content = addComponent(new LPESGuiItem("content", this, false, 2, EditorGUI.ICON_ITEM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GUI_CONTENTLORE.getLines()));

	public SSGui(String id, File file, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "gui", mandatory, editorSlot, editorIcon, editorDescription);
		this.file = file;
	}

	// get
	public File getFile() {
		return file;
	}

	public PPString getName() {
		return name;
	}

	public String getName(Player parser) {
		return name.getParsedValue(parser);
	}

	public PPInteger getSize() {
		return size;
	}

	public Integer getSize(Player parser) {
		return size.getParsedValue(parser);
	}

	public LPESGuiItem getContent() {
		return content;
	}

	// methods
	public FilledSSGui build(final Player parser) {
		return new FilledSSGui(this, parser, getName(parser), Utils.getInventorySize(getSize(parser)));
	}

}
