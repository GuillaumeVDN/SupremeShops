package com.guillaumevdn.supremeshops.module.gui;

import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;

public class SSGuiItemType {

	// base
	private Type type;
	private String setting;

	public SSGuiItemType(Type type) {
		this(type, null);
	}

	public SSGuiItemType(Type type, String setting) {
		this.type = type;
		this.setting = setting;
	}

	// get
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getSetting() {
		return setting;
	}

	public void setSetting(String setting) {
		this.setting = setting;
	}

	@Override
	public String toString() {
		return "ESGuiItemType{type=" + (type == null ? "null" : type.toString()) + ",setting=" + setting + "}";
	}

	// static methods
	public static SSGuiItemType from(String string) {
		List<String> split = Utils.split(" ", string, false);
		SSGuiItemType.Type type = Utils.valueOfOrNull(SSGuiItemType.Type.class, split.get(0));
		return type.requireSetting() && split.size() >= 2 ? new SSGuiItemType(type, split.get(1)) : new SSGuiItemType(type);
	}

	// type
	public static enum Type {

		REGULAR(false);

		private boolean setting;

		private Type(boolean setting) {
			this.setting = setting;
		}

		public boolean requireSetting() {
			return setting;
		}

	}

}
