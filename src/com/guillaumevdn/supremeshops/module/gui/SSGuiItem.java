package com.guillaumevdn.supremeshops.module.gui;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ContainerParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.container.CPItem;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPStringList;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.UserInfoSelectionGUI;
import com.guillaumevdn.supremeshops.gui.merchant.ClickerMerchantListGUI;
import com.guillaumevdn.supremeshops.gui.merchant.CustomerMerchantListGUI;
import com.guillaumevdn.supremeshops.gui.shop.ClickerShopListGUI;
import com.guillaumevdn.supremeshops.gui.shop.CustomerShopListGUI;
import com.guillaumevdn.supremeshops.gui.shop.TradePreviewGUI;
import com.guillaumevdn.supremeshops.module.adminshop.AdminShopData;
import com.guillaumevdn.supremeshops.module.adminshop.AdminShopDataTrade;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.parseable.list.ClickTypeLPSSGuiItemAction;
import com.guillaumevdn.supremeshops.util.parseable.primitive.PPSSGuiItemAction;
import com.guillaumevdn.supremeshops.util.parseable.primitive.PPSSGuiItemType;

public class SSGuiItem extends ContainerParseable {

	// base
	private PPSSGuiItemType type = addComponent(new PPSSGuiItemType("type", this, SSGuiItemType.Type.REGULAR.name(), false, 0, EditorGUI.ICON_TECHNICAL, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GUI_CONTENT_TYPELORE.getLines()));
	private CPItem item = addComponent(new CPItem("item", this, false, 1, EditorGUI.ICON_STRING, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GUI_CONTENT_ITEMLORE.getLines()));
	private PPStringList commands = addComponent(new PPStringList("commands", this, null, false, 2, EditorGUI.ICON_STRING_LIST, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GUI_CONTENT_COMMANDSLORE.getLines()));
	private ClickTypeLPSSGuiItemAction clickActions = addComponent(new ClickTypeLPSSGuiItemAction("click_actions", this, true, false, 3, EditorGUI.ICON_TECHNICAL, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GUI_CONTENT_CLICKACTIONSLORE.getLines()));

	public SSGuiItem(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "gui content item", mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPSSGuiItemType getType() {
		return type;
	}

	public SSGuiItemType getType(Player parser) {
		return type.getParsedValue(parser);
	}

	public CPItem getItem() {
		return item;
	}

	public PPStringList getCommands() {
		return commands;
	}

	public List<String> getCommands(Player parser) {
		return commands.getParsedValue(parser);
	}

	public ClickTypeLPSSGuiItemAction getClickActions() {
		return clickActions;
	}

	// methods
	public ClickeableItem buildItem(final Player parser) {
		if (item == null) return null;
		ItemData parsed = item.getParsedValue(parser).cloneWithId(getId());
		return new ClickeableItem(parsed) {

			private long lastClick = 0L;

			@Override
			public void onClick(final Player player, ClickType clickType, final GUI gui, final int pageIndex) {
				// execute action
				PPSSGuiItemAction action = clickActions.getValue(clickType.name());
				if (action != null) {
					SSGuiItemAction parsed = action.getParsedValue(parser);
					if (parsed != null) {
						executeAction(player, parsed, gui, pageIndex);
					}
				}
				// execute commands
				List<String> commands = getCommands(parser);
				if (commands != null) {
					for (String command : commands) {
						Bukkit.dispatchCommand(Bukkit.getConsoleSender(), command.replace("{player}", player.getName()));
					}
				}
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}

			private void executeAction(final Player player, final SSGuiItemAction action, final GUI fromGUI, final int fromGUIPageIndex) {
				// player shops
				if (action.getType().equals(SSGuiItemAction.Type.OPEN_PLAYER_SHOPS)) {
					new CustomerShopListGUI(null, false, fromGUI, fromGUIPageIndex, player).open(player);
				}
				// player shops by owner
				else if (action.getType().equals(SSGuiItemAction.Type.OPEN_PLAYER_SHOPS_BY_OWNER)) {
					List<UserInfo> users = new ArrayList<UserInfo>();
					for (UserInfo user : SupremeShops.inst().getData().getShops().getShopsByOwners().keySet()) {
						if (!SupremeShops.inst().getData().getShops().getElements(user, null, true, com.guillaumevdn.supremeshops.data.ShopBoard.ElementRemotePolicy.MUST_BE).isEmpty()) {
							users.add(user);
						}
					}
					new UserInfoSelectionGUI(users, fromGUI, fromGUIPageIndex) {
						@Override
						protected void onSelect(UserInfo selected, GUI gui, int pageIndex) {
							new CustomerShopListGUI(selected, false, gui, pageIndex, player).open(player);
						}
					}.open(player);
				}
				// clicker player shops
				else if (action.getType().equals(SSGuiItemAction.Type.OPEN_PLAYER_SHOPS_CLICKER)) {
					new ClickerShopListGUI(new UserInfo(player), player, fromGUI, fromGUIPageIndex).open(player);
				}
				// player merchants
				else if (action.getType().equals(SSGuiItemAction.Type.OPEN_PLAYER_MERCHANTS)) {
					new CustomerMerchantListGUI(null, false, player, fromGUI, fromGUIPageIndex).open(player);
				}
				// player merchants by owner
				else if (action.getType().equals(SSGuiItemAction.Type.OPEN_PLAYER_MERCHANTS_BY_OWNER)) {
					List<UserInfo> users = new ArrayList<UserInfo>();
					for (UserInfo user : SupremeShops.inst().getData().getMerchants().getMerchantsByOwners().keySet()) {
						if (!SupremeShops.inst().getData().getMerchants().getElements(user, null, true, com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy.MUST_BE).isEmpty()) {
							users.add(user);
						}
					}
					new UserInfoSelectionGUI(users, fromGUI, fromGUIPageIndex) {
						@Override
						protected void onSelect(UserInfo selected, GUI gui, int pageIndex) {
							new CustomerMerchantListGUI(selected, false, player, gui, pageIndex).open(player);
						}
					}.open(player);
				}
				// clicker player merchants
				else if (action.getType().equals(SSGuiItemAction.Type.OPEN_PLAYER_MERCHANTS_CLICKER)) {
					new ClickerMerchantListGUI(new UserInfo(player), player, fromGUI, fromGUIPageIndex).open(player);
				}
				// gui
				else if (action.getType().equals(SSGuiItemAction.Type.OPEN_GUI)) {
					SSGui other = SupremeShops.inst().getModuleManager().getGui(action.getSetting());
					if (other != null) {
						other.build(parser).open(player);
					} else {
						SupremeShops.inst().warning("Couldn't open GUI " + action.getSetting() + " linked in item " + SSGuiItem.this.getId() + " in GUI " + fromGUI.getName());
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
						}
					}
				}
				// admin shop trade
				else if (action.getType().equals(SSGuiItemAction.Type.OPEN_ADMIN_SHOP_TRADE)) {
					String setting = action.getSetting();
					AdminShopDataTrade trade = null;
					if (setting != null) {
						String[] split = setting.split(":");
						if (split.length == 2) {
							AdminShopData adminShop = SupremeShops.inst().getModuleManager().getAdminShop(split[0]);
							if (adminShop != null) {
								trade = adminShop.getTrades().getElement(split[1]);
							}
						}
					}
					Shop shop = trade == null ? null : trade.getShop();
					if (shop == null) {
						SupremeShops.inst().warning("Couldn't decode admin shop trade setting '" + setting + "' for item " + item.getId() + " in GUI " + fromGUI.getName() + (trade != null ? " (couldn't initialize shop)" : ""));
						return;
					}
					// can't trade
					if (!shop.areTradeConditionsValid(player, true)) {
						return;
					}
					// didn't discover all items
					if (SupremeShops.inst().getForceItemDiscover() && !shop.hasDiscoveredAllItems(player, true)) {
						return;
					}
					// done
					new TradePreviewGUI(trade.getShop(), null, 1, player, fromGUI, fromGUIPageIndex).open(player);
				}
				// quick sell admin for vault money
				else if (action.getType().equals(SSGuiItemAction.Type.QUICK_SELL_ADMIN_FOR_VAULT_MONEY)) {
					// last click is too recent ; calculating all those modifiers and max trades could be a little resource consuming so make sure the player can't spam click this button
					if (System.currentTimeMillis() - lastClick < 1000L) {
						return;
					}
					lastClick = System.currentTimeMillis();
					// click
					if (fromGUI instanceof FilledSSGui) {
						// save inventory state and money
						double initialMoney = GCoreLegacy.inst().getEconomyHandler().get(player);
						// trade as much as possible silently
						for (AdminShopDataTrade trade : ((FilledSSGui) fromGUI).getLastFilledAdminShopsTrades()) {
							// there isn't only items in taking objects
							if (trade.getShop().getObjects(Utils.asList(ObjectSide.TAKING), Utils.asList(ObjectType.ITEM)).size() != trade.getShop().getObjects(Utils.asList(ObjectSide.TAKING), null).size()) {
								continue;
							}
							// there isn't only money in giving objects
							if (trade.getShop().getObjects(Utils.asList(ObjectSide.GIVING), Utils.asList(ObjectType.VAULT_MONEY)).size() != trade.getShop().getObjects(Utils.asList(ObjectSide.GIVING), null).size()) {
								continue;
							}
							// calculate possible trades and trade
							int trades = trade.getShop().getPossibleTradesWith(player, player);
							if (trades > 0) {
								trade.getShop().attemptTradesProcessing(player, trades, null);
							}
						}
						// send end message
						double benefit = GCoreLegacy.inst().getEconomyHandler().get(player) - initialMoney;
						if (benefit == 0d) {
							SSLocale.MSG_SUPREMESHOPS_TRADEASMUCHASPOSSIBLENONE.send(player);
						} else {
							SSLocale.MSG_SUPREMESHOPS_TRADEASMUCHASPOSSIBLEBENEFIT.send(player, "{money}", Utils.round5(benefit));
						}
					}
				}
			}

		};
	}

}
