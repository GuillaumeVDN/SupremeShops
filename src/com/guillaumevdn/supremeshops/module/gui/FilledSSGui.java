package com.guillaumevdn.supremeshops.module.gui;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.messenger.Replacer;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.adminshop.AdminShopData;
import com.guillaumevdn.supremeshops.module.adminshop.AdminShopDataTrade;
import com.guillaumevdn.supremeshops.util.parseable.primitive.PPSSGuiItemAction;

public class FilledSSGui extends FilledGUI {

	// base
	private SSGui esGUI;
	private Player player;
	private List<SSGuiItem> lastFilledContent = new ArrayList<SSGuiItem>();
	private List<AdminShopDataTrade> lastFilledAdminShopsTrades = new ArrayList<AdminShopDataTrade>();

	public FilledSSGui(SSGui esGUI, Player player, String name, int size) {
		super(SupremeShops.inst(), name, size, GUI.getRegularItemSlots(0, size - 1));
		this.esGUI = esGUI;
		this.player = player;
	}

	// get
	public List<SSGuiItem> getLastFilledContent() {
		return lastFilledContent;
	}

	public List<AdminShopDataTrade> getLastFilledAdminShopsTrades() {
		return lastFilledAdminShopsTrades;
	}

	// fill
	@Override
	protected void fill() {
		// content
		lastFilledContent.clear();
		lastFilledAdminShopsTrades.clear();
		List<ClickeableItem> awaiting = new ArrayList<>();
		for (SSGuiItem item : esGUI.getContent().getElements().values()) {
			ClickeableItem add = item.buildItem(player);
			if (add != null && add.getItemData() != null && add.getItemData().getType() != null && !add.getItemData().getType().isAir() && add.getItemData().getType().exists()) {
				ItemStack itemStack = add.getItemData().getItemStack().clone();
				// has admin shop, add it
				for (PPSSGuiItemAction action : item.getClickActions().getElements().values()) {
					SSGuiItemAction parsed = action.getParsedValue(player);
					if (parsed != null && parsed.getType().equals(SSGuiItemAction.Type.OPEN_ADMIN_SHOP_TRADE)) {
						boolean decodedSetting = false;
						String setting = parsed.getSetting();
						if (setting != null) {
							String[] split = setting.split(":");
							if (split.length == 2) {
								AdminShopData adminShop = SupremeShops.inst().getModuleManager().getAdminShop(split[0]);
								if (adminShop != null) {
									AdminShopDataTrade trade = adminShop.getTrades().getElement(split[1]);
									if (trade != null) {
										lastFilledAdminShopsTrades.add(trade);
										decodedSetting = true;
										itemStack = new Replacer(trade.getShop().getMessageReplacers(true, false, player)).apply(itemStack);
									}
								}
							}
						}
						if (!decodedSetting) {
							SupremeShops.inst().warning("Couldn't decode admin shop trade setting '" + setting + "' for item " + item.getId() + " in GUI " + esGUI.getId());
						}
					}
				}
				// add content
				lastFilledContent.add(item);
				add.setItem(new ItemData(add.getItemData().getId(), add.getItemData().getSlot(), itemStack));
				if (add.getItemData().getSlot() >= 0 && getItemInSlot(getPages().size() - 1 /*if -1 it'll return null*/, add.getItemData().getSlot()) == null) {
					setRegularItem(add);
				} else {
					awaiting.add(add);
				}
			}
		}
		// add awaiting items
		for (ClickeableItem item : awaiting) {
			setRegularItem(item);
		}
	}
	@Override
	protected boolean postFill() {
		return true;
	}

}
