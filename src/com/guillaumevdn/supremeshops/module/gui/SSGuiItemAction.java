package com.guillaumevdn.supremeshops.module.gui;

import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;

public class SSGuiItemAction {

	// base
	private Type type;
	private String setting;

	public SSGuiItemAction(Type type) {
		this(type, null);
	}

	public SSGuiItemAction(Type type, String setting) {
		this.type = type;
		this.setting = setting;
	}

	// get
	public Type getType() {
		return type;
	}

	public void setType(Type type) {
		this.type = type;
	}

	public String getSetting() {
		return setting;
	}

	public void setSetting(String setting) {
		this.setting = setting;
	}

	@Override
	public String toString() {
		return "ESGuiItemAction{type=" + (type == null ? "null" : type.toString()) + ",setting=" + setting + "}";
	}

	// static methods
	public static SSGuiItemType from(String string) {
		List<String> split = Utils.split(" ", string, false);
		SSGuiItemType.Type type = Utils.valueOfOrNull(SSGuiItemType.Type.class, split.get(0));
		return type.requireSetting() && split.size() >= 2 ? new SSGuiItemType(type, split.get(1)) : new SSGuiItemType(type);
	}

	// type
	public static enum Type {

		OPEN_GUI(true),
		OPEN_PLAYER_SHOPS(false),
		OPEN_PLAYER_SHOPS_BY_OWNER(false),
		OPEN_PLAYER_SHOPS_CLICKER(false),
		OPEN_PLAYER_MERCHANTS(false),
		OPEN_PLAYER_MERCHANTS_BY_OWNER(false),
		OPEN_PLAYER_MERCHANTS_CLICKER(false),
		OPEN_ADMIN_SHOP_TRADE(true),
		QUICK_SELL_ADMIN_FOR_VAULT_MONEY(false),
		NONE(false);

		private boolean setting;

		private Type(boolean setting) {
			this.setting = setting;
		}

		public boolean requireSetting() {
			return setting;
		}

	}

}
