package com.guillaumevdn.supremeshops.module.condition;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.messenger.Text;
import com.guillaumevdn.gcorelegacy.lib.parseable.ConfigData;
import com.guillaumevdn.gcorelegacy.lib.parseable.ContainerParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.ListParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorItem;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.ModifCallback;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPText;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;

public abstract class Condition extends ContainerParseable {

	// base
	private ConditionType type;
	private PPText errorMessage = addComponent(new PPText("error_message", this, Utils.emptyList(), false, 1, EditorGUI.ICON_STRING_LIST, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_ERRORMESSAGELORE.getLines()));

	public Condition(String id, ConditionType type, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "condition", mandatory, editorSlot, editorIcon, editorDescription);
		this.type = type;
	}

	// get
	public ConditionType getType() {
		return type;
	}

	public PPText getErrorMessage() {
		return errorMessage;
	}

	public Text getErrorMessage(Player parser) {
		return errorMessage.getParsedValue(parser);
	}

	// clone
	protected Condition() {
		super();
	}

	@Override
	public Condition clone() {
		Condition clone = (Condition) super.clone();
		clone.type = type;
		return clone;
	}

	// methods
	public abstract String describe(Player parser);

	/**
	 * @param shop the shop to check
	 * @param player the settings parser
	 * @return true if the player has the condition
	 */
	public boolean isValid(Shop shop, Player player) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @param merchant the merchant to check
	 * @param parser the settings parser
	 * @return true if the player has the condition
	 */
	public boolean isValid(Merchant merchant, Player player) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @param trigger the trigger to check
	 * @param parser the settings parser
	 * @return true if the player has the condition
	 */
	public boolean isValid(Trigger trigger, Player player) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	/**
	 * @param player the player to check
	 * @param parser the settings parser
	 * @return true if the player has the condition
	 */
	public boolean isValid(Player player) throws UnsupportedOperationException {
		throw new UnsupportedOperationException();
	}

	// save
	@Override
	public void save(ConfigData data) {
		super.save(data);
		data.getConfig().set(data.getPath() + ".type", type.getId());
	}

	// editor
	@Override
	public List<String> describe(int depth) {
		List<String> desc = super.describe(depth);
		String spaces = Utils.copyString(" ", depth + 1);
		desc.add(1, spaces + " §6> type : §e" + type.getId());
		return desc;
	}

	@Override
	public void fillEditor(final EditorGUI gui, Player player, final ModifCallback onModif) {
		// add type selector
		gui.setRegularItem(new EditorItem("condition_type", 0, Mat.ENDER_CHEST, "§6type", SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_TYPELORE.getLines()) {
			@Override
			protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
				// create sub GUI
				String name = Utils.getNewInventoryName(gui.getName(), "type");
				EditorGUI sub = new EditorGUI(getLastData().getPlugin(), gui, name, 9, GUI.SLOTS_0_TO_7) {
					private EditorGUI subThis = this;
					@Override
					protected void fill() {
						// current
						EditorGUI.fillItemCurrent(subThis, player, "type", Utils.asList(type.getId()), null, "condition type", isMandatory(), getEditorIcon(), 0, onModif);
						// select
						setRegularItem(new EditorItem("control_item_select", 2, Mat.ENDER_CHEST, GLocale.GUI_GENERIC_EDITORENUMSELECT.getLine(), GLocale.GUI_GENERIC_EDITORENUMSELECTLORE.getLines()) {
							@Override
							protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
								// selection gui
								EditorGUI subSelection = new EditorGUI(getLastData().getPlugin(), gui, Utils.getNewInventoryName(gui.getName(), "Select"), 54, GUI.SLOTS_0_TO_44) {
									@Override
									protected void fill() {
										// add values
										for (final ConditionType val : ConditionType.values()) {
											final String valName = val.getId();
											setRegularItem(new EditorItem("value_" + valName, -1, Mat.ENDER_CHEST, "§6" + valName, null) {
												@Override
												protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
													Condition curr = Condition.this;
													final Condition result = val.createNew(curr.getId(), curr.getParent(), curr.getLastData().clone(), false, curr.isMandatory(), curr.getEditorSlot(), curr.getEditorIcon(), curr.getEditorDescription());
													// error loading
													if (result == null) {
														player.sendMessage("§cAn unknown error occured, check the console for more details.");
														gui.open(player);
													}
													// success
													else {
														// replace result
														if (Utils.instanceOf(Condition.this.getParent(), ContainerParseable.class)) {
															((ContainerParseable) Condition.this.getParent()).addComponent(result);
														} else if (Utils.instanceOf(Condition.this.getParent(), ListParseable.class)) {
															((ListParseable<Condition>) Condition.this.getParent()).addElement(result);
														}
														onModif.callback(result, subThis, player);
														// create result GUI
														EditorGUI comp = new EditorGUI(result.getLastData().getPlugin(), gui.getParent(), gui.getName(), result.getEditorSize(), result.getEditorRegularSlots()) {
															private EditorGUI compThis = this;
															@Override
															protected void fill() {
																result.fillEditor(compThis, player, onModif);
																// back item (inside fill())
																compThis.setPersistentItem(new EditorItem("control_item_back", result.getEditorBackSlot(), Mat.ARROW, GLocale.GUI_GENERIC_EDITORITEMBACK.getLine(), null) {
																	@Override
																	protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
																		gui.getParent().open(player);
																	}
																});
															}
														};
														// open it
														comp.open(player);
													}
												}
											});
										}
										// back item
										setPersistentItem(new EditorItem("control_item_back", 52, Mat.ARROW, GLocale.GUI_GENERIC_EDITORITEMBACK.getLine(), null) {
											@Override
											protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
												gui.open(player);
											}
										});
									}
								};
								// open it
								subSelection.open(player);
							}
						});
						// back item
						setPersistentItem(new EditorItem("control_item_back", 8, Mat.ARROW, GLocale.GUI_GENERIC_EDITORITEMBACK.getLine(), null) {
							@Override
							protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
								gui.open(player);
							}
						});
					}
				};
				// open it
				sub.open(player);
			}
		});
		// super
		super.fillEditor(gui, player, onModif);
	}

	// static methods
	/**
	 * Load an element from a configuration
	 * @return the loaded condition, or null if couldn't instantiate it for some reason
	 */
	public static Condition load(String id, Parseable parent, ConfigData data, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		// compact
		ConditionType type = null;
		// missing type setting
		data.setContains(data.getConfig().contains(data.getPath() + ".type"));
		if (!data.contains()) {
			(parent == null || parent.getLastData() == null ? data : parent.getLastData()).log("missing primitive setting 'type' (must be a condition type)");
			return null;
		}
		// invalid type
		type = ConditionType.valueOf(data.getConfig().getString(data.getPath() + ".type", ""));
		if (type == null) {
			(parent == null || parent.getLastData() == null ? data : parent.getLastData()).log("invalid primitive setting 'type' (must be a condition type)");
			return null;
		}
		// create
		return type.createNew(id, parent, data, true, mandatory, editorSlot, editorIcon, editorDescription);
	}

}
