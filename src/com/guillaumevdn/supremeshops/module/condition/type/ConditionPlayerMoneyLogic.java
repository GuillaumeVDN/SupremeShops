package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionPlayerMoneyLogic extends ConditionLogic<ConditionPlayerMoney> {

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ConditionPlayerMoney value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
		JsonUtils.writeJsonValue(out, "operation", value.getOperation().getValue());
		JsonUtils.writeJsonValue(out, "amount", value.getAmount().getValue());
	}

	@Override
	public ConditionPlayerMoney readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionPlayerMoney condition = new ConditionPlayerMoney(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		condition.getOperation().setValue(JsonUtils.readJsonValue(in));
		condition.getAmount().setValue(JsonUtils.readJsonValue(in));
		return condition;
	}

	// add procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return GCoreLegacy.inst().getEconomyHandler().getUtils() != null && SSPerm.SUPREMESHOPS_CREATE_CONDITION_PLAYER_MONEY.has(player);
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return GCoreLegacy.inst().getEconomyHandler().getUtils() != null && SSPerm.SUPREMESHOPS_CREATE_CONDITION_PLAYER_MONEY.has(player);
	}

	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return GCoreLegacy.inst().getEconomyHandler().getUtils() != null;
	}

	@Override
	public void startCreateProcedure(final Player player, final boolean sendInputMessage, final String nextId, final Parseable parent, final GUI fromGUI, final ConditionLogic<ConditionPlayerMoney>.DoneCallback callback) {
		askDecimalNumber(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONMONEYINPUT, 0, Integer.MAX_VALUE, fromGUI, new DecimalNumberSelectionCallback() {
			@Override
			public void onSelect(final double amount) {
				askEnumValue(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONOPERATIONINPUT, fromGUI, ConditionPlayerMoney.Operation.class, new EnumSelectionCallback<ConditionPlayerMoney.Operation>() {
					@Override
					public void onSelect(final ConditionPlayerMoney.Operation operation) {
						// create condition
						ConditionPlayerMoney condition = new ConditionPlayerMoney(nextId, parent, false, -1, EditorGUI.ICON_CONDITION, null);
						condition.getAmount().setValue(Utils.asList("" + amount));
						condition.getOperation().setValue(Utils.asList(operation.name()));
						// done
						callback.callback(condition);
					}
				});
			}
		});
	}

}
