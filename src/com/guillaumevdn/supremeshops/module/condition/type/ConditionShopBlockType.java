package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPMat;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.Locatable;

public class ConditionShopBlockType extends Condition {

	// base
	private PPMat blockType = addComponent(new PPMat("block_type", this, null, true, 9, EditorGUI.ICON_ENUM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_SHOPBLOCKTYPELORE.getLines()));

	public ConditionShopBlockType(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.SHOP_BLOCK_TYPE, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPMat getBlockType() {
		return blockType;
	}

	public Mat getBlockType(Player parser) {
		return blockType.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPBLOCKTYPE.getLine("{type}", blockType.getParsedValue(parser).toString().replace("_", " ").toLowerCase());
	}

	@Override
	public boolean isValid(Shop shop, Player player) throws UnsupportedOperationException {
		if (!(shop instanceof Locatable)) {
			throw new UnsupportedOperationException();
		}
		Mat blockType = getBlockType(player);
		if (blockType == null) return false;
		return Mat.fromBlock(((Locatable) shop).getBlock().toBlock()).equals(blockType);
	}

	// clone
	protected ConditionShopBlockType() {
		super();
	}

}
