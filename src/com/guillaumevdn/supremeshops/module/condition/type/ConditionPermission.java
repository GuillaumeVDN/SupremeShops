package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.Perm;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPPerm;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPString;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ConditionPermission extends Condition {

	// base
	private PPPerm permission = addComponent(new PPPerm("permission", this, null, false, 9, EditorGUI.ICON_TECHNICAL, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_PERMISSIONLORE.getLines()));
	private PPString description = addComponent(new PPString("description", this, null, false, 10, EditorGUI.ICON_STRING, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_PERMISSIONDESCRIPTIONLORE.getLines()));

	public ConditionPermission(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.PERMISSION, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPPerm getPermission() {
		return permission;
	}

	public Perm getPermission(Player parser) {
		return permission.getParsedValue(parser);
	}

	public PPString getDescription() {
		return description;
	}

	public String getDescription(Player parser) {
		return description.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERPERMISSION.getLine("{permission}", getPermission(parser), "{description}", getDescription(parser));
	}

	@Override
	public boolean isValid(Player player) {
		Perm permission = getPermission(player);
		if (permission == null) return false;
		return permission.has(player);
	}

	@Override
	public boolean isValid(Shop shop, Player player) throws UnsupportedOperationException {
		Perm permission = getPermission(player);
		if (permission == null) return false;
		return shop.isCurrentOwnerAdmin() || permission.has(shop.getCurrentOwner().toOfflinePlayer());
	}

	@Override
	public boolean isValid(Merchant merchant, Player parser) throws UnsupportedOperationException {
		Perm permission = getPermission(parser);
		if (permission == null || merchant == null) return false;
		return merchant
				.isCurrentOwnerAdmin()
				||
				permission
				.has(
						merchant
						.getCurrentOwner()
						);
	}

	// clone
	protected ConditionPermission() {
		super();
	}

}
