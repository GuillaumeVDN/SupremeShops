package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPEnum;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.parseable.primitive.PPObjectType;

public class ConditionShopObjectCount extends Condition {

	// base
	private PPEnum<ObjectSide> side = addComponent(new PPEnum<ObjectSide>("object_side", this, ObjectSide.GIVING.name(), ObjectSide.class, "object side", false, 9, EditorGUI.ICON_OBJECT, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_OBJECTSIDELORE.getLines()));
	private PPObjectType type = addComponent(new PPObjectType("object_type", this, ObjectType.VAULT_MONEY.getId(), false, 10, EditorGUI.ICON_OBJECT, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_OBJECTTYPELORE.getLines()));
	private PPEnum<Operation> operation = addComponent(new PPEnum<Operation>("operation", this, Operation.AT_LEAST.name(), Operation.class, "operation", false, 11, EditorGUI.ICON_ENUM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_OPERATIONLORE.getLines()));
	private PPInteger amount = addComponent(new PPInteger("amount", this, "1", 0, Integer.MAX_VALUE, false, 12, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_AMOUNTLORE.getLines()));

	public ConditionShopObjectCount(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.SHOP_OBJECT_COUNT, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPEnum<Operation> getOperation() {
		return operation;
	}

	public Operation getOperation(Player parser) {
		return operation.getParsedValue(parser);
	}

	public PPEnum<ObjectSide> getObjectSide() {
		return side;
	}

	public ObjectSide getObjectSide(Player parser) {
		return side.getParsedValue(parser);
	}

	public PPObjectType getObjectType() {
		return type;
	}

	public ObjectType getObjectType(Player parser) {
		return type.getParsedValue(parser);
	}

	public PPInteger getAmount() {
		return amount;
	}

	public Integer getAmount(Player parser) {
		return amount.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		int amount = getAmount(parser);
		ObjectSide side = getObjectSide(parser);
		ObjectType type = getObjectType(parser);
		Operation operation = getOperation(parser);
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPOBJECTCOUNT.getLine("{operation}", operation.name(), "{amount}", amount, "{plural}", Utils.getPlural(amount), "{side}", side == null ? "" : " " + side.name().replace("_", " ").toLowerCase(), "{type}", type == null ? "" : " " + type.getName().toLowerCase());
	}

	@Override
	public boolean isValid(Shop shop, Player player) throws UnsupportedOperationException {
		Operation operation = getOperation(player);
		ObjectType type = getObjectType(player);
		ObjectSide side = getObjectSide(player);
		Integer amount = getAmount(player);
		if (operation == null || type == null || amount == null) return false;
		if (operation.equals(Operation.MORE_THAN)) {
			return shop.getObjects(side == null ? null : Utils.asList(side), type == null ? null : Utils.asList(type)).size() > amount;
		} else if (operation.equals(Operation.AT_LEAST)) {
			return shop.getObjects(side == null ? null : Utils.asList(side), type == null ? null : Utils.asList(type)).size() >= amount;
		} else if (operation.equals(Operation.EQUALS)) {
			return shop.getObjects(side == null ? null : Utils.asList(side), type == null ? null : Utils.asList(type)).size() == amount;
		} else if (operation.equals(Operation.AT_MOST)) {
			return shop.getObjects(side == null ? null : Utils.asList(side), type == null ? null : Utils.asList(type)).size() <= amount;
		} else if (operation.equals(Operation.LESS_THAN)) {
			return shop.getObjects(side == null ? null : Utils.asList(side), type == null ? null : Utils.asList(type)).size() < amount;
		} else if (operation.equals(Operation.DIFFERENT)) {
			return shop.getObjects(side == null ? null : Utils.asList(side), type == null ? null : Utils.asList(type)).size() != amount;
		}
		return false;
	}

	// operation
	public static enum Operation {
		MORE_THAN,
		AT_LEAST,
		EQUALS,
		AT_MOST,
		LESS_THAN,
		DIFFERENT
	}

	// clone
	protected ConditionShopObjectCount() {
		super();
	}

}
