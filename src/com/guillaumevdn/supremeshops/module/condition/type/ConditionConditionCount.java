package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;
import com.guillaumevdn.supremeshops.util.parseable.primitive.PPConditionType;

public class ConditionConditionCount extends Condition {

	// base
	private PPConditionType conditionType = addComponent(new PPConditionType("condition_type", this, ConditionType.GAME_TIME.getId(), false, 10, EditorGUI.ICON_CONDITION, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_CONDITIONTYPELORE.getLines()));
	private PPInteger amount = addComponent(new PPInteger("amount", this, "1", 0, Integer.MAX_VALUE, false, 11, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_AMOUNTLORE.getLines()));

	public ConditionConditionCount(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.CONDITION_COUNT, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPConditionType getConditionType() {
		return conditionType;
	}

	public ConditionType getConditionType(Player parser) {
		return conditionType.getParsedValue(parser);
	}

	public PPInteger getAmount() {
		return amount;
	}

	public Integer getAmount(Player parser) {
		return amount.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		int amount = getAmount(parser);
		ConditionType type = getConditionType(parser);
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPCONDITIONCOUNT.getLine("{amount}", amount, "{plural}", Utils.getPlural(amount), "{type}", type == null ? "" : " " + type.getName().toLowerCase());
	}

	@Override
	public boolean isValid(Shop shop, Player player) throws UnsupportedOperationException {
		ConditionType type = getConditionType(player);
		Integer amount = getAmount(player);
		return shop.getTradeConditions(type == null ? null : Utils.asList(type)).size() >= amount;
	}

	@Override
	public boolean isValid(Trigger trigger, Player parser) throws UnsupportedOperationException {
		ConditionType type = getConditionType(parser);
		Integer amount = getAmount(parser);
		return trigger.getTriggerConditions(type == null ? null : Utils.asList(type)).size() >= amount;
	}

	@Override
	public boolean isValid(Merchant merchant, Player parser) throws UnsupportedOperationException {
		ConditionType type = getConditionType(parser);
		Integer amount = getAmount(parser);
		return merchant.getInteractConditions(type == null ? null : Utils.asList(type)).size() >= amount;
	}

	// clone
	protected ConditionConditionCount() {
		super();
	}

}
