package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPEnum;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;

public class ConditionPlayerXpLevel extends Condition {

	// base
	private PPEnum<Operation> operation = addComponent(new PPEnum<Operation>("operation", this, Operation.AT_LEAST.name(), Operation.class, "operation", false, 9, EditorGUI.ICON_ENUM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_OPERATIONLORE.getLines()));
	private PPInteger level = addComponent(new PPInteger("level", this, "1", 0, Integer.MAX_VALUE, false, 10, EditorGUI.ICON_NUMBER_LEVEL, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_XPLEVELLORE.getLines()));

	public ConditionPlayerXpLevel(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.PLAYER_XP_LEVEL, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPEnum<Operation> getOperation() {
		return operation;
	}

	public Operation getOperation(Player parser) {
		return operation.getParsedValue(parser);
	}

	public PPInteger getLevel() {
		return level;
	}

	public Integer getLevel(Player parser) {
		return level.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERXPLEVEL.getLine("{operation}", getOperation(parser).name().toLowerCase().replace("_", " "), "{level}", getLevel(parser));
	}

	@Override
	public boolean isValid(Player player) throws UnsupportedOperationException {
		Operation operation = getOperation(player);
		Integer level = getLevel(player);
		if (operation == null || level == null) return false;
		if (operation.equals(Operation.MORE_THAN)) {
			return player.getLevel() > level;
		} else if (operation.equals(Operation.AT_LEAST)) {
			return player.getLevel() >= level;
		} else if (operation.equals(Operation.EQUALS)) {
			return player.getLevel() == level;
		} else if (operation.equals(Operation.AT_MOST)) {
			return player.getLevel() <= level;
		} else if (operation.equals(Operation.LESS_THAN)) {
			return player.getLevel() < level;
		} else if (operation.equals(Operation.DIFFERENT)) {
			return player.getLevel() != level;
		}
		return false;
	}

	// operation
	public static enum Operation {
		MORE_THAN,
		AT_LEAST,
		EQUALS,
		AT_MOST,
		LESS_THAN,
		DIFFERENT
	}

	// clone
	protected ConditionPlayerXpLevel() {
		super();
	}

}
