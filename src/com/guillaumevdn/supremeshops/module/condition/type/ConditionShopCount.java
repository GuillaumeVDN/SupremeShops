package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPEnum;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.ShopType;

public class ConditionShopCount extends Condition {

	// base
	private PPEnum<ShopType> shopType = addComponent(new PPEnum<ShopType>("shop_type", this, ShopType.BLOCK.name(), ShopType.class, "shop type", true, 9, EditorGUI.ICON_ENUM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_SHOPTYPELORE.getLines()));
	private PPEnum<Operation> operation = addComponent(new PPEnum<Operation>("operation", this, Operation.AT_LEAST.name(), Operation.class, "operation", false, 10, EditorGUI.ICON_ENUM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_OPERATIONLORE.getLines()));
	private PPInteger amount = addComponent(new PPInteger("amount", this, "1", 0, Integer.MAX_VALUE, false, 11, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_AMOUNTLORE.getLines()));

	public ConditionShopCount(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.SHOP_COUNT, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPEnum<Operation> getOperation() {
		return operation;
	}

	public Operation getOperation(Player parser) {
		return operation.getParsedValue(parser);
	}

	public PPEnum<ShopType> getShopType() {
		return shopType;
	}

	public ShopType getShopType(Player parser) {
		return shopType.getParsedValue(parser);
	}

	public PPInteger getAmount() {
		return amount;
	}

	public Integer getAmount(Player parser) {
		return amount.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		ShopType type = getShopType(parser);
		int amount = getAmount(parser);
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_SHOPCOUNT.getLine("{operation}", getOperation(parser).name().toLowerCase().replace("_", " "), "{type}", type == null ? "" : type.getName(), "{amount}", amount, "{plural}", Utils.getPlural(amount));
	}

	@Override
	public boolean isValid(Player player) throws UnsupportedOperationException {
		Operation operation = getOperation(player);
		ShopType type = getShopType(player);
		Integer amount = getAmount(player);
		if (operation == null || type == null || amount == null) return false;
		if (operation.equals(Operation.MORE_THAN)) {
			return SupremeShops.inst().getData().getShops().getElementsByOwner(new UserInfo(player), type == null ? null : Utils.asList(type.getShopClass())).size() > amount;
		} else if (operation.equals(Operation.AT_LEAST)) {
			return SupremeShops.inst().getData().getShops().getElementsByOwner(new UserInfo(player), type == null ? null : Utils.asList(type.getShopClass())).size() >= amount;
		} else if (operation.equals(Operation.EQUALS)) {
			return SupremeShops.inst().getData().getShops().getElementsByOwner(new UserInfo(player), type == null ? null : Utils.asList(type.getShopClass())).size() == amount;
		} else if (operation.equals(Operation.AT_MOST)) {
			return SupremeShops.inst().getData().getShops().getElementsByOwner(new UserInfo(player), type == null ? null : Utils.asList(type.getShopClass())).size() <= amount;
		} else if (operation.equals(Operation.LESS_THAN)) {
			return SupremeShops.inst().getData().getShops().getElementsByOwner(new UserInfo(player), type == null ? null : Utils.asList(type.getShopClass())).size() < amount;
		} else if (operation.equals(Operation.DIFFERENT)) {
			return SupremeShops.inst().getData().getShops().getElementsByOwner(new UserInfo(player), type == null ? null : Utils.asList(type.getShopClass())).size() != amount;
		}
		return false;
	}

	@Override
	public boolean isValid(Merchant merchant, Player player) throws UnsupportedOperationException {
		Operation operation = getOperation(player);
		ShopType type = getShopType(player);
		Integer amount = getAmount(player);
		if (operation == null || type == null || amount == null) return false;
		if (operation.equals(Operation.MORE_THAN)) {
			return merchant.getShops(type == null ? null : Utils.asList(type.getShopClass()), false).size() > amount;
		} else if (operation.equals(Operation.AT_LEAST)) {
			return merchant.getShops(type == null ? null : Utils.asList(type.getShopClass()), false).size() >= amount;
		} else if (operation.equals(Operation.EQUALS)) {
			return merchant.getShops(type == null ? null : Utils.asList(type.getShopClass()), false).size() == amount;
		} else if (operation.equals(Operation.AT_MOST)) {
			return merchant.getShops(type == null ? null : Utils.asList(type.getShopClass()), false).size() <= amount;
		} else if (operation.equals(Operation.LESS_THAN)) {
			return merchant.getShops(type == null ? null : Utils.asList(type.getShopClass()), false).size() < amount;
		} else if (operation.equals(Operation.DIFFERENT)) {
			return merchant.getShops(type == null ? null : Utils.asList(type.getShopClass()), false).size() != amount;
		}
		return false;
	}

	// operation
	public static enum Operation {
		MORE_THAN,
		AT_LEAST,
		EQUALS,
		AT_MOST,
		LESS_THAN,
		DIFFERENT
	}

	// clone
	protected ConditionShopCount() {
		super();
	}

}
