package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionIsAdminLogic extends ConditionLogic<ConditionIsAdmin> {

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ConditionIsAdmin value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
	}

	@Override
	public ConditionIsAdmin readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionIsAdmin condition = new ConditionIsAdmin(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		return condition;
	}

	// create procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return false;
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return false;
	}

	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return false;
	}

	@Override
	public void startCreateProcedure(Player player, boolean sendInputMessage, String nextId, Parseable parent, GUI fromGUI, ConditionLogic<ConditionIsAdmin>.DoneCallback callback) {
		throw new UnsupportedOperationException();
		//callback.callback(new ConditionIsAdmin(nextId, parent, false, -1, EditorGUI.ICON_CONDITION, null));
	}

}
