package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.black_ixx.playerpoints.PlayerPoints;
import org.black_ixx.playerpoints.PlayerPointsAPI;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPEnum;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;

public class ConditionPlayerPointsPoints extends Condition {

	// base
	private PPEnum<Operation> operation = addComponent(new PPEnum<Operation>("operation", this, Operation.AT_LEAST.name(), Operation.class, "operation", false, 9, EditorGUI.ICON_ENUM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_OPERATIONLORE.getLines()));
	private PPInteger points = addComponent(new PPInteger("points", this, "1", 0, Integer.MAX_VALUE, false, 10, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_PLAYERHUNGERLORE.getLines()));

	public ConditionPlayerPointsPoints(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.PLAYERPOINTS_POINTS, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPEnum<Operation> getOperation() {
		return operation;
	}

	public Operation getOperation(Player parser) {
		return operation.getParsedValue(parser);
	}

	public PPInteger getPoints() {
		return points;
	}

	public Integer getPoints(Player parser) {
		return points.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERPOINTSPOINTS.getLine("{operation}", getOperation(parser).name().toLowerCase().replace("_", " "), "{amount}", getPoints(parser));
	}

	@Override
	public boolean isValid(Player player) throws UnsupportedOperationException {
		Operation operation = getOperation(player);
		Integer points = getPoints(player);
		PlayerPointsAPI api = ((PlayerPoints) Utils.getPlugin("PlayerPoints")).getAPI();
		if (operation == null || points == null || api == null) return false;
		if (operation.equals(Operation.MORE_THAN)) {
			return api.look(player.getUniqueId()) > points;
		} else if (operation.equals(Operation.AT_LEAST)) {
			return api.look(player.getUniqueId()) >= points;
		} else if (operation.equals(Operation.EQUALS)) {
			return api.look(player.getUniqueId()) == points;
		} else if (operation.equals(Operation.AT_MOST)) {
			return api.look(player.getUniqueId()) <= points;
		} else if (operation.equals(Operation.LESS_THAN)) {
			return api.look(player.getUniqueId()) < points;
		} else if (operation.equals(Operation.DIFFERENT)) {
			return api.look(player.getUniqueId()) != points;
		}
		return false;
	}

	// operation
	public static enum Operation {
		MORE_THAN,
		AT_LEAST,
		EQUALS,
		AT_MOST,
		LESS_THAN,
		DIFFERENT
	}

	// clone
	protected ConditionPlayerPointsPoints() {
		super();
	}

}
