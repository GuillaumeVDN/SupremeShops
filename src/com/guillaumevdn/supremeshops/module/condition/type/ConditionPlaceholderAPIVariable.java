package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPEnum;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPString;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;

public class ConditionPlaceholderAPIVariable extends Condition {

	// base
	private PPString variable = addComponent(new PPString("variable", this, null, true, 10, EditorGUI.ICON_STRING, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_VARIABLELORE.getLines()));
	private PPEnum<Operation> operation = addComponent(new PPEnum<Operation>("operation", this, "EQUALS", Operation.class, "operation", false, 11, EditorGUI.ICON_ENUM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_OPERATIONLORE.getLines()));
	private PPString value = addComponent(new PPString("value", this, null, true, 12, EditorGUI.ICON_STRING, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_VALUELORE.getLines()));
	private PPString description = addComponent(new PPString("description", this, null, false, 13, EditorGUI.ICON_STRING, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_PERMISSIONDESCRIPTIONLORE.getLines()));

	public ConditionPlaceholderAPIVariable(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.PLACEHOLDERAPI_VARIABLE, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPString getVariable() {
		return variable;
	}

	public String getVariable(Player parser) {
		return variable.getParsedValue(parser);
	}

	public PPEnum<Operation> getOperation() {
		return operation;
	}

	public Operation getOperation(Player parser) {
		return operation.getParsedValue(parser);
	}

	public PPString getValue() {
		return value;
	}

	public String getValue(Player parser) {
		return value.getParsedValue(parser);
	}

	public PPString getDescription() {
		return description;
	}

	public String getDescription(Player parser) {
		return description.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERPERMISSION.getLine("{description}", getDescription(parser));
	}

	@Override
	public boolean isValid(Player player) {
		// settings
		String variable = getVariable(player);
		Operation operation = getOperation(player);
		String value = getValue(player);
		if (variable == null || operation == null || value == null) return false;
		// check
		String current = Utils.fillPlaceholderAPI(player, variable);
		if (operation.equals(Operation.EQUALS)) {
			return value.equals(current);
		} else if (operation.equals(Operation.DIFFERENT)) {
			return !value.equals(current);
		} else if (operation.equals(Operation.LESS_THAN)) {
			try {
				double currentD = Double.parseDouble(current), valueD = Double.parseDouble(value);
				return currentD < valueD;
			} catch (NumberFormatException exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Could not check condition 'var " + variable + " " + operation.toString() + " " + value + "' ; invalid numbers (current value : " + current + ", required value = " + value + ")");
			}
		} else if (operation.equals(Operation.LESS_OR_EQUALS)) {
			try {
				double currentD = Double.parseDouble(current), valueD = Double.parseDouble(value);
				return currentD <= valueD;
			} catch (NumberFormatException exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Could not check condition 'var " + variable + " " + operation.toString() + " " + value + "' ; invalid numbers (current value : " + current + ", required value = " + value + ")");
			}
		} else if (operation.equals(Operation.MORE_OR_EQUALS)) {
			try {
				double currentD = Double.parseDouble(current), valueD = Double.parseDouble(value);
				return currentD >= valueD;
			} catch (NumberFormatException exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Could not check condition 'var " + variable + " " + operation.toString() + " " + value + "' ; invalid numbers (current value : " + current + ", required value = " + value + ")");
			}
		} else if (operation.equals(Operation.MORE_THAN)) {
			try {
				double currentD = Double.parseDouble(current), valueD = Double.parseDouble(value);
				return currentD > valueD;
			} catch (NumberFormatException exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("Could not check condition 'var " + variable + " " + operation.toString() + " " + value + "' ; invalid numbers (current value : " + current + ", required value = " + value + ")");
			}
		}

		// error
		return false;
	}

	public static enum Operation {
		LESS_THAN,// <
		LESS_OR_EQUALS,// <=
		EQUALS,// ==
		MORE_OR_EQUALS,// >=
		MORE_THAN,// >
		DIFFERENT,// !=
	}

	// clone
	protected ConditionPlaceholderAPIVariable() {
		super();
	}

}
