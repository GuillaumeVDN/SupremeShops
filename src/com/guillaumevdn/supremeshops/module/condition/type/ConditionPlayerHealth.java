package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPDouble;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPEnum;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;

public class ConditionPlayerHealth extends Condition {

	// base
	private PPEnum<Operation> operation = addComponent(new PPEnum<Operation>("operation", this, Operation.AT_LEAST.name(), Operation.class, "operation", false, 9, EditorGUI.ICON_ENUM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_OPERATIONLORE.getLines()));
	private PPDouble health = addComponent(new PPDouble("health", this, "1", 0d, Double.MAX_VALUE, false, 10, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_PLAYERHEALTHLORE.getLines()));

	public ConditionPlayerHealth(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.PLAYER_HEALTH, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPEnum<Operation> getOperation() {
		return operation;
	}

	public Operation getOperation(Player parser) {
		return operation.getParsedValue(parser);
	}

	public PPDouble getHealth() {
		return health;
	}

	public Double getHealth(Player parser) {
		return health.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERHEALTH.getLine("{operation}", getOperation(parser).name().toLowerCase().replace("_", " "), "{amount}", getHealth(parser));
	}

	@Override
	public boolean isValid(Player player) throws UnsupportedOperationException {
		Operation operation = getOperation(player);
		Double health = getHealth(player);
		if (operation == null || health == null) return false;
		if (operation.equals(Operation.MORE_THAN)) {
			return player.getHealth() > health;
		} else if (operation.equals(Operation.AT_LEAST)) {
			return player.getHealth() >= health;
		} else if (operation.equals(Operation.EQUALS)) {
			return player.getHealth() == health;
		} else if (operation.equals(Operation.AT_MOST)) {
			return player.getHealth() <= health;
		} else if (operation.equals(Operation.LESS_THAN)) {
			return player.getHealth() < health;
		} else if (operation.equals(Operation.DIFFERENT)) {
			return player.getHealth() != health;
		}
		return false;
	}

	// operation
	public static enum Operation {
		MORE_THAN,
		AT_LEAST,
		EQUALS,
		AT_MOST,
		LESS_THAN,
		DIFFERENT
	}

	// clone
	protected ConditionPlayerHealth() {
		super();
	}

}
