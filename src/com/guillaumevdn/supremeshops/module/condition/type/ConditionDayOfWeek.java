package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPEnum;
import com.guillaumevdn.gcorelegacy.lib.util.WeekDay;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;

public class ConditionDayOfWeek extends Condition {

	// base
	private PPEnum<WeekDay> startDay = addComponent(new PPEnum<WeekDay>("start_day", this, "MONDAY", WeekDay.class, "week day", false, 9, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMESTARTDAYLORE.getLines()));
	private PPEnum<WeekDay> endDay = addComponent(new PPEnum<WeekDay>("end_day", this, "FRIDAY", WeekDay.class, "week day", false, 10, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMEENDDAYLORE.getLines()));

	public ConditionDayOfWeek(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.DAY_OF_WEEK, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPEnum<WeekDay> getStartDay() {
		return startDay;
	}

	public WeekDay getStartDay(Player parser) {
		return startDay.getParsedValue(parser);
	}

	public PPEnum<WeekDay> getEndDay() {
		return endDay;
	}

	public WeekDay getEndDay(Player parser) {
		return endDay.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_DAYOFWEEK.getLine("{start}", getStartDay(parser).getText().getLine(), "{end}", getEndDay(parser).getText().getLine());
	}

	@Override
	public boolean isValid(Player player) throws UnsupportedOperationException {
		return isValid(player);
	}

	@Override
	public boolean isValid(Shop shop, Player player) throws UnsupportedOperationException {
		return check(player);
	}

	@Override
	public boolean isValid(Trigger trigger, Player parser) throws UnsupportedOperationException {
		return check(parser);
	}
	
	@Override
	public boolean isValid(Merchant merchant, Player parser) throws UnsupportedOperationException {
		return check(parser);
	}

	private boolean check(Player parser) {
		// get days
		WeekDay now = WeekDay.getCurrent();
		WeekDay start = getStartDay(parser);
		WeekDay end = getEndDay(parser);
		// start is before end
		if (start.ordinal() <= end.ordinal()) {
			return now.ordinal() >= start.ordinal() && now.ordinal() <= end.ordinal();
		}
		// end is before start
		else {
			return now.ordinal() >= start.ordinal() || now.ordinal() <= end.ordinal();
		}
	}

	// clone
	protected ConditionDayOfWeek() {
		super();
	}

}
