package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.Iterator;
import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.container.CPEnchantment;
import com.guillaumevdn.gcorelegacy.lib.parseable.container.CPItem;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.list.LPItem;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPEnum;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;

public class ConditionPlayerItems extends Condition {

	// base
	private PPEnum<Operation> operation = addComponent(new PPEnum<Operation>("operation", this, "HAS", Operation.class, "operation", false, 9, EditorGUI.ICON_ENUM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_GENERIC_OPERATIONLORE.getLines()));
	private LPItem items = addComponent(new LPItem("items", this, true, 10, EditorGUI.ICON_ITEM, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_ITEMSLORE.getLines()));
	private PPInteger itemsNeeded = addComponent(new PPInteger("items_needed", this, "100", 1, Integer.MAX_VALUE, false, 11, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_ITEMSNEEDEDLORE.getLines()));

	public ConditionPlayerItems(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.PLAYER_ITEMS, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPEnum<Operation> getOperation() {
		return operation;
	}

	public Operation getOperation(Player parser) {
		return operation.getParsedValue(parser);
	}

	public LPItem getItems() {
		return items;
	}

	public PPInteger getItemsNeeded() {
		return itemsNeeded;
	}

	public Integer getItemsNeeded(Player parser) {
		return itemsNeeded.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		if (items.getElements().size() <= 1) {
			CPItem item = getItems().getElements().get(getItems().getElements().keySet().iterator().next());
			String name = item.getName(parser);
			String description = Utils.round5(item.getAmount(parser)) + "x " + (name != null ? "\"" + name + "\"" : item.getType(parser));
			for (CPEnchantment enchant : item.getEnchants().getElements().values()) {
				description += " (" + enchant.getType(parser).getName() + " " + enchant.getLevel(parser) + ")";
			}
			return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERITEMSONE.getLine("{item}", description);
		} else {
			String description = "";
			Iterator<CPItem> iterator = getItems().getElements().values().iterator();
			while (iterator.hasNext()) {
				CPItem item = iterator.next();
				String name = item.getName(parser);
				description += Utils.round5(item.getAmount(parser)) + "x " + (name != null ? "\"" + name + "\"" : item.getType(parser));
				for (CPEnchantment enchant : item.getEnchants().getElements().values()) {
					description += " (" + enchant.getType(parser).getName() + " " + enchant.getLevel(parser) + ")";
				}
				if (iterator.hasNext()) description += ", ";
			}
			return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_PLAYERITEMSONE.getLine("{amount}", getItemsNeeded(parser), "{item}", description, "{operation}", operation.getParsedValue(parser).name().toLowerCase().replace("_", " "));
		}
	}

	@Override
	public boolean isValid(Player player) {
		Operation operation = getOperation(player);
		if (operation == null) return false;
		return operation.equals(items.contains(player, player, 10000) ? Operation.HAS : Operation.HAS_NOT);
	}

	// operation
	public static enum Operation {
		HAS, HAS_NOT
    }

	// clone
	protected ConditionPlayerItems() {
		super();
	}

}
