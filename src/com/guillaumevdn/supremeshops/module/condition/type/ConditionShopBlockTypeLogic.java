package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionShopBlockTypeLogic extends ConditionLogic<ConditionShopBlockType> {

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ConditionShopBlockType value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
		JsonUtils.writeJsonValue(out, "block_type", value.getBlockType().getValue());
	}

	@Override
	public ConditionShopBlockType readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionShopBlockType condition = new ConditionShopBlockType(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		condition.getBlockType().setValue(JsonUtils.readJsonValue(in));
		return condition;
	}

	// add procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return false;
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return false;
	}

	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return false;
	}

	@Override
	public void startCreateProcedure(final Player player, final boolean sendInputMessage, final String nextId, final Parseable parent, final GUI fromGUI, final ConditionLogic<ConditionShopBlockType>.DoneCallback callback) {
		throw new UnsupportedOperationException();
		/*askLocation(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONBLOCKTYPEINPUT, fromGUI, new LocationSelectionCallback() {
			@Override
			public void onSelect(final Location location) {
				// cancel
				Mat mat = Mat.from(location.getBlock());
				if (mat.isAir()) {
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// create condition
				ConditionShopBlockType condition = new ConditionShopBlockType(nextId, parent, false, -1, EditorGUI.ICON_CONDITION, null);
				condition.getBlockType().setValue(Utils.asList(mat.toString()));
				// done
				callback.callback(condition);
			}
		});*/
	}

}
