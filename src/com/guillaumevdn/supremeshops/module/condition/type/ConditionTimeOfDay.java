package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;

public class ConditionTimeOfDay extends Condition {

	// base
	private PPInteger startHour = addComponent(new PPInteger("start_hour", this, "16", 0, 23, false, 9, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMESTARTHOURLORE.getLines()));
	private PPInteger startMinute = addComponent(new PPInteger("start_minute", this, "45", 0, 59, false, 10, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMESTARTMINUTELORE.getLines()));
	private PPInteger endHour = addComponent(new PPInteger("end_hour", this, "17", 0, 23, false, 11, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMEENDHOURLORE.getLines()));
	private PPInteger endMinute = addComponent(new PPInteger("end_minute", this, "45", 0, 59, false, 12, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMEENDMINUTELORE.getLines()));

	public ConditionTimeOfDay(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.TIME_OF_DAY, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPInteger getStartHour() {
		return startHour;
	}

	public Integer getStartHour(Player parser) {
		return startHour.getParsedValue(parser);
	}

	public PPInteger getStartMinute() {
		return startMinute;
	}

	public Integer getStartMinute(Player parser) {
		return startMinute.getParsedValue(parser);
	}

	public PPInteger getEndHour() {
		return endHour;
	}

	public Integer getEndHour(Player parser) {
		return endHour.getParsedValue(parser);
	}

	public PPInteger getEndMinute() {
		return endMinute;
	}

	public Integer getEndMinute(Player parser) {
		return endMinute.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_TIMEOFDAY.getLine("{start_hour}", getStartHour(parser), "{start_minute}", getStartMinute(parser), "{end_hour}", getEndHour(parser), "{end_minute}", getEndMinute(parser));
	}

	@Override
	public boolean isValid(Player player) {
		return check(player);
	}

	@Override
	public boolean isValid(Shop shop, Player player) throws UnsupportedOperationException {
		return check(player);
	}

	@Override
	public boolean isValid(Trigger trigger, Player parser) throws UnsupportedOperationException {
		return check(parser);
	}

	@Override
	public boolean isValid(Merchant merchant, Player parser) throws UnsupportedOperationException {
		return check(parser);
	}

	public boolean check(Player parser) {
		Calendar calendar = GCoreLegacy.inst().getCalendarInstance();
		int year = calendar.get(Calendar.YEAR);
		int month = calendar.get(Calendar.MONTH);
		int date = calendar.get(Calendar.DATE);
		Date start = new Date(year, month, date, getStartHour(parser), getStartMinute(parser));
		Date end = new Date(year, month, date, getEndHour(parser), getEndMinute(parser));
		Date now = new Date();
		return now.after(start) && now.before(end);
	}

	// clone
	protected ConditionTimeOfDay() {
		super();
	}

}
