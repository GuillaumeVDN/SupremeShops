package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionDayOfMonthLogic extends ConditionLogic<ConditionDayOfMonth> {

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ConditionDayOfMonth value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
		JsonUtils.writeJsonValue(out, "start_day", value.getStartDay().getValue());
		JsonUtils.writeJsonValue(out, "end_day", value.getEndDay().getValue());
	}

	@Override
	public ConditionDayOfMonth readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionDayOfMonth condition = new ConditionDayOfMonth(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		condition.getStartDay().setValue(JsonUtils.readJsonValue(in));
		condition.getEndDay().setValue(JsonUtils.readJsonValue(in));
		return condition;
	}

	// add procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_DAY_OF_MONTH.has(player);
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_DAY_OF_MONTH.has(player);
	}

	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return true;
	}

	@Override
	public void startCreateProcedure(final Player player, final boolean sendInputMessage, final String nextId, final Parseable parent, final GUI fromGUI, final ConditionLogic<ConditionDayOfMonth>.DoneCallback callback) {
		askNumber(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONSTARTDAYINPUT, 1, 31, fromGUI, new NumberSelectionCallback() {
			@Override
			public void onSelect(final int startDay) {
				askNumber(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONENDDAYINPUT, 1, 31, fromGUI, new NumberSelectionCallback() {
					@Override
					public void onSelect(final int endDay) {
						// create condition
						ConditionDayOfMonth condition = new ConditionDayOfMonth(nextId, parent, false, -1, EditorGUI.ICON_CONDITION, null);
						condition.getStartDay().setValue(Utils.asList("" + startDay));
						condition.getEndDay().setValue(Utils.asList("" + endDay));
						// done
						callback.callback(condition);
					}
				});
			}
		});
	}

}
