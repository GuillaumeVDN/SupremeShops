package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.World;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.Locatable;

public class ConditionGameTime extends Condition {

	// base
	private PPInteger startTicks = addComponent(new PPInteger("start_ticks", this, "12000", 0, 24000, false, 9, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMESTARTTICKSLORE.getLines()));
	private PPInteger endTicks = addComponent(new PPInteger("end_ticks", this, "14000", 0, 24000, false, 10, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMEENDTICKSLORE.getLines()));

	public ConditionGameTime(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.GAME_TIME, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPInteger getStartTicks() {
		return startTicks;
	}

	public Integer getStartTicks(Player parser) {
		return startTicks.getParsedValue(parser);
	}

	public PPInteger getEndTicks() {
		return endTicks;
	}

	public Integer getEndTicks(Player parser) {
		return endTicks.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_GAMETIME.getLine("{start}", getStartTicks(parser), "{end}", getEndTicks(parser));
	}

	@Override
	public boolean isValid(Player player) throws UnsupportedOperationException {
		return isValid(player.getWorld(), player);
	}

	@Override
	public boolean isValid(Shop shop, Player player) throws UnsupportedOperationException {
		if (!(shop instanceof Locatable)) {
			throw new UnsupportedOperationException();
		}
		return isValid(((Locatable) shop).getBlock().getWorld(), player);// because we might be checking the condition from a remote shop, and the player might be in a different world
	}

	@Override
	public boolean isValid(Merchant merchant, Player parser) throws UnsupportedOperationException {
		if (!(merchant instanceof Locatable)) {
			throw new UnsupportedOperationException();
		}
		return isValid(((Locatable) merchant).getLocation().getWorld(), parser);
	}

	public boolean isValid(World world, Player parser) {
		long now = world.getTime();// 0 to 24000
		long start = getStartTicks(parser);
		long end = getEndTicks(parser);
		if (end < start) {// going through 2 mc 'tick days' (end is after start, for example start: 23000 / end: 7000)
			return now > start || now < end;
		} else {// during a mc 'tick day' (for example start: 7000 / end: 23000)
			return now > start && now < end;
		}
	}

	// clone
	protected ConditionGameTime() {
		super();
	}

}
