package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.container.CPItem;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.ChatInput;
import com.guillaumevdn.gcorelegacy.lib.util.input.ItemInput;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonToken;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.ItemListsUtils;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionPlayerItemsLogic extends ConditionLogic<ConditionPlayerItems> {

	// json
	private ItemData.GsonAdapter itemAdapter = new ItemData.GsonAdapter();

	@Override
	public void writeJsonProperties(JsonWriter out, ConditionPlayerItems value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
		// operation
		JsonUtils.writeJsonValue(out, "operation", value.getOperation().getValue());
		// items
		out.name("items").beginObject();
		for (CPItem item : value.getItems().getElements().values()) {
			out.name(item.getId()).value(itemAdapter.toJson(item.getParsedValue(null)));
		}
		out.endObject();
		// items needed
		JsonUtils.writeJsonValue(out, "items_needed", value.getItemsNeeded().getValue());
	}

	@Override
	public ConditionPlayerItems readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionPlayerItems condition = new ConditionPlayerItems(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		// operation
		condition.getOperation().setValue(JsonUtils.readJsonValue(in));
		// items
		in.nextName();
		in.beginObject();
		while (!in.peek().equals(JsonToken.END_OBJECT)) {
			CPItem item = new CPItem(in.nextName(), condition, false, -1, EditorGUI.ICON_ITEM, null);
			item.replace(itemAdapter.fromJson(in.nextString()), null);
		}
		in.endObject();
		// items needed
		condition.getItemsNeeded().setValue(JsonUtils.readJsonValue(in));
		return condition;
	}

	// create procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_PLAYER_ITEMS.has(player);
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_PLAYER_ITEMS.has(player);
	}

	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return true;
	}

	private Map<Player, List<ItemData>> askingItems = new HashMap<Player, List<ItemData>>();

	@Override
	public void startCreateProcedure(final Player player, final boolean sendInputMessage, final String nextId, final Parseable parent, final GUI fromGUI, final ConditionLogic<ConditionPlayerItems>.DoneCallback callback) {
		// ask items
		askingItems.put(player, new ArrayList<ItemData>());
		keepAskingItems(player, fromGUI);
		// chat control
		GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
			@Override
			public void onChat(final Player player, final String permission) {
				if (!askingItems.containsKey(player)) return;
				// cancel
				String unformat = Utils.unformat(permission.trim().toLowerCase());
				if (unformat.equals("cancel")) {
					askingItems.remove(player);
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// stop
				if (unformat.equals("stop")) {
					// empty list
					final List<ItemData> items = askingItems.remove(player);
					if (items.isEmpty()) {
						if (fromGUI != null) fromGUI.open(player);
					}
					// has items
					else {
						// ask operation
						askEnumValue(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONITEMSOPERATIONINPUT, fromGUI, ConditionPlayerItems.Operation.class, new EnumSelectionCallback<ConditionPlayerItems.Operation>() {
							@Override
							public void onSelect(final ConditionPlayerItems.Operation operation) {
								// ask items needed
								askNumber(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONITEMSNEEDEDINPUT, 1, Integer.MAX_VALUE, fromGUI, new NumberSelectionCallback() {
									@Override
									public void onSelect(final int itemsNeeded) {
										// create condition
										ConditionPlayerItems condition = new ConditionPlayerItems(nextId, parent, false, -1, EditorGUI.ICON_CONDITION, null);
										for (int i = 0; i < items.size(); ++i) {
											CPItem cpItem = new CPItem("" + (i + 1), condition.getItems(), false, -1, EditorGUI.ICON_ITEM, null);
											cpItem.replace(items.get(i), player);
											condition.getItems().addElement(cpItem);
										}
										condition.getOperation().setValue(Utils.asList(operation.name()));
										condition.getItemsNeeded().setValue(Utils.asList("" + itemsNeeded));
										// done
										callback.callback(condition);
									}
								});
							}
						});
					}
				}
			}
		});
	}

	private void keepAskingItems(Player player, final GUI fromGUI) {
		player.closeInventory();
		SSLocale.MSG_SUPREMESHOPS_CONDITIONITEMSINPUT.send(player);
		GCoreLegacy.inst().getItemInputs().put(player, new ItemInput() {
			@Override
			public void onChoose(Player player, ItemStack value) {
				if (!askingItems.containsKey(player)) return;
				// invalid item or not allowed
				if (value == null || Mat.fromItem(value).isAir() || !ItemListsUtils.isItemAllowed(value)) {
					SSLocale.MSG_SUPREMESHOPS_CREATEITEMINVALID.send(player);
					keepAskingItems(player, fromGUI);
					return;
				}
				// add and keep asking
				askingItems.get(player).add(new ItemData(value));
				SSLocale.MSG_SUPREMESHOPS_CONDITIONITEMSADDED.send(player);
				keepAskingItems(player, fromGUI);
			}
		});
	}

}
