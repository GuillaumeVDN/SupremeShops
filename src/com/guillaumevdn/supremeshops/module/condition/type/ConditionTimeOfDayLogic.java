package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.messenger.Text;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.ChatInput;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionTimeOfDayLogic extends ConditionLogic<ConditionTimeOfDay> {

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ConditionTimeOfDay value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
		JsonUtils.writeJsonValue(out, "start_hour", value.getStartHour().getValue());
		JsonUtils.writeJsonValue(out, "start_minute", value.getStartMinute().getValue());
		JsonUtils.writeJsonValue(out, "end_hour", value.getEndHour().getValue());
		JsonUtils.writeJsonValue(out, "end_minute", value.getEndMinute().getValue());
	}

	@Override
	public ConditionTimeOfDay readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionTimeOfDay condition = new ConditionTimeOfDay(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		condition.getStartHour().setValue(JsonUtils.readJsonValue(in));
		condition.getStartMinute().setValue(JsonUtils.readJsonValue(in));
		condition.getEndHour().setValue(JsonUtils.readJsonValue(in));
		condition.getEndMinute().setValue(JsonUtils.readJsonValue(in));
		return condition;
	}

	// add procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_TIME_OF_DAY.has(player);
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_TIME_OF_DAY.has(player);
	}

	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return true;
	}

	@Override
	public void startCreateProcedure(final Player player, final boolean sendInputMessage, final String nextId, final Parseable parent, final GUI fromGUI, final ConditionLogic<ConditionTimeOfDay>.DoneCallback callback) {
		// ask for start time
		askForTime(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONSTARTTIMEINPUT, fromGUI, new TimeSelectionCallback() {
			@Override
			public void onSelect(final int startHour, final int startMinute) {
				// ask for end time
				askForTime(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONENDTIMEINPUT, fromGUI, new TimeSelectionCallback() {
					@Override
					public void onSelect(final int endHour, final int endMinute) {
						// create condition	
						ConditionTimeOfDay condition = new ConditionTimeOfDay(nextId, parent, false, -1, EditorGUI.ICON_CONDITION, null);
						condition.getStartHour().setValue(Utils.asList("" + startHour));
						condition.getStartMinute().setValue(Utils.asList("" + startMinute));
						condition.getEndHour().setValue(Utils.asList("" + endHour));
						condition.getEndMinute().setValue(Utils.asList("" + endHour));
						// done
						callback.callback(condition);
					}
				});
			}
		});
	}

	private void askForTime(final Player player, final Text message, final GUI fromGUI, final TimeSelectionCallback callback) {
		message.send(player);
		player.closeInventory();
		GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
			@Override
			public void onChat(Player player, String value) {
				// cancel
				if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// invalid format
				int hour, minute;
				try {
					String[] split = value.split(":");
					hour = Integer.parseInt(split[0]);
					minute = Integer.parseInt(split[1]);
					if (hour < 0 || hour > 23 || minute < 0 || minute > 53) throw new Error();
				} catch (Throwable ignored) {
					askForTime(player, message, fromGUI, callback);
					return;
				}
				// callback
				callback.onSelect(hour, minute);
			}
		});
	}

	private interface TimeSelectionCallback {
		void onSelect(int hour, int minute);
	}

}
