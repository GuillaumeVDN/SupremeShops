package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionConditionCountLogic extends ConditionLogic<ConditionConditionCount> {

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ConditionConditionCount value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
		JsonUtils.writeJsonValue(out, "condition_type", value.getConditionType().getValue());
		JsonUtils.writeJsonValue(out, "amount", value.getAmount().getValue());
	}

	@Override
	public ConditionConditionCount readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionConditionCount condition = new ConditionConditionCount(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		condition.getConditionType().setValue(JsonUtils.readJsonValue(in));
		condition.getAmount().setValue(JsonUtils.readJsonValue(in));
		return condition;
	}

	// add procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return false;
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return false;
	}
	
	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return false;
	}

	@Override
	public void startCreateProcedure(final Player player, final boolean sendInputMessage, final String nextId, final Parseable parent, final GUI fromGUI, final ConditionLogic<ConditionConditionCount>.DoneCallback callback) {
		throw new UnsupportedOperationException();
		/*askFakeEnumValue(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONTYPEINPUT, fromGUI, ConditionType.values(), ConditionType.class, new FakeEnumSelectionCallback<ConditionType>() {
			@Override
			public void onSelect(final ConditionType type) {
				SSLocale.MSG_SUPREMESHOPS_CONDITIONCONDITIONAMOUNTINPUT.send(player);
				GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
					@Override
					public void onChat(final Player player, String value) {
						// cancel
						if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
							if (fromGUI != null) fromGUI.open(player);
							return;
						}
						// not a number
						final Integer amount = Utils.integerOrNull(value);
						if (amount == null || amount <= 0d) {
							GLocale.MSG_GENERIC_COMMAND_INVALIDINTPARAM.send(player, "{plugin}", SupremeShops.inst().getName(), "{parameter}", value);
							if (fromGUI != null) fromGUI.open(player);
							return;
						}
						// create condition
						ConditionConditionCount condition = new ConditionConditionCount(nextId, parent, false, -1, EditorGUI.ICON_CONDITION, null);
						condition.getConditionType().setValue(Utils.asList(type.getId()));
						condition.getAmount().setValue(Utils.asList("" + amount));
						// done
						callback.callback(condition);
					}
				});
			}
		});*/
	}

}
