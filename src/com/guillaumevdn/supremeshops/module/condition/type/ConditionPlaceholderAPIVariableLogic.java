package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionPlaceholderAPIVariableLogic extends ConditionLogic<ConditionPlaceholderAPIVariable> {

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ConditionPlaceholderAPIVariable value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
		JsonUtils.writeJsonValue(out, "variable", value.getVariable().getValue());
		JsonUtils.writeJsonValue(out, "operation", value.getOperation().getValue());
		JsonUtils.writeJsonValue(out, "value", value.getValue().getValue());
		JsonUtils.writeJsonValue(out, "description", value.getDescription().getValue());
	}

	@Override
	public ConditionPlaceholderAPIVariable readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionPlaceholderAPIVariable condition = new ConditionPlaceholderAPIVariable(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		condition.getVariable().setValue(JsonUtils.readJsonValue(in));
		condition.getOperation().setValue(JsonUtils.readJsonValue(in));
		condition.getValue().setValue(JsonUtils.readJsonValue(in));
		condition.getDescription().setValue(JsonUtils.readJsonValue(in));
		return condition;
	}

	// add procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_PLACEHOLDERAPIVARIABLE.has(player);
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_PLACEHOLDERAPIVARIABLE.has(player);
	}

	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return true;
	}

	@Override
	public void startCreateProcedure(final Player player, final boolean sendInputMessage, final String nextId, final Parseable parent, final GUI fromGUI, final ConditionLogic<ConditionPlaceholderAPIVariable>.DoneCallback callback) {
		askString(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONVARIABLEINPUT, fromGUI, new StringSelectionCallback() {
			@Override
			public void onSelect(final String variable) {
				askEnumValue(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONOPERATIONINPUT, fromGUI, ConditionPlaceholderAPIVariable.Operation.class, new EnumSelectionCallback<ConditionPlaceholderAPIVariable.Operation>() {
					@Override
					public void onSelect(final ConditionPlaceholderAPIVariable.Operation operation) {
						askString(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONVARIABLEVALUEINPUT, fromGUI, new StringSelectionCallback() {
							@Override
							public void onSelect(final String value) {
								askString(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONVARIABLEDESCRIPTIONINPUT, fromGUI, new StringSelectionCallback() {
									@Override
									public void onSelect(final String description) {
										// create condition
										ConditionPlaceholderAPIVariable condition = new ConditionPlaceholderAPIVariable(nextId, parent, false, -1, EditorGUI.ICON_CONDITION, null);
										condition.getVariable().setValue(Utils.asList(variable));
										condition.getOperation().setValue(Utils.asList(operation.name()));
										condition.getValue().setValue(Utils.asList(value));
										condition.getDescription().setValue(Utils.asList(description));
										// done
										callback.callback(condition);
									}
								});
							}
						});
					}
				});
			}
		});
	}

}
