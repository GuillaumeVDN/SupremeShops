package com.guillaumevdn.supremeshops.module.condition.type;

import java.io.IOException;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.module.condition.ConditionLogic;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.JsonUtils;

public class ConditionGameTimeLogic extends ConditionLogic<ConditionGameTime> {

	// json
	@Override
	public void writeJsonProperties(JsonWriter out, ConditionGameTime value) throws IOException {
		if (value.getErrorMessage().getValue() != null) JsonUtils.writeJsonValue(out, "error_message", value.getErrorMessage().getValue());
		JsonUtils.writeJsonValue(out, "start_ticks", value.getStartTicks().getValue());
		JsonUtils.writeJsonValue(out, "end_ticks", value.getEndTicks().getValue());
	}

	@Override
	public ConditionGameTime readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException {
		ConditionGameTime condition = new ConditionGameTime(id, parent, false, -1, EditorGUI.ICON_CONDITION, null);
		condition.getErrorMessage().setValue(JsonUtils.readJsonValueIfName("error_message", in));
		condition.getStartTicks().setValue(JsonUtils.readJsonValue(in));
		condition.getEndTicks().setValue(JsonUtils.readJsonValue(in));
		return condition;
	}

	// add procedure
	@Override
	public boolean canAddToShopTradeConditions(Player player, Shop shop) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_GAME_TIME.has(player);
	}

	@Override
	public boolean canAddToMerchantInteractConditions(Player player, Merchant merchant) {
		return SSPerm.SUPREMESHOPS_CREATE_CONDITION_GAME_TIME.has(player);
	}

	@Override
	public boolean canAddToRentableRentConditions(Player player, Rentable rentable) {
		return true;
	}

	@Override
	public void startCreateProcedure(final Player player, final boolean sendInputMessage, final String nextId, final Parseable parent, final GUI fromGUI, final ConditionLogic<ConditionGameTime>.DoneCallback callback) {
		askNumber(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONSTARTTICKSINPUT, 0, 24000, fromGUI, new NumberSelectionCallback() {
			@Override
			public void onSelect(final int startTicks) {
				askNumber(player, SSLocale.MSG_SUPREMESHOPS_CONDITIONENDTICKSINPUT, 0, 24000, fromGUI, new NumberSelectionCallback() {
					@Override
					public void onSelect(final int endTicks) {
						// create condition
						ConditionGameTime condition = new ConditionGameTime(nextId, parent, false, -1, EditorGUI.ICON_CONDITION, null);
						condition.getStartTicks().setValue(Utils.asList("" + startTicks));
						condition.getEndTicks().setValue(Utils.asList("" + endTicks));
						// done
						callback.callback(condition);
					}
				});
			}
		});
	}

}
