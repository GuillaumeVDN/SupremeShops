package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.Calendar;
import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;

public class ConditionDayOfMonth extends Condition {

	// base
	private PPInteger startDay = addComponent(new PPInteger("start_day", this, "5", 1, 31, false, 9, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMESTARTDAYLORE.getLines()));
	private PPInteger endDay = addComponent(new PPInteger("end_day", this, "10", 1, 31, false, 10, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMEENDDAYLORE.getLines()));

	public ConditionDayOfMonth(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.DAY_OF_MONTH, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPInteger getStartDay() {
		return startDay;
	}

	public Integer getStartDay(Player parser) {
		return startDay.getParsedValue(parser);
	}

	public PPInteger getEndDay() {
		return endDay;
	}

	public Integer getEndDay(Player parser) {
		return endDay.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_DAYOFMONTH.getLine("{start}", getStartDay(parser), "{end}", getEndDay(parser));
	}

	@Override
	public boolean isValid(Player player) throws UnsupportedOperationException {
		return isValid(player);
	}

	@Override
	public boolean isValid(Shop shop, Player player) throws UnsupportedOperationException {
		return check(player);
	}

	@Override
	public boolean isValid(Trigger trigger, Player parser) throws UnsupportedOperationException {
		return check(parser);
	}

	@Override
	public boolean isValid(Merchant merchant, Player parser) throws UnsupportedOperationException {
		return check(parser);
	}

	private boolean check(Player parser) {
		// get days
		Integer now = Calendar.getInstance().get(Calendar.DAY_OF_MONTH);
		Integer start = getStartDay(parser);
		Integer end = getEndDay(parser);
		// start is before end
		if (start <= end) {
			return now >= start && now <= end;
		}
		// end is before start
		else {
			return now >= start || now <= end;
		}
	}

	// clone
	protected ConditionDayOfMonth() {
		super();
	}

}
