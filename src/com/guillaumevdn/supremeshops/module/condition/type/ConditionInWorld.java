package com.guillaumevdn.supremeshops.module.condition.type;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPString;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.Locatable;

public class ConditionInWorld extends Condition {

	// base
	private PPString world = addComponent(new PPString("world", this, null, true, 10, EditorGUI.ICON_STRING, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_CONDITION_WORLDNAMELORE.getLines()));

	public ConditionInWorld(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, ConditionType.IN_WORLD, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPString getWorld() {
		return world;
	}

	public String getWorld(Player parser) {
		return world.getParsedValue(parser);
	}

	// methods
	@Override
	public String describe(Player parser) {
		return SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONDESCRIBE_INWORLD.getLine("{world}", getWorld(parser));
	}

	@Override
	public boolean isValid(Player player) throws UnsupportedOperationException {
		String world = getWorld(player);
		if (world == null) return false;
		return player.getWorld().getName().equalsIgnoreCase(world);
	}

	@Override
	public boolean isValid(Shop shop, Player player) throws UnsupportedOperationException {
		if (!(shop instanceof Locatable)) {
			throw new UnsupportedOperationException();
		}
		String world = getWorld(player);
		if (world == null) return false;
		return ((Locatable) shop).getBlock().getWorld().getName().equalsIgnoreCase(world);
	}

	@Override
	public boolean isValid(Merchant merchant, Player parser) throws UnsupportedOperationException {
		if (!(merchant instanceof Locatable)) {
			throw new UnsupportedOperationException();
		}
		String world = getWorld(parser);
		if (world == null) return false;
		return ((Locatable) merchant).getLocation().getWorld().getName().equalsIgnoreCase(world);
	}
	
	public static enum Operation {
		LESS_THAN,// <
		LESS_OR_EQUALS,// <=
		EQUALS,// ==
		MORE_OR_EQUALS,// >=
		MORE_THAN,// >
		DIFFERENT,// !=
	}

	// clone
	protected ConditionInWorld() {
		super();
	}
}
