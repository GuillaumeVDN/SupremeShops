package com.guillaumevdn.supremeshops.module.condition;

import java.io.IOException;
import java.util.Collection;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.messenger.Text;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.Wrapper;
import com.guillaumevdn.gcorelegacy.lib.util.input.ChatInput;
import com.guillaumevdn.gcorelegacy.lib.util.input.LocationInput;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.merchant.fake.FakeMerchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.shop.fake.FakeShop;

// LOGIC AGEEEEEEEEEEEEEEEENT
public abstract class ConditionLogic<T extends Condition> {

	// json
	public abstract void writeJsonProperties(JsonWriter out, T value) throws IOException;
	public abstract T readJsonPropertiesAndBuild(JsonReader in, Parseable parent, String id) throws IOException;

	// add procedure
	public abstract boolean canAddToShopTradeConditions(Player player, Shop shop);
	public abstract boolean canAddToMerchantInteractConditions(Player player, Merchant merchant);
	public abstract boolean canAddToRentableRentConditions(Player player, Rentable rentable);
	public abstract void startCreateProcedure(Player player, boolean sendInputMessage, String nextId, Parseable parent, GUI fromGUI, DoneCallback callback);

	public abstract class DoneCallback {
		public abstract void callback(T condition);
	}

	public void attemptToAddToShopTradeConditions(final Player player, final Shop shop, final GUI fromGUI) {
		if (!canAddToShopTradeConditions(player, shop)) {
			throw new UnsupportedOperationException();
		}
		startCreateProcedure(player, true, shop.getTradeConditions().getConditions().getNextId(), shop.getTradeConditions().getConditions(), fromGUI, new DoneCallback() {
			@Override
			public void callback(T condition) {
				// not allowed
				Shop fake = shop.asFake();
				if (fake != null) {
					((FakeShop) fake).withTradeCondition(condition);
					if (!SupremeShops.inst().getModuleManager().canShopExist(player, fake, true)) {
						if (fromGUI != null) fromGUI.open(player);
						return;
					}
				}
				// add condition
				shop.addTradeCondition(condition);
				// sound
				if (SupremeShops.inst().getModuleManager().getObjectAddSound() != null) {
					SupremeShops.inst().getModuleManager().getObjectAddSound().play(player);
				}
				// send message and open from GUI
				SSLocale.MSG_SUPREMESHOPS_CREATESHOPTRADECONDITIONADDED.send(player);
				if (fromGUI != null) fromGUI.open(player);
				// log
				SupremeShops.inst().pluginLog(shop, null, null, player, null, "Added trade condition " + condition.getId() + " (" + condition.describe(player) + ")");
			}
		});
	}

	public void attemptToAddToMerchantInteractConditions(final Player player, final Merchant merchant, final GUI fromGUI) {
		if (!canAddToMerchantInteractConditions(player, merchant)) {
			throw new UnsupportedOperationException();
		}
		startCreateProcedure(player, true, merchant.getInteractConditions().getConditions().getNextId(), merchant.getInteractConditions().getConditions(), fromGUI, new DoneCallback() {
			@Override
			public void callback(T condition) {
				// not allowed
				Merchant fake = merchant.asFake();
				((FakeMerchant) fake).withInteractCondition(condition);
				if (!SupremeShops.inst().getModuleManager().canMerchantExist(player, fake, true)) {
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// add condition
				merchant.addInteractCondition(condition);
				// sound
				if (SupremeShops.inst().getModuleManager().getObjectAddSound() != null) {
					SupremeShops.inst().getModuleManager().getObjectAddSound().play(player);
				}
				// send message and open from GUI
				SSLocale.MSG_SUPREMESHOPS_CREATEMERCHANTINTERACTIONCONDITIONADDED.send(player);
				if (fromGUI != null) fromGUI.open(player);
				// log
				SupremeShops.inst().pluginLog(null, merchant, null, player, null, "Added interact condition " + condition.getId() + " (" + condition.describe(player) + ")");
			}
		});
	}

	public void attemptToAddToRentableRentConditions(final Player player, final Rentable rentable, final GUI fromGUI) {
		if (!canAddToRentableRentConditions(player, rentable)) {
			throw new UnsupportedOperationException();
		}
		startCreateProcedure(player, true, rentable.getRentConditions().getConditions().getNextId(), rentable.getRentConditions().getConditions(), fromGUI, new DoneCallback() {
			@Override
			public void callback(T condition) {
				// add condition
				rentable.addRentCondition(condition);
				// sound
				if (SupremeShops.inst().getModuleManager().getObjectAddSound() != null) {
					SupremeShops.inst().getModuleManager().getObjectAddSound().play(player);
				}
				// send message and open from GUI
				SSLocale.MSG_SUPREMESHOPS_CREATESHOPRENTCONDITIONADDED.send(player);
				if (fromGUI != null) fromGUI.open(player);
				// log
				SupremeShops.inst().pluginLog(rentable, player, null, "Added rent condition " + condition.getId() + " (" + condition.describe(player) + ")");
			}
		});
	}

	// utils
	public static <T extends Enum<T>> void askEnumValue(final Player player, final Text message, final GUI fromGUI, final Class<T> enumClazz, final EnumSelectionCallback<T> callback) {
		message.send(player);
		final Wrapper<Boolean> selected = new Wrapper<Boolean>(false);
		GUI gui = new GUI(SupremeShops.inst(), "Select a value", Utils.getInventorySize(enumClazz.getEnumConstants().length), GUI.SLOTS_0_TO_44) {
			@Override
			protected boolean onClose(List<Player> players, int pageIndex) {
				if (!selected.getValue() && fromGUI != null) {
					fromGUI.open(player);
				}
				return false;
			}
		};
		for (final T value : enumClazz.getEnumConstants()) {
			gui.setRegularItem(new ClickeableItem(new ItemData("value_" + value.name(), -1, EditorGUI.ICON_ENUM, 1, Utils.capitalizeUnderscores(value.name()), null)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					selected.setValue(true);
					player.closeInventory();
					callback.onSelect(value);
				}
			});
		}
		gui.open(player);
	}

	public interface EnumSelectionCallback<T extends Enum<T>> {
		void onSelect(T value);
	}

	public static <T> void askFakeEnumValue(final Player player, final Text message, final GUI fromGUI, final Collection<T> values, final Class<T> enumClazz, final FakeEnumSelectionCallback<T> callback) {
		message.send(player);
		final Wrapper<Boolean> selected = new Wrapper<Boolean>(false);
		GUI gui = new GUI(SupremeShops.inst(), "Select a value", Utils.getInventorySize(values.size()), GUI.SLOTS_0_TO_44) {
			@Override
			protected boolean onClose(List<Player> players, int pageIndex) {
				if (!selected.getValue() && fromGUI != null) {
					fromGUI.open(player);
				}
				return false;
			}
		};
		for (final T value : values) {
			gui.setRegularItem(new ClickeableItem(new ItemData("value_" + value.toString(), -1, EditorGUI.ICON_ENUM, 1, Utils.capitalizeUnderscores(value.toString()), null)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					selected.setValue(true);
					player.closeInventory();
					callback.onSelect(value);
				}
			});
		}
		gui.open(player);
	}

	public interface FakeEnumSelectionCallback<T> {
		void onSelect(T value);
	}

	public static void askNumber(final Player player, final Text message, final int min, final int max, final GUI fromGUI, final NumberSelectionCallback callback) {
		player.closeInventory();
		message.send(player);
		GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
			@Override
			public void onChat(Player player, String value) {
				// cancel
				if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// not a valid number
				Integer number = Utils.integerOrNull(value);
				if (number == null || number < min || number > max) {
					askNumber(player, message, min, max, fromGUI, callback);
					return;
				}
				// callback
				callback.onSelect(number);
			}
		});
	}

	public interface NumberSelectionCallback {
		void onSelect(int value);
	}

	public static void askDecimalNumber(final Player player, final Text message, final double min, final double max, final GUI fromGUI, final DecimalNumberSelectionCallback callback) {
		player.closeInventory();
		message.send(player);
		GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
			@Override
			public void onChat(Player player, String value) {
				// cancel
				if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// not a valid number
				Double number = Utils.doubleOrNull(value);
				if (number == null || Double.isInfinite(number) || Double.isNaN(number) || number < min || number > max) {
					askDecimalNumber(player, message, min, max, fromGUI, callback);
					return;
				}
				// callback
				callback.onSelect(number);
			}
		});
	}

	public interface DecimalNumberSelectionCallback {
		void onSelect(double value);
	}

	public static void askString(final Player player, final Text message, final GUI fromGUI, final StringSelectionCallback callback) {
		player.closeInventory();
		message.send(player);
		GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
			@Override
			public void onChat(Player player, String value) {
				// cancel
				if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
					if (fromGUI != null) fromGUI.open(player);
					return;
				}
				// callback
				callback.onSelect(value);
			}
		});
	}

	public interface StringSelectionCallback {
		void onSelect(String value);
	}

	public static void askLocation(final Player player, final Text message, final GUI fromGUI, final LocationSelectionCallback callback) {
		player.closeInventory();
		SSLocale.MSG_SUPREMESHOPS_CONDITIONBLOCKTYPEINPUT.send(player);
		GCoreLegacy.inst().getLocationInputs().put(player, new LocationInput() {
			@Override
			public void onChoose(Player player, Location value) {
				callback.onSelect(value);
			}
		});
	}

	public interface LocationSelectionCallback {
		void onSelect(Location value);
	}

}
