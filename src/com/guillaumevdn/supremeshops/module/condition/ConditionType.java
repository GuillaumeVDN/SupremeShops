package com.guillaumevdn.supremeshops.module.condition;

import java.lang.reflect.Constructor;
import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ConfigData;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.util.ServerVersion;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionConditionCount;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionConditionCountLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionDayOfMonth;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionDayOfMonthLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionDayOfWeek;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionDayOfWeekLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionGameTime;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionGameTimeLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionInWorld;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionInWorldLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionInWorldguardRegion;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionInWorldguardRegionLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionIsAdmin;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionIsAdminLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPermission;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPermissionLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlaceholderAPIVariable;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlaceholderAPIVariableLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlayerHealth;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlayerHealthLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlayerHunger;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlayerHungerLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlayerItems;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlayerItemsLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlayerMoney;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlayerMoneyLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlayerPointsPoints;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlayerPointsPointsLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlayerXpLevel;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionPlayerXpLevelLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionShopBlockType;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionShopBlockTypeLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionShopCount;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionShopCountLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionShopObjectCount;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionShopObjectCountLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionShopType;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionShopTypeLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionTimeOfDay;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionTimeOfDayLogic;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionTokenEnchantTokens;
import com.guillaumevdn.supremeshops.module.condition.type.ConditionTokenEnchantTokensLogic;

public final class ConditionType implements Comparable<ConditionType> {

	// special
	public static final ConditionType CONDITION_COUNT = registerType("CONDITION_COUNT", ConditionConditionCount.class, Mat.ENDER_CHEST, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_CONDITIONCOUNT.getLine(), new ConditionConditionCountLogic());
	public static final ConditionType DAY_OF_WEEK = registerType("DAY_OF_WEEK", ConditionDayOfWeek.class, Mat.CLOCK, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_DAYOFWEEK.getLine(), new ConditionDayOfWeekLogic());
	public static final ConditionType DAY_OF_MONTH = registerType("DAY_OF_MONTH", ConditionDayOfMonth.class, Mat.CLOCK, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_DAYOFMONTH.getLine(), new ConditionDayOfMonthLogic());
	public static final ConditionType GAME_TIME = registerType("GAME_TIME", ConditionGameTime.class, Mat.CLOCK, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_GAMETIME.getLine(), new ConditionGameTimeLogic());
	public static final ConditionType IN_WORLD = registerType("IN_WORLD", ConditionInWorld.class, Mat.RAIL, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_INWORLD.getLine(), new ConditionInWorldLogic());
	public static final ConditionType IN_WORLDGUARD_REGION = registerType("IN_WORLDGUARD_REGION", ConditionInWorldguardRegion.class, Mat.RAIL, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_INWORLDGUARDREGION.getLine(), new ConditionInWorldguardRegionLogic(), "WorldGuard");
	public static final ConditionType IS_ADMIN = registerType("IS_ADMIN", ConditionIsAdmin.class, Mat.COMMAND_BLOCK_MINECART, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_ISADMIN.getLine(), new ConditionIsAdminLogic());
	public static final ConditionType PERMISSION = registerType("PERMISSION", ConditionPermission.class, Mat.COMMAND_BLOCK, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_PERMISSION.getLine(), new ConditionPermissionLogic());
	public static final ConditionType PLACEHOLDERAPI_VARIABLE = registerType("PLACEHOLDERAPI_VARIABLE", ConditionPlaceholderAPIVariable.class, Mat.COMMAND_BLOCK, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_PLACEHOLDERAPIVARIABLE.getLine(), new ConditionPlaceholderAPIVariableLogic());
	public static final ConditionType PLAYER_HEALTH = registerType("PLAYER_HEALTH", ConditionPlayerHealth.class, Mat.RED_DYE, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERHEALTH.getLine(), new ConditionPlayerHealthLogic());
	public static final ConditionType PLAYER_HUNGER = registerType("PLAYER_HUNGER", ConditionPlayerHunger.class, Mat.MELON_SLICE, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERHUNGER.getLine(), new ConditionPlayerHungerLogic());
	public static final ConditionType PLAYER_ITEMS = registerType("PLAYER_ITEMS", ConditionPlayerItems.class, Mat.IRON_PICKAXE, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERITEMS.getLine(), new ConditionPlayerItemsLogic());
	public static final ConditionType PLAYER_MONEY = registerType("PLAYER_MONEY", ConditionPlayerMoney.class, Mat.EMERALD, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERMONEY.getLine(), new ConditionPlayerMoneyLogic());
	public static final ConditionType PLAYERPOINTS_POINTS = registerType("PLAYERPOINTS_POINTS", ConditionPlayerPointsPoints.class, Mat.GOLD_INGOT, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERPOINTSPOINTS.getLine(), new ConditionPlayerPointsPointsLogic(), "PlayerPoints");
	public static final ConditionType PLAYER_XP_LEVEL = registerType("PLAYER_XP_LEVEL", ConditionPlayerXpLevel.class, Mat.EXPERIENCE_BOTTLE, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_PLAYERXPLEVEL.getLine(), new ConditionPlayerXpLevelLogic());
	public static final ConditionType SHOP_COUNT = registerType("SHOP_COUNT", ConditionShopCount.class, Mat.ENDER_CHEST, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_SHOPCOUNT.getLine(), new ConditionShopCountLogic());
	public static final ConditionType SHOP_BLOCK_TYPE = registerType("SHOP_BLOCK_TYPE", ConditionShopBlockType.class, Mat.ENDER_CHEST, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_SHOPBLOCKTYPE.getLine(), new ConditionShopBlockTypeLogic());
	public static final ConditionType SHOP_OBJECT_COUNT = registerType("SHOP_OBJECT_COUNT", ConditionShopObjectCount.class, Mat.ENDER_CHEST, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_SHOPOBJECTCOUNT.getLine(), new ConditionShopObjectCountLogic());
	public static final ConditionType SHOP_TYPE = registerType("SHOP_TYPE", ConditionShopType.class, Mat.ENDER_CHEST, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_SHOPTYPE.getLine(), new ConditionShopTypeLogic());
	public static final ConditionType TIME_OF_DAY = registerType("TIME_OF_DAY", ConditionTimeOfDay.class, Mat.CLOCK, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_TIMEOFDAY.getLine(), new ConditionTimeOfDayLogic());
	public static final ConditionType TOKENENCHANT_TOKENS = registerType("TOKENENCHANT_TOKENS", ConditionTokenEnchantTokens.class, Mat.GOLD_INGOT, SSLocaleMisc.MISC_SUPREMESHOPS_CONDITIONTYPE_TOKENENCHANTTOKENS.getLine(), new ConditionTokenEnchantTokensLogic(), "TokenEnchant");

	// registration
	public static ConditionType registerType(String id, Class<? extends Condition> conditionClass, Mat icon, String name, ConditionLogic logic, String... requiredPlugins) {
		return registerType(id, conditionClass, icon, name, logic, ServerVersion.UNSUPPORTED, ServerVersion.HIGHEST, requiredPlugins);
	}

	public static ConditionType registerType(String id, Class<? extends Condition> conditionClass, Mat icon, String name, ConditionLogic logic, ServerVersion minVersion, ServerVersion maxVersion, String... requiredPlugins) {
		id = id.toUpperCase();
		ConditionType type = new ConditionType(id, conditionClass, icon, name, logic, minVersion, maxVersion, requiredPlugins);
		SupremeShops.inst().getModuleManager().getConditionTypes().put(id, type);
		return type;
	}

	public static ConditionType valueOf(String id) {
		return SupremeShops.inst().getModuleManager().getConditionTypes().get(id.toUpperCase());
	}

	public static List<ConditionType> values() {
		return Utils.asList(SupremeShops.inst().getModuleManager().getConditionTypes().values());
	}

	// base
	private String id;
	private Class<? extends Condition> conditionClass;
	private Mat icon;
	private String name;
	private ConditionLogic logic;
	private ServerVersion minVersion, maxVersion;
	private List<String> requiredPlugins;

	private ConditionType(String id, Class<? extends Condition> conditionClass, Mat icon, String name, ConditionLogic logic, String... requiredPlugins) {
		this(id, conditionClass, icon, name, logic, ServerVersion.UNSUPPORTED, ServerVersion.HIGHEST, requiredPlugins);
	}

	private ConditionType(String id, Class<? extends Condition> conditionClass, Mat icon, String name, ConditionLogic logic, ServerVersion minVersion, ServerVersion maxVersion, String... requiredPlugins) {
		this.id = id;
		this.conditionClass = conditionClass;
		this.icon = icon;
		this.name = name;
		this.logic = logic;
		this.minVersion = minVersion;
		this.maxVersion = maxVersion;
		this.requiredPlugins = requiredPlugins == null ? Utils.emptyList() : Utils.asList(requiredPlugins);
	}

	// get
	public String getId() {
		return id;
	}

	public Class<? extends Condition> getConditionClass() {
		return conditionClass;
	}

	public Mat getIcon() {
		return icon;
	}

	public String getName() {
		return name;
	}

	public ConditionLogic getLogic() {
		return logic;
	}

	public ServerVersion getMinVersion() {
		return minVersion;
	}

	public ServerVersion getMaxVersion() {
		return maxVersion;
	}

	public List<String> getRequiredPlugins() {
		return requiredPlugins;
	}

	// methods
	public boolean hasAllRequiredPlugins() {
		for (String plugin : requiredPlugins) {
			if (!Utils.isPluginEnabled(plugin)) {
				return false;
			}
		}
		return true;
	}

	/**
	 * Unregister the condition type
	 * @return null
	 */
	public ConditionType unregister() {
		SupremeShops.inst().getModuleManager().getConditionTypes().remove(id.toUpperCase());
		return null;
	}

	public Condition createNew(String id, Parseable parent, ConfigData data, boolean loadOrSave, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		// invalid version
		if (!ServerVersion.CURRENT.isAtLeast(getMinVersion())) {
			(parent == null || parent.getLastData() == null ? data : parent.getLastData()).log("condition type " + getId() + " requires at least server version " + getMinVersion().getName());
			return null;
		}
		if (!ServerVersion.CURRENT.isOrLess(getMaxVersion())) {
			(parent == null || parent.getLastData() == null ? data : parent.getLastData()).log("condition type " + getId() + " supports at max server version " + getMaxVersion().getName());
			return null;
		}
		// invalid plugins
		for (String requiredPlugin : this.getRequiredPlugins()) {
			if (!Utils.isPluginEnabled(requiredPlugin)) {
				(parent == null || parent.getLastData() == null ? data : parent.getLastData()).log("condition type " + getId() + " requires plugin " + requiredPlugin + " to be enabled");
				return null;
			}
		}
		// create
		try {
			// create instance
			Constructor<? extends Condition> constructor = getConditionClass().getDeclaredConstructor(String.class, Parseable.class, boolean.class, int.class, Mat.class, List.class);
			Condition component = constructor.newInstance(id, parent, mandatory, editorSlot, editorIcon, editorDescription);
			// load or save data
			if (loadOrSave) {
				component.load(data);
			} else {
				component.save(data);
			}
			// return
			return component;
		}
		// couldn't create
		catch (Throwable exception) {
			(parent == null || parent.getLastData() == null ? data : parent.getLastData()).log("unknown error when creating condition " + id + " (" + getConditionClass().getName() + ") with type " + getId());
			exception.printStackTrace();
			return null;
		}
	}

	// overriden methods
	@Override
	public boolean equals(Object obj) {
		return Utils.instanceOf(obj, ConditionType.class) && getId().equalsIgnoreCase(((ConditionType) obj).getId());
	}

	@Override
	public String toString() {
		return getId();
	}

	@Override
	public int compareTo(ConditionType o) {
		return String.CASE_INSENSITIVE_ORDER.compare(getId(), o.getId());
	}

}
