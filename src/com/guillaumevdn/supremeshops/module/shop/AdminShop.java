package com.guillaumevdn.supremeshops.module.shop;

import com.guillaumevdn.gcorelegacy.data.UserInfo;

public class AdminShop extends Shop {

	// base
	public AdminShop(String id, int tradesLimit) {
		super(id, ShopType.ADMIN, null, true, true);
		setTradesLimit(tradesLimit);
	}

	// get
	@Override
	public UserInfo getCurrentOwner() {
		return getActualOwner();
	}

	@Override
	public boolean isCurrentOwnerAdmin() {
		return isActualOwnerAdmin();
	}

	// fake
	@Override
	public AdminShop asFake() {
		return null;
	}

	// data
	@Override
	public Class<?> getJsonDataClass() {
		return null;
	}

	@Override
	public Object writeJsonData() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void readJsonData(Object jsonData) {
		throw new UnsupportedOperationException();
	}

}
