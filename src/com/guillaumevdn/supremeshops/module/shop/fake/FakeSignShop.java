package com.guillaumevdn.supremeshops.module.shop.fake;

import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.data.DataManager.Callback;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.data.ShopBoard;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.SignShop;
import com.guillaumevdn.supremeshops.util.DestroyCause;

public class FakeSignShop extends SignShop implements FakeShop {

	// base
	public FakeSignShop(SignShop shop) {
		super(shop.getId(), shop.getCurrentOwner(), shop.getSign());
		setRemote(shop.isRemote());
		setParticlePatternId(shop.getParticlePatternId());
		setOpen(shop.isOpen());
		setManagers(shop.getManagers());
		setManagersWages(shop.getManagersWages());
		setLastPaidManagersWages(shop.getLastPaidManagersWages());
		setLateManagersWages(shop.getLateManagersWages());
		setBuyers(shop.getBuyers());
		setDisplayName(shop.getDisplayName());
		setObjects(shop.getObjectsMap());
		setTradeConditions(shop.getTradeConditions().clone());
	}

	// fake shop methods
	@Override
	public void withObject(TradeObject object, Double stock) {
		Map<TradeObject, Double> map = Utils.asMapCopy(getObjectsMap());
		map.put(object, stock);
		setObjects(map);
	}

	@Override
	public void withTradeCondition(Condition condition) {
		getTradeConditions().getConditions().addElement(condition);
	}

	// methods
	@Override
	public void addManager(UserInfo manager) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addObject(TradeObject object) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addObject(TradeObject object, Double stock) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addToManagerWage(UserInfo manager, TradeObject object, Double stock) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean attemptTradesProcessing(Player player, int trades, Merchant merchant) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeDisplayName(String displayName) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeLateManagerWages(UserInfo manager, int late, boolean push) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeManagementPermission(Player player, ShopManagementPermission permission, boolean has) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeManagementPermission(UserInfo player, ShopManagementPermission permission, boolean has) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeManagementPermissions(Player player, Set<ShopManagementPermission> permissions) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeManagementPermissions(UserInfo player, Set<ShopManagementPermission> permissions) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeModifyObjectStock(TradeObject object, Double delta) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeObjectStock(TradeObject object, Double stock) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeOpen(boolean open) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeActualOwner(UserInfo owner) {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public int checkToPayLateWages(boolean push) {
		throw new UnsupportedOperationException();
	}

	@Override
	public int checkToPayWages(boolean push) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean checkToPayLateWages(UserInfo manager, boolean push) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean checkToPayWage(UserInfo manager, boolean push) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void deleteAsync() {
		throw new UnsupportedOperationException();
	}
	
	@Override
	public List<Merchant> destroy(DestroyCause cause, boolean push) {
		throw new UnsupportedOperationException();
	}

	@Override
	protected ShopBoard getBoard() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Map<UserInfo, Integer> getBuyers() {
		throw new UnsupportedOperationException();
	}

	@Override
	public Class<SignShopJsonData> getJsonDataClass() {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean payWage(UserInfo manager, boolean push) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void pullAsync() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void pullAsync(Callback callback) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void push(boolean async) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void pushAsync() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void readJsonData(Object jsonData) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeManager(UserInfo manager) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeManagerWage(UserInfo manager, TradeObject wageObject) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeObject(TradeObject object) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void restockAll(Player player) {
		throw new UnsupportedOperationException();
	}

	@Override
	public Object writeJsonData() {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeRemote(boolean remote) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void changeParticlePattern(String particlePatternId) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void addTradeCondition(Condition condition) {
		throw new UnsupportedOperationException();
	}

	@Override
	public boolean areTradeConditionsValid(Player player, boolean errorMessage) {
		throw new UnsupportedOperationException();
	}

	@Override
	public void removeTradeCondition(Condition condition) {
		throw new UnsupportedOperationException();
	}

}
