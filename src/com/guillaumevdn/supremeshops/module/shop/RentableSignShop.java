package com.guillaumevdn.supremeshops.module.shop;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.WeekDay;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.shop.ShopUpdatedEvent;
import com.guillaumevdn.supremeshops.api.event.shop.ShopUpdatedRentConditionsEvent;
import com.guillaumevdn.supremeshops.api.event.shop.ShopUpdatedRentPriceEvent;
import com.guillaumevdn.supremeshops.data.SSUser;
import com.guillaumevdn.supremeshops.gui.shop.TradePreviewGUI;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.fake.FakeRentableSignShop;
import com.guillaumevdn.supremeshops.util.RentPeriod;
import com.guillaumevdn.supremeshops.util.UserOperator;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePatternAvailability;

public class RentableSignShop extends SignShop implements Rentable {

	// base
	private UserInfo currentOwner = null;
	private int pastPaidRents = 0;
	private int paidRents = 0;
	private long lastCheckRent = 0L;
	private boolean awaitingForceUnrent = false;
	private boolean rentable = false;
	private List<TradeObject> rentPrice = new ArrayList<TradeObject>();
	private RentPeriod rentPeriod = RentPeriod.MONTHLY;
	private int rentPeriodStreakLimit = -1;
	private int rentPeriodStreakLimitDelay = -1;
	private CPConditions rentConditions = new CPConditions("rentConditions", null, false, -1, null, null);
	private String unrentedParticlePatternId = null;

	private RentableSignShop(String id) {
		super(id, ShopType.RENTABLE_SIGN);
	}

	public RentableSignShop(String id, BlockCoords sign) {
		super(id, ShopType.RENTABLE_SIGN, null, sign);
	}

	// rentable
	@Override
	public UserInfo getCurrentOwner() {
		return currentOwner;
	}

	@Override
	public boolean isCurrentOwnerAdmin() {
		return false;
	}

	@Override
	public boolean isRented() {
		return currentOwner != null;
	}

	@Override
	public int getPastPaidRents() {
		return pastPaidRents;
	}

	@Override
	public int getPaidRents() {
		return paidRents;
	}

	@Override
	public boolean isRentable() {
		return rentable;
	}

	@Override
	public long getLastCheckRent() {
		return lastCheckRent;
	}

	@Override
	public boolean isAwaitingForceUnrent() {
		return awaitingForceUnrent;
	}

	@Override
	public List<TradeObject> getRentPrice() {
		return rentPrice;
	}

	@Override
	public RentPeriod getRentPeriod() {
		return rentPeriod;
	}

	@Override
	public int getRentPeriodStreakLimit() {
		return rentPeriodStreakLimit;
	}

	@Override
	public int getRentPeriodStreakLimitDelay() {
		return rentPeriodStreakLimitDelay;
	}

	@Override
	public CPConditions getRentConditions() {
		return rentConditions;
	}

	@Override
	public boolean areRentConditionsValid(Player player, boolean errorMessage) {
		return rentConditions.isValid(player, this, errorMessage);
	}

	@Override
	public String getUnrentedParticlePatternId() {
		return unrentedParticlePatternId;
	}

	@Override
	public ParticlePattern getUnrentedParticlePattern() {
		return SupremeShops.inst().getModuleManager().getParticlePattern(unrentedParticlePatternId);
	}

	@Override
	public List<String> getSignLines() {
		return (isRented() ? SSLocaleMisc.MISC_SUPREMESHOPS_RENTSIGNSHOPSIGNLINES : SSLocaleMisc.MISC_SUPREMESHOPS_UNRENTEDSIGNSHOPSIGNLINES).getLines(getMessageReplacers(true, false, null));
	}

	@Override
	public ParticlePatternAvailability getRentableParticlePatternAvailability() {
		return ParticlePatternAvailability.SIGN;
	}

	// particles displayable
	@Override
	public boolean mustDisplayParticles() {
		return isOpen() && isRentable();
	}

	@Override
	public ParticlePattern getCurrentParticlePattern() {
		return isRented() ? getParticlePattern() : getUnrentedParticlePattern();
	}

	@Override
	public boolean areCurrentParticlesConditionsValid(Player player) {
		return isRented() ? areTradeConditionsValid(player, false) : areRentConditionsValid(player, false);
	}

	// set
	protected void setCurrentOwner(UserInfo currentOwner) {
		this.currentOwner = currentOwner;
	}

	protected void setPastPaidRents(int pastPaidRents) {
		this.pastPaidRents = pastPaidRents;
	}

	protected void setPaidRents(int paidRents) {
		this.paidRents = paidRents;
	}

	protected void setRentable(boolean rentable) {
		this.rentable = rentable;
	}

	protected void setRentPrice(List<TradeObject> rentPrice) {
		this.rentPrice = rentPrice;
	}

	protected void setRentPeriod(RentPeriod rentPeriod) {
		this.rentPeriod = rentPeriod;
	}

	protected void setRentPeriodStreakLimit(int rentPeriodStreakLimit) {
		this.rentPeriodStreakLimit = rentPeriodStreakLimit;
	}

	protected void setRentPeriodStreakLimitDelay(int rentPeriodStreakLimitDelay) {
		this.rentPeriodStreakLimitDelay = rentPeriodStreakLimitDelay;
	}

	protected void setRentConditions(CPConditions rentConditions) {
		this.rentConditions = rentConditions;
	}

	protected void setUnrentedParticlePatternId(String unrentedParticlePatternId) {
		this.unrentedParticlePatternId = unrentedParticlePatternId;
	}

	protected void setLastCheckRent(long lastCheckRent) {
		this.lastCheckRent = lastCheckRent;
	}

	protected void setAwaitingForceUnrent(boolean awaitingForceUnrent) {
		this.awaitingForceUnrent = awaitingForceUnrent;
	}

	// methods
	@Override
	protected List<Object> buildMessageReplacers(boolean withDetails, boolean fromModifiersCalculation, Player parser) {
		List<Object> replacers = super.buildMessageReplacers(withDetails, fromModifiersCalculation, parser);
		// current owner
		replacers.add("{current_owner}");
		replacers.add(currentOwner == null ? "/" : currentOwner.toOfflinePlayer().getName());
		// rent time
		replacers.add("{rent_time}");
		replacers.add(currentOwner == null ? "/" : pastPaidRents);
		// rent limit
		replacers.add("{rent_limit}");
		replacers.add(currentOwner == null || rentPeriodStreakLimit <= 0 ? "∞" : rentPeriodStreakLimit);
		// paid rents in advance
		replacers.add("{paid_rents}");
		replacers.add(currentOwner == null ? "/" : paidRents);
		// rent period
		replacers.add("{rent_period}");
		replacers.add(rentPeriod.getName().getLine());
		// rented for
		replacers.add("{rented_for}");
		replacers.add(currentOwner == null ? "/" : paidRents);
		// done
		return replacers;
	}

	// methods : change
	@Override
	public void addRentPrice(TradeObject priceObject) {
		if (!rentPrice.contains(priceObject)) {
			rentPrice.add(priceObject);
			pushAsync();
			Bukkit.getPluginManager().callEvent(new ShopUpdatedRentPriceEvent(this, priceObject, ShopUpdatedRentPriceEvent.Operation.ADD_OBJECT));
		}
	}

	@Override
	public void removeRentPrice(TradeObject priceObject) {
		if (rentPrice.remove(priceObject)) {
			pushAsync();
			Bukkit.getPluginManager().callEvent(new ShopUpdatedRentPriceEvent(this, priceObject, ShopUpdatedRentPriceEvent.Operation.REMOVE_OBJECT));
		}
	}

	@Override
	public void addRentCondition(Condition condition) {
		if (rentConditions.getConditions().getElement(condition.getId()) == null) {
			rentConditions.getConditions().addElement(condition);
			Bukkit.getPluginManager().callEvent(new ShopUpdatedRentConditionsEvent(this, condition, ShopUpdatedRentConditionsEvent.Operation.ADD_CONDITION));
			pushAsync();
		}
	}

	@Override
	public void removeRentCondition(Condition condition) {
		if (rentConditions.getConditions().removeElement(condition.getId()) != null) {
			Bukkit.getPluginManager().callEvent(new ShopUpdatedRentConditionsEvent(this, condition, ShopUpdatedRentConditionsEvent.Operation.REMOVE_CONDITION));
			pushAsync();
		}
	}

	@Override
	public void changeRentable(boolean rentable) {
		// set
		setRentable(rentable);
		pushAsync();
		// close GUIs
		if (!rentable) {
			SupremeShops.inst().getGeneralManager().closeRelatedGUIs(this, false);
		}
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.TOGGLE_RENTABLE));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, (rentable ? "Set rentable" : "Set not rentable"));
		// refresh sign
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	@Override
	public void changeRentPeriod(RentPeriod period) {
		// set
		setRentPeriod(period);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.SET_RENT_PERIOD));
		// message owner
		Player onlineOwner = currentOwner == null ? null : currentOwner.toPlayer();
		if (onlineOwner != null) {
			SSLocale.MSG_SUPREMESHOPS_SHOPCHANGEDRENTPERIOD.send(onlineOwner, "{shop}", getDisplayName(), "{rent_period}", period.getName());
		}
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, "Set rent period to " + period.name());
		// refresh sign
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	@Override
	public void changeRentPeriodStreakLimit(int limit) {
		// set
		setRentPeriodStreakLimit(limit);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.SET_RENT_PERIOD_STREAK_LIMIT));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, "Set rent period streak limit to " + limit);
		// refresh sign
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	@Override
	public void changeRentPeriodStreakLimitDelay(int delay) {
		// set
		setRentPeriodStreakLimitDelay(delay);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.SET_RENT_PERIOD_STREAK_LIMIT_DELAY));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, "Set rent period streak limit delay to " + delay);
		// refresh sign
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	@Override
	public void changeUnrentedParticlePattern(String particlePatternId) {
		// set particle pattern id
		setUnrentedParticlePatternId(particlePatternId);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.SET_UNRENTED_PARTICLE_PATTERN));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, particlePatternId == null ? "Removed unrented particle pattern" : "Set unrented particle pattern to " + particlePatternId);
	}

	@Override
	public void changePaidRents(int paidRents) {
		// set
		if (this.paidRents > 0 && pastPaidRents <= rentPeriodStreakLimit) {
			awaitingForceUnrent = false;
		}
		setPaidRents(paidRents);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.SET_PAID_RENTS));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, "Set paid rents to " + paidRents);
		// refresh sign
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	@Override
	public void changeRent(UserInfo newOwner, int alreadyPaidForRents, boolean push) {
		// there's currently someone renting the shop
		if (currentOwner != null) {
			// refund paid rent prices
			OfflinePlayer owner = currentOwner.toOfflinePlayer();
			Player onlineOwner = owner.getPlayer();
			if (paidRents > 0) {
				for (TradeObject object : rentPrice) {
					object.forceWithdraw(owner, paidRents * object.getCustomAmount(onlineOwner));
				}
			}
			// withdraw stock
			for (TradeObject object : getObjects()) {
				object.forceWithdrawAll(owner, this);
			}
			// notify owner
			if (onlineOwner != null) {
				SSLocale.MSG_SUPREMESHOPS_UNRENTEDSHOP.send(onlineOwner, "{shop}", getDisplayName());
			}
		}
		UserInfo oldOwner = currentOwner;
		// reset rent settings
		setCurrentOwner(newOwner);
		setPaidRents(0);
		setPastPaidRents(alreadyPaidForRents);
		// reset shop settings
		setParticlePatternId(null);
		clearAllManagers();
		setDisplayName(getId());
		clearObjects();
		setOpen(false);
		setRemote(false);
		setTradesLimit(-1);
		setAwaitingForceUnrent(false);
		setLastCheckRent(System.currentTimeMillis());
		// update in shop board
		SupremeShops.inst().getData().getShops().updateByOwner(this, oldOwner);
		// update last rent end
		if (oldOwner != null) {
			new UserOperator(oldOwner) {
				@Override
				protected void process(SSUser user) {
					user.setLastRentEnd(RentableSignShop.this, System.currentTimeMillis());
				}
			}.operate();
		}
		// close all GUIs for this shop
		SupremeShops.inst().getGeneralManager().closeRelatedGUIs(this, false);
		// push
		if (push) {
			pushAsync();
		}
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.SET_CURRENT_OWNER));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, "Set current owner to " + (newOwner == null ? "/" : newOwner.toStringName()));
		// refresh sign
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	// methods : pay rent
	public static final long SECOND_MILLIS = 1000L;
	public static final long MINUTE_MILLIS = 60L * SECOND_MILLIS;
	public static final long HOUR_MILLIS = 60L * MINUTE_MILLIS;
	public static final long DAY_MILLIS = 24L * HOUR_MILLIS;
	public static final long WEEK_MILLIS = 7L * DAY_MILLIS;

	@Override
	public void checkToUpdateRent(boolean push) {
		// not rented
		if (!isRented()) {
			return;
		}
		// is already awaiting something
		if (awaitingForceUnrent) {
			return;
		}
		// daily
		if (RentPeriod.DAILY.equals(rentPeriod)) {
			// get current period start
			Calendar calendar = GCoreLegacy.inst().getCalendarInstance();
			long hour = calendar.get(Calendar.HOUR_OF_DAY);
			long minute = calendar.get(Calendar.MINUTE);
			long second = calendar.get(Calendar.SECOND);
			long millis = calendar.get(Calendar.MILLISECOND);
			long start = System.currentTimeMillis() - hour * HOUR_MILLIS - minute * MINUTE_MILLIS - second * SECOND_MILLIS - millis;
			// if it wasn't today, pay
			if (lastCheckRent < start) {
				updateRent(push);
				return;
			}
		}
		// weekly
		else if (RentPeriod.WEEKLY.equals(rentPeriod)) {
			// get current period start
			Calendar calendar = GCoreLegacy.inst().getCalendarInstance();
			long day = WeekDay.getCurrent().ordinal();
			long hour = calendar.get(Calendar.HOUR_OF_DAY);
			long minute = calendar.get(Calendar.MINUTE);
			long second = calendar.get(Calendar.SECOND);
			long millis = calendar.get(Calendar.MILLISECOND);
			long start = System.currentTimeMillis() - day * DAY_MILLIS - hour * HOUR_MILLIS - minute * MINUTE_MILLIS - second * SECOND_MILLIS - millis;
			// if it wasn't this week, pay
			if (lastCheckRent < start) {
				updateRent(push);
				return;
			}
		}
		// monthly
		if (RentPeriod.MONTHLY.equals(rentPeriod)) {
			// get current period start
			Calendar calendar = GCoreLegacy.inst().getCalendarInstance();
			long day = calendar.get(Calendar.DAY_OF_MONTH);
			long hour = calendar.get(Calendar.HOUR_OF_DAY);
			long minute = calendar.get(Calendar.MINUTE);
			long second = calendar.get(Calendar.SECOND);
			long millis = calendar.get(Calendar.MILLISECOND);
			long start = System.currentTimeMillis() - day * DAY_MILLIS - hour * HOUR_MILLIS - minute * MINUTE_MILLIS - second * SECOND_MILLIS - millis;
			// if it wasn't this month, pay
			if (lastCheckRent < start) {
				updateRent(push);
				return;
			}
		}
		// nothing to pay
		return;
	}

	@Override
	public void updateRent(boolean push) {
		// couldn't pay already
		if (awaitingForceUnrent) {
			return;
		}
		// can't pay
		if (paidRents <= 0) {
			lastCheckRent = System.currentTimeMillis();
			awaitingForceUnrent = true;
			// event
			Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.UPDATED_RENT));
			return;
		}
		// pay
		++pastPaidRents;
		--paidRents;
		lastCheckRent = System.currentTimeMillis();
		// is above paid rents streak limit
		if (pastPaidRents > rentPeriodStreakLimit) {
			awaitingForceUnrent = true;
			// event
			Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.UPDATED_RENT));
			return;
		}
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.UPDATED_RENT));
		// push
		if (push) pushAsync();
	}

	// methods : misc
	@Override
	public void openGUI(Merchant fromMerchant, Player player, GUI fromGUI, int fromGUIPageIndex) {
		new TradePreviewGUI(this, fromMerchant, 1, player, fromGUI, fromGUIPageIndex).open(player);
	}

	// fake
	@Override
	public RentableSignShop asFake() {
		return new FakeRentableSignShop(this);
	}

	// data
	public static class RentableSignShopJsonData {

		private final UserInfo owner;
		private final String displayName;
		private final Map<TradeObject, Double> objects;
		private final CPConditions tradeConditions;
		private final boolean open;
		private final boolean remote;
		private final int tradesLimit;
		private final boolean adminStock;
		private final Map<UserInfo, Set<ShopManagementPermission>> managers;
		private final Map<UserInfo, Map<TradeObject, Double>> managersWages;
		private final Map<UserInfo, Long> lastPaidManagersWages;
		private final Map<UserInfo, Integer> lateManagersWages;
		private final Map<UserInfo, Integer> buyers;
		private final BlockCoords sign;
		private final String particlePatternId;
		private final UserInfo currentOwner;
		private final int pastPaidRents;
		private final int paidRents;
		private final long lastCheckRent;
		private final boolean awaitingForceUnrent;
		private final boolean rentable;
		private final List<TradeObject> rentPrice;
		private final RentPeriod rentPeriod;
		private final int rentPeriodStreakLimit;
		private final int rentPeriodStreakLimitDelay;
		private final CPConditions rentConditions;
		private final String unrentedParticlePatternId;

		public RentableSignShopJsonData(RentableSignShop shop) {
			this.owner = shop.getCurrentOwner();
			this.displayName = shop.getDisplayName();
			this.objects = shop.getObjectsMap();
			this.tradeConditions = shop.getTradeConditions();
			this.open = shop.isOpen();
			this.remote = shop.isRemote();
			this.tradesLimit = shop.getTradesLimit();
			this.adminStock = shop.getAdminStock();
			this.managers = shop.getManagers();
			this.managersWages = shop.getManagersWages();
			this.lastPaidManagersWages = shop.getLastPaidManagersWages();
			this.lateManagersWages = shop.getLateManagersWages();
			this.buyers = shop.getBuyers();
			this.sign = shop.getSign();
			this.particlePatternId = shop.getParticlePatternId();
			this.currentOwner = shop.getCurrentOwner();
			this.pastPaidRents = shop.getPastPaidRents();
			this.paidRents = shop.getPaidRents();
			this.lastCheckRent = shop.getLastCheckRent();
			this.awaitingForceUnrent = shop.isAwaitingForceUnrent();
			this.rentable = shop.isRentable();
			this.rentPrice = shop.getRentPrice();
			this.rentPeriod = shop.getRentPeriod();
			this.rentPeriodStreakLimit = shop.getRentPeriodStreakLimit();
			this.rentPeriodStreakLimitDelay = shop.getRentPeriodStreakLimitDelay();
			this.rentConditions = shop.getRentConditions();
			this.unrentedParticlePatternId = shop.getUnrentedParticlePatternId();
		}

	}

	@Override
	public Class<?> getJsonDataClass() {
		return RentableSignShopJsonData.class;
	}

	@Override
	public void readJsonData(Object jsonData) {
		// invalid class
		if (!Utils.instanceOf(jsonData, getJsonDataClass())) {
			throw new IllegalArgumentException("data class is " + jsonData.getClass() + ", expected is " + getJsonDataClass().getName());
		}
		// read data
		RentableSignShopJsonData data = (RentableSignShopJsonData) jsonData;
		setActualOwner(data.owner);
		setDisplayName(data.displayName);
		setObjects(data.objects);
		setTradeConditions(data.tradeConditions);
		setOpen(data.open);
		setRemote(data.remote);
		setTradesLimit(data.tradesLimit);
		setAdminStock(data.adminStock);
		setManagers(data.managers);
		setManagersWages(data.managersWages);
		setLastPaidManagersWages(data.lastPaidManagersWages);
		setLateManagersWages(data.lateManagersWages);
		setBuyers(data.buyers);
		setSign(data.sign);
		setParticlePatternId(data.particlePatternId);
		setCurrentOwner(data.currentOwner);
		setPastPaidRents(data.pastPaidRents);
		setPaidRents(data.paidRents);
		setLastCheckRent(data.lastCheckRent);
		setAwaitingForceUnrent(data.awaitingForceUnrent);
		setRentable(data.rentable);
		setRentPrice(data.rentPrice);
		setRentPeriod(data.rentPeriod);
		setRentPeriodStreakLimit(data.rentPeriodStreakLimit);
		setRentPeriodStreakLimitDelay(data.rentPeriodStreakLimitDelay);
		setRentConditions(data.rentConditions);
		setUnrentedParticlePatternId(data.unrentedParticlePatternId);
	}

	@Override
	public Object writeJsonData() {
		return new RentableSignShopJsonData(this);
	}

}
