package com.guillaumevdn.supremeshops.module.shop;

import com.guillaumevdn.supremeshops.SSLocaleMisc;

public enum ShopType {

	// values
	ADMIN(AdminShop.class, null, SSLocaleMisc.MISC_SUPREMESHOPS_SHOPTYPE_ADMIN.getLine()),
	BLOCK(BlockShop.class, "block", SSLocaleMisc.MISC_SUPREMESHOPS_SHOPTYPE_BLOCK.getLine()),
	GUI(GuiShop.class, "gui", SSLocaleMisc.MISC_SUPREMESHOPS_SHOPTYPE_GUI.getLine()),
	MERCHANT(MerchantShop.class, "merchant", SSLocaleMisc.MISC_SUPREMESHOPS_SHOPTYPE_MERCHANT.getLine()),
	RENTABLE_BLOCK(RentableBlockShop.class, "rentable_block", SSLocaleMisc.MISC_SUPREMESHOPS_SHOPTYPE_RENTBLOCK.getLine()),
	RENTABLE_SIGN(RentableSignShop.class, "rentable_sign", SSLocaleMisc.MISC_SUPREMESHOPS_SHOPTYPE_RENTABLESIGN.getLine()),
	SIGN(SignShop.class, "sign", SSLocaleMisc.MISC_SUPREMESHOPS_SHOPTYPE_SHOP.getLine());

	// base
	private Class<? extends Shop> shopClass;
	private String diskDataFolder;
	private String name;

	private ShopType(Class<? extends Shop> shopClass, String diskDataFolder, String name) {
		this.shopClass = shopClass;
		this.diskDataFolder = diskDataFolder;
		this.name = name;
	}

	// get
	public Class<? extends Shop> getShopClass() {
		return shopClass;
	}

	public String getDiskDataFolder() {
		if (diskDataFolder == null) throw new UnsupportedOperationException("shop type " + name() + " can't be saved on disk");
		return diskDataFolder;
	}

	public boolean mustSaveData() {
		return diskDataFolder != null;
	}

	public String getName() {
		return name;
	}

}
