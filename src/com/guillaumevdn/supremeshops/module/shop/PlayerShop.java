package com.guillaumevdn.supremeshops.module.shop;

import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.WeekDay;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.shop.ShopPaidManagerWageEvent;
import com.guillaumevdn.supremeshops.api.event.shop.ShopUpdatedManagerEvent;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.util.WagePeriod;

public abstract class PlayerShop extends Shop implements Manageable<ShopManagementPermission> {

	// base
	private Map<UserInfo, Set<ShopManagementPermission>> managers = new HashMap<UserInfo, Set<ShopManagementPermission>>();
	private Map<UserInfo, Map<TradeObject, Double>> managersWages = new HashMap<UserInfo, Map<TradeObject, Double>>();
	private Map<UserInfo, Long> lastPaidManagersWages = new HashMap<UserInfo, Long>();
	private Map<UserInfo, Integer> lateManagersWages = new HashMap<UserInfo, Integer>();

	protected PlayerShop(String id, ShopType type) {
		super(id, type);
	}

	public PlayerShop(String id, ShopType type, UserInfo owner, boolean open, boolean remote) {
		super(id, type, owner, open, remote);
	}

	// get
	@Override
	public List<ShopManagementPermission> getAllPermissions() {
		return Utils.asList(ShopManagementPermission.values());
	}
	
	@Override
	public Map<UserInfo, Set<ShopManagementPermission>> getManagers() {
		return Collections.unmodifiableMap(managers);
	}

	@Override
	public Map<UserInfo, Map<TradeObject, Double>> getManagersWages() {
		return Collections.unmodifiableMap(managersWages);
	}

	@Override
	public Map<UserInfo, Long> getLastPaidManagersWages() {
		return Collections.unmodifiableMap(lastPaidManagersWages);
	}

	@Override
	public Map<UserInfo, Integer> getLateManagersWages() {
		return Collections.unmodifiableMap(lateManagersWages);
	}

	@Override
	public double getTotalObjectStock(List<ObjectType> types, boolean withManagersWages) {
		return super.getTotalObjectStock(types, withManagersWages) + (withManagersWages ? getTotalObjectStockInManagersWages(types) : 0d);
	}

	@Override
	public double getTotalObjectStockInManagersWages(List<ObjectType> types) {
		double count = 0d;
		for (Map<TradeObject, Double> wage : managersWages.values()) {
			for (TradeObject object : wage.keySet()) {
				if ((types == null || types.isEmpty()) || types.contains(object.getType())) {
					count += wage.get(object).intValue();
				}
			}
		}
		return count;
	}

	// set
	protected void setManagers(Map<UserInfo, Set<ShopManagementPermission>> managers) {
		this.managers = managers;
	}

	protected void setManagersWages(Map<UserInfo, Map<TradeObject, Double>> managersWages) {
		this.managersWages = managersWages;
	}

	protected void setLastPaidManagersWages(Map<UserInfo, Long> lastPaidManagersWages) {
		this.lastPaidManagersWages = lastPaidManagersWages;
	}

	protected void setLateManagersWages(Map<UserInfo, Integer> lateManagersWages) {
		this.lateManagersWages = lateManagersWages;
	}

	protected void setNotManager(UserInfo manager) {
        managers.remove(manager);
        managersWages.remove(manager);
        lastPaidManagersWages.remove(manager);
        lateManagersWages.remove(manager);
	}

	protected void clearAllManagers() {
		managers.clear();
		managersWages.clear();
		lastPaidManagersWages.clear();
		lateManagersWages.clear();
	}

	// methods
	@Override
	public Set<ShopManagementPermission> getManagementPermissions(OfflinePlayer player) {
		return getManagementPermissions(new UserInfo(player));
	}

	@Override
	public Set<ShopManagementPermission> getManagementPermissions(UserInfo player) {
		if (managers.containsKey(player)) {// is manager
			return Collections.unmodifiableSet(managers.get(player));
		} else if ((getCurrentOwner() == null && SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player.toOfflinePlayer())) || player.equals(getCurrentOwner())) {// is owner or can edit admin shop
			return Collections.unmodifiableSet(Utils.asSet(ShopManagementPermission.values()));
		}
		return Collections.unmodifiableSet(new HashSet<ShopManagementPermission>());// no permissions
	}

	@Override
	public boolean hasManagementPermission(Player player, ShopManagementPermission permission) {
		return getManagementPermissions(player).contains(permission);
	}

	@Override
	public Long getLastPaidManagerWage(UserInfo manager) {
		Long last = lastPaidManagersWages.get(manager);
		return last == null ? 0L : last;
	}

	@Override
	public Integer getLateManagerWages(UserInfo manager) {
		Integer late = lateManagersWages.get(manager);
		return late == null ? 0 : late;
	}

	@Override
	public Map<TradeObject, Double> getManagerWage(UserInfo manager) {
		Map<TradeObject, Double> wage = managersWages.get(manager);
		return Collections.unmodifiableMap(wage == null ? new HashMap<TradeObject, Double>() : wage);
	}

	@Override
	public void changeModifyManagerWageStock(UserInfo manager, TradeObject object, double delta) {
		Map<TradeObject, Double> wage = managersWages.get(manager);
		wage.put(object, (wage.containsKey(object) ? wage.get(object) : 0) + delta);
		pushAsync();
	}

	// methods : managers
	@Override
	public void addManager(UserInfo manager) {
		if (!managers.containsKey(manager)) {
			managers.put(manager, new HashSet<ShopManagementPermission>());
			Bukkit.getPluginManager().callEvent(new ShopUpdatedManagerEvent(this, manager, null, Utils.emptyList(ShopManagementPermission.class), ShopUpdatedManagerEvent.Operation.ADD_MANAGER));
			pushAsync();
		}
	}

	@Override
	public void addToManagerWage(UserInfo manager, TradeObject wageObject, Double stock) {
		Map<TradeObject, Double> wage = managersWages.get(manager);
		if (wage == null) managersWages.put(manager, wage = new HashMap<TradeObject, Double>());
		wage.put(wageObject, stock);
		Bukkit.getPluginManager().callEvent(new ShopUpdatedManagerEvent(this, manager, wageObject, Utils.emptyList(ShopManagementPermission.class),ShopUpdatedManagerEvent.Operation.ADD_WAGE));
		pushAsync();
	}

	@Override
	public void removeManager(UserInfo manager) {
		if (managers.remove(manager) != null) {
			pushAsync();
			// close inventory ; knowing that he's fired, that filthy manager might already be committing an obnoxious FELONY ! ;-;
			Player player = manager.toPlayer();
			if (player != null) player.closeInventory();
			// event
			Bukkit.getPluginManager().callEvent(new ShopUpdatedManagerEvent(this, manager, null, Utils.emptyList(ShopManagementPermission.class),ShopUpdatedManagerEvent.Operation.REMOVE_MANAGER));
		}
	}

	@Override
	public void removeManagerWage(UserInfo manager, TradeObject wageObject) {
		Map<TradeObject, Double> wage = managersWages.get(manager);
		if (wage != null && wage.remove(wageObject) != null) {
			pushAsync();
			Bukkit.getPluginManager().callEvent(new ShopUpdatedManagerEvent(this, manager, wageObject, Utils.emptyList(ShopManagementPermission.class),ShopUpdatedManagerEvent.Operation.REMOVE_WAGE));
		}
	}

	@Override
	public void changeManagementPermission(Player player, ShopManagementPermission permission, boolean has) {
		changeManagementPermission(new UserInfo(player), permission, has);
	}

	@Override
	public void changeManagementPermission(UserInfo manager, ShopManagementPermission permission, boolean has) {
		// get permissions
		Set<ShopManagementPermission> permissions = managers.get(manager);
		if (permissions == null) managers.put(manager, permissions = new HashSet<ShopManagementPermission>());
		// set permission
		if (has) {
			if (permissions.add(permission)) {
				Bukkit.getPluginManager().callEvent(new ShopUpdatedManagerEvent(this, manager, null, Utils.asList(permission), ShopUpdatedManagerEvent.Operation.ADD_PERMISSION));
				pushAsync();
			}
		} else {
			if (permissions.remove(permission)) {
				Bukkit.getPluginManager().callEvent(new ShopUpdatedManagerEvent(this, manager, null, Utils.asList(permission), ShopUpdatedManagerEvent.Operation.REMOVE_PERMISSION));
				pushAsync();
			}
		}
	}

	@Override
	public void changeManagementPermissions(Player player, Set<ShopManagementPermission> permissions) {
		changeManagementPermissions(new UserInfo(player), permissions);
	}

	@Override
	public void changeManagementPermissions(UserInfo manager, Set<ShopManagementPermission> permissions) {
		managers.put(manager, permissions = new HashSet<ShopManagementPermission>());
		Bukkit.getPluginManager().callEvent(new ShopUpdatedManagerEvent(this, manager, null, Utils.asList(permissions), ShopUpdatedManagerEvent.Operation.UPDATE_PERMISSIONS));
		pushAsync();
	}

	@Override
	public void changeLateManagerWages(UserInfo manager, int late, boolean push) {
		lateManagersWages.put(manager, late);
		if (push) pushAsync();
	}

	public static final long SECOND_MILLIS = 1000L;
	public static final long MINUTE_MILLIS = 60L * SECOND_MILLIS;
	public static final long HOUR_MILLIS = 60L * MINUTE_MILLIS;
	public static final long DAY_MILLIS = 24L * HOUR_MILLIS;
	public static final long WEEK_MILLIS = 7L * DAY_MILLIS;

	@Override
	public int checkToPayWages(boolean push) {
		// check managers
		int paid = 0;
		for (UserInfo manager : managers.keySet()) {
			if (checkToPayWage(manager, false)) {
				++paid;
			}
		}
		// push
		if (paid > 0 && push) {
			pushAsync();
		}
		// return
		return paid;
	}

	@Override
	public boolean checkToPayWage(UserInfo manager, boolean push) {
		long lastPaid = getLastPaidManagerWage(manager);
		// daily
		if (WagePeriod.DAILY.equals(SupremeShops.inst().getModuleManager().getWagePeriod())) {
			// get current period start
			Calendar calendar = GCoreLegacy.inst().getCalendarInstance();
			long hour = calendar.get(Calendar.HOUR_OF_DAY);
			long minute = calendar.get(Calendar.MINUTE);
			long second = calendar.get(Calendar.SECOND);
			long millis = calendar.get(Calendar.MILLISECOND);
			long start = System.currentTimeMillis() - hour * HOUR_MILLIS - minute * MINUTE_MILLIS - second * SECOND_MILLIS - millis;
			// if it wasn't today, pay
			if (lastPaid < start) {
				return payWage(manager, push);
			}
		}
		// weekly
		else if (WagePeriod.WEEKLY.equals(SupremeShops.inst().getModuleManager().getWagePeriod())) {
			// get current period start
			Calendar calendar = GCoreLegacy.inst().getCalendarInstance();
			long day = WeekDay.getCurrent().ordinal();
			long hour = calendar.get(Calendar.HOUR_OF_DAY);
			long minute = calendar.get(Calendar.MINUTE);
			long second = calendar.get(Calendar.SECOND);
			long millis = calendar.get(Calendar.MILLISECOND);
			long start = System.currentTimeMillis() - day * DAY_MILLIS - hour * HOUR_MILLIS - minute * MINUTE_MILLIS - second * SECOND_MILLIS - millis;
			// if it wasn't this week, pay
			if (lastPaid < start) {
				return payWage(manager, push);
			}
		}
		// monthly
		if (WagePeriod.MONTHLY.equals(SupremeShops.inst().getModuleManager().getWagePeriod())) {
			// get current period start
			Calendar calendar = GCoreLegacy.inst().getCalendarInstance();
			long day = calendar.get(Calendar.DAY_OF_MONTH);
			long hour = calendar.get(Calendar.HOUR_OF_DAY);
			long minute = calendar.get(Calendar.MINUTE);
			long second = calendar.get(Calendar.SECOND);
			long millis = calendar.get(Calendar.MILLISECOND);
			long start = System.currentTimeMillis() - day * DAY_MILLIS - hour * HOUR_MILLIS - minute * MINUTE_MILLIS - second * SECOND_MILLIS - millis;
			// if it wasn't this month, pay
			if (lastPaid < start) {
				return payWage(manager, push);
			}
		}
		// nothing to pay
		return false;
	}

	@Override
	public int checkToPayLateWages(boolean push) {
		// check managers
		int paid = 0;
		for (UserInfo manager : managers.keySet()) {
			if (checkToPayLateWages(manager, false)) {
				++paid;
			}
		}
		// push
		if (paid > 0 && push) {
			pushAsync();
		}
		// return
		return paid;
	}

	@Override
	public boolean checkToPayLateWages(UserInfo manager, boolean push) {
		// offline
		Player managerPlayer = manager.toPlayer();
		if (managerPlayer == null) {
			return false;
		}
		// see how late we are
		int late = getLateManagerWages(manager);
		if (late == 0) {
			return false;
		}
		// no wage
		Map<TradeObject, Double> wage = getManagerWage(manager);
		if (wage.isEmpty()) {
			return false;
		}
		// see how many complete wages we can pay
		int canPay = late;
		for (TradeObject object : wage.keySet()) {
			int can = object.getTradesForStock(wage.get(object), object.getCustomAmount(null));
			if (can < canPay) canPay = can;
		}
		// can at least pay one
		if (canPay > 0) {
			// pay
			for (TradeObject object : wage.keySet()) {
				object.give(managerPlayer, canPay);
			}
			late -= canPay;
			lateManagersWages.put(manager, late);
			SSLocale.MSG_SUPREMESHOPS_SHOPLATEWAGERECEIVE.send(managerPlayer, "{player}", managerPlayer.getName(), "{name}", getDisplayName(), "{count}", late, "{plural}", Utils.getPlural(late));
			// event
			Bukkit.getPluginManager().callEvent(new ShopPaidManagerWageEvent(this, manager, canPay));
			// push
			if (push) pushAsync();
		}
		return true;
	}

	@Override
	public boolean payWage(UserInfo manager, boolean push) {
		// offline
		Player managerPlayer = manager.toPlayer();
		if (managerPlayer == null) return false;
		// no wage
		Map<TradeObject, Double> wage = getManagerWage(manager);
		if (wage.isEmpty()) {
			return false;
		}
		// can't pay the complete wage
		if (manageStock()) {
			for (TradeObject object : wage.keySet()) {
				if (object.getTradesForStock(wage.get(object), object.getCustomAmount(null)) <= 0) {
					int late = getLateManagerWages(manager) + 1;
					lateManagersWages.put(manager, late);
					// notify manager and owner
					SSLocale.MSG_SUPREMESHOPS_SHOPOWNERCANTPAYMANAGEMENT.send(managerPlayer, "{player}", managerPlayer.getName(), "{name}", getDisplayName(), "{count}", late, "{plural}", Utils.getPlural(late));
					Player ownerPlayer = getCurrentOwner() == null ? null : getCurrentOwner().toPlayer();
					if (ownerPlayer != null) SSLocale.MSG_SUPREMESHOPS_SHOPCANTPAYMANAGER.send(ownerPlayer, "{player}", ownerPlayer.getName(), "{name}", getDisplayName(), "{count}", late, "{plural}", Utils.getPlural(late));
					return false;
				}
			}
		}
		// pay
		for (TradeObject object : wage.keySet()) {
			object.give(managerPlayer, object.getCustomAmount(null));
		}
		lastPaidManagersWages.put(manager, System.currentTimeMillis());
		SSLocale.MSG_SUPREMESHOPS_SHOPWAGERECEIVE.send(managerPlayer, "{player}", managerPlayer.getName(), "{name}", getDisplayName());
		// event
		Bukkit.getPluginManager().callEvent(new ShopPaidManagerWageEvent(this, manager, 0));
		// push
		if (push) pushAsync();
		return true;
	}

	// methods : change
	@Override
	public void changeActualOwner(UserInfo owner) {
		setNotManager(owner);// before the other method, otherwise the manager changes won't be pushed
		super.changeActualOwner(owner);
	}

}
