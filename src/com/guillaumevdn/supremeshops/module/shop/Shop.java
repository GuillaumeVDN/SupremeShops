package com.guillaumevdn.supremeshops.module.shop;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.data.DataElement;
import com.guillaumevdn.gcorelegacy.lib.data.mysql.Query;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.shop.ShopDestroyedEvent;
import com.guillaumevdn.supremeshops.api.event.shop.ShopTradeProcessEvent;
import com.guillaumevdn.supremeshops.api.event.shop.ShopTradeProcessedEvent;
import com.guillaumevdn.supremeshops.api.event.shop.ShopUpdatedEvent;
import com.guillaumevdn.supremeshops.api.event.shop.ShopUpdatedObjectEvent;
import com.guillaumevdn.supremeshops.api.event.shop.ShopUpdatedTradeConditionsEvent;
import com.guillaumevdn.supremeshops.data.MainBoard;
import com.guillaumevdn.supremeshops.data.SSUser;
import com.guillaumevdn.supremeshops.data.ShopBoard;
import com.guillaumevdn.supremeshops.data.ShopBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.display.items.ItemsDisplayable;
import com.guillaumevdn.supremeshops.module.itemvalue.ItemValueSetting;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.modifier.Modifier;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.object.TradeObject.ObjectRestockLogic;
import com.guillaumevdn.supremeshops.module.object.type.ObjectItem;
import com.guillaumevdn.supremeshops.util.DestroyCause;
import com.guillaumevdn.supremeshops.util.ShopsSorter;
import com.guillaumevdn.supremeshops.util.ShopsSorter.SortCriteria;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;

public abstract class Shop extends DataElement {

	// base
	private final String id;
	private final ShopType type;
	private UserInfo owner = null;
	private String displayName = null;
	private Map<TradeObject, Double> objects = new HashMap<TradeObject, Double>();
	private Map<UserInfo, Integer> buyers = new HashMap<UserInfo, Integer>();
	private CPConditions tradeConditions = new CPConditions("tradeConditions", null, false, -1, null, null);
	private boolean open = false;
	private boolean remote = false;
	private int tradesLimit = -1;
	private boolean adminStock = false;

	protected Shop(String id, ShopType type) {
		this.id = id;
		this.type = type;
	}

	public Shop(String id, ShopType type, UserInfo owner, boolean open, boolean remote) {
		this.id = id;
		this.displayName = id;
		this.type = type;
		this.owner = owner;
		this.open = open;
		this.remote = remote;
	}

	// get
	public String getId() {
		return id;
	}

	public ShopType getType() {
		return type;
	}

	public String getDisplayName() {
		return displayName;
	}

	public UserInfo getActualOwner() {
		return owner;
	}

	public boolean isActualOwnerAdmin() {
		return owner == null;
	}

	public abstract UserInfo getCurrentOwner();
	public abstract boolean isCurrentOwnerAdmin();

	public Map<TradeObject, Double> getObjectsMap() {
		return Collections.unmodifiableMap(objects);
	}

	public Collection<TradeObject> getObjects() {
		return Collections.unmodifiableCollection(objects.keySet());
	}

	public Collection<TradeObject> getObjects(List<ObjectSide> sides, List<ObjectType> types) {
		List<TradeObject> result = new ArrayList<TradeObject>();
		for (TradeObject object : objects.keySet()) {
			if (((sides == null || sides.isEmpty()) || sides.contains(object.getSide())) && ((types == null || types.isEmpty()) || types.contains(object.getType()))) {
				result.add(object);
			}
		}
		return Collections.unmodifiableCollection(result);
	}

	public double getObjectStock(TradeObject object) {
		Double stock = objects.get(object);
		return stock == null ? 0d : stock;
	}

	public double getTotalObjectStock(List<ObjectType> types, boolean withManagersWages) {
		// objects
		int count = 0;
		for (TradeObject object : getObjects(null, types)) {
			count += (int) getObjectStock(object);
		}
		// done
		return count;
	}

	public Map<UserInfo, Integer> getBuyers() {
		return Collections.unmodifiableMap(buyers);
	}

	public CPConditions getTradeConditions() {
		return tradeConditions;
	}

	public Collection<Condition> getTradeConditions(List<ConditionType> types) {
		List<Condition> result = new ArrayList<Condition>();
		for (Condition condition : tradeConditions.getConditions().getElements().values()) {
			if ((types == null || types.isEmpty()) || types.contains(condition.getType())) {
				result.add(condition);
			}
		}
		return Collections.unmodifiableCollection(result);
	}

	public int getBuyerTrades(UserInfo user) {
		Integer trades = buyers.get(user);
		return trades == null ? 0 : trades;
	}

	public int getBuyersTrades() {
		int trades = 0;
		for (UserInfo user : buyers.keySet()) {
			trades += buyers.get(user);
		}
		return trades;
	}

	public boolean isOpen() {
		return open;
	}

	public boolean ensureOpenOrError(Player player) {
		if (!open) {
			SSLocale.MSG_SUPREMESHOPS_SHOPCLOSED.send(player);
		}
		return open;
	}

	public boolean isRemote() {
		return remote;
	}

	public int getTradesLimit() {
		return tradesLimit;
	}

	public boolean getAdminStock() {
		return adminStock;
	}

	public ItemData getGuiIcon(Player parser) {
		// check items first
		for (ObjectSide side : Utils.asList(ObjectSide.GIVING, ObjectSide.TAKING)) {// check giving first, then taking
			for (TradeObject object : getObjects(Utils.asList(side), Utils.asList(ObjectType.ITEM))) {
				ItemData item = ((ObjectItem) object).getItem(parser);
				if (item != null && item.getType() != null && !item.getType().isAir()) {
					return item.clone();
				}
			}
		}
		// check other items
		List<ObjectType> toCheck = Utils.asList(ObjectType.values());
		toCheck.remove(ObjectType.ITEM);
		for (ObjectSide side : Utils.asList(ObjectSide.GIVING, ObjectSide.TAKING)) {
			for (TradeObject object : getObjects(Utils.asList(side), toCheck)) {
				ItemData item = object.getPreviewItem(1d, parser);
				if (item != null && item.getType() != null && !item.getType().isAir()) {
					return item.clone();
				}
			}
		}
		// block shop
		if (this instanceof BlockShop) {
			Mat mat = Mat.fromBlock(((BlockShop) this).getBlock().toBlock());
			if (!mat.isAir()) {
				return new ItemData(null, -1, mat, 1, null, null);
			}
		}
		// sign shop
		if (this instanceof SignShop) {
			Mat mat = Mat.fromBlock(((SignShop) this).getSign().toBlock());
			if (!mat.isAir()) {
				return new ItemData(null, -1, mat, 1, null, null);
			}
		}
		// nothing was found
		return new ItemData(null, -1, Mat.CHEST, 1, null, null);
	}

	// set
	protected void setActualOwner(UserInfo owner) {
		this.owner = owner;
	}

	protected void setDisplayName(String displayName) {
		this.displayName = displayName;
	}

	protected void setObjects(Map<TradeObject, Double> objects) {
		this.objects = objects;
	}

	protected void clearObjects() {
		objects.clear();
	}

	protected void setObjectStock(TradeObject object, Double stock) {
		if (objects.containsKey(object)) {
			objects.put(object, stock);
			Bukkit.getPluginManager().callEvent(new ShopUpdatedObjectEvent(this, object, ShopUpdatedObjectEvent.Operation.SET_STOCK));
		}
	}

	public void setTradeConditions(CPConditions tradeConditions) {
		this.tradeConditions = tradeConditions;
	}

	protected void setBuyers(Map<UserInfo, Integer> buyers) {
		this.buyers = buyers;
	}

	protected void setOpen(boolean open) {
		this.open = open;
	}

	protected void setRemote(boolean remote) {
		this.remote = remote;
	}

	protected void setTradesLimit(int tradesLimit) {
		this.tradesLimit = tradesLimit;
	}

	protected void setAdminStock(boolean adminStock) {
		this.adminStock = adminStock;
	}

	// methods
	public boolean manageStock() {
		return getCurrentOwner() != null || adminStock;
	}

	public boolean areTradeConditionsValid(Player player, boolean errorMessage) {
		return tradeConditions.isValid(player, this, errorMessage);
	}

	public boolean hasDiscoveredAllItems(Player player, boolean errorMessage) {
		SSUser user = SSUser.get(player);
		for (TradeObject object : getObjects(null, Utils.asList(ObjectType.ITEM))) {
			if (!user.hasDiscoveredMaterial(((ObjectItem) object).getItem().getType(player))) {
				if (errorMessage) SSLocale.MSG_SUPREMESHOPS_DIDNTDISCOVERALLSHOPITEMS.send(player);
				return false;
			}
		}
		return true;
	}

	public Object[] getMessageReplacers(boolean withDetails, boolean fromModifiersCalculation, Player parser) {
		List<Object> replacers = buildMessageReplacers(withDetails, fromModifiersCalculation, parser);
		return replacers.toArray(new Object[replacers.size()]);
	}

	protected List<Object> buildMessageReplacers(boolean withDetails, boolean fromModifiersCalculation, Player parser) {
		List<Object> replacers = new ArrayList<Object>();
		// type
		replacers.add("{type}");
		replacers.add(type.name().toLowerCase());
		// owner
		replacers.add("{owner}");
		replacers.add(owner != null ? owner.toOfflinePlayer().getName() : "[admin]");
		// open
		replacers.add("{name}");
		replacers.add(displayName);
		// open
		replacers.add("{shop_open}");
		replacers.add(open ? SSLocaleMisc.MISC_SUPREMESHOPS_SHOPOPEN.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_SHOPCLOSED.getLine());
		// trade count and trades limit
		int tradesCount = parser == null ? 0 : SSUser.get(parser).getShopTradeCount(this);
		replacers.add("{trades_count}");
		replacers.add(tradesCount);
		replacers.add("{trades_limit}");
		replacers.add(tradesLimit > 0 ? "/" + tradesLimit : "");
		replacers.add("{trades_count_plural}");
		replacers.add(Utils.getPlural(tradesCount));
		// stock for trades
		if (withDetails && !fromModifiersCalculation && parser != null) {
			int possibleTradesPlayer = getPossibleTradesForPlayerStock(parser, parser);
			int possibleTradesShop = getPossibleTradesForShopStock(parser);
			int possibleTrades = Math.min(possibleTradesPlayer, possibleTradesShop);
			replacers.add("{possible_trades_player}");
			replacers.add(possibleTradesPlayer);
			replacers.add("{possible_trades_stop}");
			replacers.add(possibleTradesShop == Integer.MAX_VALUE ? "∞" : possibleTradesShop);
			replacers.add("{possible_trades}");
			replacers.add(possibleTrades);
		}
		// trade description
		if (withDetails && !fromModifiersCalculation) {
			// taken objects
			List<String> takenObjects = new ArrayList<String>();
			for (TradeObject object : getObjects(Utils.asList(ObjectSide.TAKING), null)) {
				takenObjects.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_TAKENOBJECTDESCRIPTIONLINE.getLines("{description}", object.describeForAmount(object.calculateAmountForTrades(this, 1, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), parser), parser)));
			}
			replacers.add("{taken_objects}");
			replacers.add(takenObjects.isEmpty() ? "/" : takenObjects);
			// given objects
			List<String> givenObjects = new ArrayList<String>();
			for (TradeObject object : getObjects(Utils.asList(ObjectSide.GIVING), null)) {
				givenObjects.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_GIVENOBJECTDESCRIPTIONLINE.getLines("{description}", object.describeForAmount(object.calculateAmountForTrades(this, 1, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), parser), parser)));
			}
			replacers.add("{given_objects}");
			replacers.add(givenObjects.isEmpty() ? "/" : givenObjects);
		}
		// stats
		if (withDetails) {
			// successful trades
			replacers.add("{successful_trades}");
			replacers.add(getBuyersTrades());
			// unique buyers
			replacers.add("{unique_buyers}");
			replacers.add(buyers.size());
		}
		// rankings
		if (withDetails) {
			// shop ranking (server)
			List<Shop> sortedServerShops = new ShopsSorter(SupremeShops.inst().getData().getShops().getElements(owner, null, false, ElementRemotePolicy.MIGHT_BE), SortCriteria.BY_RANKING_SERVER).getSortedList();
			replacers.add("{shop_server_ranking}");
			replacers.add(sortedServerShops.indexOf(this) + 1);
			replacers.add("{server_shops}");
			replacers.add(sortedServerShops.size());
			// shop ranking (seller)
			List<Shop> sortedSellerShops = new ShopsSorter(SupremeShops.inst().getData().getShops().getElements(owner, null, false, ElementRemotePolicy.MIGHT_BE), SortCriteria.BY_RANKING_SELLER).getSortedList();
			replacers.add("{shop_owner_ranking}");
			replacers.add(sortedSellerShops.indexOf(this) + 1);
			replacers.add("{owner_shops}");
			replacers.add(sortedSellerShops.size());
			// seller ranking
			Map<UserInfo, List<Shop>> shopsByOwner = SupremeShops.inst().getData().getShops().getAllByOwners(true);
			final Map<UserInfo, Integer> ownerSuccessfulTrades = new HashMap<UserInfo, Integer>();
			for (UserInfo own : shopsByOwner.keySet()) {
				int trades = 0;
				for (Shop sh : shopsByOwner.get(own)) {
					trades += sh.getBuyersTrades();
				}
				ownerSuccessfulTrades.put(own, trades);
			}
			List<UserInfo> allOwners = Utils.asList(shopsByOwner.keySet());
			Utils.sortList(allOwners, new Comparator<UserInfo>() {
				@Override
				public int compare(UserInfo o1, UserInfo o2) {
					return Integer.compare(ownerSuccessfulTrades.get(o1), ownerSuccessfulTrades.get(o2));
				}
			});
			replacers.add("{owner_ranking}");
			replacers.add(allOwners.indexOf(owner) + 1);
			replacers.add("{server_owners}");
			replacers.add(allOwners.size());
		}
		// return
		return replacers;
	}

	/**
	 * @return a list of merchants to push if "push" parameter is false ; otherwise null
	 */
	public List<Merchant> destroy(DestroyCause cause, boolean push) {
		// close GUIs
		SupremeShops.inst().getGeneralManager().closeRelatedGUIs(this, false);
		// unlink from merchants
		List<Merchant> merchantsToPush = new ArrayList<Merchant>();
		for (Merchant merchant : SupremeShops.inst().getData().getMerchants().getAll().values()) {
			if (merchant.getShopsIds().contains(getId())) {
				merchant.removeShop(this, false);
				merchantsToPush.add(merchant);
			}
		}
		if (!merchantsToPush.isEmpty() && push) {
			SupremeShops.inst().getData().getMerchants().pushAsync(merchantsToPush);
		}
		// give stock to owner
		if (manageStock() && owner != null) {
			OfflinePlayer player = owner.toOfflinePlayer();
			for (TradeObject give : objects.keySet()) {
				try {
					give.forceWithdrawAll(player, this);
				} catch (Throwable exception) {}
			}
		}
		// delete shop
		open = false;
		remote = false;
		if (push) SupremeShops.inst().getData().getShops().delete(this);
		// event
		Bukkit.getPluginManager().callEvent(new ShopDestroyedEvent(this, cause));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, "Destroyed shop (" + cause.name() + ")");
		// return
		return push ? null : merchantsToPush;
	}

	// methods : objects and stock
	public void addObject(TradeObject object) {
		addObject(object, 0d);
	}

	public void addObject(TradeObject object, Double stock) {
		addObject(object, stock, true);
	}

	public void addObject(TradeObject object, Double stock, boolean push) {
		if (!objects.containsKey(object)) {
			objects.put(object, stock);
			Bukkit.getPluginManager().callEvent(new ShopUpdatedObjectEvent(this, object, ShopUpdatedObjectEvent.Operation.ADD_OBJECT));
			if (push) pushAsync();
		}
	}

	public void addTradeCondition(Condition condition) {
		if (tradeConditions.getConditions().getElement(condition.getId()) == null) {
			tradeConditions.getConditions().addElement(condition);
			Bukkit.getPluginManager().callEvent(new ShopUpdatedTradeConditionsEvent(this, condition, ShopUpdatedTradeConditionsEvent.Operation.ADD_CONDITION));
			pushAsync();
		}
	}

	public void removeObject(TradeObject object) {
		removeObject(object, true);
	}

	public void removeObject(TradeObject object, boolean push) {
		if (objects.remove(object) != null) {
			Bukkit.getPluginManager().callEvent(new ShopUpdatedObjectEvent(this, object, ShopUpdatedObjectEvent.Operation.REMOVE_OBJECT));
			if (push) pushAsync();
		}
	}

	public void removeTradeCondition(Condition condition) {
		if (tradeConditions.getConditions().removeElement(condition.getId()) != null) {
			Bukkit.getPluginManager().callEvent(new ShopUpdatedTradeConditionsEvent(this, condition, ShopUpdatedTradeConditionsEvent.Operation.REMOVE_CONDITION));
			pushAsync();
		}
	}

	public void restockAll(Player player) {
		// can't restock
		if (!manageStock()) {
			return;
		}
		// see how much we can restock
		boolean restocked = false;
		for (final TradeObject object : getObjects(Utils.asList(ObjectSide.GIVING), null)) {
			if (object.restockMax(player, null, new ObjectRestockLogic() {
				@Override
				public OfflinePlayer getCurrentOwner() {
					return owner == null ? null : owner.toOfflinePlayer();
				}
				@Override
				public double getTotalObjectStock(List<ObjectType> types, boolean withManagersWages) {
					return Shop.this.getTotalObjectStock(types, withManagersWages);
				}
				@Override
				public void addStockAmount(double amount) {
					objects.put(object, objects.get(object) + amount);
				}
			}) > 0d) {
				restocked = true;
			}
		}
		// push
		if (restocked) {
			pushAsync();
		}
	}

	public int getPossibleTradesForPlayerStock(Player player, Player parser) {
		Collection<TradeObject> taking = getObjects(Utils.asList(ObjectSide.TAKING), null);
		if (taking.size() == 0) return 0;
		for (int maxForPlayer = 0, tries = 0; true; ++maxForPlayer) {
			int current = maxForPlayer + 1;
			for (TradeObject take : taking) {
				if (take.getPlayerStock(player) < take.calculateAmountForTrades(this, current, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), parser)) {
					return maxForPlayer;
				}
			}
			if (++tries > 5000) return maxForPlayer;// it's still an infinite loop, who knows what could happen LMAO LOL SERVER CRASH NOT MY FAULT, SILENCE, CUSTOMER
		}
	}

	public int getPossibleTradesForShopStock(Player parser) {
		Collection<TradeObject> giving = getObjects(Utils.asList(ObjectSide.GIVING), null);
		if (giving.size() == 0) return 0;
		if (manageStock()) {
			for (int maxForShop = 0, tries = 0; true; ++maxForShop) {
				int current = maxForShop + 1;
				for (TradeObject give : giving) {
					if (getObjectStock(give) < give.calculateAmountForTrades(this, current, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), parser)) {
						return maxForShop;
					}
				}
				if (++tries > 5000) return maxForShop;// it's still an infinite loop, who knows what could happen LMAO LOL SERVER CRASH NOT MY FAULT, SILENCE, CUSTOMER
			}
		} else {
			return Integer.MAX_VALUE;
		}
	}

	public int getPossibleTradesWith(Player player, Player parser) {
		int maxForPlayer = getPossibleTradesForPlayerStock(player, parser);
		return maxForPlayer == 0 ? 0 : Math.min(maxForPlayer, getPossibleTradesForShopStock(parser));
	}

	// methods : change
	public void changeActualOwner(UserInfo owner) {
		// set owner
		setActualOwner(owner);
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.SET_ACTUAL_OWNER));
		// save
		pushAsync();
	}

	public void changeDisplayName(String displayName) {
		// set display name
		setDisplayName(displayName);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.SET_DISPLAY_NAME));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, "Set display name to " + displayName);
	}

	public void changeObjectStock(TradeObject object, Double stock) {
		// set stock
		setObjectStock(object, stock);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedObjectEvent(this, object, ShopUpdatedObjectEvent.Operation.SET_STOCK));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, "{OBJECT " + object.getType().getId() + "} {SIDE " + object.getSide().name() + "}", "Set stock to " + stock);
	}

	public void changeTradesLimit(int limit) {
		// set limit
		setTradesLimit(limit);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.SET_TRADES_LIMIT));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, "Set trades limit to " + limit);
	}

	public void changeAdminStock(boolean adminStock) {
		// set stock
		setAdminStock(adminStock);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.TOGGLE_ADMIN_STOCK));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, (adminStock ? "Enabled" : "Disabled") + " admin stock");
	}

	public void changeModifyObjectStock(TradeObject object, Double delta) {
		// set stock
		setObjectStock(object, getObjectStock(object) + delta);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedObjectEvent(this, object, ShopUpdatedObjectEvent.Operation.SET_STOCK));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, "{OBJECT " + object.getType().getId() + "} {SIDE " + object.getSide().name() + "}", "Modified stock with " + delta);
	}

	public void changeOpen(boolean open) {
		// set open
		setOpen(open);
		pushAsync();
		// close GUIs
		if (!open) {
			SupremeShops.inst().getGeneralManager().closeRelatedGUIs(this, true);
		}
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.TOGGLE_OPEN));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, (open ? "Opened shop" : "Closed shop"));
		// remove display items (will be re-added soon anyway)
		if (this instanceof ItemsDisplayable && SupremeShops.inst().getGeneralManager().getItemsDisplayableManager() != null) {
			SupremeShops.inst().getGeneralManager().getItemsDisplayableManager().stop((ItemsDisplayable) this);
		}
	}

	public void changeRemote(boolean remote) {
		// set remote
		setRemote(remote);
		pushAsync();
		// unlink from merchants if toggled off
		if (!remote) {
			List<Merchant> toPush = new ArrayList<Merchant>();
			for (Merchant merchant : SupremeShops.inst().getData().getMerchants().getAll().values()) {
				if (merchant.removeShop(this, false)) {
					toPush.add(merchant);
				}
			}
			if (!toPush.isEmpty()) {
				SupremeShops.inst().getData().getMerchants().pushAsync(toPush);
			}
		}
		// close GUIs
		if (!remote) {
			SupremeShops.inst().getGeneralManager().closeRelatedGUIs(this, true);
		}
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.TOGGLE_REMOTE));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, (remote ? "Enabled remote" : "Disabled remote"));
	}

	// methods : trade
	public boolean attemptTradesProcessing(final Player player, int trades, Merchant merchant) {
		// invalid amount
		if (trades <= 0) {
			return false;
		}
		// closed
		if (!open) {
			SSLocale.MSG_SUPREMESHOPS_SHOPCLOSED.send(player);
			return false;
		}
		// invalid trade conditions
		if (!areTradeConditionsValid(player, true)) {
			// sound
			if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
				SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
			}
			return false;
		}
		// can't trade in spectator mode
		if (player.getGameMode().toString().equals("SPECTATOR")) {// don't use the actual field, see https://gitlab.com/GuillaumeVDN/SupremeShops/issues/6
			SSLocale.MSG_SUPREMESHOPS_DISALLOWEDGAMEMODE.send(player);
			// sound
			if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
				SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
			}
			return false;
		}
		// trade limit reached
		SSUser user = SSUser.get(player);
		if (tradesLimit > 0 && user.getShopTradeCount(this) + trades > tradesLimit) {
			SSLocale.MSG_SUPREMESHOPS_TRADESLIMITWOULDEXCEEDSHOP.send(player);
			return false;
		}
		// merchant trade limit reached
		if (merchant != null && merchant.getTradesLimit() > 0 && user.getMerchantTradeCount(merchant) + trades > merchant.getTradesLimit()) {
			SSLocale.MSG_SUPREMESHOPS_TRADESLIMITWOULDEXCEEDMERCHANT.send(player);
			return false;
		}
		// nothing to trade
		Collection<TradeObject> giving = getObjects(Utils.asList(ObjectSide.GIVING), null);
		Collection<TradeObject> taking = getObjects(Utils.asList(ObjectSide.TAKING), null);
		if (giving.isEmpty() && taking.isEmpty()) {
			return false;
		}
		// event
		ShopTradeProcessEvent tradeEvent = new ShopTradeProcessEvent(this, player, trades, merchant);
		Bukkit.getPluginManager().callEvent(tradeEvent);
		if (tradeEvent.isCancelled()) {
			return false;
		}
		trades = tradeEvent.getTrades();
		// calculate amounts for objects
		Map<TradeObject, Double> amounts = new HashMap<TradeObject, Double>();
		for (TradeObject give : giving) {
			amounts.put(give, give.calculateAmountForTrades(this, trades, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), player));
		}
		for (TradeObject take : taking) {
			amounts.put(take, take.calculateAmountForTrades(this, trades, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), player));
		}
		// ensure there's enough stock in the shop
		if (manageStock()) {
			for (TradeObject give : giving) {
				if (getObjectStock(give) < amounts.get(give)) {
					SSLocale.MSG_SUPREMESHOPS_NOSTOCK.send(player);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
					}
					return false;
				}
			}
		}
		// ensure the player has enough stock
		Map<ItemStack, Double> alreadyCheckedAmounts = new HashMap<>();
		for (TradeObject take : taking) {
			double playerStock = take.getPlayerStock(player);
			// adapt player stock for similar items
			// if we have two "item" objects with the exact same item, only add one to the amounts map and remove it from the taking map
			// (otherwise if we have for instance 64 + 36, since checks are made independently below, the player can have only 64 and both checks will validate since 64 >= 64 and 64 >= 36)
			if (take instanceof ObjectItem) {
				ItemStack it = ((ObjectItem) take).getItem().getParsedValue(player).getItemStack();
				boolean processed = false;
				for (ItemStack alreadyCheckedIt : Utils.asList(alreadyCheckedAmounts.keySet())) {
					if (alreadyCheckedIt.isSimilar(it)) {
						double alreadyCheckedAmount = alreadyCheckedAmounts.get(alreadyCheckedIt);
						playerStock -= alreadyCheckedAmount;
						alreadyCheckedAmounts.put(alreadyCheckedIt, alreadyCheckedAmount + it.getAmount());
						processed = true;
						break;
					}
				}
				if (!processed) {
					alreadyCheckedAmounts.put(it, (double) it.getAmount());
				}
			}
			// check
			if (playerStock < amounts.get(take)) {
				take.sendHasntMessage(player, amounts.get(take));
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
				}
				return false;
			}
		}
		// take
		for (TradeObject take : taking) {
			// take object from the player
			take.take(player, amounts.get(take));
			// apply taxes modifiers and set stock
			setObjectStock(take, getObjectStock(take) + Modifier.applyModifiers(take, this, amounts.get(take), trades, SupremeShops.inst().getModuleManager().getTradeModifiersTaxes().values(), player));
		}
		// give
		for (TradeObject give : giving) {
			// set stock
			setObjectStock(give, getObjectStock(give) - amounts.get(give));
			// apply taxes modifiers and give object to the player
			give.give(player, Modifier.applyModifiers(give, this, amounts.get(give), trades, SupremeShops.inst().getModuleManager().getTradeModifiersTaxes().values(), player));
		}
		// sound
		if (SupremeShops.inst().getModuleManager().getShopTradeSound() != null) {
			SupremeShops.inst().getModuleManager().getShopTradeSound().play(player);
		}
		// shop and user stat
		UserInfo buyerInfo = new UserInfo(player);
		buyers.put(buyerInfo, getBuyerTrades(buyerInfo) + trades);
		user.setShopTradeCount(this, user.getShopTradeCount(this) + trades);
		// merchant and user stat
		if (merchant != null) {
			merchant.addBuyerTrades(buyerInfo, trades);
			user.setMerchantTradeCount(merchant, user.getMerchantTradeCount(merchant) + trades);
		}
		// stat
		GCoreLegacy.inst().getData().getStatistics().add("supremeshops_trades", player.getUniqueId(), trades);
		SupremeShops.inst().getData().getBoard().set(MainBoard.KEY_MISC_SUCCESSFUL_TRADES, SupremeShops.inst().getData().getBoard().get(MainBoard.KEY_MISC_SUCCESSFUL_TRADES, 0d) + (double) trades);
		if (owner != null) GCoreLegacy.inst().getData().getStatistics().add("supremeshops_trades_in_owned_shop", owner.getUniqueId(), trades);
		// if advanced economy is enabled for this shop
		if (SupremeShops.inst().getModuleManager().getItemValueManager().getShopSetting(this).equals(ItemValueSetting.ENABLED)) {
			// get items in trade
			final Map<ItemData, ObjectSide> toApply = new HashMap<ItemData, ObjectSide>();
			for (TradeObject object : getObjects(null, Utils.asList(ObjectType.ITEM))) {
				toApply.put(((ObjectItem) object).getItem(player), object.getSide());
			}
			if (!toApply.isEmpty()) {
				// apply modifiers asynchronously
				final int finalTrades = trades;
				new BukkitRunnable() {
					@Override
					public void run() {
						for (ItemData item : toApply.keySet()) {
							SupremeShops.inst().getModuleManager().getItemValueManager().applyPostTradeModifier(item, toApply.get(item), finalTrades);
							SupremeShops.inst().getModuleManager().getItemValueManager().applyPostOppositeTradeModifier(item, toApply.get(item), finalTrades);
						}
						// and then resync to update shops
						new BukkitRunnable() {
							@Override
							public void run() {
								// update similar shops
								SupremeShops.inst().getGeneralManager().updateShopsWithItems(toApply.keySet(), Shop.this, player);
							}
						}.runTask(SupremeShops.inst());
					}
				}.runTaskAsynchronously(SupremeShops.inst());
			}
		}
		// event
		Bukkit.getPluginManager().callEvent(new ShopTradeProcessedEvent(this, player, trades, merchant));
		// log
		SupremeShops.inst().tradeLog(this, merchant, player, trades);
		// push
		pushAsync();
		return true;
	}

	// abstract methods
	public abstract Shop asFake();
	public abstract Class<?> getJsonDataClass();
	public abstract void readJsonData(Object jsonData);
	public abstract Object writeJsonData();

	// overriden
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Shop other = (Shop) obj;
		if (id == null) {
			return other.id == null;
		} else return id.equals(other.id);
	}

	// board and id
	@Override
	protected ShopBoard getBoard() {
		return SupremeShops.inst().getData().getShops();
	}

	@Override
	public String getDataId() {
		return getId();
	}

	// data
	private transient boolean loadError = false;

	public boolean hasLoadError() {
		return loadError;
	}

	@Override
	public final void jsonPull() {
		// not saved
		if (getJsonDataClass() == null) return;
		// pull
		loadError = true;
		File file = getBoard().getJsonFile(this);
		if (file.exists()) {
			Object data = Utils.loadFromGson(getJsonDataClass(), file, true, SupremeShops.GSON);
			if (data != null) {
				readJsonData(data);
				loadError = false;
			}
		}
	}

	@Override
	protected final void jsonPush() {
		// not saved
		if (getJsonDataClass() == null) return;
		// push
		File file = getBoard().getJsonFile(this);
		Utils.saveToGson(writeJsonData(), file, SupremeShops.GSON);
	}

	@Override
	protected final void jsonDelete() {
		// not saved
		if (getJsonDataClass() == null) return;
		// delete
		File file = getBoard().getJsonFile(this);
		if (file.exists()) {
			file.delete();
		}
	}

	// MYSQL
	@Override
	public final void mysqlPull(ResultSet set) throws SQLException {
		// not saved
		if (getJsonDataClass() == null) return;
		// pull
		loadError = true;
		Object data = SupremeShops.GSON.fromJson(set.getString("data"), getJsonDataClass());
		if (data != null) {
			readJsonData(data);
			loadError = false;
		}
	}

	@Override
	protected final Query getMySQLPullQuery() {
		// not saved
		if (getJsonDataClass() == null) return new Query();
		// build query
		return new Query("SELECT * FROM `" + getBoard().getMySQLTable() + "` WHERE `id`=?;", getDataId());
	}

	@Override
	protected final Query getMySQLPushQuery() {
		// not saved
		if (getJsonDataClass() == null) return new Query();
		// build queryb
		return new Query("REPLACE INTO `" + getBoard().getMySQLTable() + "`(`id`,`type`,`owner`,`data`) VALUES(?,?,?,?);",
				getDataId(), type.name(), owner == null ? "null" : owner.toString(), SupremeShops.GSON.toJson(writeJsonData()));
	}

	protected final Query getMySQLDeleteQuery() {
		// not saved
		if (getJsonDataClass() == null) return new Query();
		// build query
		return new Query("DELETE FROM `" + getBoard().getMySQLTable() + "` WHERE `id`=?;", getDataId());
	}

}
