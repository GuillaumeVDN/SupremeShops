package com.guillaumevdn.supremeshops.module.shop;

import java.util.Map;
import java.util.Set;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.fake.FakeGuiShop;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;

public class GuiShop extends PlayerShop {

	// base
	private GuiShop(String id) {
		super(id, ShopType.GUI);
	}

	public GuiShop(String id, UserInfo owner) {
		super(id, ShopType.GUI, owner, false, true);
	}

	// get
	@Override
	public UserInfo getCurrentOwner() {
		return getActualOwner();
	}

	@Override
	public boolean isCurrentOwnerAdmin() {
		return isActualOwnerAdmin();
	}

	// fake
	@Override
	public GuiShop asFake() {
		return new FakeGuiShop(this);
	}

	// data
	public static class GuiShopJsonData {

		private final UserInfo owner;
		private final String displayName;
		private final Map<TradeObject, Double> objects;
		private final CPConditions tradeConditions;
		private final boolean open;
		private final boolean remote;
		private final int tradesLimit;
		private final boolean adminStock;
		private final Map<UserInfo, Set<ShopManagementPermission>> managers;
		private final Map<UserInfo, Map<TradeObject, Double>> managersWages;
		private final Map<UserInfo, Long> lastPaidManagersWages;
		private final Map<UserInfo, Integer> lateManagersWages;
		private final Map<UserInfo, Integer> buyers;

		public GuiShopJsonData(GuiShop shop) {
			this.owner = shop.getActualOwner();
			this.displayName = shop.getDisplayName();
			this.objects = shop.getObjectsMap();
			this.tradeConditions = shop.getTradeConditions();
			this.open = shop.isOpen();
			this.remote = shop.isRemote();
			this.tradesLimit = shop.getTradesLimit();
			this.adminStock = shop.getAdminStock();
			this.managers = shop.getManagers();
			this.managersWages = shop.getManagersWages();
			this.lastPaidManagersWages = shop.getLastPaidManagersWages();
			this.lateManagersWages = shop.getLateManagersWages();
			this.buyers = shop.getBuyers();
		}

	}

	@Override
	public Class<?> getJsonDataClass() {
		return GuiShopJsonData.class;
	}

	@Override
	public void readJsonData(Object jsonData) {
		// invalid class
		if (!Utils.instanceOf(jsonData, getJsonDataClass())) {
			throw new IllegalArgumentException("data class is " + jsonData.getClass() + ", expected is " + getJsonDataClass().getName());
		}
		// read data
		GuiShopJsonData data = (GuiShopJsonData) jsonData;
		setActualOwner(data.owner);
		setDisplayName(data.displayName);
		setObjects(data.objects);
		setTradeConditions(data.tradeConditions);
		setOpen(data.open);
		setRemote(data.remote);
		setTradesLimit(data.tradesLimit);
		setAdminStock(data.adminStock);
		setManagers(data.managers);
		setManagersWages(data.managersWages);
		setLastPaidManagersWages(data.lastPaidManagersWages);
		setLateManagersWages(data.lateManagersWages);
		setBuyers(data.buyers);
	}

	@Override
	public Object writeJsonData() {
		return new GuiShopJsonData(this);
	}

}
