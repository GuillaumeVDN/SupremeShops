package com.guillaumevdn.supremeshops.module.shop;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.shop.ShopUpdatedEvent;
import com.guillaumevdn.supremeshops.module.display.particles.SingleParticlesDisplayable;
import com.guillaumevdn.supremeshops.module.display.sign.SignDisplayable;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.particlepatternassignable.ParticlePatternAssignable;
import com.guillaumevdn.supremeshops.module.shop.fake.FakeSignShop;
import com.guillaumevdn.supremeshops.util.DestroyCause;
import com.guillaumevdn.supremeshops.util.Locatable;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePatternAvailability;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticleScript;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticleScriptExecution;

public class SignShop extends PlayerShop implements Locatable, SignDisplayable, SingleParticlesDisplayable, ParticlePatternAssignable {

	// base
	private BlockCoords sign;
	private String particlePatternId;

	private SignShop(String id) {
		super(id, ShopType.SIGN);
	}

	protected SignShop(String id, ShopType type) {
		super(id, type);
	}

	public SignShop(String id, UserInfo owner, BlockCoords sign) {
		this(id, ShopType.SIGN, owner, sign);
	}

	protected SignShop(String id, ShopType type, UserInfo owner, BlockCoords sign) {
		super(id, type, owner, false, false);
		this.sign = sign;
	}

	// get
	@Override
	public World getWorld() {
		return sign.getWorld();
	}

	@Override
	public BlockCoords getBlock() {
		return sign;
	}

	@Override
	public Location getLocation() {
		return sign.toLocation();
	}

	@Override
	public BlockCoords getSign() {
		return sign;
	}

	@Override
	public UserInfo getCurrentOwner() {
		return getActualOwner();
	}

	@Override
	public boolean isCurrentOwnerAdmin() {
		return isActualOwnerAdmin();
	}

	// particle pattern
	@Override
	public String getParticlePatternId() {
		return particlePatternId;
	}

	@Override
	public ParticlePattern getParticlePattern() {
		return particlePatternId == null ? null : SupremeShops.inst().getModuleManager().getParticlePattern(particlePatternId);
	}

	@Override
	public List<ParticlePattern> getAvailableParticlePatterns(Player player) {
		List<ParticlePattern> patterns = new ArrayList<ParticlePattern>();
		for (ParticlePattern pattern : SupremeShops.inst().getModuleManager().getParticlePatterns().values()) {
			if (pattern.getAvailability().equals(ParticlePatternAvailability.SIGN) && (isCurrentOwnerAdmin() || (pattern.getPermission() == null || pattern.getPermission().has(getCurrentOwner().toOfflinePlayer())))) {
				patterns.add(pattern);
			}
		}
		return patterns;
	}

	// sign displayable
	@Override
	public List<String> getSignLines() {
		return SSLocaleMisc.MISC_SUPREMESHOPS_SIGNSHOPSIGNLINES.getLines(getMessageReplacers(true, false, null));
	}

	// particles displayable
	@Override
	public boolean mustDisplayParticles() {
		return isOpen();
	}

	@Override
	public ParticlePattern getCurrentParticlePattern() {
		return getParticlePattern();
	}

	@Override
	public boolean areCurrentParticlesConditionsValid(Player player) {
		return areTradeConditionsValid(player, false);
	}

	@Override
	public Location getParticlesBase() {
		return getSign().toLocation();
	}

	// set
	protected void setSign(BlockCoords sign) {
		this.sign = sign;
	}

	protected void setParticlePatternId(String particlePatternId) {
		this.particlePatternId = particlePatternId;
	}

	// methods
	@Override
	public List<Merchant> destroy(DestroyCause cause, boolean push) {
		// remove sign
		try {
			sign.toBlock().getWorld().dropItem(sign.toLocation(), new ItemStack(Mat.fromBlock(sign.toBlock()).getCurrentMaterial()));
			Mat.AIR.setBlock(sign.toBlock());
		} catch (Throwable ignored) {
			try {
				sign.toBlock().breakNaturally();
			} catch (Throwable ignored2) {}
		}
		// destroy
		return super.destroy(cause, push);
	}

	@Override
	protected List<Object> buildMessageReplacers(boolean withDetails, boolean fromModifiersCalculation, Player parser) {
		List<Object> replacers = super.buildMessageReplacers(withDetails, fromModifiersCalculation, parser);
		// location
		replacers.add("{location}");
		replacers.add(sign.toString());
		// return
		return replacers;
	}

	// methods : change
	@Override
	public void changeDisplayName(String displayName) {
		super.changeDisplayName(displayName);
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	@Override
	public void changeOpen(boolean open) {
		super.changeOpen(open);
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	@Override
	public void changeActualOwner(UserInfo owner) {
		super.changeActualOwner(owner);
		SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(this);
	}

	@Override
	public void changeParticlePattern(String particlePatternId) {
		// set particle pattern id
		setParticlePatternId(particlePatternId);
		pushAsync();
		// event
		Bukkit.getPluginManager().callEvent(new ShopUpdatedEvent(this, ShopUpdatedEvent.Operation.SET_PARTICLE_PATTERN));
		// log
		SupremeShops.inst().pluginLog(this, null, null, null, null, particlePatternId == null ? "Removed particle pattern" : "Set particle pattern to" + particlePatternId);
	}

	// methods : trade
	@Override
	public boolean attemptTradesProcessing(Player player, int trades, Merchant merchant) {
		// trade
		if (!super.attemptTradesProcessing(player, trades, merchant)) {
			return false;
		}
		// display on buy particle scripts
		ParticleScript script = SupremeShops.inst().getModuleManager().getOnBuyParticleScript(player);
		if (script != null) {
			final ParticleScriptExecution execution = new ParticleScriptExecution(script, sign.toLocation());
			new BukkitRunnable() {
				@Override
				public void run() {
					if (execution.update(null, true)) {
						cancel();
					}
				}
			}.runTaskTimerAsynchronously(SupremeShops.inst(), 0L, 1L);
		}
		// ok
		return true;
	}

	// fake
	@Override
	public SignShop asFake() {
		return new FakeSignShop(this);
	}

	// data
	public static class SignShopJsonData {

		private final UserInfo owner;
		private final String displayName;
		private final Map<TradeObject, Double> objects;
		private final CPConditions tradeConditions;
		private final boolean open;
		private final boolean remote;
		private final int tradesLimit;
		private final boolean adminStock;
		private final Map<UserInfo, Set<ShopManagementPermission>> managers;
		private final Map<UserInfo, Map<TradeObject, Double>> managersWages;
		private final Map<UserInfo, Long> lastPaidManagersWages;
		private final Map<UserInfo, Integer> lateManagersWages;
		private final Map<UserInfo, Integer> buyers;
		private final BlockCoords sign;
		private final String particlePatternId;

		public SignShopJsonData(SignShop shop) {
			this.owner = shop.getActualOwner();
			this.displayName = shop.getDisplayName();
			this.objects = shop.getObjectsMap();
			this.tradeConditions = shop.getTradeConditions();
			this.open = shop.isOpen();
			this.remote = shop.isRemote();
			this.tradesLimit = shop.getTradesLimit();
			this.adminStock = shop.getAdminStock();
			this.managers = shop.getManagers();
			this.managersWages = shop.getManagersWages();
			this.lastPaidManagersWages = shop.getLastPaidManagersWages();
			this.lateManagersWages = shop.getLateManagersWages();
			this.buyers = shop.getBuyers();
			this.sign = shop.getSign();
			this.particlePatternId = shop.getParticlePatternId();
		}

	}

	@Override
	public Class<?> getJsonDataClass() {
		return SignShopJsonData.class;
	}

	@Override
	public void readJsonData(Object jsonData) {
		// invalid class
		if (!Utils.instanceOf(jsonData, getJsonDataClass())) {
			throw new IllegalArgumentException("data class is " + jsonData.getClass() + ", expected is " + getJsonDataClass().getName());
		}
		// read data
		SignShopJsonData data = (SignShopJsonData) jsonData;
		setActualOwner(data.owner);
		setDisplayName(data.displayName);
		setObjects(data.objects);
		setTradeConditions(data.tradeConditions);
		setOpen(data.open);
		setRemote(data.remote);
		setTradesLimit(data.tradesLimit);
		setAdminStock(data.adminStock);
		setManagers(data.managers);
		setManagersWages(data.managersWages);
		setLastPaidManagersWages(data.lastPaidManagersWages);
		setLateManagersWages(data.lateManagersWages);
		setBuyers(data.buyers);
		setSign(data.sign);
		setParticlePatternId(data.particlePatternId);
	}

	@Override
	public Object writeJsonData() {
		return new SignShopJsonData(this);
	}

}
