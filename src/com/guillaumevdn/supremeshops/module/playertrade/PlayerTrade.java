package com.guillaumevdn.supremeshops.module.playertrade;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.inventory.Inventory;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.ChatInput;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.playertrade.PlayerTradePreprocessEvent;
import com.guillaumevdn.supremeshops.api.event.playertrade.PlayerTradeProcessEvent;
import com.guillaumevdn.supremeshops.gui.playertrade.PlayerTradeGUI;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.object.type.ObjectItem;
import com.guillaumevdn.supremeshops.module.object.type.ObjectVaultMoney;

public class PlayerTrade implements Listener {

	// base
	private Player player1, player2;
	private List<TradeObject> offering1 = new ArrayList<TradeObject>(), offering2 = new ArrayList<TradeObject>();
	private boolean ready1 = false, ready2 = false;
	private boolean chatting1 = false, chatting2 = false;
	private PlayerTradeGUI gui1 = null, gui2 = null;
	private boolean done = false;

	public PlayerTrade(Player player1, Player player2) {
		this.player1 = player1;
		this.player2 = player2;
	}

	// get
	public Player getPlayer1() {
		return player1;
	}

	public Player getPlayer2() {
		return player2;
	}

	public boolean isLocked() {
		return ready1 || ready2;
	}

	public boolean isReady(Player player) {
		return player.equals(player1) ? ready1 : (player.equals(player2) && ready2);
	}

	public boolean isChatting(Player player) {
		return player.equals(player1) ? chatting1 : (player.equals(player2) && chatting2);
	}

	public List<TradeObject> getOffering(Player player) {
		if (player.equals(player1)) {
			return getOffering1();
		} else if (player.equals(player2)) {
			return getOffering2();
		}
		return null;
	}

	public List<TradeObject> getOffering1() {
		return Collections.unmodifiableList(offering1);
	}

	public List<TradeObject> getOffering2() {
		return Collections.unmodifiableList(offering2);
	}

	public boolean isDone() {
		return done;
	}

	// set
	public void setReady(Player player, boolean ready) {
		// set ready
		if (player.equals(player1)) {
			this.ready1 = ready;
		} else if (player.equals(player2)) {
			this.ready2 = ready;
		} else {
			return;
		}
		// proceed or update items
		if (ready1 && ready2) {
			proceed();
		} else {
			gui1.updateReadyItems();
			gui2.updateReadyItems();
			// notify unbalanced
			if (SupremeShops.inst().getModuleManager().getItemValueManager().getNotifyUnbalancedPlayerTrades()) {
				// get all values
				double giving1 = 0d, giving2 = 0d;
				for (TradeObject object : offering1) {
					if (object instanceof ObjectVaultMoney) {
						giving1 += object.getCustomAmount(player2);
					} else if (object instanceof ObjectItem) {
						giving1 += SupremeShops.inst().getModuleManager().getItemValueManager().calculateItemValue(((ObjectItem) object).getItem(player2), object.getSide(), true);
					}
				}
				for (TradeObject object : offering2) {
					if (object instanceof ObjectVaultMoney) {
						giving2 += object.getCustomAmount(player1);
					} else if (object instanceof ObjectItem) {
						giving2 += SupremeShops.inst().getModuleManager().getItemValueManager().calculateItemValue(((ObjectItem) object).getItem(player1), object.getSide(), true);
					}
				}
				// notify eventually
				SupremeShops.inst().getModuleManager().getItemValueManager().notifyUnbalancedItemsValue(player1, player2, giving1, giving2);
			}
		}
	}

	// methods
	public void openGUI(Player player) {
		if (player.equals(player1)) {
			gui1.open(player);
		} else if (player.equals(player2)) {
			gui2.open(player);
		}
	}

	public void chat(Player player) {
		// set chatting
		if (player.equals(player1)) {
			if (chatting1) return;// already chatting
			chatting1 = true;
		} else if (player.equals(player2)) {
			if (chatting2) return;// already chatting
			chatting2 = true;
		} else {
			return;
		}
		// await chat
		player.closeInventory();
		// enter message in chat
		SSLocale.MSG_SUPREMESHOPS_PLAYERTRADEMESSAGEINPUT.send(player);
		GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
			@Override
			public void onChat(Player player, String value) {
				// not cancelling
				if (!Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
					SSLocale.MSG_SUPREMESHOPS_PLAYERTRADEMESSAGE.send(Utils.asList(player1, player2), "{sender}", player.getName(), "{receiver}", player.equals(player1) ? player2.getName() : player1.getName(), "{message}", value);
				}
				// reset chatting and open GUI
				if (player.equals(player1)) {
					chatting1 = false;
					gui1.open(player);
				} else if (player.equals(player2)) {
					chatting2 = false;
					gui2.open(player);
				}
			}
		});
	}

	public void addOffering(Player player, TradeObject object) {
		if (player.equals(player1)) {
			offering1.add(object);
			gui2.updateOtherOffering();
		} else if (player.equals(player2)) {
			offering2.add(object);
			gui1.updateOtherOffering();
		}
	}

	public void removeOffering(Player player, TradeObject object) {
		if (player.equals(player1)) {
			if (offering1.remove(object)) {
				object.give(player1, object.getCustomAmount(null));
				gui2.updateOtherOffering();
			}
		} else if (player.equals(player2)) {
			if (offering2.add(object)) {
				object.give(player2, object.getCustomAmount(null));
				gui1.updateOtherOffering();
			}
		}
	}

	public void start() {
		if (gui1 != null) return;
		// create GUIs
		(gui1 = new PlayerTradeGUI(this, player1)).open(player1);
		(gui2 = new PlayerTradeGUI(this, player2)).open(player2);
		// register events
		Bukkit.getPluginManager().registerEvents(this, SupremeShops.inst());
	}

	public void proceed() {
		// done
		if (done) return;
		// preprocess event
		PlayerTradePreprocessEvent event = new PlayerTradePreprocessEvent(this);
		Bukkit.getPluginManager().callEvent(event);
		if (event.isCancelled()) {
			return;
		}
		// set done and unregister
		SupremeShops.inst().getGeneralManager().getPlayerTrades().remove(this);
		done = true;
		// unregister GUIs and events
		gui1.unregister();
		gui2.unregister();
		HandlerList.unregisterAll(this);
		// give to players
		for (TradeObject object : offering1) {
			object.give(player2, object.getCustomAmount(null));
		}
		for (TradeObject object : offering2) {
			object.give(player1, object.getCustomAmount(null));
		}
		// sound
		if (SupremeShops.inst().getModuleManager().getPlayerTradeSound() != null) {
			SupremeShops.inst().getModuleManager().getPlayerTradeSound().play(player1, player2);
		}
		// message
		SSLocale.MSG_SUPREMESHOPS_PLAYERTRADESUCCESS.send(player1, "{player}", player2.getName());
		SSLocale.MSG_SUPREMESHOPS_PLAYERTRADESUCCESS.send(player2, "{player}", player1.getName());
		// event
		Bukkit.getPluginManager().callEvent(new PlayerTradeProcessEvent(this));
	}

	public void cancel() {
		// done
		if (done) return;
		// set done and unregister
		SupremeShops.inst().getGeneralManager().getPlayerTrades().remove(this);
		done = true;
		// unregister GUIs and events
		gui1.unregister();
		gui2.unregister();
		HandlerList.unregisterAll(this);
		// give to players
		for (TradeObject object : offering1) {
			object.give(player1, object.getCustomAmount(null));
		}
		for (TradeObject object : offering2) {
			object.give(player2, object.getCustomAmount(null));
		}
		// message
		SSLocale.MSG_SUPREMESHOPS_PLAYERTRADEFAIL.send(player1, "{player}", player2.getName());
		SSLocale.MSG_SUPREMESHOPS_PLAYERTRADEFAIL.send(player2, "{player}", player1.getName());
	}

	// events
	@EventHandler(priority = EventPriority.LOWEST)
	public void event(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		if (player.equals(player1) || player.equals(player2)) {
			cancel();
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(EntityDamageEvent event) {
		if (!Utils.instanceOf(event.getEntity(), Player.class)) return;
		Player player = (Player) event.getEntity();
		Inventory open = player.getOpenInventory() != null ? player.getOpenInventory().getTopInventory() : null;
		if (open != null && ((player.equals(player1) && gui1.getPage(0).equals(open)) || (player.equals(player2) && gui2.getPage(0).equals(open)))) {
			player.closeInventory();
		}
	}

	@EventHandler(priority = EventPriority.LOWEST)
	public void event(InventoryCloseEvent event) {
		Player player = (Player) event.getPlayer();
		Inventory open = player.getOpenInventory() != null ? player.getOpenInventory().getTopInventory() : null;
		if (open != null && !done && ((player.equals(player1) && open.equals(gui1.getPage(0))) || (player.equals(player2) && open.equals(gui2.getPage(0))))) {
			SSLocale.MSG_SUPREMESHOPS_PLAYERTRADECLOSEDGUI.send(player);
		}
	}

}
