package com.guillaumevdn.supremeshops.module.trigger;

import java.lang.reflect.Constructor;
import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ConfigData;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.trigger.type.AdminShopTriggerBlock;
import com.guillaumevdn.supremeshops.module.trigger.type.AdminShopTriggerCitizensNpc;
import com.guillaumevdn.supremeshops.module.trigger.type.AdminShopTriggerEntity;
import com.guillaumevdn.supremeshops.module.trigger.type.AdminShopTriggerMythicmobsMob;
import com.guillaumevdn.supremeshops.module.trigger.type.AdminShopTriggerNpc;
import com.guillaumevdn.supremeshops.module.trigger.type.AdminShopTriggerPreciseEntity;

public final class TriggerType implements Comparable<TriggerType> {

	// special
	public static final TriggerType BLOCK = registerType("BLOCK", AdminShopTriggerBlock.class);
	public static final TriggerType CITIZENS_NPC = registerType("CITIZENS_NPC", AdminShopTriggerCitizensNpc.class, "Citizens");
	public static final TriggerType ENTITY = registerType("ENTITY", AdminShopTriggerEntity.class);
	public static final TriggerType MYTHICMOBS_MOB = registerType("MYTHICMOBS_MOB", AdminShopTriggerMythicmobsMob.class, "MythicMobs");
	public static final TriggerType NPC = registerType("NPC", AdminShopTriggerNpc.class, "ProtocolLib");
	public static final TriggerType PRECISE_ENTITY = registerType("PRECISE_ENTITY", AdminShopTriggerPreciseEntity.class);

	// registration
	public static TriggerType registerType(String id, Class<? extends Trigger> activatorClass, String... requiredPlugins) {
		id = id.toUpperCase();
		TriggerType type = new TriggerType(id, activatorClass, requiredPlugins);
		SupremeShops.inst().getModuleManager().getTriggerTypes().put(id, type);
		return type;
	}

	public static TriggerType valueOf(String id) {
		return SupremeShops.inst().getModuleManager().getTriggerTypes().get(id.toUpperCase());
	}

	public static List<TriggerType> values() {
		return Utils.asList(SupremeShops.inst().getModuleManager().getTriggerTypes().values());
	}

	// base
	private String id;
	private Class<? extends Trigger> activatorClass;
	private List<String> requiredPlugins;

	private TriggerType(String id, Class<? extends Trigger> activatorClass, String... requiredPlugins) {
		this.id = id;
		this.activatorClass = activatorClass;
		this.requiredPlugins = requiredPlugins == null ? Utils.emptyList() : Utils.asList(requiredPlugins);
	}

	// overriden methods
	@Override
	public boolean equals(Object obj) {
		return Utils.instanceOf(obj, TriggerType.class) && getId().equalsIgnoreCase(((TriggerType) obj).getId());
	}

	@Override
	public String toString() {
		return getId();
	}

	@Override
	public int compareTo(TriggerType o) {
		return String.CASE_INSENSITIVE_ORDER.compare(getId(), o.getId());
	}

	// get
	public String getId() {
		return id;
	}

	public Class<? extends Trigger> getActivatorClass() {
		return activatorClass;
	}

	public List<String> getRequiredPlugins() {
		return requiredPlugins;
	}

	// methods
	/**
	 * Unregister the activator type
	 * @return null
	 */
	public TriggerType unregister() {
		SupremeShops.inst().getModuleManager().getTriggerTypes().remove(id.toUpperCase());
		return null;
	}

	public Trigger createNew(String id, Parseable parent, ConfigData data, boolean loadOrSave, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		// invalid plugins
		for (String requiredPlugin : this.getRequiredPlugins()) {
			if (!Utils.isPluginEnabled(requiredPlugin)) {
				data.log("activator type " + getId() + " requires plugin " + requiredPlugin + " to be enabled");
				return null;
			}
		}
		// create
		try {
			// create instance
			Constructor<? extends Trigger> constructor = getActivatorClass().getDeclaredConstructor(String.class, Parseable.class, boolean.class, int.class, Mat.class, List.class);
			Trigger component = constructor.newInstance(id, parent, mandatory, editorSlot, editorIcon, editorDescription);
			// load or save data
			if (loadOrSave) {
				component.load(data);
			} else {
				component.save(data);
			}
			// return
			return component;
		}
		// couldn't create
		catch (Throwable exception) {
			data.log("unknown error when creating activator " + id + " (" + getActivatorClass().getName() + ") with type " + getId());
			exception.printStackTrace();
			return null;
		}
	}

}
