package com.guillaumevdn.supremeshops.module.trigger.type;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPString;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;
import com.guillaumevdn.supremeshops.module.trigger.TriggerType;

import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.adapters.AbstractLocation;
import io.lumine.xikage.mythicmobs.mobs.ActiveMob;

public class AdminShopTriggerMythicmobsMob extends Trigger {

	// base
	private PPString mobId = addComponent(new PPString("mob_id", this, null, true, SETTINGS_SLOT_START, EditorGUI.ICON_STRING, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRIGGER_MYTHICSMOBIDLORE.getLines()));

	public AdminShopTriggerMythicmobsMob(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, TriggerType.MYTHICMOBS_MOB, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPString getMobId() {
		return mobId;
	}

	public String getMobId(Player parser) {
		return mobId.getParsedValue(parser);
	}

	public List<ActiveMob> getActiveMobs(Player parser) {
		List<ActiveMob> mobs = new ArrayList<ActiveMob>();
		String mobId = getMobId(parser);
		for (ActiveMob mob : MythicMobs.inst().getMobManager().getActiveMobs()) {
			if (mob.getType().getInternalName().equalsIgnoreCase(mobId)) {
				mobs.add(mob);
			}
		}
		return mobs;
	}

	// methods
	@Override
	public List<? extends BaseLocationFetcher> getBaseLocations(final Player parser) {
		List<BaseLocationFetcher> fetchers = new ArrayList<BaseLocationFetcher>();
		for (final ActiveMob mob : getActiveMobs(parser)) {
			fetchers.add(new BaseLocationFetcher() {
				@Override
				public Location getBaseLocation() {
					if (mob.isDead()) {
						return null;
					}
					AbstractLocation loc = mob.getLocation();
					World world = loc == null ? null : Bukkit.getWorld(loc.getWorld().getName());
					return world == null ? null : new Location(world, loc.getX(), loc.getY(), loc.getZ(), loc.getYaw(), loc.getPitch());
				}
			});
		}
		return fetchers;
	}

}
