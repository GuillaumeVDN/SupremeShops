package com.guillaumevdn.supremeshops.module.trigger.type;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.npc.Npc;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;
import com.guillaumevdn.supremeshops.module.trigger.TriggerType;

public class AdminShopTriggerNpc extends Trigger {

	// base
	private PPInteger id = addComponent(new PPInteger("npc_id", this, null, 0, Integer.MAX_VALUE, true, SETTINGS_SLOT_START, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRIGGER_NPCIDLORE.getLines()));

	public AdminShopTriggerNpc(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, TriggerType.NPC, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPInteger getNpcId() {
		return id;
	}

	public Integer getNpcId(Player parser) {
		return id.getParsedValue(parser);
	}

	public Npc getNpc(Player player) {
		return GCoreLegacy.inst().getNpcManager().getNpc(player, getNpcId(player));
	}

	// methods
	@Override
	public List<? extends BaseLocationFetcher> getBaseLocations(final Player player) {
		return Utils.asList(new BaseLocationFetcher() {
			@Override
			public Location getBaseLocation() {
				if (!player.isOnline()) return null;
				Npc npc = getNpc(player);
				return npc == null || !npc.isSpawned() ? null : npc.getLocation();
			}
		});
	}

}
