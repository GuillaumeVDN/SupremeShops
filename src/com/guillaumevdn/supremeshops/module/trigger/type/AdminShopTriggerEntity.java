package com.guillaumevdn.supremeshops.module.trigger.type;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPString;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;
import com.guillaumevdn.supremeshops.module.trigger.TriggerType;

public class AdminShopTriggerEntity extends Trigger {

	// base
	private PPString entityName = addComponent(new PPString("entity_name", this, null, true, SETTINGS_SLOT_START, EditorGUI.ICON_STRING, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRIGGER_ENTITYNAMELORE.getLines()));

	public AdminShopTriggerEntity(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, TriggerType.ENTITY, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPString getEntityName() {
		return entityName;
	}

	public String getEntityName(Player parser) {
		return entityName.getParsedValue(parser);
	}

	public List<Entity> getEntities(Player parser) {
		List<Entity> entities = new ArrayList<Entity>();
		String name = getEntityName(parser);
		for (World world : Bukkit.getWorlds()) {
			for (Entity entity : world.getEntities()) {
				if (entity.getCustomName() != null && entity.getCustomName().equals(name)) {
					entities.add(entity);
				}
			}
		}
		return entities;
	}

	// methods
	@Override
	public List<? extends BaseLocationFetcher> getBaseLocations(final Player player) {
		List<BaseLocationFetcher> fetchers = new ArrayList<BaseLocationFetcher>();
		for (final Entity entity : getEntities(player)) {
			fetchers.add(new BaseLocationFetcher() {
				@Override
				public Location getBaseLocation() {
					return entity.isDead() ? null : entity.getLocation();
				}
			});
		}
		return fetchers;
	}

}
