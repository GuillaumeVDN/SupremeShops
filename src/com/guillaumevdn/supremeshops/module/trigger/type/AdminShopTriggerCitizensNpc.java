package com.guillaumevdn.supremeshops.module.trigger.type;

import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;
import com.guillaumevdn.supremeshops.module.trigger.TriggerType;

import net.citizensnpcs.api.CitizensAPI;
import net.citizensnpcs.api.npc.NPC;

public class AdminShopTriggerCitizensNpc extends Trigger {

	// base
	private PPInteger id = addComponent(new PPInteger("npc_id", this, null, 0, Integer.MAX_VALUE, true, SETTINGS_SLOT_START, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRIGGER_CITIZENSNPCIDLORE.getLines()));

	public AdminShopTriggerCitizensNpc(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, TriggerType.CITIZENS_NPC, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPInteger getNpcId() {
		return id;
	}

	public Integer getNpcId(Player parser) {
		return id.getParsedValue(parser);
	}

	public NPC getNpc(Player parser) {
		return CitizensAPI.getNPCRegistry().getById(getNpcId(parser));
	}

	// methods
	@Override
	public List<? extends BaseLocationFetcher> getBaseLocations(final Player player) {
		return Utils.asList(new BaseLocationFetcher() {
			@Override
			public Location getBaseLocation() {
				NPC npc = getNpc(player);
				return npc == null || !npc.isSpawned() || npc.getEntity() == null ? null : npc.getEntity().getLocation();
			}
		});
	}

}
