package com.guillaumevdn.supremeshops.module.trigger.type;

import java.util.List;
import java.util.UUID;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPUUID;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;
import com.guillaumevdn.supremeshops.module.trigger.TriggerType;

public class AdminShopTriggerPreciseEntity extends Trigger {

	// base
	private PPUUID uuid = addComponent(new PPUUID("entity_uuid", this, null, true, SETTINGS_SLOT_START, EditorGUI.ICON_TECHNICAL, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRIGGER_ENTITYUUIDLORE.getLines()));

	public AdminShopTriggerPreciseEntity(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, TriggerType.PRECISE_ENTITY, parent, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public PPUUID getUniqueId() {
		return uuid;
	}

	public UUID getEntityUniqueId(Player parser) {
		return uuid.getParsedValue(parser);
	}

	public Entity getEntity(Player parser) {
		return Utils.getEntityByUUID(getEntityUniqueId(parser));
	}

	// methods
	@Override
	public List<? extends BaseLocationFetcher> getBaseLocations(final Player player) {
		final Entity entity = getEntity(player);
		return entity == null || entity.isDead() ? Utils.emptyList(BaseLocationFetcher.class) : Utils.asList(new BaseLocationFetcher() {
			@Override
			public Location getBaseLocation() {
				return entity == null || entity.isDead() ? null : entity.getLocation();
			}
		});
	}

}
