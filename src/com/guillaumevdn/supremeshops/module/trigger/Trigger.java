package com.guillaumevdn.supremeshops.module.trigger;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.lib.Perm;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ConfigData;
import com.guillaumevdn.gcorelegacy.lib.parseable.ContainerParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.ParseableContainment;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorItem;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.ModifCallback;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPPerm;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPString;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.module.display.particles.MultipleParticlesDisplayable;
import com.guillaumevdn.supremeshops.module.gui.SSGui;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;

public abstract class Trigger extends ContainerParseable implements MultipleParticlesDisplayable {

	// base
	private TriggerType type;
	public int SETTINGS_SLOT_START = 1;
	private PPString particlePatternId = addComponent(new PPString("particle_pattern_id", this, null, false, 9, EditorGUI.ICON_GUI, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRIGGER_PARTICLEPATTERNIDLORE.getLines()));
	private PPPerm triggerPermission = addComponent(new PPPerm("trigger_permission", this, null, false, 10, EditorGUI.ICON_TECHNICAL, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRIGGER_TRIGGERPERMISSIONLORE.getLines()));
	private CPConditions triggerConditions = addComponent(new CPConditions("trigger_conditions", this, false, 11, EditorGUI.ICON_CONDITION, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRIGGER_TRIGGERCONDITIONSLORE.getLines()));
	private PPString guiId = addComponent(new PPString("gui_id", this, null, false, 12, EditorGUI.ICON_GUI, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRIGGER_GUIIDLORE.getLines()));

	public Trigger(String id, TriggerType type, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "trigger", mandatory, editorSlot, editorIcon, editorDescription);
		this.type = type;
	}

	// get
	public TriggerType getType() {
		return type;
	}

	public PPString getParticlePatternId() {
		return particlePatternId;
	}

	public String getParticlePatternId(Player parser) {
		return particlePatternId.getParsedValue(parser);
	}

	public ParticlePattern getParticlePattern() {
		return SupremeShops.inst().getModuleManager().getParticlePattern(getParticlePatternId(null));
	}

	public PPPerm getTriggerPermission() {
		return triggerPermission;
	}

	public Perm getTriggerPermission(Player parser) {
		return triggerPermission.getParsedValue(parser);
	}

	public CPConditions getTriggerConditions() {
		return triggerConditions;
	}

	public Collection<Condition> getTriggerConditions(List<ConditionType> types) {
		List<Condition> result = new ArrayList<Condition>();
		for (Condition condition : triggerConditions.getConditions().getElements().values()) {
			if ((types == null || types.isEmpty()) || types.contains(condition.getType())) {
				result.add(condition);
			}
		}
		return result;
	}

	public PPString getGuiId() {
		return guiId;
	}

	public String getGuiId(Player parser) {
		return guiId.getParsedValue(parser);
	}

	public SSGui getGui(Player parser) {
		return SupremeShops.inst().getModuleManager().getGui(getGuiId(parser));
	}

	// particles displayables
	public boolean mustDisplayParticles() {
		return true;
	}

	public ParticlePattern getCurrentParticlePattern() {
		return getParticlePattern();
	}

	public boolean areCurrentParticlesConditionsValid(Player player) {
		Perm perm = getTriggerPermission(player);
		return (perm == null || perm.has(player)) && areTriggerConditionsValid(player, false);
	}

	// abstract methods
	public abstract List<? extends BaseLocationFetcher> getBaseLocations(Player parser);

	// methods
	public boolean areTriggerConditionsValid(Player player, boolean errorMessage) {
		return triggerConditions.isValid(player, this, errorMessage);
	}

	// enums and classes
	public interface BaseLocationFetcher {
		Location getBaseLocation();
	}

	// save
	@Override
	public void save(ConfigData data) {
		super.save(data);
		data.getConfig().set(data.getPath() + ".type", type.getId());
	}

	// editor
	@Override
	public List<String> describe(int depth) {
		List<String> desc = super.describe(depth);
		String spaces = Utils.copyString(" ", depth + 1);
		desc.add(1, spaces + " §6> type : §e" + type.getId());
		return desc;
	}

	@Override
	protected void fillEditor(final EditorGUI gui, Player player, final ModifCallback onModif) {
		// add type selector
		gui.setRegularItem(new EditorItem("admin_shop_trigger_type", 0, Mat.ENDER_CHEST, "§6type", SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_TRIGGER_TYPELORE.getLines()) {
			@Override
			protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
				// create sub GUI
				String name = Utils.getNewInventoryName(gui.getName(), "type");
				EditorGUI sub = new EditorGUI(getLastData().getPlugin(), gui, name, 9, GUI.SLOTS_0_TO_7) {
					private EditorGUI subThis = this;
					@Override
					protected void fill() {
						// current
						EditorGUI.fillItemCurrent(subThis, player, "type", Utils.asList(type.getId()), null, "admin shop trigger type", isMandatory(), getEditorIcon(), 0, onModif);
						// select
						setRegularItem(new EditorItem("control_item_select", 2, Mat.ENDER_CHEST, GLocale.GUI_GENERIC_EDITORENUMSELECT.getLine(), GLocale.GUI_GENERIC_EDITORENUMSELECTLORE.getLines()) {
							@Override
							protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
								// selection gui
								EditorGUI subSelection = new EditorGUI(getLastData().getPlugin(), gui, Utils.getNewInventoryName(gui.getName(), "Select"), 54, GUI.SLOTS_0_TO_44) {
									@Override
									protected void fill() {
										// add values
										for (final TriggerType val : TriggerType.values()) {
											final String valName = val.getId();
											setRegularItem(new EditorItem("value_" + valName, -1, Mat.ENDER_CHEST, "§6" + valName, null) {
												@Override
												protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
													Trigger curr = Trigger.this;
													final Trigger result = val.createNew(curr.getId(), curr.getParent(), curr.getLastData().clone(), false, curr.isMandatory(), curr.getEditorSlot(), curr.getEditorIcon(), curr.getEditorDescription());
													// error loading
													if (result == null) {
														player.sendMessage("§cAn unknown error occured, check the console for more details.");
														gui.open(player);
													}
													// success
													else {
														// replace result
														if (Utils.instanceOf(Trigger.this.getParent(), ParseableContainment.class)) {
															((ParseableContainment<Trigger>) Trigger.this.getParent()).replaceContaining(result);
														}
														onModif.callback(result, subThis, player);
														// create result GUI
														String name = Utils.getNewInventoryName(gui.getName(), result.getId());
														EditorGUI comp = new EditorGUI(result.getLastData().getPlugin(), gui.getParent(), name, result.getEditorSize(), result.getEditorRegularSlots()) {
															private EditorGUI compThis = this;
															@Override
															protected void fill() {
																result.fillEditor(compThis, player, onModif);
																// back item (inside fill())
																compThis.setPersistentItem(new EditorItem("control_item_back", result.getEditorBackSlot(), Mat.ARROW, GLocale.GUI_GENERIC_EDITORITEMBACK.getLine(), null) {
																	@Override
																	protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
																		gui.getParent().open(player);
																	}
																});
															}
														};
														// open it
														comp.open(player);
													}
												}
											});
										}
										// back item
										setPersistentItem(new EditorItem("control_item_back", 52, Mat.ARROW, GLocale.GUI_GENERIC_EDITORITEMBACK.getLine(), null) {
											@Override
											protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
												gui.open(player);
											}
										});
									}
								};
								// open it
								subSelection.open(player);
							}
						});
						// back item
						setPersistentItem(new EditorItem("control_item_back", 8, Mat.ARROW, GLocale.GUI_GENERIC_EDITORITEMBACK.getLine(), null) {
							@Override
							protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
								gui.open(player);
							}
						});
					}
				};
				// open it
				sub.open(player);
			}
		});
		// super
		super.fillEditor(gui, player, onModif);
	}

	// static methods
	/**
	 * Load an element from a configuration
	 * @return the loaded admin shop trigger, or null if couldn't instantiate it for some reason
	 */
	public static Trigger load(String id, Parseable parent, ConfigData data, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		// compact
		TriggerType type = null;
		// missing type setting
		data.setContains(data.getConfig().contains(data.getPath() + ".type"));
		if (!data.contains()) {
			data.log("missing primitive setting 'type' (must be an admin shop trigger type)");
			return null;
		}
		// invalid type
		type = TriggerType.valueOf(data.getConfig().getString(data.getPath() + ".type", ""));
		if (type == null) {
			data.log("invalid primitive setting 'type' (must be an admin shop trigger type)");
			return null;
		}
		// create
		return type.createNew(id, parent, data, true, mandatory, editorSlot, editorIcon, editorDescription);
	}

	// overriden
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((getId() == null) ? 0 : getId().hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Trigger other = (Trigger) obj;
		if (getId() == null) {
			return other.getId() == null;
		} else return getId().equals(other.getId());
	}

}
