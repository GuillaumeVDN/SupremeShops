package com.guillaumevdn.supremeshops.module.display.particles;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.trigger.Trigger.BaseLocationFetcher;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticleScriptExecution;

public class MultipleParticlesDisplayableTask extends BukkitRunnable {

	// base
	private Map<MultipleParticlesDisplayable, Map<UUID, List<ParticleScriptExecution>>> executions = new ConcurrentHashMap<>();

	// methods
	@Override
	public void run() {
		// get all displayables
		List<MultipleParticlesDisplayable> displayables = Utils.asListMultiple(
				SupremeShops.inst().getModuleManager().getTriggers().values()
				);
		// check block shops
		for (final MultipleParticlesDisplayable displayable : displayables) {
			// shouldn't execute
			ParticlePattern pattern = displayable.getCurrentParticlePattern();
			if (pattern == null) {
				continue;
			}
			// initialize displayable in executions
			Map<UUID, List<ParticleScriptExecution>> displayableExecutions = executions.get(displayable);
			if (displayableExecutions == null) executions.put(displayable, displayableExecutions = new ConcurrentHashMap<>());
			// proceed for players
			for (final Player player : Utils.getOnlinePlayers()) {
				// allowed
				if (displayable.areCurrentParticlesConditionsValid(player)) {
					// eventually initialize executions
					List<ParticleScriptExecution> list = displayableExecutions.get(player.getUniqueId());
					if (list == null) displayableExecutions.put(player.getUniqueId(), list = Collections.synchronizedList(new ArrayList<>()));
					if (list.isEmpty()) {
						for (final BaseLocationFetcher fetcher : displayable.getBaseLocations(player)) {
							list.add(new ParticleScriptExecution(pattern.getScript()) {
								@Override
								public Location getBaseLocation() {
									return fetcher.getBaseLocation();
								}
							});
						}
					}
					// update executions
					if (!list.isEmpty()) {
						synchronized (list) {
							List<Player> playerList = Utils.asList(player);
							Iterator<ParticleScriptExecution> iter = list.iterator();
							while (iter.hasNext()) {
								if (iter.next().update(playerList, true)) {// execution is done, remove it
									iter.remove();
								}
							}
						}
					}
				}
			}
		}
	}

	public void stop() {
		executions.clear();
		cancel();
	}

}
