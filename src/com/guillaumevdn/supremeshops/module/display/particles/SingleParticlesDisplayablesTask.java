package com.guillaumevdn.supremeshops.module.display.particles;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticleScriptExecution;

public class SingleParticlesDisplayablesTask extends BukkitRunnable {

	// base
	private Map<SingleParticlesDisplayable, ParticleScriptExecution> executions = new HashMap<SingleParticlesDisplayable, ParticleScriptExecution>();

	// methods
	@Override
	public void run() {
		try {
			// not loaded yet
			if (SupremeShops.inst().getData() == null || SupremeShops.inst().getData().getShops() == null || SupremeShops.inst().getData() == null || SupremeShops.inst().getData().getMerchants() == null) {
				return;
			}
			// get all displayables
			List<SingleParticlesDisplayable> displayables = Utils.asListMultiple(
					SupremeShops.inst().getData().getShops().getAll(Utils.asList(SingleParticlesDisplayable.class), false, com.guillaumevdn.supremeshops.data.ShopBoard.ElementRemotePolicy.MIGHT_BE),
					SupremeShops.inst().getData().getMerchants().getAll(Utils.asList(SingleParticlesDisplayable.class), false, com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy.MIGHT_BE)
					);
			// check block shops
			for (final SingleParticlesDisplayable displayable : displayables) {
				// shouldn't execute
				if (!displayable.mustDisplayParticles()) {
					continue;
				}
				// get pattern
				ParticlePattern pattern = displayable.getCurrentParticlePattern();
				if (pattern == null) {
					continue;
				}
				// get allowed players
				List<Player> players = new ArrayList<Player>();
				for (Player player : Utils.getOnlinePlayers()) {
					if (displayable.areCurrentParticlesConditionsValid(player)) {
						players.add(player);
					}
				}
				// update execution if at least one player is allowed
				if (!players.isEmpty()) {
					// eventually initialize execution
					ParticleScriptExecution execution = executions.get(displayable);
					if (execution == null) {
						executions.put(displayable, execution = new ParticleScriptExecution(pattern.getScript()) {
							@Override
							public Location getBaseLocation() {
								return displayable.getParticlesBase();
							}
						});
					}
					// update execution
					if (execution.update(players, true)) {// execution is done, remove it
						executions.remove(displayable);
					}
				}
			}
		} catch (ConcurrentModificationException ignored) {}
	}

	public void stop() {
		executions.clear();
		cancel();
	}

}
