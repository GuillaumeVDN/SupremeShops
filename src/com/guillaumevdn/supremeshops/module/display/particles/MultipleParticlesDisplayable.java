package com.guillaumevdn.supremeshops.module.display.particles;

import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.supremeshops.module.trigger.Trigger.BaseLocationFetcher;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;

public interface MultipleParticlesDisplayable {

	boolean mustDisplayParticles();
	ParticlePattern getCurrentParticlePattern();
	boolean areCurrentParticlesConditionsValid(Player player);
	List<? extends BaseLocationFetcher> getBaseLocations(Player parser);

}
