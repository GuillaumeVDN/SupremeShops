package com.guillaumevdn.supremeshops.module.display.particles;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;

public interface SingleParticlesDisplayable {

	boolean isOpen();
	boolean mustDisplayParticles();
	ParticlePattern getCurrentParticlePattern();
	boolean areCurrentParticlesConditionsValid(Player player);
	Location getParticlesBase();

}
