package com.guillaumevdn.supremeshops.module.display.items;

import org.bukkit.block.BlockFace;

import com.guillaumevdn.supremeshops.SupremeShops;

public enum FacingModifier {

	// values
	FACING_SOUTH(BlockFace.SOUTH, 0.05d, 0.75d, 0d, -0.5d, 0.95d, 0.75d, 0d, -0.5d),
	FACING_NORTH(BlockFace.NORTH, 0.95d, 0.055d, 0d, 0.5d, 0.05d, 0.055d, 0d, 0.5d),
	FACING_WEST(BlockFace.WEST, 0.05d, 0.05d, 0.5d, 0d, 0d, 0.95d, 0.5d, 0d),
	FACING_EAST(BlockFace.EAST, 0.95d, 0.95d, -0.5d, 0d, 0.95d, 0.05d, -0.5d, 0d);

	// fields
	private BlockFace face;
	private double takeBaseOffX, takeBaseOffZ, takeDeltaX, takeDeltaZ, giveBaseOffX, giveBaseOffZ, giveDeltaX, giveDeltaZ;

	private FacingModifier(BlockFace face, double takeBaseOffX, double takeBaseOffZ, double takeDeltaX, double takeDeltaZ, double giveBaseOffX, double giveBaseOffZ, double giveDeltaX, double giveDeltaZ) {
		this.face = face;
		String path = "shop_display_items_offsets." + name();
		this.takeBaseOffX = SupremeShops.inst().getConfiguration().getDouble(path + ".take_base_offx", takeBaseOffX);
		this.takeBaseOffZ = SupremeShops.inst().getConfiguration().getDouble(path + ".take_base_offz", takeBaseOffZ);
		this.takeDeltaX = SupremeShops.inst().getConfiguration().getDouble(path + ".take_delta_offx", takeDeltaX);
		this.takeDeltaZ = SupremeShops.inst().getConfiguration().getDouble(path + ".take_delta_offz", takeDeltaZ);
		this.giveBaseOffX = SupremeShops.inst().getConfiguration().getDouble(path + ".give_base_offx", giveBaseOffX);
		this.giveBaseOffZ = SupremeShops.inst().getConfiguration().getDouble(path + ".give_base_offz", giveBaseOffZ);
		this.giveDeltaX = SupremeShops.inst().getConfiguration().getDouble(path + ".give_delta_offx", giveDeltaX);
		this.giveDeltaZ = SupremeShops.inst().getConfiguration().getDouble(path + ".give_delta_offz", giveDeltaZ);
	}

	// get
	public BlockFace getFace() {
		return face;
	}

	public double getTakeBaseOffX() {
		return takeBaseOffX;
	}

	public double getTakeBaseOffZ() {
		return takeBaseOffZ;
	}

	public double getTakeDeltaX() {
		return takeDeltaX;
	}

	public double getTakeDeltaZ() {
		return takeDeltaZ;
	}

	public double getGiveBaseOffX() {
		return giveBaseOffX;
	}

	public double getGiveBaseOffZ() {
		return giveBaseOffZ;
	}

	public double getGiveDeltaX() {
		return giveDeltaX;
	}

	public double getGiveDeltaZ() {
		return giveDeltaZ;
	}

	// static methods
	public static FacingModifier from(BlockFace face) {
		for (FacingModifier modifier : values()) {
			if (modifier.getFace().equals(face)) {
				return modifier;
			}
		}
		return null;
	}

}
