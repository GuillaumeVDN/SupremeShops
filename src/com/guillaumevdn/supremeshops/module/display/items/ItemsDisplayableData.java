package com.guillaumevdn.supremeshops.module.display.items;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.bukkit.entity.Item;
import org.bukkit.util.Vector;

import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.util.GravityUtils;

public class ItemsDisplayableData {

	// base
	private ItemsDisplayable displayable;
	private List<Item> items = new ArrayList<Item>();
	private List<AwaitingItem> awaitingItems = new ArrayList<AwaitingItem>();
	private boolean awaitingOperation = false;

	public ItemsDisplayableData(ItemsDisplayable displayable)  {
		this.displayable = displayable;
	}

	// get
	public ItemsDisplayable getShop() {
		return displayable;
	}

	public List<Item> getItems() {
		return items;
	}

	public boolean isAwaitingOperation() {
		return awaitingOperation;
	}

	public List<AwaitingItem> getAwaitingItems() {
		return awaitingItems;
	}

	// set
	public void setAwaitingOperation(boolean awaitingOperation) {
		this.awaitingOperation = awaitingOperation;
	}

	// methods
	public int countValidItems() {
		int valid = 0;
		try {
			Collection<Item> worldEntities = displayable.getItemsDisplayBase().getWorld().getEntitiesByClass(Item.class);
			for (Item item : items) {
				if (!item.isDead() && worldEntities.contains(item)) {
					++valid;
				}
			}
		} catch (IndexOutOfBoundsException ignored) {} // see #7
		return valid;
	}

	public void removeItems() {
		// clean all items
		SupremeShops.inst().getGeneralManager().cleanDisplayItems(displayable);
	}

	public void spawnItems() {
		// spawn awaiting items
		for (AwaitingItem awaitingItem : awaitingItems) {
			// spawn item
			Item item = awaitingItem.getLocation().getWorld().dropItem(awaitingItem.getLocation(), awaitingItem.getStack());
			if (awaitingItem.getCustomName() != null) {
				item.setCustomNameVisible(true);
				item.setCustomName(awaitingItem.getCustomName());
			}
			try {
				GravityUtils.setGravity(item, false);
			} catch (Throwable ignored) {}
			item.setPickupDelay(Integer.MAX_VALUE);
			item.setVelocity(new Vector(0d, 0d, 0d));
			// add to displayable list
			items.add(item);
		}
		awaitingItems.clear();
	}

}
