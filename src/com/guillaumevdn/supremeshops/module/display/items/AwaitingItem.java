package com.guillaumevdn.supremeshops.module.display.items;

import org.bukkit.Location;
import org.bukkit.inventory.ItemStack;

public class AwaitingItem {

	// base
	private ItemsDisplayable displayable;
	private Location location;
	private ItemStack stack;
	private String customName;

	public AwaitingItem(ItemsDisplayable displayable, Location location, ItemStack stack, String customName) {
		this.displayable = displayable;
		this.location = location;
		this.stack = stack;
		this.customName = customName;
	}

	// get
	public ItemsDisplayable getDisplayable() {
		return displayable;
	}

	public Location getLocation() {
		return location;
	}

	public ItemStack getStack() {
		return stack;
	}

	public String getCustomName() {
		return customName;
	}

}
