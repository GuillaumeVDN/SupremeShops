package com.guillaumevdn.supremeshops.module.display.items;

import java.util.ConcurrentModificationException;
import java.util.Iterator;

import org.bukkit.scheduler.BukkitRunnable;

public class ProcessTask extends BukkitRunnable {

	// base
	private ItemsDisplayableManager manager;
	private int limit;

	public ProcessTask(ItemsDisplayableManager manager, int limit) {
		this.manager = manager;
		this.limit = limit;
	}

	// get
	public ItemsDisplayableManager getManager() {
		return manager;
	}

	// methods
	@Override
	public void run() {
		try {
			int operations = 0;
			// process shops
			Iterator<ItemsDisplayable> iterator = manager.getData().keySet().iterator();
			while (iterator.hasNext() && operations < limit) {
				// awaiting operation
				ItemsDisplayableData data = manager.getData().get(iterator.next());
				if (data.isAwaitingOperation()) {
					data.setAwaitingOperation(false);
					++operations;
					data.removeItems();
					data.spawnItems();
				}
			}
		} catch (ConcurrentModificationException | NullPointerException ignored) {}// when reloading
	}

}
