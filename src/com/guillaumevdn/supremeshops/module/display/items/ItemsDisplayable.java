package com.guillaumevdn.supremeshops.module.display.items;

import java.util.Collection;
import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;

public interface ItemsDisplayable {

	String getId();
	BlockCoords getItemsDisplayBase();
	BlockCoords getItemsDisplayFacingBase();
	boolean isOpen();
	boolean getDisplayItems();
	Collection<TradeObject> getObjects();
	Collection<TradeObject> getObjects(List<ObjectSide> sides, List<ObjectType> types);

}
