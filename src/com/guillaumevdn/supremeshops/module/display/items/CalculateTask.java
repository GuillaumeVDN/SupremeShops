package com.guillaumevdn.supremeshops.module.display.items;

import java.util.ConcurrentModificationException;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;
import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.modifier.Modifier;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.object.type.ObjectItem;
import com.guillaumevdn.supremeshops.module.object.type.ObjectVaultMoney;
import com.guillaumevdn.supremeshops.module.object.type.ObjectXpLevel;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class CalculateTask extends BukkitRunnable {

	// base
	private ItemsDisplayableManager manager;
	private double distanceTolerance = 0d;

	public CalculateTask(ItemsDisplayableManager manager, double distanceTolerance) {
		this.manager = manager;
		this.distanceTolerance = distanceTolerance;
	}

	// get
	public ItemsDisplayableManager getManager() {
		return manager;
	}

	public double getDistanceTolerance() {
		return distanceTolerance;
	}

	// methods
	@Override
	public void run() {
		try {
			// load settings
			double baseOffy = SupremeShops.inst().getConfiguration().getDouble("shop_display_items_base_offy", 1d);
			double resetOffy = SupremeShops.inst().getConfiguration().getDouble("shop_display_items_reset_offy", 0.55d);
			double otherOffy = SupremeShops.inst().getConfiguration().getDouble("shop_display_items_other_offy", 0.4d);
			int resetEveryItem = SupremeShops.inst().getConfiguration().getInt("shop_display_items_reset_every_item", 2);
			if (resetEveryItem < 1) resetEveryItem = 1;
			boolean showDiff1 = SupremeShops.inst().getConfiguration().getBoolean("shop_display_items_show_diff1", true);
			// not loaded yet
			if (SupremeShops.inst().getData() == null || SupremeShops.inst().getData().getShops() == null) {
				return;
			}
			// get all displayables
			List<ItemsDisplayable> displayables = Utils.asListMultiple(SupremeShops.inst().getData().getShops().getAll(Utils.asList(ItemsDisplayable.class), true, com.guillaumevdn.supremeshops.data.ShopBoard.ElementRemotePolicy.MIGHT_BE));
			// check displayables
			for (ItemsDisplayable displayable : displayables) {
				// shouldn't execute
				if (!displayable.isOpen() || !displayable.getDisplayItems()) {
					continue;
				}
				// there's no player around
				final BlockCoords shopBlock = displayable.getItemsDisplayBase();
				ItemsDisplayableData data = manager.getData(displayable);
				if (!hasPlayersAround(shopBlock)) {
					if (!data.isAwaitingOperation()) {
						data.getAwaitingItems().clear();
						if (!data.getItems().isEmpty()) data.setAwaitingOperation(true);
					}
					continue;
				}
				// an operation is awaiting for this shop
				if (data.isAwaitingOperation()) {
					continue;
				}
				// not enough valid items
				if (data.countValidItems() != displayable.getObjects().size()) {
					data.setAwaitingOperation(true);
				}
				// one of the entities is too far away from the shop for some reason
				else {
					for (Item item : data.getItems()) {
						if (item.getLocation().getY() < shopBlock.getY() || Math.abs(item.getLocation().getX() - (shopBlock.getX() + 0.5d)) >= 0.75d || Math.abs(item.getLocation().getZ() - (shopBlock.getZ() + 0.5d)) >= 0.75d) {
							data.setAwaitingOperation(true);
							break;
						}
					}
				}
				// reset items
				if (data.isAwaitingOperation()) {
					data.getAwaitingItems().clear();
					// get modifier
					FacingModifier modifier = FacingModifier.from(shopBlock.toBlock().getFace(displayable.getItemsDisplayFacingBase().toBlock()));
					if (modifier == null) modifier = FacingModifier.FACING_SOUTH;
					// add giving display items
					double x = shopBlock.getX(), y = shopBlock.getY(), z = shopBlock.getZ();
					double offx = modifier.getGiveBaseOffX();
					double offz = modifier.getGiveBaseOffZ();
					double offy = baseOffy;
					int i = 0;
					for (TradeObject give : Utils.asList(displayable.getObjects(Utils.asList(ObjectSide.GIVING), null))) {
						// build item
						ItemStack stack = give.getPreviewItem(1d, null).getItemStack().clone();
						ItemMeta meta = stack.getItemMeta();
						meta.setDisplayName(ItemsDisplayableManager.ITEM_MARK_BEGIN + displayable.getId());
						stack.setItemMeta(meta);
						stack.setAmount(1);
						// build custom name
						String customName = null;
						if (give instanceof ObjectVaultMoney) {
							ObjectVaultMoney giveMoney = (ObjectVaultMoney) give;
							customName = SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTVAULTMONEYPREVIEWITEMGIVENAME.getLine("{amount}", Utils.round5(giveMoney.calculateAmountForTrades(displayable instanceof Shop ? ((Shop) displayable) : null, 1, Utils.emptyList(Modifier.class), null)));
						} else if (give instanceof ObjectXpLevel) {
							ObjectXpLevel giveXpLevel = (ObjectXpLevel) give;
							customName = SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTXPLEVELPREVIEWITEMGIVENAME.getLine("{amount}", Utils.round5(giveXpLevel.calculateAmountForTrades(displayable instanceof Shop ? ((Shop) displayable) : null, 1, Utils.emptyList(Modifier.class), null)));
						} else if (give instanceof ObjectItem) {
							ObjectItem giveItem = (ObjectItem) give;
							double amount = giveItem.calculateAmountForTrades(displayable instanceof Shop ? ((Shop) displayable) : null, 1, Utils.emptyList(Modifier.class), null);
							if (amount != 1d || showDiff1) {
								customName = SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTITEMPREVIEWITEMGIVENAME.getLine("{amount}", Utils.round5(amount));
							}
						} else {
							customName = give.describeForAmount(give.calculateAmountForTrades(displayable instanceof Shop ? ((Shop) displayable) : null, 1, Utils.emptyList(Modifier.class), null), null);
						}
						// add item to spawn
						data.getAwaitingItems().add(new AwaitingItem(displayable, new Location(shopBlock.getWorld(), x + offx, y + offy, z + offz), stack, customName));
						// update offset
						offx += modifier.getGiveDeltaX();
						offz += modifier.getGiveDeltaZ();
						if (++i % resetEveryItem == 0) {
							offx = modifier.getGiveBaseOffX();
							offz = modifier.getGiveBaseOffZ();
							offy += resetOffy;
						} else {
							offy += otherOffy;
						}
					}
					// add taking display items
					offx = modifier.getTakeBaseOffX();
					offz = modifier.getTakeBaseOffZ();
					offy = baseOffy;
					i = 0;
					for (TradeObject take : displayable.getObjects(Utils.asList(ObjectSide.TAKING), null)) {
						// build item
						ItemStack stack = take.getPreviewItem(take.calculateAmountForTrades(displayable instanceof Shop ? ((Shop) displayable) : null, 1, Utils.emptyList(Modifier.class), null), null).getItemStack();
						ItemMeta meta = stack.getItemMeta();
						meta.setDisplayName(ItemsDisplayableManager.ITEM_MARK_BEGIN + displayable.getId());
						stack.setItemMeta(meta);
						stack.setAmount(1);
						// build custom name
						String customName = null;
						if (take instanceof ObjectVaultMoney) {
							ObjectVaultMoney takeMoney = (ObjectVaultMoney) take;
							customName = SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTVAULTMONEYPREVIEWITEMTAKENAME.getLine("{amount}", Utils.round5(takeMoney.calculateAmountForTrades(displayable instanceof Shop ? ((Shop) displayable) : null, 1, Utils.emptyList(Modifier.class), null)));
						} else if (take instanceof ObjectXpLevel) {
							ObjectXpLevel takeXpLevel = (ObjectXpLevel) take;
							customName = SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTXPLEVELPREVIEWITEMTAKENAME.getLine("{amount}", Utils.round5(takeXpLevel.calculateAmountForTrades(displayable instanceof Shop ? ((Shop) displayable) : null, 1, Utils.emptyList(Modifier.class), null)));
						} else if (take instanceof ObjectItem) {
							ObjectItem takeItem = (ObjectItem) take;
							double amount = takeItem.calculateAmountForTrades(displayable instanceof Shop ? ((Shop) displayable) : null, 1, Utils.emptyList(Modifier.class), null);
							if (amount != 1d || showDiff1) {
								customName = SSLocaleMisc.MISC_SUPREMESHOPS_OBJECTITEMPREVIEWITEMTAKENAME.getLine("{amount}", Utils.round5(amount));
							}
						} else {
							customName = take.describeForAmount(take.calculateAmountForTrades(displayable instanceof Shop ? ((Shop) displayable) : null, 1, Utils.emptyList(Modifier.class), null), null);
						}
						// add item to spawn
						data.getAwaitingItems().add(new AwaitingItem(displayable, new Location(shopBlock.getWorld(), x + offx, y + offy, z + offz), stack, customName));
						// update offset
						offx += modifier.getTakeDeltaX();
						offz += modifier.getTakeDeltaZ();
						if (++i % resetEveryItem == 0) {
							offx = modifier.getTakeBaseOffX();
							offz = modifier.getTakeBaseOffZ();
							offy += resetOffy;
						} else {
							offy += otherOffy;
						}
					}
				}
			}	
		} catch (ConcurrentModificationException | NullPointerException ignored) {}// when reloading
	}

	private boolean hasPlayersAround(BlockCoords coords) {
		for (Player player : coords.getWorld().getPlayers()) {
			Location loc = player.getLocation();
			if (coords.distance(loc.getBlockX(), loc.getBlockY(), loc.getBlockZ()) <= distanceTolerance) {
				return true;
			}
		}
		return false;
	}

}
