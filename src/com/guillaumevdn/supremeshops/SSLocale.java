package com.guillaumevdn.supremeshops;

import java.io.File;
import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.messenger.Text;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;

public class SSLocale {

	public static final File file = new File(SupremeShops.inst().getDataFolder() + "/texts.yml");

	private static Text n(String id, Object... content) {
		return new Text(id, file, content);
	}

	private static List<String> l(String... content) {
		return Utils.asList(content);
	}

	public static final Text MSG_SUPREMESHOPS_HASNTMESSAGE_ITEM = n("MSG_SUPREMESHOPS_HASNTMESSAGE_ITEM",
			"en_US", l("&6SupremeShops >> &7You need this item to trade : &c{item}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez besoin de cet item pour procéder à l'échange : &c{item}&7."),
			"zh_CN", l("&6SupremeShops >> &7Y你需要这件物品才能进行交易 : &c{item}&7."),
			"zh_TW", l("&6SupremeShops >> &7你需要這件物品才能進行交易: &c{item}&7。"),
			"ru_RU", l("&6SupremeShops >> &7Для обмена необходим следующий предмет : &c{item}&7."),
			"hu_HU", l("&6SupremeShops >> &7Szükségd van a &c{item}&7 tételre a kereskedéshez."),
			"es_ES", l("&6SupremeShops >> &7Necesitas este objeto para comerciar : &c{item}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_HASNTMESSAGE_VAULTMONEY = n("MSG_SUPREMESHOPS_HASNTMESSAGE_VAULTMONEY",
			"en_US", l("&6SupremeShops >> &7You need &c{money}$ &7to trade."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez besoin de &c{money}$ &7pour procéder à l'échange."),
			"zh_CN", l("&6SupremeShops >> &7你需要&c{money}$&7进行交易."),
			"zh_TW", l("&6SupremeShops >> &7你需要 &c{money} &7才能進行交易。"),
			"ru_RU", l("&6SupremeShops >> &7Для обмена необходимо &c{money}$ &7монет для совершения обмена."),
			"hu_HU", l("&6SupremeShops >> &7Szükséged van &c{money}$ &7költőpénzre a kereskedéshez."),
			"es_ES", l("&6SupremeShops >> &7Necesitas &c{money}$ &7para comerciar.")
			);

	public static final Text MSG_SUPREMESHOPS_HASNTMESSAGE_XPLEVEL = n("MSG_SUPREMESHOPS_HASNTMESSAGE_XPLEVEL",
			"en_US", l("&6SupremeShops >> &7You need &c{amount} LVL &7to trade."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez besoin de &c{amount} LVL &7pour procéder à l'échange."),
			"zh_CN", l("&6SupremeShops >> &7你需要&c{amount}级&7才能进行交易."),
			"zh_TW", l("&6SupremeShops >> &7你需要經驗 &c{amount}級 &7才能進行交易。"),
			"ru_RU", l("&6SupremeShops >> &7Вам необходимо &c{amount} LVL &7для совершения обмена."),
			"hu_HU", l("&6SupremeShops >> &7Szükséged van &c{amount} szintre &7a kereskedéshez."),
			"es_ES", l("&6SupremeShops >> &7Necesitas &c{amount} nivel(es) &7para comerciar.")
			);

	public static final Text MSG_SUPREMESHOPS_HASNTMESSAGE_PLAYERPOINTSPOINTS = n("MSG_SUPREMESHOPS_HASNTMESSAGE_PLAYERPOINTSPOINTS",
			"en_US", l("&6SupremeShops >> &7You need &c{amount} PlayerPoints points &7to trade."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez besoin de &c{amount} points PlayerPoints &7pour procéder à l'échange."),
			"zh_CN", l("&6SupremeShops >> &7你需要&c{amount}点PlayerPoints点券&7才能进行交易."),
			"zh_TW", l("&6SupremeShops >> &7你需要PlayerPoints &c{amount}點 &7才能進行交易。"),
			"ru_RU", l("&6SupremeShops >> &7Вам необходимо &c{amount} очков PlayerPoints &7для совершения обмена."),
			"hu_HU", l("&6SupremeShops >> &7Szükséged van &c{amount} PlayerPoints pontra &7a kereskedéshez."),
			"es_ES", l("&6SupremeShops >> &7Necesitas &c{amount} PlayerPoints &7puntos para comerciar.")
			);

	public static final Text MSG_SUPREMESHOPS_HASNTMESSAGE_TOKENENCHANTTOKENS = n("MSG_SUPREMESHOPS_HASNTMESSAGE_TOKENENCHANTTOKENS",
			"en_US", l("&6SupremeShops >> &7You need &c{amount} TokenEnchant tokens &7to trade."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez besoin de &c{amount} tokens TokenEnchant &7pour procéder à l'échange."),
			"zh_TW", l("&6SupremeShops >> &7你需要TokenEnchant &c{amount}代幣 &7才能进行交易。"),
			"ru_RU", l("&6SupremeShops >> &7Вам необходимо &c{amount} TokenEnchant-токенов &7для совершения обмена."),
			"hu_HU", l("&6SupremeShops >> &7Szükséged van &c{amount} TokenEnchant tokenre &7a kereskedéshez."),
			"es_ES", l("&6SupremeShops >> &7Necesitas &c{amount} TokenEnchant tokens &7para comerciar.")
			);

	public static final Text MSG_SUPREMESHOPS_HASNTMESSAGE_SHOP = n("MSG_SUPREMESHOPS_HASNTMESSAGE_SHOP",
			"en_US", l("&6SupremeShops >> &7You need this shop to trade : &c{coords}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez besoin de ce shop pour procéder à l'échange : &c{coords}&7."),
			"zh_CN", l("&6SupremeShops >> &7你需要使用这个商店才能进行交易: &c{coords}&7."),
			"zh_TW", l("&6SupremeShops >> &7你需要這個商店才能進行交易: &c{coords}。"),
			"ru_RU", l("&6SupremeShops >> &7Вам нужен этот магазин : &c{coords}&7."),
			"hu_HU", l("&6SupremeShops >> &7Szükséged van a &c{coords}&7 helyszínen található boltra  akereskedéshez."),
			"es_ES", l("&6SupremeShops >> &7Necesitas esta tienda para poder comerciar : &c{coords}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_NOSTOCK = n("MSG_SUPREMESHOPS_NOSTOCK",
			"en_US", l("&6SupremeShops >> &7There's no remaining stock in this shop."),
			"fr_FR", l("&6SupremeShops >> &7Il n'y a aucun stock restant dans ce shop."),
			"zh_CN", l("&6SupremeShops >> &7商店库存不足."),
			"zh_TW", l("&6SupremeShops >> &7商店庫存不足。"),
			"ru_RU", l("&6SupremeShops >> &7В этом магазине не осталось складских запасов."),
			"hu_HU", l("&6SupremeShops >> &7Ebben az üzletben már nincs raktárkészlet."),
			"es_ES", l("&6SupremeShops >> &7No quedan existencias en esta tienda.")
			);

	public static final Text MSG_SUPREMESHOPS_NOPOSSIBLETRADE = n("MSG_SUPREMESHOPS_NOPOSSIBLETRADE",
			"en_US", l("&6SupremeShops >> &7No trade is currently possible with the current stock (your stock or the shop's stock)."),
			"fr_FR", l("&6SupremeShops >> &7Aucun échange n'est actuellement possible avec les stocks actuels (votre stock ou celui du shop)."),
			"zh_CN", l("&6SupremeShops >> &7当前库存不足以进行交易 (你的库存或商店的库存)."),
			"zh_TW", l("&6SupremeShops >> &7當前的庫存無法進行交易 (你的庫存或商店的庫存)。"),
			"ru_RU", l("&6SupremeShops >> &7В настоящее время обмен не возможен (нет нужных предметов в магазине или у Вас)."),
			"hu_HU", l("&6SupremeShops >> &7Kereskedelem nem jöhet létre a jelenlegi raktárkészlet miatt (a tied, vagy a bolté.)."),
			"es_ES", l("&6SupremeShops >> &7No es posible comerciar actualmente con las existencias actuales (tus existencias o las existencias de la tienda)")
			);

	public static final Text MSG_SUPREMESHOPS_SHOPNOTYOURS = n("MSG_SUPREMESHOPS_SHOPNOTYOURS",
			"en_US", l("&6SupremeShops >> &7This shop isn't yours or you can't manage it."),
			"fr_FR", l("&6SupremeShops >> &7Ce shop n'est pas le vôtre ou vous ne pouvez pas le gérer."),
			"zh_CN", l("&6SupremeShops >> &7这不是你的商店."),
			"zh_TW", l("&6SupremeShops >> &7這不是你的商店，你不能編輯它。"),
			"ru_RU", l("&6SupremeShops >> &7Этот магазин не принадлежит вам, либо у вас нет прав на управление им."),
			"hu_HU", l("&6SupremeShops >> &7Ez az bolt nem a tied, vagy nincs engedélyed kezelni.")
			);

	public static final Text MSG_SUPREMESHOPS_SHOPCANTTRANSFERRENTED = n("MSG_SUPREMESHOPS_SHOPCANTTRANSFERRENTED",
			"en_US", l("&6SupremeShops >> &7Rented shops can't be traded."),
			"fr_FR", l("&6SupremeShops >> &7Les shops loués ne peuvent pas être échangés."),
			"zh_TW", l("&6SupremeShops >> &7已出租的商店不可被交易。"),
			"ru_RU", l("&6SupremeShops >> &7Арендованные магазины не подлежат продаже/обмену."),
			"hu_HU", l("&6SupremeShops >> &7Bérelt boltok nem kereskedhetnek."),
			"es_ES", l("&6SupremeShops >> &7No se puede comerciar en tiendas alquiladas.")
			);

	public static final Text MSG_SUPREMESHOPS_MERCHANTNOTYOURS = n("MSG_SUPREMESHOPS_MERCHANTNOTYOURS",
			"en_US", l("&6SupremeShops >> &7This merchant isn't yours or you can't manage it."),
			"fr_FR", l("&6SupremeShops >> &7Ce marchand n'est pas le vôtre ou vous ne pouvez pas le gérer."),
			"zh_CN", l("&6SupremeShops >> &7这不是你的商人."),
			"zh_TW", l("&6SupremeShops >> &7這不是你的商人，你不能編輯它。"),
			"ru_RU", l("&6SupremeShops >> &7Этот торговец не принадлежит вам, либо у вас нет прав на управление им."),
			"hu_HU", l("&6SupremeShops >> &7Ez a kereskedő nem a tied, vagy nincs engedélyed kezelni."),
			"es_ES", l("&6SupremeShops >> &7Este mercado no es tuyo o no puedes editarlo.")
			);

	public static final Text MSG_SUPREMESHOPS_MERCHANTCANTTRANSFERRENTED = n("MSG_SUPREMESHOPS_MERCHANTCANTTRANSFERRENTED",
			"en_US", l("&6SupremeShops >> &7Rented merchants can't be traded."),
			"fr_FR", l("&6SupremeShops >> &7Les marchands loués ne peuvent pas être échangés."),
			"zh_TW", l("&6SupremeShops >> &7已出租的商人不可被交易。"),
			"ru_RU", l("&6SupremeShops >> &7Арендованные торговцы не подлежат продаже\\обмену."),
			"hu_HU", l("&6SupremeShops >> &7A bérelt kereskedők nem kereskedhetnek."),
			"es_ES", l("&6SupremeShops >> &7No se puede comerciar con los mercaderes alquilados.")
			);

	public static final Text MSG_SUPREMESHOPS_SHOPPERMISSIONSCOPYSTART = n("MSG_SUPREMESHOPS_SHOPPERMISSIONSCOPYSTART",
			"en_US", l("&6SupremeShops >> &7Right-click on the shops to which the management permissions for &a{player} &7should be copied. Sneak to stop."),
			"fr_FR", l("&6SupremeShops >> &7Cliquez-droit sur les shops auquels les permissions de management de &a{player} &7doivent être copiées. Accroupissez-vous pour arrêter."),
			"zh_CN", l("&6SupremeShops >> &7右击&a{player}的商店以复制管理权限to which the management permissions"),
			"zh_TW", l("&6SupremeShops >> &f右鍵點擊&7商店，複製 &a{player} &7的管理權限。(SHIFT 取消)"),
			"ru_RU", l("&6SupremeShops >> &7Нажми ПМК по магазину, права управления которым должны быть скопированы для игрока &a{player} &7. SHIFT, чтобы прекратить."),
			"hu_HU", l("&6SupremeShops >> &7Jobb klikk azokra a boltokra, ahova &a{player} &7játékos engedélyét másolni kell. Lopakodó gomb a leállításhoz."),
			"es_ES", l("&6SupremeShops >> &7Haz clic derecho en las tiendas para seleccionar de qué permiso de gestión para &a{player} &7deberían copiarse. Agáchate para parar.")
			);

	public static final Text MSG_SUPREMESHOPS_MERCHANTPERMISSIONSCOPYSTART = n("MSG_SUPREMESHOPS_MERCHANTPERMISSIONSCOPYSTART",
			"en_US", l("&6SupremeShops >> &7Right-click on the merchants to which the management permissions for &a{player} &7should be copied. Sneak to stop."),
			"fr_FR", l("&6SupremeShops >> &7Cliquez-droit sur les marchands auquels les permissions de management de &a{player} &7doivent être copiées. Accroupissez-vous pour arrêter."),
			"zh_CN", l("&6SupremeShops >> &7右击商人merchants to which the management permissions for"),
			"zh_TW", l("&6SupremeShops >> &f右鍵點擊&7商人，複製 &a{player} &7的管理權限。(SHIFT 取消)"),
			"ru_RU", l("&6SupremeShops >> &7Нажми ПМК по торговцу, права управления которым должны быть скопированы для игрока &a{player} &7SHIFT, чтобы прекратить."),
			"hu_HU", l("&6SupremeShops >> &7Jobb klikk azokra a kereskedőkre, ahova &a{player} &7játékos engedélyét másolni kell. Lopakodó gomb a leállításhoz."),
			"es_ES", l("&6SupremeShops >> &7Haz clic derecho en los mercaderes para seleccionar de qué permiso de gestión para &a{player} &7deberían copiarse. Agáchate para parar.")
			);

	public static final Text MSG_SUPREMESHOPS_COPIEDPERMISSIONS = n("MSG_SUPREMESHOPS_COPIEDPERMISSIONS",
			"en_US", l("&6SupremeShops >> &7Permissions copied for manager &a{player}&7."),
			"fr_FR", l("&6SupremeShops >> &7Permissions copiées pour le manager &a{player}&7."),
			"zh_CN", l("&6SupremeShops >> &7已复制&a{player}&7的权限."),
			"zh_TW", l("&6SupremeShops >> &7已複製 &a{player} &7的商店管理權限。"),
			"ru_RU", l("&6SupremeShops >> &7Права управления скопированы для &a{player}&7."),
			"hu_HU", l("&6SupremeShops >> &7Jogok átmásolva &a{player}&7 játékos számára."),
			"es_ES", l("&6SupremeShops >> &7Permiso copiado para el gestor &a{player}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_PERMISSIONSCOPYEND = n("MSG_SUPREMESHOPS_PERMISSIONSCOPYEND",
			"en_US", l("&6SupremeShops >> &7Permissions copy ended."),
			"fr_FR", l("&6SupremeShops >> &7Copie des permissions terminée."),
			"zh_CN", l("&6SupremeShops >> &7已结束复制权限."),
			"zh_TW", l("&6SupremeShops >> &7已取消權限複製。"),
			"ru_RU", l("&6SupremeShops >> &7Копирования прав завершено."),
			"hu_HU", l("&6SupremeShops >> &7Jogok átmásolása végetért."),
			"es_ES", l("&6SupremeShops >> &7Copia de permisos terminado.")
			);

	public static final Text MSG_SUPREMESHOPS_MULTIRESTOCKALLSTART = n("MSG_SUPREMESHOPS_MULTIRESTOCKALLSTART",
			"en_US", l("&6SupremeShops >> &7Right-click on the shops to restock all. Sneak to stop."),
			"fr_FR", l("&6SupremeShops >> &7Cliquez-droit sur les shops à restocker complètement. Accroupissez-vous pour arrêter."),
			"zh_CN", l("&6SupremeShops >> &7Right-click on the shops to restock all. 潜行以停止."),
			"zh_TW", l("&6SupremeShops >> &f右鍵點擊&7商店進行補貨。(SHIFT 取消)"),
			"ru_RU", l("&6SupremeShops >> &7ПМК по магазинам, чтобы пополнить запасы всех. SHIFT, дабы прекратить."),
			"hu_HU", l("&6SupremeShops >> &7Jobb-klikk a boltokra az árufeltöltéshez. Lopakodó gomb a leállításhoz."),
			"es_ES", l("&6SupremeShops >> &7Haz clic derecho en las tiendas para realizar un restock completo. Agáchate para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_MULTIWITHDRAWALLBENEFITSSTART = n("MSG_SUPREMESHOPS_MULTIWITHDRAWALLBENEFITSSTART",
			"en_US", l("&6SupremeShops >> &7Right-click on the shops to withdraw all (benefits). Sneak to stop."),
			"fr_FR", l("&6SupremeShops >> &7Cliquez-droit sur les shops desquels il faut retirer complètement (bénéfices). Accroupissez-vous pour arrêter."),
			"zh_CN", l("&6SupremeShops >> &7右击商店取出所有的 (benefits).潜行以停止."),
			"zh_TW", l("&6SupremeShops >> &f右鍵點擊&7商店取回全部的物品 (買家->你 的東西)。(SHIFT 取消)"),
			"ru_RU", l("&6SupremeShops >> &7ПКМ по магазинам, чтобы вывести всё заработанное. SHIFT, дабы прекратить."),
			"hu_HU", l("&6SupremeShops >> &7Jobb-klikk a boltokra az összes visszavonásához (haszon). Lopakodó gomb a leállításhoz."),
			"es_ES", l("&6SupremeShops >> &7Haz clic derecho en las tiendas para recoger todo el beneficio. Agáchate para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_MULTIWITHDRAWALLSTOCKSTART = n("MSG_SUPREMESHOPS_MULTIWITHDRAWALLSTOCKSTART",
			"en_US", l("&6SupremeShops >> &7Right-click on the shops to withdraw all (stock). Sneak to stop."),
			"fr_FR", l("&6SupremeShops >> &7Cliquez-droit sur les shops desquels il faut retirer complètement (stock). Accroupissez-vous pour arrêter."),
			"zh_CN", l("&6SupremeShops >> &7右击商店取出所有的 (stock). 潜行以停止."),
			"zh_TW", l("&6SupremeShops >> &f右鍵點擊&7商店取回全部的物品 (你->買家 的東西)。(SHIFT 取消)"),
			"ru_RU", l("&6SupremeShops >> &7ПКМ по магазинам, чтобы снять ассортимент с продажи. SHIFT, дабы прекратить."),
			"hu_HU", l("&6SupremeShops >> &7Jobb-klikk a boltokra az összes visszavonásához. (készlet). Lopakodó gomb a leállításhoz."),
			"es_ES", l("&6SupremeShops >> Haz clic derecho para recoger todo el stock. Agáchate para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_RESTOCKEDALLMULTI = n("MSG_SUPREMESHOPS_RESTOCKEDALLMULTI",
			"en_US", l("&6SupremeShops >> &7Restock all for this shop."),
			"fr_FR", l("&6SupremeShops >> &7Restocké complètement pour ce shop."),
			"zh_CN", l("&6SupremeShops >> &7已为商店补充所有库存."),
			"zh_TW", l("&6SupremeShops >> &7已為商店補充所有的庫存。"),
			"ru_RU", l("&6SupremeShops >> &7Пополнить все запасы этого магазина."),
			"hu_HU", l("&6SupremeShops >> &7Minden készlet feltöltése ennél a boltnál."),
			"es_ES", l("&6SupremeShops >> &7Realizar un reestock completo para esta tienda.")
			);

	public static final Text MSG_SUPREMESHOPS_WITHDRAWNALLBENEFITSMULTI = n("MSG_SUPREMESHOPS_WITHDRAWNALLBENEFITSMULTI",
			"en_US", l("&6SupremeShops >> &7Withdrawn all benefits for this shop."),
			"fr_FR", l("&6SupremeShops >> &7Bénéfices retirés complètement pour ce shop."),
			"zh_CN", l("&6SupremeShops >> &7从这个商店拿取所有的benefits."),
			"zh_TW", l("&6SupremeShops >> &7從這個商店拿取所有的物品 (買家->你 的東西)"),
			"ru_RU", l("&6SupremeShops >> &7Вывести всё заработанное с этого магазина."),
			"hu_HU", l("&6SupremeShops >> &7A bolt minden juttatása visszavonva."),
			"es_ES", l("&6SupremeShops >> &7Recoge todos los beneficios de esta tienda.")
			);

	public static final Text MSG_SUPREMESHOPS_WITHDRAWNALLSTOCKMULTI = n("MSG_SUPREMESHOPS_WITHDRAWNALLSTOCKMULTI",
			"en_US", l("&6SupremeShops >> &7Withdrawn all stock for this shop."),
			"fr_FR", l("&6SupremeShops >> &7Stock retiré complètement pour ce shop."),
			"zh_CN", l("&6SupremeShops >> &7已取出这个商店的所有库存."),
			"zh_TW", l("&6SupremeShops >> &7已取出這個商店所有的物品 (你->買家 的東西)"),
			"ru_RU", l("&6SupremeShops >> &7Снять с продажи весь ассортимент этого магазина."),
			"hu_HU", l("&6SupremeShops >> &7A bolt minden készlete visszavonva."),
			"es_ES", l("&6SupremeShops >> &7Recoge todo el stock de esta tienda.")
			);

	public static final Text MSG_SUPREMESHOPS_NOSTOCKADMINSHOP = n("MSG_SUPREMESHOPS_NOSTOCKADMINSHOP",
			"en_US", l("&6SupremeShops >> &7Admin shops have no stock management."),
			"fr_FR", l("&6SupremeShops >> &7Les shops admins n'ont pas de gestion du stock."),
			"zh_CN", l("&6SupremeShops >> &7管理员商店不需要补充库存."),
			"zh_TW", l("&6SupremeShops >> &7管理員商店不需要補貨。"),
			"ru_RU", l("&6SupremeShops >> &7Админ-шоп не требует управлением остатками."),
			"hu_HU", l("&6SupremeShops >> &7Az Admini boltokban nincs raktárkezelés."),
			"es_ES", l("&6SupremeShops >> &7Las tiendas de administrador no tienen gestión de stock (si desactivas esta opción tendrás que realizar el restock manualmente!).")
			);

	public static final Text MSG_SUPREMESHOPS_DIDNTDISCOVERALLSHOPITEMS = n("MSG_SUPREMESHOPS_DIDNTDISCOVERALLSHOPITEMS",
			"en_US", l("&6SupremeShops >> &7There are items you didn't discover yet in this shop."),
			"fr_FR", l("&6SupremeShops >> &7Il y a des items que vous n'avez pas encore découvert dans ce shop."),
			"zh_TW", l("&6SupremeShops >> &7這個商店中有一些你尚未發現的商品"),
			"ru_RU", l("&6SupremeShops >> &7В этом магазине есть предметы, которые вы ещё не посмотрели."),
			"hu_HU", l("&6SupremeShops >> &7Vannak olyan tételek ebben a boltban, amik még nincsenek felfedezve."),
			"es_ES", l("&6SupremeShops >> &7Hay objetos que aún no has descubierto en esta tienda.")
			);

	public static final Text MSG_SUPREMESHOPS_MULTIRESTOCKALLEND = n("MSG_SUPREMESHOPS_MULTIRESTOCKALLEND",
			"en_US", l("&6SupremeShops >> &7Multiple shop restock all ended."),
			"fr_FR", l("&6SupremeShops >> &7Restock complet de multiple shops terminé."),
			"zh_CN", l("&6SupremeShops >> &7已为多个商店补充库存."),
			"zh_TW", l("&6SupremeShops >> &7已為多個商店補貨。"),
			"ru_RU", l("&6SupremeShops >> &7Пополнение запасов нескольких магазинов выполнено."),
			"hu_HU", l("&6SupremeShops >> &7Több üzlet készletfeltöltése véget ért."),
			"es_ES", l("&6SupremeShops >> &7Terminó de hacerse el restock completo de múltiples tiendas.")
			);

	public static final Text MSG_SUPREMESHOPS_MULTIWITHDRAWALLBENEFITSEND = n("MSG_SUPREMESHOPS_MULTIWITHDRAWALLBENEFITSEND",
			"en_US", l("&6SupremeShops >> &7Multiple shop benefits withdraw all ended."),
			"fr_FR", l("&6SupremeShops >> &7Retrait de bénéfices complet de multiple shops terminé."),
			"zh_CN", l("&6SupremeShops >> &7Multiple shop benefits withdraw all ended."),
			"zh_TW", l("&6SupremeShops >> &7已從多個商店取回物品 (買家->你 的東西)"),
			"ru_RU", l("&6SupremeShops >> &7Вывод заработанного из нескольких магазинов прошёл успешно."),
			"hu_HU", l("&6SupremeShops >> &7Több bolt juttatásának visszavonása véget ért."),
			"es_ES", l("&6SupremeShops >> &7Terminó de hacerse la extracción de beneficios de múltiples tiendas.")
			);

	public static final Text MSG_SUPREMESHOPS_MULTIWITHDRAWALLSTOCKSEND = n("MSG_SUPREMESHOPS_MULTIWITHDRAWALLSTOCKSEND",
			"en_US", l("&6SupremeShops >> &7Multiple shop stock withdraw all ended."),
			"fr_FR", l("&6SupremeShops >> &7Retrait de stock complet de multiple shops terminé."),
			"zh_CN", l("&6SupremeShops >> &7Multiple shop stock withdraw all ended."),
			"zh_TW", l("&6SupremeShops >> &7已從多個商店取回物品 (你->買家 的東西)"),
			"ru_RU", l("&6SupremeShops >> &7Снятие с продажи остатков в нескольких ваших магазинах прошло успешно."),
			"hu_HU", l("&6SupremeShops >> &7Több bolt raktárkészletének visszavonása mind véget ért."),
			"es_ES", l("&6SupremeShops >> &7Terminó de hacerse la extracción de stocks de múltiples tiendas.")
			);

	public static final Text MSG_SUPREMESHOPS_CONFIRSHOPSPURGE = n("MSG_SUPREMESHOPS_CONFIRSHOPSPURGE",
			"en_US", l("&6SupremeShops >> &7Please execute the command again to confirm the &cdeletion of all shops of {owner} &7(expires in 15 seconds)."),
			"fr_FR", l("&6SupremeShops >> &7Merci d'exécuter la commande à nouveau pour confirmer la &csuppression de tous les shops de {owner} &7(expire dans 15 secondes)."),
			"zh_CN", l("&6SupremeShops >> &7请再次输入指令以确认&c删除{owner}的所有商店 &7(15秒后过期)."),
			"zh_TW", l("&6SupremeShops >> &7請再次輸入指令以確認&c刪除{owner}&7的所有商店 &f(15秒後過期)"),
			"ru_RU", l("&6SupremeShops >> &7Повторите команду, дабы подтвердить &cудаление всех магазинов игрока {owner} &7(у вас есть 15 секунд на подтверждение)."),
			"hu_HU", l("&6SupremeShops >> &7Kérlek, hajtsd végre a parancsot újra, hogy megerősítsd {owner} összes üzletének &ctörlését&7. (15 másodperc múlva lejár)."),
			"es_ES", l("&6SupremeShops >> &7Por favor ejecuta el comando de nuevo para confirmar la &celminación de todas las tiendas de {owner} &7(expira en 15 segundos).")
			);

	public static final Text MSG_SUPREMESHOPS_SHOPSPURGED = n("MSG_SUPREMESHOPS_SHOPSPURGED",
			"en_US", l("&6SupremeShops >> &7Deleted all shops of &c{owner}&7."),
			"fr_FR", l("&6SupremeShops >> &7Tous les shops de &c{owner} &7ont été supprimés."),
			"zh_CN", l("&6SupremeShops >> &7已删除&c{owner}&7的所有商店."),
			"zh_TW", l("&6SupremeShops >> &7已刪除&c{owner}&7的所有商店。"),
			"ru_RU", l("&6SupremeShops >> &7Все магазины игрока &c{owner}&7 успешно удалены."),
			"hu_HU", l("&6SupremeShops >> &c{owner}&7 minden boltját törölted."),
			"es_ES", l("&6SupremeShops >> &7Se han borrado todas las tiendas de &c{owner}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CONFIRMERCHANTSPURGE = n("MSG_SUPREMESHOPS_CONFIRMERCHANTSPURGE",
			"en_US", l("&6SupremeShops >> &7Please execute the command again to confirm the &cdeletion of all merchants of {owner} &7(expires in 15 seconds)."),
			"fr_FR", l("&6SupremeShops >> &7Merci d'exécuter la commande à nouveau pour confirmer la &csuppression de tous les marchands de {owner} &7(expire dans 15 secondes)."),
			"zh_CN", l("&6SupremeShops >> &7请再次输入指令以确认&c删除{owner}的所有商人 &7(15秒后过期)."),
			"zh_TW", l("&6SupremeShops >> &7請再次輸入指令以確認&c刪除{owner}&7的所有商人 &f(15秒後過期)"),
			"ru_RU", l("&6SupremeShops >> &7Повторите команду, дабы подтвердить &cудаление всех торговцев игрока {owner} &7(у вас есть 15 секунд на подтверждение)."),
			"hu_HU", l("&6SupremeShops >> &7Kérlek, hajtsd végre a parancsot újra, hogy megerősítsd {owner} összes kereskedőjének &ctörlését&7. (15 másodperc múlva lejár)."),
			"es_ES", l("&6SupremeShops >> &7Por favor ejecuta de nuevo el comando para confirmar la &celiminación de todos los mercaderes de {owner} &7(expira en 15 segundos).")
			);

	public static final Text MSG_SUPREMESHOPS_MERCHANTSPURGED = n("MSG_SUPREMESHOPS_MERCHANTSPURGED",
			"en_US", l("&6SupremeShops >> &7Deleted all merchants of &c{owner}&7."),
			"fr_FR", l("&6SupremeShops >> &7Tous les marchands de &c{owner} &7ont été supprimés."),
			"zh_CN", l("&6SupremeShops >> &7已删除&c{owner}&7的所有商人."),
			"zh_TW", l("&6SupremeShops >> &7已刪除&c{owner}&7的所有商人。"),
			"ru_RU", l("&6SupremeShops >> &7Все магазины игрока &c{owner}&7 успешно удалены."),
			"hu_HU", l("&6SupremeShops >> &c{owner}&7 minden kereskedőjét törölted."),
			"es_ES", l("&6SupremeShops >> &7Se han eliminado todos los mercaderes de &c{owner}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_PLEASEWITHDRAW = n("MSG_SUPREMESHOPS_PLEASEWITHDRAW",
			"en_US", l("&6SupremeShops >> &7Please withdraw the remaining stock first."),
			"fr_FR", l("&6SupremeShops >> &7Merci de récupérer le stock restant d'abord."),
			"zh_CN", l("&6SupremeShops >> &7请先取出所有库存."),
			"zh_TW", l("&6SupremeShops >> &7請先取出所有物品 (你->買家 的東西)"),
			"ru_RU", l("&6SupremeShops >> &7Для начала нужно забрать остатки со склада."),
			"hu_HU", l("&6SupremeShops >> &7Kérlek, először vedd ki a fennmaradó készletet."),
			"es_ES", l("&6SupremeShops >> &7Por favor retira el stock restante primero.")
			);

	public static final Text MSG_SUPREMESHOPS_SHOPCLOSED = n("MSG_SUPREMESHOPS_SHOPCLOSED",
			"en_US", l("&6SupremeShops >> &7This shop is closed."),
			"fr_FR", l("&6SupremeShops >> &7Ce magasin est fermé."),
			"zh_CN", l("&6SupremeShops >> &7商店已打烊."),
			"zh_TW", l("&6SupremeShops >> &7商店已打烊。"),
			"hu_HU", l("&6SupremeShops >> &7Ez a bolt bezárt."),
			"es_ES", l("&6SupremeShops >> &7La tienda está cerrada.")
			);

	public static final Text MSG_SUPREMESHOPS_MERCHANTCLOSED = n("MSG_SUPREMESHOPS_MERCHANTCLOSED",
			"en_US", l("&6SupremeShops >> &7This merchant is closed."),
			"fr_FR", l("&6SupremeShops >> &7Ce marchand est fermé."),
			"zh_CN", l("&6SupremeShops >> &7商店已打烊."),
			"zh_TW", l("&6SupremeShops >> &7商人已打烊。"),
			"hu_HU", l("&6SupremeShops >> &7Ez a kereskedő bezárt."),
			"es_ES", l("&6SupremeShops >> &7Este mercader está cerrado.")
			);

	public static final Text MSG_SUPREMESHOPS_EMPTY = n("MSG_SUPREMESHOPS_EMPTY",
			"en_US", l("&6SupremeShops >> &7This shop is empty, there's nothing to trade."),
			"fr_FR", l("&6SupremeShops >> &7Ce magasin est vide, il n'y a rien à échanger."),
			"zh_CN", l("&6SupremeShops >> &7这个商店空空如也，没有可以交易的东西."),
			"zh_TW", l("&6SupremeShops >> &7這個商店空空如也，沒有可以交易的物品。"),
			"ru_RU", l("&6SupremeShops >> &7Данный магазин пуст, даже купить нечего :("),
			"hu_HU", l("&6SupremeShops >> &7Ez a bolt üres. Nincs itt semmi, amivel kereskedni lehetne."),
			"es_ES", l("&6SupremeShops >> &7Esta tienda está vacía, no hay nada que comerciar.")
			);

	public static final Text MSG_SUPREMESHOPS_DISALLOWEDGAMEMODE = n("MSG_SUPREMESHOPS_DISALLOWEDGAMEMODE",
			"en_US", l("&6SupremeShops >> &7You can't trade with this gamemode."),
			"fr_FR", l("&6SupremeShops >> &7Vous ne pouvez pas échanger avec ce gamemode."),
			"zh_CN", l("&6SupremeShops >> &7你不能在这个游戏模式下进行交易."),
			"zh_TW", l("&6SupremeShops >> &7你不能在這個遊戲模式下進行交易。"),
			"ru_RU", l("&6SupremeShops >> &7Сделки запрещены в данном игровом режиме."),
			"hu_HU", l("&6SupremeShops >> &7Nem tudsz kereskedni ezzel a játékmóddal."),
			"es_ES", l("&6SupremeShops >> &7No puedes comerciar con este modo de juego.")
			);

	public static final Text MSG_SUPREMESHOPS_TRADESLIMITWOULDEXCEEDSHOP = n("MSG_SUPREMESHOPS_TRADESLIMITWOULDEXCEEDSHOP",
			"en_US", l("&6SupremeShops >> &7Trading this much would exceed the trade limit for this shop."),
			"fr_FR", l("&6SupremeShops >> &7Échanger autant dépasserait le nombre limite d'échanges pour ce shop."),
			"zh_TW", l("&6SupremeShops >> &7交易這些數量會超出商店的交易最大限制。"),
			"ru_RU", l("&6SupremeShops >> &7Обмен такого объёма превысил бы торговый лимит самого магазина..."),
			"hu_HU", l("&6SupremeShops >> &7Ha ennyit kereskednél, meghaladnád ennek a boltnak a kereskedelmi korlátait."),
			"es_ES", l("&6SupremeShops >> &7Comerciar tanto hará que el límite de comercios para esta tienda se exceda.")
			);

	public static final Text MSG_SUPREMESHOPS_TRADESLIMITWOULDEXCEEDFREEINVENTORYSPACE = n("MSG_SUPREMESHOPS_TRADESLIMITWOULDEXCEEDFREEINVENTORYSPACE",
			"en_US", l("&6SupremeShops >> &7Trading this much would exceed the free space in your inventory."),
			"fr_FR", l("&6SupremeShops >> &7Échanger autant dépasserait l'espace libre dans votre inventaire."),
			"zh_TW", l("&6SupremeShops >> &7交易這些數量會超出你包包有的空間。"),
			"hu_HU", l("&6SupremeShops >> &7Ha ennyit kereskednél, meghaladnád az eszköztárban lévő összes szabad helyet."),
			"es_ES", l("&6SupremeShops >> &7Comerciar tanto hará que el espacio libre en tu inventario se llene.")
			);

	public static final Text MSG_SUPREMESHOPS_TRADESLIMITWOULDEXCEEDMERCHANT = n("MSG_SUPREMESHOPS_TRADESLIMITWOULDEXCEEDMERCHANT",
			"en_US", l("&6SupremeShops >> &7Trading this much would exceed the trade limit for this merchant."),
			"fr_FR", l("&6SupremeShops >> &7Échanger autant dépasserait le nombre limite d'échanges pour ce marchand."),
			"zh_TW", l("&6SupremeShops >> &7交易這些數量會超出商人的最大交易數量。"),
			"ru_RU", l("&6SupremeShops >> &7Обмен такого объёма превысил бы торговый лимит самого торговца..."),
			"hu_HU", l("&6SupremeShops >> &7Ha ennyit kereskednél, meghaladnád ennek a kereskedőnek a kereskedelmi korlátait."),
			"es_ES", l("&6SupremeShops >> &7Comerciar tanto hará que el límite de comercio para este mercader se exceda.")
			);

	public static final Text MSG_SUPREMESHOPS_MAXSHOPS = n("MSG_SUPREMESHOPS_MAXSHOPS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum shops limit."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de shops."),
			"zh_CN", l("&6SupremeShops >> &7你已达到商店上限."),
			"zh_TW", l("&6SupremeShops >> &7你已達到商店數量上限。"),
			"ru_RU", l("&6SupremeShops >> &7Вы достигли максимального количества магазинов."),
			"hu_HU", l("&6SupremeShops >> &7Elérted a maximum bolt korlátot."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de tiendas.")
			);

	public static final Text MSG_SUPREMESHOPS_SHOPCANTEXIST = n("MSG_SUPREMESHOPS_SHOPCANTEXIST",
			"en_US", l("&6SupremeShops >> &7This shop can't exist (maybe because of creation conditions, for example)."),
			"fr_FR", l("&6SupremeShops >> &7Ce shop ne peut pas exister (peut-être à cause de conditions de création, par exemple)."),
			"es_ES", l("&6SupremeShops >> &7Esta tienda no puede existir (quizás por las condiciones de creación, por ejemplo).")
			);

	public static final Text MSG_SUPREMESHOPS_MAXRENTEDSHOPS = n("MSG_SUPREMESHOPS_MAXRENTEDSHOPS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum rented shops limit."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de shops loués."),
			"zh_TW", l("&6SupremeShops >> &7你已達到租用商店數量上限。"),
			"ru_RU", l("&6SupremeShops >> &7Вы достигли лимита арендных магазинов."),
			"hu_HU", l("&6SupremeShops >> &7Elérted a maximum kibérelhető bolt korlátot."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de tiendas alquiladas.")
			);

	public static final Text MSG_SUPREMESHOPS_MAXRENTEDMERCHANTS = n("MSG_SUPREMESHOPS_MAXRENTEDMERCHANTS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum rented merchants limit."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de marchands loués."),
			"zh_TW", l("&6SupremeShops >> &7你已達到租用商人數量上限。"),
			"hu_HU", l("&6SupremeShops >> &7Elérted a maximum kibérelhető kereskedő korlátot."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de mercaderes alquilados.")
			);

	public static final Text MSG_SUPREMESHOPS_MAXMERCHANTS = n("MSG_SUPREMESHOPS_MAXMERCHANTS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum merchants limit."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de marchands."),
			"zh_CN", l("&6SupremeShops >> &7你已达到商人上限."),
			"zh_TW", l("&6SupremeShops >> &7你已達到商人數量上限。"),
			"ru_RU", l("&6SupremeShops >> &7Вы достигли максимального количества торговцев."),
			"hu_HU", l("&6SupremeShops >> &7Elérted a kereskedők maximális korlátját."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de mercaderes.")
			);

	public static final Text MSG_SUPREMESHOPS_MAXLINKEDMERCHANTSHOPS = n("MSG_SUPREMESHOPS_MAXLINKEDMERCHANTSHOPS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum linked shops limit for this merchant."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de shops liés à ce marchand."),
			"zh_TW", l("&6SupremeShops >> &7你已達到這個商人與已連結商店的最大限制。"),
			"hu_HU", l("&6SupremeShops >> &7Elérted a kapcsolt üzletek korlátját ennél a kereskedőnél."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de tiendas enlazadas para este comerciante.")
			);

	public static final Text MSG_SUPREMESHOPS_MAXCREATEDMERCHANTSHOPS = n("MSG_SUPREMESHOPS_MAXCREATEDMERCHANTSHOPS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum created shops limit belonging to this merchant."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de shops créés appartenants à ce marchand."),
			"zh_TW", l("&6SupremeShops >> &7你已達到在這個商人中創建商店的最大限制。"),
			"hu_HU", l("&6SupremeShops >> &7Elérted a kereskedőkhöz tartozó maximálisan létrehozott boltkorlátot."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de tiendas creadas pertenecientes a este mercader.")
			);

	public static final Text MSG_SUPREMESHOPS_MAXBLOCKSHOPS = n("MSG_SUPREMESHOPS_MAXBLOCKSHOPS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum block shops limit."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de shops blocs."),
			"zh_TW", l("&6SupremeShops >> &7你已達到方塊商店的最大限制。"),
			"ru_RU", l("&6SupremeShops >> &7Вы достигли лимита по количеству магазинов с блоками."),
			"hu_HU", l("&6SupremeShops >> &7Elérted a maximális blokk-bolt korlátot."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de tiendas de bloques.")
			);

	public static final Text MSG_SUPREMESHOPS_MAXBLOCKMERCHANTS = n("MSG_SUPREMESHOPS_MAXBLOCKMERCHANTS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum block merchants limit."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de shops marchands."),
			"zh_TW", l("&6SupremeShops >> &7你已達到方塊商人的最大限制。"),
			"ru_RU", l("&6SupremeShops >> &7Вы достигли лимита по количеству торговцев блоками."),
			"hu_HU", l("&6SupremeShops >> &7Elérted a maximális blokk-kereskedő korlátot."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de mercaderes de bloques.")
			);

	public static final Text MSG_SUPREMESHOPS_MAXSIGNSHOPS = n("MSG_SUPREMESHOPS_MAXSIGNSHOPS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum sign shops limit."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de shops panneaux."),
			"zh_TW", l("&6SupremeShops >> &7你已達到告示牌商店的最大限制。"),
			"ru_RU", l("&6SupremeShops >> &7Вы достигли лимита своих магазинов-табличек."),
			"hu_HU", l("&6SupremeShops >> &7Elérted a maximum tábla-bolt korlátot."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de tiendas de señales.")
			);

	public static final Text MSG_SUPREMESHOPS_MAXSIGNMERCHANTS = n("MSG_SUPREMESHOPS_MAXSIGNMERCHANTS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum sign merchants limit."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de marchands panneaux."),
			"zh_TW", l("&6SupremeShops >> &7你已達到告示牌商人最大限制。"),
			"ru_RU", l("&6SupremeShops >> &7Вы достигли лимита своих торговцев-табличек."),
			"hu_HU", l("&6SupremeShops >> &7Elérted a maximális tábla-kereskedő korlátot."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de mercader de señales.")
			);

	public static final Text MSG_SUPREMESHOPS_MAXSHOPTRADECONDITIONS = n("MSG_SUPREMESHOPS_MAXSHOPTRADECONDITIONS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum trade conditions limit for this shop."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de conditions d'échange pour ce shop."),
			"zh_CN", l("&6SupremeShops >> &7你已达到这个商店的交易上限."),
			"zh_TW", l("&6SupremeShops >> &7你已達到這個商店的交易最大限制。"),
			"ru_RU", l("&6SupremeShops >> &7Вы достигли верхнего предела условий торговли для этого магазина."),
			"hu_HU", l("&6SupremeShops >> &7Elérted a maximális kereskedelmi feltételeket ehhez az üzlethez."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de condiciones para comerciar en esta tienda.")
			);

	public static final Text MSG_SUPREMESHOPS_MAXMERCHANTINTERACTCONDITIONS = n("MSG_SUPREMESHOPS_MAXMERCHANTINTERACTCONDITIONS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum interact conditions limit for this merchant."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de conditions d'interaction pour ce marchand."),
			"zh_CN", l("&6SupremeShops >> &7你已达到这个商人的交互上限."),
			"zh_TW", l("&6SupremeShops >> &7你已達到這個商人的互動最大限制。"),
			"ru_RU", l("&6SupremeShops >> &7Вы достигли верхнего предела условий взаимодействия для этого торговца."),
			"hu_HU", l("&6SupremeShops >> &7Elérted a kereskedő maximális interaktív feltételeit."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo para las condiciones de interacción para este mercader.")
			);

	public static final Text MSG_SUPREMESHOPS_MAXGUISHOPS = n("MSG_SUPREMESHOPS_MAXGUISHOPS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum GUI shops limit."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de shops GUIs."),
			"zh_CN", l("&6SupremeShops >> &7你已达到GUI商店上限."),
			"zh_TW", l("&6SupremeShops >> &7你已達到GUI商店的最大限制。"),
			"hu_HU", l("&6SupremeShops >> &7Elérted a maximális GUI boltok korlátját."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de tiendas de GUI (interfaz visual).")
			);

	public static final Text MSG_SUPREMESHOPS_MAXMERCHANTSHOPS = n("MSG_SUPREMESHOPS_MAXMERCHANTSHOPS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum merchant shops limit (for all merchants)."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de shops marchands (pour tous les marchands)."),
			"zh_CN", l("&6SupremeShops >> &7你已达到这个商人的商店上限."),
			"zh_TW", l("&6SupremeShops >> &7你已達到這個商人的商店最大限制。(全部商人)"),
			"hu_HU", l("&6SupremeShops >> &7Elérted a maximális kereskedő boltok korlátját (minden kereskedőért)."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de tiendas de mercaderes (para todos los mercaderes).")
			);

	public static final Text MSG_SUPREMESHOPS_MAXPHYSICALREMOTESHOPS = n("MSG_SUPREMESHOPS_MAXPHYSICALREMOTESHOPS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum physcal remote shops limit."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de shops physiques à distance."),
			"zh_CN", l("&6SupremeShops >> &7You have reached your maximum physcal remote shops limit."),
			"zh_TW", l("&6SupremeShops >> &7你已達到物理控制商店最大限制。"),
			"ru_RU", l("&6SupremeShops >> &7Вы достигли максимального количество удалённых магазинов."),
			"hu_HU", l("&6SupremeShops >> &7Elérted a maximális fizikai távirányításos boltok korlátját."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de tiendas remotas físicas.")
			);

	public static final Text MSG_SUPREMESHOPS_MAXREMOTEMERCHANTS = n("MSG_SUPREMESHOPS_MAXREMOTEMERCHANTS",
			"en_US", l("&6SupremeShops >> &7You have reached your maximum remote merchants limit."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez atteint votre limite maximum de merchants à distance."),
			"zh_CN", l("&6SupremeShops >> &7You have reached your maximum remote merchants limit."),
			"zh_TW", l("&6SupremeShops >> &7你已達到物理控制商人最大限制。"),
			"ru_RU", l("&6SupremeShops >> &7Вы достигли максимального количество удалённых торговцев."),
			"hu_HU", l("&6SupremeShops >> &7Elérted a maximális távirányításos kereskedők korlátját."),
			"es_ES", l("&6SupremeShops >> &7Has alcanzado tu límite máximo de mercaderes remotos.")
			);

	public static final Text MSG_SUPREMESHOPS_MUSTUNRENTSHOPMAXSTREAKLIMIT = n("MSG_SUPREMESHOPS_MUSTUNRENTSHOPMAXSTREAKLIMIT",
			"en_US", l("&6SupremeShops >> &7Your rented shop &c{shop} &7has exceeded the rent periods streak limit. You have &c{time} &7to empty it and unrent it, otherwise it'll be done automatically."),
			"fr_FR", l("&6SupremeShops >> &7Votre shop loué &c{shop} &7a dépassé la limite de périodes de location consécutives. Vous avez &c{time} &7pour le vider et arrêter la location, sinon ce sera fait automatiquement."),
			"zh_TW", l("&6SupremeShops >> &7你租用的商店 &c{shop} &7已經超過租借者設定的租用次數限制。你有 &c{time} &7將商店清空並退租，否則將會自動完成。"),
			"ru_RU", l("&6SupremeShops >> &7Срок аренды для магазина (&c{shop}) &7вышел. У вас есть &c{time} &7на то, чтобы освободить его и вернуть арендатору, в ином случае это будет сделано автоматически."),
			"hu_HU", l("&6SupremeShops >> &7Az általad bérelt &c{shop} &7bolt túllépte a bérleti időszakra vonatkozó korlátot. A készletek kiürítésére van &c{time} &7időd, különben automatikusan megtörténik."),
			"es_ES", l("&6SupremeShops >> &7Tu tienda alquilada &c{shop}&7 ha excedido el límite del período de alquiler. Tienes &c{time}&7 para vaciarlo y dejar el alquiler, de otra manera se hará automáticamente.")
			);

	public static final Text MSG_SUPREMESHOPS_MUSTPAYSHOPRENT = n("MSG_SUPREMESHOPS_MUSTPAYSHOPRENT",
			"en_US", l("&6SupremeShops >> &7You didn't pay rent for your rented shop &c{shop}&7. You have &c{time} &7to pay (or to empty it and unrent it), otherwise it'll be unrented automatically."),
			"fr_FR", l("&6SupremeShops >> &7Vous n'avez pas payé le prix de location pour votre shop loué &c{shop}&7. Vous avez &c{time} &7pour payer (ou le vider et arrêter la location), sinon un huissier viendra chercher vos meubles."),
			"zh_TW", l("&6SupremeShops >> &7你尚未支付商店 &c{shop} &7的租借費用。你還剩下 &c{time} &7可以付款(或是清空商店並退租)，否則期限一到將會自動退租。"),
			"ru_RU", l("&6SupremeShops >> &7Вы не заплатили арендную плату за магазин (&c{shop})&7. У вас есть &c{time} &7на то, чтобы оплатить аренду или отказаться от него, в ином случае это будет сделано автоматически."),
			"hu_HU", l("&6SupremeShops >> &7Nem fizetted ki a &c{shop}&7 üzltetedre szóló bérleti díjat. A kifizetésre &c{time} &7időd van (vagy ürísd ki a készltetet és mondd le), különben automatikusan megszűnik a bérleti szerződés."),
			"es_ES", l("&6SupremeShops >> &7No has pagado el alquiler para tu tienda alquilada &c{shop}&7. Tienes &c{time}&7 para pagar (o vaciarla y dejar el alquiler), de otra manera se desregistrará tu alquiler automáticamente.")
			);

	public static final Text MSG_SUPREMESHOPS_MUSTUNRENTMERCHANTMAXSTREAKLIMIT = n("MSG_SUPREMESHOPS_MUSTUNRENTMERCHANTMAXSTREAKLIMIT",
			"en_US", l("&6SupremeShops >> &7Your rented merchant &c{merchant} &7has exceeded the rent periods streak limit. You have &c{time} &7to empty it and unrent it, otherwise it'll be done automatically."),
			"fr_FR", l("&6SupremeShops >> &7Votre marchand loué &c{merchant} &7a dépassé la limite de périodes de location consécutives. Vous avez &c{time} &7pour le vider et arrêter la location, sinon ce sera fait automatiquement."),
			"zh_TW", l("&6SupremeShops >> &7你租用的商人 &c{shop} &7已經超過租借者設定的租用次數限制。你有 &c{time} &7將商店清空並退租，否則將會自動完成。"),
			"ru_RU", l("&6SupremeShops >> &7Срок аренды торговца (&c{merchant}) &7вышел. У вас есть &c{time} &7на то, чтобы освободить его, в ином случае это будет сделано автоматически."),
			"hu_HU", l("&6SupremeShops >> &7A &c{merchant}&7 nevű bérelt kereskedőd túllépte a bérleti időszakra vonatkozó korlátot. A készletek kiürítésére &c{time} &7időd van, különben automatikusan megtörténik."),
			"es_ES", l("&6SupremeShops >> &7Tu mercader alquilado &c{merchant}&7 ha excedido el límite del período de alquiler. Tienes &c{time}&7 para vaciarlo y dejar el alquiler, de otra manera se hará automáticamente.")
			);

	public static final Text MSG_SUPREMESHOPS_MUSTPAYMERCHANTRENT = n("MSG_SUPREMESHOPS_MUSTPAYMERCHANTRENT",
			"en_US", l("&6SupremeShops >> &7You didn't pay rent for your rented merchant &c{merchant}&7. You have &c{time} &7to pay (or to empty it and unrent it), otherwise it'll be unrented automatically."),
			"fr_FR", l("&6SupremeShops >> &7Vous n'avez pas payé le prix de location pour votre marchand loué &c{merchant}&7. Vous avez &c{time} &7pour payer (ou le vider et arrêter la location), sinon un huissier viendra chercher vos meubles."),
			"zh_TW", l("&6SupremeShops >> &7你尚未支付商人 &c{merchant} &7的租借費用。你還剩下 &c{time} &7可以付款(或是清空商店並退租)，否則期限一到將會自動退租。"),
			"ru_RU", l("&6SupremeShops >> &7Вы не заплатили арендную плату за торгоца (&c{merchant})&7. У вас есть &c{time} &7на то, чтобы оплатить аренду или отказаться от него, в ином случае это будет сделано автоматически."),
			"hu_HU", l("&6SupremeShops >> &7Nem fizetted ki a &c{merchant}&7 üzltetedre szóló bérleti díjat. A kifizetésre &c{time} &7időd van (vagy ürísd ki a készltetet és mondd le), különben automatikusan megszűnik a bérleti szerződés."),
			"es_ES", l("&6SupremeShops >> &7No has pagado el alquiler para tu mercader alquilado &c{merchant}&7. Tienes &c{time}&7 para pagar (o vaciarla y dejar el alquiler), de otra manera se desregistrará tu alquiler automáticamente.")
			);

	public static final Text MSG_SUPREMESHOPS_SHOPCHANGEDRENTPERIOD = n("MSG_SUPREMESHOPS_SHOPCHANGEDRENTPERIOD",
			"en_US", l("&6SupremeShops >> &7The rent price payment period for your rented shop &a{shop} &7was updated to &a{rent_period}&7."),
			"fr_FR", l("&6SupremeShops >> &7La période de paiement pour le prix de location de votre shop loué &c{shop} &7a été changée sur &a{rent_period}&7."),
			"zh_TW", l("&6SupremeShops >> &7你租借的商店 &a{shop} &7的租借費用週期，已被更新為 &a{rent_period}。"),
			"ru_RU", l("&6SupremeShops >> &7Срок аренды вашего магазина (&a{shop}) &7был обновлён. Новый срок - &a{rent_period}&7."),
			"hu_HU", l("&6SupremeShops >> &7Az általad bérelt &a{shop} &7bolt fizetési időszakát &a{rent_period}&7 időszakra frissítették."),
			"es_ES", l("&6SupremeShops >> &7El período de pago de tu tienda alquilada &a{shop}&7 fue actualizada a &a{rent_period}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_MERCHANTCHANGEDRENTPERIOD = n("MSG_SUPREMESHOPS_MERCHANTCHANGEDRENTPERIOD",
			"en_US", l("&6SupremeShops >> &7The rent price payment period for your rented merchant &a{merchant} &7was updated to &a{rent_period}&7."),
			"fr_FR", l("&6SupremeShops >> &7La période de paiement pour le prix de location de votre marchand loué &c{merchant} &7a été changée sur &a{rent_period}&7."),
			"zh_TW", l("&6SupremeShops >> &7你租借的商人 &a{merchant} &7的租借費用週期，已被更新為 &a{rent_period}。"),
			"ru_RU", l("&6SupremeShops >> &7Срок аренды вашего торговца (&a{merchant}) &7был обновлён. Новый срок - &a{rent_period}&7."),
			"hu_HU", l("&6SupremeShops >> &7Az általad bérelt &a{merchant} &7kereskedő fizetési időszakát &a{rent_period}&7 időszakra frissítették."),
			"es_ES", l("&6SupremeShops >> &7El período de pago de tu mercader alquilado &a{merchant}&7 fue actualizada a &a{rent_period}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CANTRENTSHOPSTREAKLIMITDELAY = n("MSG_SUPREMESHOPS_CANTRENTSHOPSTREAKLIMITDELAY",
			"en_US", l("&6SupremeShops >> &7You have to wait &c{time} &7to rent this shop again."),
			"fr_FR", l("&6SupremeShops >> &7Vous devez attendre &c{time} &7pour pouvoir louer ce shop à nouveau."),
			"zh_TW", l("&6SupremeShops >> &7你必須要等帶 &c{time} &7才能繼續租用這間商店。"),
			"ru_RU", l("&6SupremeShops >> &7Вам нужно подождать ещё &c{time} &7, прежде чем арендовать данный магазин снова."),
			"hu_HU", l("&6SupremeShops >> &7Várnod kell &c{time} &7időt, hogy újra kibérelhesd ezt a boltot."),
			"es_ES", l("&6SupremeShops >> &7Tienes que esperar &c{time}&7 para alquilar esta tienda de nuevo.")
			);

	public static final Text MSG_SUPREMESHOPS_CANTRENTMERCHANTSTREAKLIMITDELAY = n("MSG_SUPREMESHOPS_CANTRENTMERCHANTSTREAKLIMITDELAY",
			"en_US", l("&6SupremeShops >> &7You have to wait &c{time} &7to rent this merchant again."),
			"fr_FR", l("&6SupremeShops >> &7Vous devez attendre &c{time} &7pour pouvoir louer ce marchand à nouveau."),
			"zh_TW", l("&6SupremeShops >> &7你必須要等待 &c{time} &7才能繼續租用這個商人。"),
			"ru_RU", l("&6SupremeShops >> &7Вам нужно подождать ещё &c{time} &7, прежде чем арендовать данного торговца снова."),
			"hu_HU", l("&6SupremeShops >> &7Várnod kell &c{time} &7időt, hogy újra kibérelhesd ezt a kereskedőt."),
			"es_ES", l("&6SupremeShops >> &7Tienes que esperar &c{time}&7 para alquilar este mercader de nuevo.")
			);

	public static final Text MSG_SUPREMESHOPS_UNRENTEDSHOP = n("MSG_SUPREMESHOPS_UNRENTEDSHOP",
			"en_US", l("&6SupremeShops >> &7Your shop &c{shop} &7was unrented. Paid rents and remaining stock have been given to you (or eventually dropped on the ground)."),
			"fr_FR", l("&6SupremeShops >> &7La location de votre shop &c{shop} &7a été arrêtée. Les prix de locations payés et le stock restant vous ont été donnés (ou éventuellement droppés sur le sol)."),
			"zh_TW", l("&6SupremeShops >> &7你的商店 &c{shop} &7已被退租。預付的租金和剩餘的物品已返還給您(或是掉落在地上了)。"),
			"ru_RU", l("&6SupremeShops >> &7Ваша аренда магазина (&c{shop}) &7была отменена. Оставшаяся сумма за аренду и остатки были возвращены вам (что-то могло выпасть на землю)."),
			"hu_HU", l("&6SupremeShops >> &7A &c{shop} &7boltod bérleti díja fel lett mondva. A fizetett bérleti díjakat és a fennmaradó raktár készleteket megkaptad (vagy lehet a földre esett)."),
			"es_ES", l("&6SupremeShops >> &7Tu tiendas &c{shop}&7 fue desalquilada. Los alquileres pagados y el stock restante te han sido devueltos (o eventualmente será dropeado en el suelo).")
			);

	public static final Text MSG_SUPREMESHOPS_UNRENTEDMERCHANT = n("MSG_SUPREMESHOPS_UNRENTEDMERCHANT",
			"en_US", l("&6SupremeShops >> &7Your merchant &c{merchant} &7was unrented. Paid rents and remaining stock have been given to you (or eventually dropped on the ground). Shops belonging to the merchant have been removed and other shops unlinked."),
			"fr_FR", l("&6SupremeShops >> &7La location de votre marchand &c{merchant} &7a été arrêtée. Les prix de locations payés et le stock restant vous ont été donnés (ou éventuellement droppés sur le sol). Les shops appartenants au marchand ont été supprimés et les autres shops déliés."),
			"zh_TW", l("&6SupremeShops >> &7你的商店 &c{merchant} &7已被退租。預付的租金和剩餘的物品已返還給您(或是掉落在地上了)。"),
			"ru_RU", l("&6SupremeShops >> &7Ваша аренда торговца &c{merchant} &7была отменена. Оставшаяся сумма за аренду и остатки были возвращены вам (что-то могло выпасть на землю). Принадлежащие торговцу магазины были удалены, а все привязанные - отвязаны."),
			"hu_HU", l("&6SupremeShops >> &7A &c{merchant} &7kereskedőd bérleti díja fel lett mondva. A fizetett bérleti díjakat és a fennmaradó raktár készleteket megkaptad (vagy lehet a földre esett). A kereskedőnél lévő összes bolt eltávolításra került."),
			"es_ES", l("&6SupremeShops >> &7Tu mercader &c{merchant}&7 fue desalquilado. Los alquileres pagados y el stock restante te han sido devueltos (o eventualmente será dropeado en el suelo).")
			);

	public static final Text MSG_SUPREMESHOPS_NOTENOUGHMONEYCREATIONTAX = n("MSG_SUPREMESHOPS_NOTENOUGHMONEYCREATIONTAX",
			"en_US", l("&6SupremeShops >> &7You must pay &c{money}$ &7of taxes and you don't have it."),
			"fr_FR", l("&6SupremeShops >> &7Vous devez payer &c{money}$ &7de taxes et vous ne les avez pas."),
			"zh_CN", l("&6SupremeShops >> &7你必须支付&c{money}$ &7of taxes and you don't have it."),
			"zh_TW", l("&6SupremeShops >> &7你必須要支付 &c{money}$ &7當作稅款，但是你並沒有足夠的餘額。"),
			"ru_RU", l("&6SupremeShops >> &7Вам нужно &c{money}$ &7монет, для уплаты комиссии, но у вас недостаточно средств."),
			"hu_HU", l("&6SupremeShops >> &7Ki kell fizetned &c{money}$ &7adót és neked nincs ennyi."),
			"es_ES", l("&6SupremeShops >> &7Debes pagar &c{money}$ &7de impuestos y tú no los tienes.")
			);

	public static final Text MSG_SUPREMESHOPS_PAIDCREATIONTAX = n("MSG_SUPREMESHOPS_PAIDCREATIONTAX",
			"en_US", l("&6SupremeShops >> &7You paid &a{money}$ &7of creation taxes."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez payé &a{money}$ &7de taxes de création."),
			"zh_CN", l("&6SupremeShops >> &7You paid &a{money}$ &7of taxes for the creation of this"),
			"zh_TW", l("&6SupremeShops >> &7你支付 &a{money}$ &7當作創建稅款。"),
			"hu_HU", l("&6SupremeShops >> &7Kifizetted &a{money}$ &7alkotói adót."),
			"es_ES", l("&6SupremeShops >> &7Has pagado &a{money}$ &7de impuestos por creación.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATESHOP = n("MSG_SUPREMESHOPS_CREATESHOP",
			"en_US", l("&6SupremeShops >> &7You created a shop (&a{amount}&7/{max}&7)."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez créé un shop (&a{amount}&7/{max}&7)."),
			"zh_CN", l("&6SupremeShops >> &7You created a shop (&a{amount}&7/{max}&7)."),
			"zh_TW", l("&6SupremeShops >> &7你創建了商店 (&a{amount}&7/&a{max}&7)"),
			"ru_RU", l("&6SupremeShops >> &7Вы создали магазин (&a{amount}&7/{max}&7)."),
			"hu_HU", l("&6SupremeShops >> &7Elkészítettél egy boltot (&a{amount}&7/{max}&7)."),
			"es_ES", l("&6SupremeShops >> &7Has creado una tienda (&a{amount}&7/{max}&7).")
			);

	public static final Text MSG_SUPREMESHOPS_CREATESHOPADMIN = n("MSG_SUPREMESHOPS_CREATESHOPADMIN",
			"en_US", l("&6SupremeShops >> &7You created an admin shop."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez créé un shop admin."),
			"zh_TW", l("&6SupremeShops >> &7你創建了一個管理員商店。"),
			"ru_RU", l("&6SupremeShops >> &7Вы создали Админ-шоп."),
			"hu_HU", l("&6SupremeShops >> &7Elkészítettél egy admin boltot."),
			"es_ES", l("&6SupremeShops >> &7Has creado una tienda de tipo administrador.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATESHOPRENT = n("MSG_SUPREMESHOPS_CREATESHOPRENT",
			"en_US", l("&6SupremeShops >> &7You created a rent shop."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez créé un shop louable."),
			"zh_TW", l("&6SupremeShops >> &7你創建了一個租借商店。"),
			"ru_RU", l("&6SupremeShops >> &7Вы создали арендуемый магазин."),
			"hu_HU", l("&6SupremeShops >> &7Elkészítettél egy bérelt boltot."),
			"es_ES", l("&6SupremeShops >> &7Has creado una tienda de alquiler.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEMERCHANT = n("MSG_SUPREMESHOPS_CREATEMERCHANT",
			"en_US", l("&6SupremeShops >> &7You created a merchant (&a{amount}&7/{max}&7)."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez créé un marchand (&a{amount}&7/{max}&7)."),
			"zh_CN", l("&6SupremeShops >> &7You created a merchant (&a{amount}&7/{max}&7)."),
			"zh_TW", l("&6SupremeShops >> &7你創建了一個商人 (&a{amount}&7/&a{max}&7)"),
			"ru_RU", l("&6SupremeShops >> &7Вы создали торговца (&a{amount}&7/{max}&7)."),
			"hu_HU", l("&6SupremeShops >> &7Elkészítettél egy kereskedőt (&a{amount}&7/{max}&7)."),
			"es_ES", l("&6SupremeShops >> &7Has creado un mercader (&a{amount}&7/{max}&7).")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEMERCHANTRENT = n("MSG_SUPREMESHOPS_CREATEMERCHANTRENT",
			"en_US", l("&6SupremeShops >> &7You created a rent merchant."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez créé un marchand louable."),
			"zh_TW", l("&6SupremeShops >> &7你創建了一個租借商人。"),
			"ru_RU", l("&6SupremeShops >> &7Вы создали арендуемого торговца."),
			"hu_HU", l("&6SupremeShops >> &7Elkészítettél egy bérelhető kereskedőt."),
			"es_ES", l("&6SupremeShops >> &7Has creado un mercader de alquiler.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEMERCHANTADMIN = n("MSG_SUPREMESHOPS_CREATEMERCHANTADMIN",
			"en_US", l("&6SupremeShops >> &7You created an admin merchant."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez créé un marchand admin."),
			"zh_TW", l("&6SupremeShops >> &7你創建了一個管理員商人。"),
			"ru_RU", l("&6SupremeShops >> &7Вы создали Админ-торговца."),
			"hu_HU", l("&6SupremeShops >> &7Elkészítettél egy admin kereskedőt."),
			"es_ES", l("&6SupremeShops >> &7Has creado un mercader de tipo administrador.")
			);

	public static final Text MSG_SUPREMESHOPS_UNRENTABLESHOP = n("MSG_SUPREMESHOPS_UNRENTABLESHOP",
			"en_US", l("&6SupremeShops >> &7This shop can't currently be rented."),
			"fr_FR", l("&6SupremeShops >> &7Ce shop n'est actuellement pas louable."),
			"zh_TW", l("&6SupremeShops >> &7這個商店目前不開放租用。"),
			"ru_RU", l("&6SupremeShops >> &7Этот магазин не может быть арендован."),
			"hu_HU", l("&6SupremeShops >> &7Ezt a boltot jelenleg nem tudod kibérelni."),
			"es_ES", l("&6SupremeShops >> &7Esta tienda no puede ser alquilada actualmente.")
			);

	public static final Text MSG_SUPREMESHOPS_UNRENTABLEMERCHANT = n("MSG_SUPREMESHOPS_UNRENTABLEMERCHANT",
			"en_US", l("&6SupremeShops >> &7This merchant can't currently be rented."),
			"fr_FR", l("&6SupremeShops >> &7Ce marchand n'est actuellement pas louable."),
			"zh_TW", l("&6SupremeShops >> &7這個商人目前不開放租用。"),
			"ru_RU", l("&6SupremeShops >> &7Данный торговец не может быть арендован."),
			"hu_HU", l("&6SupremeShops >> &7Ezt a kereskedőt jelenleg nem tudod kibérelni."),
			"es_ES", l("&6SupremeShops >> &7Este mercader no puede ser alquilado actualmente.")
			);

	public static final Text MSG_SUPREMESHOPS_SHOPHELP = n("MSG_SUPREMESHOPS_SHOPHELP",
			"en_US", l("&6SupremeShops >> &a{preview_click} &7to preview and trade with shop."),
			"fr_FR", l("&6SupremeShops >> &a{preview_click} &7pour prévisualiser et échanger avec le shop."),
			"zh_CN", l("&6SupremeShops >> &a{preview_click} &7to preview and trade with shop."),
			"zh_TW", l("&6SupremeShops >> &7對這個商店 &a{preview_click} &7預覽或交易。"),
			"ru_RU", l("&6SupremeShops >> &a{preview_click} &7, дабы открыть меню предпросмотра."),
			"hu_HU", l("&6SupremeShops >> &a{preview_click}&7, hogy megnézd, és kereskedj ezzel a bolttal."),
			"es_ES", l("&6SupremeShops >> &a{preview_click}&7 para ver y comerciar con la tienda.")
			);

	public static final Text MSG_SUPREMESHOPS_SHOPHELPEDIT = n("MSG_SUPREMESHOPS_SHOPHELPEDIT",
			"en_US", l("&6SupremeShops >> &a{preview_click} &7to preview and edit shop."),
			"fr_FR", l("&6SupremeShops >> &a{preview_click} &7pour prévisualiser et éditer le shop."),
			"zh_CN", l("&6SupremeShops >> &a{preview_click} &7to preview and edit shop."),
			"zh_TW", l("&6SupremeShops >> &7對這個商店 &a{preview_click} &7預覽或編輯。"),
			"ru_RU", l("&6SupremeShops >> &a{preview_click} &7, дабы открыть меню предпросмотра и редактирования."),
			"hu_HU", l("&6SupremeShops >> &a{preview_click}&7, hogy megnézd, és szerkeszd a boltot."),
			"es_ES", l("&6SupremeShops >> &a{preview_click} &7para previsualizar y editar la tienda.")
			);

	public static final Text MSG_SUPREMESHOPS_MERCHANTHELP = n("MSG_SUPREMESHOPS_MERCHANTHELP",
			"en_US", l("&6SupremeShops >> &a{preview_click} &7to preview and trade with merchant."),
			"fr_FR", l("&6SupremeShops >> &a{preview_click} &7pour prévisualiser et échanger avec le marchand."),
			"zh_CN", l("&6SupremeShops >> &a{preview_click} &7以预览并与商人进行交易."),
			"zh_TW", l("&6SupremeShops >> &7對這個商人 &a{preview_click} &7預覽和交易。"),
			"ru_RU", l("&6SupremeShops >> &a{preview_click} &7, дабы начать обмен с торговцем."),
			"hu_HU", l("&6SupremeShops >> &a{preview_click}&7, hogy megnézd, és kereskedj ezzel a kereskedővel."),
			"es_ES", l("&6SupremeShops >> &a{preview_click} &7para previsualizar y comerciar con el mercader.")
			);

	public static final Text MSG_SUPREMESHOPS_MERCHANTHELPEDIT = n("MSG_SUPREMESHOPS_MERCHANTHELPEDIT",
			"en_US", l("&6SupremeShops >> &a{preview_click} &7to preview and edit merchant."),
			"fr_FR", l("&6SupremeShops >> &a{preview_click} &7pour prévisualiser et éditer le marchand."),
			"zh_CN", l("&6SupremeShops >> &a{preview_click} &7以预览并编辑商人."),
			"zh_TW", l("&6SupremeShops >> &7對這個商人 &a{preview_click} &7預覽和編輯。"),
			"ru_RU", l("&6SupremeShops >> &a{preview_click} &7, дабы открыть меню предпросмотра и редактирования."),
			"hu_HU", l("&6SupremeShops >> &a{preview_click}&7, hogy megnézd, és szerkeszd a kereskedőt."),
			"es_ES", l("&6SupremeShops >> &a{preview_click} &7para previsualizar y editar el mercader.")
			);

	public static final Text MSG_SUPREMESHOPS_WITHDRAWVAULTMONEY = n("MSG_SUPREMESHOPS_WITHDRAWVAULTMONEY",
			"en_US", l("&6SupremeShops >> &7You withdrew &a{amount}$&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez retiré &a{amount}$&7."),
			"zh_CN", l("&6SupremeShops >> &7你取出了&a{amount}$&7."),
			"zh_TW", l("&6SupremeShops >> &7你取出了 &a{amount}$"),
			"hu_HU", l("&6SupremeShops >> &7Kivettél &a{amount}$&7 pénzt."),
			"es_ES", l("&6SupremeShops >> &7Has retirado &a{amount}$&7.")
			);

	public static final Text MSG_SUPREMESHOPS_WITHDRAWXPLEVEL = n("MSG_SUPREMESHOPS_WITHDRAWXPLEVEL",
			"en_US", l("&6SupremeShops >> &7You withdrew &a{amount} LVL&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez retiré &a{amount} LVL&7."),
			"zh_CN", l("&6SupremeShops >> &7你取出了&a{amount}级&7."),
			"zh_TW", l("&6SupremeShops >> &7你取出了經驗值 &a{amount} &7等"),
			"ru_RU", l("&6SupremeShops >> &7Вы вывели &a{amount} LVL&7."),
			"hu_HU", l("&6SupremeShops >> &7Kivettél &a{amount} szintet&7."),
			"es_ES", l("&6SupremeShops >> &7Has retirado &a{amount} nivel(es)&7.")
			);

	public static final Text MSG_SUPREMESHOPS_WITHDRAWPLAYERPOINTSPOINTS = n("MSG_SUPREMESHOPS_WITHDRAWPLAYERPOINTSPOINTS",
			"en_US", l("&6SupremeShops >> &7You withdrew &a{amount} PlayerPoints points&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez retiré &a{amount} points PlayerPoints&7."),
			"zh_CN", l("&6SupremeShops >> &7你取出了&a{amount} 点券&7."),
			"zh_TW", l("&6SupremeShops >> &7你取出了 &a{amount} &7PlayerPoints 點數"),
			"ru_RU", l("&6SupremeShops >> &7Вы вывели &a{amount} очков PlayerPoints&7."),
			"hu_HU", l("&6SupremeShops >> &7Kivettél &a{amount} PlayerPoints pontot&7."),
			"es_ES", l("&6SupremeShops >> &7Has retirado &a{amount} PlayerPoints puntos.&7.")
			);

	public static final Text MSG_SUPREMESHOPS_WITHDRAWTOKENENCHANTTOKENS = n("MSG_SUPREMESHOPS_WITHDRAWTOKENENCHANTTOKENS",
			"en_US", l("&6SupremeShops >> &7You withdrew &a{amount} TokenEnchant tokens&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez retiré &a{amount} tokens TokenEnchant&7."),
			"zh_TW", l("&6SupremeShops >> &7你取出了 &a{amount} &7TokenEnchant 代幣"),
			"ru_RU", l("&6SupremeShops >> &7Вы вывели &a{amount} TokenEnchant токенов&7."),
			"hu_HU", l("&6SupremeShops >> &7Kivettél &a{amount} TokenEnchant tokent&7."),
			"es_ES", l("&6SupremeShops >> &7Has retirado &a{amount} TokenEnchant tokens&7.")
			);

	public static final Text MSG_SUPREMESHOPS_WITHDRAWITEM = n("MSG_SUPREMESHOPS_WITHDRAWITEM",
			"en_US", l("&6SupremeShops >> &7You withdrew &a{item}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez retiré &a{item}."),
			"zh_CN", l("&6SupremeShops >> &7你取出了&a{item}&7."),
			"zh_TW", l("&6SupremeShops >> &7你取出了 &a{item}"),
			"hu_HU", l("&6SupremeShops >> &7Kivettél &a{item}&7 itemet."),
			"es_ES", l("&6SupremeShops >> &7Has retirado &a{item}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_FORCEWITHDRAWITEM = n("MSG_SUPREMESHOPS_FORCEWITHDRAWITEM",
			"en_US", l("&6SupremeShops >> &7You were forced to withdraw items from your shop, item &c{item} &7is currently dropped on the ground at &c{location}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez été forcé de retirer des items de votre shop, l'item &c{item} &7est actuellement droppés sur le sol à &c{location}&7."),
			"zh_CN", l("&6SupremeShops >> &7你被强制取出了商店的物品,物品&c{item} &7已掉落在&c{location}&7."),
			"zh_TW", l("&6SupremeShops >> &7你被強制取出了商店的物品，物品 &c{item} &7已掉落在 &c{location}"),
			"ru_RU", l("&6SupremeShops >> &7Вам пришлось вывести предметы из магазина, &c{item} &7выпал на землю тут - &c{location}&7."),
			"hu_HU", l("&6SupremeShops >> &7Kényszerítve kivetted az itemeket a boltból, &c{item} &7itemek jelenleg a földre estek a &c{location}&7 helyszínen."),
			"es_ES", l("&6SupremeShops >> &7Has sido forzado a retirar los ítems de tu tienda, el ítem &c{item}&7 será dropeado en el suelo en la localización &c{location}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RESTOCKVALUESELECTDONTHAVE = n("MSG_SUPREMESHOPS_RESTOCKVALUESELECTDONTHAVE",
			"en_US", l("&6SupremeShops >> &7You don't have any stock for that."),
			"fr_FR", l("&6SupremeShops >> &7Vous n'avez aucun stock pour cela."),
			"zh_CN", l("&6SupremeShops >> &7你没有库存."),
			"zh_TW", l("&6SupremeShops >> &7你沒有任何物品 (你->買家 的物品)"),
			"ru_RU", l("&6SupremeShops >> &7У вас недостаточно остатков для этого."),
			"hu_HU", l("&6SupremeShops >> &7Nincs ehhez raktárkészleted."),
			"es_ES", l("&6SupremeShops >> &7No tienes ningún stock para eso.")
			);

	public static final Text MSG_SUPREMESHOPS_PAYRENTVALUESELECTDONTHAVE = n("MSG_SUPREMESHOPS_PAYRENTVALUESELECTDONTHAVE",
			"en_US", l("&6SupremeShops >> &7You can't afford to pay this much."),
			"fr_FR", l("&6SupremeShops >> &7Vous ne pouvez pas vous permettre de payer autant."),
			"zh_TW", l("&6SupremeShops >> &7你無法支付如此龐大的費用。"),
			"ru_RU", l("&6SupremeShops >> &7У вас недостаточно средств."),
			"hu_HU", l("&6SupremeShops >> &7Nem engedhezed meg magadnak, hogy ilyen sokat kifizess."),
			"es_ES", l("&6SupremeShops >> &7No puedes permitirte pagar esta cantidad.")
			);

	public static final Text MSG_SUPREMESHOPS_PAYRENTVALUESELECTSTREAKLIMIT = n("MSG_SUPREMESHOPS_PAYRENTVALUESELECTSTREAKLIMIT",
			"en_US", l("&6SupremeShops >> &7You can't rent for that long."),
			"fr_FR", l("&6SupremeShops >> &7Vous ne pouvez pas louer aussi longtemps."),
			"zh_TW", l("&6SupremeShops >> &7你不能租借這麼久的時間。"),
			"ru_RU", l("&6SupremeShops >> &7Вы не можете заключить аренду на столь долгий срок."),
			"hu_HU", l("&6SupremeShops >> &7Nem tudod ennyi időre kibérelni."),
			"es_ES", l("&6SupremeShops >> &7No puedes alquilar por tanta cantidad de tiempo.")
			);

	public static final Text MSG_SUPREMESHOPS_RESTOCKVALUESELECTSTOCKLIMITREACHED = n("MSG_SUPREMESHOPS_RESTOCKVALUESELECTSTOCKLIMITREACHED",
			"en_US", l("&6SupremeShops >> &7The stock limit for this shop has been reached."),
			"fr_FR", l("&6SupremeShops >> &7La limite de stock pour ce shop a été atteinte."),
			"zh_CN", l("&6SupremeShops >> &7已达到商店库存上限."),
			"zh_TW", l("&6SupremeShops >> &7已達到商店物品上限 (你->買家 的物品)"),
			"ru_RU", l("&6SupremeShops >> &7Вы достигли ограничения по остаткам этого магазина."),
			"hu_HU", l("&6SupremeShops >> &7A bolt készlet korlátja el lett érve."),
			"es_ES", l("&6SupremeShops >> &7El límite de stock para esta tienda ha sido alcanzado.")
			);

	public static final Text MSG_SUPREMESHOPS_STOCKLIMITREACHED = n("MSG_SUPREMESHOPS_STOCKLIMITREACHED",
			"en_US", l("&6SupremeShops >> &7This is too much, the limit is &c{limit}&7."),
			"fr_FR", l("&6SupremeShops >> &7C'est trop, la limite est &c{limit}&7."),
			"zh_TW", l("&6SupremeShops >> &7這樣太多了，極限是 &c{limit}"),
			"ru_RU", l("&6SupremeShops >> &7Это слишком, лимит - &c{limit}&7."),
			"hu_HU", l("&6SupremeShops >> &7Ez túl sok. A korlát &c{limit}&7 lehet maximum."),
			"es_ES", l("&6SupremeShops >> &7Esto es demasiado, el límite es &c{limit}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RESTOCKVAULTMONEYDONTHAVE = n("MSG_SUPREMESHOPS_RESTOCKVAULTMONEYDONTHAVE",
			"en_US", l("&6SupremeShops >> &7You don't have &c{amount}$&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous n'avez pas &c{amount}$&7."),
			"zh_CN", l("&6SupremeShops >> &7你没有&c{amount}$&7."),
			"zh_TW", l("&6SupremeShops >> &7你沒有 &c{amount}$"),
			"ru_RU", l("&6SupremeShops >> &7У вас нехватает &c{amount}$&7."),
			"hu_HU", l("&6SupremeShops >> &7Nincs &c{amount}$&7 költőpénzed."),
			"es_ES", l("&6SupremeShops >> &7No tienes &c{amount}$&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RESTOCKXPLEVELDONTHAVE = n("MSG_SUPREMESHOPS_RESTOCKXPLEVELDONTHAVE",
			"en_US", l("&6SupremeShops >> &7You don't have &c{amount} LVL&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous n'avez pas &c{amount} LVL&7."),
			"zh_CN", l("&6SupremeShops >> &7你没有&c{amount}级&7."),
			"zh_TW", l("&6SupremeShops >> &7你沒有經驗值 &c{amount}等"),
			"ru_RU", l("&6SupremeShops >> &7Вам нехватает &c{amount} LVL&7."),
			"hu_HU", l("&6SupremeShops >> &7Nincs elegendő &c{amount} szinted&7."),
			"es_ES", l("&6SupremeShops >> &7No tienes &c{amount} nivel(es)&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RESTOCKPLAYERPOINTSPOINTSDONTHAVE = n("MSG_SUPREMESHOPS_RESTOCKPLAYERPOINTSPOINTSDONTHAVE",
			"en_US", l("&6SupremeShops >> &7You don't have &c{amount} PlayerPoints points&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous n'avez pas &c{amount} points PlayerPoints&7."),
			"zh_CN", l("&6SupremeShops >> &7你没有&c{amount} 点券&7."),
			"zh_TW", l("&6SupremeShops >> &7你沒有 &c{amount} &7PlayerPoints 點數"),
			"ru_RU", l("&6SupremeShops >> &7Вам нехватает &c{amount} очков PlayerPoints&7."),
			"hu_HU", l("&6SupremeShops >> &7Nincs elegendő &c{amount} PlayerPoints pontod&7."),
			"es_ES", l("&6SupremeShops >> &7No tienes &c{amount} PlayerPoints puntos&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RESTOCKTOKENENCHANTTOKENSDONTHAVE = n("MSG_SUPREMESHOPS_RESTOCKTOKENENCHANTTOKENSDONTHAVE",
			"en_US", l("&6SupremeShops >> &7You don't have &c{amount} TokenEnchant tokens&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous n'avez pas &c{amount} tokens TokenEnchant&7."),
			"zh_TW", l("&6SupremeShops >> &7你沒有 &c{amount} &7TokenEnchant 代幣"),
			"ru_RU", l("&6SupremeShops >> &7Вам нехватает &c{amount} TokenEnchant токенов&7."),
			"hu_HU", l("&6SupremeShops >> &7Nincs elegednő &c{amount} TokenEnchant tokened&7."),
			"es_ES", l("&6SupremeShops >> &7No tienes &c{amount} TokenEnchant tokens&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RESTOCKVAULTMONEY = n("MSG_SUPREMESHOPS_RESTOCKVAULTMONEY",
			"en_US", l("&6SupremeShops >> &7You restocked &a{amount}$&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez restocké &a{amount}$&7."),
			"zh_CN", l("&6SupremeShops >> &7你补充了&a{amount}$&7."),
			"zh_TW", l("&6SupremeShops >> &7你補貨了 &a{amount}$"),
			"ru_RU", l("&6SupremeShops >> &7Вы пополнили остатки на &a{amount}$&7."),
			"hu_HU", l("&6SupremeShops >> &7Feltöltöttél &a{amount}$&7 keretet."),
			"es_ES", l("&6SupremeShops >> &7Has realizado un restock de &a{amount}$&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RESTOCKXPLEVEL = n("MSG_SUPREMESHOPS_RESTOCKXPLEVEL",
			"en_US", l("&6SupremeShops >> &7You restocked &a{amount} LVL&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez restocké &a{amount} LVL&7."),
			"zh_CN", l("&6SupremeShops >> &7你补充了&a{amount}级&7."),
			"zh_TW", l("&6SupremeShops >> &7你補貨了經驗值 &a{amount}等"),
			"ru_RU", l("&6SupremeShops >> &7Вы пополнили &a{amount} LVL&7."),
			"es_ES", l("&6SupremeShops >> &7Has realizado un restock de &a{amount} nivel(es)&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RESTOCKPLAYERPOINTSPOINTS = n("MSG_SUPREMESHOPS_RESTOCKPLAYERPOINTSPOINTS",
			"en_US", l("&6SupremeShops >> &7You restocked &a{amount} PlayerPoints points&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez restocké &a{amount} points PlayerPoints&7."),
			"zh_CN", l("&6SupremeShops >> &7你补充了&a{amount} 点券&7."),
			"zh_TW", l("&6SupremeShops >> &7你補貨了 &a{amount} &7PlayerPoints 點數"),
			"ru_RU", l("&6SupremeShops >> &7Вы пополнили &a{amount} очков PlayerPoints&7."),
			"hu_HU", l("&6SupremeShops >> &7Feltöltöttél &a{amount} PlayerPoints pontot&7."),
			"es_ES", l("&6SupremeShops >> &7Has realizado un restock de &a{amount} PlayerPoints puntos&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RESTOCKTOKENENCHANTTOKENS = n("MSG_SUPREMESHOPS_RESTOCKTOKENENCHANTTOKENS",
			"en_US", l("&6SupremeShops >> &7You restocked &a{amount} TokenEnchant tokens&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez restocké &a{amount} tokens TokenEnchant&7."),
			"zh_TW", l("&6SupremeShops >> &7你補貨了 &a{amount} &7TokenEnchant 代幣"),
			"ru_RU", l("&6SupremeShops >> &7Вы пополнили &a{amount} TokenEnchant токенов&7."),
			"hu_HU", l("&6SupremeShops >> &7Feltöltöttél &a{amount} TokenEnchant tokent&7."),
			"es_ES", l("&6SupremeShops >> &7Has realizado un restock de &a{amount} TokenEnchant tokens&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RESTOCKITEMDONTHAVE = n("MSG_SUPREMESHOPS_RESTOCKITEMDONTHAVE",
			"en_US", l("&6SupremeShops >> &7You don't have &a{item}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous n'avez pas &a{item}&7."),
			"zh_CN", l("&6SupremeShops >> &7你没有&a{item}&7."),
			"zh_TW", l("&6SupremeShops >> &7你沒有 &a{item}"),
			"hu_HU", l("&6SupremeShops >> &7Nincs elgendő &a{item}&7 itemed."),
			"es_ES", l("&6SupremeShops >> &7No tienes &a{item}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RESTOCKITEM = n("MSG_SUPREMESHOPS_RESTOCKITEM",
			"en_US", l("&6SupremeShops >> &7You restocked &a{item}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez restocké &a{item}&7."),
			"zh_CN", l("&6SupremeShops >> &7You restocked &a{item}&7."),
			"zh_TW", l("&6SupremeShops >> &7你補貨了 &a{item}"),
			"ru_RU", l("&6SupremeShops >> &7Вы пополнили &a{item}&7."),
			"hu_HU", l("&6SupremeShops >> &7Feltöltöttél &a{item}&7 itemet."),
			"es_ES", l("&6SupremeShops >> &7Has realizado un restock del objeto &a{item}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CHANGESHOPDISPLAYNAMEINPUT = n("MSG_SUPREMESHOPS_CHANGESHOPDISPLAYNAMEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the shop display name in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nom d'affichage du shop dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7在聊天框内输入商店展示名，或&ccancel&7以取消."),
			"zh_TW", l("&6SupremeShops >> &7在聊天頻道內輸入商店展示名稱，或是輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите имя  магазина в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a bolt nevét a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe en el chat el nombre de tu tienda o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CHANGESHOPTRADESLIMITINPUT = n("MSG_SUPREMESHOPS_CHANGESHOPTRADESLIMITINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the maximum trades count in chat, or &c-1 &7for no limit, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nombre d'échanges maximum dans le chat, ou &c-1 &7pour aucune limite, ou &ccancel &7pour annuler."),
			"zh_TW", l("&6SupremeShops >> &7在聊天頻道內輸入最大交易數量限制，輸入 &c-1 &7為無上限，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите макс. количество обменов, либо &c-1 &7, дабы убрать лимит, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a maximális kereskedelem számát a chatba, vagy &c-1 &7a korlátlanért, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el número máximo de comercios en el chat, &c-1&7 para no tener ningún límite o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CHANGEMERCHANTTRADESLIMITINPUT = n("MSG_SUPREMESHOPS_CHANGEMERCHANTTRADESLIMITINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the maximum trades count in chat, or &c-1 &7for no limit, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nombre d'échanges maximum dans le chat, ou &c-1 &7pour aucune limite, ou &ccancel &7pour annuler."),
			"zh_TW", l("&6SupremeShops >> &7在聊天頻道內輸入最大交易數量限制，輸入 &c-1 &7為無上限，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите макс. количество обменов, либо &c-1 &7, дабы убрать лимит, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a maximális kereskedelem számát a chatba, vagy &c-1 &7a korlátlanért, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el número de comercios máximos en el chat, &c-1&7 para ilimitados o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CHANGEMERCHANTDISPLAYNAMEINPUT = n("MSG_SUPREMESHOPS_CHANGEMERCHANTDISPLAYNAMEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the merchant display name in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nom d'affichage du marchand dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入商人展示名, 或&ccancel&7以取消."),
			"zh_TW", l("&6SupremeShops >> &7在聊天頻道內輸入商人展示名稱，或是輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите имя торговца в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a kereskedő nevét a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el nombre del mercader en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CHANGEDSHOPDISPLAYNAME = n("MSG_SUPREMESHOPS_CHANGEDSHOPDISPLAYNAME",
			"en_US", l("&6SupremeShops >> &7You changed the display name of this shop to &a{name}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez modifié le nom d'affichage de ce shop pour &a{name}&7."),
			"zh_CN", l("&6SupremeShops >> &7你已将商店的展示名更改为&a{name}&7."),
			"zh_TW", l("&6SupremeShops >> &7你已將這間商店的展示名稱設置為 &a{name}。"),
			"ru_RU", l("&6SupremeShops >> &7Вы изменили имя магазина на &a{name}&7."),
			"hu_HU", l("&6SupremeShops >> &7A bolt nevét &a{name}&7 új névre cserélted."),
			"es_ES", l("&6SupremeShops >> &7Has cambiado el nombre de esta tienda a &a{name}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CHANGEDSHOPTRADESLIMIT = n("MSG_SUPREMESHOPS_CHANGEDSHOPTRADESLIMIT",
			"en_US", l("&6SupremeShops >> &7You changed the trades limit of this shop to &a{limit}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez modifié le nombre d'échanges limite de ce shop pour &a{limit}&7."),
			"zh_TW", l("&6SupremeShops >> &7你已將這間商店的交易數量限制設定為 &a{limit}。"),
			"ru_RU", l("&6SupremeShops >> &7Вы изменили лимит обменов магазина на &a{limit}&7."),
			"hu_HU", l("&6SupremeShops >> &7A bolt kereskedős korlátját &a{limit}&7 értékre állítottad."),
			"es_ES", l("&6SupremeShops >> &7Has cambiado el límite de comercios de esta tienda a &a{limit}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RENTABLECHANGEDRENTPERIODTYPE = n("MSG_SUPREMESHOPS_RENTABLECHANGEDRENTPERIODTYPE",
			"en_US", l("&6SupremeShops >> &7You changed the rent period to &a{period}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez modifié la fréquence de location pour &a{period}&7."),
			"zh_TW", l("&6SupremeShops >> &7你已將租借週期設置為 &a{period}。"),
			"ru_RU", l("&6SupremeShops >> &7Вы изменили период аренды на &a{period}&7."),
			"hu_HU", l("&6SupremeShops >> &7A bérleti időszakot &a{period}&7 időszakra váltottad."),
			"es_ES", l("&6SupremeShops >> &7Has cambiado el período de alquiler a &a{period}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RENTABLECHANGEDRENTPERIODSTREAKLIMIT = n("MSG_SUPREMESHOPS_RENTABLECHANGEDRENTPERIODSTREAKLIMIT",
			"en_US", l("&6SupremeShops >> &7You changed the rent period streak limit to &a{limit}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez modifié la limite de périodes de location consécutives pour &a{limit}&7."),
			"zh_TW", l("&6SupremeShops >> &7你已將租借次數限制設置為 &a{limit}。"),
			"ru_RU", l("&6SupremeShops >> &7Вы изменили период непрерывной аренды на &a{limit}&7."),
			"hu_HU", l("&6SupremeShops >> &7A bérleti sáv korlátot &a{limit}&7 értékre váltottad."),
			"es_ES", l("&6SupremeShops >> &7Has cambiado la cantidad de períodos de alquiler a &a{limit}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RENTABLECHANGEDRENTPERIODSTREAKLIMITDELAY = n("MSG_SUPREMESHOPS_RENTABLECHANGEDRENTPERIODSTREAKLIMITDELAY",
			"en_US", l("&6SupremeShops >> &7You changed the delay between rent period streak limit to &a{delay}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez modifié le délai entre la limite de périodes de location consécutives pour &a{delay}&7."),
			"zh_TW", l("&6SupremeShops >> &7你已將租借週期與租借次數限制之間的延遲設置為 &a{delay}。"),
			"ru_RU", l("&6SupremeShops >> &7Вы изменили задержку между окончанием периода непрерывной аренды на &a{delay}&7."),
			"hu_HU", l("&6SupremeShops >> &7Bérleti periódusok közötti határidőt &a{delay}&7 időre változtattad meg."),
			"es_ES", l("&6SupremeShops >> &7Has cambiado el tiempo entre períodos de alquiler a &a{delay}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CHANGEDMERCHANTTRADESLIMIT = n("MSG_SUPREMESHOPS_CHANGEDMERCHANTTRADESLIMIT",
			"en_US", l("&6SupremeShops >> &7You changed the trades limit of this merchant to &a{limit}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez modifié le nombre d'échanges limite de ce marchand pour &a{limit}&7."),
			"zh_TW", l("&6SupremeShops >> &7你已將這個商人的交易數量限制設定為 &a{limit}。"),
			"ru_RU", l("&6SupremeShops >> &7Вы изменили лимит обменов торговца на &a{limit}&7."),
			"hu_HU", l("&6SupremeShops >> &7Ennél a kereskedőnél a kereskedelmi korlátot &a{limit}&7 értékre váltottad."),
			"es_ES", l("&6SupremeShops >> &7Has cambiado el tiempo entre períodos de este mercader a &a{delay}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CHANGEDMERCHANTDISPLAYNAME = n("MSG_SUPREMESHOPS_CHANGEDMERCHANTDISPLAYNAME",
			"en_US", l("&6SupremeShops >> &7You changed the display name of this merchant to &a{name}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez modifié le nom d'affichage de ce marchand pour &a{name}&7."),
			"zh_CN", l("&6SupremeShops >> &7你已将商人的展示名更改为&a{name}&7."),
			"zh_TW", l("&6SupremeShops >> &7你已將這個商人的展示名稱設置為 &a{name}。"),
			"ru_RU", l("&6SupremeShops >> &7Вы изменили имя торговца на &a{name}&7."),
			"hu_HU", l("&6SupremeShops >> &7Ennek a kereskedőnek a nevét &a{name}&7 új névre változtattad."),
			"es_ES", l("&6SupremeShops >> &7Has cambiado el nombre de este mercader a &a{name}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_BLACKLISTEDPLAYERINPUT = n("MSG_SUPREMESHOPS_BLACKLISTEDPLAYERINPUT",
			"en_US", l("&6SupremeShops >> &7Word &c{word} &7isn't allowed, it's too close to &c{similar}&7."),
			"fr_FR", l("&6SupremeShops >> &7Le mot &c{word} &7n'est pas autorisé, c'est trop proche de &c{similar}&7."),
			"zh_TW", l("&6SupremeShops >> &7文字 &c{word} &7是不被允許的，它和 &c{similar} &7有些相似。"),
			"ru_RU", l("&6SupremeShops >> &7Фраза &c{word} &7недопустима, она очень похожа на &c{similar}&7."),
			"hu_HU", l("&6SupremeShops >> &7A&c{word} &7szó nem használható, mert túl közel áll a &c{similar}&7 szóhoz."),
			"es_ES", l("&6SupremeShops >> &7La palabra &c{word}&7 no está permitida, es muy parecida a &c{similar}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_RENTPERIODSTREAKLIMITINPUT = n("MSG_SUPREMESHOPS_RENTPERIODSTREAKLIMITINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the rent period streak limit in chat, or &c-1 &7for no limit, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez la limite de périodes de location consécutives dans le chat, ou &c-1 &7pour pas de limite, ou &ccancel &7pour annuler."),
			"zh_TW", l("&6SupremeShops >> &7在聊天頻道內輸入租借週期次數限制，輸入 &c-1 &7為無上限，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите лимит непрерывной аренды, либо &c-1 &7, дабы убрать лимит, либо &ccancel &7для отмены."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a bérleti periódus csak korlátját a chatba, vagy &c-1 &7a korlátlanhoz, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el conjunto de períodos de alquiler en el chat, &c-1&7 para ilimitado o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_RENTPERIODSTREAKLIMITDELAYINPUT = n("MSG_SUPREMESHOPS_RENTPERIODSTREAKLIMITDELAYINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the delay between rent period streak limit in chat (in days), or &c-1 &7for no limit, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le délai entre la limite périodes de location consécutives dans le chat (en jours), ou &c-1 &7pour pas de limite, ou &ccancel &7pour annuler."),
			"zh_TW", l("&6SupremeShops >> &7在聊天頻道內輸入租借週期次數限制間的延遲(單位: 天)，輸入 &c-1 &7為無上限，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите задержку между периодами непрерывной аренды в чат (в днях), либо &c-1 &7для снятия ограничений, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a bérleti időszakok kötötti határidőt a chatba (napokban), vagy &c-1 &7a korlátlanhoz, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el tiempo de espera entre conjuntos de alquileres en el chat (en días), &c-1&7 para ilimitado o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEITEMINPUT = n("MSG_SUPREMESHOPS_CREATEITEMINPUT",
			"en_US", l("&6SupremeShops >> &7Drop the item you want to add on the ground (with the wanted amount for this object, so if you want 32 items per trade then drop half a stack in one time)."),
			"fr_FR", l("&6SupremeShops >> &7Droppez l'item que vous voulez ajouter au sol (avec le montant désiré pour cet objet, donc si vous voulez 32 items par échange alors droppez un demi stack en un coup)."),
			"zh_CN", l("&6SupremeShops >> &7Drop the item you want to add on the ground (with the"),
			"zh_TW", l("&6SupremeShops >> &7將要在這個物件中匯入的物品包含數量一起丟出(例如: 你要匯入32個蘋果，就一次拿著32個蘋果從包包內丟出即可)"),
			"ru_RU", l("&6SupremeShops >> &7Выбросите на землю предметы, которые хотите добавить в магазин в нужном количестве за один раз)."),
			"hu_HU", l("&6SupremeShops >> &7Dobd ki azt az itemet, amit szeretnél, a földre (ehhez az objektumhoz a kívánt mennyiséggel, tehát ha 32 itemmel szeretnél kereskedni, annyit ejts a földre egyszerre.)."),
			"es_ES", l("&6SupremeShops >> &7Lanza el objeto que quieras añadir al suelo (con la cantidad que se desee para este objeto, así que si quieres 32 ítems por comercio lanza 32 ítems al suelo).")
			);

	public static final Text MSG_SUPREMESHOPS_CREATECOMMANDSPREVIEWITEMINPUT = n("MSG_SUPREMESHOPS_CREATECOMMANDSPREVIEWITEMINPUT",
			"en_US", l("&6SupremeShops >> &7Drop the preview item you want to add on the ground, it'll be used for display purposes to inform the player what he'll get."),
			"fr_FR", l("&6SupremeShops >> &7Droppez l'item de prévisualisation que vous voulez ajouter au sol, il sera utilisé pour montrer au joueur ce qu'il recevra."),
			"zh_CN", l("&6SupremeShops >> &7Drop the preview item you want to add on the ground,"),
			"zh_TW", l("&6SupremeShops >> &7將要添加的預覽物品丟出，它將用於顯示功能，以告知玩家他會在這次交易中得到什麼"),
			"ru_RU", l("&6SupremeShops >> &7Выбросите на землю предмет, дабы установить его в качестве показательного товара."),
			"hu_HU", l("&6SupremeShops >> &7Dobd a földre a kívánt előnézeti itemet. Eztek megjelenítési célokra használható, hogy tájékoztassák a játékosot, mit fog kapni."),
			"es_ES", l("&6SupremeShops >> &7Lanza el objeto de previsualización que quieras añadir al suelo, se usará para fines de previsualización para informar al jugador de qué obtendrá.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATECOMMANDSEXECUTIONSIDEINPUT = n("MSG_SUPREMESHOPS_CREATECOMMANDSEXECUTIONSIDEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the execution side in the chat (PLAYER, SERVER) or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le côté d'exécution dans le chat (PLAYER, SERVER) ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the execution side in the chat (PLAYER, SERVER)"),
			"zh_TW", l("&6SupremeShops >> &7在聊天頻道內輸入指令執行方 (PLAYER、SERVER)，輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите (PLAYER, SERVER), дабы выбрать с чьей стороны будет использоваться команда, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a végrehajtásó modot a chatba (PLAYER, SERVER) vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el lugar de ejecución en el chat (PLAYER en relación a por parte del jugador, SERVER en parte al servidor) o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATECOMMANDSINPUT = n("MSG_SUPREMESHOPS_CREATECOMMANDSINPUT",
			"en_US", l("&6SupremeShops >> &7Enter a command in the chat (without the first &a/&7), or &astop &7to stop, or &acancel &7to cancel. Currently &a{amount} &7command{plural}."),
			"fr_FR", l("&6SupremeShops >> &7Entrez une commande dans le chat (sans le premier &a/&7), ou &astop &7pour arrêter, ou &acancel &7pour annuler. Actuellement &a{amount} &7commande{plural}."),
			"zh_CN", l("&6SupremeShops >> &7Enter a command in the chat (without the first &a/&7),"),
			"zh_TW", l("&6SupremeShops >> &7在聊天頻道內輸入指令執行方 (PLAYER、SERVER)，輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите команду в чат (без &a/&7), или &astop &7, дабы остановить, либо &acancel &7, дабы отменить. Сейчас &a{amount} &7команд{plural}."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a parancsot a chatba (az első &a/&7 jel nélkül), vagy &astop&7, hogy megállítsd, vagy &acancel&7, hogy megszakítsd. Jelenleg &a{amount} &7parancs."),
			"es_ES", l("&6SupremeShops >> &7Escribe un comando en el chat (sin el primer &a/&7), &astop&7 para detenerlo o &acancel&7 para cancelar. Actualmente &a{amount} &7comando{plural}.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATECOMMANDSEMPTY = n("MSG_SUPREMESHOPS_CREATECOMMANDSEMPTY",
			"en_US", l("&6SupremeShops >> &7You didn't specify any command."),
			"fr_FR", l("&6SupremeShops >> &7Vous n'avez pas spécifié de commande."),
			"zh_CN", l("&6SupremeShops >> &7你没有指定任何指令."),
			"zh_TW", l("&6SupremeShops >> &7你沒有指定任何指令。"),
			"ru_RU", l("&6SupremeShops >> &7Вы не выбрали ни одну команду."),
			"hu_HU", l("&6SupremeShops >> &7Nem adtál meg egy parancsot sem."),
			"es_ES", l("&6SupremeShops >> &7No has especificado ningún comando.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEVAULTMONEYINVALID = n("MSG_SUPREMESHOPS_CREATEVAULTMONEYINVALID",
			"en_US", l("&6SupremeShops >> &7This money can't be added."),
			"fr_FR", l("&6SupremeShops >> &7Ce montant ne peut pas être ajouté."),
			"zh_CN", l("&6SupremeShops >> &7无法加钱."),
			"zh_TW", l("&6SupremeShops >> &7無法增加金錢。"),
			"ru_RU", l("&6SupremeShops >> &7Данная валюта не может быть добавлена."),
			"hu_HU", l("&6SupremeShops >> &7Ez a pénz nem lett hozzáadva."),
			"es_ES", l("&6SupremeShops >> &7Este dinero no puede ser añadido.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEITEMINVALID = n("MSG_SUPREMESHOPS_CREATEITEMINVALID",
			"en_US", l("&6SupremeShops >> &7This item can't be added."),
			"fr_FR", l("&6SupremeShops >> &7Cet item ne peut pas être ajouté."),
			"zh_CN", l("&6SupremeShops >> &无法添加该物品."),
			"zh_TW", l("&6SupremeShops >> &7無法添加該物品。"),
			"ru_RU", l("&6SupremeShops >> &7Данный предмет не может быть добавлен."),
			"hu_HU", l("&6SupremeShops >> &7Ezt az itemet nem lehet hozzáadni."),
			"es_ES", l("&6SupremeShops >> &7Este objeto no puede ser añadido.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEITEMINVALIDDURABILITY = n("MSG_SUPREMESHOPS_CREATEITEMINVALIDDURABILITY",
			"en_US", l("&6SupremeShops >> &7This item can't be added, it's too broken."),
			"fr_FR", l("&6SupremeShops >> &7Cet item ne peut pas être ajouté, il est en mauvais état."),
			"zh_CN", l("&6SupremeShops >> &7无法添加该物品，你需要修复这件物品."),
			"zh_TW", l("&6SupremeShops >> &7無法添加這件物品，它有點快壞了。"),
			"ru_RU", l("&6SupremeShops >> &7Данный предмет не может быть добавлен, он поломан."),
			"hu_HU", l("&6SupremeShops >> &7Nem adhatod hozzá ezt az itemet, mert nagyon meg van törve."),
			"es_ES", l("&6SupremeShops >> &7Este ítem no puede ser añadido, está demasiado dañado.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEITEMADDED = n("MSG_SUPREMESHOPS_CREATEITEMADDED",
			"en_US", l("&6SupremeShops >> &7You added an item."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté un item."),
			"zh_CN", l("&6SupremeShops >> &7你已添加一件物品."),
			"zh_TW", l("&6SupremeShops >> &7你已添加一件物品。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили предмет."),
			"hu_HU", l("&6SupremeShops >> &7Sikeresen hozzáadtál egy itemet."),
			"es_ES", l("&6SupremeShops >> &7Has añadido un objeto.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEVAULTMONEYINPUT = n("MSG_SUPREMESHOPS_CREATEVAULTMONEYINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the amount of money to add in chat (it might now be useful if you're trading items which value is defined by the server), or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le montant d'argent à ajouter dans le chat (cela pourrait ne pas être utile si vous échangez des items dont la valeur est définie par le serveur), ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the amount of money to add in chat, or &ccancel"),
			"zh_TW", l("&6SupremeShops >> &7在聊天頻道內輸入金錢數量(如果你是讓伺服器決定你的物品價值，那這個設置幾乎沒有作用)，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите в чат нужное количество денег (это может быть полезно, если вы торгуете предметами, цена которых фиксирована), либо введите &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a chatba azt a pénz-mennyiséget, amit hozzá szeretnél adni (hasznos lehet, ha olyan tételeket keresel, amelyek értékét a szerver határozza meg), vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad de dinero para añadir en el chat (podría ser útil si estás comerciando objetos cuyo valor está definido por el servidor) o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEXPLEVELSINPUT = n("MSG_SUPREMESHOPS_CREATEXPLEVELSINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the amount of XP levels to add in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nombre de niveaux d'XP à ajouter dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the amount of XP levels to add in chat, or &ccancel"),
			"zh_TW", l("&6SupremeShops >> &7在聊天頻道內輸入經驗值等級，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите количество уровней, которое необходимо добавить, либо введите &ccancel&7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a kívánt XP szint mennyiségét a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad de niveles de XP a añadir o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEPOINTSINPUT = n("MSG_SUPREMESHOPS_CREATEPOINTSINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the amount of points to add in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nombre de points à ajouter dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the amount of points to add in chat, or &ccancel"),
			"zh_TW", l("&6SupremeShops >> &7在聊天頻道內輸入PlayerPoints 點數數量，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите количество очков в чат, которое нужно добавить, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a kívánt pont összeget a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad de puntos a añadir en el chat o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATETOKENSINPUT = n("MSG_SUPREMESHOPS_CREATETOKENSINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the amount of tokens to add in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nombre de tokens à ajouter dans le chat, ou &ccancel &7pour annuler."),
			"zh_TW", l("&6SupremeShops >> &7在聊天頻道內輸入TokenEnchant 代幣數量，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите количество токенов в чат, которое нужно добавить, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a kívánt token mennyiséget a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad de tokens a añadir en el chat o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEVAULTMONEYRECOMMENDEDINPUT = n("MSG_SUPREMESHOPS_CREATEVAULTMONEYRECOMMENDEDINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the amount of money to add in chat (&arecommended : {recommended}&7), or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le montant à ajouter dans le chat (&arecommandé : {recommended}&7), ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the amount of money to add in chat (&arecommended"),
			"zh_TW", l("&6SupremeShops >> &7在聊天頻道內輸入金錢數量(&a建議: {recommended}&7)，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите количество монет в чат, которое нужно добавить (&aрекомендуется : {recommended}&7), либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a kívánt pénz mennyiséget a chatba (&aJavasolt : {recommended}&7), vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad de dinero a añadir en el chat (&arecomendado: {recommended}&7) o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEVAULTADDEDNOMONEY = n("MSG_SUPREMESHOPS_CREATEVAULTADDEDNOMONEY",
			"en_US", l("&6SupremeShops >> &7You added a money object to your shop, the amount will be calculated in real time depending on the economy."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté de l'argent à votre shop, le montant sera calculé en temps réel en fonction de l'économie."),
			"zh_CN", l("&6SupremeShops >> &7You added a money object to your shop, the amount will"),
			"zh_TW", l("&6SupremeShops >> &7你在商店內添加了金錢選項，金額為將根據經濟實際其況計算。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили деньги в ваш магазин, количество будет подсчитываться в реальном времени, исходя из состояния экономики."),
			"hu_HU", l("&6SupremeShops >> &7Pénz tárgyat adtál hozzá a boltodhoz, az összeget valós időben számolják a gazdaságtól függően."),
			"es_ES", l("&6SupremeShops >> &7Has añadido un objeto de dinero a tu tienda, la cantidad será calculada en tiempo real dependiendo de la economía.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEVAULTMONEYTOOMUCH = n("MSG_SUPREMESHOPS_CREATEVAULTMONEYTOOMUCH",
			"en_US", l("&6SupremeShops >> &7The money limit per object is &c{amount}$&7."),
			"fr_FR", l("&6SupremeShops >> &7L'argent limite par objet est &c{amount}$&7."),
			"zh_CN", l("&6SupremeShops >> &7The money limit per object is &c{amount}$&7."),
			"zh_TW", l("&6SupremeShops >> &7每一個金錢選項的金額限制為 &c{amount}$。"),
			"ru_RU", l("&6SupremeShops >> &7Лимит монет - &c{amount}$&7."),
			"hu_HU", l("&6SupremeShops >> &7A pénz korlátja &c{amount}$&7 per tárgy."),
			"es_ES", l("&6SupremeShops >> &7El límite de dinero por objeto es &c{amount}$&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEXPLEVELTOOMUCH = n("MSG_SUPREMESHOPS_CREATEXPLEVELTOOMUCH",
			"en_US", l("&6SupremeShops >> &7The XP level limit per object is &c{amount} LVL&7."),
			"fr_FR", l("&6SupremeShops >> &7Le nombre de niveaux d'XP par objet est &c{amount} LVL&7."),
			"zh_CN", l("&6SupremeShops >> &7The XP level limit per object is &c{amount} LVL&7."),
			"zh_TW", l("&6SupremeShops >> &7每一個經驗值選項的等級限制為 &c{amount} &7級。"),
			"ru_RU", l("&6SupremeShops >> &7Лимит уровней - &c{amount} LVL&7."),
			"hu_HU", l("&6SupremeShops >> &7Az XP szint korlátja &c{amount} szint&7 per tárgy."),
			"es_ES", l("&6SupremeShops >> &7El límite de XP por objeto es &c{amount} nivel&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEVAULTMONEYADDED = n("MSG_SUPREMESHOPS_CREATEVAULTMONEYADDED",
			"en_US", l("&6SupremeShops >> &7You added &a{amount}$&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté &a{amount}$&7."),
			"zh_CN", l("&6SupremeShops >> &7You added &a{amount}$&7."),
			"zh_TW", l("&6SupremeShops >> &7你添加了 &a{amount}$&7。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили &a{amount}$&7."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtál &a{amount}$&7 pénzt."),
			"es_ES", l("&6SupremeShops >> &7Has añadido &a{amount}$&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEXPLEVELSADDED = n("MSG_SUPREMESHOPS_CREATEXPLEVELSADDED",
			"en_US", l("&6SupremeShops >> &7You added &a{amount} LVL&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté &a{amount} LVL&7."),
			"zh_CN", l("&6SupremeShops >> &7你已添加&a{amount}级&7."),
			"zh_TW", l("&6SupremeShops >> &7你已添加 &a{amount} &7級。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили &a{amount} LVL&7."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtál &a{amount} szintet&7."),
			"es_ES", l("&6SupremeShops >> &7Has añadido &a{amount} niveles&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEPLAYERPOINTSPOINTSADDED = n("MSG_SUPREMESHOPS_CREATEPLAYERPOINTSPOINTSADDED",
			"en_US", l("&6SupremeShops >> &7You added &a{amount} PlayerPoints points&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté &a{amount} points PlayerPoints&7."),
			"zh_CN", l("&6SupremeShops >> &7你已添加&a{amount} 点券&7."),
			"zh_TW", l("&6SupremeShops >> &7你已添加 &a{amount} &7PlayerPoints 點數。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили &a{amount} очков PlayerPoints&7."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtál &a{amount} PlayerPoints pontot&7."),
			"es_ES", l("&6SupremeShops >> &7Has añadido &a{amount} PlayerPoints puntos&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATETOKENENCHANTTOKENSADDED = n("MSG_SUPREMESHOPS_CREATETOKENENCHANTTOKENSADDED",
			"en_US", l("&6SupremeShops >> &7You added &a{amount} TokenEnchant tokens&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté &a{amount} tokens TokenEnchant&7."),
			"zh_TW", l("&6SupremeShops >> &7你已添加 &a{amount} &7TokenEnchant 代幣&7。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили &a{amount} TokenEnchant токенов&7."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtál &a{amount} TokenEnchant tokent&7."),
			"es_ES", l("&6SupremeShops >> &7Has añadido &a{amount} TokenEnchant tokens&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATESHOPTRADECONDITIONADDED = n("MSG_SUPREMESHOPS_CREATESHOPTRADECONDITIONADDED",
			"en_US", l("&6SupremeShops >> &7You added a trade condition."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté une condition d'échange."),
			"zh_CN", l("&6SupremeShops >> &7你已添加交易条件."),
			"zh_TW", l("&6SupremeShops >> &7你已設置一個交易條件。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили условие обмена."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtad a kereskedési feltételeket."),
			"es_ES", l("&6SupremeShops >> &7Has añadido una condición de comercio.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEMERCHANTINTERACTIONCONDITIONADDED = n("MSG_SUPREMESHOPS_CREATEMERCHANTINTERACTIONCONDITIONADDED",
			"en_US", l("&6SupremeShops >> &7You added an interaction condition."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté une condition d'interaction."),
			"zh_CN", l("&6SupremeShops >> &7You added an interaction condition."),
			"zh_TW", l("&6SupremeShops >> &7你已設置一個交互條件。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили условие взаимодействия."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtad az interakciós feltételt."),
			"es_ES", l("&6SupremeShops >> &7Has añadido una condición de interacción.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATESHOPRENTCONDITIONADDED = n("MSG_SUPREMESHOPS_CREATESHOPRENTCONDITIONADDED",
			"en_US", l("&6SupremeShops >> &7You added a rent condition."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté une condition de location."),
			"zh_TW", l("&6SupremeShops >> &7你已設置一個租借條件。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили условие аренды."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtad a bérleti feltételt."),
			"es_ES", l("&6SupremeShops >> &7Has añadido una condición de alquiler.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEMERCHANTRENTCONDITIONADDED = n("MSG_SUPREMESHOPS_CREATEMERCHANTRENTCONDITIONADDED",
			"en_US", l("&6SupremeShops >> &7You added a rent condition."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté une condition de location."),
			"zh_TW", l("&6SupremeShops >> &7你已設置一個租借條件。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили условие аренды."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtad a bérleti feltételt."),
			"es_ES", l("&6SupremeShops >> &7Has añadido una condición de alquiler.")
			);

	public static final Text MSG_SUPREMESHOPS_SETCONDITIONSREQUIREDVALID = n("MSG_SUPREMESHOPS_SETCONDITIONSREQUIREDVALID",
			"en_US", l("&6SupremeShops >> &7You set the amount of required valid conditions to &a{amount}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez défini le montant de conditions valides requises à &a{amount}&7."),
			"zh_TW", l("&6SupremeShops >> &7你將所需的有效條件數量設置為 &a{amount}&7。"),
			"ru_RU", l("&6SupremeShops >> &7Вы установили количество необходимых условий на - &a{amount}&7."),
			"hu_HU", l("&6SupremeShops >> &7Beállítottad a szükséges érvényességi feltételek mennyisétét &a{amount}&7 darabra."),
			"es_ES", l("&6SupremeShops >> &7Has establecido la cantidad de condiciones válidas requeridas a &a{amount}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_SETCONDITIONSREQUIREDNOTVALID = n("MSG_SUPREMESHOPS_SETCONDITIONSREQUIREDNOTVALID",
			"en_US", l("&6SupremeShops >> &7You set the amount of required not valid conditions to &a{amount}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez défini le montant de conditions non valides requises à &a{amount}&7."),
			"zh_TW", l("&6SupremeShops >> &7你將所需的無效條件數量設置為 &a{amount}&7。"),
			"ru_RU", l("&6SupremeShops >> &7Вы установили количество отсутствующих условий на &a{amount}&7."),
			"hu_HU", l("&6SupremeShops >> &7A szükséges érvénytelen feltételek mennyiségét &a{amount}&7 darabra állítottad."),
			"es_ES", l("&6SupremeShops >> &7Has establecido la cantidad de condiciones no válidas requeridas a &a{amount}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_SETCONDITIONSGENERALERRORMESSAGE = n("MSG_SUPREMESHOPS_SETCONDITIONSGENERALERRORMESSAGE",
			"en_US", l("&6SupremeShops >> &7You set the conditions general error message to &a{message}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez défini le message d'erreur général des conditions à &a{message}&7."),
			"zh_TW", l("&6SupremeShops >> &7你將未達成所需條件時顯示的錯誤訊息設置為 &a{message}。"),
			"ru_RU", l("&6SupremeShops >> &7Вы установили основное сообщение об ошибках, связанных с условиями на - &a{message}&7."),
			"hu_HU", l("&6SupremeShops >> &7Beállítottad az általános hibaüzenetet &a{message}&7 üzenetre ennél a feltételnél."),
			"es_ES", l("&6SupremeShops >> &7Has establecido el mensaje de error de las condiciones generales a &a{message}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONERRORMESSAGEINPUT = n("MSG_SUPREMESHOPS_CONDITIONERRORMESSAGEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the condition error message in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le message d'erreur de la condition dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入条件错误消息，或输入&ccancel &7以取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入未達成所需條件時顯示的錯誤訊息，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите сообщение об ошибке, связанной с условиями в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a feltétel hibaüzenetét a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el mensaje de error de la condición o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONERRORMESSAGECHANGED = n("MSG_SUPREMESHOPS_CONDITIONERRORMESSAGECHANGED",
			"en_US", l("&6SupremeShops >> &7You set the error message for this condition to &a{message}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez défini le message d'erreur de cette condition à &a{message}&7."),
			"zh_CN", l("&6SupremeShops >> &7你已将这个条件的错误消息设置为：&a{message}&7."),
			"zh_TW", l("&6SupremeShops >> &7你已將未達成這個條件的錯誤訊息設置為: &a{message}&7。"),
			"ru_RU", l("&6SupremeShops >> &7Вы установили условие сообщения об ошибке на - &a{message}&7."),
			"hu_HU", l("&6SupremeShops >> &7Beállítottad a &a{message}&7 hibaüzenetet ennél a feltételnél."),
			"es_ES", l("&6SupremeShops >> &7Has establecido el mensaje de error para esta condicion a &a{message}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONSTARTDAYINPUT = n("MSG_SUPREMESHOPS_CONDITIONSTARTDAYINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the start day in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le jour de début dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the start day in chat, or &ccancel &7to cancel."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a開始天&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите день начала в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a kezdei napot a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el día inicial en el chat o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONENDDAYINPUT = n("MSG_SUPREMESHOPS_CONDITIONENDDAYINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the end day in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le jour de fin dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the end day in chat, or &ccancel &7to cancel."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a結束天&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите день начала в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a befejező napot a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el día final en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONSTARTTICKSINPUT = n("MSG_SUPREMESHOPS_CONDITIONSTARTTICKSINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the start ticks in chat (between 0 and 24000), or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez les ticks de début dans le chat (entre 0 et 24000), ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入开始的时刻 (0 - 24000), or &ccancel &7to cancel."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a開始的遊戲刻&f(0 ~ 24000)&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите время начала в тиках (от 0 до 24000), либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a kezdeti tick-eket a chatba (0 és 24000 között), vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el tick inicial en el chat (entre 0 y 24000) o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONENDTICKSINPUT = n("MSG_SUPREMESHOPS_CONDITIONENDTICKSINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the end ticks in chat (between 0 and 24000), or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez les ticks de fin dans le chat (entre 0 et 24000), ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the end ticks in chat (0 - 24000)，或输入&ccancel"),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a結束的遊戲刻&f(0 ~ 24000)&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите время окончания в тиках (от 0 до 24000), либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a befejező tick-eket a chatba (0 és 24000 között), vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el tick final en el chat (entre 0 y 24000) o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONMONEYINPUT = n("MSG_SUPREMESHOPS_CONDITIONMONEYINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the money amount in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le montant d'argent dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入钱数，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a金額數量&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите количество монет в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a pénz mennyiségét a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad de dinero en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONOPERATIONINPUT = n("MSG_SUPREMESHOPS_CONDITIONOPERATIONINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the operation in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez l'opération dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the operation in chat, or &ccancel &7to cancel."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a操作類型&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите действие в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a műveletet a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la operación en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONPERMISSIONINPUT = n("MSG_SUPREMESHOPS_CONDITIONPERMISSIONINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the permission in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez la permission dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入权限，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a權限碼&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите пермишен в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &Írd be a jogot a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el permiso en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONPERMISSIONDESCRIPTIONINPUT = n("MSG_SUPREMESHOPS_CONDITIONPERMISSIONDESCRIPTIONINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the condition description in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez la description de la condition dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入条件描述，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a條件描述&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите описание условия в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a feltétel leírást a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la descripción de la condición en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONWORLDINPUT = n("MSG_SUPREMESHOPS_CONDITIONWORLDINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the world in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le monde dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the world in chat，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a世界名&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите название мира в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írdd be a világot a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el mundo en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONWORLDGUARDREGIONINPUT = n("MSG_SUPREMESHOPS_CONDITIONWORLDGUARDREGIONINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the WorldGuard region id in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez l'id de région WorldGuard dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the WorldGuard region id in chat，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &aWorldGuard 保護區ID&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите ID региона в чат, либо &ccancel &7, дабы отменить"),
			"hu_HU", l("&6SupremeShops >> &7Írd be a WorldGuard régió azonosítóját a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la id de la región de WorldGuard en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONVARIABLEINPUT = n("MSG_SUPREMESHOPS_CONDITIONVARIABLEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the variable name in chat (with &a%% &7or &a{}&7), or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nom de variable dans le chat (avec &a%% &7ou &a{}&7), ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the variable name in chat (with &a%% &7or &a{}&7)，或输入&ccancel"),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a變數名稱&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите название переменной в чаит (вместе с &a%% &7или &a{}&7), либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a változó nevét a chatba (&a%% &7vagy &a{}&7 jelekkel), vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el nombre de la variable en el chat (con &a%%&7 o &a{}&7) o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONVARIABLEVALUEINPUT = n("MSG_SUPREMESHOPS_CONDITIONVARIABLEVALUEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the variable value in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez la valeur de variable dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the variable value in chat，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a變數數值&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите переменную в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a változó értékét a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el valor de la variable en el chat o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONVARIABLEDESCRIPTIONINPUT = n("MSG_SUPREMESHOPS_CONDITIONVARIABLEDESCRIPTIONINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the condition description in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez la description de la condition le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the condition description in chat，或输入&ccancel"),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a條件描述&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите описание условия в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a feltétel leírást a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la descripción de la condición en el chat o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONITEMSINPUT = n("MSG_SUPREMESHOPS_CONDITIONITEMSINPUT",
			"en_US", l("&6SupremeShops >> &7Drop the item you want to add on the ground (with the wanted amount). Type &cstop &7in the chat to stop, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Droppez l'item que vous voulez ajouter au sol (avec le montant désiré). Tapez &cstop &7dans le chat pour arrêter, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Drop the item you want to add on the ground (with the"),
			"zh_TW", l("&6SupremeShops >> &7將要在這個物件中匯入的物品包含數量一起丟出(例如: 你要匯入32個蘋果，就一次拿著32個蘋果從包包內丟出即可)。 請在聊天欄內輸入 &cstop &7停止，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Ввыбросите на землю нужный предмет (в нужном количестве) Напишите &cstop &7в чат, дабы завершить, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Dobd az itemet a földre, amit hozzá szeretnél adni (az keresett mennyiséggel). Írd be &cstop &7a chatba, hogy megállítsd, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Dropea el objeto que quieres añadir en el suelo (con la cantidad deseada). Escribe &cstop&7 en el chat para detener la acción o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONITEMSADDED = n("MSG_SUPREMESHOPS_CONDITIONITEMSADDED",
			"en_US", l("&6SupremeShops >> &7You added an item."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté un item."),
			"zh_CN", l("&6SupremeShops >> &7You added an item."),
			"zh_TW", l("&6SupremeShops >> &7你添加了一件物品。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили предмет."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtál egy itemet."),
			"es_ES", l("&6SupremeShops >> &7Has añadido un ítem.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONITEMSOPERATIONINPUT = n("MSG_SUPREMESHOPS_CONDITIONITEMSOPERATIONINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the operation in chat (HAS, HAS_NOT), or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez l'opération dans le chat (HAS, HAS_NOT), ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the operation in chat (HAS, HAS_NOT)，或输入&ccancel"),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a操作類型&f(HAS、HAS_NOT)&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите действие в чат (HAS, HAS_NOT), либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a műveletet a chatba (HAS, HAS_NOT), vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la operación en el chat (HAS, HAS_NOT) o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONITEMSNEEDEDINPUT = n("MSG_SUPREMESHOPS_CONDITIONITEMSNEEDEDINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the needed items amount, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nombre d'items requis dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the needed items amount, or &ccancel &7to cancel."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入要求的 &a物品數量&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите нужное количество, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a szükséges item mennyiséget a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad de ítems necesarios o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONXPLEVELINPUT = n("MSG_SUPREMESHOPS_CONDITIONXPLEVELINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the xp level in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le niveau d'xp dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入经验等级，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入要求的 &a經驗等級&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите нужное количество опыта в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be az XP szintet a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el nivel de XP en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONHUNGERINPUT = n("MSG_SUPREMESHOPS_CONDITIONHUNGERINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the food level in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le niveau de nourriture dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入饥饿值，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入要求的 &a飢餓度&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите уровень еды в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a kaja szintet a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el nivel de comida en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONPOINTSINPUT = n("MSG_SUPREMESHOPS_CONDITIONPOINTSINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the points amount in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nombre de points dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入点券数，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入要求的 &aPlayerPoints 點數數量&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите количество очков в чат, либо &ccancel&7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a pontok mennyiségét a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe los puntos necesarios en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONTOKENSINPUT = n("MSG_SUPREMESHOPS_CONDITIONTOKENSINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the tokens amount in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nombre de tokens dans le chat, ou &ccancel &7pour annuler."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入要求的 &aTokenEnchant 代幣數量&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите количество токенов чат, либо &ccancel&7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a tokenek mennyiségét a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad de tokens o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONHEALTHINPUT = n("MSG_SUPREMESHOPS_CONDITIONHEALTHINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the health in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez la vie le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入血量值，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入要求的 &a血量&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите количество здоровья в чат, либо &ccancel&7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be az életet a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la vida en el chat o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONBLOCKTYPEINPUT = n("MSG_SUPREMESHOPS_CONDITIONBLOCKTYPEINPUT",
			"en_US", l("&6SupremeShops >> &7Click on the wanted block type, or &csneak (in air) &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Cliquez sur le type de bloc voulu, ou &caccroupissez-vous (dans l'air) &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入想要的方块类型，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7右鍵點擊要求的 &a方塊類型&7，或&c 半蹲 &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Нажмите на желаемый тип блока, либо &cSHIFT (на пустом месте) &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Kattints a kereskett blokk típusára, vagy &cguggolj (a levegőben)&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Haz clic en el tipo de bloque deseado o &cagáchate (en el aire)&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONOBJECTSIDEINPUT = n("MSG_SUPREMESHOPS_CONDITIONOBJECTSIDEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the object side in chat (GIVING, TAKING), or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le côté de l'objet dans le chat (GIVING, TAKING), ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入object side in chat (GIVING, TAKING)，或输入&ccancel"),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a物件分類&f(GIVING、TAKING)&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите действие в чат (GIVING, TAKING), либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a tárgy oldalát a chatba (GIVING, TAKING), vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el tipo de objeto en el chat (GIVING, TAKING) o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONOBJECTTYPEINPUT = n("MSG_SUPREMESHOPS_CONDITIONOBJECTTYPEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the object type in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le type de l'objet dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入物品类型，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a物件類型&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите действие в чат, либо &ccancel&7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a tárgy típusát a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el tipo de objeto en el chat o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONOBJECTAMOUNTINPUT = n("MSG_SUPREMESHOPS_CONDITIONOBJECTAMOUNTINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the object amount in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nombre d'objets dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入物品数，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a物件數量&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите количество действий, либо &ccancel&7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a tárgy menyniségét a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad de objetos en el chat o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONSHOPAMOUNTINPUT = n("MSG_SUPREMESHOPS_CONDITIONSHOPAMOUNTINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the shop amount in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nombre de shops dans le chat, ou &ccancel &7pour annuler."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a商店數量&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите количество в чат, либо &ccancel&7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be az üzlet mennyiségét a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad de tiendas en el chat o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONCONDITIONAMOUNTINPUT = n("MSG_SUPREMESHOPS_CONDITIONCONDITIONAMOUNTINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the condition amount in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nombre de conditions dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7请在聊天框内输入条件数，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a條件數量&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите количество условий в чат, либо &ccancel&7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a feltétel mennyiségét a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad de condiciones en el chat o escribe &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONTYPEINPUT = n("MSG_SUPREMESHOPS_CONDITIONTYPEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the condition type in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le type de condition dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the condition type in chat，或输入&ccancel &7取消."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a條件類型&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите тип условия в чат, либо &ccancel&7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a feltétel típusát a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el tipo de condición en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONSHOPTYPEINPUT = n("MSG_SUPREMESHOPS_CONDITIONSHOPTYPEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the shop type in chat (BLOCK, GUI, ADMIN), or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le type de shop dans le chat (BLOCK, GUI, ADMIN), ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the shop type in chat (BLOCK, GUI, ADMIN)，或输入&ccancel"),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a商店類型&f(BLOCK、GUI、ADMIN)&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите тип магазина в чат (BLOCK, GUI, ADMIN), либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a bolt típusát a chatba (BLOCK, GUI, ADMIN), vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el tipo de tienda en el chat (BLOCK, GUI, ADMIN) o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONSTARTTIMEINPUT = n("MSG_SUPREMESHOPS_CONDITIONSTARTTIMEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the start time in the chat (HH:mm, 24h format), or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le temps de début dans le chat (HH:mm, format 24h), ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the start time in the chat (HH:mm, 24h format)，或输入&ccancel"),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a開始時間&f(HH:mm，24小時制)&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите время начала в формате (HH:mm, 24ч), либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a kezdeti időt a chatba (HH:mm, 24h órás formátum), vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la hora de inicio en el chat (HH:mm, formato de 24 horas) o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONENDTIMEINPUT = n("MSG_SUPREMESHOPS_CONDITIONENDTIMEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the end time in the chat (HH:mm, 24h format), or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le temps de fin dans le chat (HH:mm, format 24h), ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the end time in the chat (HH:mm, 24h format)，或输入&ccancel"),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a結束時間&f(HH:mm，24小時制)&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите время окончания в формате (HH:mm, 24ч), либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a befejezési időt a chatba (HH:mm, 24 órás formátum), vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la hora final en el chat (HH:mm, formato de 24 horas) o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONREQUIREDVALIDINPUT = n("MSG_SUPREMESHOPS_CONDITIONREQUIREDVALIDINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the amount of required valid conditions in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nombre de conditions valides requises dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the amount of required valid conditions in chat，或输入&ccancel"),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a有效的條件數量&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите количество выполненных условий в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a chatba az érvényes feltételek mennyiségét, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad de condiciones válidas requeridas en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONREQUIREDNOTVALIDINPUT = n("MSG_SUPREMESHOPS_CONDITIONREQUIREDNOTVALIDINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the amount of required not valid conditions in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nombre de conditions non valides requises dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the amount of required not valid conditions in"),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a無效的條件數量&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите количество невыполненных условий в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a chatba a NEM érvényes feltételek mennyiségét, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad requerida de condiciones inválidas en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_CONDITIONGENERALERRORMESSAGEINPUT = n("MSG_SUPREMESHOPS_CONDITIONGENERALERRORMESSAGEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the conditions general error message in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le message d'erreur général des conditions dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the conditions general error message in chat，或输入&ccancel"),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a未達到要求條件時顯示的錯誤訊息&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите основное сообщение об ошибке в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a feltételek általános hibaüzenetét a chatba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el mensaje de error general de condiciones en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_BLACKLISTITEMINPUT = n("MSG_SUPREMESHOPS_BLACKLISTITEMINPUT",
			"en_US", l("&6SupremeShops >> &7Drop the item you want to blacklist on the ground."),
			"fr_FR", l("&6SupremeShops >> &7Droppez l'item que vous voulez blacklister au sol."),
			"zh_CN", l("&6SupremeShops >> &7Drop the item you want to blacklist on the ground."),
			"zh_TW", l("&6SupremeShops >> &7將 &a要列入黑名單的物品 &7丟到地上。"),
			"ru_RU", l("&6SupremeShops >> &7Выбросите предмет, который хотите запретить."),
			"hu_HU", l("&6SupremeShops >> &7Dobd ki a földre azt az itemet, amit feketelistára akarsz tenni."),
			"es_ES", l("&6SupremeShops >> &7Dropea el objeto que quieras añadir a la lista negra en el suelo.")
			);

	public static final Text MSG_SUPREMESHOPS_BLACKLISTITEMINVALID = n("MSG_SUPREMESHOPS_BLACKLISTITEMINVALID",
			"en_US", l("&6SupremeShops >> &7This item is already blacklisted."),
			"fr_FR", l("&6SupremeShops >> &7Cet item est déjà blacklisté."),
			"zh_TW", l("&6SupremeShops >> &7該物品已經被禁用了。"),
			"ru_RU", l("&6SupremeShops >> &7Этот предмет уже запрещён."),
			"hu_HU", l("&6SupremeShops >> &7Ez az item már a feketelistán van."),
			"es_ES", l("&6SupremeShops >> &7Este objeto ya está en la lista negra.")
			);

	public static final Text MSG_SUPREMESHOPS_BLACKLISTITEMADDED = n("MSG_SUPREMESHOPS_BLACKLISTITEMADDED",
			"en_US", l("&6SupremeShops >> &7You blacklisted an item."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez blacklisté un item."),
			"zh_CN", l("&6SupremeShops >> &7你已允许使用该物品."),
			"zh_TW", l("&6SupremeShops >> &7你已禁用該物品。"),
			"ru_RU", l("&6SupremeShops >> &7Вы запретили предмет."),
			"hu_HU", l("&6SupremeShops >> &7Feketelistáztál egy itemet."),
			"es_ES", l("&6SupremeShops >> &7Has añadido a la lista negra un objeto.")
			);

	public static final Text MSG_SUPREMESHOPS_WHITELISTITEMINPUT = n("MSG_SUPREMESHOPS_WHITELISTITEMINPUT",
			"en_US", l("&6SupremeShops >> &7Drop the item you want to whitelist on the ground."),
			"fr_FR", l("&6SupremeShops >> &7Droppez l'item que vous voulez whitelister au sol."),
			"zh_CN", l("&6SupremeShops >> &7将你想要给予白名单的物品丢到地上."),
			"zh_TW", l("&6SupremeShops >> &7將 &a要列入白名單的物品 &7丟到地上。"),
			"ru_RU", l("&6SupremeShops >> &7Выбросите предмет, который хотите разрешить."),
			"hu_HU", l("&6SupremeShops >> &7Dobd a földre azt az itemet, amit a fehérlistára akarsz tenni."),
			"es_ES", l("&6SupremeShops >> &7Dropea en el suelo el objeto que quieras añadir a la lista blanca.")
			);

	public static final Text MSG_SUPREMESHOPS_WHITELISTITEMINVALID = n("MSG_SUPREMESHOPS_WHITELISTITEMINVALID",
			"en_US", l("&6SupremeShops >> &7This item is already whitelisted."),
			"fr_FR", l("&6SupremeShops >> &7Cet item est déjà whitelisté."),
			"zh_TW", l("&6SupremeShops >> &7該物品已經在白名單內了。"),
			"ru_RU", l("&6SupremeShops >> &7Этот предмет уже разрешён."),
			"hu_HU", l("&6SupremeShops >> &7Ez az item már a fehérlistán van."),
			"es_ES", l("&6SupremeShops >> &7Este objeto ya está en la lista blanca.")
			);

	public static final Text MSG_SUPREMESHOPS_WHITELISTITEMADDED = n("MSG_SUPREMESHOPS_WHITELISTITEMADDED",
			"en_US", l("&6SupremeShops >> &7You whitelisted an item."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez whitelisté un item."),
			"zh_CN", l("&6SupremeShops >> &7你给予了一件物品白名单."),
			"zh_TW", l("&6SupremeShops >> &7你已將這件物品新增至白名單內。"),
			"ru_RU", l("&6SupremeShops >> &7Вы разрешили предмет."),
			"hu_HU", l("&6SupremeShops >> &7Fehérlistáztál egy itemet."),
			"es_ES", l("&6SupremeShops >> &7Añadiste a la lista blanca un objeto.")
			);

	public static final Text MSG_SUPREMESHOPS_CREATEMANAGERINPUT = n("MSG_SUPREMESHOPS_CREATEMANAGERINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the name of the manager to add in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le nom du manager à ajouter dans le chat, ou &ccancel &7pour annuler."),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a商店管理者的遊戲ID&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите имя менеджера, которого нужно добавить в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a kezelő nevét a chatba, hogy hozzáadd, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el nombre del manager a añadir en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_ADMINEDITSHOPINPUT = n("MSG_SUPREMESHOPS_ADMINEDITSHOPINPUT",
			"en_US", l("&6SupremeShops >> &7Click on the shop you want to add, or close this GUI and interact with it (don't crouch during this)."),
			"fr_FR", l("&6SupremeShops >> &7Cliquez sur le shop que vous voulez ajouter, ou fermez ce GUI et interagisser avec (ne vous accroupissez pas pendant ceci)."),
			"zh_CN", l("&6SupremeShops >> &7Click on the shop you want to add, or close this GUI"),
			"zh_TW", l("&6SupremeShops >> &7右鍵點擊要添加的商店，或關閉此GUI並與之交互（在此期間不要半蹲）"),
			"ru_RU", l("&6SupremeShops >> &7Нажмите на магазин, который хотите добавить, либо взаимодействуйте с ним напрямую (не нажимайте в это время SHIFT)."),
			"hu_HU", l("&6SupremeShops >> &7Kattints a boltra, amit hozzá akarsz adni, vagy zárd be a GUI-t és az interakciót ezzel (ne guggolj az idő alatt)."),
			"es_ES", l("&6SupremeShops >> &7Haz clic en la tienda que quieras añadir o cierra esta GUI e interacciona con ella (no te agaches durante este proceso).")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADELOCKED = n("MSG_SUPREMESHOPS_PLAYERTRADELOCKED",
			"en_US", l("&6SupremeShops >> &7One of the two player is already ready, you can't modify objects."),
			"fr_FR", l("&6SupremeShops >> &7Un des deux joueurs est déjà prêt, vous ne pouvez pas modifier d'objets."),
			"zh_CN", l("&6SupremeShops >> &7One of the two player is already ready, you can't modify"),
			"zh_TW", l("&6SupremeShops >> &7兩位玩家其中之一已經準備好了，你不能更動物件。"),
			"ru_RU", l("&6SupremeShops >> &71 из 2 игроков уже готов, вы не можете вносить изменения."),
			"hu_HU", l("&6SupremeShops >> &7Két játékosból egy készen áll. Már nem módosíthatod a tételeket."),
			"es_ES", l("&6SupremeShops >> &7Uno de los dos jugadores ya está listo, no puedes modificar objetos.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADESUCCESS = n("MSG_SUPREMESHOPS_PLAYERTRADESUCCESS",
			"en_US", l("&6SupremeShops >> &7Your trade with &a{player} &7ended successfully."),
			"fr_FR", l("&6SupremeShops >> &7Votre échange avec &a{player} &7s'est déroulé avec succès."),
			"zh_CN", l("&6SupremeShops >> &7Your trade with &a{player} &7ended successfully."),
			"zh_TW", l("&6SupremeShops >> &7你與玩家 &a{player} &7的交易成功結束了。"),
			"ru_RU", l("&6SupremeShops >> &7Сделка с игроком &a{player} &7прошла успешно."),
			"hu_HU", l("&6SupremeShops >> &7A kereskedelmed &a{player} &7játékossal sikeresen véget ért."),
			"es_ES", l("&6SupremeShops >> &7Tu comercio con &a{player}&7 terminó con éxito.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADEFAIL = n("MSG_SUPREMESHOPS_PLAYERTRADEFAIL",
			"en_US", l("&6SupremeShops >> &7Your trade with &c{player} &7was cancelled."),
			"fr_FR", l("&6SupremeShops >> &7Votre échange avec &c{player} &7a été annulé."),
			"zh_CN", l("&6SupremeShops >> &7Your trade with &c{player} &7was cancelled."),
			"zh_TW", l("&6SupremeShops >> &7你與玩家 &a{player} &7的交易取消了。"),
			"ru_RU", l("&6SupremeShops >> &7Сделка с игроком &c{player} &7не состоялась."),
			"hu_HU", l("&6SupremeShops >> &7Kereskedésed &c{player} &7játékossal meg lett szakítva."),
			"es_ES", l("&6SupremeShops >> &7Tu comercio con &c{player}&7 fue cancelado.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADECLOSEDGUI = n("MSG_SUPREMESHOPS_PLAYERTRADECLOSEDGUI",
			"en_US", l("&6SupremeShops >> &7You can reopen the trade menu with &a/trade&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous pouvez réouvrir le menu d'échange avec &a/trade&7."),
			"zh_CN", l("&6SupremeShops >> &7You can reopen the trade menu with &a/trade&7."),
			"zh_TW", l("&6SupremeShops >> &7你可以透過指令 &a/trade &7重新開啟交易界面。"),
			"ru_RU", l("&6SupremeShops >> &7Вы можете открыть меню обмена, используя &a/trade&7."),
			"hu_HU", l("&6SupremeShops >> &7Újra megnyithatod a kereskedés menüt a &a/trade&7 paranccsal."),
			"es_ES", l("&6SupremeShops >> &7Puedes reabrir el menú de comercio con &a/trade&7.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADEMESSAGEINPUT = n("MSG_SUPREMESHOPS_PLAYERTRADEMESSAGEINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the message to send in the chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le message à envoyer dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the message to send in the chat, or &ccancel &7to"),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a要發送的訊息&7，或輸入&c cancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите сообщение, которое нужно отправить в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a chat-ba az üzenetet, hogy elküldhesd, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe el mensaje a enviar en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADEMESSAGE = n("MSG_SUPREMESHOPS_PLAYERTRADEMESSAGE",
			"en_US", l("&6SupremeShops >> &a{sender} &f-> &a{receiver} &f: &7{message}"),
			"zh_CN", l("&6SupremeShops >> &a{sender} &f-> &a{receiver} &f: &7{message}"),
			"zh_TW", l("&6SupremeShops >> &a{sender} &f-> &a{receiver} &f: &7{message}"),
			"ru_RU", l("&6SupremeShops >> &a{sender} &f-> &a{receiver} &f: &7{message}"),
			"hu_HU", l("&6SupremeShops >> &a{sender} &f-> &a{receiver} &f: &7{message}"),
			"es_ES", l("&6SupremeShops >> &a{sender} &f-> &a{receiver} &f: &7{message}")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADECREATEITEMINPUT = n("MSG_SUPREMESHOPS_PLAYERTRADECREATEITEMINPUT",
			"en_US", l("&6SupremeShops >> &7Drop on the ground the item you want to add."),
			"fr_FR", l("&6SupremeShops >> &7Droppez au sol l'item que vous voulez ajouter."),
			"zh_CN", l("&6SupremeShops >> &7Drop on the ground the item you want to add."),
			"zh_TW", l("&6SupremeShops >> &7將 &a要新增的物品 &7丟在地上。"),
			"ru_RU", l("&6SupremeShops >> &7Выбросите предмет, который хотите добавить."),
			"hu_HU", l("&6SupremeShops >> &7Dobd a földre az itemet, amit hozzá szeretnél adni."),
			"es_ES", l("&6SupremeShops >> &7Dropea los objetos que quieras añadir al suelo.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADECREATESHOPINPUT = n("MSG_SUPREMESHOPS_PLAYERTRADECREATESHOPINPUT",
			"en_US", l("&6SupremeShops >> &7Click on the shop you want to add, or close this GUI and interact with it (don't crouch during this)."),
			"fr_FR", l("&6SupremeShops >> &7Cliquez sur le shop que vous voulez ajouter, ou fermez ce GUI et interagissez avec (ne vous accroupissez pas pendant ceci)."),
			"zh_CN", l("&6SupremeShops >> &7Click on the shop you want to add, or close this GUI"),
			"zh_TW", l("&6SupremeShops >> &7右鍵點擊要添加的商店，或關閉此GUI並與之交互（在此期間不要半蹲）"),
			"ru_RU", l("&6SupremeShops >> &7Нажмите на магазин, который хотите добавить, либо взаимодействуйте с ним напрямую (не нажимайте в это время SHIFT)."),
			"hu_HU", l("&6SupremeShops >> &7Kattints a boltra, amit hozzá akarsz adni, vagy zárd be ezt a GUI-t és az interakciót ezzel. (ne guggolj az idő alatt)."),
			"es_ES", l("&6SupremeShops >> &7Haz clic en la tienda que quieras añadir o cierra esta GUI e interactúa con ella (no te agaches durante este proceso).")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADEADMINEDITSHOPINPUT = n("MSG_SUPREMESHOPS_PLAYERTRADEADMINEDITSHOPINPUT",
			"en_US", l("&6SupremeShops >> &7Click on the shop you want to edit, or close this GUI and interact with it (don't crouch during this)."),
			"fr_FR", l("&6SupremeShops >> &7Cliquez sur le shop que vous voulez éditer, ou fermez ce GUI et interagissez avec (ne vous accroupissez pas pendant ceci)."),
			"zh_CN", l("&6SupremeShops >> &7Click on the shop you want to edit, or close this GUI"),
			"zh_TW", l("&6SupremeShops >> &7右鍵點擊要編輯的商店，或關閉此GUI並與之交互（在此期間不要半蹲）"),
			"ru_RU", l("&6SupremeShops >> &7Нажмите на магазин, который хотите изменить, либо взаимодействуйте с ним напрямую (не нажимайте в это время SHIFT)."),
			"hu_HU", l("&6SupremeShops >> &7Kattints a boltra, amit szerkeszteni akarsz, vagy zárd be ezt a GUI-t és az interakciót ezzel. (ne guggolj az idő alatt)."),
			"es_ES", l("&6SupremeShops >> &7Haz clic en la tienda que deseas editar o cierra esta GUI e interactúa con ella (no te agaches durante este proceso).")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADECREATEMERCHANTINPUT = n("MSG_SUPREMESHOPS_PLAYERTRADECREATEMERCHANTINPUT",
			"en_US", l("&6SupremeShops >> &7Click on the merchant you want to add, or close this GUI and interact with it."),
			"fr_FR", l("&6SupremeShops >> &7Cliquez sur le marchand que vous voulez ajouter, ou fermez ce GUI et interagissez avec."),
			"zh_CN", l("&6SupremeShops >> &7Click on the merchant you want to add, or close this"),
			"zh_TW", l("&6SupremeShops >> &7右鍵點擊要添加的商人，或關閉此GUI並與之交互（在此期間不要半蹲）"),
			"ru_RU", l("&6SupremeShops >> &7Нажмите на торговца, которого хотите добавить, либо взаимодействуйте с ним напрямую (не нажимайте в это время SHIFT)."),
			"hu_HU", l("&6SupremeShops >> &7Kattints a kereskedpre, amit hozzá akarsz adni, vagy zárd be ezt a GUI-t és az interakciót ezzel."),
			"es_ES", l("&6SupremeShops >> &7Haz clic en el mercader que quieras añadir o cierra esta GUI e interactúa con él.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADEADMINEDITMERCHANTINPUT = n("MSG_SUPREMESHOPS_PLAYERTRADEADMINEDITMERCHANTINPUT",
			"en_US", l("&6SupremeShops >> &7Click on the merchant you want to edit, or close this GUI and interact with it."),
			"fr_FR", l("&6SupremeShops >> &7Cliquez sur le marchand que vous voulez éditer, ou fermez ce GUI et interagissez avec."),
			"zh_CN", l("&6SupremeShops >> &7Click on the merchant you want to edit, or close this"),
			"zh_TW", l("&6SupremeShops >> &7右鍵點擊要編輯的商人，或關閉此GUI並與之交互（在此期間不要半蹲）"),
			"ru_RU", l("&6SupremeShops >> &7Нажмите на торговца, которого хотите изменить, либо взаимодействуйте с ним напрямую (не нажимайте в это время SHIFT)."),
			"hu_HU", l("&6SupremeShops >> &7Kattints a kereskedőre, amit szerkeszteni akarsz, vagy zárd be ezt a GUI-t és az interakciót ezzel."),
			"es_ES", l("&6SupremeShops >> &7Haz clic en el mercader que quieras editar o cierra esta GUI e interactúa con él.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADECREATEITEMINVALID = n("MSG_SUPREMESHOPS_PLAYERTRADECREATEITEMINVALID",
			"en_US", l("&6SupremeShops >> &7This item can't be added."),
			"fr_FR", l("&6SupremeShops >> &7Cet item ne peut pas être ajouté."),
			"zh_CN", l("&6SupremeShops >> &7This item can't be added."),
			"zh_TW", l("&6SupremeShops >> &7這件物品不能被添加。"),
			"ru_RU", l("&6SupremeShops >> &7Этот предмет не может быть добавлен."),
			"hu_HU", l("&6SupremeShops >> &7Ezt az itemet nem adhatod hozzá."),
			"es_ES", l("&6SupremeShops >> &7Este ítem no puede ser añadido.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADECREATESHOPINVALID = n("MSG_SUPREMESHOPS_PLAYERTRADECREATESHOPINVALID",
			"en_US", l("&6SupremeShops >> &7This isn't a valid shop."),
			"fr_FR", l("&6SupremeShops >> &7Ce shop est invalide."),
			"zh_CN", l("&6SupremeShops >> &7This isn't a valid shop."),
			"zh_TW", l("&6SupremeShops >> &7這不是一間有效的商店。"),
			"ru_RU", l("&6SupremeShops >> &7Это неверный магазин."),
			"hu_HU", l("&6SupremeShops >> &7Ez nem egy érvényes bolt."),
			"es_ES", l("&6SupremeShops >> &7Esto no es una tienda válida.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADECREATEMERCHANTINVALID = n("MSG_SUPREMESHOPS_PLAYERTRADECREATEMERCHANTINVALID",
			"en_US", l("&6SupremeShops >> &7This isn't a valid merchant."),
			"fr_FR", l("&6SupremeShops >> &7Ce marchand est invalide."),
			"zh_CN", l("&6SupremeShops >> &7This isn't a valid merchant."),
			"zh_TW", l("&6SupremeShops >> &7這不是一個有效的商人。"),
			"ru_RU", l("&6SupremeShops >> &7Это неверный торговец."),
			"hu_HU", l("&6SupremeShops >> &7Ez nem egy érvényes kereskedő."),
			"es_ES", l("&6SupremeShops >> &7Este no es un mercader válido.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADECREATEITEMADDED = n("MSG_SUPREMESHOPS_PLAYERTRADECREATEITEMADDED",
			"en_US", l("&6SupremeShops >> &7You added an item."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté un item."),
			"zh_CN", l("&6SupremeShops >> &7你添加了一件物品."),
			"zh_TW", l("&6SupremeShops >> &7你添加了一件物品。"),
			"ru_RU", l("&6SupremeShops >> &7ВЫ добавили предмет."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtál egy itemet."),
			"es_ES", l("&6SupremeShops >> &7Has añadido un ítem.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADECREATECOMMANDSADDED = n("MSG_SUPREMESHOPS_PLAYERTRADECREATECOMMANDSADDED",
			"en_US", l("&6SupremeShops >> &7You added commands."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté des commandes."),
			"zh_CN", l("&6SupremeShops >> &7You added commands."),
			"zh_TW", l("&6SupremeShops >> &7你添加了指令。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили команды."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtál egy parancsot"),
			"es_ES", l("&6SupremeShops >> &7Has añadido comandos.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADECREATESHOPADDED = n("MSG_SUPREMESHOPS_PLAYERTRADECREATESHOPADDED",
			"en_US", l("&6SupremeShops >> &7You added a shop."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté un shop."),
			"zh_CN", l("&6SupremeShops >> &7你添加了一个商店."),
			"zh_TW", l("&6SupremeShops >> &7你添加了一間商店。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили магазин."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtál egy boltot"),
			"es_ES", l("&6SupremeShops >> &7Has añadido una tienda.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADECREATEMERCHANTADDED = n("MSG_SUPREMESHOPS_PLAYERTRADECREATEMERCHANTADDED",
			"en_US", l("&6SupremeShops >> &7You added a merchant."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté un marchand."),
			"zh_CN", l("&6SupremeShops >> &7你添加了一个商人."),
			"zh_TW", l("&6SupremeShops >> &7你添加了一個商人。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили торговца."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtál egy kereskedőt."),
			"es_ES", l("&6SupremeShops >> &7Has añadido un mercader.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADECREATEVAULTMONEYINPUT = n("MSG_SUPREMESHOPS_PLAYERTRADECREATEVAULTMONEYINPUT",
			"en_US", l("&6SupremeShops >> &7Enter the amount of money to add in chat, or &ccancel &7to cancel."),
			"fr_FR", l("&6SupremeShops >> &7Entrez le montant à ajouter dans le chat, ou &ccancel &7pour annuler."),
			"zh_CN", l("&6SupremeShops >> &7Enter the amount of money to add in chat, or &ccancel"),
			"zh_TW", l("&6SupremeShops >> &7請在聊天欄內輸入 &a要新增個金錢數量&7，或輸入 &ccancel &7取消。"),
			"ru_RU", l("&6SupremeShops >> &7Введите сумму в чат, либо &ccancel &7, дабы отменить."),
			"hu_HU", l("&6SupremeShops >> &7Írd be a pénz mennyiségét a chat-ba, vagy &ccancel&7, hogy megszakítsd."),
			"es_ES", l("&6SupremeShops >> &7Escribe la cantidad de dinero a añadir en el chat o &ccancel&7 para cancelar.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADECREATEVAULTADDED = n("MSG_SUPREMESHOPS_PLAYERTRADECREATEVAULTADDED",
			"en_US", l("&6SupremeShops >> &7You added &a{amount}$&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez ajouté &a{amount}$&7."),
			"zh_CN", l("&6SupremeShops >> &7You added &a{amount}$&7."),
			"zh_TW", l("&6SupremeShops >> &7你添加了 &a{amount}$&7。"),
			"ru_RU", l("&6SupremeShops >> &7Вы добавили &a{amount}$&7."),
			"hu_HU", l("&6SupremeShops >> &7Hozzáadtál &a{amount}$&7 pénzt."),
			"es_ES", l("&6SupremeShops >> &7Has añadido &a{amount}$&7.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADEREQUESTCANTSELF = n("MSG_SUPREMESHOPS_PLAYERTRADEREQUESTCANTSELF",
			"en_US", l("&6SupremeShops >> &7You can't trade with yourself, that wouldn't be very useful."),
			"fr_FR", l("&6SupremeShops >> &7Vous ne pouvez pas échanger avec vous-même, ça ne serait pas très utile."),
			"zh_CN", l("&6SupremeShops >> &7你不能与自己交易."),
			"zh_TW", l("&6SupremeShops >> &7你不能與自己交易，不覺得這很沒意義嗎?"),
			"ru_RU", l("&6SupremeShops >> &7Вы не можете торговать с самим собой."),
			"hu_HU", l("&6SupremeShops >> &7Nem kereskedhetsz magaddal. Nem lenne igazán hasznos dolog."),
			"es_ES", l("&6SupremeShops >> &7No puedes comerciar contigo mismo, eso no sería muy útil.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADEREQUEST = n("MSG_SUPREMESHOPS_PLAYERTRADEREQUEST",
			"en_US", l("&6SupremeShops >> &a{player} &7requested to trade with you. Use &a/trade &7and select him to accept. This will expire in one minute."),
			"fr_FR", l("&6SupremeShops >> &a{player} &7a demandé à échanger avec vous. Utilisez &a/trade &7et sélectionnez-le pour accepter. Ceci expirera dans une minute."),
			"zh_CN", l("&6SupremeShops >> &a{player} &7请求与你进行交易. 输入 &a/trade&7并选择这名玩家来接受交易.请求将在一分钟后过期."),
			"zh_TW", l("&6SupremeShops >> &a{player} &7請求與你交易。 使用指令 &a/trade &7並選擇這名玩家接受交易&f(交易請求將在一分鐘後過期)"),
			"ru_RU", l("&6SupremeShops >> &a{player} &7запросил обмен. Используйте &a/trade &7и выбирайте его. У вас есть 1 минута."),
			"hu_HU", l("&6SupremeShops >> &a{player} &7játékos kereskedelmi kérelmet küldött neked. Használd a &a/trade &7parancsot és válaszd ki, hogy elfogadod. Ez a kérés egy perc múlva lejár."),
			"es_ES", l("&6SupremeShops >> &a{player}&7 ha solicitado comerciar contigo. Usa &a/trade&7 y selecciónalo para aceptar. Esto expirará &cen un minuto&7.")
			);

	public static final Text MSG_SUPREMESHOPS_PLAYERTRADEREQUESTSELF = n("MSG_SUPREMESHOPS_PLAYERTRADEREQUESTSELF",
			"en_US", l("&6SupremeShops >> &7You sent a trade request to &a{player}&7. This will expire in one minute."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez envoyé une demande d'échange à &a{player}&7. Ceci expirera dans une minute."),
			"zh_CN", l("&6SupremeShops >> &7你已发送交易请求给&a{player}&7.请求将在一分钟后过期."),
			"zh_TW", l("&6SupremeShops >> &7你已發送交易請求給 &a{player}&7，若對方在一分鐘內沒有接受你的請求，請求將會取消。"),
			"ru_RU", l("&6SupremeShops >> &7Вы отправили обмен игроку &a{player}&7. У него есть минута."),
			"hu_HU", l("&6SupremeShops >> &7Kereskedelmi kérelmet küldtél &a{player}&7 játékosnak. Ez egy perc múlva lejár."),
			"es_ES", l("&6SupremeShops >> &7Has enviado una petición de comercio a &a{player}&7. Esto &cexpirará en un minuto&7.")
			);

	public static final Text MSG_SUPREMESHOPS_MANAGEABLEMAXMANAGERSREACHED = n("MSG_SUPREMESHOPS_MANAGEABLEMAXMANAGERSREACHED",
			"en_US", l("&6SupremeShops >> &7There are already are too many managers for this."),
			"fr_FR", l("&6SupremeShops >> &7Il y a déjà trop de managers pour ceci."),
			"zh_TW", l("&6SupremeShops >> &7這裡已經有太多的商店管理者了。"),
			"ru_RU", l("&6SupremeShops >> &7Слишком много менеджеров."),
			"hu_HU", l("&6SupremeShops >> &7Túl sok kezelőd van itt."),
			"es_ES", l("&6SupremeShops >> &7Hay demasiados managers para esto.")
			);

	public static final Text MSG_SUPREMESHOPS_MANAGEABLEALREADYMANAGER = n("MSG_SUPREMESHOPS_MANAGEABLEALREADYMANAGER",
			"en_US", l("&6SupremeShops >> &c{player} &7is already a manager for this."),
			"fr_FR", l("&6SupremeShops >> &c{player} &7est déjà un manager pour ceci."),
			"zh_TW", l("&6SupremeShops >> &c{player} &7已經是這裡的商店管理者了。"),
			"ru_RU", l("&6SupremeShops >> &c{player} &7уже менеджер данного предприятия."),
			"hu_HU", l("&6SupremeShops >> &c{player} &7játékos már kezelője ennek."),
			"es_ES", l("&6SupremeShops >> &c{player} &7es ya un manager para esto.")
			);

	public static final Text MSG_SUPREMESHOPS_MANAGEABLEADDMANAGER = n("MSG_SUPREMESHOPS_MANAGEABLEADDMANAGER",
			"en_US", l("&6SupremeShops >> &a{player} &7is now a manager for this."),
			"fr_FR", l("&6SupremeShops >> &a{player} &7est désormais un manager pour ceci."),
			"zh_TW", l("&6SupremeShops >> &a{player} &7現在是這裡的商店管理者了。"),
			"ru_RU", l("&6SupremeShops >> &a{player} &7стал менеджером данного предприятия."),
			"hu_HU", l("&6SupremeShops >> &a{player} &7játékos most már kezelője lett ennek."),
			"es_ES", l("&6SupremeShops >> &c{player} &7es ahora un manager de esta acción.")
			);

	public static final Text MSG_SUPREMESHOPS_SHOPCANTPAYMANAGER = n("MSG_SUPREMESHOPS_SHOPCANTPAYMANAGER",
			"en_US", l("&6SupremeShops >> &7You can't pay the wage of manager &c{player} &7for your shop &c{name}&7. You're &c{count} &7wage{plural} late."),
			"fr_FR", l("&6SupremeShops >> &7Vous ne pouvez pas payer le salaire du manager &c{player} &7pour votre shop &c{name}&7. Vous êtes à &c{count} &7salaire{plural} de retard."),
			"zh_CN", l("&6SupremeShops >> &7You can't pay the wage of manager &c{player} &7for"),
			"zh_TW", l("&6SupremeShops >> &7你不能支付你的商店 &c{name} &7的管理者 &c{player} &7的薪資。你還有 &c{count}&7筆 &7薪資尚未支付。"),
			"ru_RU", l("&6SupremeShops >> &7Вы не выплачиваете ЗП игроку &c{player} &7за магазин &c{name}&7. Вы задолжали &c{count} &7ЗП {plural}."),
			"hu_HU", l("&6SupremeShops >> &7Nem tudtad kifizetni &c{player} &7kezelői díját a &c{name}&7 boltnál. Eddig &c{count} &7béred van késésbem."),
			"es_ES", l("&6SupremeShops >> &7No puedes pagar el salario al manager &c{player}&7 por tu tienda &c{name}&7. Vas &c{count}&7 salario{plural} tarde.")
			);

	public static final Text MSG_SUPREMESHOPS_SHOPOWNERCANTPAYMANAGEMENT = n("MSG_SUPREMESHOPS_SHOPOWNERCANTPAYMANAGEMENT",
			"en_US", l("&6SupremeShops >> &c{player} &7can't pay your manager wage for his shop &c{name}&7. He's &c{count} &7wage{plural} late."),
			"fr_FR", l("&6SupremeShops >> &c{player} &7ne peut pas payer votre salaire de manager pour son shop &c{name}&7. Il est à &c{count} &7salaire{plural} de retard."),
			"zh_CN", l("&6SupremeShops >> &c{player} &7can't pay your manager wage for his shop"),
			"zh_TW", l("&6SupremeShops >> &c{player} &7無法支付你在他的商店 &c{name} &7擔任管理者的薪資。他還欠你 &c{count}&7筆 &7薪資。"),
			"ru_RU", l("&6SupremeShops >> &c{player} &7не может оплатить ваши услуги в магазине &c{name}&7. &7Он задолжал &c{count} &7ЗП {plural}"),
			"hu_HU", l("&6SupremeShops >> &c{player} &7játékos nem tudta kifizetni a kezelői bér díjad a &c{name}&7 bolthoz. Neki &a{count} &7bére van késésben."),
			"es_ES", l("&6SupremeShops >> &c{player}&7 no puede pagarte tu salario por la tienda &c{name}&7. Lleva &c{count}&7 salario{plural} atrasado.")
			);

	public static final Text MSG_SUPREMESHOPS_SHOPWAGERECEIVE = n("MSG_SUPREMESHOPS_SHOPWAGERECEIVE",
			"en_US", l("&6SupremeShops >> &a{player} &7paid your manager wage for his shop &a{name}&7."),
			"fr_FR", l("&6SupremeShops >> &a{player} &7a payé votre salaire de manager pour son shop &a{name}&7."),
			"zh_CN", l("&6SupremeShops >> &a{player} &7paid your manager wage for his shop &a{name}&7."),
			"zh_TW", l("&6SupremeShops >> &a{player} &7已支付你在他的商店 &a{name}&7擔任管理者的薪資。"),
			"ru_RU", l("&6SupremeShops >> &a{player} &7оплатил ваши услуги в магазине &a{name}&7."),
			"hu_HU", l("&6SupremeShops >> &a{player} &7játékos kifizette a kezelői bér-díjadat a &a{name}&7 bolthoz."),
			"es_ES", l("&6SupremeShops >> &a{player} &7pagó tu salario de manager por su tienda &a{name}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_SHOPLATEWAGERECEIVE = n("MSG_SUPREMESHOPS_SHOPLATEWAGERECEIVE",
			"en_US", l("&6SupremeShops >> &a{player} &7paid your late manager wage for his shop &a{name}&7. He's &a{count} &7wage{plural} late."),
			"fr_FR", l("&6SupremeShops >> &a{player} &7a payé votre salaire de manager en retard pour son shop &a{name}&7. Il est à &a{count} &7salaire{plural} de retard."),
			"zh_CN", l("&6SupremeShops >> &a{player} &7paid your late manager wage for his shop"),
			"zh_TW", l("&6SupremeShops >> &a{player} &7已支付給你在他的商店擔任管理者積欠的薪資"),
			"ru_RU", l("&6SupremeShops >> &a{player} &7выплатил задолженность по ЗП в магазине &a{name}&7. Остаток &a{count} &7ЗП {plural}"),
			"hu_HU", l("&6SupremeShops >> &a{player} &7játékos kifizette a késői-kezelői béredet a &a{name}&7 bolthoz. Neki &a{count} &7bére van késésben."),
			"es_ES", l("&6SupremeShops >> &a{player}&7 te pagó tu salario de manager por su tienda &a{name}&7. Lleva &a{count}&7 salario{plural} atrasado{plural}.")
			);

	public static final Text MSG_SUPREMESHOPS_WAGERECEIVE = n("MSG_SUPREMESHOPS_WAGERECEIVE",
			"en_US", l("&6SupremeShops >> &a{player} &7paid your manager wage for his merchant &a{name}&7."),
			"fr_FR", l("&6SupremeShops >> &a{player} &7a payé votre salaire de manager pour son marchand &a{name}&7."),
			"zh_CN", l("&6SupremeShops >> &a{player} &7paid your manager wage for his merchant &a{name}&7."),
			"zh_TW", l("&6SupremeShops >> &a{player} &7已支付你在他的商人 &a{name}&7擔任管理者的薪資。"),
			"ru_RU", l("&6SupremeShops >> &a{player} &7оплатил ваши услуги за торговца &a{name}&7."),
			"hu_HU", l("&6SupremeShops >> &a{player} &7játékos kifizette a kezelői bér-díjadat a &a{name}&7 kereskedőhöz."),
			"es_ES", l("&6SupremeShops >> &a{player} &7te pagó tu salario de manager por su mercader &a{name}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_MERCHANTCANTPAYMANAGER = n("MSG_SUPREMESHOPS_MERCHANTCANTPAYMANAGER",
			"en_US", l("&6SupremeShops >> &7You can't pay the wage of manager &c{player} &7for your merchant &c{name}&7. You're &c{count} &7wage{plural} late."),
			"fr_FR", l("&6SupremeShops >> &7Vous ne pouvez pas payer le salaire du manager &c{player} &7pour votre marchand &c{name}&7. Vous êtes à &c{count} &7salaire{plural} de retard."),
			"zh_CN", l("&6SupremeShops >> &7You can't pay the wage of manager &c{player} &7for"),
			"zh_TW", l("&6SupremeShops >> &7你不能支付你的商人 &c{name} &7的管理者 &c{player} &7的薪資。你還有 &c{count}&7筆 &7薪資尚未支付。"),
			"ru_RU", l("&6SupremeShops >> &7Вы не выплачиваете ЗП игроку &c{player} &7за торговца &c{name}&7. Вы задолжали &c{count} &7ЗП {plural}."),
			"hu_HU", l("&6SupremeShops >> &7Nem tudtad kifizetni &c{player} &7kezelői díját a &c{name}&7 kereskedődnél. Eddig &c{count} &7béred van késésbem."),
			"es_ES", l("&6SupremeShops >> &7No puedes pagar el salario al manager &c{player}&7 por tu mercader &c{name}&7. Llevas &c{count}&7 salario{plural} atrasado{plural}.")
			);

	public static final Text MSG_SUPREMESHOPS_MERCHANTOWNERCANTPAYMANAGEMENT = n("MSG_SUPREMESHOPS_MERCHANTOWNERCANTPAYMANAGEMENT",
			"en_US", l("&6SupremeShops >> &c{player} &7can't pay your manager wage for his merchant &c{name}&7. He's &c{count} &7wage{plural} late."),
			"fr_FR", l("&6SupremeShops >> &c{player} &7ne peut pas payer votre salaire de manager pour son marchand &c{name}&7. Il est à &c{count} &7salaire{plural} de retard."),
			"zh_CN", l("&6SupremeShops >> &c{player} &7can't pay your manager wage for his merchant"),
			"zh_TW", l("&6SupremeShops >> &c{player} &7無法支付你在他的商人 &c{name} &7擔任管理者的薪資。他還欠你 &c{count}&7筆 &7薪資。"),
			"ru_RU", l("&6SupremeShops >> &c{player} &7не может оплатить ваши услуги у торговца &c{name}&7. &7Он задолжал &c{count} &7ЗП {plural}"),
			"hu_HU", l("&6SupremeShops >> &c{player} &7játékos nem tudta kifizetni a kezelői bér díjad a &c{name}&7 kereskedőhöz. Neki &a{count} &7bére van késésben."),
			"es_ES", l("&6SupremeShops >> &c{player}&7 no puede pagarte tu salario de manager por su mercader &c{name}&7. Lleva &c{count}&7 salario{plural} atrasado{plural}.")
			);

	public static final Text MSG_SUPREMESHOPS_MERCHANTLATEWAGERECEIVE = n("MSG_SUPREMESHOPS_MERCHANTLATEWAGERECEIVE",
			"en_US", l("&6SupremeShops >> &a{player} &7paid your late manager wage for his merchant &a{name}&7. He's &a{count} &7wage{plural} late."),
			"fr_FR", l("&6SupremeShops >> &a{player} &7a payé votre salaire de manager en retard pour son marchand &a{name}&7. Il est à &a{count} &7salaire{plural} de retard."),
			"zh_CN", l("&6SupremeShops >> &a{player} &7paid your late manager wage for his merchant"),
			"zh_TW", l("&6SupremeShops >> &a{player} &7已支付給你在他的商人擔任管理者積欠的薪資"),
			"ru_RU", l("&6SupremeShops >> &a{player} &7оплатил ваши услуги за торговца &a{name}&7."),
			"hu_HU", l("&6SupremeShops >> &a{player} &7játékos kifizette a késői-kezelői béredet a &a{name}&7 kereskedőhöz. Neki &a{count} &7bére van késésben."),
			"es_ES", l("&6SupremeShops >> &a{player}&7 te pagó tu salario de manager por su mercader &c{name}&7. Lleva &c{count}&7 salario{plural} atrasado{plural}.")
			);

	public static final Text MSG_SUPREMESHOPS_OBJECTTAKE_ITEM = n("MSG_SUPREMESHOPS_OBJECTTAKE_ITEM",
			"en_US", l("&6SupremeShops >> &7You gave &c{item}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez donné &c{item}&7."),
			"zh_CN", l("&6SupremeShops >> &7你给予了&c{item}&7."),
			"zh_TW", l("&6SupremeShops >> &7你給予了 &c{item}&7。"),
			"ru_RU", l("&6SupremeShops >> &7Вы отдали &c{item}&7."),
			"hu_HU", l("&6SupremeShops >> &7Adtál &c{item}&7."),
			"es_ES", l("&6SupremeShops >> &7Diste &c{item}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_OBJECTGIVE_ITEM = n("MSG_SUPREMESHOPS_OBJECTGIVE_ITEM",
			"en_US", l("&6SupremeShops >> &7You received &a{item}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez reçu &a{item}&7."),
			"zh_CN", l("&6SupremeShops >> &7你获得了&a{item}&7."),
			"zh_TW", l("&6SupremeShops >> &7你獲得了 &a{item}&7。"),
			"ru_RU", l("&6SupremeShops >> &7Вы получили &a{item}&7."),
			"hu_HU", l("&6SupremeShops >> &7Kaptál &a{item}&7."),
			"es_ES", l("&6SupremeShops >> &7Recibiste &a{item}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_OBJECTTAKE_VAULTMONEY = n("MSG_SUPREMESHOPS_OBJECTTAKE_VAULTMONEY",
			"en_US", l("&6SupremeShops >> &7You gave &c{money}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez donné &c{money}&7."),
			"zh_CN", l("&6SupremeShops >> &7你给予了&c{money}&7."),
			"zh_TW", l("&6SupremeShops >> &7你支付了 &a{money}&7。"),
			"ru_RU", l("&6SupremeShops >> &7Вы потратили &c{money}&7."),
			"hu_HU", l("&6SupremeShops >> &7Adtál &c{money}&7."),
			"es_ES", l("&6SupremeShops >> &7Diste &c{money}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_OBJECTTAKE_XPLEVEL = n("MSG_SUPREMESHOPS_OBJECTTAKE_XPLEVEL",
			"en_US", l("&6SupremeShops >> &7You gave &c{amount} LVL&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez donné &c{amount} LVL&7."),
			"zh_CN", l("&6SupremeShops >> &7你给予了&c{amount}级&7."),
			"zh_TW", l("&6SupremeShops >> &7你給予了 &c{amount} &7級。"),
			"ru_RU", l("&6SupremeShops >> &7Вы отдали &c{amount} LVL&7."),
			"hu_HU", l("&6SupremeShops >> &7Adtál &c{amount} szintet&7."),
			"es_ES", l("&6SupremeShops >> &7Diste &c{amount} nivel(es)&7.")
			);

	public static final Text MSG_SUPREMESHOPS_OBJECTTAKE_PLAYERPOINTSPOINTS = n("MSG_SUPREMESHOPS_OBJECTTAKE_PLAYERPOINTSPOINTS",
			"en_US", l("&6SupremeShops >> &7You gave &c{amount} PlayerPoints points&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez donné &c{amount} points PlayerPoints&7."),
			"zh_CN", l("&6SupremeShops >> &7你给予了&c{amount} 点券&7."),
			"zh_TW", l("&6SupremeShops >> &7你給予了 &c{amount} &7PlyaerPoints 點數。"),
			"ru_RU", l("&6SupremeShops >> &7Вы потратили &c{amount} очков PlayerPoints&7."),
			"hu_HU", l("&6SupremeShops >> &7Adtál &c{amount} PlayerPoints pontot&7."),
			"es_ES", l("&6SupremeShops >> &7Diste &c{amount} PlayerPoints puntos&7.")
			);

	public static final Text MSG_SUPREMESHOPS_OBJECTTAKE_TOKENENCHANTTOKENS = n("MSG_SUPREMESHOPS_OBJECTTAKE_TOKENENCHANTTOKENS",
			"en_US", l("&6SupremeShops >> &7You gave &c{amount} TokenEnchant tokens&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez donné &c{amount} tokens TokenEnchant&7."),
			"zh_TW", l("&6SupremeShops >> &7你給予了 &c{amount} &7TokenEnchant 代幣。"),
			"ru_RU", l("&6SupremeShops >> &7Вы потратили &c{amount} TokenEnchant токенов&7."),
			"hu_HU", l("&6SupremeShops >> &7Adtál &c{amount} TokenEnchant tokent&7."),
			"es_ES", l("&6SupremeShops >> &7Has dado &c{amount} TokenEnchant tokens&7.")
			);

	public static final Text MSG_SUPREMESHOPS_OBJECTGIVE_VAULTMONEY = n("MSG_SUPREMESHOPS_OBJECTGIVE_VAULTMONEY",
			"en_US", l("&6SupremeShops >> &7You received &a{money}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez reçu &a{money}&7."),
			"zh_CN", l("&6SupremeShops >> &7你获得了&a{money}&7."),
			"zh_TW", l("&6SupremeShops >> &7你獲得了 &a{money}&7。"),
			"ru_RU", l("&6SupremeShops >> &7Вы получили &a{money}&7."),
			"hu_HU", l("&6SupremeShops >> &7Kaptál &a{money}&7."),
			"es_ES", l("&6SupremeShops >> &7Has recibido &a{money}&7.")
			);

	public static final Text MSG_SUPREMESHOPS_OBJECTGIVE_XPLEVEL = n("MSG_SUPREMESHOPS_OBJECTGIVE_XPLEVEL",
			"en_US", l("&6SupremeShops >> &7You received &a{amount}&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez reçu &a{amount}&7."),
			"zh_CN", l("&6SupremeShops >> &7你获得了&a{amount}&7."),
			"zh_TW", l("&6SupremeShops >> &7你獲得了 &a{amount}&7級。"),
			"ru_RU", l("&6SupremeShops >> &7Вы получили &a{amount} LVL&7."),
			"hu_HU", l("&6SupremeShops >> &7Kaptál &a{amount}&7."),
			"es_ES", l("&6SupremeShops >> &7Has recibido &a{amount} nivel(es)&7.")
			);

	public static final Text MSG_SUPREMESHOPS_OBJECTGIVE_PLAYERPOINTSPOINTS = n("MSG_SUPREMESHOPS_OBJECTGIVE_PLAYERPOINTSPOINTS",
			"en_US", l("&6SupremeShops >> &7You received &a{amount} PlayerPoints points&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez reçu &a{amount} points PlayerPoints&7."),
			"zh_CN", l("&6SupremeShops >> &7你获得了&a{amount}点 PlayerPoints points&7."),
			"zh_TW", l("&6SupremeShops >> &7你獲得了 &a{amount}&7點 PlayerPoints 點數。"),
			"ru_RU", l("&6SupremeShops >> &7Вы получили &a{amount} очков PlayerPoints&7."),
			"hu_HU", l("&6SupremeShops >> &7Kaptál &a{amount} PlayerPoints pontot&7."),
			"es_ES", l("&6SupremeShops >> &7Has recibido &a{amount} PlayerPoints puntos&7.")
			);

	public static final Text MSG_SUPREMESHOPS_OBJECTGIVE_TOKENENCHANTTOKENS = n("MSG_SUPREMESHOPS_OBJECTGIVE_TOKENENCHANTTOKENS",
			"en_US", l("&6SupremeShops >> &7You received &a{amount} TokenEnchant tokens&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez reçu &a{amount} tokens TokenEnchant&7."),
			"zh_TW", l("&6SupremeShops >> &7你獲得了 &a{amount} &7TokenEnchant 代幣。"),
			"ru_RU", l("&6SupremeShops >> &7Вы получили &a{amount} TokenEnchant токенов&7."),
			"hu_HU", l("&6SupremeShops >> &7Kaptál &a{amount} TokenEnchant tokent&7."),
			"es_ES", l("&6SupremeShops >> &7Has recibido &a{amount} TokenEnchant tokens&7.")
			);

	public static final Text MSG_SUPREMESHOPS_TRADEASMUCHASPOSSIBLEBENEFIT = n("MSG_SUPREMESHOPS_TRADEASMUCHASPOSSIBLEBENEFIT",
			"en_US", l("&6SupremeShops >> &7You sold items for &a{money}$&7."),
			"fr_FR", l("&6SupremeShops >> &7Vous avez vendu des items pour &a{money}$&7."),
			"zh_CN", l("&6SupremeShops >> &7你出售了物品并获得了&a{money}$&7."),
			"zh_TW", l("&6SupremeShops >> &7你出售物品並獲得了 &a{money}$&7。"),
			"ru_RU", l("&6SupremeShops >> &7Вы продали предметы за &a{money}$&7."),
			"hu_HU", l("&6SupremeShops >> &7Eladott elemekért &a{money}$&7 kaptál."),
			"es_ES", l("&6SupremeShops >> &7Has vendido objetos por &a{money}$&7.")
			);

	public static final Text MSG_SUPREMESHOPS_TRADEASMUCHASPOSSIBLENONE = n("MSG_SUPREMESHOPS_TRADEASMUCHASPOSSIBLENONE",
			"en_US", l("&6SupremeShops >> &7You can't sell anything here."),
			"fr_FR", l("&6SupremeShops >> &7Vous ne pouvez rien vendre ici."),
			"zh_CN", l("&6SupremeShops >> &7你不能在这里出售物品."),
			"zh_TW", l("&6SupremeShops >> &7你不能在這裡出售任何物品。"),
			"ru_RU", l("&6SupremeShops >> &7Вам здесь нечего продать."),
			"hu_HU", l("&6SupremeShops >> &7Nem adhatod el ezeket itt."),
			"es_ES", l("&6SupremeShops >> &7No puedes vender nada aquí.")
			);

	public static final Text MSG_SUPREMESHOPS_NOTIFYUNBALANCEDLOOSINGMONEY = n("MSG_SUPREMESHOPS_NOTIFYUNBALANCEDLOOSINGMONEY",
			"en_US", l("&6SupremeShops >> &cWarning : the trade is unbalanced : you're loosing {money}$ compared to the real value of items (only items vs money were calculated)."),
			"fr_FR", l("&6SupremeShops >> &cAttention : l'échange est déséquilibré : vous perdez {money}$ par rapport à la valeur réelle des items (seulement les items contre l'argent ont été calculés)."),
			"zh_CN", l("&6SupremeShops >> &c警告: 交易不平衡 : 相较于物品的实际价值，你亏了{money}$(只计算 物品 vs 金钱)."),
			"zh_TW", l("&6SupremeShops >> &c警告: &6這筆交易是不平衡的，和實際物價比起來在這個物品上你會額外損失 &c{money}$ &f(只計算 物品 vs 金錢)"),
			"ru_RU", l("&6SupremeShops >> &cВнимание : обмен не равнозначен : вы потеряете {money}$ , считая среднюю цену предметов (предметы vs деньги)."),
			"hu_HU", l("&6SupremeShops >> &cFigyelem: a kereskedés kiegyensúlyozatlan: nagyjából {money}$ pénzt veszítesz a valódi értékhez képest. (Csak az Item vs Pénz lett kikalkulálva)."),
			"es_ES", l("&6SupremeShops >> &cAviso : El intercambio está desbalanceado: estás perdiendo {money}$ comparado con el valor real de los objetos (solo objetos vs. ítems están siendo calculados.")
			);

	public static final Text MSG_SUPREMESHOPS_NOTIFYUNBALANCEDGAININGMONEY = n("MSG_SUPREMESHOPS_NOTIFYUNBALANCEDGAININGMONEY",
			"en_US", l("&6SupremeShops >> &aWarning : the trade is unbalanced : you're gaining {money}$ compared to the real value of items (only items vs money were calculated)."),
			"fr_FR", l("&6SupremeShops >> &aAttention : l'échange est déséquilibré : vous gagnez {money}$ par rapport à la valeur réelle des items (seulement les items contre l'argent ont été calculés)."),
			"zh_CN", l("&6SupremeShops >> &a警告: 交易不平衡 : 相较于物品的实际价值，你赚了{money}$(只计算 物品 vs 金钱)."),
			"zh_TW", l("&6SupremeShops >> &c警告: &6這筆交易是不平衡的，和實際物價比起來在這個物品上你會額外賺取 &a{money}$ &f(只計算 物品 vs 金錢)"),
			"ru_RU", l("&6SupremeShops >> &cВнимание : обмен не равнозначен : вы выйграете {money}$ , считая среднюю цену предметов (предметы vs деньги)."),
			"hu_HU", l("&6SupremeShops >> &cFigyelem: a kereskedés kiegyensúlyozatlan: nagyjából {money}$ pénzt veszítesz a valódi értékhez képest. (Csak az Item vs Pénz lett kikalkulálva)."),
			"es_ES", l("&6SupremeShops >> &7Atención : El intercambio está desbalanceado : estás ganando {money}$ en comparación al valor real de los objetos (solo ítems vs. objetos están siendo calculados).")
			);

}
