package com.guillaumevdn.supremeshops.editor;

import java.io.File;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ConfigData;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorItem;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorSelectionGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.ModifCallback;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.Wrapper;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.adminshop.AdminShopData;
import com.guillaumevdn.supremeshops.module.gui.SSGui;
import com.guillaumevdn.supremeshops.module.modifier.Modifier;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;
import com.guillaumevdn.supremeshops.module.trigger.type.AdminShopTriggerEntity;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;

public class EditorMainGUI extends EditorGUI {

	// base
	public EditorMainGUI() {
		super(SupremeShops.inst(), null, "SupremeShops editor", 9, GUI.SLOTS_0_TO_8);
	}

	// methods
	@Override
	protected void fill() {
		// admin shops
		setRegularItem(new EditorItem("admin_shops", 0, EditorGUI.ICON_TECHNICAL, SSLocaleGUI.GUI_SUPREMESHOPS_EDITORADMINSHOPS.getLine(), SSLocaleGUI.GUI_SUPREMESHOPS_EDITORADMINSHOPSLORE.getLines()) {
			@Override
			protected void onClick(final Player player, ClickType clickType, int pageIndex) {
				// sub
				EditorSelectionGUI sub = new EditorSelectionGUI<AdminShopData>(SupremeShops.inst(), EditorMainGUI.this, "Admin shops") {
					@Override
					protected void fill() {
						super.fill();
						// add values
						int slot = -1;
						for (AdminShopData shop : Utils.asSortedList(SupremeShops.inst().getModuleManager().getAdminShops().values())) {
							addItem(new Wrapper<>(shop), EditorGUI.ICON_TECHNICAL, shop.getId(), "§6" + shop.getId(), ++slot);
						}
					}
					@Override
					protected boolean doesElementExist(String id) {
						return SupremeShops.inst().getModuleManager().getAdminShop(id) != null;
					}
					@Override
					protected void onElementClick(Wrapper<AdminShopData> element, Player player, ClickType clickType, int pageIndex) {
						element.getValue().createEditor(this, player, new ModifCallback<AdminShopData>(element, AdminShopData.class) {
							@Override
							protected void onModif(AdminShopData instance, EditorGUI from, Player player) {
								SupremeShops.inst().getModuleManager().getAdminShops().put(instance.getId().toLowerCase(), instance);// replace instance
								YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), instance.getFile(), null, true, true);
								instance.save(new ConfigData(SupremeShops.inst(), "admin shop " + instance.getId(), config, ""));
								config.save();
							}
						}).open(player);
					}
					@Override
					protected void createElement(String id) {
						AdminShopData shop = new AdminShopData(id, new File(SupremeShops.inst().getDataFolder() + "/shops/admin/shops/" + id + ".yml"), null, false, -1, null, null);
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), shop.getFile(), null, true, true);
						shop.save(new ConfigData(SupremeShops.inst(), "admin shop " + shop.getId(), config, ""));
						config.save();
						SupremeShops.inst().getModuleManager().getAdminShops().put(id, shop);
					}
					@Override
					protected void deleteElement(Wrapper<AdminShopData> element) {
						SupremeShops.inst().getModuleManager().getAdminShops().remove(element.getValue().getId().toLowerCase());
						if (element.getValue().getFile().exists()) {
							File newFile = new File(SupremeShops.inst().getDataFolder() + "/deleted/shops/admin/shops/" + element.getValue().getId() + "_" + UUID.randomUUID().toString().split("-")[0] + ".yml");
							newFile.getParentFile().mkdirs();
							if (!element.getValue().getFile().renameTo(newFile)) {
								if (!element.getValue().getFile().delete()) {
									element.getValue().getFile().deleteOnExit();
								}
							}
						}
					}
				};
				// open sub
				sub.open(player);
			}
		});
		// triggers
		setRegularItem(new EditorItem("triggers", 1, EditorGUI.ICON_TECHNICAL, SSLocaleGUI.GUI_SUPREMESHOPS_EDITORTRIGGERS.getLine(), SSLocaleGUI.GUI_SUPREMESHOPS_EDITORTRIGGERSLORE.getLines()) {
			@Override
			protected void onClick(final Player player, ClickType clickType, int pageIndex) {
				// sub
				EditorSelectionGUI sub = new EditorSelectionGUI<Trigger>(SupremeShops.inst(), EditorMainGUI.this, "Triggers") {
					@Override
					protected void fill() {
						super.fill();
						// add values
						int slot = -1;
						for (Trigger trigger : Utils.asSortedList(SupremeShops.inst().getModuleManager().getTriggers().values())) {
							addItem(new Wrapper<>(trigger), EditorGUI.ICON_TRIGGER, trigger.getId(), "§6" + trigger.getId(), ++slot);
						}
					}
					@Override
					protected boolean doesElementExist(String id) {
						return SupremeShops.inst().getModuleManager().getTrigger(id) != null;
					}
					@Override
					protected void onElementClick(Wrapper<Trigger> element, Player player, ClickType clickType, int pageIndex) {
						element.getValue().createEditor(this, player, new ModifCallback<Trigger>(element, Trigger.class) {
							@Override
							protected void onModif(Trigger instance, EditorGUI from, Player player) {
								SupremeShops.inst().getModuleManager().getTriggers().put(instance.getId().toLowerCase(), instance);// replace instance
								YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/triggers.yml"), "triggers.yml", true, true);
								instance.save(new ConfigData(SupremeShops.inst(), "trigger " + instance.getId(), config, "triggers." + instance.getId()));
								config.save();
							}
						}).open(player);
					}
					@Override
					protected void createElement(String id) {
						Trigger element = new AdminShopTriggerEntity(id, null, false, -1, EditorGUI.ICON_TRIGGER, null);
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/triggers.yml"), "triggers.yml", true, true);
						element.save(new ConfigData(SupremeShops.inst(), "trigger " + element.getId(), config, "triggers." + element.getId()));
						config.save();
						SupremeShops.inst().getModuleManager().getTriggers().put(id, element);
					}
					@Override
					protected void deleteElement(Wrapper<Trigger> element) {
						SupremeShops.inst().getModuleManager().getTriggers().remove(element.getValue().getId().toLowerCase());
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/triggers.yml"), "triggers.yml", true, true);
						config.set(element.getValue().getId(), null);
						config.save();
					}
				};
				// open sub
				sub.open(player);
			}
		});
		// guis
		setRegularItem(new EditorItem("guis", 2, Mat.CHEST, SSLocaleGUI.GUI_SUPREMESHOPS_EDITORGUIS.getLine(), SSLocaleGUI.GUI_SUPREMESHOPS_EDITORGUISLORE.getLines()) {
			@Override
			protected void onClick(final Player player, ClickType clickType, int pageIndex) {
				// sub
				EditorSelectionGUI sub = new EditorSelectionGUI<SSGui>(SupremeShops.inst(), EditorMainGUI.this, "GUIs") {
					@Override
					protected void fill() {
						super.fill();
						// add values
						int slot = -1;
						for (SSGui gui : Utils.asSortedList(SupremeShops.inst().getModuleManager().getGuis().values())) {
							String name = gui.getName(player);
							addItem(new Wrapper<>(gui), Mat.CHEST, gui.getId(), "§6" + gui.getId() + (name != null ? " (" + name + ")" : ""), ++slot);
						}
					}
					@Override
					protected boolean doesElementExist(String id) {
						return SupremeShops.inst().getModuleManager().getGui(id) != null;
					}
					@Override
					protected void onElementClick(Wrapper<SSGui> element, Player player, ClickType clickType, int pageIndex) {
						element.getValue().createEditor(this, player, new ModifCallback<SSGui>(element, SSGui.class) {
							@Override
							protected void onModif(SSGui instance, EditorGUI from, Player player) {
								YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), instance.getFile(), null, true, true);
								instance.save(new ConfigData(SupremeShops.inst(), "gui " + instance.getId(), config, ""));
								config.save();
							}
						}).open(player);
					}
					@Override
					protected void createElement(String id) {
						SSGui gui = new SSGui(id, new File(SupremeShops.inst().getDataFolder() + "/guis/" + id + ".yml"), null, false, -1, null, null);
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), gui.getFile(), null, true, true);
						gui.save(new ConfigData(SupremeShops.inst(), "gui " + gui.getId(), config, ""));
						config.save();
						SupremeShops.inst().getModuleManager().getGuis().put(id, gui);
					}
					@Override
					protected void deleteElement(Wrapper<SSGui> element) {
						SupremeShops.inst().getModuleManager().getGuis().remove(element.getValue().getId().toLowerCase());
						if (element.getValue().getFile().exists()) {
							File newFile = new File(SupremeShops.inst().getDataFolder() + "/deleted/guis/" + element.getValue().getId() + "_" + UUID.randomUUID().toString().split("-")[0] + ".yml");
							newFile.getParentFile().mkdirs();
							if (!element.getValue().getFile().renameTo(newFile)) {
								if (!element.getValue().getFile().delete()) {
									element.getValue().getFile().deleteOnExit();
								}
							}
						}
					}
				};
				// open sub
				sub.open(player);
			}
		});
		// shop existence conditions
		setRegularItem(new EditorItem("shop_existence_conditions", 3, EditorGUI.ICON_CONDITION, SSLocaleGUI.GUI_SUPREMESHOPS_EDITORSHOPEXISTENCECONDITIONS.getLine(), SSLocaleGUI.GUI_SUPREMESHOPS_EDITORSHOPEXISTENCECONDITIONSLORE.getLines()) {
			@Override
			protected void onClick(final Player player, ClickType clickType, int pageIndex) {
				// sub
				EditorSelectionGUI sub = new EditorSelectionGUI<CPConditions>(SupremeShops.inst(), EditorMainGUI.this, "Shop existence conditions") {
					@Override
					protected void fill() {
						super.fill();
						// add values
						int slot = -1;
						for (CPConditions condition : Utils.asSortedList(SupremeShops.inst().getModuleManager().getShopExistenceConditions().values())) {
							addItem(new Wrapper<>(condition), EditorGUI.ICON_CONDITION, condition.getId(), "§6" + condition.getId(), ++slot);
						}
					}
					@Override
					protected boolean doesElementExist(String id) {
						return SupremeShops.inst().getModuleManager().getShopExistenceCondition(id) != null;
					}
					@Override
					protected void onElementClick(Wrapper<CPConditions> element, Player player, ClickType clickType, int pageIndex) {
						element.getValue().createEditor(this, player, new ModifCallback<CPConditions>(element, CPConditions.class) {
							@Override
							protected void onModif(CPConditions instance, EditorGUI from, Player player) {
								SupremeShops.inst().getModuleManager().getShopExistenceConditions().put(instance.getId().toLowerCase(), instance);// replace instance
								YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/existence_conditions.yml"), "shops/existence_conditions.yml", true, true);
								instance.save(new ConfigData(SupremeShops.inst(), "shop existence condition " + instance.getId(), config, "conditions." + instance.getId()));
								config.save();
							}
						}).open(player);
					}
					@Override
					protected void createElement(String id) {
						CPConditions element = new CPConditions(id, null, false, -1, null, null);
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/existence_conditions.yml"), "shops/existence_conditions.yml", true, true);
						element.save(new ConfigData(SupremeShops.inst(), "shop existence condition " + element.getId(), config, "conditions." + element.getId()));
						config.save();
						SupremeShops.inst().getModuleManager().getShopExistenceConditions().put(id, element);
					}
					@Override
					protected void deleteElement(Wrapper<CPConditions> element) {
						SupremeShops.inst().getModuleManager().getShopExistenceConditions().remove(element.getValue().getId());
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/existence_conditions.yml"), "shops/existence_conditions.yml", true, true);
						config.set(element.getValue().getId(), null);
						config.save();
					}
				};
				// open sub
				sub.open(player);
			}
		});
		// shop destruction conditions
		setRegularItem(new EditorItem("shop_destruction_conditions", 4, EditorGUI.ICON_CONDITION, SSLocaleGUI.GUI_SUPREMESHOPS_EDITORSHOPDESTRUCTIONCONDITIONS.getLine(), SSLocaleGUI.GUI_SUPREMESHOPS_EDITORSHOPDESTRUCTIONCONDITIONSLORE.getLines()) {
			@Override
			protected void onClick(final Player player, ClickType clickType, int pageIndex) {
				// sub
				EditorSelectionGUI sub = new EditorSelectionGUI<CPConditions>(SupremeShops.inst(), EditorMainGUI.this, "Shop destruction conditions") {
					@Override
					protected void fill() {
						super.fill();
						// add values
						int slot = -1;
						for (CPConditions condition : Utils.asSortedList(SupremeShops.inst().getModuleManager().getShopDestructionConditions().values())) {
							addItem(new Wrapper<>(condition), EditorGUI.ICON_CONDITION, condition.getId(), "§6" + condition.getId(), ++slot);
						}
					}
					@Override
					protected boolean doesElementExist(String id) {
						return SupremeShops.inst().getModuleManager().getShopDestructionCondition(id) != null;
					}
					@Override
					protected void onElementClick(Wrapper<CPConditions> element, Player player, ClickType clickType, int pageIndex) {
						element.getValue().createEditor(this, player, new ModifCallback<CPConditions>(element, CPConditions.class) {
							@Override
							protected void onModif(CPConditions instance, EditorGUI from, Player player) {
								SupremeShops.inst().getModuleManager().getShopDestructionConditions().put(instance.getId().toLowerCase(), instance);// replace instance
								YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/destruction_conditions.yml"), "shops/destruction_conditions.yml", true, true);
								instance.save(new ConfigData(SupremeShops.inst(), "shop destruction condition " + instance.getId(), config, "conditions." + instance.getId()));
								config.save();
							}
						}).open(player);
					}
					@Override
					protected void createElement(String id) {
						CPConditions element = new CPConditions(id, null, false, -1, null, null);
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/destruction_conditions.yml"), "shops/destruction_conditions.yml", true, true);
						element.save(new ConfigData(SupremeShops.inst(), "shop destruction condition " + element.getId(), config, "conditions." + element.getId()));
						config.save();
						SupremeShops.inst().getModuleManager().getShopDestructionConditions().put(id, element);
					}
					@Override
					protected void deleteElement(Wrapper<CPConditions> element) {
						SupremeShops.inst().getModuleManager().getShopDestructionConditions().remove(element.getValue().getId());
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/destruction_conditions.yml"), "shops/destruction_conditions.yml", true, true);
						config.set(element.getValue().getId(), null);
						config.save();
					}
				};
				// open sub
				sub.open(player);
			}
		});
		// merchant existence conditions
		setRegularItem(new EditorItem("merchant_existence_conditions", 5, EditorGUI.ICON_CONDITION, SSLocaleGUI.GUI_SUPREMESHOPS_EDITORMERCHANTEXISTENCECONDITIONS.getLine(), SSLocaleGUI.GUI_SUPREMESHOPS_EDITORMERCHANTEXISTENCECONDITIONSLORE.getLines()) {
			@Override
			protected void onClick(final Player player, ClickType clickType, int pageIndex) {
				// sub
				EditorSelectionGUI sub = new EditorSelectionGUI<CPConditions>(SupremeShops.inst(), EditorMainGUI.this, "Merchant existence conditions") {
					@Override
					protected void fill() {
						super.fill();
						// add values
						int slot = -1;
						for (CPConditions condition : Utils.asSortedList(SupremeShops.inst().getModuleManager().getMerchantExistenceConditions().values())) {
							addItem(new Wrapper<>(condition), EditorGUI.ICON_CONDITION, condition.getId(), "§6" + condition.getId(), ++slot);
						}
					}
					@Override
					protected boolean doesElementExist(String id) {
						return SupremeShops.inst().getModuleManager().getMerchantExistenceCondition(id) != null;
					}
					@Override
					protected void onElementClick(Wrapper<CPConditions> element, Player player, ClickType clickType, int pageIndex) {
						element.getValue().createEditor(this, player, new ModifCallback<CPConditions>(element, CPConditions.class) {
							@Override
							protected void onModif(CPConditions instance, EditorGUI from, Player player) {
								SupremeShops.inst().getModuleManager().getMerchantExistenceConditions().put(instance.getId().toLowerCase(), instance);// replace instance
								YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/merchants/existence_conditions.yml"), "merchants/existence_conditions.yml", true, true);
								instance.save(new ConfigData(SupremeShops.inst(), "merchant existence condition " + instance.getId(), config, "conditions." + instance.getId()));
								config.save();
							}
						}).open(player);
					}
					@Override
					protected void createElement(String id) {
						CPConditions element = new CPConditions(id, null, false, -1, null, null);
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/merchants/existence_conditions.yml"), "merchants/existence_conditions.yml", true, true);
						element.save(new ConfigData(SupremeShops.inst(), "merchant existence condition " + element.getId(), config, "conditions." + element.getId()));
						config.save();
						SupremeShops.inst().getModuleManager().getMerchantExistenceConditions().put(id, element);
					}
					@Override
					protected void deleteElement(Wrapper<CPConditions> element) {
						SupremeShops.inst().getModuleManager().getMerchantExistenceConditions().remove(element.getValue().getId());
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/merchants/existence_conditions.yml"), "merchants/existence_conditions.yml", true, true);
						config.set(element.getValue().getId(), null);
						config.save();
					}
				};
				// open sub
				sub.open(player);
			}
		});
		// merchant destruction conditions
		setRegularItem(new EditorItem("merchant_destruction_conditions", 6, EditorGUI.ICON_CONDITION, SSLocaleGUI.GUI_SUPREMESHOPS_EDITORMERCHANTDESTRUCTIONCONDITIONS.getLine(), SSLocaleGUI.GUI_SUPREMESHOPS_EDITORMERCHANTDESTRUCTIONCONDITIONSLORE.getLines()) {
			@Override
			protected void onClick(final Player player, ClickType clickType, int pageIndex) {
				// sub
				EditorSelectionGUI sub = new EditorSelectionGUI<CPConditions>(SupremeShops.inst(), EditorMainGUI.this, "Merchant destruction conditions") {
					@Override
					protected void fill() {
						super.fill();
						// add values
						int slot = -1;
						for (CPConditions condition : Utils.asSortedList(SupremeShops.inst().getModuleManager().getMerchantDestructionConditions().values())) {
							addItem(new Wrapper<>(condition), EditorGUI.ICON_CONDITION, condition.getId(), "§6" + condition.getId(), ++slot);
						}
					}
					@Override
					protected boolean doesElementExist(String id) {
						return SupremeShops.inst().getModuleManager().getMerchantDestructionCondition(id) != null;
					}
					@Override
					protected void onElementClick(final Wrapper<CPConditions> element, Player player, ClickType clickType, int pageIndex) {
						element.getValue().createEditor(this, player, new ModifCallback<CPConditions>(element, CPConditions.class) {
							@Override
							protected void onModif(CPConditions instance, EditorGUI from, Player player) {
								SupremeShops.inst().getModuleManager().getMerchantDestructionConditions().put(instance.getId().toLowerCase(), instance);// replace instance
								YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/merchants/destruction_conditions.yml"), "merchants/destruction_conditions.yml", true, true);
								instance.save(new ConfigData(SupremeShops.inst(), "merchant destruction condition " + instance.getId(), config, "conditions." + instance.getId()));
								config.save();
							}
						}).open(player);
					}
					@Override
					protected void createElement(String id) {
						CPConditions element = new CPConditions(id, null, false, -1, null, null);
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/merchants/destruction_conditions.yml"), "merchants/destruction_conditions.yml", true, true);
						element.save(new ConfigData(SupremeShops.inst(), "merchant destruction condition " + element.getId(), config, "conditions." + element.getId()));
						config.save();
						SupremeShops.inst().getModuleManager().getMerchantDestructionConditions().put(id, element);
					}
					@Override
					protected void deleteElement(Wrapper<CPConditions> element) {
						SupremeShops.inst().getModuleManager().getMerchantDestructionConditions().remove(element.getValue().getId());
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/merchants/destruction_conditions.yml"), "merchants/destruction_conditions.yml", true, true);
						config.set(element.getValue().getId(), null);
						config.save();
					}
				};
				// open sub
				sub.open(player);
			}
		});
		// trade modifiers
		setRegularItem(new EditorItem("trade_modifiers", 7, EditorGUI.ICON_MODIFIER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITORTRADEMODIFIER.getLine(), SSLocaleGUI.GUI_SUPREMESHOPS_EDITORTRADEMODIFIERLORE.getLines()) {
			@Override
			protected void onClick(final Player player, ClickType clickType, int pageIndex) {
				// sub
				EditorSelectionGUI sub = new EditorSelectionGUI<Modifier>(SupremeShops.inst(), EditorMainGUI.this, "Trade modifiers") {
					@Override
					protected void fill() {
						super.fill();
						// add values
						int slot = -1;
						for (Modifier modifier : Utils.asSortedList(SupremeShops.inst().getModuleManager().getTradeModifiers().values())) {
							addItem(new Wrapper<Modifier>(modifier), EditorGUI.ICON_MODIFIER, modifier.getId(), "§6" + modifier.getId(), ++slot);
						}
					}
					@Override
					protected boolean doesElementExist(String id) {
						return SupremeShops.inst().getModuleManager().getTradeModifier(id) != null;
					}
					@Override
					protected void onElementClick(Wrapper<Modifier> element, Player player, ClickType clickType, int pageIndex) {
						element.getValue().createEditor(this, player, new ModifCallback<Modifier>(element, Modifier.class) {
							@Override
							protected void onModif(Modifier instance, EditorGUI from, Player player) {
								SupremeShops.inst().getModuleManager().getTradeModifiers().put(instance.getId().toLowerCase(), instance);// replace instance
								YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/trade_modifiers.yml"), "shops/trade_modifiers.yml", true, true);
								instance.save(new ConfigData(SupremeShops.inst(), "trade modifier " + instance.getId(), config, "modifiers." + instance.getId()));
								config.save();
							}
						}).open(player);
					}
					@Override
					protected void createElement(String id) {
						Modifier element = new Modifier(id, null, false, -1, null, null);
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/trade_modifiers.yml"), "shops/trade_modifiers.yml", true, true);
						element.save(new ConfigData(SupremeShops.inst(), "trade modifier " + element.getId(), config, "modifiers." + element.getId()));
						config.save();
						SupremeShops.inst().getModuleManager().getTradeModifiers().put(id, element);
					}
					@Override
					protected void deleteElement(Wrapper<Modifier> element) {
						SupremeShops.inst().getModuleManager().getTradeModifiers().remove(element.getValue().getId());
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/trade_modifiers.yml"), "shops/trade_modifiers.yml", true, true);
						config.set(element.getValue().getId(), null);
						config.save();
					}
				};
				// open sub
				sub.open(player);
			}
		});
		// trade modifiers taxes
		setRegularItem(new EditorItem("trade_modifiers_taxes", 8, EditorGUI.ICON_MODIFIER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITORTRADEMODIFIERTAXES.getLine(), SSLocaleGUI.GUI_SUPREMESHOPS_EDITORTRADEMODIFIERTAXESLORE.getLines()) {
			@Override
			protected void onClick(final Player player, ClickType clickType, int pageIndex) {
				// sub
				EditorSelectionGUI sub = new EditorSelectionGUI<Modifier>(SupremeShops.inst(), EditorMainGUI.this, "Trade modifiers (taxes)") {
					@Override
					protected void fill() {
						super.fill();
						// add values
						int slot = -1;
						for (Modifier modifier : Utils.asSortedList(SupremeShops.inst().getModuleManager().getTradeModifiersTaxes().values())) {
							addItem(new Wrapper<>(modifier), EditorGUI.ICON_MODIFIER, modifier.getId(), "§6" + modifier.getId(), ++slot);
						}
					}
					@Override
					protected boolean doesElementExist(String id) {
						return SupremeShops.inst().getModuleManager().getTradeModifierTax(id) != null;
					}
					@Override
					protected void onElementClick(Wrapper<Modifier> element, Player player, ClickType clickType, int pageIndex) {
						element.getValue().createEditor(this, player, new ModifCallback<Modifier>(element, Modifier.class) {
							@Override
							protected void onModif(Modifier instance, EditorGUI from, Player player) {
								SupremeShops.inst().getModuleManager().getTradeModifiersTaxes().put(instance.getId().toLowerCase(), instance);// replace instance
								YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/trade_modifiers_taxes.yml"), "shops/trade_modifiers_taxes.yml", true, true);
								instance.save(new ConfigData(SupremeShops.inst(), "trade modifier tax " + instance.getId(), config, "modifiers." + instance.getId()));
								config.save();
							}
						}).open(player);
					}
					@Override
					protected void createElement(String id) {
						Modifier element = new Modifier(id, null, false, -1, null, null);
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/trade_modifiers_taxes.yml"), "shops/trade_modifiers_taxes.yml", true, true);
						element.save(new ConfigData(SupremeShops.inst(), "trade modifier tax " + element.getId(), config, "modifiers." + element.getId()));
						config.save();
						SupremeShops.inst().getModuleManager().getTradeModifiersTaxes().put(id, element);
					}
					@Override
					protected void deleteElement(Wrapper<Modifier> element) {
						SupremeShops.inst().getModuleManager().getTradeModifiersTaxes().remove(element.getValue().getId());
						YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/trade_modifiers_taxes.yml"), "shops/trade_modifiers_taxes.yml", true, true);
						config.set(element.getValue().getId(), null);
						config.save();
					}
				};
				// open sub
				sub.open(player);
			}
		});
	}

}
