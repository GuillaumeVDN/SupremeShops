package com.guillaumevdn.supremeshops.gui;

import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;

public abstract class UserInfoSelectionGUI extends FilledGUI {

	// base
	private List<UserInfo> users;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public UserInfoSelectionGUI(List<UserInfo> users, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADESELECTPLAYERGUINAME.getLine(), 54, GUI.SLOTS_0_TO_53);
		this.users = users;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	public List<UserInfo> getUsers() {
		return users;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// methods
	@Override
	protected void fill() {
		// add users
		for (final UserInfo user : Utils.asSortedList(users, Utils.objectSorter)) {
			OfflinePlayer offline = user != null ? user.toOfflinePlayer() : null;
			if (offline == null) continue;
			setRegularItem(new ClickeableItem(GUI.getPlayerHead(-1, "§6" + offline.getName(), null)) {
				@Override
				public void onClick(Player clicker, ClickType clickType, GUI gui, int pageIndex) {
					clicker.closeInventory();
					onSelect(user, gui, pageIndex);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(clicker);
					}
				}
			});
		}
		// add back item
		if (fromGUI != null) {
			setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(52)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					fromGUI.open(player, fromGUIPageIndex);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
	}

	@Override
	protected boolean postFill() {
		return true;
	}

	// abstract methods
	protected abstract void onSelect(UserInfo selected, GUI gui, int pageIndex);

}
