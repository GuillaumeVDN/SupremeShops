package com.guillaumevdn.supremeshops.gui.playertrade;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.gui.ShowcaseRowsGUI;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.playertrade.PlayerTrade;

public class PlayerTradeGUI extends ShowcaseRowsGUI {

	// base
	private PlayerTrade trade;
	private Player mainPlayer, otherPlayer;
	private Row mainRow, otherRow;

	public PlayerTradeGUI(final PlayerTrade trade, final Player mainPlayer) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADEGUINAME.getLine(), 54, GUI.SLOTS_0_TO_53, false, SupremeShops.inst().getModuleManager().getGuiClickSound());
		this.trade = trade;
		this.mainPlayer = mainPlayer;
		this.otherPlayer = trade.getPlayer1().equals(mainPlayer) ? trade.getPlayer2() : trade.getPlayer1();
		// disable pages
		disablePages(true);
		createPage();
		// add item rows
		addRow(mainRow = this.new Row(Utils.asList(GUI.getItemSlots(9, 26))));
		addRow(otherRow = this.new Row(Utils.asList(GUI.getItemSlots(36, 53))));
	}

	// get
	public PlayerTrade getTrade() {
		return trade;
	}

	public Player getMainPlayer() {
		return mainPlayer;
	}

	public Player getOtherPlayer() {
		return otherPlayer;
	}

	// fill
	@Override
	protected void fill() {
		// set separators
		ItemStack stack = new ItemData(null, -1, Mat.GRAY_STAINED_GLASS_PANE, 1, " ", null).getItemStack();
		for (int slot : Utils.asList(2, 4, 5, 6, 7, 29, 31, 32, 33, 34, 35)) {
			setRegularItem(new ClickeableItem(new ItemData("separator_" + slot, slot, stack)));
		}
		// set heads
		setRegularItem(new ClickeableItem(GUI.getPlayerHead(0, SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADEOFFERINGSELFHEADERNAME.getLine(), null)));
		setRegularItem(new ClickeableItem(GUI.getPlayerHead(27, SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADEOFFERINGHEADERNAME.getLine("{player}", otherPlayer.getName()), null)));
		// set ready items
		updateReadyItems();
		// set add object item
		setRegularItem(new ClickeableItem(new ItemData("add_object", 3, Mat.BLAZE_ROD, 1, SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADEADDOBJECTNAME.getLine(), null)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// can't add if one of the two is already ready
				if (trade.isLocked()) {
					SSLocale.MSG_SUPREMESHOPS_PLAYERTRADELOCKED.send(player);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
					}
					return;
				}
				// select type
				int size = Utils.getInventorySize(ObjectType.values().size());
				GUI typeGui = new GUI(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADEADDOBJECTSELECTTYPEGUINAME.getLine(), size, GUI.getRegularItemSlots(0, size - 1));
				for (final ObjectType type : ObjectType.values()) {
					// can't add
					if (!type.getLogic().canAddToPlayerTrade(mainPlayer, trade)) continue;
					// attempt to add
					typeGui.setRegularItem(new ClickeableItem(new ItemData("type_" + type.getId(), -1, type.getIcon(), 1, "§a" + Utils.capitalizeFirstLetter(type.getName()), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							type.getLogic().attemptToAddToPlayerTrade(mainPlayer, trade, PlayerTradeGUI.this);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				}
				typeGui.open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// set chat item
		setRegularItem(new ClickeableItem(new ItemData("chat", 30, Mat.PAPER, 1, SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADECHATNAME.getLine("{player}", otherPlayer.getName()), null)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				trade.chat(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// set cancel item
		setRegularItem(new ClickeableItem(new ItemData("cancel", 8, Mat.TNT_MINECART, 1, SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADECANCELNAME.getLine(), null)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				trade.cancel();
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// update rows
		updateMainOffering();
		updateOtherOffering();
	}

	@Override
	protected boolean postFill() {
		return true;
	}

	public void updateReadyItems() {
		// main player
		final boolean mainReady = trade.isReady(mainPlayer);
		setRegularItem(new ClickeableItem(new ItemData("ready_main", 1, mainReady ? Mat.GREEN_WOOL : Mat.RED_WOOL, 1, (mainReady ? SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADEREADYNAME : SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADENOTREADYNAME).getLine("{player}", mainPlayer.getName()), null)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				trade.setReady(player, !mainReady);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// other player
		final boolean otherReady = trade.isReady(otherPlayer);
		setRegularItem(new ClickeableItem(new ItemData("ready_other", 28, otherReady ? Mat.GREEN_WOOL : Mat.RED_WOOL, 1, (otherReady ? SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADEREADYNAME : SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADENOTREADYNAME).getLine("{player}", otherPlayer.getName()), null)));
		updateFirstSlot();// for god's sake
	}

	public void updateMainOffering() {
		// build items
		List<ClickeableItem> items = new ArrayList<ClickeableItem>();
		for (final TradeObject object : trade.getOffering(mainPlayer)) {
			// add controls in lore
			ItemStack stack = object.getPreviewItem(object.getCustomAmount(null), null).getItemStack();
			ItemMeta meta = stack.getItemMeta();
			List<String> lore;
			if (meta.getLore() == null || meta.getLore().isEmpty()) {
				lore = new ArrayList<String>();
			} else {
				lore = Utils.asList(meta.getLore());
				lore.add(" ");
			}
			lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADEOBJECTITEMLORE.getLines());
			meta.setLore(lore);
			stack.setItemMeta(meta);
			// build item
			items.add(new ClickeableItem(new ItemData("main_offering_" + UUID.randomUUID().toString().split("-")[0], -1, stack)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// shift + right-click : remove item
					if (clickType.equals(ClickType.SHIFT_RIGHT)) {
						trade.removeOffering(mainPlayer, object);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				}
			});
		}
		// replace items
		mainRow.replaceItems(items, true);
	}

	public void updateOtherOffering() {
		// build items
		List<ClickeableItem> items = new ArrayList<ClickeableItem>();
		for (final TradeObject object : trade.getOffering(otherPlayer)) {
			items.add(new ClickeableItem(new ItemData("other_offering_" + UUID.randomUUID().toString().split("-")[0], -1, object.getPreviewItem(object.getCustomAmount(null), null).getItemStack())));
		}
		// replace items
		otherRow.replaceItems(items, true);
	}

}
