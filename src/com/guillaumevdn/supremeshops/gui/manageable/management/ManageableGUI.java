package com.guillaumevdn.supremeshops.gui.manageable.management;

import com.guillaumevdn.supremeshops.module.manageable.Manageable;

public interface ManageableGUI {

	Manageable getManageable();

}
