package com.guillaumevdn.supremeshops.gui.manageable.management;

import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.ChatInput;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.manageable.ManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ManageableEditManagersGUI<P extends ManagementPermission> extends FilledGUI implements ManageableGUI {

	// amount
	private Manageable manageable;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public ManageableEditManagersGUI(Manageable<P> manageable, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_MANAGEABLEEDITMANAGERSGUINAME.getLine(manageable.getMessageReplacers(false, false, player)), 54, GUI.SLOTS_0_TO_44);
		this.manageable = manageable;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public Manageable getManageable() {
		return manageable;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill GUI
	@Override
	protected void fill() {
		// add contents
		Set<UserInfo> managers = manageable.getManagers().keySet();
		for (final UserInfo manager : managers) {
			// set item
			final OfflinePlayer managerPlayer = manager.toOfflinePlayer();
			setRegularItem(new ClickeableItem(GUI.getPlayerHead(-1, managerPlayer.getName(), SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITMANAGERLORE.getLines())) {
				@Override
				public void onClick(final Player player, final ClickType clickType, final GUI gui, final int pageIndex) {
					// left-click : edit permissions
					if (clickType.equals(ClickType.LEFT)) {
						new ManageableEditManagerPermissionsGUI(manageable, manager, player, ManageableEditManagersGUI.this, pageIndex).open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
					// shift + left-click : copy permissions
					else if (clickType.equals(ClickType.SHIFT_LEFT)) {
						if (manageable instanceof Shop) {
							SupremeShops.inst().getGeneralManager().setCopyingShopManagerPermissions(player, manager, Utils.asSet(manageable.getManagementPermissions(manager)));
							SSLocale.MSG_SUPREMESHOPS_SHOPPERMISSIONSCOPYSTART.send(player, "{player}", manager.toOfflinePlayer().getName());
						} else if (manageable instanceof Merchant) {
							SupremeShops.inst().getGeneralManager().setCopyingMerchantManagerPermissions(player, manager, Utils.asSet(manageable.getManagementPermissions(manager)));
							SSLocale.MSG_SUPREMESHOPS_MERCHANTPERMISSIONSCOPYSTART.send(player, "{player}", manager.toOfflinePlayer().getName());
						}
						player.closeInventory();
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
					// right-click : edit wage
					else if (clickType.equals(ClickType.RIGHT)) {
						new ManageableEditManagerWageGUI(manageable, manager, player, ManageableEditManagersGUI.this, pageIndex).open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
					// shift + right-click : remove manager
					else if (clickType.equals(ClickType.SHIFT_RIGHT)) {
						manageable.removeManager(manager);
						ManageableEditManagersGUI.this.open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
						// log
						SupremeShops.inst().pluginLog(manageable, player, null, "Removed manager " + manager.toString());
					}
				}
			});
		}
		// add manager item
		setPersistentItem(new ClickeableItem(new ItemData("add_manager", 49, Mat.BLAZE_ROD, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MANAGEABLEEDITADDMANAGERLORE.getLine(), null)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// can't add more
				if (!manageable.isCurrentOwnerAdmin() && manageable.getManagers().size() >= (manageable instanceof Shop ? SupremeShops.inst().getModuleManager().getMaxShopManagers(manageable.getCurrentOwner().toOfflinePlayer()) : (manageable instanceof Merchant ? SupremeShops.inst().getModuleManager().getMaxMerchantManagers(manageable.getCurrentOwner().toOfflinePlayer()) : Integer.MAX_VALUE))) {
					SSLocale.MSG_SUPREMESHOPS_MANAGEABLEMAXMANAGERSREACHED.send(player);
					return;
				}
				// enter name in chat
				player.closeInventory();
				SSLocale.MSG_SUPREMESHOPS_CREATEMANAGERINPUT.send(player);
				GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
					@Override
					public void onChat(Player player, String value) {
						// cancel
						if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
							ManageableEditManagersGUI.this.open(player);
							return;
						}
						// invalid player
						OfflinePlayer managerPlayer = Bukkit.getOfflinePlayer(value);
						if (managerPlayer == null) {
							ManageableEditManagersGUI.this.open(player);
							GLocale.MSG_GENERIC_INVALIDPLAYEROFFLINE.send(player, "{player}", value, "{plugin}", SupremeShops.inst().getName());
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
							}
							return;
						}
						// already a manager
						UserInfo manager = new UserInfo(managerPlayer);
						if (manageable.getManagers().containsKey(manager)) {
							ManageableEditManagersGUI.this.open(player);
							SSLocale.MSG_SUPREMESHOPS_MANAGEABLEALREADYMANAGER.send(player, "{player}", managerPlayer.getName());
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
							}
							return;
						}
						// add manager
						manageable.addManager(manager);
						ManageableEditManagersGUI.this.open(player);
						SSLocale.MSG_SUPREMESHOPS_MANAGEABLEADDMANAGER.send(player, "{player}", managerPlayer.getName());
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
						SupremeShops.inst().pluginLog(manageable, player, null, "Added manager " + manager.toString());
					}
				});
			}
		});
		// add back item
		setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(52)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				fromGUI.open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
