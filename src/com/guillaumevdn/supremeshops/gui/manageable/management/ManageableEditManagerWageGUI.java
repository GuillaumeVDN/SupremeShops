package com.guillaumevdn.supremeshops.gui.manageable.management;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.object.TradeObject.ObjectRestockLogic;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ManageableEditManagerWageGUI extends FilledGUI implements ManageableGUI {

	// amount
	private Manageable manageable;
	private UserInfo manager;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public ManageableEditManagerWageGUI(Manageable manageable, UserInfo manager, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_MANAGEABLEEDITMANAGERWAGEGUINAME.getLine("{player}", manager.toOfflinePlayer().getName(), manageable.getMessageReplacers(false, false, player)), 54, GUI.SLOTS_0_TO_44);
		this.manageable = manageable;
		this.manager = manager;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public Manageable getManageable() {
		return manageable;
	}

	public UserInfo getManager() {
		return manager;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill GUI
	@Override
	protected void fill() {
		// add contents
		final Map<TradeObject, Double> wage = manageable.getManagerWage(manager);
		for (final TradeObject object : wage.keySet()) {
			// add stock in lore
			ItemStack stack = object.getPreviewItem(object.getCustomAmount(null), null).getItemStack();
			ItemMeta meta = stack.getItemMeta();
			List<String> lore;
			if (meta.getLore() == null || meta.getLore().isEmpty()) {
				lore = new ArrayList<String>();
			} else {
				lore = Utils.asList(meta.getLore());
				lore.add(" ");
			}
			lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_MANAGEABLEEDITMANAGERWAGEITEMLORE.getLines("{stock}", Utils.round5(wage.get(object))));
			// add controls in lore
			lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONTROLRESTOCKLORE.getLines());
			lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONTROLWITHDRAWLORE.getLines());
			lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONTROLREMOVEOBJECTLORE.getLines());
			// set item
			meta.setLore(lore);
			stack.setItemMeta(meta);
			setRegularItem(new ClickeableItem(new ItemData("wage_" + object.hashCode(), -1, stack)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// left-click : add stock
					if (clickType.equals(ClickType.LEFT)) {
						object.startRestock(player, ManageableEditManagerWageGUI.this, new ObjectRestockLogic() {
							@Override
							public OfflinePlayer getCurrentOwner() {
								return manageable.getCurrentOwner() == null ? null : manageable.getCurrentOwner().toOfflinePlayer();
							}
							@Override
							public double getTotalObjectStock(List<ObjectType> types, boolean withManagersWages) {
								if (manageable instanceof Shop) {
									return ((Shop) manageable).getTotalObjectStock(types, withManagersWages);
								} else {
									return manageable.getTotalObjectStockInManagersWages(types);
								}
							}
							@Override
							public void addStockAmount(double amount) {
								manageable.changeModifyManagerWageStock(manager, object, amount);
							}
						});
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
					// right-click : withdraw
					else if (clickType.equals(ClickType.RIGHT)) {
						double stock = wage.get(object);
						if (stock > 0d) {
							manageable.changeModifyManagerWageStock(manager, object, -stock);
							object.give(player, stock);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					}
					// shift + right-click : remove item
					else if (clickType.equals(ClickType.SHIFT_RIGHT)) {
						if (wage.get(object) > 0d) {// there's still stock
							SSLocale.MSG_SUPREMESHOPS_PLEASEWITHDRAW.getLines();
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
							}
						} else {// delete item
							manageable.removeManagerWage(manager, object);
							ManageableEditManagerWageGUI.this.open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
							// log
							SupremeShops.inst().pluginLog(manageable, player, null, "Removed " + object.getSide().name() + " object " + object.toString() + " from wage of manager " + manager.toStringName());
						}
					}
				}
			});
		}
		// add object item
		setPersistentItem(new ClickeableItem(new ItemData("add_object", 47, Mat.BLAZE_ROD, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITADDOBJECTNAME.getLine(), null)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// select type
				int size = Utils.getInventorySize(ObjectType.values().size());
				GUI typeGui = new GUI(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITADDOBJECTSELECTTYPE.getLine(), size, GUI.getRegularItemSlots(0, size - 1));
				for (final ObjectType type : ObjectType.values()) {
					// can't add
					if (!type.getLogic().canAddToManageableManagerWage(player, manageable, manager)) continue;
					// attempt to add
					typeGui.setRegularItem(new ClickeableItem(new ItemData("type_" + type.getId(), -1, type.getIcon(), 1, type.getName(), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							type.getLogic().attemptToAddToManageableManagerWage(player, manageable, manager, ManageableEditManagerWageGUI.this);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				}
				typeGui.open(player);
			}
		});
		// add back item
		setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(52)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				fromGUI.open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
