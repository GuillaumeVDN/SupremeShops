package com.guillaumevdn.supremeshops.gui.manageable.management;

import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.manageable.ManagementPermission;

public class ManageableEditManagerPermissionsGUI<P extends ManagementPermission> extends FilledGUI implements ManageableGUI {

	// base
	private Manageable<P> manageable;
	private UserInfo manager;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public ManageableEditManagerPermissionsGUI(Manageable<P> manageable, UserInfo manager, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_MANAGEABLEEDITMANAGERPERMISSIONSGUINAME.getLine("{player}", manager.toOfflinePlayer().getName(), manageable.getMessageReplacers(false, false, player)), Utils.getInventorySize(manageable.getAllPermissions().size()), GUI.SLOTS_0_TO_44);
		this.manageable = manageable;
		this.manager = manager;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public Manageable<P> getManageable() {
		return manageable;
	}

	public UserInfo getManager() {
		return manager;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill
	private long lastToggle = 0L;

	@Override
	protected void fill() {
		// add permissions
		Set<P> permissions = manageable.getManagementPermissions(manager);
		for (final P permission : manageable.getAllPermissions()) {
			// can't edit
			if (!permission.canEditInGui() || (!permission.getPermission().has(manageable.getCurrentOwner()) && !manageable.isCurrentOwnerAdmin())) {
				continue;
			}
			// add
			final boolean has = permissions.contains(permission);
			setRegularItem(new ClickeableItem(new ItemData("permission_" + permission.name(), -1, has ? Mat.GREEN_WOOL : Mat.RED_WOOL, 1, (has ? "§a" : "§c") + permission.name(), SSLocaleMisc.MISC_SUPREMESHOPS_MANAGEABLEEDITCONTROLTOGGLEPERMISSIONLORE.getLines())) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// right-click : toggle
					if (clickType.equals(ClickType.RIGHT)) {
						// too recent
						if (System.currentTimeMillis() - lastToggle < 500L) {
							return;
						}
						lastToggle = System.currentTimeMillis();
						// toggle
						manageable.changeManagementPermission(manager, permission, !has);
						open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
						SupremeShops.inst().pluginLog(manageable, player, null, (!has ? "Added" : "Removed") + " permission " + permission.name() + " to manager " + manager.toString());
					}
				}
			});
		}
		// add back item
		setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(getSize() == 54 ? 52 : getSize() - 1)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				fromGUI.open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
