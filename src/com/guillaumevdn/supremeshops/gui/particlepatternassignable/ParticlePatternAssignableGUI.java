package com.guillaumevdn.supremeshops.gui.particlepatternassignable;

import com.guillaumevdn.supremeshops.module.particlepatternassignable.ParticlePatternAssignable;

/**
 * @author GuillaumeVDN
 */
public interface ParticlePatternAssignableGUI {

	ParticlePatternAssignable getParticlePatternAssignable();

}
