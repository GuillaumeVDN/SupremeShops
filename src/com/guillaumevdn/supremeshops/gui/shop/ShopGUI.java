package com.guillaumevdn.supremeshops.gui.shop;

import com.guillaumevdn.supremeshops.module.shop.Shop;

public interface ShopGUI {

	Shop getShop();

}
