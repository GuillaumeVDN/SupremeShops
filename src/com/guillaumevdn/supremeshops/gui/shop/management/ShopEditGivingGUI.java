package com.guillaumevdn.supremeshops.gui.shop.management;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.shop.ShopGUI;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.modifier.Modifier;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.object.TradeObject.ObjectRestockLogic;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ShopEditGivingGUI extends FilledGUI implements ShopGUI {

	// amount
	private Shop shop;
	private Set<ShopManagementPermission> permissions;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public ShopEditGivingGUI(Shop shop, Set<ShopManagementPermission> permissions, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITGIVEGUINAME.getLine(shop.getMessageReplacers(false, false, player)), 54, GUI.SLOTS_0_TO_44);
		this.shop = shop;
		this.permissions = permissions;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public Shop getShop() {
		return shop;
	}

	public Set<ShopManagementPermission> getPermissions() {
		return permissions;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill GUI
	@Override
	protected void fill() {
		// add contents
		for (final TradeObject object : getShop().getObjects(Utils.asList(ObjectSide.GIVING), null)) {
			// add stock in lore
			double amount = object.calculateAmountForTrades(shop, 1, Utils.emptyList(Modifier.class), null);
			ItemStack stack = object.getPreviewItem(amount, null).getItemStack();
			ItemMeta meta = stack.getItemMeta();
			List<String> lore;
			if (meta.getLore() == null || meta.getLore().isEmpty()) {
				lore = new ArrayList<String>();
			} else {
				lore = Utils.asList(meta.getLore());
				lore.add(" ");
			}
			lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_EDITGIVEITEMLORE.getLines("{stock}", Utils.round5(shop.getObjectStock(object))));
			// add controls in lore
			if (getShop().manageStock()) lore.addAll(permissions.contains(ShopManagementPermission.ADD_STOCK) ? SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONTROLRESTOCKLORE.getLines() : Utils.addBeforeAll(Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONTROLRESTOCKLORE.getLines()), "§7§o"));
			if (getShop().manageStock()) lore.addAll(permissions.contains(ShopManagementPermission.REMOVE_STOCK) ? SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONTROLWITHDRAWLORE.getLines() : Utils.addBeforeAll(Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONTROLWITHDRAWLORE.getLines()), "§7§o"));
			lore.addAll(permissions.contains(ShopManagementPermission.REMOVE_OBJECT) ? SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONTROLREMOVEOBJECTLORE.getLines() : Utils.addBeforeAll(Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONTROLREMOVEOBJECTLORE.getLines()), "§7§o"));
			// set item
			meta.setLore(lore);
			stack.setItemMeta(meta);
			setRegularItem(new ClickeableItem(new ItemData("giving_" + object.hashCode(), -1, stack)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// left-click : add stock
					if (clickType.equals(ClickType.LEFT)) {
						if (shop.manageStock() && permissions.contains(ShopManagementPermission.ADD_STOCK)) {
							object.startRestock(player, ShopEditGivingGUI.this, new ObjectRestockLogic() {
								@Override
								public OfflinePlayer getCurrentOwner() {
									return shop.getCurrentOwner() == null ? null : shop.getCurrentOwner().toOfflinePlayer();
								}
								@Override
								public double getTotalObjectStock(java.util.List<ObjectType> types, boolean withManagersWages) {
									return shop.getTotalObjectStock(types, withManagersWages);
								}
								@Override
								public void addStockAmount(double amount) {
									shop.changeModifyObjectStock(object, amount);
								}
							});
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					}
					// right-click : withdraw stock
					else if (clickType.equals(ClickType.RIGHT)) {
						if (shop.manageStock() && permissions.contains(ShopManagementPermission.REMOVE_STOCK) && object.withdrawAsMuchStockAsPossible(player, shop) > 0d) {
							getShop().pushAsync();
							open(player, pageIndex);
						}
					}
					// shift + right-click : remove object
					else if (clickType.equals(ClickType.SHIFT_RIGHT)) {
						if (permissions.contains(ShopManagementPermission.REMOVE_OBJECT)) {
							if (shop.manageStock() && shop.getObjectStock(object) > 0d) {// there's still stock
								SSLocale.MSG_SUPREMESHOPS_PLEASEWITHDRAW.send(player);
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
								}
							} else {// delete item
								getShop().removeObject(object);
								ShopEditGivingGUI.this.open(player);
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
								// log
								SupremeShops.inst().pluginLog(shop, null, null, player, null, "Removed " + object.getSide().name() + " object " + object.toString());
							}
						}
					}
				}
			});
		}
		// add object item
		if (permissions.contains(ShopManagementPermission.ADD_OBJECT)) {
			setPersistentItem(new ClickeableItem(new ItemData("add_object", 46, Mat.BLAZE_ROD, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITADDOBJECTNAME.getLine(), null)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// select type
					int size = Utils.getInventorySize(ObjectType.values().size());
					GUI typeGui = new GUI(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITADDOBJECTSELECTTYPE.getLine(), size, GUI.getRegularItemSlots(0, size - 3));
					for (final ObjectType type : ObjectType.values()) {
						// can't add
						if (!type.getLogic().canAddToShop(player, getShop(), ObjectSide.GIVING)) continue;
						// attempt to add
						typeGui.setRegularItem(new ClickeableItem(new ItemData("type_" + type.getId(), -1, type.getIcon(), 1, "§a" + Utils.capitalizeFirstLetter(type.getName()), null)) {
							@Override
							public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
								type.getLogic().attemptToAddToShop(player, getShop(), ObjectSide.GIVING, ShopEditGivingGUI.this);
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
							}
						});
					}
					// add back item
					if (fromGUI != null) {
						typeGui.setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(typeGui.getSize() - 2)) {
							@Override
							public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
								ShopEditGivingGUI.this.open(player);
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
							}
						});
					}
					// open gui
					typeGui.open(player);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		} else {
			setPersistentItem(new ClickeableItem(new ItemData("add_object", 46, Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_EDITADDOBJECTNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
		}
		// add restock all item
		if (shop.manageStock()) {
			if (permissions.contains(ShopManagementPermission.ADD_STOCK)) {
				setPersistentItem(new ClickeableItem(new ItemData("restock_all", 48, Mat.ENDER_CHEST, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITRESTOCKALLOBJECTNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITRESTOCKALLOBJECTLORE.getLines())) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						// restock this
						if (clickType.isLeftClick()) {
							getShop().restockAll(player);
							ShopEditGivingGUI.this.open(player);
						}
						// restock this and other shops
						else if (clickType.isRightClick()) {
							getShop().restockAll(player);
							player.closeInventory();
							SSLocale.MSG_SUPREMESHOPS_MULTIRESTOCKALLSTART.send(player);
							SupremeShops.inst().getGeneralManager().getMultiRestockingAll().add(player);
						}
					}
				});
			} else {
				setPersistentItem(new ClickeableItem(new ItemData("restock_all", 48, Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_EDITRESTOCKALLOBJECTNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
		// add withdraw all item
		if (shop.manageStock()) {
			if (permissions.contains(ShopManagementPermission.REMOVE_STOCK)) {
				setPersistentItem(new ClickeableItem(new ItemData("withdraw_all", 50, Mat.HOPPER, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITWITHDRAWALLOBJECTNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITWITHDRAWALLSTOCKLORE.getLines())) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						// withdraw this
						if (clickType.isLeftClick()) {
							// withdraw
							double count = 0;
							for (TradeObject object : getShop().getObjects(Utils.asList(ObjectSide.GIVING), null)) {
								count += object.withdrawAsMuchStockAsPossible(player, shop);
							}
							// reopen GUI
							if (count > 0) {
								ShopEditGivingGUI.this.open(player);
							}
						}
						// withdraw this and other shops
						else if (clickType.isRightClick()) {
							// withdraw
							double count = 0;
							for (TradeObject object : getShop().getObjects(Utils.asList(ObjectSide.GIVING), null)) {
								count += object.withdrawAsMuchStockAsPossible(player, shop);
							}
							// reopen GUI
							if (count > 0) {
								ShopEditGivingGUI.this.open(player);
							}
							// withdraw others
							player.closeInventory();
							SSLocale.MSG_SUPREMESHOPS_MULTIWITHDRAWALLSTOCKSTART.send(player);
							SupremeShops.inst().getGeneralManager().getMultiWithdrawingAllStock().add(player);
						}
					}
				});
			} else {
				setPersistentItem(new ClickeableItem(new ItemData("withraw_all", 50, Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_EDITWITHDRAWALLOBJECTNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
		// add back item
		setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(52)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				fromGUI.open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
