package com.guillaumevdn.supremeshops.gui.shop.management;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorItem;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;

public abstract class RestockXpLevelAmountSelectionGUI extends FilledGUI {

	// amount
	private int value = 0;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public RestockXpLevelAmountSelectionGUI(GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_RESTOCKXPLEVELAMOUNTSELECTGUINAME.getLine(), 27, GUI.SLOTS_0_TO_26);
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	public double getValue() {
		return value;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill GUI
	@Override
	protected void fill() {
		// add control items
		for (int i = 0; i < 9; ++i) fillDeltaItem((int) Math.pow(10, i), 9 + i, Mat.GREEN_WOOL);
		for (int i = 0; i < 9; ++i) fillDeltaItem((int) -Math.pow(10, i), 18 + i, Mat.RED_WOOL);
		// add back item
		if (fromGUI != null) {
			setRegularItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(8)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					fromGUI.open(player, fromGUIPageIndex);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
		// add separators around value item
		ItemStack stack = new ItemData(null, -1, Mat.GRAY_STAINED_GLASS_PANE, 1, " ", null).getItemStack();
		for (int i = 0; i < 9; ++i) {
			if (i != 4 && i != 8) setRegularItem(new ClickeableItem(new ItemData("separator_" + i, i, stack)));
		}
		// add value item
		updateValueItem();
	}

	@Override
	protected boolean postFill() {
		return true;
	}

	private void fillDeltaItem(final int delta, final int slot, final Mat icon) {
		setRegularItem(new EditorItem("control_item_" + (delta > 0 ? "add_" + delta : "take_" + Math.abs(delta)), slot, icon, (delta > 0 ? GLocale.GUI_GENERIC_EDITORNUMBERADD : GLocale.GUI_GENERIC_EDITORNUMBERTAKE).getLine("{amount}", Math.abs(delta)), GLocale.GUI_GENERIC_EDITORNUMBERADDTAKELORE.getLines()) {
			@Override
			protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
				// replace value
				value += delta;
				if (value < 0) {
					value = 0;
				} else {
					// more than the player has
					int balance = player.getLevel();
					if (value > balance) {
						value = balance;
						SSLocale.MSG_SUPREMESHOPS_RESTOCKVALUESELECTDONTHAVE.send(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
						}
					}
					// too much money in the shop already
					OfflinePlayer owner = getOwner();
					int current = getTotalXpLevelsInStock();
					int limit = owner == null ? Integer.MAX_VALUE /* technically we shouldn't be with an admin shop at this point but meh we never know */
							: SupremeShops.inst().getModuleManager().getMaxShopXpLevels(owner);
					if (current + value > limit) {
						value = limit - current;
						SSLocale.MSG_SUPREMESHOPS_RESTOCKVALUESELECTSTOCKLIMITREACHED.send(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
						}
					}
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
				// update value item
				updateValueItem();
			}
		});
	}

	private void updateValueItem() {
		setRegularItem(new EditorItem("value", 4, Mat.NETHER_STAR, SSLocaleMisc.MISC_SUPREMESHOPS_RESTOCKXPLEVELAMOUNTSELECTVALUEITEMNAME.getLine("{amount}", Utils.round5(value)), SSLocaleMisc.MISC_SUPREMESHOPS_RESTOCKXPLEVELAMOUNTSELECTVALUEITEMLORE.getLines()) {
			@Override
			protected void onClick(Player player, ClickType clickType, int pageIndex) {
				player.closeInventory();
				onSelect(player, value);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
	}
	
	protected abstract OfflinePlayer getOwner();
	protected abstract int getTotalXpLevelsInStock();
	protected abstract void onSelect(Player player, int amount);

}
