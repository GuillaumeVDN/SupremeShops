package com.guillaumevdn.supremeshops.gui.shop.management;

import java.util.List;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.ChatInput;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.ShopBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.gui.manageable.management.ManageableEditManagersGUI;
import com.guillaumevdn.supremeshops.gui.particlepatternassignable.EditParticlePatternGUI;
import com.guillaumevdn.supremeshops.gui.rentable.PayRentAmountSelectionGUI;
import com.guillaumevdn.supremeshops.gui.rentable.SeeRentGUI;
import com.guillaumevdn.supremeshops.gui.shop.ShopGUI;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.particlepatternassignable.ParticlePatternAssignable;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.BlockShop;
import com.guillaumevdn.supremeshops.module.shop.MerchantShop;
import com.guillaumevdn.supremeshops.module.shop.PlayerShop;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.DestroyCause;

public class ShopManagementGUI extends FilledGUI implements ShopGUI {

	// base
	private Shop shop;
	private Set<ShopManagementPermission> permissions;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public ShopManagementGUI(Shop shop, Set<ShopManagementPermission> permissions, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_SHOPMANAGEMENTGUINAME.getLine(shop.getMessageReplacers(false, false, player)), Utils.getInventorySize(SupremeShops.inst().getModuleManager().getShopManagementGuiSize()), GUI.SLOTS_0_TO_53);
		this.shop = shop;
		this.permissions = permissions;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public Shop getShop() {
		return shop;
	}

	public Set<ShopManagementPermission> getPermissions() {
		return permissions;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill GUI
	private long lastToggle = 0L;

	@Override
	protected void fill() {
		// add info item
		updateInfoItem();
		// add open/close item
		updateCloseToggleItem();
		// add remote/non-remote item
		updateRemoteToggleItem();
		// add set display/non-display items item
		updateDisplayItemsToggleItem();
		// add set admin stock
		updateAdminStockToggleItem();
		// add managers item
		if (shop instanceof Manageable) {
			if (shop.isCurrentOwnerAdmin() || ShopManagementPermission.EDIT_MANAGERS.getPermission().has(shop.getCurrentOwner().toOfflinePlayer())) {
				if (permissions.contains(ShopManagementPermission.EDIT_MANAGERS)) {
					setRegularItem(new ClickeableItem(new ItemData("managers", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiManagers(), Mat.CHEST, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITMANAGERSNAME.getLine(), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							new ManageableEditManagersGUI<>((Manageable) shop, player, gui, pageIndex).open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				} else {
					setRegularItem(new ClickeableItem(new ItemData("managers", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiManagers(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_EDITMANAGERSNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
				}
			}
		}
		// add trades limit item
		if (shop.isCurrentOwnerAdmin() || ShopManagementPermission.EDIT_TRADES_LIMIT.getPermission().has(shop.getCurrentOwner().toOfflinePlayer())) {
			if (permissions.contains(ShopManagementPermission.EDIT_TRADES_LIMIT)) {
				setRegularItem(new ClickeableItem(new ItemData("trades_limit", SupremeShops.inst().getModuleManager().getShopManagementSlotsGuiTradesLimit(), EditorGUI.ICON_NUMBER, 1, SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITTRADESLIMITNAME.getLine(), null)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						player.closeInventory();
						// enter name in chat
						SSLocale.MSG_SUPREMESHOPS_CHANGESHOPTRADESLIMITINPUT.send(player);
						GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
							@Override
							public void onChat(Player player, String value) {
								// cancel
								if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
									ShopManagementGUI.this.open(player);
									return;
								}
								// not a number
								Integer limit = Utils.integerOrNull(value.trim());
								if (limit == null || limit < -1 || limit > Integer.MAX_VALUE) {
									GLocale.MSG_GENERIC_COMMAND_INVALIDINTPARAM.send(player, "{plugin}", SupremeShops.inst().getName(), "{parameter}", value);
									ShopManagementGUI.this.open(player);
									return;
								}
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
								// done
								shop.changeTradesLimit(limit == 0 ? -1 : limit);
								SSLocale.MSG_SUPREMESHOPS_CHANGEDSHOPTRADESLIMIT.send(player, "{limit}", limit == 0 ? -1 : limit);
								ShopManagementGUI.this.open(player);
							}
						});
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("trades_limit", SupremeShops.inst().getModuleManager().getShopManagementSlotsGuiTradesLimit(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITTRADESLIMITNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
		// add conditions item
		if (shop.isCurrentOwnerAdmin() || ShopManagementPermission.EDIT_TRADE_CONDITIONS.getPermission().has(shop.getCurrentOwner().toOfflinePlayer())) {
			if (permissions.contains(ShopManagementPermission.EDIT_TRADE_CONDITIONS)) {
				setRegularItem(new ClickeableItem(new ItemData("trade_conditions", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiTradeConditions(), EditorGUI.ICON_TECHNICAL, 1, SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITTRADECONDITIONSNAME.getLine(), null)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						new ShopEditTradeConditionsGUI(shop, player, gui, pageIndex).open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("trade_conditions", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiTradeConditions(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITTRADECONDITIONSNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
		// add display name item
		if (shop.isCurrentOwnerAdmin() || ShopManagementPermission.SET_DISPLAY_NAME.getPermission().has(shop.getCurrentOwner().toOfflinePlayer())) {
			if (permissions.contains(ShopManagementPermission.SET_DISPLAY_NAME)) {
				setRegularItem(new ClickeableItem(new ItemData("display_name", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiDisplayName(), Mat.NAME_TAG, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITDISPLAYNAMENAME.getLine(), null)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						player.closeInventory();
						// enter name in chat
						SSLocale.MSG_SUPREMESHOPS_CHANGESHOPDISPLAYNAMEINPUT.send(player);
						GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
							@Override
							public void onChat(Player player, String value) {
								// cancel
								if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
									ShopManagementGUI.this.open(player);
									return;
								}
								// forbidden words
								List<String> forbidden = SupremeShops.inst().getConfiguration().getListFormatted("player_input_blacklist", Utils.emptyList());
								String[] words = Utils.format(value).toLowerCase().split(" ");
								for (String word : words) {
									for (String forb : forbidden) {
										int similarity = Utils.getLevenshteinSimilarity(word, forb);
										if (similarity < 3) {
											SSLocale.MSG_SUPREMESHOPS_BLACKLISTEDPLAYERINPUT.send(player, "{word}", word, "{similar}", forb);
											ShopManagementGUI.this.open(player);
											// sound
											if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
												SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
											}
											return;
										}
									}
								}
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
								// done
								shop.changeDisplayName(value);
								SSLocale.MSG_SUPREMESHOPS_CHANGEDSHOPDISPLAYNAME.send(player, "{name}", value);
								new ShopManagementGUI(shop, permissions, player, ShopManagementGUI.this.fromGUI, ShopManagementGUI.this.fromGUIPageIndex).open(player);// refresh the whole GUI since the display name changed (so the inventory name as well)
							}
						});
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("particle_pattern", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiDisplayName(), Mat.GRAY_STAINED_GLASS_PANE, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITDISPLAYNAMENAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
		// add particle pattern item
		if (shop instanceof ParticlePatternAssignable) {
			if (shop.isCurrentOwnerAdmin() || ShopManagementPermission.SET_PARTICLE_PATTERN.getPermission().has(shop.getCurrentOwner().toOfflinePlayer())) {
				if (permissions.contains(ShopManagementPermission.SET_PARTICLE_PATTERN)) {
					setRegularItem(new ClickeableItem(new ItemData("particle_pattern", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiParticlePatterns(), Mat.BLAZE_POWDER, 1, SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITPARTICLEPATTERNNAME.getLine(), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							new EditParticlePatternGUI((ParticlePatternAssignable) shop, player, gui, pageIndex).open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				} else {
					setRegularItem(new ClickeableItem(new ItemData("particle_pattern", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiParticlePatterns(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITPARTICLEPATTERNNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
				}
			}
		}
		// add take items edition item
		setRegularItem(new ClickeableItem(new ItemData("take_items", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiTakenObjects(), Mat.HOPPER, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITTAKEITEMSNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITTAKEITEMSLORE.getLines())) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				new ShopEditTakingGUI(shop, permissions, player, gui, pageIndex).open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// add give items edition item
		setRegularItem(new ClickeableItem(new ItemData("give_items", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiGivenObjects(), Mat.DROPPER, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITGIVEITEMSNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITGIVEITEMSLORE.getLines())) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				new ShopEditGivingGUI(shop, permissions, player, gui, pageIndex).open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// add rent button
		if (shop instanceof Rentable) {
			setRegularItem(new ClickeableItem(new ItemData("rent", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiRent(), Mat.CLOCK, 1, SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTLORE.getLines(shop.getMessageReplacers(true, false, player)))) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// left-click, see rent price
					if (clickType.equals(ClickType.LEFT)) {
						new SeeRentGUI((Rentable) shop, player, gui, pageIndex).open(player);
					}
					// right-click, pay rent (in advance ?)
					else if (clickType.equals(ClickType.RIGHT)) {
						new PayRentAmountSelectionGUI((Rentable) shop, player) {
							@Override
							protected void onSelect(Player player, int amount) {
								// ensure he still has everything needed
								for (TradeObject object : ((Rentable) shop).getRentPrice()) {
									double objectAm = object.getCustomAmount(player) * amount;
									if (object.getTradesForStock(object.getPlayerStock(player), objectAm) < amount) {
										object.sendHasntMessage(player, objectAm);
										if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
											SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
										}
										ShopManagementGUI.this.open(player);
										return;
									}
								}
								// pay
								for (TradeObject object : ((Rentable) shop).getRentPrice()) {
									object.take(player, object.getCustomAmount(player) * amount);
								}
								// change paid rents
								((Rentable) shop).changePaidRents(((Rentable) shop).getPaidRents() + amount);
								// done
								ShopManagementGUI.this.open(player);
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
							}
						}.open(player);
					}
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
		// add stop renting button
		if (shop instanceof Rentable) {
			if (permissions.contains(ShopManagementPermission.STOP_RENTING)) {
				setRegularItem(new ClickeableItem(new ItemData("stop_renting", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiStopRenting(), Mat.TNT_MINECART, 1, SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITSTOPRENTINGNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITSTOPRENTINGLORE.getLines())) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						GUI confirmGUI = new GUI(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_SHOPSTOPRENTINGCONFIRMGUINAME.getLine(), 9, GUI.SLOTS_0_TO_8);
						confirmGUI.setRegularItem(new ClickeableItem(new ItemData("confirm", 3, Mat.TNT_MINECART, 1, SSLocaleMisc.MISC_SUPREMESHOPS_SHOPSTOPRENTINGCONFIRMITEMNAME.getLine(), null)) {
							@Override
							public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
								((Rentable) shop).changeRent(null, 0, true);
								player.closeInventory();
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
							}
						});
						confirmGUI.setRegularItem(new ClickeableItem(new ItemData("cancel", 5, Mat.ARROW, 1, SSLocaleMisc.MISC_SUPREMESHOPS_SHOPSTOPRENTINGCANCELITEMNAME.getLine(), null)) {
							@Override
							public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
								ShopManagementGUI.this.open(player);
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
							}
						});
						confirmGUI.open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("destroy", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiStopRenting(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITSTOPRENTINGNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
		// add destroy item
		else {
			if (permissions.contains(ShopManagementPermission.DESTROY)) {
				setRegularItem(new ClickeableItem(new ItemData("destroy", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiDestroy(), Mat.TNT_MINECART, 1, SSLocaleMisc.MISC_SUPREMESHOPS_SHOPDESTROYNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_SHOPDESTROYLORE.getLines())) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						GUI confirmGUI = new GUI(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_SHOPDESTROYCONFIRMGUINAME.getLine(), 9, GUI.SLOTS_0_TO_8);
						confirmGUI.setRegularItem(new ClickeableItem(new ItemData("confirm", 3, Mat.TNT_MINECART, 1, SSLocaleMisc.MISC_SUPREMESHOPS_SHOPDESTROYCONFIRMITEMNAME.getLine(), null)) {
							@Override
							public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
								getShop().destroy(DestroyCause.EDITION_DESTROY_BUTTON, true);
								player.closeInventory();
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
							}
						});
						confirmGUI.setRegularItem(new ClickeableItem(new ItemData("cancel", 5, Mat.ARROW, 1, SSLocaleMisc.MISC_SUPREMESHOPS_DESTROYCANCELITEMNAME.getLine(), null)) {
							@Override
							public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
								ShopManagementGUI.this.open(player);
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
							}
						});
						confirmGUI.open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("destroy", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiDestroy(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_SHOPDESTROYNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
		// add back item
		if (fromGUI != null) {
			setRegularItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(SupremeShops.inst().getModuleManager().getShopManagementSlotGuiBack())) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					fromGUI.open(player, fromGUIPageIndex);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
	}

	@Override
	protected boolean postFill() {
		return true;
	}

	private void updateInfoItem() {
		// build icon
		ItemStack iconStack = (shop instanceof Rentable ? SupremeShops.inst().getModuleManager().getPreviewGuiRentShopInfo() : (shop.isCurrentOwnerAdmin() ? SupremeShops.inst().getModuleManager().getPreviewGuiAdminShopInfo() : SupremeShops.inst().getModuleManager().getPreviewGuiShopInfo())).getItemStack(shop.getMessageReplacers(true, false, player)).clone();
		ItemData icon = new ItemData("info", SupremeShops.inst().getModuleManager().getShopManagementSlotInfo(), iconStack);
		// set icon
		setRegularItem(new ClickeableItem(icon));
	}

	private void updateCloseToggleItem() {
		if (shop.isCurrentOwnerAdmin() || ShopManagementPermission.SET_OPEN.getPermission().has(shop.getCurrentOwner().toOfflinePlayer())) {
			if (permissions.contains(ShopManagementPermission.SET_OPEN)) {
				setRegularItem(new ClickeableItem(new ItemData("closed", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiCloseToggle(), ((PlayerShop) shop).isOpen() ? Mat.GREEN_WOOL : Mat.RED_WOOL, 1, ((PlayerShop) shop).isOpen() ? SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITOPENEDITEMNAME.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITCLOSEDITEMNAME.getLine(), null)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						// too recent
						if (System.currentTimeMillis() - lastToggle < 500L) {
							return;
						}
						lastToggle = System.currentTimeMillis();
						// toggle
						shop.changeOpen(! shop.isOpen());
						updateCloseToggleItem();
						updateInfoItem();
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("closed", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiCloseToggle(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7" + Utils.unformat(shop.isOpen() ? SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITOPENEDITEMNAME.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITCLOSEDITEMNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
	}

	private void updateRemoteToggleItem() {
		if (!(shop instanceof MerchantShop)) {
			if (shop.isCurrentOwnerAdmin() || ShopManagementPermission.SET_REMOTE.getPermission().has(shop.getCurrentOwner().toOfflinePlayer())) {
				if (permissions.contains(ShopManagementPermission.SET_REMOTE)) {
					setRegularItem(new ClickeableItem(new ItemData("remote", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiRemoteToggle(), shop.isRemote() ? Mat.GREEN_WOOL : Mat.RED_WOOL, 1, shop.isRemote() ? SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITREMOTEITEMNAME.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITNONREMOTEITEMNAME.getLine(), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							// too recent
							if (System.currentTimeMillis() - lastToggle < 500L) {
								return;
							}
							lastToggle = System.currentTimeMillis();
							// max remote shops reached already
							if (!getShop().isCurrentOwnerAdmin()) {
								List<Shop> blockShops = SupremeShops.inst().getData().getShops().getElements(getShop().getCurrentOwner(), null, false, ElementRemotePolicy.MUST_BE);
								if (blockShops.size() >= SupremeShops.inst().getModuleManager().getMaxRemoteShops(getShop().getCurrentOwner().toOfflinePlayer())) {
									SSLocale.MSG_SUPREMESHOPS_MAXPHYSICALREMOTESHOPS.send(player);
									// sound
									if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
										SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
									}
								}
							}
							// set remote
							shop.changeRemote(!shop.isRemote());
							updateRemoteToggleItem();
							updateInfoItem();
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				} else {
					setRegularItem(new ClickeableItem(new ItemData("remote", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiRemoteToggle(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7" + Utils.unformat(shop.isRemote() ? SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITREMOTEITEMNAME.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_SHOPEDITNONREMOTEITEMNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
				}
			}
		}
	}

	private void updateDisplayItemsToggleItem() {
		if (shop instanceof BlockShop) {
			if (shop.isCurrentOwnerAdmin() || ShopManagementPermission.SET_DISPLAY_ITEMS.getPermission().has(shop.getCurrentOwner().toOfflinePlayer())) {
				if (permissions.contains(ShopManagementPermission.SET_DISPLAY_ITEMS)) {
					setRegularItem(new ClickeableItem(new ItemData("display_items", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiDisplayItemsToggle(), ((BlockShop) shop).getDisplayItems() ? Mat.GREEN_WOOL : Mat.RED_WOOL, 1, ((BlockShop) shop).getDisplayItems() ? SSLocaleMisc.MISC_SUPREMESHOPS_EDITDISPLAYITEMSITEMNAME.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_EDITNONDISPLAYITEMSITEMNAME.getLine(), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							// too recent
							if (System.currentTimeMillis() - lastToggle < 500L) {
								return;
							}
							lastToggle = System.currentTimeMillis();
							// set display items
							((BlockShop) shop).changeDisplayItems(!((BlockShop) shop).getDisplayItems());
							updateDisplayItemsToggleItem();
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				} else {
					setRegularItem(new ClickeableItem(new ItemData("display_items", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiDisplayItemsToggle(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7" + Utils.unformat(shop.isRemote() ? SSLocaleMisc.MISC_SUPREMESHOPS_EDITDISPLAYITEMSITEMNAME.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_EDITNONDISPLAYITEMSITEMNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
				}
			}
		}
	}

	private void updateAdminStockToggleItem() {
		if (shop.isCurrentOwnerAdmin()) {
			if (permissions.contains(ShopManagementPermission.SET_ADMIN_STOCK)) {
				setRegularItem(new ClickeableItem(new ItemData("admin_stock", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiAdminStockToggle(), shop.getAdminStock() ? Mat.GREEN_WOOL : Mat.RED_WOOL, 1, shop.getAdminStock() ? SSLocaleMisc.MISC_SUPREMESHOPS_EDITADMINSTOCKITEMNAME.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_EDITNONADMINSTOCKITEMNAME.getLine(), null)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						// too recent
						if (System.currentTimeMillis() - lastToggle < 500L) {
							return;
						}
						lastToggle = System.currentTimeMillis();
						// set display items
						shop.changeAdminStock(!shop.getAdminStock());
						updateAdminStockToggleItem();
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("admin_stock", SupremeShops.inst().getModuleManager().getShopManagementSlotGuiAdminStockToggle(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7" + Utils.unformat(shop.getAdminStock() ? SSLocaleMisc.MISC_SUPREMESHOPS_EDITADMINSTOCKITEMNAME.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_EDITNONADMINSTOCKITEMNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
	}

}
