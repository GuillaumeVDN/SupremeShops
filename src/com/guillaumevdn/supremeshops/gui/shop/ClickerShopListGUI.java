package com.guillaumevdn.supremeshops.gui.shop;

import java.util.List;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.messenger.Replacer;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.shop.ShopCreateEvent;
import com.guillaumevdn.supremeshops.data.ShopBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.GuiShop;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.ShopsSorter;
import com.guillaumevdn.supremeshops.util.ShopsSorter.SortCriteria;

public class ClickerShopListGUI extends FilledGUI {

	// base
	private UserInfo owner;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;
	private SortCriteria sortCriteria = SortCriteria.BY_NAME;
	private long lastChangedCriteria = System.currentTimeMillis();

	public ClickerShopListGUI(UserInfo owner, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_YOURSHOPLISTGUINAME.getLine(), 54, GUI.SLOTS_0_TO_44);
		this.owner = owner;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	public UserInfo getOwner() {
		return owner;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	public SortCriteria getSortCriteria() {
		return sortCriteria;
	}

	public long getLastChangedCriteria() {
		return lastChangedCriteria;
	}

	// fill
	@Override
	protected void fill() {
		// calculate contents
		List<Shop> content = SupremeShops.inst().getData().getShops().getElements(owner, null, false, ElementRemotePolicy.MIGHT_BE);
		// sort content
		content = new ShopsSorter(content, sortCriteria).getSortedList();
		// initialize content
		for (final Shop shop : content) {
			// build lore
			ItemData infoItem = shop instanceof Rentable ? SupremeShops.inst().getModuleManager().getPreviewGuiRentShopInfo() : (shop.getCurrentOwner() != null ? SupremeShops.inst().getModuleManager().getPreviewGuiShopInfo() : SupremeShops.inst().getModuleManager().getPreviewGuiAdminShopInfo());
			List<String> lore = infoItem.getLore() != null ? Utils.asList(infoItem.getLore()) : Utils.emptyList();
			lore.add("");
			lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_SHOPLISTLORECONTROLPREVIEWANDEDIT.getLines());
			Replacer replacer = new Replacer(shop.getMessageReplacers(true, false, player));
			// build icon
			ItemData icon = shop.getGuiIcon(player);
			icon.setId("shop_" + shop.getDataId());
			icon.setSlot(-1);
			icon.setAmount(1);
			icon.setName(replacer.apply(infoItem.getName()));
			icon.setLore(replacer.apply(lore));
			// build and set item
			setRegularItem(new ClickeableItem(icon) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// preview trade
					if (clickType.equals(ClickType.LEFT)) {
						new TradePreviewGUI(shop, null, 1, player, gui, pageIndex).open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				}
			});
		}
		// add shop item
		setPersistentItem(new ClickeableItem(new ItemData("add_shop", 46, Mat.BLAZE_ROD, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITADDSHOPNAME.getLine(), null)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// can't create
				if (!SSPerm.SUPREMESHOPS_SHOP_CREATE_GUI.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
					}
					return;
				}
				// max gui shops reached
				int amount = SupremeShops.inst().getData().getShops().getElements(new UserInfo(player), Utils.asList(GuiShop.class), false, ElementRemotePolicy.MIGHT_BE).size();
				int max = SupremeShops.inst().getModuleManager().getMaxGuiShops(player);
				if (amount >= max) {
					SSLocale.MSG_SUPREMESHOPS_MAXGUISHOPS.send(player);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
					}
					return;
				}
				// max shops reached
				amount = SupremeShops.inst().getData().getShops().getElements(new UserInfo(player), null, false, ElementRemotePolicy.MIGHT_BE).size();
				max = SupremeShops.inst().getModuleManager().getMaxShops(player);
				if (amount >= max) {
					SSLocale.MSG_SUPREMESHOPS_MAXSHOPS.send(player);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
					}
					return;
				}
				// create shop
				String shopId = "gui_" + UUID.randomUUID().toString().replace("-", "").substring(0, 5);
				final GuiShop shop = new GuiShop(shopId, owner);
				// isn't allowed
				if (!SupremeShops.inst().getModuleManager().canShopExist(player, shop.asFake(), true)) {
					return;
				}
				// event
				ShopCreateEvent ev = new ShopCreateEvent(shop, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					return;
				}
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
				// create
				SupremeShops.inst().getData().getShops().add(shop);
				SSLocale.MSG_SUPREMESHOPS_CREATESHOP.send(player, "{amount}", amount + 1, "{max}", max);
				SupremeShops.inst().pluginLog(shop, null, null, null, null, "Created shop");
				ClickerShopListGUI.this.open(player);
				return;
			}
		});
		// add sort item
		setPersistentItem(new ClickeableItem(new ItemData("sort", 50, Mat.COMMAND_BLOCK, 1, sortCriteria.getName().getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_SHOPLISTSORTCRITERIALORE.getLines())) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// too often
				if (System.currentTimeMillis() - lastChangedCriteria < 500L) {
					return;
				}
				// left-click : by name
				if (clickType.equals(ClickType.LEFT)) {
					sortCriteria = SortCriteria.BY_NAME;
					open(player, pageIndex);
				}
				// right-click : by unique buyers
				else if (clickType.equals(ClickType.RIGHT)) {
					sortCriteria = SortCriteria.BY_UNIQUE_BUYERS;
					open(player, pageIndex);
				}
				// shift + left-click : by ranking (server)
				else if (clickType.equals(ClickType.SHIFT_LEFT)) {
					sortCriteria = SortCriteria.BY_RANKING_SERVER;
					open(player, pageIndex);
				}
				// shift + right-click : by ranking (seller)
				else if (clickType.equals(ClickType.SHIFT_LEFT)) {
					sortCriteria = SortCriteria.BY_RANKING_SELLER;
					open(player, pageIndex);
				}
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// add back item
		if (fromGUI != null) {
			setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(52)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					fromGUI.open(player, fromGUIPageIndex);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
