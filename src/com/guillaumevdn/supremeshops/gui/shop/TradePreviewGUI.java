package com.guillaumevdn.supremeshops.gui.shop;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.gui.ShowcaseRowsGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.SSUser;
import com.guillaumevdn.supremeshops.gui.merchant.MerchantGUI;
import com.guillaumevdn.supremeshops.gui.rentable.management.RentableManagementGUI;
import com.guillaumevdn.supremeshops.gui.shop.management.ShopManagementGUI;
import com.guillaumevdn.supremeshops.module.itemvalue.ItemValueSetting;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.object.type.ObjectItem;
import com.guillaumevdn.supremeshops.module.object.type.ObjectVaultMoney;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.PlayerShop;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class TradePreviewGUI extends ShowcaseRowsGUI implements ShopGUI, MerchantGUI {

	// base
	private Shop shop;
	private Merchant merchant;
	private int trades, maxTrades;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;
	private Row takingRow, givingRow;
	private long lastSelectionClick = 0L;
	private long lastTradeClick = 0L;

	public TradePreviewGUI(Shop shop, Merchant merchant, int trades, Player player, final GUI fromGUI, final int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_PREVIEWGUINAME.getLine(shop.getMessageReplacers(true, false, player)), SupremeShops.inst().getModuleManager().getPreviewGuiSize(), GUI.getItemSlots(0, SupremeShops.inst().getModuleManager().getPreviewGuiSize() - 1), false, SupremeShops.inst().getModuleManager().getGuiClickSound());
		this.shop = shop;
		this.merchant = merchant;
		this.trades = trades;
		this.maxTrades = SupremeShops.inst().getModuleManager().getMaxShopTrades(player);
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
		// disable pages (we don't need them for this GUI)
		disablePages(true);
		createPage();
		// add rows
		addRow(takingRow = this.new Row(SupremeShops.inst().getModuleManager().getPreviewGuiTakenItemSlots()));
		addRow(givingRow = this.new Row(SupremeShops.inst().getModuleManager().getPreviewGuiGivenItemSlots()));
	}

	// get
	@Override
	public Shop getShop() {
		return shop;
	}

	@Override
	public Merchant getMerchant() {
		return merchant;
	}

	public int getTrades() {
		return trades;
	}

	public int getMaxTrades() {
		return maxTrades;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	public Row getTakingRow() {
		return takingRow;
	}

	public Row getGivingRow() {
		return givingRow;
	}

	// fill
	@Override
	protected void fill() {
		// info item
		ItemData infoItem = shop instanceof Rentable ? SupremeShops.inst().getModuleManager().getPreviewGuiRentShopInfo() : (shop.getCurrentOwner() != null ? SupremeShops.inst().getModuleManager().getPreviewGuiShopInfo() : SupremeShops.inst().getModuleManager().getPreviewGuiAdminShopInfo());
		setRegularItem(new ClickeableItem(new ItemData(infoItem.getId(), infoItem.getSlot(), infoItem.getItemStack(shop.getMessageReplacers(true, false, player)))));
		// add back item
		if (fromGUI != null) {
			setRegularItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getPreviewGuiBackItem()) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					fromGUI.open(player, fromGUIPageIndex);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
		// can edit rent shop
		final Set<ShopManagementPermission> managementPermissions;
		if (shop instanceof Rentable && SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
			// edit rent admin button
			if (SupremeShops.inst().getModuleManager().getPreviewGuiEditAdminRentItem() != null && !SupremeShops.inst().getModuleManager().getPreviewGuiEditAdminRentItem().getType().isAir()) {
				setRegularItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getPreviewGuiEditAdminRentItem()) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						new RentableManagementGUI((Rentable) shop, player, gui, pageIndex).open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			}
		}
		// can manage
		else if (!(managementPermissions = shop instanceof PlayerShop ? ((PlayerShop) shop).getManagementPermissions(player) : new HashSet<ShopManagementPermission>()).isEmpty()) {
			// edit button
			if (SupremeShops.inst().getModuleManager().getPreviewGuiEditItem() != null && !SupremeShops.inst().getModuleManager().getPreviewGuiEditItem().getType().isAir()) {
				setRegularItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getPreviewGuiEditItem()) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						new ShopManagementGUI(shop, managementPermissions, player, gui, pageIndex).open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			}
		}
		// can trade
		else {
			// trade button
			updateTradeButton();
			// trade amount selection max possible
			setRegularItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getPreviewGuiSelectMaxTrades()) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					tryToSetTradesAmountTo(shop.getPossibleTradesWith(player, player), 0);
				}
			});
			// trade amount modifier items
			for (int slot : SupremeShops.inst().getModuleManager().getPreviewGuiTradeAmountModifiers().keySet()) {
				int delta = SupremeShops.inst().getModuleManager().getPreviewGuiTradeAmountModifiers().get(slot);
				if (delta != 0 && Math.abs(delta) <= maxTrades) {
					if (delta < 0) {
						if (SupremeShops.inst().getModuleManager().getPreviewGuiTradeAmountModifyItemNegative() != null && !SupremeShops.inst().getModuleManager().getPreviewGuiTradeAmountModifyItemNegative().getType().isAir()) {
							setAmountModificationItem(new ItemData("trade_amount_modifier_negative_" + delta, slot, SupremeShops.inst().getModuleManager().getPreviewGuiTradeAmountModifyItemNegative().getItemStack("{delta}", -delta)), delta);
						}
					} else {
						if (SupremeShops.inst().getModuleManager().getPreviewGuiTradeAmountModifyItemPositive() != null && !SupremeShops.inst().getModuleManager().getPreviewGuiTradeAmountModifyItemPositive().getType().isAir()) {
							setAmountModificationItem(new ItemData("trade_amount_modifier_positive_" + delta, slot, SupremeShops.inst().getModuleManager().getPreviewGuiTradeAmountModifyItemPositive().getItemStack("{delta}", delta)), delta);
						}
					}
				}
			}
		}
		// content items
		for (ItemData content : SupremeShops.inst().getModuleManager().getPreviewGuiContent()) {
			if (getItemInSlot(0, content.getSlot()) == null) {
				setRegularItem(new ClickeableItem(content));
			}
		}
		// refresh rows
		refreshRows();
		// update first slot FFS
		updateFirstSlot();
	}

	@Override
	protected boolean postFill() {
		return true;
	}

	private void updateTradeButton() {
		if (SupremeShops.inst().getModuleManager().getPreviewGuiTradeItem() != null && !SupremeShops.inst().getModuleManager().getPreviewGuiTradeItem().getType().isAir()) {
			setRegularItem(new ClickeableItem(new ItemData(SupremeShops.inst().getModuleManager().getPreviewGuiTradeItem().getId(), SupremeShops.inst().getModuleManager().getPreviewGuiTradeItem().getSlot(), SupremeShops.inst().getModuleManager().getPreviewGuiTradeItem().getItemStack("{trades}", trades))) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// too recent
					if (System.currentTimeMillis() - lastTradeClick < 500L) {
						return;
					}
					// trade
					lastTradeClick = System.currentTimeMillis();
					shop.attemptTradesProcessing(player, trades, merchant);
				}
			});
		}
	}

	private void setAmountModificationItem(ItemData item, final int delta) {
		setRegularItem(new ClickeableItem(item) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				if ((delta >= 0 || trades > 1) && (delta <= 0 || trades < maxTrades)) {
					tryToSetTradesAmountTo(trades + delta, delta);
				}
			}
		});
	}

	public void refreshRows() {
		// clear rows
		lastSelectionClick = System.currentTimeMillis();
		takingRow.clear(false);
		givingRow.clear(false);
		// add taking objects
		Collection<TradeObject> taking = Utils.asList(shop.getObjects(Utils.asList(ObjectSide.TAKING), null));
		int objectNumber = 0;
		for (TradeObject object : taking) {
			++objectNumber;
			double calculatedAmount = object.calculateAmountForTrades(shop, trades, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), player);
			ItemData previewItem = object.getPreviewItem(calculatedAmount, player);
			// item : separate into multiple stacks
			if (object instanceof ObjectItem) {
				for (int splitAmount : previewItem.getSplitAmounts(SupremeShops.inst().getModuleManager().getSplitUnstackableItems() ? previewItem.getItemStack().getMaxStackSize() : 64)) {
					// set amount and total amount description
					ItemData clone = previewItem.cloneWithIdAndAmount(UUID.randomUUID().toString(), splitAmount);
					List<String> lore = clone.getLore() != null ? Utils.asList(clone.getLore()) : new ArrayList<String>();
					lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_TRADEPREVIEWITEMDESC.getLines("{current}", objectNumber, "{max}", taking.size(), "{amount}", previewItem.getAmount()));
					clone.setLore(lore);
					takingRow.addItem(new ClickeableItem(clone), false);
				}
			}
			// other : don't separate
			else {
				ItemData clone = previewItem.cloneWithId(UUID.randomUUID().toString());
				List<String> lore = clone.getLore() != null ? Utils.asList(clone.getLore()) : new ArrayList<String>();
				lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_TRADEPREVIEWITEMDESC.getLines("{current}", objectNumber, "{max}", taking.size(), "{amount}", previewItem.getAmount()));
				clone.setLore(lore);
				takingRow.addItem(new ClickeableItem(clone), false);
			}
		}
		// add giving objects
		Collection<TradeObject> giving = Utils.asList(shop.getObjects(Utils.asList(ObjectSide.GIVING), null));
		objectNumber = 0;
		for (TradeObject object : giving) {
			++objectNumber;
			double calculatedAmount = object.calculateAmountForTrades(shop, trades, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), player);
			ItemData previewItem = object.getPreviewItem(calculatedAmount, player);
			// item : separate into multiple stacks
			if (object instanceof ObjectItem) {
				int amount = previewItem.getAmount();
				int max = previewItem.getItemStack().getMaxStackSize();
				while (amount > 0) {
					// get amount
					int am;
					if (amount > max) {
						am = max;
						amount -= max;
					} else {
						am = amount;
						amount = 0;
					}
					// set amount and total amount description
					ItemData clone = previewItem.cloneWithIdAndAmount(UUID.randomUUID().toString(), am);
					List<String> lore = clone.getLore() != null ? Utils.asList(clone.getLore()) : new ArrayList<String>();
					lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_TRADEPREVIEWITEMDESC.getLines("{current}", objectNumber, "{max}", giving.size(), "{amount}", previewItem.getAmount()));
					clone.setLore(lore);
					givingRow.addItem(new ClickeableItem(clone), false);
				}
			}
			// other : don't separate
			else {
				ItemData clone = previewItem.cloneWithId(UUID.randomUUID().toString());
				List<String> lore = clone.getLore() != null ? Utils.asList(clone.getLore()) : new ArrayList<String>();
				lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_TRADEPREVIEWITEMDESC.getLines("{current}", objectNumber, "{max}", taking.size(), "{amount}", previewItem.getAmount()));
				clone.setLore(lore);
				givingRow.addItem(new ClickeableItem(clone), false);
			}
		}
		// update rows
		takingRow.update();
		givingRow.update();
	}

	public void tryToSetTradesAmountTo(int amount, int hasDelta) {
		// too recent
		if (System.currentTimeMillis() - lastSelectionClick < 500L) {
			return;
		}
		// change amount
		trades = amount;
		boolean error = false;
		// max shop trades
		if (shop.getTradesLimit() > 0 && SSUser.get(player).getShopTradeCount(shop) + trades > shop.getTradesLimit()) {
			trades = shop.getTradesLimit();
			SSLocale.MSG_SUPREMESHOPS_TRADESLIMITWOULDEXCEEDSHOP.send(player);
			error = true;
		}
		// max merchant trades
		if (merchant != null) {
			if (merchant.getTradesLimit() > 0 && SSUser.get(player).getMerchantTradeCount(merchant) + trades > merchant.getTradesLimit()) {
				trades = merchant.getTradesLimit();
				SSLocale.MSG_SUPREMESHOPS_TRADESLIMITWOULDEXCEEDMERCHANT.send(player);
				error = true;
			}
		}
		// max trades
		if (trades > maxTrades) {
			trades = maxTrades;
			error = true;
		}
		// less than 1 trade
		if (trades < 1) {
			trades = 1;
			if (hasDelta > 0) {
				SSLocale.MSG_SUPREMESHOPS_NOPOSSIBLETRADE.send(player);
				error = true;
			}
		}
		// error
		if (error) {
			if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
				SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
			}
		}
		// ok
		else {
			if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
				SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
			}
		}
		// update gui
		refreshRows();
		updateTradeButton();
		// notify unbalanced trade
		if (SupremeShops.inst().getModuleManager().getItemValueManager().getShopSetting(shop).equals(ItemValueSetting.RECOMMENDED)) {
			double shopGiving = 0d, shopTaking = 0d;
			for (TradeObject giving : shop.getObjects(Utils.asList(ObjectSide.GIVING), Utils.asList(ObjectType.VAULT_MONEY, ObjectType.ITEM))) {
				if (giving instanceof ObjectVaultMoney) {
					shopGiving += giving.calculateAmountForTrades(shop, 1, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), player);
				} else if (giving instanceof ObjectItem) {
					shopGiving += SupremeShops.inst().getModuleManager().getItemValueManager().calculateItemValue(((ObjectItem) giving).getItem(player), giving.getSide(), true);
				}
			}
			for (TradeObject taking : shop.getObjects(Utils.asList(ObjectSide.TAKING), Utils.asList(ObjectType.VAULT_MONEY, ObjectType.ITEM))) {
				if (taking instanceof ObjectVaultMoney) {
					shopTaking += taking.calculateAmountForTrades(shop, 1, SupremeShops.inst().getModuleManager().getTradeModifiers().values(), player);
				} else if (taking instanceof ObjectItem) {
					shopTaking += SupremeShops.inst().getModuleManager().getItemValueManager().calculateItemValue(((ObjectItem) taking).getItem(player), taking.getSide(), true);
				}
			}
			SupremeShops.inst().getModuleManager().getItemValueManager().notifyUnbalancedItemsValue(player, null, shopTaking, shopGiving);
		}
	}

}
