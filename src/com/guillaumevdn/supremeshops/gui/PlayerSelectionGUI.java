package com.guillaumevdn.supremeshops.gui;

import java.util.Comparator;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;

public abstract class PlayerSelectionGUI extends FilledGUI implements Listener {

	// static base
	private static final Comparator<Player> playerSorter = new Comparator<Player>() {
		@Override
		public int compare(Player o1, Player o2) {
			return o1.getName().compareTo(o2.getName());
		}
	};

	// base
	private Player who;

	public PlayerSelectionGUI(Player who) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_PLAYERTRADESELECTPLAYERGUINAME.getLine(), 54, GUI.SLOTS_0_TO_53);
		this.who = who;
		Bukkit.getPluginManager().registerEvents(this, SupremeShops.inst());
	}

	// methods
	@Override
	protected void fill() {
		for (final Player player : Utils.asSortedList(Utils.getOnlinePlayers(), playerSorter)) {
			if (!player.equals(who) && !SupremeShops.inst().getGeneralManager().getAskedPlayerTrades(player).contains(who)) {
				setRegularItem(new ClickeableItem(GUI.getPlayerHead(-1, "§6" + player.getName(), null)) {
					@Override
					public void onClick(Player clicker, ClickType clickType, GUI gui, int pageIndex) {
						clicker.closeInventory();
						onSelect(who, player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			}
		}
	}

	@Override
	protected boolean postFill() {
		return true;
	}

	@Override
	protected boolean onClose(List<Player> players, int pageIndex) {
		HandlerList.unregisterAll(this);
		return false;
	}

	// events
	@EventHandler
	public void event(PlayerJoinEvent event) {
		int pageIndex = getPageIndex(who.getOpenInventory().getTopInventory());
		if (pageIndex != -1) {
			open(who, pageIndex);
		}
	}

	@EventHandler
	public void event(PlayerQuitEvent event) {
		int pageIndex = getPageIndex(who.getOpenInventory().getTopInventory());
		if (pageIndex != -1) {
			open(who, pageIndex);
		}
	}

	// abstract methods
	protected abstract void onSelect(Player player, Player selected);

}
