package com.guillaumevdn.supremeshops.gui.rentable;

import com.guillaumevdn.supremeshops.module.rentable.Rentable;

public interface RentableGUI {

	Rentable getRentable();

}
