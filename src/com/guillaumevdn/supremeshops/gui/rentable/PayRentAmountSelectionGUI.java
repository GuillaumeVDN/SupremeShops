package com.guillaumevdn.supremeshops.gui.rentable;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorItem;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;

public abstract class PayRentAmountSelectionGUI extends FilledGUI implements RentableGUI {

	// amount
	private Rentable rentable;
	private Player player;
	private int value = 0;

	public PayRentAmountSelectionGUI(Rentable rentable, Player player) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_PAYRENTAMOUNTSELECTGUINAME.getLine(rentable.getMessageReplacers(false, false, player)), 27, GUI.SLOTS_0_TO_26);
		this.rentable = rentable;
		this.player = player;
	}

	// get
	@Override
	public Rentable getRentable() {
		return rentable;
	}
	
	public Player getPlayer() {
		return player;
	}

	public int getValue() {
		return value;
	}

	// fill GUI
	@Override
	protected void fill() {
		// add control items
		for (int i = 0; i < 9; ++i) fillDeltaItem(i + 1, 9 + i, Mat.GREEN_WOOL);
		for (int i = 0; i < 9; ++i) fillDeltaItem(-i - 1, 18 + i, Mat.RED_WOOL);
		// add separators around value item
		ItemStack stack = new ItemData(null, -1, Mat.GRAY_STAINED_GLASS_PANE, 1, " ", null).getItemStack();
		for (int i = 0; i < 9; ++i) {
			if (i != 4) setRegularItem(new ClickeableItem(new ItemData("separator_" + i, i, stack)));
		}
		// add value item
		updateValueItem();
	}

	@Override
	protected boolean postFill() {
		return true;
	}

	private void fillDeltaItem(final int delta, final int slot, final Mat icon) {
		setRegularItem(new EditorItem("control_item_" + (delta > 0 ? "add_" + delta : "take_" + Math.abs(delta)), slot, icon, (delta > 0 ? GLocale.GUI_GENERIC_EDITORNUMBERADD : GLocale.GUI_GENERIC_EDITORNUMBERTAKE).getLine("{amount}", Math.abs(delta)), GLocale.GUI_GENERIC_EDITORNUMBERADDTAKELORE.getLines()) {
			@Override
			protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
				// replace value
				value += delta;
				if (value < 0) {
					value = 0;
				} else {
					// more than he has
					boolean changed = false;
					for (TradeObject object : rentable.getRentPrice()) {
						int has = object.getTradesForStock(object.getPlayerStock(player), object.getCustomAmount(player));
						if (has < value) {
							value = has;
							changed = true;
							if (value <= 0) {
								value = 0;
								break;
							}
						}
					}
					if (changed) {
						SSLocale.MSG_SUPREMESHOPS_PAYRENTVALUESELECTDONTHAVE.send(player);
						if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
						}
					}
					// more than he can pay max (rent streak)
					else {
						if (rentable.getRentPeriodStreakLimit() > 0 && rentable.getPastPaidRents() + rentable.getPaidRents() + value > rentable.getRentPeriodStreakLimit()) {
							value = rentable.getRentPeriodStreakLimit() - rentable.getPastPaidRents() - rentable.getPaidRents();
							SSLocale.MSG_SUPREMESHOPS_PAYRENTVALUESELECTSTREAKLIMIT.send(player);
							if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
							}
						}
						// select sound
						else if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				}
				// update value item
				updateValueItem();
			}
		});
	}

	private void updateValueItem() {
		setRegularItem(new EditorItem("value", 4, Mat.NETHER_STAR, SSLocaleMisc.MISC_SUPREMESHOPS_PAYRENTAMOUNTSELECTVALUEITEMNAME.getLine("{amount}", value, "{plural}", Utils.getPlural(value)), SSLocaleMisc.MISC_SUPREMESHOPS_PAYRENTAMOUNTSELECTVALUEITEMLORE.getLines()) {
			@Override
			protected void onClick(Player player, ClickType clickType, int pageIndex) {
				player.closeInventory();
				onSelect(player, value);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
	}

	protected abstract void onSelect(Player player, int amount);

}
