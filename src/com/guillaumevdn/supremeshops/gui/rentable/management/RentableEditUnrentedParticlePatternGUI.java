package com.guillaumevdn.supremeshops.gui.rentable.management;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.rentable.RentableGUI;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;

public class RentableEditUnrentedParticlePatternGUI extends FilledGUI implements RentableGUI {

	// amount
	private Rentable rentable;
	private List<ParticlePattern> patterns;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public RentableEditUnrentedParticlePatternGUI(Rentable rentable, List<ParticlePattern> patterns, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITUNRENTEDPARTICLEPATTERNGUINAME.getLine(rentable.getMessageReplacers(false, false, player)), Utils.getInventorySize(patterns.size()), GUI.SLOTS_0_TO_44);
		this.rentable = rentable;
		this.patterns = patterns;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public Rentable getRentable() {
		return rentable;
	}

	public List<ParticlePattern> getPatterns() {
		return patterns;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill
	private long lastToggle = 0L;

	@Override
	protected void fill() {
		// add patterns
		for (final ParticlePattern pattern : patterns) {
			// build icon
			ItemData icon = pattern.getIcon().cloneWithId("pattern_" + pattern.hashCode());
			List<String> lore = icon.getLore() != null ? icon.getLore() : Utils.emptyList();
			lore.addAll(pattern.getId().equalsIgnoreCase(rentable.getUnrentedParticlePatternId()) ? SSLocaleMisc.MISC_SUPREMESHOPS_EDITPARTICLEPATTERNSELECTEDLORE.getLines() : SSLocaleMisc.MISC_SUPREMESHOPS_EDITPARTICLEPATTERNNOTSELECTEDLORE.getLines());
			icon.setLore(lore);
			// set icon
			setRegularItem(new ClickeableItem(icon) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// right-click : select
					if (clickType.equals(ClickType.RIGHT)) {
						if (!pattern.getId().equalsIgnoreCase(rentable.getUnrentedParticlePatternId())) {
							// too recent
							if (System.currentTimeMillis() - lastToggle < 500L) {
								return;
							}
							lastToggle = System.currentTimeMillis();
							// select
							rentable.changeUnrentedParticlePattern(pattern.getId());
							RentableEditUnrentedParticlePatternGUI.this.open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					}
					// shift + right-click : remove
					else if (clickType.equals(ClickType.SHIFT_RIGHT)) {
						if (pattern.getId().equalsIgnoreCase(rentable.getUnrentedParticlePatternId())) {
							// too recent
							if (System.currentTimeMillis() - lastToggle < 500L) {
								return;
							}
							lastToggle = System.currentTimeMillis();
							// select
							rentable.changeUnrentedParticlePattern(null);
							RentableEditUnrentedParticlePatternGUI.this.open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					}
				}
			});
		}
		// add back item
		setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(getSize() == 54 ? 52 : getSize() - 1)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				fromGUI.open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
