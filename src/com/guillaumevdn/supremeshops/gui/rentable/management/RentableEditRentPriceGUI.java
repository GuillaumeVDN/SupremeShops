package com.guillaumevdn.supremeshops.gui.rentable.management;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.rentable.RentableGUI;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;

public class RentableEditRentPriceGUI extends FilledGUI implements RentableGUI {

	// amount
	private Rentable rentable;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public RentableEditRentPriceGUI(Rentable rentable, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITRENTPRICEGUINAME.getLine(rentable.getMessageReplacers(false, false, player)), 54, GUI.SLOTS_0_TO_44);
		this.rentable = rentable;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public Rentable getRentable() {
		return rentable;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill GUI
	@Override
	protected void fill() {
		// add contents
		for (final TradeObject object : rentable.getRentPrice()) {
			// add stock in lore
			ItemStack stack = object.getPreviewItem(object.getCustomAmount(null), null).getItemStack();
			ItemMeta meta = stack.getItemMeta();
			List<String> lore;
			if (meta.getLore() == null || meta.getLore().isEmpty()) {
				lore = new ArrayList<String>();
			} else {
				lore = Utils.asList(meta.getLore());
				lore.add(" ");
			}
			// add controls in lore
			lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_EDITCONTROLREMOVEOBJECTLORE.getLines());
			// set item
			meta.setLore(lore);
			stack.setItemMeta(meta);
			setRegularItem(new ClickeableItem(new ItemData("wage_" + object.hashCode(), -1, stack)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// shift + right-click : remove item
					if (clickType.equals(ClickType.SHIFT_RIGHT)) {
						rentable.removeRentPrice(object);
						RentableEditRentPriceGUI.this.open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
						// log
						SupremeShops.inst().pluginLog(rentable, player, null, "Removed  " + object.getSide().name() + " object " + object.toString() + " from rent price");
					}
				}
			});
		}
		// add object item
		setPersistentItem(new ClickeableItem(new ItemData("add_object", 47, Mat.BLAZE_ROD, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITADDOBJECTNAME.getLine(), null)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// select type
				int size = Utils.getInventorySize(ObjectType.values().size());
				GUI typeGui = new GUI(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITADDOBJECTSELECTTYPE.getLine(), size, GUI.getRegularItemSlots(0, size - 1));
				for (final ObjectType type : ObjectType.values()) {
					// can't add
					if (!type.getLogic().canAddToRentableRentPrice(player, rentable)) continue;
					// attempt to add
					typeGui.setRegularItem(new ClickeableItem(new ItemData("type_" + type.getId(), -1, type.getIcon(), 1, type.getName(), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							type.getLogic().attemptToAddToRentableRentPrice(player, rentable, RentableEditRentPriceGUI.this);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				}
				typeGui.open(player);
			}
		});
		// add back item
		setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(52)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				fromGUI.open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
