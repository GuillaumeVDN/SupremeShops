package com.guillaumevdn.supremeshops.gui.rentable.management;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.ChatInput;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.rentable.RentableGUI;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.DestroyCause;
import com.guillaumevdn.supremeshops.util.RentPeriod;
import com.guillaumevdn.supremeshops.util.particlepattern.ParticlePattern;

public class RentableManagementGUI extends FilledGUI implements RentableGUI {

	// base
	private Rentable rentable;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public RentableManagementGUI(Rentable rentable, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_RENTMERCHANTMANAGEMENTGUINAME.getLine(rentable.getMessageReplacers(false, false, player)), Utils.getInventorySize(SupremeShops.inst().getModuleManager().getRentableManagementGuiSize()), GUI.SLOTS_0_TO_53);
		this.rentable = rentable;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public Rentable getRentable() {
		return rentable;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill GUI
	private long lastToggle = 0L;

	@Override
	protected void fill() {
		// add info item
		updateInfoItem();
		// add rentable toggle item
		updateRentableToggleItem();
		// add rent price item
		setRegularItem(new ClickeableItem(new ItemData("rent_price", SupremeShops.inst().getModuleManager().getRentableManagementGuiSlotRentPrice(), Mat.CHEST, 1, SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTPRICENAME.getLine(), null)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				new RentableEditRentPriceGUI(rentable, player, gui, pageIndex).open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// add rent period item
		setRegularItem(new ClickeableItem(new ItemData("rent_period", SupremeShops.inst().getModuleManager().getRentableManagementGuiSlotRentPeriod(), Mat.CLOCK, 1, SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODLORE.getLines("{current}", rentable.getRentPeriod().getName().getLine()))) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// select type
				int size = Utils.getInventorySize(RentPeriod.values().length);
				GUI typeGui = new GUI(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITRENTPRICEPERIODSELECTTYPE.getLine(), size, GUI.getRegularItemSlots(0, size - 1));
				for (final RentPeriod type : RentPeriod.values()) {
					typeGui.setRegularItem(new ClickeableItem(new ItemData("type_" + type.name(), -1, Mat.CLOCK, 1, type.getName().getLine(), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							// set delay
							rentable.changeRentPeriod(type);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
							// notify and open GUI
							SSLocale.MSG_SUPREMESHOPS_RENTABLECHANGEDRENTPERIODTYPE.send(player, "{period}", type.getName().getLine());
							RentableManagementGUI.this.open(player);
						}
					});
				}
				typeGui.open(player);
			}
		});
		// add rent period streak limit item
		setRegularItem(new ClickeableItem(new ItemData("rent_period_streak_limit", SupremeShops.inst().getModuleManager().getRentableManagementGuiSlotRentPeriodStreakLimit(), Mat.CLOCK, 1, SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODSTREAKLIMITNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODSTREAKLIMITLORE.getLines("{current}", rentable.getRentPeriodStreakLimit()))) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				player.closeInventory();
				SSLocale.MSG_SUPREMESHOPS_RENTPERIODSTREAKLIMITINPUT.send(player);
				GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
					@Override
					public void onChat(final Player player, String value) {
						// cancel
						if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
							if (fromGUI != null) fromGUI.open(player);
							return;
						}
						// not a number
						final Integer limit = Utils.integerOrNull(value);
						if (limit == null || limit < -1) {
							GLocale.MSG_GENERIC_COMMAND_INVALIDINTPARAM.send(player, "{plugin}", SupremeShops.inst().getName(), "{parameter}", value);
							if (fromGUI != null) fromGUI.open(player);
							return;
						}
						// set streak limit
						rentable.changeRentPeriodStreakLimit(limit == 0 ? -1 : limit);
						// notify and open GUI
						SSLocale.MSG_SUPREMESHOPS_RENTABLECHANGEDRENTPERIODSTREAKLIMIT.send(player, "{limit}", limit == 0 ? -1 : limit);
						RentableManagementGUI.this.open(player);
					}
				});
			}
		});
		// add rent period streak limit delay item
		setRegularItem(new ClickeableItem(new ItemData("rent_period_streak_limit_delay", SupremeShops.inst().getModuleManager().getRentableManagementGuiSlotRentPeriodStreakLimitDelay(), Mat.CLOCK, 1, SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODSTREAKLIMITDELAYNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTPERIODSTREAKLIMITDELAYLORE.getLines("{current}", rentable.getRentPeriodStreakLimitDelay()))) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				player.closeInventory();
				SSLocale.MSG_SUPREMESHOPS_RENTPERIODSTREAKLIMITDELAYINPUT.send(player);
				GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
					@Override
					public void onChat(final Player player, String value) {
						// cancel
						if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
							if (fromGUI != null) fromGUI.open(player);
							return;
						}
						// not a number
						final Integer limit = Utils.integerOrNull(value);
						if (limit == null || limit < -1) {
							GLocale.MSG_GENERIC_COMMAND_INVALIDINTPARAM.send(player, "{plugin}", SupremeShops.inst().getName(), "{parameter}", value);
							if (fromGUI != null) fromGUI.open(player);
							return;
						}
						// set streak limit
						rentable.changeRentPeriodStreakLimitDelay(limit == 0 ? -1 : limit);
						// notify and open GUI
						SSLocale.MSG_SUPREMESHOPS_RENTABLECHANGEDRENTPERIODSTREAKLIMITDELAY.send(player, "{delay}", limit == 0 ? -1 : Utils.formatDurationSeconds(limit * 24 * 60 * 60));
						RentableManagementGUI.this.open(player);
					}
				});
			}
		});
		// add rent conditions item
		setRegularItem(new ClickeableItem(new ItemData("rent_conditions", SupremeShops.inst().getModuleManager().getRentableManagementGuiSlotRentConditions(), EditorGUI.ICON_CONDITION, 1, SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTONDITIONSNAME.getLine(), null)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				new RentableEditRentConditionsGUI(rentable, player, gui, pageIndex).open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// add unrented particle pattern id
		setRegularItem(new ClickeableItem(new ItemData("unrented_particle_pattern", SupremeShops.inst().getModuleManager().getRentableManagementGuiSlotUnrentedParticlePattern(), Mat.BLAZE_POWDER, 1, SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITUNRENTEDPARTICLEPATTERNNAME.getLine(), null)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				List<ParticlePattern> patterns = new ArrayList<ParticlePattern>();
				for (ParticlePattern pattern : SupremeShops.inst().getModuleManager().getParticlePatterns().values()) {
					if (pattern.getAvailability().equals(rentable.getRentableParticlePatternAvailability())) {
						patterns.add(pattern);	
					}
				}
				new RentableEditUnrentedParticlePatternGUI(rentable, patterns, player, gui, pageIndex).open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// add force stop renting item
		if (rentable.isRented()) {
			setRegularItem(new ClickeableItem(new ItemData("force_stop_renting", SupremeShops.inst().getModuleManager().getRentableManagementGuiSlotForceStopRenting(), Mat.GOLDEN_SWORD, 1, SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITFORCESTOPRENTINGNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITFORCESTOPRENTINGLORE.getLines())) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					GUI confirmGUI = new GUI(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEFORCESTOPRENTINGCONFIRMGUINAME.getLine(), 9, GUI.SLOTS_0_TO_8);
					confirmGUI.setRegularItem(new ClickeableItem(new ItemData("confirm", 3, Mat.TNT_MINECART, 1, SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEFORCESTOPRENTINGCONFIRMITEMNAME.getLine(), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							rentable.changeRent(null, 0, true);
							player.closeInventory();
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
					confirmGUI.setRegularItem(new ClickeableItem(new ItemData("cancel", 5, Mat.ARROW, 1, SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEFORCESTOPRENTINGCANCELITEMNAME.getLine(), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							RentableManagementGUI.this.open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
					confirmGUI.open(player);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
		// add destroy item
		setRegularItem(new ClickeableItem(new ItemData("destroy", SupremeShops.inst().getModuleManager().getRentableManagementGuiSlotDestroy(), Mat.TNT_MINECART, 1, (rentable instanceof Shop ? SSLocaleMisc.MISC_SUPREMESHOPS_SHOPDESTROYNAME : SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTDESTROYNAME).getLine(), (rentable instanceof Shop ? SSLocaleMisc.MISC_SUPREMESHOPS_SHOPDESTROYLORE : SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTDESTROYLORE).getLines())) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				GUI confirmGUI = new GUI(SupremeShops.inst(), (rentable instanceof Shop ? SSLocaleMisc.MISC_SUPREMESHOPS_SHOPDESTROYCONFIRMGUINAME : SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTDESTROYCONFIRMGUINAME).getLine(), 9, GUI.SLOTS_0_TO_8);
				confirmGUI.setRegularItem(new ClickeableItem(new ItemData("confirm", 3, Mat.TNT_MINECART, 1, (rentable instanceof Shop ? SSLocaleMisc.MISC_SUPREMESHOPS_SHOPDESTROYCONFIRMITEMNAME : SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTDESTROYCONFIRMITEMNAME).getLine(), null)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						if (rentable instanceof Shop) {
							((Shop) rentable).destroy(DestroyCause.EDITION_DESTROY_BUTTON, true);
						} else if (rentable instanceof Merchant) {
							((Merchant) rentable).destroy(DestroyCause.EDITION_DESTROY_BUTTON, true);
						}
						player.closeInventory();
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
				confirmGUI.setRegularItem(new ClickeableItem(new ItemData("cancel", 5, Mat.ARROW, 1, SSLocaleMisc.MISC_SUPREMESHOPS_DESTROYCANCELITEMNAME.getLine(), null)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						RentableManagementGUI.this.open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
				confirmGUI.open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// add back item
		if (fromGUI != null) {
			setRegularItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(SupremeShops.inst().getModuleManager().getRentableManagementGuiSlotBack())) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					fromGUI.open(player, fromGUIPageIndex);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
	}

	@Override
	protected boolean postFill() {
		return true;
	}

	private void updateInfoItem() {
		// build icon
		ItemStack iconStack = (rentable instanceof Shop ? SupremeShops.inst().getModuleManager().getPreviewGuiRentShopInfo() : SupremeShops.inst().getModuleManager().getPreviewGuiRentMerchantInfo()).getItemStack(rentable.getMessageReplacers(true, false, player));
		ItemData icon = new ItemData("info", SupremeShops.inst().getModuleManager().getMerchantManagementSlotInfo(), iconStack);
		// set icon
		setRegularItem(new ClickeableItem(icon));
	}

	private void updateRentableToggleItem() {
		if (!rentable.isRented()) {
			setRegularItem(new ClickeableItem(new ItemData("rentable", SupremeShops.inst().getModuleManager().getRentableManagementGuiSlotRentableToggle(), rentable.isRentable() ? Mat.GREEN_WOOL : Mat.RED_WOOL, 1, rentable.isRentable() ? SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTABLEITEMNAME.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITNOTRENTABLEITEMNAME.getLine(), null)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// too recent
					if (System.currentTimeMillis() - lastToggle < 500L) {
						return;
					}
					lastToggle = System.currentTimeMillis();
					// toggle
					rentable.changeRentable(!rentable.isRentable());
					updateRentableToggleItem();
					updateInfoItem();
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
	}

}
