package com.guillaumevdn.supremeshops.gui.merchant.management;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.merchant.MerchantGUI;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchant;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchantSkin;

public class MerchantEditSkinGUI extends FilledGUI implements MerchantGUI {

	// amount
	private NpcMerchant merchant;
	private List<NpcMerchantSkin> skins;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public MerchantEditSkinGUI(NpcMerchant merchant, List<NpcMerchantSkin> skins, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITSKINGUINAME.getLine(merchant.getMessageReplacers(false, false, player)), Utils.getInventorySize(skins.size()), GUI.SLOTS_0_TO_44);
		this.merchant = merchant;
		this.skins = skins;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public NpcMerchant getMerchant() {
		return merchant;
	}

	public List<NpcMerchantSkin> getPatterns() {
		return skins;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill
	private long lastToggle = 0L;

	@Override
	protected void fill() {
		// add skins
		for (final NpcMerchantSkin skin : skins) {
			// build icon
			ItemData icon = skin.getIcon().cloneWithId("skin_" + skin.hashCode());
			List<String> lore = icon.getLore() != null ? icon.getLore() : Utils.emptyList();
			lore.addAll(skin.getId().equalsIgnoreCase(merchant.getSkinId()) ? SSLocaleMisc.MISC_SUPREMESHOPS_EDITSKINSELECTEDLORE.getLines() : SSLocaleMisc.MISC_SUPREMESHOPS_EDITSKINNOTSELECTEDLORE.getLines());
			icon.setLore(lore);
			// set icon
			setRegularItem(new ClickeableItem(icon) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// right-click : select
					if (clickType.equals(ClickType.RIGHT)) {
						if (!skin.getId().equalsIgnoreCase(merchant.getSkinId())) {
							// too recent
							if (System.currentTimeMillis() - lastToggle < 500L) {
								return;
							}
							lastToggle = System.currentTimeMillis();
							// select
							merchant.changeSkin(skin.getId());
							MerchantEditSkinGUI.this.open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					}
					// shift + right-click : remove
					else if (clickType.equals(ClickType.SHIFT_RIGHT)) {
						if (skin.getId().equalsIgnoreCase(merchant.getSkinId())) {
							// too recent
							if (System.currentTimeMillis() - lastToggle < 500L) {
								return;
							}
							lastToggle = System.currentTimeMillis();
							// select
							merchant.changeSkin(null);
							MerchantEditSkinGUI.this.open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					}
				}
			});
		}
		// add back item
		setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(getSize() == 54 ? 52 : getSize() - 1)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				fromGUI.open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
