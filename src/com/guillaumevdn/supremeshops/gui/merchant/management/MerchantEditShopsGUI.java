package com.guillaumevdn.supremeshops.gui.merchant.management;

import java.util.List;
import java.util.Set;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.messenger.Replacer;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.shop.ShopCreateEvent;
import com.guillaumevdn.supremeshops.data.ShopBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.gui.merchant.MerchantGUI;
import com.guillaumevdn.supremeshops.gui.shop.management.ShopManagementGUI;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.BlockShop;
import com.guillaumevdn.supremeshops.module.shop.GuiShop;
import com.guillaumevdn.supremeshops.module.shop.MerchantShop;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class MerchantEditShopsGUI extends FilledGUI implements MerchantGUI {

	// amount
	private Merchant merchant;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public MerchantEditShopsGUI(Merchant merchant, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITMERCHANTSHOPSGUINAME.getLine(merchant.getMessageReplacers(false, false, player)), 54, GUI.SLOTS_0_TO_53);
		this.merchant = merchant;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public Merchant getMerchant() {
		return merchant;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill
	private long lastToggle = 0L;

	@Override
	protected void fill() {
		// add merchant shops
		for (Shop s : merchant.getShops(Utils.asList(MerchantShop.class), false)) {
			final MerchantShop shop = (MerchantShop) s;
			// build name and lore
			ItemData infoItem = shop instanceof Rentable ? SupremeShops.inst().getModuleManager().getPreviewGuiRentShopInfo() : (shop.getCurrentOwner() != null ? SupremeShops.inst().getModuleManager().getPreviewGuiShopInfo() : SupremeShops.inst().getModuleManager().getPreviewGuiAdminShopInfo());
			List<String> lore = infoItem.getLore() != null ? Utils.asList(infoItem.getLore()) : Utils.emptyList();
			lore.add("");
			lore.addAll(!shop.getManagementPermissions(player).isEmpty() ? SSLocaleMisc.MISC_SUPREMESHOPS_EDITMERCHANTSHOPCANEDITLINELORE.getLines() : SSLocaleMisc.MISC_SUPREMESHOPS_EDITMERCHANTSHOPCANTEDITLINELORE.getLines());
			lore = new Replacer(shop.getMessageReplacers(true, false, player)).apply(lore);
			// build icon
			ItemData icon = shop.getGuiIcon(player);
			icon.setId("shop_" + shop.getDataId());
			icon.setSlot(-1);
			icon.setAmount(1);
			icon.setName(infoItem.getName());
			icon.setLore(infoItem.getLore());
			// build and set item
			setRegularItem(new ClickeableItem(icon) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// right-click : edit
					if (clickType.equals(ClickType.RIGHT)) {
						Set<ShopManagementPermission> permissions = shop.getManagementPermissions(player);
						if (!permissions.isEmpty()) {
							new ShopManagementGUI(shop, permissions, player, gui, pageIndex).open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					}
				}
			});
		}
		// add linked shops
		for (final Shop shop : SupremeShops.inst().getData().getShops().getElements(merchant.getCurrentOwner(), Utils.asList(BlockShop.class, GuiShop.class), false, com.guillaumevdn.supremeshops.data.ShopBoard.ElementRemotePolicy.MUST_BE)) {
			// build name and lore
			ItemData infoItem = shop instanceof Rentable ? SupremeShops.inst().getModuleManager().getPreviewGuiRentShopInfo() : (shop.getCurrentOwner() != null ? SupremeShops.inst().getModuleManager().getPreviewGuiShopInfo() : SupremeShops.inst().getModuleManager().getPreviewGuiAdminShopInfo());
			List<String> lore = infoItem.getLore() != null ? Utils.asList(infoItem.getLore()) : Utils.emptyList();
			lore.add("");
			lore.addAll(merchant.hasShop(shop) ? SSLocaleMisc.MISC_SUPREMESHOPS_EDITMERCHANTSHOPLINKEDLINELORE.getLines() : SSLocaleMisc.MISC_SUPREMESHOPS_EDITMERCHANTSHOPNOTLINKEDLINELORE.getLines());
			Replacer replacer = new Replacer(shop.getMessageReplacers(true, false, player));
			// build icon
			ItemData icon = shop.getGuiIcon(player);
			icon.setId("shop_" + shop.getDataId());
			icon.setSlot(-1);
			icon.setAmount(1);
			icon.setName(replacer.apply(infoItem.getName()));
			icon.setLore(replacer.apply(infoItem.getLore()));
			// build and set item
			setRegularItem(new ClickeableItem(icon) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// right-click : toggle
					if (clickType.equals(ClickType.RIGHT)) {
						// too recent
						if (System.currentTimeMillis() - lastToggle < 500L) {
							return;
						}
						lastToggle = System.currentTimeMillis();
						// max linked shops per merchant reached
						int amount = merchant.getShopsIds().size();
						int max = merchant.isCurrentOwnerAdmin() ? Integer.MAX_VALUE : SupremeShops.inst().getModuleManager().getMaxMerchantsLinkedShops(merchant.getCurrentOwner().toOfflinePlayer());
						if (amount >= max) {
							SSLocale.MSG_SUPREMESHOPS_MAXLINKEDMERCHANTSHOPS.send(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
							}
							return;
						}
						// select
						if (merchant.hasShop(shop)) {
							merchant.removeShop(shop, true);
						} else {
							merchant.addShop(shop);
						}
						MerchantEditShopsGUI.this.open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				}
			});
		}
		// create shop item
		setPersistentItem(new ClickeableItem(new ItemData("create_shop", 46, Mat.BLAZE_ROD, 1, SSLocaleMisc.MISC_SUPREMESHOPS_EDITADDMERCHANTSHOPNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITADDMERCHANTSHOPLORE.getLines())) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// can't create
				if (!SSPerm.SUPREMESHOPS_SHOP_CREATE_MERCHANT.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
					}
					return;
				}
				// max merchant shops reached
				int amount = SupremeShops.inst().getData().getShops().getElements(new UserInfo(player), Utils.asList(MerchantShop.class), false, ElementRemotePolicy.MIGHT_BE).size();
				int max = merchant.isCurrentOwnerAdmin() ? Integer.MAX_VALUE : SupremeShops.inst().getModuleManager().getMaxMerchantShops(merchant.getCurrentOwner().toOfflinePlayer());
				if (amount >= max) {
					SSLocale.MSG_SUPREMESHOPS_MAXMERCHANTSHOPS.send(player);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
					}
					return;
				}
				// max created shops per merchant reached
				amount = merchant.getShops(Utils.asList(MerchantShop.class), false).size();
				max = merchant.isCurrentOwnerAdmin() ? Integer.MAX_VALUE : SupremeShops.inst().getModuleManager().getMaxMerchantsCreatedShops(player);
				if (amount >= max) {
					SSLocale.MSG_SUPREMESHOPS_MAXCREATEDMERCHANTSHOPS.send(player);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
					}
					return;
				}
				// max shops reached
				amount = SupremeShops.inst().getData().getShops().getElements(new UserInfo(player), null, false, ElementRemotePolicy.MIGHT_BE).size();
				max = SupremeShops.inst().getModuleManager().getMaxShops(player);
				if (amount >= max) {
					SSLocale.MSG_SUPREMESHOPS_MAXSHOPS.send(player);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
					}
					return;
				}
				// create shop
				String shopId = "merchant_" + merchant.getId() + "_" + UUID.randomUUID().toString().replace("-", "").substring(0, 5);
				final MerchantShop shop = new MerchantShop(shopId, merchant.getCurrentOwner(), merchant.getId());
				// isn't allowed
				if (!SupremeShops.inst().getModuleManager().canShopExist(player, shop.asFake(), true)) {
					SSLocale.MSG_SUPREMESHOPS_SHOPCANTEXIST.send(player);
					return;
				}
				// event
				ShopCreateEvent ev = new ShopCreateEvent(shop, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					return;
				}
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
				// create
				SupremeShops.inst().getData().getShops().add(shop);
				SSLocale.MSG_SUPREMESHOPS_CREATESHOP.send(player, "{amount}", amount + 1, "{max}", max);
				SupremeShops.inst().pluginLog(shop, null, null, null, null, "Created shop");
				// link to merchant
				if (!merchant.hasShop(shop)) {
					merchant.addShop(shop);
				}
				// done
				MerchantEditShopsGUI.this.open(player);
				return;
			}
		});
		// add back item
		setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(52)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				fromGUI.open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
