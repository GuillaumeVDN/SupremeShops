package com.guillaumevdn.supremeshops.gui.merchant.management;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.ChatInput;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.gui.manageable.management.ManageableEditManagersGUI;
import com.guillaumevdn.supremeshops.gui.merchant.MerchantGUI;
import com.guillaumevdn.supremeshops.gui.particlepatternassignable.EditParticlePatternGUI;
import com.guillaumevdn.supremeshops.gui.rentable.PayRentAmountSelectionGUI;
import com.guillaumevdn.supremeshops.gui.rentable.SeeRentGUI;
import com.guillaumevdn.supremeshops.module.manageable.MerchantManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchant;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchantEquipment;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchantSkin;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.particlepatternassignable.ParticlePatternAssignable;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.util.DestroyCause;

public class MerchantManagementGUI extends FilledGUI implements MerchantGUI {

	// base
	private Merchant merchant;
	private Set<MerchantManagementPermission> permissions;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public MerchantManagementGUI(Merchant merchant, Set<MerchantManagementPermission> permissions, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTMANAGEMENTGUINAME.getLine(merchant.getMessageReplacers(false, false, player)), Utils.getInventorySize(SupremeShops.inst().getModuleManager().getMerchantManagementGuiSize()), GUI.SLOTS_0_TO_53);
		this.merchant = merchant;
		this.permissions = permissions;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public Merchant getMerchant() {
		return merchant;
	}

	public Set<MerchantManagementPermission> getPermissions() {
		return permissions;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill GUI
	private long lastToggle = 0L;

	@Override
	protected void fill() {
		// add info item
		updateInfoItem();
		// add open/close item
		updateCloseToggleItem();
		// add remote/non-remote item
		updateRemoteToggleItem();
		// add shops item
		if (permissions.contains(MerchantManagementPermission.EDIT_SHOPS)) {
			setRegularItem(new ClickeableItem(new ItemData("shops", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiShops(), Mat.ENDER_CHEST, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITSHOPSNAME.getLine(), null)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					new MerchantEditShopsGUI(merchant, player, gui, pageIndex).open(player);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		} else {
			setRegularItem(new ClickeableItem(new ItemData("shops", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiShops(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITSHOPSNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
		}
		// add trades limit item
		if (merchant.isCurrentOwnerAdmin() || MerchantManagementPermission.EDIT_TRADES_LIMIT.getPermission().has(merchant.getCurrentOwnerPlayer())) {
			if (permissions.contains(MerchantManagementPermission.EDIT_TRADES_LIMIT)) {
				setRegularItem(new ClickeableItem(new ItemData("trades_limit", SupremeShops.inst().getModuleManager().getMerchantManagementSlotsGuiTradesLimit(), EditorGUI.ICON_NUMBER, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITTRADESLIMITNAME.getLine(), null)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						player.closeInventory();
						// enter name in chat
						SSLocale.MSG_SUPREMESHOPS_CHANGEMERCHANTTRADESLIMITINPUT.send(player);
						GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
							@Override
							public void onChat(Player player, String value) {
								// cancel
								if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
									MerchantManagementGUI.this.open(player);
									return;
								}
								// not a number
								Integer limit = Utils.integerOrNull(value.trim());
								if (limit == null || limit < -1 || limit > Integer.MAX_VALUE) {
									GLocale.MSG_GENERIC_COMMAND_INVALIDINTPARAM.send(player, "{plugin}", SupremeShops.inst().getName(), "{parameter}", value);
									MerchantManagementGUI.this.open(player);
									return;
								}
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
								// done
								merchant.changeTradesLimit(limit == 0 ? -1 : limit);
								SSLocale.MSG_SUPREMESHOPS_CHANGEDMERCHANTTRADESLIMIT.send(player, "{limit}", limit == 0 ? -1 : limit);
								MerchantManagementGUI.this.open(player);
							}
						});
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("trades_limit", SupremeShops.inst().getModuleManager().getMerchantManagementSlotsGuiTradesLimit(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITTRADESLIMITNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
		// add conditions item
		if (merchant.isCurrentOwnerAdmin() || MerchantManagementPermission.EDIT_INTERACT_CONDITIONS.getPermission().has(merchant.getCurrentOwnerPlayer())) {
			if (permissions.contains(MerchantManagementPermission.EDIT_INTERACT_CONDITIONS)) {
				setRegularItem(new ClickeableItem(new ItemData("conditions", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiInteractConditions(), EditorGUI.ICON_TECHNICAL, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITCONDITIONSNAME.getLine(), null)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						new MerchantEditInteractConditionsGUI(merchant, player, gui, pageIndex).open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("managers", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiInteractConditions(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITMANAGERSNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
		// add managers item
		if (merchant.isCurrentOwnerAdmin() || MerchantManagementPermission.EDIT_MANAGERS.getPermission().has(merchant.getCurrentOwnerPlayer())) {
			if (permissions.contains(MerchantManagementPermission.EDIT_MANAGERS)) {
				setRegularItem(new ClickeableItem(new ItemData("managers", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiManagers(), Mat.CHEST, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITMANAGERSNAME.getLine(), null)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						new ManageableEditManagersGUI<>(merchant, player, gui, pageIndex).open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("managers", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiManagers(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITMANAGERSNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
		// add display name item
		if (merchant.isCurrentOwnerAdmin() || MerchantManagementPermission.SET_DISPLAY_NAME.getPermission().has(merchant.getCurrentOwnerPlayer())) {
			if (permissions.contains(MerchantManagementPermission.SET_DISPLAY_NAME)) {
				setRegularItem(new ClickeableItem(new ItemData("display_name", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiDisplayName(), Mat.NAME_TAG, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITDISPLAYNAMENAME.getLine(), null)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						player.closeInventory();
						// enter name in chat
						SSLocale.MSG_SUPREMESHOPS_CHANGEMERCHANTDISPLAYNAMEINPUT.send(player);
						GCoreLegacy.inst().getChatInputs().put(player, new ChatInput() {
							@Override
							public void onChat(Player player, String value) {
								// cancel
								if (Utils.unformat(value.trim().toLowerCase()).equals("cancel")) {
									MerchantManagementGUI.this.open(player);
									return;
								}
								// forbidden words
								List<String> forbidden = SupremeShops.inst().getConfiguration().getListFormatted("player_input_blacklist", Utils.emptyList());
								String[] words = Utils.format(value).toLowerCase().split(" ");
								for (String word : words) {
									for (String forb : forbidden) {
										int similarity = Utils.getLevenshteinSimilarity(word, forb);
										if (similarity < 3) {
											SSLocale.MSG_SUPREMESHOPS_BLACKLISTEDPLAYERINPUT.send(player, "{word}", word, "{similar}", forb);
											MerchantManagementGUI.this.open(player);
											// sound
											if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
												SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
											}
											return;
										}
									}
								}
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
								// done
								merchant.changeDisplayName(value);
								SSLocale.MSG_SUPREMESHOPS_CHANGEDMERCHANTDISPLAYNAME.send(player, "{name}", value);
								new MerchantManagementGUI(merchant, permissions, player, MerchantManagementGUI.this.fromGUI, MerchantManagementGUI.this.fromGUIPageIndex).open(player);// refresh the whole GUI since the display name changed (so the inventory name as well)
							}
						});
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("particle_pattern", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiDisplayName(), Mat.GRAY_STAINED_GLASS_PANE, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITDISPLAYNAMENAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
		// add particle pattern item
		if (merchant instanceof ParticlePatternAssignable) {
			if (merchant.isCurrentOwnerAdmin() || MerchantManagementPermission.SET_PARTICLE_PATTERN.getPermission().has(merchant.getCurrentOwnerPlayer())) {
				if (permissions.contains(MerchantManagementPermission.SET_PARTICLE_PATTERN)) {
					setRegularItem(new ClickeableItem(new ItemData("particle_pattern", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiParticlePatterns(), Mat.BLAZE_POWDER, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITPARTICLEPATTERNNAME.getLine(), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							new EditParticlePatternGUI((ParticlePatternAssignable) merchant, player, gui, pageIndex).open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				} else {
					setRegularItem(new ClickeableItem(new ItemData("particle_pattern", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiParticlePatterns(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITPARTICLEPATTERNNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
				}
			}
		}
		// add skins item
		if (merchant instanceof NpcMerchant) {
			if (merchant.isCurrentOwnerAdmin() || MerchantManagementPermission.SET_SKIN.getPermission().has(merchant.getCurrentOwnerPlayer())) {
				if (permissions.contains(MerchantManagementPermission.SET_SKIN)) {
					setRegularItem(new ClickeableItem(new ItemData("skin", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiSkins(), Mat.LEATHER, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITSKINNNAME.getLine(), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							List<NpcMerchantSkin> skins = new ArrayList<NpcMerchantSkin>();
							for (NpcMerchantSkin skin : SupremeShops.inst().getModuleManager().getMerchantSkins().values()) {
								if (merchant.isCurrentOwnerAdmin() || skin.hasPermission(merchant.getCurrentOwnerPlayer())) {
									skins.add(skin);	
								}
							}
							new MerchantEditSkinGUI((NpcMerchant) merchant, skins, player, gui, pageIndex).open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				} else {
					setRegularItem(new ClickeableItem(new ItemData("skin", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiSkins(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITSKINNNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
				}
			}
		}
		// add equipments item
		if (merchant instanceof NpcMerchant) {
			if (merchant.isCurrentOwnerAdmin() || MerchantManagementPermission.SET_EQUIPMENT.getPermission().has(merchant.getCurrentOwnerPlayer())) {
				if (permissions.contains(MerchantManagementPermission.SET_EQUIPMENT)) {
					setRegularItem(new ClickeableItem(new ItemData("equipment", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiEquipments(), Mat.IRON_PICKAXE, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITEQUIPMENTNAME.getLine(), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							List<NpcMerchantEquipment> equipments = new ArrayList<NpcMerchantEquipment>();
							for (NpcMerchantEquipment equipment : SupremeShops.inst().getModuleManager().getMerchantEquipments().values()) {
								if (merchant.isCurrentOwnerAdmin() || equipment.hasPermission(merchant.getCurrentOwnerPlayer())) {
									equipments.add(equipment);	
								}
							}
							new MerchantEditEquipmentGUI((NpcMerchant) merchant, equipments, player, gui, pageIndex).open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				} else {
					setRegularItem(new ClickeableItem(new ItemData("equipment", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiEquipments(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITSKINNNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
				}
			}
		}
		// add status item
		if (merchant instanceof NpcMerchant) {
			if (merchant.isCurrentOwnerAdmin() || MerchantManagementPermission.SET_STATUS.getPermission().has(merchant.getCurrentOwnerPlayer())) {
				if (permissions.contains(MerchantManagementPermission.SET_STATUS)) {
					setRegularItem(new ClickeableItem(new ItemData("status", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiStatus(), Mat.FLINT_AND_STEEL, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITSTATUSNAME.getLine(), null)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							new MerchantEditStatusGUI((NpcMerchant) merchant, player, gui, pageIndex).open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				} else {
					setRegularItem(new ClickeableItem(new ItemData("status", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiStatus(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITSTATUSNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
				}
			}
		}
		// add rent button
		if (merchant instanceof Rentable) {
			setRegularItem(new ClickeableItem(new ItemData("rent", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiRent(), Mat.CLOCK, 1, SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_RENTABLEEDITRENTLORE.getLines(merchant.getMessageReplacers(true, false, player)))) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// left-click, see rent price
					if (clickType.equals(ClickType.LEFT)) {
						new SeeRentGUI((Rentable) merchant, player, gui, pageIndex).open(player);
					}
					// right-click, pay rent (in advance ?)
					else if (clickType.equals(ClickType.RIGHT)) {
						new PayRentAmountSelectionGUI((Rentable) merchant, player) {
							@Override
							protected void onSelect(Player player, int amount) {
								// ensure he still has everything needed
								for (TradeObject object : ((Rentable) merchant).getRentPrice()) {
									double objectAm = object.getCustomAmount(player) * amount;
									if (object.getTradesForStock(object.getPlayerStock(player), objectAm) < amount) {
										object.sendHasntMessage(player, objectAm);
										if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
											SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
										}
										MerchantManagementGUI.this.open(player);
										return;
									}
								}
								// pay
								for (TradeObject object : ((Rentable) merchant).getRentPrice()) {
									object.take(player, object.getCustomAmount(player) * amount);
								}
								// change paid rents
								((Rentable) merchant).changePaidRents(((Rentable) merchant).getPaidRents() + amount);
								// done
								MerchantManagementGUI.this.open(player);
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
							}
						}.open(player);
					}
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
		// add stop renting button
		if (merchant instanceof Rentable) {
			if (permissions.contains(MerchantManagementPermission.STOP_RENTING)) {
				setRegularItem(new ClickeableItem(new ItemData("stop_renting", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiStopRenting(), Mat.TNT_MINECART, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITSTOPRENTINGNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITSTOPRENTINGLORE.getLines())) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						GUI confirmGUI = new GUI(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_SHOPSTOPRENTINGCONFIRMGUINAME.getLine(), 9, GUI.SLOTS_0_TO_8);
						confirmGUI.setRegularItem(new ClickeableItem(new ItemData("confirm", 3, Mat.TNT_MINECART, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTSTOPRENTINGCONFIRMITEMNAME.getLine(), null)) {
							@Override
							public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
								((Rentable) merchant).changeRent(null, 0, true);
								player.closeInventory();
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
							}
						});
						confirmGUI.setRegularItem(new ClickeableItem(new ItemData("cancel", 5, Mat.ARROW, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTSTOPRENTINGCANCELITEMNAME.getLine(), null)) {
							@Override
							public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
								MerchantManagementGUI.this.open(player);
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
							}
						});
						confirmGUI.open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("destroy", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiStopRenting(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITSTOPRENTINGNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
		// add destroy item
		else {
			if (permissions.contains(MerchantManagementPermission.DESTROY)) {
				setRegularItem(new ClickeableItem(new ItemData("destroy", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiDestroy(), Mat.TNT_MINECART, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTDESTROYNAME.getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTDESTROYLORE.getLines())) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						GUI confirmGUI = new GUI(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTDESTROYCONFIRMGUINAME.getLine(), 9, GUI.SLOTS_0_TO_8);
						confirmGUI.setRegularItem(new ClickeableItem(new ItemData("confirm", 3, Mat.TNT_MINECART, 1, SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTDESTROYCONFIRMITEMNAME.getLine(), null)) {
							@Override
							public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
								merchant.destroy(DestroyCause.EDITION_DESTROY_BUTTON, true);
								player.closeInventory();
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
							}
						});
						confirmGUI.setRegularItem(new ClickeableItem(new ItemData("cancel", 5, Mat.ARROW, 1, SSLocaleMisc.MISC_SUPREMESHOPS_DESTROYCANCELITEMNAME.getLine(), null)) {
							@Override
							public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
								MerchantManagementGUI.this.open(player);
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
								}
							}
						});
						confirmGUI.open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("destroy", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiDestroy(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7§o" + Utils.unformat(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTDESTROYNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
		// add back item
		if (fromGUI != null) {
			setRegularItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiBack())) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					fromGUI.open(player, fromGUIPageIndex);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
	}

	@Override
	protected boolean postFill() {
		return true;
	}

	private void updateInfoItem() {
		// build icon
		ItemStack iconStack = (merchant instanceof Rentable ? SupremeShops.inst().getModuleManager().getPreviewGuiRentMerchantInfo() : (merchant.isCurrentOwnerAdmin() ? SupremeShops.inst().getModuleManager().getPreviewGuiAdminMerchantInfo() : SupremeShops.inst().getModuleManager().getPreviewGuiMerchantInfo())).getItemStack(merchant.getMessageReplacers(true, false, player)).clone();
		ItemData icon = new ItemData("info", SupremeShops.inst().getModuleManager().getMerchantManagementSlotInfo(), iconStack);
		// set icon
		setRegularItem(new ClickeableItem(icon));
	}

	private void updateCloseToggleItem() {
		if (merchant.isCurrentOwnerAdmin() || MerchantManagementPermission.SET_OPEN.getPermission().has(merchant.getCurrentOwnerPlayer())) {
			if (permissions.contains(MerchantManagementPermission.SET_OPEN)) {
				setRegularItem(new ClickeableItem(new ItemData("closed", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiCloseToggle(), merchant.isOpen() ? Mat.GREEN_WOOL : Mat.RED_WOOL, 1, merchant.isOpen() ? SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITOPENEDITEMNAME.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITCLOSEDITEMNAME.getLine(), null)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						// too recent
						if (System.currentTimeMillis() - lastToggle < 500L) {
							return;
						}
						lastToggle = System.currentTimeMillis();
						// toggle
						merchant.changeOpen(!merchant.isOpen());
						updateCloseToggleItem();
						updateInfoItem();
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("closed", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiCloseToggle(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7" + Utils.unformat(merchant.isOpen() ? SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITOPENEDITEMNAME.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITCLOSEDITEMNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
	}

	private void updateRemoteToggleItem() {
		if (merchant.isCurrentOwnerAdmin() || MerchantManagementPermission.SET_REMOTE.getPermission().has(merchant.getCurrentOwnerPlayer())) {
			if (permissions.contains(MerchantManagementPermission.SET_REMOTE)) {
				setRegularItem(new ClickeableItem(new ItemData("remote", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiRemoteToggle(), merchant.isRemote() ? Mat.GREEN_WOOL : Mat.RED_WOOL, 1, merchant.isRemote() ? SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITREMOTEITEMNAME.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITNONREMOTEITEMNAME.getLine(), null)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						// too recent
						if (System.currentTimeMillis() - lastToggle < 500L) {
							return;
						}
						lastToggle = System.currentTimeMillis();
						// max remote merchants reached already
						if (!merchant.isCurrentOwnerAdmin()) {
							List<Merchant> merchants = SupremeShops.inst().getData().getMerchants().getElements(merchant.getCurrentOwner(), null, false, ElementRemotePolicy.MUST_BE);
							if (merchants.size() >= SupremeShops.inst().getModuleManager().getMaxRemoteMerchants(merchant.getCurrentOwnerPlayer())) {
								SSLocale.MSG_SUPREMESHOPS_MAXREMOTEMERCHANTS.send(player);
								// sound
								if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
									SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
								}
								return;
							}
						}
						// set remote
						merchant.changeRemote(!merchant.isRemote());
						updateRemoteToggleItem();
						updateInfoItem();
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			} else {
				setRegularItem(new ClickeableItem(new ItemData("remote", SupremeShops.inst().getModuleManager().getMerchantManagementSlotGuiRemoteToggle(), Mat.GRAY_STAINED_GLASS_PANE, 1, "§7" + Utils.unformat(merchant.isRemote() ? SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITREMOTEITEMNAME.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITNONREMOTEITEMNAME.getLine()), SSLocaleMisc.MISC_SUPREMESHOPS_EDITNOPERMISSIONLORE.getLines())));
			}
		}
	}

}
