package com.guillaumevdn.supremeshops.gui.merchant.management;

import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.lib.Perm;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.npc.NpcStatus;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.merchant.MerchantGUI;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchant;

public class MerchantEditStatusGUI extends FilledGUI implements MerchantGUI {

	// base
	private NpcMerchant merchant;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public MerchantEditStatusGUI(NpcMerchant merchant, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITSTATUSGUINAME.getLine(merchant.getMessageReplacers(false, false, player)), Utils.getInventorySize(NpcStatus.values().length), GUI.SLOTS_0_TO_44);
		this.merchant = merchant;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public NpcMerchant getMerchant() {
		return merchant;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill
	private long lastToggle = 0L;

	@Override
	protected void fill() {
		// add status
		OfflinePlayer owner = merchant.isCurrentOwnerAdmin() ? null : merchant.getCurrentOwner().toOfflinePlayer();
		for (final NpcStatus status : NpcStatus.values()) {
			// can't have
			if (owner != null && ! new Perm(SSPerm.SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_ALLSTATUS, "supremeshops.merchant.management.status." + status.name().toLowerCase()).has(owner)) {
				continue;
			}
			// add
			boolean has = merchant.getStatus().contains(status);
			setRegularItem(new ClickeableItem(new ItemData("status_" + status.name(), -1, has ? Mat.GREEN_WOOL : Mat.RED_WOOL, 1, (has ? "§a" : "§c") + status.name(), SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITCONTROLTOGGLESTATUSLORE.getLines())) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// right-click : toggle
					if (clickType.equals(ClickType.RIGHT)) {
						// too recent
						if (System.currentTimeMillis() - lastToggle < 500L) {
							return;
						}
						lastToggle = System.currentTimeMillis();
						// toggle
						List<NpcStatus> list = Utils.asList(merchant.getStatus());
						boolean has = list.contains(status);
						if (has) {
							list.remove(status);
						} else {
							list.add(status);
						}
						merchant.changeStatus(list);
						open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
						SupremeShops.inst().pluginLog(null, merchant, null, player, null, (!has ? "Added" : "Removed") + " status " + status.name());
					}
				}
			});
		}
		// add back item
		setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(getSize() == 54 ? 52 : getSize() - 1)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				fromGUI.open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
