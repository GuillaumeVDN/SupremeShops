package com.guillaumevdn.supremeshops.gui.merchant.management;

import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.merchant.MerchantGUI;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchant;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchantEquipment;

public class MerchantEditEquipmentGUI extends FilledGUI implements MerchantGUI {

	// amount
	private NpcMerchant merchant;
	private List<NpcMerchantEquipment> equipments;
	private Player player;
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public MerchantEditEquipmentGUI(NpcMerchant merchant, List<NpcMerchantEquipment> equipments, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTEDITEQUIPMENTGUINAME.getLine(merchant.getMessageReplacers(false, false, player)), Utils.getInventorySize(equipments.size()), GUI.SLOTS_0_TO_44);
		this.merchant = merchant;
		this.equipments = equipments;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public NpcMerchant getMerchant() {
		return merchant;
	}

	public List<NpcMerchantEquipment> getPatterns() {
		return equipments;
	}

	public Player getPlayer() {
		return player;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill
	private long lastToggle = 0L;

	@Override
	protected void fill() {
		// add equipments
		for (final NpcMerchantEquipment equipment : equipments) {
			// build icon
			ItemData icon = equipment.getIcon().cloneWithId("equipment_" + equipment.hashCode());
			List<String> lore = icon.getLore() != null ? icon.getLore() : Utils.emptyList();
			lore.addAll(equipment.getId().equalsIgnoreCase(merchant.getEquipmentId()) ? SSLocaleMisc.MISC_SUPREMESHOPS_EDITEQUIPMENTSELECTEDLORE.getLines() : SSLocaleMisc.MISC_SUPREMESHOPS_EDITEQUIPMENTNOTSELECTEDLORE.getLines());
			icon.setLore(lore);
			// set icon
			setRegularItem(new ClickeableItem(icon) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// right-click : select
					if (clickType.equals(ClickType.RIGHT)) {
						if (!equipment.getId().equalsIgnoreCase(merchant.getEquipmentId())) {
							// too recent
							if (System.currentTimeMillis() - lastToggle < 500L) {
								return;
							}
							lastToggle = System.currentTimeMillis();
							// select
							merchant.changeEquipment(equipment.getId());
							MerchantEditEquipmentGUI.this.open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					}
					// shift + right-click : remove
					else if (clickType.equals(ClickType.SHIFT_RIGHT)) {
						if (equipment.getId().equalsIgnoreCase(merchant.getEquipmentId())) {
							// too recent
							if (System.currentTimeMillis() - lastToggle < 500L) {
								return;
							}
							lastToggle = System.currentTimeMillis();
							// select
							merchant.changeEquipment(null);
							MerchantEditEquipmentGUI.this.open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					}
				}
			});
		}
		// add back item
		setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(getSize() == 54 ? 52 : getSize() - 1)) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				fromGUI.open(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
