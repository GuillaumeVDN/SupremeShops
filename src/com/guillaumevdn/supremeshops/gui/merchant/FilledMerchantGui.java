package com.guillaumevdn.supremeshops.gui.merchant;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.messenger.Replacer;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.merchant.management.MerchantManagementGUI;
import com.guillaumevdn.supremeshops.gui.rentable.management.RentableManagementGUI;
import com.guillaumevdn.supremeshops.gui.shop.TradePreviewGUI;
import com.guillaumevdn.supremeshops.module.manageable.MerchantManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.PlayerShop;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class FilledMerchantGui extends FilledGUI implements MerchantGUI {

	// base
	private Merchant merchant;
	private Player player;
	private List<Shop> lastFilledShops = new ArrayList<Shop>();
	private GUI fromGUI;
	private int fromGUIPageIndex;

	public FilledMerchantGui(Merchant merchant, Player player, String name, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), name, 54, GUI.SLOTS_0_TO_44);
		this.merchant = merchant;
		this.player = player;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
	}

	// get
	@Override
	public Merchant getMerchant() {
		return merchant;
	}

	public Player getPlayer() {
		return player;
	}

	public List<Shop> getLastFilledShops() {
		return lastFilledShops;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	// fill
	@Override
	protected void fill() {
		// merchants shops
		lastFilledShops.clear();
		for (final Shop shop : merchant.getShops(null, true)) {
			// can't trade
			if (!shop.areTradeConditionsValid(player, false)) {
				continue;
			}
			// didn't discover items
			if (SupremeShops.inst().getForceItemDiscover() && !shop.hasDiscoveredAllItems(player, false)) {
				continue;
			}
			// build name and lore
			ItemData infoItem = shop instanceof Rentable ? SupremeShops.inst().getModuleManager().getPreviewGuiRentShopInfo() : (shop.getCurrentOwner() != null ? SupremeShops.inst().getModuleManager().getPreviewGuiShopInfo() : SupremeShops.inst().getModuleManager().getPreviewGuiAdminShopInfo());
			List<String> lore = infoItem.getLore() != null ? Utils.asList(infoItem.getLore()) : Utils.emptyList();
			lore.add("");
			lore.addAll(shop instanceof PlayerShop && !((PlayerShop) shop).getManagementPermissions(player).isEmpty() ? SSLocaleMisc.MISC_SUPREMESHOPS_SHOPLISTLORECONTROLPREVIEWANDEDIT.getLines() : SSLocaleMisc.MISC_SUPREMESHOPS_SHOPLISTLORECONTROLPREVIEWANDTRADE.getLines());
			Replacer replacer = new Replacer(shop.getMessageReplacers(true, false, player));
			// build icon
			ItemData icon = shop.getGuiIcon(player);
			icon.setId("shop_" + shop.getDataId());
			icon.setSlot(-1);
			icon.setAmount(1);
			icon.setName(replacer.apply(infoItem.getName()));
			icon.setLore(replacer.apply(lore));
			// build and set item
			setRegularItem(new ClickeableItem(icon) {
				@Override
				public void onClick(final Player player, ClickType clickType, final GUI gui, final int pageIndex) {
					// preview trade
					if (clickType.equals(ClickType.LEFT)) {
						new TradePreviewGUI(shop, merchant, 1, player, gui, pageIndex).open(player);
					}
				}
			});
		}
		// edit rent button
		if (merchant instanceof Rentable && SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
			// edit rent admin button
			if (SupremeShops.inst().getModuleManager().getPreviewGuiEditAdminRentItem() != null && !SupremeShops.inst().getModuleManager().getPreviewGuiEditAdminRentItem().getType().isAir()) {
				setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getPreviewGuiEditAdminRentItem().cloneWithSlot(49)) {
					@Override
					public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
						new RentableManagementGUI((Rentable) merchant, player, null, -1).open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				});
			}
		}
		// edit button
		else {
			final Set<MerchantManagementPermission> permissions = merchant.getManagementPermissions(player);
			if (permissions != null && !permissions.isEmpty()) {
				// edit button
				if (SupremeShops.inst().getModuleManager().getPreviewGuiEditItem() != null && !SupremeShops.inst().getModuleManager().getPreviewGuiEditItem().getType().isAir()) {
					setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getPreviewGuiEditItem().cloneWithSlot(49)) {
						@Override
						public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
							new MerchantManagementGUI(merchant, permissions, player, gui, pageIndex).open(player);
							// sound
							if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
								SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
							}
						}
					});
				}
			}
		}
		// add back item
		if (fromGUI != null) {
			setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(52)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					fromGUI.open(player, fromGUIPageIndex);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
