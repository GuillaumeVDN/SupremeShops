package com.guillaumevdn.supremeshops.gui.merchant;

import java.util.Collection;
import java.util.List;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.messenger.Replacer;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchant;

public class MerchantListAdminGUI extends GUI {

	// base
	public MerchantListAdminGUI(Collection<Merchant> merchants, OfflinePlayer owner, Player player) {
		super(SupremeShops.inst(), owner != null ? SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTLISTOWNERGUINAME.getLine("{owner}", owner.getName()) : SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTLISTGUINAME.getLine(), 54, GUI.SLOTS_0_TO_53);
		// initialize
		for (final Merchant merchant : merchants) {
			// build lore
			ItemData infoItem = merchant.isCurrentOwnerAdmin() ? SupremeShops.inst().getModuleManager().getPreviewGuiAdminMerchantInfo() : SupremeShops.inst().getModuleManager().getPreviewGuiMerchantInfo();
			List<String> lore = infoItem.getLore() != null ? Utils.asList(infoItem.getLore()) : Utils.emptyList();
			if (merchant instanceof NpcMerchant) {
				lore.add("");
				lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTLISTLORECONTROLTELEPORT.getLines());
			}
			lore = new Replacer(merchant.getMessageReplacers(true, false, player)).apply(lore);
			// build and set item
			setRegularItem(new ClickeableItem(new ItemData("merchant_" + merchant.getDataId(), -1, EditorGUI.ICON_NPC, 1,
					"§e" + merchant.getDisplayName() + " " + (merchant.isOpen() ? SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTOPEN.getLine() : SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTCLOSED.getLine()),
					lore)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// teleport
					if (clickType.equals(ClickType.LEFT) && merchant instanceof NpcMerchant) {
						player.teleport(((NpcMerchant) merchant).getLocation());
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				}
			});
		}
	}

}
