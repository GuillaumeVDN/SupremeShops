package com.guillaumevdn.supremeshops.gui.merchant;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.ClickeableItem;
import com.guillaumevdn.gcorelegacy.lib.gui.FilledGUI;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.messenger.Replacer;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.util.MerchantsSorter;
import com.guillaumevdn.supremeshops.util.MerchantsSorter.SortCriteria;

public class CustomerMerchantListGUI extends FilledGUI {

	// base
	private UserInfo owner;
	private boolean adminOnly;
	private GUI fromGUI;
	private int fromGUIPageIndex;
	private Player player;
	private long lastChangedCriteria = System.currentTimeMillis();
	private SortCriteria sortCriteria = SortCriteria.BY_NAME;

	public CustomerMerchantListGUI(UserInfo owner, boolean adminOnly, Player player, GUI fromGUI, int fromGUIPageIndex) {
		super(SupremeShops.inst(), owner != null ? SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTLISTOWNERGUINAME.getLine("{owner}", owner.toOfflinePlayer().getName()) : SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTLISTGUINAME.getLine(), 54, GUI.SLOTS_0_TO_44);
		this.owner = owner;
		this.adminOnly = adminOnly;
		this.fromGUI = fromGUI;
		this.fromGUIPageIndex = fromGUIPageIndex;
		this.player = player;
	}

	// get
	public UserInfo getOwner() {
		return owner;
	}

	public boolean isAllowAdmin() {
		return adminOnly;
	}

	public long getLastChangedCriteria() {
		return lastChangedCriteria;
	}

	public SortCriteria getSortCriteria() {
		return sortCriteria;
	}

	public GUI getFromGUI() {
		return fromGUI;
	}

	public int getFromGUIPageIndex() {
		return fromGUIPageIndex;
	}

	public Player getPlayer() {
		return player;
	}

	// fill
	@Override
	protected void fill() {
		// calculate content
		List<Merchant> content = new ArrayList<Merchant>();
		if (owner == null) {
			if (adminOnly) {
				content.addAll(SupremeShops.inst().getData().getMerchants().getElements(null, null, true, ElementRemotePolicy.MUST_BE));
			} else {
				content.addAll(SupremeShops.inst().getData().getMerchants().getAll(null, true, ElementRemotePolicy.MUST_BE));
			}
		} else {
			content.addAll(SupremeShops.inst().getData().getMerchants().getElements(owner, null, true, ElementRemotePolicy.MUST_BE));
		}
		// remove content that don't match conditions
		Iterator<Merchant> iterator = content.iterator();
		while (iterator.hasNext()) {
			Merchant merchant = iterator.next();
			if (merchant.getManagementPermissions(player).isEmpty()) {// can manage
				iterator.remove();
			} else if (!merchant.areInteractConditionsValid(player, false)) {// invalid interact conditions
				iterator.remove();
			}
		}
		// sort content
		content = new MerchantsSorter(content, sortCriteria).getSortedList();
		// initialize content
		for (final Merchant merchant : content) {
			// build lore
			ItemData infoItem = merchant.isCurrentOwnerAdmin() ? SupremeShops.inst().getModuleManager().getPreviewGuiAdminMerchantInfo() : SupremeShops.inst().getModuleManager().getPreviewGuiMerchantInfo();
			List<String> lore = infoItem.getLore() != null ? Utils.asList(infoItem.getLore()) : Utils.emptyList();
			lore.add("");
			lore.addAll(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTLISTLORECONTROLPREVIEWANDTRADE.getLines());
			lore = new Replacer(merchant.getMessageReplacers(true, false, player)).apply(lore);
			// build and set item
			setRegularItem(new ClickeableItem(new ItemData("merchant_" + merchant.getDataId(), -1, Mat.EMERALD, 1,
					"§e" + merchant.getDisplayName() + " " + SSLocaleMisc.MISC_SUPREMESHOPS_SHOPOPEN.getLine(),
					lore)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					// preview trade
					if (clickType.equals(ClickType.LEFT)) {
						merchant.buildGui(player, gui, pageIndex).open(player);
						// sound
						if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
							SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
						}
					}
				}
			});
		}
		// add sort item
		setPersistentItem(new ClickeableItem(new ItemData("sort", 50, Mat.COMMAND_BLOCK, 1, sortCriteria.getName().getLine(), SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIALORE.getLines())) {
			@Override
			public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
				// too often
				if (System.currentTimeMillis() - lastChangedCriteria < 500L) {
					return;
				}
				// left-click : by name
				if (clickType.equals(ClickType.LEFT)) {
					sortCriteria = SortCriteria.BY_NAME;
					open(player, pageIndex);
				}
				// right-click : by unique buyers
				else if (clickType.equals(ClickType.RIGHT)) {
					sortCriteria = SortCriteria.BY_UNIQUE_BUYERS;
					open(player, pageIndex);
				}
				// shift + left-click : by ranking (server)
				else if (clickType.equals(ClickType.SHIFT_LEFT)) {
					sortCriteria = SortCriteria.BY_RANKING_SERVER;
					open(player, pageIndex);
				}
				// shift + right-click : by ranking (seller)
				else if (clickType.equals(ClickType.SHIFT_LEFT)) {
					sortCriteria = SortCriteria.BY_RANKING_SELLER;
					open(player, pageIndex);
				}
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
				}
			}
		});
		// add back item
		if (fromGUI != null) {
			setPersistentItem(new ClickeableItem(SupremeShops.inst().getModuleManager().getBackItem().cloneWithSlot(52)) {
				@Override
				public void onClick(Player player, ClickType clickType, GUI gui, int pageIndex) {
					fromGUI.open(player, fromGUIPageIndex);
					// sound
					if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
						SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
					}
				}
			});
		}
	}

	@Override
	protected boolean postFill() {
		return true;
	}

}
