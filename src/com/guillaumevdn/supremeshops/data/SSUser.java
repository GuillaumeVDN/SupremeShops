package com.guillaumevdn.supremeshops.data;

import java.io.File;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.data.DataElement;
import com.guillaumevdn.gcorelegacy.lib.data.mysql.Query;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class SSUser extends DataElement {

	// static get
	public static SSUser get(Object param) {
		return SupremeShops.inst().getData().getUsers().getElement(param);
	}

	// base
	private UserInfo user;
	private Map<String, Integer> shopTradeCount = new HashMap<String, Integer>();
	private Map<String, Integer> merchantTradeCount = new HashMap<String, Integer>();
	private Map<String, Long> lastRentEnd = new HashMap<String, Long>();
	private Set<Mat> discoveredMaterials = new HashSet<Mat>();

	SSUser(UserInfo user) {
		this.user = user;
	}

	// get
	public UserInfo getInfo() {
		return user;
	}

	public Map<String, Integer> getShopTradeCount() {
		return Collections.unmodifiableMap(shopTradeCount);
	}

	public int getShopTradeCount(Shop shop) {
		Integer trades = shopTradeCount.get(shop.getId().toLowerCase());
		return trades != null ? trades : 0;
	}

	public Map<String, Integer> getMerchantTradeCount() {
		return Collections.unmodifiableMap(merchantTradeCount);
	}

	public int getMerchantTradeCount(Merchant merchant) {
		Integer trades = merchantTradeCount.get(merchant.getId().toLowerCase());
		return trades != null ? trades : 0;
	}

	public Map<String, Long> getLastRentEnd() {
		return Collections.unmodifiableMap(lastRentEnd);
	}

	public long getLastRentEnd(Rentable rentable) {
		Long end = lastRentEnd.get(rentable.getId().toLowerCase());
		return end == null ? 0L : end;
	}

	public Set<Mat> getDiscoveredMaterials() {
		return Collections.unmodifiableSet(discoveredMaterials);
	}

	public boolean hasDiscoveredMaterial(Mat mat) {
		return discoveredMaterials.contains(mat);
	}

	// set
	public void setShopTradeCount(Shop shop, int count) {
		shopTradeCount.put(shop.getId().toLowerCase(), count);
		pushAsync();
	}

	public void setMerchantTradeCount(Merchant merchant, int count) {
		merchantTradeCount.put(merchant.getId().toLowerCase(), count);
		pushAsync();
	}

	public void setLastRentEnd(Rentable rentable, long end) {
		lastRentEnd.put(rentable.getId().toLowerCase(), end);
		pushAsync();
	}

	public void setMaterialDiscovered(Mat material, boolean push) {
		if (discoveredMaterials.add(material) && push) {
			pushAsync();
		}
	}

	public void setMaterialsDiscovered(Collection<Mat> materials, boolean push) {
		int count = 0;
		for (Mat mat : materials) {
			if (discoveredMaterials.add(mat)) {
				++count;
			}
		}
		if (++count > 0 && push) {
			pushAsync();
		}
	}

	// data
	@Override
	protected final UserBoard getBoard() {
		return SupremeShops.inst().getData().getUsers();
	}

	@Override
	protected final String getDataId() {
		return user.toString();
	}

	private static final class JsonData {
		private final Map<String, Integer> shopTradeCount;
		private final Map<String, Integer> merchantTradeCount;
		private final Map<String, Long> lastRentEnd;
		private final Set<Mat> discoveredMaterials;
		private JsonData(SSUser user) {
			this.shopTradeCount = user.shopTradeCount;
			this.merchantTradeCount = user.merchantTradeCount;
			this.lastRentEnd = user.lastRentEnd;
			this.discoveredMaterials = user.discoveredMaterials;
		}
	}

	private void readJsonData(JsonData data) {
		if (data.shopTradeCount != null) this.shopTradeCount = data.shopTradeCount;
		if (data.merchantTradeCount != null) this.merchantTradeCount = data.merchantTradeCount;
		if (data.lastRentEnd != null) this.lastRentEnd = data.lastRentEnd;
		if (data.discoveredMaterials != null) this.discoveredMaterials = data.discoveredMaterials;
	}

	@Override
	protected final void jsonPull() {
		File file = getBoard().getJsonFile(this);
		JsonData data = Utils.loadFromGson(JsonData.class, file, true, SupremeShops.GSON);
		if (data != null) {
			readJsonData(data);
		}
	}

	@Override
	protected final void jsonPush() {
		File file = getBoard().getJsonFile(this);
		Utils.saveToGson(new JsonData(this), file, SupremeShops.GSON);
	}

	@Override
	protected final void jsonDelete() {
		File file = getBoard().getJsonFile(this);
		if (file.exists()) {
			file.delete();
		}
	}

	// MySQL
	@Override
	protected final void mysqlPull(ResultSet set) throws SQLException {
		// data
		JsonData data = SupremeShops.UNPRETTY_GSON.fromJson(set.getString("data"), JsonData.class);
		if (data != null) {
			readJsonData(data);
		}
	}

	@Override
	protected final Query getMySQLPullQuery() {
		return new Query("SELECT * FROM `" + getBoard().getMySQLTable() + "` WHERE `id`=?;", getDataId());
	}

	@Override
	protected final Query getMySQLPushQuery() {
		return new Query("REPLACE INTO `" + getBoard().getMySQLTable() + "`(`id`,`data`) VALUES(?,?);", getDataId(), SupremeShops.UNPRETTY_GSON.toJson(new JsonData(this)));
	}

	@Override
	protected final Query getMySQLDeleteQuery() {
		return new Query("DELETE FROM `" + getBoard().getMySQLTable() + "` WHERE `id`=?;", getDataId());
	}

}
