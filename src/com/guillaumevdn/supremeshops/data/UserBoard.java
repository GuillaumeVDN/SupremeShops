package com.guillaumevdn.supremeshops.data;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.data.DataBoard;
import com.guillaumevdn.gcorelegacy.lib.data.DataManager.Callback;
import com.guillaumevdn.gcorelegacy.lib.data.mysql.Query;
import com.guillaumevdn.gcorelegacy.lib.event.UserDataProfileChangedEvent;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;

public class UserBoard extends DataBoard<SSUser> implements Listener {

	// fields
	private Map<UserInfo, SSUser> cache = new HashMap<UserInfo, SSUser>();

	// get
	public Map<UserInfo, SSUser> getCache() {
		return Collections.unmodifiableMap(cache);
	}

	@Override
	public SSUser getElement(Object param) {
		if (param instanceof OfflinePlayer) {
			return cache.get(new UserInfo((OfflinePlayer) param));
		} else if (param instanceof UUID) {
			return cache.get(new UserInfo((UUID) param));
		} else if (param instanceof UserInfo) {
			return cache.get(param);
		}
		throw new IllegalArgumentException("param type " + param.getClass() + " isn't allowed");
	}

	// methods
	public void pullOnline() {
		// get users
		final List<SSUser> toPull = new ArrayList<SSUser>();
		for (Player pl : Utils.getOnlinePlayers()) {
			UserInfo info = new UserInfo(pl);
			SSUser user = cache.get(info);
			if (user == null) cache.put(info, user = new SSUser(info));// add to cache
			toPull.add(user);// add to pull
		}
		// pull users
		pullAsync(toPull, new Callback() {
			@Override
			public void callback() {
				for (SSUser user : toPull) {
					pulledUsed(user, null);
				}
			}
		});
	}

	/**
	 * Loads an user, overwriting the current cached data if any is present - also loads user quests if is an online player
	 * @param info the user info
	 * @param finalCallback final callback (or null)
	 */
	public void loadUser(final UserInfo info, final Callback finalCallback) {
		// get user or add to cache
		final SSUser user;
		if (cache.containsKey(info)) {
			user = cache.get(info);
		} else {
			cache.put(info, user = new SSUser(info));
		}
		// pull user
		user.pullAsync(new Callback() {
			@Override
			public void callback() {
				pulledUsed(user, finalCallback);
			}
		});
	}

	private void pulledUsed(final SSUser user, final Callback finalCallback) {
		// not online
		final Player player = user.getInfo().toPlayer();
		if (player == null) {
			// final callback (early)
			if (finalCallback != null) finalCallback.callback();
			return;
		}
	}

	/**
	 * Unloads an user from the cache, pause his quests (where leader), and if online : remove the quest journal, reset camera mode, remove scoreboard
	 * <br>Use this with caution, you shouldn't unload an online player. This is called automatically when an user switches profile.
	 * @param info the user info
	 */
	public void unloadUser(UserInfo info) {
		// loaded
		SSUser user = cache.remove(info);
		if (user != null) {
			// not online
			Player player = info.toPlayer();
			if (player == null) {
				return;
			}
		}
	}

	// events
	@EventHandler(priority = EventPriority.LOW)
	public void event(final PlayerJoinEvent event) {
		// load user
		loadUser(new UserInfo(event.getPlayer()), null);
	}

	@EventHandler(priority = EventPriority.LOW)
	public void event(UserDataProfileChangedEvent event) {
		// unload
		UserInfo user = event.getUser();
		unloadUser(user);
		// load if online
		if (user.toPlayer() != null) {
			loadUser(user, null);
		}
	}

	@EventHandler(priority = EventPriority.HIGH)
	public void event(PlayerQuitEvent event) {
		// unload user
		unloadUser(new UserInfo(event.getPlayer()));
	}

	// data
	@Override
	public SSDataManager getDataManager() {
		return SupremeShops.inst().getData();
	}

	@Override
	protected final File getJsonFile(SSUser element) {
		return new File(GCoreLegacy.inst().getUserDataRootFolder() + "/" + element.getDataId() + "/supremeshops_user.json");
	}

	@Override
	protected final void jsonPull() {
		throw new UnsupportedOperationException();// can't pull the whole user board
	}

	@Override
	protected final void jsonDelete() {
		File root = GCoreLegacy.inst().getUserDataRootFolder();
		if (root.exists() && root.isDirectory()) {
			for (File userRoot : root.listFiles()) {
				if (userRoot.exists() && userRoot.isDirectory()) {
					File user = new File(userRoot.getPath() + "/supremeshops_user.json");
					if (user.exists()) {
						user.delete();
					}
				}
			}
		}
	}

	// MySQL
	@Override
	protected final String getMySQLTable() {
		return "supremeshops_users";
	}

	@Override
	protected final Query getMySQLInitQuery() {
		return new Query(
				// create table if not exists
				"CREATE TABLE IF NOT EXISTS `" + getMySQLTable() + "`("
				+ "id VARCHAR(100) NOT NULL,"
				+ "data LONGTEXT NOT NULL,"
				+ "PRIMARY KEY(id)"
				+ ") ENGINE=InnoDB DEFAULT CHARSET=?;", "utf8");
	}

	@Override
	protected final void mysqlPull() {
		throw new UnsupportedOperationException();// can't pull the whole user board
	}

	@Override
	protected final void mysqlDelete() {
		getDataManager().performMySQLUpdateQuery(new Query("DELETE FROM `" + getMySQLTable() + "`;"));
	}

}
