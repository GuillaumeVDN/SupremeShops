package com.guillaumevdn.supremeshops.data;

import java.io.File;
import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.data.DataSingleton;
import com.guillaumevdn.gcorelegacy.lib.data.mysql.Query;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;

public class MainBoard extends DataSingleton {

	// static base
	public static final String KEY_MISC_ = "misc:";
	public static final String KEY_MISC_SUCCESSFUL_TRADES = "successful_trades";
	public static final String KEY_ITEMVALUE_ = "itemvalue:";
	public static final String KEY_ITEMVALUE_GENERIC_ = KEY_ITEMVALUE_ + "generic:";
	public static final String KEY_ITEMVALUE_GENERIC_GIVING_ = KEY_ITEMVALUE_GENERIC_ + "giving:";
	public static final String KEY_ITEMVALUE_GENERIC_TAKING_ = KEY_ITEMVALUE_GENERIC_ + "taking:";
	public static final String KEY_ITEMVALUE_SPECIFIC_ = KEY_ITEMVALUE_ + "specific:";
	public static final String KEY_ITEMVALUE_SPECIFIC_GIVING_ = KEY_ITEMVALUE_SPECIFIC_ + "giving:";
	public static final String KEY_ITEMVALUE_SPECIFIC_TAKING_ = KEY_ITEMVALUE_SPECIFIC_ + "taking:";

	// base
	private ConcurrentHashMap<String, Double> values = new ConcurrentHashMap<>();
	private transient Set<String> mustPush = new HashSet<String>();

	// get
	public Map<String, Double> getAll() {
		return Collections.unmodifiableMap(values);
	}

	public Map<String, Double> getAll(String keyStart) {
		Map<String, Double> result = new HashMap<String, Double>();
		keyStart = keyStart.toLowerCase();
		for (String key : values.keySet()) {
			if (key.startsWith(keyStart)) {
				result.put(key, values.get(key));
			}
		}
		return Collections.unmodifiableMap(result);
	}

	public Double get(String key) {
		return values.get(key.toLowerCase());
	}

	public Double get(String key, double def) {
		Double value = get(key);
		return value != null ? value : def;
	}

	// set
	public void set(String key, double value) {
		key = key.toLowerCase();
		values.put(key, value);
		mustPush.add(key);
	}

	public void remove(String key) {
		if (values.remove(key.toLowerCase()) != null) {
			deleteAsync(key);
		}
	}

	// data
	public void pushIfNeeded(boolean async) {
		if (!mustPush.isEmpty()) {
			if (async) {
				pushAsync(mustPush);
			} else {
				push(mustPush);
			}
		}
	}

	@Override
	public SSDataManager getDataManager() {
		return SupremeShops.inst().getData();
	}

	public final static class JsonData {
		private final ConcurrentHashMap<String, Double> values;
		public JsonData(MainBoard board) {
			this.values = board.values;
		}
	}

	@Override
	public final File getJsonFile() {
		return new File(GCoreLegacy.inst().getDataRootFolder() + "/supremeshops_board.json");
	}

	@Override
	public final void jsonPull() {
		File file = getJsonFile();
		if (file.exists()) {
			JsonData data = Utils.loadFromGson(JsonData.class, file, true);
			if (data != null) {
				if (data.values != null) this.values = data.values;
			}
		}
	}

	@Override
	public final void jsonPush() {
		Utils.saveToGson(new JsonData(this), getJsonFile());
	}

	@Override
	public final void jsonDelete() {
		File file = getJsonFile();
		if (file.exists()) {
			file.delete();
		}
	}

	@Override
	public final String getMySQLTable() {
		return "supremeshops_board";
	}

	@Override
	public final Query getMySQLInitQuery() {
		return new Query("CREATE TABLE IF NOT EXISTS `" + getMySQLTable() + "`(" +
				"`key` VARCHAR(100) NOT NULL," +
				"`value` DECIMAL(20,10) NOT NULL," +
				"PRIMARY KEY(`key`)" +
				") ENGINE=InnoDB DEFAULT CHARSET=?;", "utf8");
	}

	@Override
	public final void mysqlPull() throws SQLException {
		getDataManager().performMySQLGetQuery(new Query("SELECT * FROM `" + getMySQLTable() + "`;"), set -> {
			while (set.next()) {
				String id = set.getString("key");
				double value = set.getDouble("value");
				values.put(id, value);
			}
		});
	}

	@Override
	public final void mysqlPush(Object... params) {
		// get keys
		Collection<String> keys;
		if (params != null && params.length > 0) {
			keys = (Collection<String>) params[0];
		} else {
			keys = values.keySet();
		}
		// build query
		Query query = new Query();
		for (String key : keys) {
			Double value = values.get(key.toLowerCase());
			if (value != null) {
				query.add("REPLACE INTO `" + getMySQLTable() + "`(`key`,`value`) VALUES(?,?);", key, BigDecimal.valueOf(values.get(key)).toPlainString());
			}
		}
		// execute query
		if (!query.isEmpty()) getDataManager().performMySQLUpdateQuery(query);
		// done, clear values
		keys.clear();
	}

	@Override
	public void mysqlDelete(Object... params) {
		if (params != null && params.length > 0) {
			String key = (String) params[0];
			getDataManager().performMySQLUpdateQuery(new Query("DELETE FROM `" + getMySQLTable() + "` WHERE `key`=?;", key));
		} else {
			getDataManager().performMySQLUpdateQuery(new Query("DELETE FROM `" + getMySQLTable() + "`;"));
		}
	}

}
