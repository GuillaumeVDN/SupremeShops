package com.guillaumevdn.supremeshops.data;

import org.bukkit.Bukkit;
import org.bukkit.event.HandlerList;

import com.guillaumevdn.gcorelegacy.lib.data.DataManager;
import com.guillaumevdn.gcorelegacy.lib.util.Handler;
import com.guillaumevdn.supremeshops.SupremeShops;

public class SSDataManager extends DataManager {

	// base
	private ShopBoard shopBoard = null;
	private MerchantBoard merchantBoard = null;
	private MainBoard board = null;
	private UserBoard userBoard = null;

	public SSDataManager(BackEnd backend) {
		super(SupremeShops.inst(), backend);
	}

	// get
	public ShopBoard getShops() {
		return shopBoard;
	}

	public MerchantBoard getMerchants() {
		return merchantBoard;
	}

	public MainBoard getBoard() {
		return board;
	}

	public UserBoard getUsers() {
		return userBoard;
	}

	// methods
	@Override
	protected void innerEnable() {
		// shop board
		shopBoard = new ShopBoard();
		shopBoard.initAsync(new Callback() {
			@Override
			public void callback() {
				shopBoard.pullAsync(null);
			}
		});
		// merchant board
		merchantBoard = new MerchantBoard();
		merchantBoard.initAsync(new Callback() {
			@Override
			public void callback() {
				merchantBoard.pullAsync(new Callback() {
					@Override
					public void callback() {
						// spawn npcs for online players
						SupremeShops.inst().getGeneralManager().spawnMerchantsNpcs();
					}
				});
			}
		});
		// board
		board = new MainBoard();
		board.initAsync(new Callback() {
			@Override
			public void callback() {
				board.pullAsync(null);
			}
		});
		// user board
		userBoard = new UserBoard();
		userBoard.initAsync(new Callback() {
			@Override
			public void callback() {
				userBoard.pullOnline();
				Bukkit.getPluginManager().registerEvents(userBoard, getPlugin());
			}
		});
	}

	@Override
	protected void innerSynchronize() {
		// shop board
		shopBoard.pullAsync(null);
		// merchant board
		merchantBoard.pullAsync(new Callback() {
			@Override
			public void callback() {
				new Handler() {
					@Override
					public void execute() {
						// refresh npcs for online players
						SupremeShops.inst().getGeneralManager().removeMerchantsNpcs();
						SupremeShops.inst().getGeneralManager().spawnMerchantsNpcs();
					}
				}.runSync();// merchant updateDisplay must be sync
			}
		});
		// board
		board.pullAsync(null);
		// user board
		userBoard.pullOnline();
	}

	@Override
	protected void innerReset() {
		// stop all displays
		SupremeShops.inst().getGeneralManager().stopDisplayManagers();
		// shop board
		shopBoard.deleteAsync();
		// merchant board
		SupremeShops.inst().getGeneralManager().removeMerchantsNpcs();
		merchantBoard.deleteAsync();
		// board
		board.deleteAsync();
		// user board
		userBoard.deleteAsync();
	}

	@Override
	protected void innerDisable() {
		// shop board
		shopBoard = null;
		// merchant board
		merchantBoard = null;
		// board
		board = null;
		// user board
		HandlerList.unregisterAll(userBoard);
		userBoard = null;
	}

}
