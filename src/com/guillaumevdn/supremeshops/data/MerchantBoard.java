package com.guillaumevdn.supremeshops.data;

import java.io.File;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.data.DataBoard;
import com.guillaumevdn.gcorelegacy.lib.data.DataManager.Callback;
import com.guillaumevdn.gcorelegacy.lib.data.mysql.Query;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.merchant.BlockMerchant;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.merchant.MerchantType;
import com.guillaumevdn.supremeshops.module.merchant.SignMerchant;

public class MerchantBoard extends DataBoard<Merchant> {

	// fields
	private Map<String, Merchant> merchants = new ConcurrentHashMap<String, Merchant>();
	private Map<UserInfo, List<Merchant>> merchantsByOwners = new HashMap<UserInfo, List<Merchant>>(); // null key = admin ; concurrent hashmaps can't have a null key ? :confusedwat: so whatever
	private Map<BlockCoords, BlockMerchant> blockMerchantsByBlock = new ConcurrentHashMap<BlockCoords, BlockMerchant>();
	private Map<BlockCoords, BlockMerchant> blockMerchantsBySign = new ConcurrentHashMap<BlockCoords, BlockMerchant>();
	private Map<BlockCoords, SignMerchant> signMerchantsBySign = new ConcurrentHashMap<BlockCoords, SignMerchant>();

	// methods
	public Map<String, Merchant> getAll() {
		return Collections.unmodifiableMap(merchants);
	}

	public List<Merchant> getAll(Collection<? extends Class<?>> types, boolean mustBeOpen, ElementRemotePolicy remotePolicy) {
		// get merchants
		List<Merchant> result = new ArrayList<Merchant>();
		// get matching merchants
		for (Merchant merchant : merchants.values()) {
			// type don't match
			if (types != null && !types.isEmpty() && !Utils.hasOneSuper(merchant.getClass(), types)) {
				continue;
			}
			// must be open but isn't
			if (mustBeOpen && !merchant.isOpen()) {
				continue;
			}
			// must be remote but isn't
			if (remotePolicy.equals(ElementRemotePolicy.MUST_BE)) {
				if (!merchant.isRemote()) {
					continue;
				}
			}
			// is remote but musn't be
			else if (remotePolicy.equals(ElementRemotePolicy.CANT_BE)) {
				if (merchant.isRemote()) {
					continue;
				}
			}
			// we good
			result.add(merchant);
		}
		// done
		return Collections.unmodifiableList(result);
	}

	public Map<UserInfo, List<Merchant>> getAllByOwners(boolean withAdmin) {
		if (withAdmin) {
			return Collections.unmodifiableMap(merchantsByOwners);
		} else {
			Map<UserInfo, List<Merchant>> copy = Utils.asMapCopy(merchantsByOwners);
			copy.remove(null);
			return Collections.unmodifiableMap(copy);
		}
	}

	/**
	 * Register and save a merchant
	 */
	public void add(Merchant merchant) {
		if (!merchants.containsKey(merchant.getId().toLowerCase())) {
			merchants.put(merchant.getId().toLowerCase(), merchant);
			merchant.pushAsync();
			// update by player
			updateByOwner(merchant, null);
			// update by block/sign
			if (merchant instanceof BlockMerchant) {
				blockMerchantsByBlock.put(((BlockMerchant) merchant).getBlock(), (BlockMerchant) merchant);
				blockMerchantsBySign.put(((BlockMerchant) merchant).getSign(), (BlockMerchant) merchant);
			}
			// update by sign
			if (merchant instanceof SignMerchant) {
				signMerchantsBySign.put(((SignMerchant) merchant).getSign(), (SignMerchant) merchant);
			}
		}
	}

	/**
	 * Delete and unregister a merchant
	 */
	public void delete(Merchant merchant) {
		if (merchants.containsKey(merchant.getId().toLowerCase())) {
			merchants.remove(merchant.getId().toLowerCase());
			merchant.deleteAsync();
			// update by player
			List<Merchant> byPlayer = merchantsByOwners.get(merchant.getCurrentOwner());
			if (byPlayer != null) {
				byPlayer.remove(merchant);
			}
			// update by block/sign
			if (merchant instanceof BlockMerchant) {
				blockMerchantsByBlock.remove(((BlockMerchant) merchant).getBlock());
				blockMerchantsBySign.remove(((BlockMerchant) merchant).getSign());
			}
			// update by sign
			if (merchant instanceof SignMerchant) {
				signMerchantsBySign.remove(((SignMerchant) merchant).getSign());
			}
		}
	}

	public void addAll(Collection<? extends Merchant> merchants, Callback dataCallback) {
		// add shops
		for (Merchant merchant : merchants) {
			this.merchants.put(merchant.getId().toLowerCase(), merchant);
		}
		// push all
		pushAsync(merchants, dataCallback);
		// update block/sign
		for (Merchant merchant : merchants) {
			if (merchant instanceof BlockMerchant) {
				blockMerchantsByBlock.remove(((BlockMerchant) merchant).getBlock());
				blockMerchantsBySign.remove(((BlockMerchant) merchant).getSign());
			}
			if (merchant instanceof SignMerchant) {
				signMerchantsBySign.put(((SignMerchant) merchant).getSign(), (SignMerchant) merchant);
			}
		}
		// update by player
		merchantsByOwners.clear();
		for (Merchant merchant : merchants) {
			updateByOwner(merchant, null);
		}
	}

	public void updateByOwner(Merchant merchant, UserInfo oldOwner) {
		// remove old
		if (oldOwner != null) {
			List<Merchant> byOwner = merchantsByOwners.get(oldOwner);
			if (byOwner != null && byOwner.remove(merchant) && byOwner.isEmpty()) {
				merchantsByOwners.remove(oldOwner);
			}
		}
		// add new
		List<Merchant> byPlayer = merchantsByOwners.get(merchant.getCurrentOwner());
		if (byPlayer == null) merchantsByOwners.put(merchant.getCurrentOwner(), byPlayer = new ArrayList<Merchant>());
		byPlayer.add(merchant);
	}

	/**
	 * Get a merchant by its coordinates
	 * @param param the merchant id
	 */
	@Override
	public Merchant getElement(Object param) {
		if (param instanceof String) {
			return merchants.get(((String) param).toLowerCase());
		}
		throw new IllegalArgumentException("param type " + param.getClass() + " must be a merchant id");
	}

	public List<Merchant> getElementsByOwner(UserInfo owner) {
		return getElementsByOwner(owner, null);
	}

	public Merchant getPhysicalElementLinkedToBlock(BlockCoords block) {
		// sign
		Merchant merchant = signMerchantsBySign.get(block);
		if (merchant != null) {
			return merchant;
		}
		// block
		merchant = blockMerchantsBySign.get(block);
		return merchant != null ? merchant : blockMerchantsByBlock.get(block);
	}

	public List<Merchant> getElementsByOwner(UserInfo owner, Collection<? extends Class<?>> types) {
		List<Merchant> merchants = new ArrayList<Merchant>();
		if (merchantsByOwners.containsKey(owner)) {
			for (Merchant merchant : merchantsByOwners.get(owner)) {
				if ((types == null || types.isEmpty()) || Utils.hasOneSuper(merchant.getClass(), types)) {
					merchants.add(merchant);
				}
			}
		}
		return Collections.unmodifiableList(merchants);
	}

	public Map<UserInfo, List<Merchant>> getMerchantsByOwners() {
		return Collections.unmodifiableMap(merchantsByOwners);
	}

	public List<Merchant> getElements(UserInfo player, Collection<? extends Class<?>> types, boolean mustBeOpen, ElementRemotePolicy remotePolicy) {
		// get merchants
		List<Merchant> result = new ArrayList<Merchant>();
		List<Merchant> byOwner = merchantsByOwners.get(player);
		if (byOwner != null) {
			// get matching merchants
			for (Merchant merchant : byOwner) {
				// type don't match
				if (types != null && !types.isEmpty() && !Utils.hasOneSuper(merchant.getClass(), types)) {
					continue;
				}
				// must be open but isn't
				if (mustBeOpen && !merchant.isOpen()) {
					continue;
				}
				// must be remote but isn't
				if (remotePolicy.equals(ElementRemotePolicy.MUST_BE)) {
					if (!merchant.isRemote()) {
						continue;
					}
				}
				// is remote but musn't be
				else if (remotePolicy.equals(ElementRemotePolicy.CANT_BE)) {
					if (merchant.isRemote()) {
						continue;
					}
				}
				// we good
				result.add(merchant);
			}
		}
		// done
		return Collections.unmodifiableList(result);
	}

	public static enum ElementRemotePolicy {
		MUST_BE,
		MIGHT_BE,
		CANT_BE
	}

	// data
	@Override
	public SSDataManager getDataManager() {
		return SupremeShops.inst().getData();
	}

	@Override
	public final File getJsonFile(Merchant element) {
		return new File(GCoreLegacy.inst().getDataRootFolder() + "/supremeshops_merchants/" + element.getType().getDiskDataFolder() + "/" + element.getDataId() + ".json");
	}

	@Override
	protected final void jsonPull() {
		merchants.clear();
		// load merchants by type
		for (MerchantType type : MerchantType.values()) {
			if (type.mustSaveData()) {
				File root = new File(GCoreLegacy.inst().getDataRootFolder() + "/supremeshops_merchants/" + type.getDiskDataFolder() + "/");
				if (root.exists() && root.isDirectory()) {
					// load merchants in this
					for (File file : root.listFiles()) {
						if (file.getName().toLowerCase().endsWith(".json")) {
							String id = file.getName().substring(0, file.getName().length() - 5);
							try {
								Constructor<? extends Merchant> constructor = type.getMerchantClass().getDeclaredConstructor(String.class);
								constructor.setAccessible(true);
								Merchant merchant = constructor.newInstance(id);
								merchant.jsonPull();
								if (!merchant.hasLoadError()) {
									merchants.put(merchant.getId().toLowerCase(), merchant);
								}
							} catch (Throwable exception) {
								exception.printStackTrace();
								SupremeShops.inst().error("Couldn't load merchant " + id + " from file " + file.getPath());
							}
						}
					}
				}
			}
		}
		// update by block/sign
		for (Merchant merchant : merchants.values()) {
			if (merchant instanceof BlockMerchant) {
				blockMerchantsByBlock.put(((BlockMerchant) merchant).getBlock(), (BlockMerchant) merchant);
				blockMerchantsBySign.put(((BlockMerchant) merchant).getSign(), (BlockMerchant) merchant);
			}
			// update by sign
			if (merchant instanceof SignMerchant) {
				signMerchantsBySign.put(((SignMerchant) merchant).getSign(), (SignMerchant) merchant);
			}
		}
		// update by player
		merchantsByOwners.clear();
		for (Merchant merchant : merchants.values()) {
			updateByOwner(merchant, null);
		}
	}

	@Override
	protected final void jsonDelete() {
		File root = new File(GCoreLegacy.inst().getDataRootFolder() + "/supremeshops_merchants/");
		if (root.exists() && root.isDirectory()) {
			root.delete();
		}
	}

	// MySQL
	@Override
	public final String getMySQLTable() {
		return "supremeshops_merchants";
	}

	@Override
	protected final Query getMySQLInitQuery() {
		return new Query("CREATE TABLE IF NOT EXISTS `" + getMySQLTable() + "`("
				+ "`id` VARCHAR(100) NOT NULL,"
				+ "`type` VARCHAR(100) NOT NULL,"
				+ "`owner` VARCHAR(100) NOT NULL,"
				+ "`data` LONGTEXT NOT NULL,"
				+ "PRIMARY KEY(`id`)"
				+ ") ENGINE=`InnoDB` DEFAULT CHARSET=?;", "utf8");
	}

	@Override
	protected final void mysqlPull() {
		// load
		getDataManager().performMySQLGetQuery(new Query("SELECT * FROM `" + getMySQLTable() + "`;"), set -> {
			merchants.clear();
			while (set.next()) {
				String id = set.getString("id");
				try {
					MerchantType type = Utils.valueOfOrNull(MerchantType.class, set.getString("type"));
					Constructor<? extends Merchant> constructor = type.getMerchantClass().getDeclaredConstructor(String.class);
					constructor.setAccessible(true);
					Merchant merchant = constructor.newInstance(id);
					merchant.mysqlPull(set);
					if (!merchant.hasLoadError()) {
						merchants.put(merchant.getId().toLowerCase(), merchant);
					}
				} catch (Throwable exception) {
					exception.printStackTrace();
					SupremeShops.inst().error("Couldn't load merchant " + id + " from database");
				}
			}
		});
		// update by block/sign
		for (Merchant merchant : merchants.values()) {
			if (merchant instanceof BlockMerchant) {
				blockMerchantsByBlock.put(((BlockMerchant) merchant).getBlock(), (BlockMerchant) merchant);
				blockMerchantsBySign.put(((BlockMerchant) merchant).getSign(), (BlockMerchant) merchant);
			}
			// update by sign
			if (merchant instanceof SignMerchant) {
				signMerchantsBySign.put(((SignMerchant) merchant).getSign(), (SignMerchant) merchant);
			}
		}
		// update by player
		merchantsByOwners.clear();
		for (Merchant merchant : merchants.values()) {
			updateByOwner(merchant, null);
		}
	}

	@Override
	protected final void mysqlDelete() {
		getDataManager().performMySQLUpdateQuery(new Query("DELETE FROM `" + getMySQLTable() + "`;"));
	}

}
