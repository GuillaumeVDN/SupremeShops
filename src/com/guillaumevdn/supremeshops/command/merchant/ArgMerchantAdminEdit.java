package com.guillaumevdn.supremeshops.command.merchant;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.npc.Npc;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.NpcInput;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.merchant.MerchantSelectionGUI;
import com.guillaumevdn.supremeshops.gui.merchant.management.MerchantManagementGUI;
import com.guillaumevdn.supremeshops.module.manageable.MerchantManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;

public class ArgMerchantAdminEdit extends CommandArgument {

	// base
	public ArgMerchantAdminEdit() {
		super(SupremeShops.inst(), Utils.asList("adminedit", "aedit"), "edit a merchant in admin mode", SSPerm.SUPREMESHOPS_COMMAND_MERCHANT_ADMINEDIT, true);
	}

	// call
	@Override
	public void perform(final CommandCall call) {
		final Player player = call.getSenderAsPlayer();
		SSLocale.MSG_SUPREMESHOPS_PLAYERTRADEADMINEDITMERCHANTINPUT.send(player);
		// open selection GUI, but they're free to close it
		new MerchantSelectionGUI(null, false, player, true, null, -1) {
			@Override
			protected void onSelect(Merchant merchant) {
				// remove location input
				GCoreLegacy.inst().getLocationInputs().remove(player);
				// edit merchant
				new MerchantManagementGUI(merchant, Utils.asSet(MerchantManagementPermission.values()), player, null, -1).open(player);
			}
		}.open(player);
		// add npc input
		GCoreLegacy.inst().getNpcInputs().put(player, new NpcInput() {
			@Override
			public void onChoose(Player player, Npc value) {
				// invalid merchant
				Merchant merchant = SupremeShops.inst().getGeneralManager().getMerchantByNpc(value);
				if (merchant == null) {
					SSLocale.MSG_SUPREMESHOPS_PLAYERTRADECREATEMERCHANTINVALID.send(player);
					return;
				}
				// edit merchant
				new MerchantManagementGUI(merchant, Utils.asSet(MerchantManagementPermission.values()), player, null, -1).open(player);
			}
		});
	}

}
