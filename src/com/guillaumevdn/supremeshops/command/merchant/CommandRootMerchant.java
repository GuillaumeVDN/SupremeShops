package com.guillaumevdn.supremeshops.command.merchant;

import com.guillaumevdn.gcorelegacy.lib.command.CommandRoot;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;

public class CommandRootMerchant extends CommandRoot {

	// base
	public CommandRootMerchant() {
		super(SupremeShops.inst(), Utils.asList("suprememerchants", "suprememerchant", "smerchants", "smerchant"), "manage SupremeShops merchants", null, true);
	}

}
