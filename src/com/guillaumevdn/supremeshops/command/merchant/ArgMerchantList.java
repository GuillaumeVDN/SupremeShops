package com.guillaumevdn.supremeshops.command.merchant;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.command.Param;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.gui.merchant.MerchantListAdminGUI;

public class ArgMerchantList extends CommandArgument {

	// base
	private static Param paramOwner = new Param(Utils.asList("owner", "o"), "owner name or ADMIN", null, false, false);

	public ArgMerchantList() {
		super(SupremeShops.inst(), Utils.asList("list"), "list merchants", SSPerm.SUPREMESHOPS_COMMAND_MERCHANT_LIST, true, paramOwner);
	}

	// call
	@Override
	public void perform(CommandCall call) {
		// has owner
		Player sender = call.getSenderAsPlayer();
		if (paramOwner.has(call)) {
			// admin
			String str = paramOwner.getString(call);
			if (str != null && str.trim().equalsIgnoreCase("admin")) {
				new MerchantListAdminGUI(SupremeShops.inst().getData().getMerchants().getElements(null, null, false, ElementRemotePolicy.MIGHT_BE), null, sender).open(sender);
			}
			// owner
			else {
				OfflinePlayer owner = paramOwner.getOfflinePlayer(call, false);
				if (owner != null) {
					new MerchantListAdminGUI(SupremeShops.inst().getData().getMerchants().getElements(new UserInfo(owner), null, false, ElementRemotePolicy.MIGHT_BE), owner, sender).open(sender);
				}
			}
		}
		// no owner
		else {
			new MerchantListAdminGUI(SupremeShops.inst().getData().getMerchants().getAll().values(), null, sender).open(sender);
		}
	}

}
