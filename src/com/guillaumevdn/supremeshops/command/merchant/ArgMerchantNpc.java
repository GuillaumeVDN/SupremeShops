package com.guillaumevdn.supremeshops.command.merchant;

import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;

public class ArgMerchantNpc extends CommandArgument {

	public ArgMerchantNpc() {
		super(SupremeShops.inst(), Utils.asList("npc"), "npc merchant commands", SSPerm.SUPREMESHOPS_COMMAND_MERCHANT_NPC, false);
	}

}
