package com.guillaumevdn.supremeshops.command.merchant;

import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.command.Param;
import com.guillaumevdn.gcorelegacy.lib.messenger.Messenger;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.merchant.MerchantCreateEvent;
import com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchant;
import com.guillaumevdn.supremeshops.module.merchant.RentableNpcMerchant;
import com.guillaumevdn.supremeshops.util.CreationTax;

public class ArgMerchantNpcCreate extends CommandArgument {

	// base
	private static final Param paramAdmin = new Param(Utils.asList("admin"), null, SSPerm.SUPREMESHOPS_EDIT_ADMIN, false, false);
	private static final Param paramRent = new Param(Utils.asList("rent"), null, SSPerm.SUPREMESHOPS_EDIT_ADMIN, false, false);

	static {
		paramAdmin.setIncompatibleWith(paramRent);
		paramRent.setIncompatibleWith(paramAdmin);
	}

	public ArgMerchantNpcCreate() {
		super(SupremeShops.inst(), Utils.asList("create", "new", "add"), "create a npc merchant", SSPerm.SUPREMESHOPS_COMMAND_MERCHANT_NPC_CREATE, true, paramAdmin, paramRent);
	}

	// call
	@Override
	public void perform(CommandCall call) {
		// no npc manager
		if (GCoreLegacy.inst().getNpcManager() == null) {
			Messenger.send(call.getSender(), Messenger.Level.SEVERE_ERROR, SupremeShops.inst().getName(), "No GCoreLegacy NPCs manager found ; did you install ProtocolLib ?");
			return;
		}
		// rent merchant
		Player player = call.getSenderAsPlayer();
		if (paramRent.has(call)) {
			// can't create
			if (!paramRent.ensureAuthorization(call)) {
				return;
			}
			// create merchant
			String merchantId = "rent_npc_" + UUID.randomUUID().toString().replace("-", "").substring(0, 5);
			RentableNpcMerchant merchant = new RentableNpcMerchant(merchantId, player.getLocation().clone());
			// event
			MerchantCreateEvent ev = new MerchantCreateEvent(merchant, player);
			Bukkit.getPluginManager().callEvent(ev);
			if (ev.isCancelled()) {
				return;
			}
			// create
			SupremeShops.inst().getData().getMerchants().add(merchant);
			SupremeShops.inst().getGeneralManager().spawnMerchantsNpcs();
			SSLocale.MSG_SUPREMESHOPS_CREATEMERCHANTRENT.send(player);
			SupremeShops.inst().pluginLog(null, merchant, null, null, null, "Created merchant");
		}
		// admin merchant
		else if (paramAdmin.has(call)) {
			// can't create
			if (!paramAdmin.ensureAuthorization(call)) {
				return;
			}
			// create merchant
			String merchantId = "npc_" + UUID.randomUUID().toString().replace("-", "").substring(0, 5);
			NpcMerchant merchant = new NpcMerchant(merchantId, null, player.getLocation().clone());
			// event
			MerchantCreateEvent ev = new MerchantCreateEvent(merchant, player);
			Bukkit.getPluginManager().callEvent(ev);
			if (ev.isCancelled()) {
				return;
			}
			// create
			SupremeShops.inst().getData().getMerchants().add(merchant);
			SupremeShops.inst().getGeneralManager().spawnMerchantsNpcs();
			SSLocale.MSG_SUPREMESHOPS_CREATEMERCHANTADMIN.send(player);
			SupremeShops.inst().pluginLog(null, merchant, null, null, null, "Created merchant");
		}
		// regular merchant
		else {
			// can't create
			if (!SSPerm.SUPREMESHOPS_MERCHANT_CREATE_NPC.has(player)) {
				GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
				}
				return;
			}
			// max merchants reached
			int amount = SupremeShops.inst().getData().getMerchants().getElements(new UserInfo(player), null, false, ElementRemotePolicy.MIGHT_BE).size();
			int max = SupremeShops.inst().getModuleManager().getMaxShops(player);
			if (amount >= max) {
				SSLocale.MSG_SUPREMESHOPS_MAXMERCHANTS.send(player);
				// sound
				if (SupremeShops.inst().getModuleManager().getGuiClickErrorSound() != null) {
					SupremeShops.inst().getModuleManager().getGuiClickErrorSound().play(player);
				}
				return;
			}
			// create merchant
			String merchantId = "npc_" + UUID.randomUUID().toString().replace("-", "").substring(0, 5);
			NpcMerchant merchant = new NpcMerchant(merchantId, new UserInfo(player), player.getLocation().clone());
			// isn't allowed
			if (!SupremeShops.inst().getModuleManager().canMerchantExist(player, merchant.asFake(), true)) {
				return;
			}
			// can't pay taxes
			double taxMoney = 0d;
			for (CreationTax tax : SupremeShops.inst().getModuleManager().getMerchantCreationTaxes().values()) {
				if (tax.getConditions().isValid(player, merchant, false)) {
					taxMoney += tax.getVaultMoneyTax(player);
				}
			}
			if (taxMoney > 0d && GCoreLegacy.inst().getEconomyHandler().get(player) < taxMoney) {
				SSLocale.MSG_SUPREMESHOPS_NOTENOUGHMONEYCREATIONTAX.send(player, "{money}", Utils.round5(taxMoney));
				return;
			}
			// event
			MerchantCreateEvent ev = new MerchantCreateEvent(merchant, player);
			Bukkit.getPluginManager().callEvent(ev);
			if (ev.isCancelled()) {
				return;
			}
			// pay taxes
			if (taxMoney > 0d) {
				GCoreLegacy.inst().getEconomyHandler().take(player, taxMoney);
				SSLocale.MSG_SUPREMESHOPS_PAIDCREATIONTAX.send(player, "{money}", Utils.round5(taxMoney));
			}
			// sound
			if (SupremeShops.inst().getModuleManager().getGuiClickSound() != null) {
				SupremeShops.inst().getModuleManager().getGuiClickSound().play(player);
			}
			// create
			SupremeShops.inst().getData().getMerchants().add(merchant);
			SupremeShops.inst().getGeneralManager().spawnMerchantsNpcs();
			SSLocale.MSG_SUPREMESHOPS_CREATEMERCHANT.send(player, "{amount}", amount + 1, "{max}", max);
			SupremeShops.inst().pluginLog(null, merchant, null, null, null, "Created merchant");
		}
	}

}
