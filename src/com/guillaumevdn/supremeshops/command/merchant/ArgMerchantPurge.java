package com.guillaumevdn.supremeshops.command.merchant;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.bukkit.OfflinePlayer;
import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.command.Param;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.DestroyCause;

public class ArgMerchantPurge extends CommandArgument {

	// base
	private static Param paramOwner = new Param(Utils.asList("owner", "o"), "owner name or ADMIN", null, false, true);

	public ArgMerchantPurge() {
		super(SupremeShops.inst(), Utils.asList("purge"), "delete merchants from a player", SSPerm.SUPREMESHOPS_COMMAND_MERCHANT_PURGE, false, paramOwner);
	}

	// call
	private List<CommandSender> confirm = new ArrayList<CommandSender>();

	@Override
	public void perform(final CommandCall call) {
		// get owner
		OfflinePlayer owner = null;
		boolean admin = false;
		if (paramOwner.has(call) && paramOwner.getString(call).trim().equalsIgnoreCase("admin")) {
			admin = true;
		} else {
			owner = paramOwner.getOfflinePlayer(call, false);
		}
		if (!admin && owner == null) return;
		// confirm
		if (!confirm.contains(call.getSender())) {
			SSLocale.MSG_SUPREMESHOPS_CONFIRMERCHANTSPURGE.send(call.getSender(), "{owner}", admin ? "[ADMIN]" : owner.getName());
			confirm.add(call.getSender());
			new BukkitRunnable() {
				@Override
				public void run() {
					confirm.remove(call.getSender());
				}
			}.runTaskLater(SupremeShops.inst(), 20L * 15L);
			return;
		}
		// destroy merchants
		Set<Merchant> merchantsToDestroy = new HashSet<Merchant>(SupremeShops.inst().getData().getMerchants().getElements(admin ? null : new UserInfo(owner), null, false, ElementRemotePolicy.MIGHT_BE));
		Set<Shop> shopsToDestroy = new HashSet<Shop>();
		if (!merchantsToDestroy.isEmpty()) {
			for (Merchant merchant : merchantsToDestroy) {
				shopsToDestroy.addAll(merchant.destroy(DestroyCause.PURGE_PLAYER_COMMAND, false));
			}
			SupremeShops.inst().getData().getShops().deleteAll(shopsToDestroy);
		}
		// destroy shops
		Set<Merchant> moreMerchantsToPush = new HashSet<Merchant>();
		if (!shopsToDestroy.isEmpty()) {
			for (Shop shop : shopsToDestroy) {
				moreMerchantsToPush.addAll(shop.destroy(DestroyCause.PURGE_PLAYER_COMMAND, false));
			}
			SupremeShops.inst().getData().getShops().deleteAll(shopsToDestroy);
		}
		// push remaining merchants
		moreMerchantsToPush.removeAll(merchantsToDestroy);
		if (!moreMerchantsToPush.isEmpty()) {
			SupremeShops.inst().getData().getMerchants().pullAsync(moreMerchantsToPush, null);
		}
		SSLocale.MSG_SUPREMESHOPS_MERCHANTSPURGED.send(call.getSender(), "{owner}", admin ? "[ADMIN]" : owner.getName());
	}

}
