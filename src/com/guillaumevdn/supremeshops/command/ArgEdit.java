package com.guillaumevdn.supremeshops.command;

import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.editor.EditorMainGUI;

public class ArgEdit extends CommandArgument {

	public ArgEdit() {
		super(SupremeShops.inst(), Utils.asList("edit", "editor", "modify", "change", "admin"), "edit things in-game", SSPerm.SUPREMESHOPS_COMMAND_EDIT, true);
	}

	@Override
	public void perform(CommandCall call) {
		new EditorMainGUI().open(call.getSenderAsPlayer());
	}

}
