package com.guillaumevdn.supremeshops.command;

import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.ItemWhitelistEditionGUI;

public class ArgItemWhitelist extends CommandArgument {

	// base
	public ArgItemWhitelist() {
		super(SupremeShops.inst(), Utils.asList("whitelist", "wl"), "see the items whitelist", SSPerm.SUPREMESHOPS_COMMAND_ITEM_WHITELIST, true);
	}

	// call
	@Override
	public void perform(CommandCall call) {
		new ItemWhitelistEditionGUI().open(call.getSenderAsPlayer());
	}

}
