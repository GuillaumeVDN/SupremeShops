package com.guillaumevdn.supremeshops.command;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.messenger.Messenger;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;

public class ArgItemResetvalue extends CommandArgument {

	// base
	public ArgItemResetvalue() {
		super(SupremeShops.inst(), Utils.asList("resetvalue", "resetval"), "reset the value of the item you're holding", SSPerm.SUPREMESHOPS_COMMAND_ITEM_RESETVALUE, true);
	}

	// call
	@Override
	public void perform(CommandCall call) {
		Player player = call.getSenderAsPlayer();
		try {
			int i = 0;
			if (SupremeShops.inst().getModuleManager().getItemValueManager().resetItemValue(new ItemData(player.getItemInHand()), ObjectSide.GIVING)) ++i;
			if (SupremeShops.inst().getModuleManager().getItemValueManager().resetItemValue(new ItemData(player.getItemInHand()), ObjectSide.TAKING)) ++i;
			if (i > 0) {
				Messenger.send(player, Messenger.Level.NORMAL_SUCCESS, SupremeShops.inst().getName(), "The value was reset for the item in your hand.");
			} else {
				Messenger.send(player, Messenger.Level.SEVERE_ERROR, SupremeShops.inst().getName(), "There's nothing to reset for the item in your hand.");
			}
		} catch (Throwable exception) {
			exception.printStackTrace();
			Messenger.send(player, Messenger.Level.SEVERE_ERROR, SupremeShops.inst().getName(), "Couldn't reset the value for the item in your hand.");
		}
	}

}
