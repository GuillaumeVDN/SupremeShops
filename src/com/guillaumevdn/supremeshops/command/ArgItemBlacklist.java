package com.guillaumevdn.supremeshops.command;

import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.ItemBlacklistEditionGUI;

public class ArgItemBlacklist extends CommandArgument {

	// base
	public ArgItemBlacklist() {
		super(SupremeShops.inst(), Utils.asList("blacklist", "bl"), "edit the items blacklist", SSPerm.SUPREMESHOPS_COMMAND_ITEM_BLACKLIST, true);
	}

	// call
	@Override
	public void perform(CommandCall call) {
		new ItemBlacklistEditionGUI().open(call.getSenderAsPlayer());
	}

}
