package com.guillaumevdn.supremeshops.command;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.Chest;
import org.bukkit.block.DoubleChest;
import org.bukkit.command.CommandSender;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.command.Param;
import com.guillaumevdn.gcorelegacy.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.messenger.Messenger;
import com.guillaumevdn.gcorelegacy.lib.messenger.Replacer;
import com.guillaumevdn.gcorelegacy.lib.util.Pair;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.versioncompat.Compat;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.itemvalue.ItemValueSetting;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;

public class ArgAdmingen extends CommandArgument {

	private static final Param paramFile = new Param(Utils.asList("file", "f"), "file name", null, false, false);
	private static final Param paramFromchestid = new Param(Utils.asList("fromchestid"), "admin shop id to generate", null, true, false);
	private static final Param paramMixed = new Param(Utils.asList("mixed"), null, null, false, false);

	static {
		paramFile.setIncompatibleWith(paramFromchestid);
		paramFromchestid.setIncompatibleWith(paramFile);
	}

	public ArgAdmingen() {
		super(SupremeShops.inst(), Utils.asList("admingen", "admingeneration", "generateadmin"), "generate an admin shop", SSPerm.SUPREMESHOPS_COMMAND_ADMINGEN, false, paramFile, paramFromchestid, paramMixed);
	}

	@Override
	public void perform(CommandCall call) {
		// from file
		final boolean mixed = paramMixed.has(call);
		CommandSender sender = call.getSender();
		if (paramFile.has(call)) {
			// get valid file
			String fileName = paramFile.getString(call);
			File file = fileName == null ? null : new File(SupremeShops.inst().getDataFolder() + "/shops/admin/generate/" + fileName);
			if (file == null || !file.exists() || !file.getName().toLowerCase().endsWith(".txt")) {
				sender.sendMessage(Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "The file must be an existing .txt file in </plugins/SupremeShops/shops/admin/generate/>."));
				return;
			}
			// generate shops
			int lineNb = 0;
			String line = null;
			try {
				// decode lines
				List<Trade> trades = new ArrayList<Trade>();
				BufferedReader reader = new BufferedReader(new FileReader(file));
				Trade trade = null;
				while ((line = reader.readLine()) != null) {
					++lineNb;
					// remove comments and whitespaces
					int commentIndex = line.indexOf('#');
					if (commentIndex != -1) line = line.substring(0, commentIndex);
					line = line.trim();
					// begin new trade
					if (line.startsWith("-")) {
						trades.add(trade = new Trade());
					}
					// set icon
					else if (line.toLowerCase().startsWith("icon ")) {
						trade.icon = decodeItem(line.substring("icon ".length()));
					}
					// set slot
					else if (line.toLowerCase().startsWith("slot ")) {
						trade.slot = Integer.parseInt(line.substring("slot ".length()).trim());
					}
					// set take object
					else if (line.toLowerCase().startsWith("take ")) {
						line = line.substring("take ".length());
						if (line.toLowerCase().startsWith("item ")) {
							TradeObjectItem object = new TradeObjectItem();
							object.side = ObjectSide.TAKING;
							object.item = decodeItem(line.substring("item ".length()));
							trade.objects.add(object);
						} else if (line.toLowerCase().startsWith("money ")) {
							TradeObjectMoney object = new TradeObjectMoney();
							object.side = ObjectSide.TAKING;
							if (SupremeShops.inst().getModuleManager().getItemValueManager().getAdminSetting().equals(ItemValueSetting.DISABLED)) object.amount = Double.parseDouble(line.substring("money ".length()).trim());
							trade.objects.add(object);
						} else if (line.toLowerCase().startsWith("playerpoints ")) {
							TradeObjectPlayerPointsPoints object = new TradeObjectPlayerPointsPoints();
							object.amount = Integer.parseInt(line.substring("playerpoints".length()).trim());
							object.side = ObjectSide.TAKING;
							trade.objects.add(object);
						} else if (line.toLowerCase().startsWith("tokenenchant ")) {
							TradeObjectTokenEnchantTokens object = new TradeObjectTokenEnchantTokens();
							object.amount = Integer.parseInt(line.substring("tokenenchant".length()).trim());
							object.side = ObjectSide.TAKING;
							trade.objects.add(object);
						} else if (line.toLowerCase().startsWith("xp ")) {
							TradeObjectXpLevel object = new TradeObjectXpLevel();
							object.amount = Integer.parseInt(line.substring("xp ".length()).trim());
							object.side = ObjectSide.TAKING;
							trade.objects.add(object);
						} else if (line.toLowerCase().startsWith("commands ")) {
							TradeObjectCommands object = new TradeObjectCommands();
							object.side = ObjectSide.TAKING;
							object.commands.addAll(Utils.split(",", line.substring("commands ".length()), false));
							trade.objects.add(object);
						}
					}
					// set give object
					else if (line.toLowerCase().startsWith("give ")) {
						line = line.substring("give ".length());
						if (line.toLowerCase().startsWith("item ")) {
							TradeObjectItem object = new TradeObjectItem();
							object.side = ObjectSide.GIVING;
							object.item = decodeItem(line.substring("item ".length()));
							trade.objects.add(object);
						} else if (line.toLowerCase().startsWith("money ")) {
							TradeObjectMoney object = new TradeObjectMoney();
							object.side = ObjectSide.GIVING;
							if (SupremeShops.inst().getModuleManager().getItemValueManager().getAdminSetting().equals(ItemValueSetting.DISABLED)) object.amount = Double.parseDouble(line.substring("money ".length()));
							trade.objects.add(object);
						} else if (line.toLowerCase().startsWith("playerpoints ")) {
							TradeObjectPlayerPointsPoints object = new TradeObjectPlayerPointsPoints();
							object.amount = Integer.parseInt(line.substring("playerpoints".length()).trim());
							object.side = ObjectSide.GIVING;
							trade.objects.add(object);
						} else if (line.toLowerCase().startsWith("tokenenchant ")) {
							TradeObjectTokenEnchantTokens object = new TradeObjectTokenEnchantTokens();
							object.amount = Integer.parseInt(line.substring("tokenenchant".length()).trim());
							object.side = ObjectSide.GIVING;
							trade.objects.add(object);
						} else if (line.toLowerCase().startsWith("xp ")) {
							TradeObjectXpLevel object = new TradeObjectXpLevel();
							object.amount = Integer.parseInt(line.substring("xp ".length()).trim());
							object.side = ObjectSide.GIVING;
							trade.objects.add(object);
						} else if (line.toLowerCase().startsWith("commands ")) {
							TradeObjectCommands object = new TradeObjectCommands();
							object.side = ObjectSide.GIVING;
							object.commands.addAll(Utils.split(",", line.substring("commands ".length()), false));
							trade.objects.add(object);
						}
					}
				}
				reader.close();
				// generate
				String id = Utils.getFileNameWithoutExtension(file).toLowerCase();
				generateFiles(sender, id, trades, mixed);
			} catch (Throwable exception) {
				sender.sendMessage(Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "An unknown error occured while generating the admin shops : " + exception.getClass().getName() + ", " + exception.getMessage()) + ", at line " + lineNb + " (" + line + ")");
			}
		}
		// from inv
		else {
			// no advanced admin economy
			if (!SupremeShops.inst().getModuleManager().getItemValueManager().getAdminSetting().equals(ItemValueSetting.ENABLED)) {
				sender.sendMessage(Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "Items money value isn't enabled in config for admin shops."));
				return;
			}
			// get id
			final String id = paramFromchestid.getStringAlphanumeric(call);
			if (id != null) {
				// ask for chest input
				sender.sendMessage(Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "<Click on the chest> you want to create an admin shop from, or <sneak to cancel>."));
				final Player player = call.getSenderAsPlayer();
				Bukkit.getPluginManager().registerEvents(new Listener() {
					@EventHandler(priority = EventPriority.LOWEST, ignoreCancelled = true)
					public void event(PlayerInteractEvent event) {
						if (event.getPlayer().equals(player)) {
							Block block = event.getClickedBlock();
							if (block != null) {
								// cancel event and unregister events
								event.setCancelled(true);
								HandlerList.unregisterAll(this);
								// generate
								try {
									// double chest
									Inventory inv = null;
									if (Utils.instanceOf(block.getState(), DoubleChest.class)) {
										inv = ((DoubleChest) block.getState()).getInventory();
									} else if (Utils.instanceOf(block.getState(), Chest.class)) {
										inv = ((Chest) block.getState()).getInventory();
									}
									// not a chest
									if (inv == null) {
										player.sendMessage(Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "This isn't a valid chest."));
										return;
									}
									// generate trades
									List<Trade> trades = new ArrayList<Trade>();
									for (int slot = 0; slot < inv.getSize(); ++slot) {
										// invalid item
										ItemStack item = inv.getItem(slot);
										if (item == null || Mat.fromItem(item).isAir()) {
											continue;
										}
										// create trade and set slot
										Trade trade = new Trade();
										trades.add(trade);
										trade.slot = slot;
										trade.icon = new ItemData(item);
										// add money object
										TradeObjectMoney moneyObject = new TradeObjectMoney();
										moneyObject.side = ObjectSide.TAKING;
										trade.objects.add(moneyObject);
										// add item object
										TradeObjectItem itemObject = new TradeObjectItem();
										itemObject.side = ObjectSide.GIVING;
										itemObject.item = new ItemData(item);
										trade.objects.add(itemObject);
									}
									// generate files
									generateFiles(player, id, trades, mixed);
								} catch (Throwable exception) {
									exception.printStackTrace();
									String message = Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "An unknown error occured while generating the admin shops : " + exception.getClass().getName() + ", " + exception.getMessage());
									player.sendMessage(message);
									Bukkit.getConsoleSender().sendMessage(message);
								}
							}
						}
					}
					@EventHandler(priority = EventPriority.NORMAL /* normal because sneak cancel GUI delay is LOWEST*/, ignoreCancelled = true)
					public void event(PlayerToggleSneakEvent event) {
						if (event.getPlayer().equals(player)) {
							// unregister events
							HandlerList.unregisterAll(this);
							// message
							player.sendMessage(Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "Cancelled admin shop generation."));
						}
					}
				}, SupremeShops.inst());
			}
		}
	}

	private static ItemData decodeItem(String raw) throws Throwable {
		// decode params
		raw = raw.trim();
		String[] params = raw.split("-");
		Mat type = null;
		int amount = 1;
		boolean unbreakable = false;
		String name = null;
		List<String> lore = null;
		Map<Enchantment, Integer> enchants = new HashMap<Enchantment, Integer>();
		for (int i = 0; i < params.length; ++i) {
			// type and amount
			String param = params[i];
			if (i == 0) {
				String[] split = param.split(" ");
				type = Mat.valueOf(split[0]);
				amount = Integer.parseInt(split[1]);
			}
			// other params
			else {
				Pair<String, String> pair = Utils.separateRootAtChar(param, ' ');
				// durability
				if (pair.getA().trim().toLowerCase().equals("durability")) {
					type = type.cloneWithDurability(Integer.parseInt(pair.getB().trim()));
				}
				// unbreakable
				else if (pair.getA().trim().toLowerCase().equals("unbreakable")) {
					unbreakable = true;
				}
				// name
				else if (pair.getA().trim().toLowerCase().equals("name")) {
					name = pair.getB().trim();
				}
				// lore
				else if (pair.getA().trim().toLowerCase().equals("lore")) {
					lore = Utils.split(",", pair.getB().trim(), true);
				}
				// enchant
				else if (pair.getA().trim().toLowerCase().equals("enchant")) {
					String[] split = pair.getB().trim().split(",");
					enchants.put(Enchantment.getByName(split[0]), Integer.parseInt(split[1]));
				}
			}
		}
		// build item
		ItemData item = new ItemData(UUID.randomUUID().toString().replace("-", "").substring(0, 5));
		item.setType(type);
		item.setUnbreakable(unbreakable);
		item.setAmount(amount);
		item.setName(name);
		item.setLore(lore);
		for (Enchantment enchant : enchants.keySet()) {
			item.setEnchant(enchant, enchants.get(enchant));
		}
		// done
		return type == null ? null : item;
	}

	private static void writeTrade(BufferedWriter writer, String spaces, Trade trade) throws Throwable {
		// write settings
		writer.write(spaces + "" + trade.id + ":\n");
		writer.write(spaces + "  name: '" + Utils.capitalizeFirstLetter(trade.name.replace("'", "''")) + "'\n");
		/*if (trade.slot >= 0 && trade.slot < 54) {
			writer.write(spaces + "  slot: " + trade.slot + "\n");
		}*/
		// write icon
		/*if (trade.icon == null) {
			Mat icon = null;
			for (TradeObject object : trade.objects) {
				if (object instanceof TradeObjectItem) {
					if (((TradeObjectItem) object).item == null) continue;
					icon = ((TradeObjectItem) object).item.getType();
					break;
				}
			}
			if (icon == null) icon = Mat.EMERALD;
			writer.write(spaces + "  icon: " + icon.toString() + "\n");
		} else if (trade.icon.getAmount() == 1 && trade.icon.getType().getDurability() == 0 && !trade.icon.isUnbreakable() && (trade.icon.getName() == null ? true : trade.icon.getName().isEmpty()) && (trade.icon.getLore() == null ? true : trade.icon.getLore().isEmpty()) && trade.icon.getEnchants().isEmpty()) {
			writer.write(spaces + "  icon: " + trade.icon.getType().toString() + "\n");
		} else {
			writeItem(writer, "    ", "item", trade.icon);
		}*/
		// write objects
		if (!trade.objects.isEmpty()) {
			writer.write(spaces + "  objects:\n");
			int objectNb = 0;
			for (TradeObject object : trade.objects) {
				// id and side
				writer.write(spaces + "    " + ++objectNb + ":\n");
				writer.write(spaces + "      side: " + object.side.name() + "\n");
				// item
				if (object instanceof TradeObjectItem) {
					writer.write(spaces + "      type: " + ObjectType.ITEM.getId() + "\n");
					writeItem(writer, "        ", "item", ((TradeObjectItem) object).item);
				}
				// money
				else if (object instanceof TradeObjectMoney) {
					writer.write(spaces + "      type: " + ObjectType.VAULT_MONEY.getId() + "\n");
					if (SupremeShops.inst().getModuleManager().getItemValueManager().getAdminSetting().equals(ItemValueSetting.DISABLED)) writer.write(spaces + "      custom_amount: " + ((TradeObjectMoney) object).amount + "\n");
				}
				// xp
				else if (object instanceof TradeObjectXpLevel) {
					writer.write(spaces + "      type: " + ObjectType.XP_LEVEL.getId() + "\n");
					writer.write(spaces + "      amount: " + ((TradeObjectXpLevel) object).amount + "\n");
				}
				// PlayerPoints points
				else if (object instanceof TradeObjectPlayerPointsPoints) {
					writer.write(spaces + "      type: " + ObjectType.PLAYERPOINTS_POINTS.getId() + "\n");
					writer.write(spaces + "      amount: " + ((TradeObjectPlayerPointsPoints) object).amount + "\n");
				}
				// xp
				else if (object instanceof TradeObjectTokenEnchantTokens) {
					writer.write(spaces + "      type: " + ObjectType.TOKENENCHANT_TOKENS.getId() + "\n");
					writer.write(spaces + "      amount: " + ((TradeObjectTokenEnchantTokens) object).amount + "\n");
				}
				// commands
				else if (object instanceof TradeObjectCommands) {
					writer.write(spaces + "      commands:\n");
					for (String command : ((TradeObjectCommands) object).commands) {
						writer.write(spaces + "        - '" + command.replace("'", "''") + "'\n");
					}
				}
			}
		}
	}

	private static void writeItem(BufferedWriter writer, String spaces, String id, ItemData item) throws Throwable {
		writer.write(spaces + id + ":\n");
		// null item
		if (item == null) return;
		// data
		if (item.getSlot() >= 0) writer.write(spaces + "  slot: " + item.getSlot() + "\n");
		// item (potion damage is saved in durability)
		String type = item.getType().toString();
		if (type.contains(":") || type.contains("(")) {
			GCoreLegacy.inst().warning("Exported mat '" + type + "'. Name '" + item.getType().getModernName() + ", legacy " + item.getType().getLegacyName() + "', toString() '" + item.getType().toString() + "', material '" + (item.getType().exists() ? "null" : String.valueOf(item.getType().getCurrentMaterial())) + "'. Please notify the developer with /gcore support");
		}
		writer.write(spaces + "  type: " + type + "\n");
		if (item.getType().getDurability() != (short) 0) writer.write(spaces + "  durability: " + item.getType().getDurability() + "\n");
		if (item.isUnbreakable()) writer.write(spaces + "  unbreakable: true\n");
		// amount
		writer.write(spaces + "  amount: " + item.getAmount() + "\n");
		// meta
		if (item.getName() != null) writer.write(spaces + "  name: '" + item.getName().replace("'", "''") + "'\n");
		if (item.getLore() != null && !item.getLore().isEmpty()) {
			writer.write(spaces + "  lore:\n");
			for (String line : item.getLore()) {
				writer.write(spaces + "    - '" + line.replace("'", "''") + "'\n");
			}
		}
		// enchants
		if (!item.getEnchants().isEmpty()) {
			writer.write(spaces + "  lore:\n");
			for (Enchantment enchant : item.getEnchants().keySet()) {
				writer.write(spaces + "    - '" + enchant.getName() + "," + item.getEnchants().get(enchant) + "'\n");
			}
		}
		// nbt
		try {
			if (item.getCustomNbt() != null) {
				writer.write(spaces + "  nbt: '" + Compat.INSTANCE.serializeNbt(item.getCustomNbt()).replace("'", "''") + "'\n");
			}
		} catch (Throwable exception) {
			exception.printStackTrace();
		}
	}

	private void generateFiles(CommandSender sender, String id, List<Trade> trades, boolean mixed) throws Throwable {
		// read settings
		YMLConfiguration settings = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/shops/admin/generate/settings.yml"), "shops/admin/generate/settings.yml", true, true);
		String nameFormat = settings.getString("name_format", "&c{take} &7-> &a{give}");
		String nameFormatItem = settings.getString("name_format_item", "{amount} {type}");
		String nameFormatMoney = settings.getString("name_format_money", "money");
		String nameFormatMoneyCustom = settings.getString("name_format_money_custom", "{amount}$");
		String guiItemSingleName = settings.getString("gui_item_single_name", "&7{name}");
		List<String> guiItemSingleLore = settings.getList("gui_item_single_lore", Utils.emptyList());
		String guiItemMixedName = settings.getString("gui_item_mixed_name", "&7{name} &7(left click)");
		List<String> guiItemMixedLore = settings.getList("gui_item_mixed_lore", Utils.asList("&7{name_reverse} &7(right click)"));
		// initialize shop file and writer
		File shopWriterFile = new File(SupremeShops.inst().getDataFolder() + "/shops/admin/generate/shops/" + id + ".yml");
		shopWriterFile.getParentFile().mkdirs();
		shopWriterFile.createNewFile();
		BufferedWriter shopWriter = new BufferedWriter(new FileWriter(shopWriterFile));
		shopWriter.write("#----------------------------------------------------------------------------------------------------\n"
				+ "# Admin shop configuration file (file name without the extension is the admin shop identifier)\n"
				+ "# This file was generated automatically from file using the admin generation command\n"
				+ "#----------------------------------------------------------------------------------------------------\n\n"
				+ "# Content\n\ntrades:\n\n");
		// initialize GUI file and writer
		File guiWriterFile = new File(SupremeShops.inst().getDataFolder() + "/shops/admin/generate/guis/adminshop_" + id + ".yml");
		guiWriterFile.getParentFile().mkdirs();
		guiWriterFile.createNewFile();
		BufferedWriter guiWriter = new BufferedWriter(new FileWriter(guiWriterFile));
		guiWriter.write("#----------------------------------------------------------------------------------------------------\n"
				+ "# GUI configuration file (file name without the extension is the GUI identifier)\n"
				+ "# This file was generated automatically from file using the admin generation command\n"
				+ "#----------------------------------------------------------------------------------------------------\n\n"
				+ "# Settings\n"
				+ "name: 'Shops > " + Utils.capitalizeFirstLetter(id.replace("'", "''")) + "'\n"
				+ "size: 54\n\n"
				+ "# Content : items of this GUI\n"
				+ "content:\n"
				+ "  \n"
				+ "  # open the player shops menu for the player\n"
				+ "  back:\n"
				+ "    item:\n"
				+ "      slot: 53\n"
				+ "      type: ARROW\n"
				+ "      name: '&6Go back'\n"
				+ "    click_actions:\n" + 
				"        DEFAULT: OPEN_GUI main_gui\n"
				+ "  \n"
				+ "  # sell every item the player has quickly in the first admin trade that gives money\n"
				+ "  sell_all:\n"
				+ "    item:\n"
				+ "      slot: 52\n"
				+ "      type: EMERALD\n"
				+ "      name: '&6Quick sell for money'\n"
				+ "      lore:\n"
				+ "        - '&7Sell every item you have &7 in the shops'\n"
				+ "        - '&7 listed here, if it can be sold for money'\n"
				+ "    click_actions:\n" + 
				"        DEFAULT: QUICK_SELL_ADMIN_FOR_VAULT_MONEY\n"
				+ "\n  # Admin shops trades\n");
		// write trades
		for (Trade trade : trades) {
			// mixed
			if (mixed) {
				// trade
				trade.generateName(nameFormat, nameFormatItem, nameFormatMoney, nameFormatMoneyCustom);
				writeTrade(shopWriter, "  ", trade);
				shopWriter.write("\n");
				// reverse trade
				Trade reverseTrade = trade.clone();
				reverseTrade.id = Utils.reverseString(trade.id);
				for (TradeObject object : reverseTrade.objects) {
					object.side = object.side.equals(ObjectSide.GIVING) ? ObjectSide.TAKING : ObjectSide.GIVING;
				}
				reverseTrade.generateName(nameFormat, nameFormatItem, nameFormatMoney, nameFormatMoneyCustom);
				writeTrade(shopWriter, "  ", reverseTrade);
				shopWriter.write("\n");
				// GUI item
				guiWriter.write("  " + trade.id + ":\n");
				Replacer replacer = new Replacer("{name}", trade.name, "{name_reverse}", reverseTrade.name);
				ItemData icon = trade.icon.clone();
				icon.setSlot(trade.slot);
				icon.setName(replacer.apply(guiItemMixedName));
				icon.setLore(replacer.apply(guiItemMixedLore));
				writeItem(guiWriter, "    ", "item", icon);
				guiWriter.write("    click_actions:\n      LEFT: OPEN_ADMIN_SHOP_TRADE " + id + ":" + trade.id + "\n      RIGHT: OPEN_ADMIN_SHOP_TRADE " + id + ":" + reverseTrade.id + "\n\n");
			}
			// single
			else {
				// trade
				trade.generateName(nameFormat, nameFormatItem, nameFormatMoney, nameFormatMoneyCustom);
				writeTrade(shopWriter, "  ", trade);
				shopWriter.write("\n");
				// GUI item
				guiWriter.write("  " + trade.id + ":\n");
				Replacer replacer = new Replacer("{name}", trade.name);
				ItemData icon = trade.icon.clone();
				icon.setSlot(trade.slot);
				icon.setName(replacer.apply(guiItemSingleName));
				icon.setLore(replacer.apply(guiItemSingleLore));
				writeItem(guiWriter, "    ", "item", icon);
				guiWriter.write("    click_actions:\n      DEFAULT: OPEN_ADMIN_SHOP_TRADE " + id + ":" + trade.id + "\n\n");
			}
		}
		// close writers
		shopWriter.close();
		guiWriter.close();
		// done
		sender.sendMessage(Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "Generated files."));
	}

	private static class Trade implements Cloneable {

		protected String id = generateId();
		protected String name;
		protected ItemData icon = null;
		protected int slot = -1;
		protected List<TradeObject> objects = new ArrayList<TradeObject>();

		@Override
		protected Trade clone() {
			Trade clone = new Trade();
			clone.icon = icon;
			clone.slot = slot;
			for (TradeObject obj : objects) {
				clone.objects.add(obj.clone());
			}
			return clone;
		}

		protected void generateName(String nameFormat, String nameFormatItem, String nameFormatMoney, String nameFormatMoneyCustom) {
			// get name and id
			String describeTake = "", describeGive = "";
			for (TradeObject object : objects) {
				if (object.side.equals(ObjectSide.TAKING)) {
					if (object instanceof TradeObjectItem) {
						if (((TradeObjectItem) object).item == null) continue;
						ItemData item = ((TradeObjectItem) object).item;
						describeTake += (describeTake.isEmpty() ? "" : ", ") + nameFormatItem.replace("{amount}", "" + item.getAmount()).replace("{type}", Utils.capitalizeUnderscores(item.getType().toString().toLowerCase()));
					} else if (object instanceof TradeObjectMoney) {
						double amount = ((TradeObjectMoney) object).amount;
						describeTake += (describeTake.isEmpty() ? "" : ", ") + (amount == 0d ? nameFormatMoney : nameFormatMoneyCustom.replace("{amount}", Utils.round(amount)));
					}
				} else if (object.side.equals(ObjectSide.GIVING)) {
					if (object instanceof TradeObjectItem) {
						if (((TradeObjectItem) object).item == null) continue;
						ItemData item = ((TradeObjectItem) object).item;
						describeGive += (describeGive.isEmpty() ? "" : ", ") + nameFormatItem.replace("{amount}", "" + item.getAmount()).replace("{type}", Utils.capitalizeUnderscores(item.getType().toString().toLowerCase()));
					} else if (object instanceof TradeObjectMoney) {
						double amount = ((TradeObjectMoney) object).amount;
						describeGive += (describeGive.isEmpty() ? "" : ", ") + (amount == 0d ? nameFormatMoney : nameFormatMoneyCustom.replace("{amount}", Utils.round(amount)));
					}
				}
			}
			name = nameFormat.replace("{take}", describeTake.isEmpty() ? "/" : describeTake).replace("{give}", describeGive.isEmpty() ? "/" : describeGive);
		}
		
		private static String generateId() {
			StringBuilder result = new StringBuilder();
			for (char ch : UUID.randomUUID().toString().replace("-", "").toLowerCase().toCharArray()) {
				if (ch >= 'a' && ch <= 'z') {
					result.append(ch);
					if (result.length() >= 5) break;
				}
			}
			return result.toString();
		}

	}

	private static abstract class TradeObject implements Cloneable {

		protected ObjectSide side;

		@Override
		protected abstract TradeObject clone();

	}

	private static class TradeObjectItem extends TradeObject {

		protected ItemData item;

		@Override
		protected TradeObjectItem clone() {
			TradeObjectItem clone = new TradeObjectItem();
			clone.side = side;
			clone.item = item.clone();
			return clone;
		}

	}

	private static class TradeObjectMoney extends TradeObject {

		protected double amount;

		@Override
		protected TradeObjectMoney clone() {
			TradeObjectMoney clone = new TradeObjectMoney();
			clone.side = side;
			clone.amount = amount;
			return clone;
		}

	}

	private static class TradeObjectXpLevel extends TradeObject {

		protected int amount;

		@Override
		protected TradeObjectXpLevel clone() {
			TradeObjectXpLevel clone = new TradeObjectXpLevel();
			clone.side = side;
			clone.amount = amount;
			return clone;
		}

	}

	private static class TradeObjectPlayerPointsPoints extends TradeObject {

		protected int amount;

		@Override
		protected TradeObjectPlayerPointsPoints clone() {
			TradeObjectPlayerPointsPoints clone = new TradeObjectPlayerPointsPoints();
			clone.side = side;
			clone.amount = amount;
			return clone;
		}

	}

	private static class TradeObjectTokenEnchantTokens extends TradeObject {

		protected int amount;

		@Override
		protected TradeObjectTokenEnchantTokens clone() {
			TradeObjectTokenEnchantTokens clone = new TradeObjectTokenEnchantTokens();
			clone.side = side;
			clone.amount = amount;
			return clone;
		}

	}

	private static class TradeObjectCommands extends TradeObject {

		protected List<String> commands = new ArrayList<String>();

		@Override
		protected TradeObjectCommands clone() {
			TradeObjectCommands clone = new TradeObjectCommands();
			clone.side = side;
			clone.commands = Utils.asList(commands);
			return clone;
		}

	}

}
