package com.guillaumevdn.supremeshops.command.shop;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.util.input.LocationInput;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.gui.shop.ShopSelectionGUI;
import com.guillaumevdn.supremeshops.gui.shop.management.ShopManagementGUI;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ArgShopAdminEdit extends CommandArgument {

	// base
	public ArgShopAdminEdit() {
		super(SupremeShops.inst(), Utils.asList("adminedit", "aedit"), "edit a shop in admin mode", SSPerm.SUPREMESHOPS_COMMAND_SHOP_ADMINEDIT, true);
	}

	// call
	@Override
	public void perform(final CommandCall call) {
		final Player player = call.getSenderAsPlayer();
		SSLocale.MSG_SUPREMESHOPS_PLAYERTRADEADMINEDITSHOPINPUT.send(player);
		// open selection GUI, but they're free to close it
		new ShopSelectionGUI(null, false, player, true, null, -1) {
			@Override
			protected void onSelect(Shop shop) {
				// remove location input
				GCoreLegacy.inst().getLocationInputs().remove(player);
				// edit shop
				new ShopManagementGUI(shop, Utils.asSet(ShopManagementPermission.values()), player, null, -1).open(player);
			}
		}.open(player);
		// add location input
		GCoreLegacy.inst().getLocationInputs().put(player, new LocationInput() {
			@Override
			public void onChoose(Player player, Location value) {
				// invalid shop
				Shop shop = null;
				if (value != null) {
					BlockCoords coords = new BlockCoords(value);
					shop = SupremeShops.inst().getData().getShops().getPhysicalElementLinkedToBlock(coords);
				}
				if (shop == null) {
					SSLocale.MSG_SUPREMESHOPS_PLAYERTRADECREATESHOPINVALID.send(player);
					return;
				}
				// edit shop
				new ShopManagementGUI(shop, Utils.asSet(ShopManagementPermission.values()), player, null, -1).open(player);
			}
		});
	}

}
