package com.guillaumevdn.supremeshops.command.shop;

import org.bukkit.OfflinePlayer;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.command.Param;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.ShopBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.gui.shop.AdminShopListGUI;

public class ArgShopList extends CommandArgument {

	// base
	private static Param paramOwner = new Param(Utils.asList("owner", "o"), "owner name or ADMIN", null, false, false);

	public ArgShopList() {
		super(SupremeShops.inst(), Utils.asList("list"), "list shops", SSPerm.SUPREMESHOPS_COMMAND_SHOP_LIST, true, paramOwner);
	}

	// call
	@Override
	public void perform(CommandCall call) {
		// has owner
		Player sender = call.getSenderAsPlayer();
		if (paramOwner.has(call)) {
			// admin
			String str = paramOwner.getString(call);
			if (str != null && str.trim().equalsIgnoreCase("admin")) {
				new AdminShopListGUI(SupremeShops.inst().getData().getShops().getElements(null, null, false, ElementRemotePolicy.MIGHT_BE), null, sender).open(sender);
			}
			// owner
			else {
				OfflinePlayer owner = paramOwner.getOfflinePlayer(call, false);
				if (owner != null) {
					new AdminShopListGUI(SupremeShops.inst().getData().getShops().getElements(new UserInfo(owner), null, false, ElementRemotePolicy.MIGHT_BE), owner, sender).open(sender);
				}
			}
		}
		// no owner
		else {
			new AdminShopListGUI(SupremeShops.inst().getData().getShops().getAll().values(), null, sender).open(sender);
		}
	}

}
