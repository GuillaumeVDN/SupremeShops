package com.guillaumevdn.supremeshops.command.convert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.UUID;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.material.Chest;
import org.bukkit.material.Sign;
import org.maxgamer.quickshop.QuickShop;
import org.maxgamer.quickshop.Shop.ContainerShop;
import org.maxgamer.quickshop.Shop.ShopType;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.data.DataManager.Callback;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.messenger.Messenger;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Pair;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.display.sign.SignDisplayable;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.type.ObjectItem;
import com.guillaumevdn.supremeshops.module.object.type.ObjectVaultMoney;
import com.guillaumevdn.supremeshops.module.shop.BlockShop;
import com.guillaumevdn.supremeshops.module.shop.Shop;

/**
 * @author GuillaumeVDN
 */
public class QuickShopUtils {

	private static boolean converting = false;

	public static void convert(CommandSender sender) {
		// already converting
		if (converting) {
			Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "[QuickShop-Import] A data conversion is already running.");
			return;
		}
		converting = true;
		// import
		Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "[QuickShop-Import] Starting data import and sign add...");
		int invalid = 0, cantCreateSign = 0, cantImport = 0;
		List<org.maxgamer.quickshop.Shop.Shop> imported = new ArrayList<org.maxgamer.quickshop.Shop.Shop>();
		final List<Shop> importedShops = new ArrayList<Shop>();
		// check chests
		Iterator<org.maxgamer.quickshop.Shop.Shop> iterator = QuickShop.instance.getShopManager().getShopIterator();
		while (iterator.hasNext()) {
			org.maxgamer.quickshop.Shop.Shop toImport = iterator.next();
			// invalid
			if (toImport.getItem() == null || toImport.getLocation() == null || toImport.getLocation().getWorld() == null) {
				continue;
			}
			UserInfo user = toImport.isUnlimited() ? null : new UserInfo(toImport.getOwner());
			// can buy
			if (toImport.getShopType().equals(ShopType.BUYING)) {
				// create sign
				Pair<BlockCoords, BlockCoords> coords = ensureSignExistence(toImport);
				if (coords == null) {
					++cantCreateSign;
				}
				// create shop
				BlockShop shop = new BlockShop("block_" + coords.getA().getX() + "_" + coords.getA().getY() + "_" + coords.getA().getZ(), user, coords.getA(), coords.getB());
				copyBuyTrade(toImport, shop, user == null ? 0 : toImport.getRemainingStock());
				importedShops.add(shop);
				imported.add(toImport);
			}
			// can sell
			else if (toImport.getShopType().equals(ShopType.SELLING)) {
				// create sign
				Pair<BlockCoords, BlockCoords> coords = ensureSignExistence(toImport);
				if (coords == null) {
					++cantCreateSign;
				}
				// create shop
				BlockShop shop = new BlockShop("block_" + coords.getA().getX() + "_" + coords.getA().getY() + "_" + coords.getA().getZ(), user, coords.getA(), coords.getB());
				copySellTrade(toImport, shop, user == null ? 0 : toImport.getRemainingStock());
				importedShops.add(shop);
				imported.add(toImport);
			}
			// can't import
			else {
				++cantImport;
			}
		}
		Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[QuickShop-Import] ... imported " + importedShops.size() + " shop" + Utils.getPlural(importedShops.size()));
		if (invalid > 0) Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "[QuickShop-Import] ... couldn't import " + invalid + " QuickShop shop" + Utils.getPlural(invalid) + " ; invalid QuickShop data");
		if (cantCreateSign > 0) Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "[QuickShop-Import] ... couldn't import " + cantCreateSign + " QuickShop shop" + Utils.getPlural(cantCreateSign) + " ; can't create sign (no space ?)");
		if (cantImport > 0) Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "[QuickShop-Import] ... couldn't import " + cantImport + " QuickShop shop" + Utils.getPlural(cantImport) + " ; no sell/buy price");
		// push shops
		Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "[QuickShop-Import] Saving SupremeShops data...");
		if (!importedShops.isEmpty()) {
			SupremeShops.inst().getData().getShops().addAll(importedShops, new Callback() {
				@Override
				public void callback() {
					Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[QuickShop-Import] ... saved SupremeShops data for " + importedShops.size() + " shop" + Utils.getPlural(importedShops.size()) + ".");
				}
			});
		}
		// clear items
		Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "[QuickShop-Import] Clearing existing shops inventories (stock is managed directly in data files with SupremeShops so leaving the items in the chest would create duplicates)...");
		for (org.maxgamer.quickshop.Shop.Shop shop : imported) {
			try {
				if (shop instanceof ContainerShop) {
					((ContainerShop) shop).getInventory().clear();
				}
			} catch (Throwable ignored) {}
		}
		Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[QuickShop-Import] ... cleared shops inventories for imported shops.");
		// updating signs
		Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "[QuickShop-Import] Updating new signs (this might be performance consuming, so just wait)...");
		for (Shop shop : importedShops) {
			if (shop instanceof SignDisplayable) {
				SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign((SignDisplayable) shop);
			}
		}
		Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[QuickShop-Import] ... updated signs for newly created shops.");
		Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[QuickShop-Import] You're all done ! If you haven't seen the data saving success message for shops yet, please wait for it to appear, since data might still be saving. Then the next step is to restart your server withouht QuickShop, and everything will be done ! If you have encountered any problem, please contact me on Discord.");
		converting = true;
	}

	private static Pair<BlockCoords, BlockCoords> ensureSignExistence(org.maxgamer.quickshop.Shop.Shop toImport) {
		try {
			Block block = toImport.getLocation().getBlock();
			Block sign = null;
			// has free space around for a sign
			Chest chestData = (Chest) block.getState().getData();
			Block potentialSign = block.getRelative(chestData.getFacing());
			// directly in the front face
			if (Mat.fromBlock(potentialSign).isAir()) {
				sign = potentialSign;
			}
			// or try other faces
			else {
				for (BlockFace face : Utils.asList(BlockFace.NORTH, BlockFace.SOUTH, BlockFace.EAST, BlockFace.WEST)) {
					if (Mat.fromBlock(potentialSign = block.getRelative(face)).isAir()) {
						sign = potentialSign;
						break;
					}
				}
			}
			// no sign
			if (sign == null) {
				return null;
			}
			// update chest facing
			BlockFace face = block.getFace(sign);
			chestData.setFacingDirection(face);
			block.getState().setData(chestData);
			block.getState().update(true);
			// set sign
			Mat.WALL_SIGN.setBlock(sign);
			Sign signData = (Sign) sign.getState().getData();
			signData.setFacingDirection(face);
			sign.getState().setData(signData);
			sign.getState().update(true);
			// done
			return new Pair<BlockCoords, BlockCoords>(new BlockCoords(block), new BlockCoords(sign));
		} catch (Throwable ignored) {
			return null;
		}
	}

	private static void copyBuyTrade(org.maxgamer.quickshop.Shop.Shop toImport, Shop shop, int itemsStock) {
		ObjectVaultMoney money = new ObjectVaultMoney(UUID.randomUUID().toString().replace("-", "").substring(0, 5), ObjectSide.TAKING, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
		money.getCustomAmount().setValue(Utils.asList("" + BigDecimal.valueOf(toImport.getPrice()).toPlainString()));
		shop.addObject(money, 0d, false);
		ObjectItem item = new ObjectItem(UUID.randomUUID().toString().replace("-", "").substring(0, 5), ObjectSide.GIVING, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
		item.getItem().replace(new ItemData(toImport.getItem()), null);
		shop.addObject(item, (double) itemsStock, false);
	}

	private static void copySellTrade(org.maxgamer.quickshop.Shop.Shop toImport, Shop shop, int itemsStock) {
		ObjectItem item = new ObjectItem(UUID.randomUUID().toString().replace("-", "").substring(0, 5), ObjectSide.TAKING, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
		item.getItem().replace(new ItemData(toImport.getItem()), null);
		shop.addObject(item, (double) itemsStock, false);
		ObjectVaultMoney money = new ObjectVaultMoney(UUID.randomUUID().toString().replace("-", "").substring(0, 5), ObjectSide.GIVING, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
		money.getCustomAmount().setValue(Utils.asList("" + BigDecimal.valueOf(toImport.getPrice()).toPlainString()));
		shop.addObject(money, 0d, false);
	}

}
