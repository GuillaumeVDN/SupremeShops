package com.guillaumevdn.supremeshops.command.convert;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.command.CommandSender;
import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.messenger.Messenger;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;

// ShopChest : https://www.spigotmc.org/resources/shopchest.11431/
public class ArgConvertShopchest extends CommandArgument {

	public ArgConvertShopchest() {
		super(SupremeShops.inst(), Utils.asList("shopchest"), "convert ShopChest chests", SSPerm.SUPREMESHOPS_ADMIN, false);
	}

	private Set<CommandSender> confirm = new HashSet<CommandSender>();

	@Override
	public void perform(CommandCall call) {
		// not installed
		if (!Utils.isPluginEnabled("ShopChest")) {
			Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "ShopChest don't seem to be enabled.");
			return;
		}
		// confirm
		final CommandSender sender = call.getSender();
		if (!confirm.add(sender)) {
			Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "--------------------------------------------------");
			Messenger.Level.SEVERE_INFO.format(SupremeShops.inst().getName(), "You're about to convert all shops from ShopChest to SupremeShops. §lIt's recommended to do a server backup before using this command ; it most likely won't break anything, but you never know what could happen. I won't be to blame if a problem occured but you didn't do a backup first. This message might look scary but the point is really to get you to do a backup.");
			Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "--------------------------------------------------");
			Messenger.Level.SEVERE_INFO.format(SupremeShops.inst().getName(), "Please make sure everything is configured correctly in SupremeShops ; by this I mean any setting like 'max shops/merchants per player', allowed bloc/merchant shops, etc. Merchants will be created if the shop can both buy and sell items, and otherwise regular Shops will be created. Please read the plugin wiki first to understand how SupremeShops works.");
			Messenger.Level.SEVERE_INFO.format(SupremeShops.inst().getName(), "IN PARTICULAR, look into item value settings in this file (it's enabled by default for the admin shops) in this file : </plugins/SupremeShops/items/values.yml>");
			Messenger.Level.SEVERE_INFO.format(SupremeShops.inst().getName(), "IN PARTICULAR, if you wish to import shops directly to a MySQL database, please configure the database (and ensure it initializes correctly) in this file : </plugins/SupremeShops/config.yml>");
			Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "--------------------------------------------------");
			Messenger.Level.SEVERE_INFO.format(SupremeShops.inst().getName(), "This command will : <retrieve the existing shops of ShopChest through its API, create corresponding shops (or merchants if the shop can buy AND sell), then if the operation is successful, content of the shops will be cleared.>");
			Messenger.Level.SEVERE_INFO.format(SupremeShops.inst().getName(), "To confirm, please execute this command again (expires in 30s).");
			Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "--------------------------------------------------");
			new BukkitRunnable() {
				@Override
				public void run() {
					confirm.remove(sender);
				}
			}.runTaskLater(SupremeShops.inst(), 20L * 30L);
			return;
		}
		// convert
		ShopChestUtils.convert(sender);
	}

}
