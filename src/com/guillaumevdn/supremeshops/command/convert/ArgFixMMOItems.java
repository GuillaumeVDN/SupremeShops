package com.guillaumevdn.supremeshops.command.convert;

import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.command.CommandCall;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;

// QuantumShop : https://www.spigotmc.org/resources/quantumrpg-1-13-1-14.40007/
public class ArgFixMMOItems extends CommandArgument {

	public ArgFixMMOItems() {
		super(SupremeShops.inst(), Utils.asList("fixmmoitems"), "fix shops with MMOItems items", SSPerm.SUPREMESHOPS_ADMIN, false);
	}

	//private Set<CommandSender> confirm = new HashSet<CommandSender>();

	@Override
	public void perform(CommandCall call) {
		/*// not installed
		if (!Utils.isPluginEnabled("MMOItems")) {
			Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "MMOItems don't seem to be enabled.");
			return;
		}
		// confirm
		final CommandSender sender = call.getSender();
		if (!confirm.add(sender)) {
			Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "--------------------------------------------------");
			Messenger.Level.SEVERE_INFO.format(SupremeShops.inst().getName(), "You're about to fix all MMOItems saved in shops. §lIt's recommended to do a data backup before using this command ; it most likely won't break anything, but you never know what could happen. I won't be to blame if a problem occured but you didn't do a backup first. This message might look scary but the point is really to get you to do a backup.");
			Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "--------------------------------------------------");
			Messenger.Level.SEVERE_INFO.format(SupremeShops.inst().getName(), "This command will : <replace all MMOItems items saved in shops by a fresh copy of it (by ID). Any other modified data that is not in the MMOItems item config will not be saved.>");
			Messenger.Level.SEVERE_INFO.format(SupremeShops.inst().getName(), "To confirm, please execute this command again (expires in 30s).");
			Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "--------------------------------------------------");
			new BukkitRunnable() {
				@Override
				public void run() {
					confirm.remove(sender);
				}
			}.runTaskLater(SupremeShops.inst(), 20L * 30L);
			return;
		}
		// convert
		MMOItemsUtils.convert(sender);*/
	}

}
