package com.guillaumevdn.supremeshops.command.convert;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.command.CommandSender;
import org.bukkit.inventory.InventoryHolder;
import org.bukkit.inventory.ItemStack;
import org.bukkit.material.Chest;
import org.bukkit.material.Sign;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.data.DataManager.Callback;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.messenger.Messenger;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Pair;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.display.sign.SignDisplayable;
import com.guillaumevdn.supremeshops.module.merchant.BlockMerchant;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.type.ObjectItem;
import com.guillaumevdn.supremeshops.module.object.type.ObjectVaultMoney;
import com.guillaumevdn.supremeshops.module.shop.BlockShop;
import com.guillaumevdn.supremeshops.module.shop.MerchantShop;
import com.guillaumevdn.supremeshops.module.shop.Shop;

import de.epiceric.shopchest.ShopChest;
import de.epiceric.shopchest.shop.Shop.ShopType;

/**
 * @author GuillaumeVDN
 */
public class ShopChestUtils {

	private static boolean converting = false;

	public static void convert(CommandSender sender) {
		// already converting
		if (converting) {
			Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "[ShopChest-Import] A data conversion is already running.");
			return;
		}
		converting = true;
		// import
		Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "[ShopChest-Import] Starting data import and sign add...");
		int invalid = 0, cantCreateSign = 0, cantImport = 0;
		List<de.epiceric.shopchest.shop.Shop> imported = new ArrayList<de.epiceric.shopchest.shop.Shop>();
		final List<Shop> importedShops = new ArrayList<Shop>();
		final List<BlockMerchant> importedMerchants = new ArrayList<BlockMerchant>();
		// check chests
		for (de.epiceric.shopchest.shop.Shop toImport : ShopChest.getInstance().getShopUtils().getShops()) {
			// invalid
			if (toImport.getProduct() == null || toImport.getLocation() == null || toImport.getLocation().getWorld() == null) {
				continue;
			}
			// can buy and sell : merchant
			UserInfo user = toImport.getShopType().equals(ShopType.ADMIN) ? null : new UserInfo(toImport.getVendor());
			if (toImport.getBuyPrice() > 0d && toImport.getSellPrice() > 0d) {
				// create sign
				Pair<BlockCoords, BlockCoords> coords = ensureSignExistence(toImport);
				if (coords == null) {
					++cantCreateSign;
				}
				// create merchant
				BlockMerchant merchant = new BlockMerchant("block_" + coords.getA().getX() + "_" + coords.getA().getY() + "_" + coords.getA().getZ(), user, coords.getA(), coords.getB());
				// create "buy" shop
				MerchantShop buyShop = new MerchantShop("merchant_" + merchant.getId() + "_" + UUID.randomUUID().toString().replace("-", "").substring(0, 5), user, merchant.getId());
				copyBuyTrade(toImport, buyShop, user == null ? 0 : countItemsInInventory(toImport.getInventoryHolder(), toImport.getProduct()));
				merchant.addShop(buyShop);
				// create "sell" shop
				MerchantShop sellShop = new MerchantShop("merchant_" + merchant.getId() + "_" + UUID.randomUUID().toString().replace("-", "").substring(0, 5), user, merchant.getId());
				copySellTrade(toImport, sellShop, 0);
				merchant.addShop(sellShop);
				// add all
				importedShops.add(buyShop);
				importedShops.add(sellShop);
				importedMerchants.add(merchant);
				imported.add(toImport);
			}
			// can buy
			else if (toImport.getBuyPrice() > 0d) {
				// create sign
				Pair<BlockCoords, BlockCoords> coords = ensureSignExistence(toImport);
				if (coords == null) {
					++cantCreateSign;
				}
				// create shop
				BlockShop shop = new BlockShop("block_" + coords.getA().getX() + "_" + coords.getA().getY() + "_" + coords.getA().getZ(), user, coords.getA(), coords.getB());
				copyBuyTrade(toImport, shop, user == null ? 0 : countItemsInInventory(toImport.getInventoryHolder(), toImport.getProduct()));
				importedShops.add(shop);
				imported.add(toImport);
			}
			// can sell
			else if (toImport.getSellPrice() > 0d) {
				// create sign
				Pair<BlockCoords, BlockCoords> coords = ensureSignExistence(toImport);
				if (coords == null) {
					++cantCreateSign;
				}
				// create shop
				BlockShop shop = new BlockShop("block_" + coords.getA().getX() + "_" + coords.getA().getY() + "_" + coords.getA().getZ(), user, coords.getA(), coords.getB());
				copySellTrade(toImport, shop, user == null ? 0 : countItemsInInventory(toImport.getInventoryHolder(), toImport.getProduct()));
				importedShops.add(shop);
				imported.add(toImport);
			}
			// can't import
			else {
				++cantImport;
			}
		}
		Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[ShopChest-Import] ... imported " + importedMerchants.size() + " merchant" + Utils.getPlural(importedMerchants.size()) + " (and " + importedShops.size() + " shop" + Utils.getPlural(importedShops.size()) + " in total)");
		if (invalid > 0) Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "[ShopChest-Import] ... couldn't import " + invalid + " ShopChest shop" + Utils.getPlural(invalid) + " ; invalid ShopChest data");
		if (cantCreateSign > 0) Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "[ShopChest-Import] ... couldn't import " + cantCreateSign + " ShopChest shop" + Utils.getPlural(cantCreateSign) + " ; can't create sign (no space ?)");
		if (cantImport > 0) Messenger.Level.SEVERE_ERROR.format(SupremeShops.inst().getName(), "[ShopChest-Import] ... couldn't import " + cantImport + " ShopChest shop" + Utils.getPlural(cantImport) + " ; no sell/buy price");
		// push shops
		Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "[ShopChest-Import] Saving SupremeShops data...");
		if (!importedShops.isEmpty()) {
			SupremeShops.inst().getData().getShops().addAll(importedShops, new Callback() {
				@Override
				public void callback() {
					Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[ShopChest-Import] ... saved SupremeShops data for " + importedShops.size() + " shop" + Utils.getPlural(importedShops.size()) + ".");
				}
			});
		}
		if (!importedMerchants.isEmpty()) {
			SupremeShops.inst().getData().getMerchants().addAll(importedMerchants, new Callback() {
				@Override
				public void callback() {
					Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[ShopChest-Import] ... saved SupremeShops data for " + importedMerchants.size() + " merchant" + Utils.getPlural(importedMerchants.size()) + ".");
				}
			});
		}
		// clear items
		Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "[ShopChest-Import] Clearing existing shops inventories (stock is managed directly in data files with SupremeShops so leaving the items in the chest would create duplicates)...");
		for (de.epiceric.shopchest.shop.Shop shop : imported) {
			try {
				shop.getInventoryHolder().getInventory().clear();
			} catch (Throwable ignored) {}
		}
		Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[ShopChest-Import] ... cleared shops inventories for imported shops.");
		// updating signs
		Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "[ShopChest-Import] Updating new signs (this might be performance consuming, so just wait)...");
		for (Shop shop : importedShops) {
			if (shop instanceof SignDisplayable) {
				SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign((SignDisplayable) shop);
			}
		}
		for (BlockMerchant merchant : importedMerchants) {
			SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(merchant);
		}
		Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[ShopChest-Import] ... updated signs for newly created shops and merchants.");
		Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[ShopChest-Import] You're all done ! If you haven't seen the data saving success message for both merchants and shops yet, please wait for it to appear, since data might still be saving. Then the next step is to restart your server withouht ShopChest, and everything will be done ! If you have encountered any problem, please contact me on Discord.");
		converting = true;
	}

	private static Pair<BlockCoords, BlockCoords> ensureSignExistence(de.epiceric.shopchest.shop.Shop toImport) {
		try {
			Block block = toImport.getLocation().getBlock();
			Block sign = null;
			// has free space around for a sign
			Chest chestData = (Chest) block.getState().getData();
			Block potentialSign = block.getRelative(chestData.getFacing());
			// directly in the front face
			if (Mat.fromBlock(potentialSign).isAir()) {
				sign = potentialSign;
			}
			// or try other faces
			else {
				for (BlockFace face : Utils.asList(BlockFace.NORTH, BlockFace.SOUTH, BlockFace.EAST, BlockFace.WEST)) {
					if (Mat.fromBlock(potentialSign = block.getRelative(face)).isAir()) {
						sign = potentialSign;
						break;
					}
				}
			}
			// no sign
			if (sign == null) {
				return null;
			}
			// update chest facing
			BlockFace face = block.getFace(sign);
			chestData.setFacingDirection(face);
			block.getState().setData(chestData);
			block.getState().update(true);
			// set sign
			Mat.WALL_SIGN.setBlock(sign);
			Sign signData = (Sign) sign.getState().getData();
			signData.setFacingDirection(face);
			sign.getState().setData(signData);
			sign.getState().update(true);
			// done
			return new Pair<BlockCoords, BlockCoords>(new BlockCoords(block), new BlockCoords(sign));
		} catch (Throwable ignored) {
			return null;
		}
	}

	private static void copyBuyTrade(de.epiceric.shopchest.shop.Shop toImport, Shop shop, int itemsStock) {
		ObjectVaultMoney money = new ObjectVaultMoney(UUID.randomUUID().toString().replace("-", "").substring(0, 5), ObjectSide.TAKING, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
		money.getCustomAmount().setValue(Utils.asList("" + BigDecimal.valueOf(toImport.getBuyPrice()).toPlainString()));
		shop.addObject(money, 0d, false);
		ObjectItem item = new ObjectItem(UUID.randomUUID().toString().replace("-", "").substring(0, 5), ObjectSide.GIVING, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
		item.getItem().replace(new ItemData(toImport.getProduct()), null);
		shop.addObject(item, (double) itemsStock, false);
	}

	private static void copySellTrade(de.epiceric.shopchest.shop.Shop toImport, Shop shop, int itemsStock) {
		ObjectItem item = new ObjectItem(UUID.randomUUID().toString().replace("-", "").substring(0, 5), ObjectSide.TAKING, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
		item.getItem().replace(new ItemData(toImport.getProduct()), null);
		shop.addObject(item, (double) itemsStock, false);
		ObjectVaultMoney money = new ObjectVaultMoney(UUID.randomUUID().toString().replace("-", "").substring(0, 5), ObjectSide.GIVING, false, null, false, -1, EditorGUI.ICON_OBJECT, null);
		money.getCustomAmount().setValue(Utils.asList("" + BigDecimal.valueOf(toImport.getSellPrice()).toPlainString()));
		shop.addObject(money, 0d, false);
	}

	private static int countItemsInInventory(InventoryHolder holder, ItemStack item) {
		return holder == null || holder.getInventory() == null ? 0 : new ItemData(item).count(holder.getInventory());
	}

}
