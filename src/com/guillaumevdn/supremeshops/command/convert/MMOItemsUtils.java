package com.guillaumevdn.supremeshops.command.convert;

/**
 * @author GuillaumeVDN
 */
public class MMOItemsUtils {

	/*private static boolean fixing = false;

	public static void convert(CommandSender sender) {
		// already converting
		if (fixing) {
			Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "[MMOItems-Fix] A data fix is already running.");
			return;
		}
		fixing = true;
		// import
		Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "[MMOItems-Fix] Starting items fixing...");
		Set<Shop> toPush = new HashSet<>();
		int fixed = 0;
		for (Shop shop : SupremeShops.inst().getData().getShops().getAll().values()) {
			for (TradeObject object : Utils.asList(shop.getObjects())) {
				if (object instanceof ObjectItem) {
					ItemStack item = ((ObjectItem) object).getItem().getParsedValue(null).getItemStack();
					NBTItem nbtItem = item == null ? null : NBTItem.get(item);
					if (nbtItem != null && nbtItem.getType() != null) {
						String mmoItemId = nbtItem.getString("MMOITEMS_ITEM_ID");
						if (mmoItemId != null) {
							shop.removeObject(object, false);
							ObjectItem obj = new ObjectItem(object.getId(), object.getSide(), object.getCanEditSide(), object.getParent(), object.isMandatory(), object.getEditorSlot(), object.getEditorIcon(), object.getEditorDescription());
							obj.getItem().replace(new ItemData(MMOItems.plugin.getItems().getItem(nbtItem.getType(), mmoItemId)), null);
							shop.addObject(object, shop.getObjectStock(object), false);
							toPush.add(shop);
							++fixed;
						}
					}
				}
			}
		}
		Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[MMOItems-Fix] ... fixed " + fixed + " item" + Utils.getPlural(fixed) + " in " + toPush.size() + " shop" + Utils.getPlural(toPush.size()) + ".");
		// push shops
		Messenger.Level.NORMAL_INFO.format(SupremeShops.inst().getName(), "[MMOItems-Fix] Saving SupremeShops data...");
		if (!toPush.isEmpty()) {
			SupremeShops.inst().getData().getShops().addAll(toPush, new Callback() {
				@Override
				public void callback() {
					Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[MMOItems-Fix] ... saved SupremeShops data for " + toPush.size() + " shop" + Utils.getPlural(toPush.size()) + ".");
				}
			});
		}
		Messenger.Level.NORMAL_SUCCESS.format(SupremeShops.inst().getName(), "[MMOItems-Fix] You're all done ! If you haven't seen the data saving success message yet, please wait for it to appear, since data might still be saving. If you have encountered any problem, please contact me on Discord.");
		fixing = true;
	}*/

}
