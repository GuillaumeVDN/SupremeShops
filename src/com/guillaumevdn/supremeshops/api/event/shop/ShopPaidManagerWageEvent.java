package com.guillaumevdn.supremeshops.api.event.shop;

import org.bukkit.event.HandlerList;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ShopPaidManagerWageEvent extends ShopEvent {

	// base
	private UserInfo manager;
	private int paidLateWages;

	public ShopPaidManagerWageEvent(Shop shop, UserInfo manager, int paidLateWages) {
		super(shop);
		this.manager = manager;
		this.paidLateWages = paidLateWages;
	}

	// get
	public UserInfo getManager() {
		return manager;
	}

	public int getPaidLateWages() {
		return paidLateWages;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
