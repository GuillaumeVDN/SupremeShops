package com.guillaumevdn.supremeshops.api.event.shop;

import java.util.List;

import org.bukkit.event.HandlerList;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ShopUpdatedManagerEvent extends ShopEvent {

	// base
	private UserInfo manager;
	private Operation operation;
	private TradeObject wageObject;
	private List<ShopManagementPermission> affectedPermissions;

	public ShopUpdatedManagerEvent(Shop shop, UserInfo manager, TradeObject wageObject, List<ShopManagementPermission> affectedPermissions, Operation operation) {
		super(shop);
		this.manager = manager;
		this.operation = operation;
		this.wageObject = wageObject;
		this.affectedPermissions = affectedPermissions;
	}

	// get
	public UserInfo getManager() {
		return manager;
	}

	public Operation getOperation() {
		return operation;
	}

	public TradeObject getWageObject() {
		return wageObject;
	}

	public List<ShopManagementPermission> getAffectedPermissions() {
		return affectedPermissions;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	// operation
	public static enum Operation {
		ADD_MANAGER,
		REMOVE_MANAGER,
		ADD_PERMISSION,
		REMOVE_PERMISSION,
		UPDATE_PERMISSIONS,
		ADD_WAGE,
		REMOVE_WAGE
	}

}
