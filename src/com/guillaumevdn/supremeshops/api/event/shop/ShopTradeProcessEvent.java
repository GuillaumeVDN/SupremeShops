package com.guillaumevdn.supremeshops.api.event.shop;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ShopTradeProcessEvent extends ShopEvent implements Cancellable {

	// base
	private Player player;
	private int trades;
	private Merchant merchant;

	public ShopTradeProcessEvent(Shop shop, Player player, int trades, Merchant merchant) {
		super(shop);
		this.player = player;
		this.trades = trades;
		this.merchant = merchant;
	}

	// get
	public Player getPlayer() {
		return player;
	}

	public int getTrades() {
		return trades;
	}

	public Merchant getMerchant() {
		return merchant;
	}

	// set
	public void setTrades(int trades) {
		this.trades = trades;
	}

	// cancellable
	private boolean cancelled;

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
