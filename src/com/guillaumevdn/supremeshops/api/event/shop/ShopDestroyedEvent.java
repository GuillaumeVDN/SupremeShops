package com.guillaumevdn.supremeshops.api.event.shop;

import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.DestroyCause;

public class ShopDestroyedEvent extends ShopEvent {

	// base
	private DestroyCause cause;

	public ShopDestroyedEvent(Shop shop, DestroyCause cause) {
		super(shop);
		this.cause = cause;
	}

	// get
	public DestroyCause getCause() {
		return cause;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
