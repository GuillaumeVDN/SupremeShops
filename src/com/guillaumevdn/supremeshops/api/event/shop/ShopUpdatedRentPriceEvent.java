package com.guillaumevdn.supremeshops.api.event.shop;

import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ShopUpdatedRentPriceEvent extends ShopEvent {

	// base
	private Operation operation;
	private TradeObject priceObject;

	public ShopUpdatedRentPriceEvent(Shop shop, TradeObject priceObject, Operation operation) {
		super(shop);
		this.operation = operation;
		this.priceObject = priceObject;
	}

	// get
	public Operation getOperation() {
		return operation;
	}

	public TradeObject getPriceObject() {
		return priceObject;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	// operation
	public static enum Operation {
		ADD_OBJECT,
		REMOVE_OBJECT
	}

}
