package com.guillaumevdn.supremeshops.api.event.shop;

import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import com.guillaumevdn.supremeshops.module.shop.Shop;

public abstract class ShopEvent extends Event {

	// base
	private Shop shop;

	public ShopEvent(Shop shop) {
		super(!Bukkit.isPrimaryThread());
		this.shop = shop;
	}

	// get
	public Shop getShop() {
		return shop;
	}

}
