package com.guillaumevdn.supremeshops.api.event.playertrade;

import org.bukkit.Bukkit;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.playertrade.PlayerTrade;

public class PlayerTradeProcessEvent extends Event {

	// base
	private PlayerTrade trade;

	public PlayerTradeProcessEvent(PlayerTrade trade) {
		super(!Bukkit.isPrimaryThread());
		this.trade = trade;
	}

	// get
	public PlayerTrade getTrade() {
		return trade;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
