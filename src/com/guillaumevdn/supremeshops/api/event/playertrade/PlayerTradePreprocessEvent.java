package com.guillaumevdn.supremeshops.api.event.playertrade;

import org.bukkit.Bukkit;
import org.bukkit.event.Cancellable;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.playertrade.PlayerTrade;

public class PlayerTradePreprocessEvent extends Event implements Cancellable {

	// base
	private PlayerTrade trade;

	public PlayerTradePreprocessEvent(PlayerTrade trade) {
		super(!Bukkit.isPrimaryThread());
		this.trade = trade;
	}

	// get
	public PlayerTrade getTrade() {
		return trade;
	}

	// cancellable
	private boolean cancelled;

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
