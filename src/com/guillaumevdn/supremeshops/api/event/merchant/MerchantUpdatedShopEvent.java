package com.guillaumevdn.supremeshops.api.event.merchant;

import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class MerchantUpdatedShopEvent extends MerchantEvent {

	// base
	private Shop shop;
	private Operation operation;

	public MerchantUpdatedShopEvent(Merchant merchant, Shop shop, Operation operation) {
		super(merchant);
		this.shop = shop;
		this.operation = operation;
	}

	// get
	public Shop getShop() {
		return shop;
	}

	public Operation getOperation() {
		return operation;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	// operation
	public static enum Operation {
		ADD_SHOP,
		REMOVE_SHOP
	}

}
