package com.guillaumevdn.supremeshops.api.event.merchant;

import org.bukkit.entity.Player;
import org.bukkit.event.Cancellable;
import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.merchant.Merchant;

public class MerchantCreateEvent extends MerchantEvent implements Cancellable {

	// base
	private Player whoCreated;

	public MerchantCreateEvent(Merchant merchant, Player whoCreated) {
		super(merchant);
		this.whoCreated = whoCreated;
	}

	// get
	public Player getWhoCreated() {
		return whoCreated;
	}

	// cancellable
	private boolean cancelled;

	@Override
	public boolean isCancelled() {
		return cancelled;
	}

	@Override
	public void setCancelled(boolean cancelled) {
		this.cancelled = cancelled;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
