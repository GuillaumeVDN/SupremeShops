package com.guillaumevdn.supremeshops.api.event.merchant;

import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.merchant.Merchant;

public class MerchantUpdatedEvent extends MerchantEvent {

	// base
	private Operation operation;

	public MerchantUpdatedEvent(Merchant merchant, Operation operation) {
		super(merchant);
		this.operation = operation;
	}

	// get
	public Operation getOperation() {
		return operation;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	// operation
	public static enum Operation {
		SET_ACTUAL_OWNER,
		SET_DISPLAY_NAME,
		SET_EQUIPMENT,
		SET_SKIN,
		SET_STATUS,
		SET_PARTICLE_PATTERN,
		TOGGLE_OPEN,
		TOGGLE_REMOTE,
		SET_TRADES_LIMIT,
		TOGGLE_RENTABLE,
		SET_RENT_PERIOD,
		SET_RENT_PERIOD_STREAK_LIMIT,
		SET_RENT_PERIOD_STREAK_LIMIT_DELAY,
		SET_UNRENTED_PARTICLE_PATTERN,
		SET_PAID_RENTS,
		SET_CURRENT_OWNER,
		UPDATED_RENT
	}

}
