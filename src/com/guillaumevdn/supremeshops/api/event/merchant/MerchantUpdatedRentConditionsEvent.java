package com.guillaumevdn.supremeshops.api.event.merchant;

import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;

public class MerchantUpdatedRentConditionsEvent extends MerchantEvent {

	// base
	private Condition condition;
	private Operation operation;

	public MerchantUpdatedRentConditionsEvent(Merchant merchant, Condition condition, Operation operation) {
		super(merchant);
		this.condition = condition;
		this.operation = operation;
	}

	// get
	public Condition getCondition() {
		return condition;
	}

	public Operation getOperation() {
		return operation;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

	// operation
	public static enum Operation {
		ADD_CONDITION,
		REMOVE_CONDITION
	}

}
