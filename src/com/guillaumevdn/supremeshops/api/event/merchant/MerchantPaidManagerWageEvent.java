package com.guillaumevdn.supremeshops.api.event.merchant;

import org.bukkit.event.HandlerList;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;

public class MerchantPaidManagerWageEvent extends MerchantEvent {

	// base
	private UserInfo manager;
	private int paidLateWages;

	public MerchantPaidManagerWageEvent(Merchant merchant, UserInfo manager, int paidLateWages) {
		super(merchant);
		this.manager = manager;
		this.paidLateWages = paidLateWages;
	}

	// get
	public UserInfo getManager() {
		return manager;
	}

	public int getPaidLateWages() {
		return paidLateWages;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
