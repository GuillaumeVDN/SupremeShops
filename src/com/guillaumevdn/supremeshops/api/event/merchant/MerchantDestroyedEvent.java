package com.guillaumevdn.supremeshops.api.event.merchant;

import org.bukkit.event.HandlerList;

import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.util.DestroyCause;

public class MerchantDestroyedEvent extends MerchantEvent {

	// base
	private DestroyCause cause;

	public MerchantDestroyedEvent(Merchant merchant, DestroyCause cause) {
		super(merchant);
		this.cause = cause;
	}

	// get
	public DestroyCause getCause() {
		return cause;
	}

	// handlers
	private static final HandlerList handlers = new HandlerList();

	@Override
	public HandlerList getHandlers() {
		return handlers;
	}

	public static HandlerList getHandlerList() {
		return handlers;
	}

}
