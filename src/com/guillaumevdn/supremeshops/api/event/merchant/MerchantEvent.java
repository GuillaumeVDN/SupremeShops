package com.guillaumevdn.supremeshops.api.event.merchant;

import org.bukkit.Bukkit;
import org.bukkit.event.Event;

import com.guillaumevdn.supremeshops.module.merchant.Merchant;

public abstract class MerchantEvent extends Event {

	// base
	private Merchant merchant;

	public MerchantEvent(Merchant merchant) {
		super(!Bukkit.isPrimaryThread());
		this.merchant = merchant;
	}

	// get
	public Merchant getMerchant() {
		return merchant;
	}

}
