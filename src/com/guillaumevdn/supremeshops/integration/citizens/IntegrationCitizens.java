package com.guillaumevdn.supremeshops.integration.citizens;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import com.guillaumevdn.gcorelegacy.lib.integration.PluginIntegration;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.gui.SSGui;
import com.guillaumevdn.supremeshops.module.trigger.type.AdminShopTriggerCitizensNpc;
import com.guillaumevdn.supremeshops.util.ClickType;

import net.citizensnpcs.api.event.NPCClickEvent;
import net.citizensnpcs.api.event.NPCLeftClickEvent;
import net.citizensnpcs.api.event.NPCRightClickEvent;
import net.citizensnpcs.api.npc.NPC;

public class IntegrationCitizens extends PluginIntegration implements Listener {

	// base
	public IntegrationCitizens(String pluginName) {
		super(pluginName);
	}

	// methods
	@Override
	public void enable() {
		// events
		Bukkit.getPluginManager().registerEvents(this, SupremeShops.inst());
		// register
		SupremeShops.inst().setIntegrationCitizens(this);
	}

	@Override
	public void disable() {
		// unregister events
		HandlerList.unregisterAll(this);
		// unregister
		SupremeShops.inst().setIntegrationCitizens(null);
	}

	// events
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void event(NPCRightClickEvent event) {
		npcClickEvent(event, ClickType.RIGHT_CLICK);
	}

	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void event(NPCLeftClickEvent event) {
		npcClickEvent(event, ClickType.LEFT_CLICK);
	}

	private void npcClickEvent(NPCClickEvent event, ClickType clickType) {
		NPC npc = event.getNPC();
		for (AdminShopTriggerCitizensNpc trigger : SupremeShops.inst().getModuleManager().getTriggersByClass(AdminShopTriggerCitizensNpc.class)) {
			Player player = event.getClicker();
			if (npc.getId() == trigger.getNpcId(player)) {
				if (trigger.areTriggerConditionsValid(player, true)) {
					SSGui gui = trigger.getGui(player);
					if (gui != null) {
						gui.build(player).open(player);
					} else {
						SupremeShops.inst().warning("Couldn't find GUI with id " + trigger.getGuiId(player) + " for trigger " + trigger.getId());
					}
				}
				return;
			}
		}
	}

}
