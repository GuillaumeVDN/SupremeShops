package com.guillaumevdn.supremeshops.integration.mythicmobs;

import org.bukkit.Bukkit;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

import com.guillaumevdn.gcorelegacy.lib.integration.PluginIntegration;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.gui.SSGui;
import com.guillaumevdn.supremeshops.module.trigger.type.AdminShopTriggerMythicmobsMob;

import io.lumine.xikage.mythicmobs.MythicMobs;
import io.lumine.xikage.mythicmobs.mobs.ActiveMob;

public class IntegrationMythicMobs extends PluginIntegration implements Listener {

	// base
	public IntegrationMythicMobs(String pluginName) {
		super(pluginName);
	}

	// methods
	@Override
	public void enable() {
		// events
		Bukkit.getPluginManager().registerEvents(this, SupremeShops.inst());
		// register
		SupremeShops.inst().setIntegrationMythicMobs(this);
	}

	@Override
	public void disable() {
		// unregister events
		HandlerList.unregisterAll(this);
		// unregister
		SupremeShops.inst().setIntegrationMythicMobs(null);
	}

	// events
	@SuppressWarnings("resource")
	@EventHandler(priority = EventPriority.HIGH, ignoreCancelled = true)
	public void event(PlayerInteractEntityEvent event) {
		Entity entity = event.getRightClicked();
		if (entity == null) return;
		for (ActiveMob mob : MythicMobs.inst().getMobManager().getActiveMobs()) {
			if (mob.getEntity() != null && entity.equals(mob.getEntity().getBukkitEntity())) {
				// mob
				for (AdminShopTriggerMythicmobsMob trigger : SupremeShops.inst().getModuleManager().getTriggersByClass(AdminShopTriggerMythicmobsMob.class)) {
					Player player = event.getPlayer();
					if (mob.getType().getInternalName().equalsIgnoreCase(trigger.getMobId(player))) {
						if (trigger.areTriggerConditionsValid(player, true)) {
							SSGui gui = trigger.getGui(player);
							if (gui != null) {
								gui.build(player).open(player);
							} else {
								SupremeShops.inst().warning("Couldn't find GUI with id " + trigger.getGuiId(player) + " for trigger " + trigger.getId());
							}
						}
						return;
					}
				}
				// return
				return;
			}
		}
	}

}
