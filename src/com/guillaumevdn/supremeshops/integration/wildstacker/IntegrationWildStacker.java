package com.guillaumevdn.supremeshops.integration.wildstacker;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;
import org.bukkit.inventory.ItemStack;

import com.bgsoftware.wildstacker.api.events.ItemStackEvent;
import com.guillaumevdn.gcorelegacy.lib.integration.PluginIntegration;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.display.items.ItemsDisplayableManager;

public class IntegrationWildStacker extends PluginIntegration implements Listener {

	// base
	public IntegrationWildStacker(String pluginName) {
		super(pluginName);
	}

	// methods
	@Override
	public void enable() {
		// reg
		Bukkit.getPluginManager().registerEvents(this, SupremeShops.inst());
		SupremeShops.inst().setIntegrationWildStacker(this);
	}

	@Override
	public void disable() {
		// unreg
		HandlerList.unregisterAll(this);
		SupremeShops.inst().setIntegrationWildStacker(null);
	}

	// events
	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void event(ItemStackEvent event) {
		// display item, cancel
		ItemStack item = event.getItem().getItemStack();
		if (item.getItemMeta() != null && item.getItemMeta().getDisplayName() != null && item.getItemMeta().getDisplayName().startsWith(ItemsDisplayableManager.ITEM_MARK_BEGIN)) {
			event.setCancelled(true);
		}
	}

}
