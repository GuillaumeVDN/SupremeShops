package com.guillaumevdn.supremeshops.integration.areashop;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.HandlerList;
import org.bukkit.event.Listener;

import com.guillaumevdn.gcorelegacy.lib.integration.PluginIntegration;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.DestroyCause;
import com.guillaumevdn.supremeshops.util.Locatable;

import me.wiefferink.areashop.events.notify.DeletedRegionEvent;
import me.wiefferink.areashop.events.notify.UnrentedRegionEvent;
import me.wiefferink.areashop.regions.GeneralRegion;

public class IntegrationAreaShop extends PluginIntegration implements Listener {

	// base
	public IntegrationAreaShop(String pluginName) {
		super(pluginName);
	}

	// methods
	@Override
	public void enable() {
		// events
		Bukkit.getPluginManager().registerEvents(this, SupremeShops.inst());
		// register
		SupremeShops.inst().setIntegrationAreaShop(this);
	}

	@Override
	public void disable() {
		// unregister events
		HandlerList.unregisterAll(this);
		// unregister
		SupremeShops.inst().setIntegrationAreaShop(null);
	}

	// events

	@EventHandler
	public void onRegionDeleted(DeletedRegionEvent event) {
		if (SupremeShops.inst().getConfiguration().getBoolean("areashop_regiondelete_delete", true)) {
			removeInRegion(event.getRegion());
		}
	}

	@EventHandler
	public void onRegionUnrented(UnrentedRegionEvent event) {
		if (SupremeShops.inst().getConfiguration().getBoolean("areashop_regionunrent_delete", true)) {
			removeInRegion(event.getRegion());
		}
	}

	private void removeInRegion(GeneralRegion region) {
		// get shops to destroy
		Set<Shop> shopsToDestroy = new HashSet<Shop>();
		for (Shop s : SupremeShops.inst().getData().getShops().getAll().values()) {
			if (!(s instanceof Locatable)) continue;
			Locatable shop = (Locatable) s;
			BlockCoords min = new BlockCoords(region.getWorld(), region.getMinimumPoint().getBlockX(), region.getMinimumPoint().getBlockY(), region.getMinimumPoint().getBlockZ());
			BlockCoords max = new BlockCoords(region.getWorld(), region.getMaximumPoint().getBlockX(), region.getMaximumPoint().getBlockY(), region.getMaximumPoint().getBlockZ());
			if (Utils.isBlockInArea(shop.getBlock(), min, max)) {
				shopsToDestroy.add(s);
			}
		}
		// get merchants to destroy
		Set<Merchant> merchantsToDestroy = new HashSet<Merchant>();
		for (Merchant m : SupremeShops.inst().getData().getMerchants().getAll().values()) {
			if (!(m instanceof Locatable)) continue;
			Locatable merchant = (Locatable) m;
			BlockCoords min = new BlockCoords(region.getWorld(), region.getMinimumPoint().getBlockX(), region.getMinimumPoint().getBlockY(), region.getMinimumPoint().getBlockZ());
			BlockCoords max = new BlockCoords(region.getWorld(), region.getMaximumPoint().getBlockX(), region.getMaximumPoint().getBlockY(), region.getMaximumPoint().getBlockZ());
			if (Utils.isBlockInArea(merchant.getBlock(), min, max)) {
				merchantsToDestroy.add(m);
			}
		}
		// destroy merchants
		if (!merchantsToDestroy.isEmpty()) {
			for (Merchant merchant : merchantsToDestroy) {
				shopsToDestroy.addAll(merchant.destroy(DestroyCause.AREASHOP_REGION_END, false));
			}
			SupremeShops.inst().getData().getShops().deleteAll(shopsToDestroy);
		}
		// destroy shops
		Set<Merchant> moreMerchantsToPush = new HashSet<Merchant>();
		if (!shopsToDestroy.isEmpty()) {
			for (Shop shop : shopsToDestroy) {
				moreMerchantsToPush.addAll(shop.destroy(DestroyCause.AREASHOP_REGION_END, false));
			}
			SupremeShops.inst().getData().getShops().deleteAll(shopsToDestroy);
		}
		// push remaining merchants
		moreMerchantsToPush.removeAll(merchantsToDestroy);
		if (!moreMerchantsToPush.isEmpty()) {
			SupremeShops.inst().getData().getMerchants().pullAsync(moreMerchantsToPush, null);
		}
	}

}
