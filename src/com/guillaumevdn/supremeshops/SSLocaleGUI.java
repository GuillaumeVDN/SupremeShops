package com.guillaumevdn.supremeshops;

import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.messenger.Text;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;

public class SSLocaleGUI {

	private static Text n(String id, Object... content) {
		return new Text(id, SSLocale.file, content);
	}

	private static List<String> l(String... content) {
		return Utils.asList(content);
	}

	public static final Text GUI_SUPREMESHOPS_EDITOR_GENERIC_SETTINGLORE = n("GUI_SUPREMESHOPS_EDITOR_GENERIC_SETTINGLORE",
			"en_US", l("&6Setting (if applicable)"),
			"fr_FR", l("&6Paramètre (si applicable)"),
			"zh_CN", l("&6Setting (if applicable)"),
			"zh_TW", l("&6設置 &7(如果可用)"),
			"ru_RU", l("&6Настройка (если применимо)"),
			"hu_HU", l("&6Beállítások (ha megfelelő)"),
			"es_ES", l("&6Configuración (si pueden aplicarse)")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GENERIC_OPERATIONLORE = n("GUI_SUPREMESHOPS_EDITOR_GENERIC_OPERATIONLORE",
			"en_US", l("&7Operation to perform/check"),
			"fr_FR", l("&7Opération à effectuer/vérifier"),
			"zh_CN", l("&7Operation to perform/check"),
			"zh_TW", l("&7執行/檢查 操作"),
			"ru_RU", l("&7Операция для проверки\\выполнения"),
			"hu_HU", l("&7Művelet végrehajts/ellenőrzés"),
			"es_ES", l("&7Operación a performar/checkeo")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_SHOPBLOCKTYPELORE = n("GUI_SUPREMESHOPS_EDITOR_SHOPBLOCKTYPELORE",
			"en_US", l("&7Shop block type"),
			"fr_FR", l("&7Type de block de shop"),
			"zh_CN", l("&7Shop block type"),
			"zh_TW", l("&7商店方塊類型"),
			"ru_RU", l("&7Тип блока магазина"),
			"hu_HU", l("&7Bolt blokk típus"),
			"es_ES", l("&7Tienda (tipo de bloque)")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_SHOPTYPELORE = n("GUI_SUPREMESHOPS_EDITOR_SHOPTYPELORE",
			"en_US", l("&7Shop type"),
			"fr_FR", l("&7Type de shop"),
			"zh_CN", l("&7Shop type"),
			"zh_TW", l("&7商店類型"),
			"ru_RU", l("&7Тип магазина"),
			"hu_HU", l("&7Bolt típus"),
			"es_ES", l("&7Tipo de tienda")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_TYPELORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_TYPELORE",
			"en_US", l("&7Type of condition"),
			"fr_FR", l("&7Type de condition"),
			"zh_CN", l("&7Type of condition"),
			"zh_TW", l("&7條件類別"),
			"ru_RU", l("&7Тип условия"),
			"hu_HU", l("&7Feltétel típusa"),
			"es_ES", l("&7Tipo de condición")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GENERIC_AMOUNTLORE = n("GUI_SUPREMESHOPS_EDITOR_GENERIC_AMOUNTLORE",
			"en_US", l("&7Amount"),
			"fr_FR", l("&7Montant"),
			"zh_CN", l("&7Amount"),
			"zh_TW", l("&7數量"),
			"ru_RU", l("&7Количество"),
			"hu_HU", l("&7Mennyiség"),
			"es_ES", l("&7Cantidad")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GENERIC_PERMISSIONLORE = n("GUI_SUPREMESHOPS_EDITOR_GENERIC_PERMISSIONLORE",
			"en_US", l("&7Permission"),
			"fr_FR", l("&7Permission"),
			"zh_CN", l("&7Permission"),
			"zh_TW", l("&7權限碼"),
			"ru_RU", l("&7Пермишен"),
			"hu_HU", l("&7Jog"),
			"es_ES", l("&7Permiso")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GENERIC_PERMISSIONDESCRIPTIONLORE = n("GUI_SUPREMESHOPS_EDITOR_GENERIC_PERMISSIONDESCRIPTIONLORE",
			"en_US", l("&7What does this permission allow"),
			"fr_FR", l("&7Qu'est-ce que cette permission autorise"),
			"zh_CN", l("&7What does this permission allow"),
			"zh_TW", l("&7這個權限允許什麼"),
			"ru_RU", l("&7Что даёт этот пермишен"),
			"hu_HU", l("&7Mit engedjen ez a jog"),
			"es_ES", l("&7Qué permite este permiso")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMESTARTDAYLORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMESTARTDAYLORE",
			"en_US", l("&7Time frame start day"),
			"fr_FR", l("&7Jour de début de la période"),
			"zh_CN", l("&7Time frame start day"),
			"zh_TW", l("&7時間範圍 開始日"),
			"ru_RU", l("&7Временные рамки начала работы"),
			"hu_HU", l("&7Időkeret kezdete napban"),
			"es_ES", l("&7Período de inicio del día")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMEENDDAYLORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMEENDDAYLORE",
			"en_US", l("&7Time frame end day"),
			"fr_FR", l("&7Jour de fin de la période"),
			"zh_CN", l("&7Time frame end day"),
			"zh_TW", l("&7時間範圍 結束日"),
			"ru_RU", l("&7Временные рамки окончания работы"),
			"hu_HU", l("&7Időkeret vége napban"),
			"es_ES", l("&7Período de finalización del día")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMESTARTTICKSLORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMESTARTTICKSLORE",
			"en_US", l("&7Time frame start ticks"),
			"fr_FR", l("&7Ticks de début de période"),
			"zh_CN", l("&7Time frame start ticks"),
			"zh_TW", l("&7時間範圍 開始遊戲刻"),
			"ru_RU", l("&7Временные рамки начала работы в тиках"),
			"hu_HU", l("&7Időkeret kezdete tickben"),
			"es_ES", l("&7Período de inicio en ticks")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMEENDTICKSLORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMEENDTICKSLORE",
			"en_US", l("&7Time frame end ticks"),
			"fr_FR", l("&7Ticks de fin de la période"),
			"zh_CN", l("&7Time frame end ticks"),
			"zh_TW", l("&7時間範圍 結束遊戲刻"),
			"ru_RU", l("&7Временные рамки окончания работы в тиках"),
			"hu_HU", l("&7Időkeret vége Tickben"),
			"es_ES", l("&7Período de finalización en ticks")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_ITEMSLORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_ITEMSLORE",
			"en_US", l("&7List of items needed"),
			"fr_FR", l("&7Liste d'items requis"),
			"zh_CN", l("&7List of items needed"),
			"zh_TW", l("&7所需的物品列表"),
			"ru_RU", l("&7Список необходимых предметов"),
			"hu_HU", l("&7Szükséges elemek listája"),
			"es_ES", l("&7Lista de ítems necesarios")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_ITEMSNEEDEDLORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_ITEMSNEEDEDLORE",
			"en_US", l("&7Amount of different items required (in the &6items &7list)"),
			"fr_FR", l("&7Nombre d'items différents requis (dans la liste &6items&7)"),
			"zh_CN", l("&7Amount of different items required (in the &6items &7list)"),
			"zh_TW", l("&7在 &6物品列表 &7中所需不同物品的數量"),
			"ru_RU", l("&7Количество нужных предметов (в списке &6предметов&7)"),
			"hu_HU", l("&7Különböző itemek szükségének mennyisége (az &6items &7listában)"),
			"es_ES", l("&7Cantidad diferente de ítems necesarios (en la lista de &6ítems&7)")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMESTARTHOURLORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMESTARTHOURLORE",
			"en_US", l("&7Time frame start hour"),
			"fr_FR", l("&7Heure de début de la période"),
			"zh_CN", l("&7Time frame start hour"),
			"zh_TW", l("&7時間範圍 開始小時"),
			"ru_RU", l("&7Час открытия"),
			"hu_HU", l("&7Időkeret kezdete órában"),
			"es_ES", l("&7Período de inicio (hora)")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMESTARTMINUTELORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMESTARTMINUTELORE",
			"en_US", l("&7Time frame start minute"),
			"fr_FR", l("&7Minute de début de la période"),
			"zh_CN", l("&7Time frame start minute"),
			"zh_TW", l("&7時間範圍 開始分鐘"),
			"ru_RU", l("&7Минута открытия"),
			"hu_HU", l("&7Időkeret kezdete percben"),
			"es_ES", l("&7Período de inicio (minuto)")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMEENDHOURLORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMEENDHOURLORE",
			"en_US", l("&7Time frame end hour"),
			"fr_FR", l("&7Heure de fin de la période"),
			"zh_CN", l("&7Time frame end hour"),
			"zh_TW", l("&7時間範圍 結束小時"),
			"ru_RU", l("&7Час закрытия"),
			"hu_HU", l("&7Időkeret vége órában"),
			"es_ES", l("&7Período de finalización (hora)")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMEENDMINUTELORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_TIMEENDMINUTELORE",
			"en_US", l("&7Time frame end minute"),
			"fr_FR", l("&7Minute de fin de la période"),
			"zh_CN", l("&7Time frame end minute"),
			"zh_TW", l("&7時間範圍 結束分鐘"),
			"ru_RU", l("&7Минута закрытия"),
			"hu_HU", l("&7Időkeret vége percben"),
			"es_ES", l("&7Período de finalización (minuto)")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_VARIABLELORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_VARIABLELORE",
			"en_US", l("&7Name of variable"),
			"fr_FR", l("&7Nom de la variable"),
			"zh_CN", l("&7变量名"),
			"zh_TW", l("&7變數名稱"),
			"ru_RU", l("&7Название переменной"),
			"hu_HU", l("&7Változók neve"),
			"es_ES", l("&7Nombre de la variable")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_VALUELORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_VALUELORE",
			"en_US", l("&7Value of variable"),
			"fr_FR", l("&7Valeur de la variable"),
			"zh_CN", l("&7Value of variable"),
			"zh_TW", l("&7變數的數值"),
			"ru_RU", l("&7Значение переменной"),
			"hu_HU", l("&7Változó értéke"),
			"es_ES", l("&7Valor de la variable")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GENERIC_XPLEVELLORE = n("GUI_SUPREMESHOPS_EDITOR_GENERIC_XPLEVELLORE",
			"en_US", l("&7Player XP level"),
			"fr_FR", l("&7Niveau d'XP du joueur"),
			"zh_CN", l("&7玩家经验等级"),
			"zh_TW", l("&7玩家的經驗等擊"),
			"ru_RU", l("&7Уровень игрока"),
			"hu_HU", l("&7Játékos XP szint"),
			"es_ES", l("&7XP del jugador")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GENERIC_PLAYERHUNGERLORE = n("GUI_SUPREMESHOPS_EDITOR_GENERIC_PLAYERHUNGERLORE",
			"en_US", l("&7Player hunger level"),
			"fr_FR", l("&7Niveau de faim du joueur"),
			"zh_CN", l("&7玩家饥饿度"),
			"zh_TW", l("&7玩家的飢餓度"),
			"ru_RU", l("&7Голод игрока"),
			"hu_HU", l("&7Player éhségi szint"),
			"es_ES", l("&7Hambre del jugador")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GENERIC_TOKENENCHANTTOKENSLORE = n("GUI_SUPREMESHOPS_EDITOR_GENERIC_TOKENENCHANTTOKENSLORE",
			"en_US", l("&7TokenEnchant tokens"),
			"fr_FR", l("&7Tokens TokenEnchant"),
			"zh_TW", l("&7TokenEnchant 代幣"),
			"ru_RU", l("&7TokenEnchant токены"),
			"hu_HU", l("&7TokenEnchant token"),
			"es_ES", l("&7TokenEnchant tokens")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GENERIC_PLAYERHEALTHLORE = n("GUI_SUPREMESHOPS_EDITOR_GENERIC_PLAYERHEALTHLORE",
			"en_US", l("&7Player health"),
			"fr_FR", l("&7Vie du joueur"),
			"zh_CN", l("&7玩家血量"),
			"zh_TW", l("&7玩家血量"),
			"ru_RU", l("&7Здоровье игрока"),
			"hu_HU", l("&7Játékos élet"),
			"es_ES", l("&7Vida del jugador")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_OBJECT_CONDITIONSLORE = n("GUI_SUPREMESHOPS_EDITOR_OBJECT_CONDITIONSLORE",
			"en_US", l("&7List of conditions"),
			"fr_FR", l("&7Liste de conditions"),
			"zh_CN", l("&7条件列表"),
			"zh_TW", l("&7條件列表"),
			"ru_RU", l("&7Список условий"),
			"hu_HU", l("&7Feltételek listája"),
			"es_ES", l("&7Lista de condiciones")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_OBJECT_CONDITIONSREQUIREDVALIDLORE = n("GUI_SUPREMESHOPS_EDITOR_OBJECT_CONDITIONSREQUIREDVALIDLORE",
			"en_US", l("&7How many conditions needs to be valid"),
			"fr_FR", l("&7Combien de conditions doivent être valides"),
			"zh_CN", l("&7How many conditions needs to be valid"),
			"zh_TW", l("&7有多少條件必須要是有效的"),
			"ru_RU", l("&7Сколько условий должно быть выполнено"),
			"hu_HU", l("&7Hány feltétel szükséges, hogy érvényes legyen"),
			"es_ES", l("&7Cuántas condiciones necesitan ser válidas")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_OBJECT_CONDITIONSREQUIREDNOTVALIDLORE = n("GUI_SUPREMESHOPS_EDITOR_OBJECT_CONDITIONSREQUIREDNOTVALIDLORE",
			"en_US", l("&7How many conditions needs to be NOT valid"),
			"fr_FR", l("&7Combien de conditions doivent être NON valides"),
			"zh_CN", l("&7How many conditions needs to be NOT valid"),
			"zh_TW", l("&7有多少條件必須是無效的"),
			"ru_RU", l("&7Сколько условий НЕ должно быть выполнено"),
			"hu_HU", l("&7Hány feltétel szükségesm hogy ne legyen érvényes"),
			"es_ES", l("&7Cuantas condiciones deben NO ser válidas")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_OBJECT_CONDITIONSGENERALERRORMESSAGELORE = n("GUI_SUPREMESHOPS_EDITOR_OBJECT_CONDITIONSGENERALERRORMESSAGELORE",
			"en_US", l("&7If this is specified, then this message will be", "&7 sent if the conditions are not completed", "&7 instead of all the conditions error messages"),
			"fr_FR", l("&7Si ceci est spécifié, alors ce message sera", "&7 envoyé si les conditions ne sont pas complétées", "&7 au lieu de tous les messages d'erreurs des conditions"),
			"zh_CN", l("&7If this is specified, then this message will be", "&7 sent if the conditions are not completed", "&7 instead of all the conditions error messages"),
			"zh_TW", l("&7如果這個有指定，那麼這個訊息", "&7會在這個條件未達成時發送給玩家", "&7而不是未達成其他任一條件時顯示的錯誤訊息"),
			"ru_RU", l("&7Если это сообщение установлено, то", "&7 оно будет отправлено, если условия не выполнены,", "&7 заменяя основное сообщение об ошибке."),
			"hu_HU", l("&7Ha ez megvan adva, akkor ez az üzenet lesz", "&7 elküldve, ha a feltételek nem teljesülnek", "&7 a feltételek hibaüzenete helyett"),
			"es_ES", l("&7Si esto es especificado, entonces el mensaje será", "&7enviado si las condiciones no se han completado", "&7en vez de completarse todos los mensajes de error", "&7de condición")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_ERRORMESSAGELORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_ERRORMESSAGELORE",
			"en_US", l("&7An error message displayed when", "&7 the condition isn't completed"),
			"fr_FR", l("&7Un message d'erreur affiché quand", "&7 la condition n'est pas complétée"),
			"zh_CN", l("&7不满足条件时", "&7显示的错误消息"),
			"zh_TW", l("&7未達成條件時", "&7顯示的錯誤訊息"),
			"ru_RU", l("&7Выводимое сообщение об ошибке,", "&7 если условия не выполнены"),
			"hu_HU", l("&7Egy hiba jelent meg, mikor a", "&7 a feltétel nem teljesült"),
			"es_ES", l("&7Un mensaje de error será mostrado cuando", "&7las condiciones no se completen")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_WORLDNAMELORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_WORLDNAMELORE",
			"en_US", l("&7Name of world"),
			"fr_FR", l("&7Nom du monde"),
			"zh_CN", l("&7世界名"),
			"zh_TW", l("&7世界名稱"),
			"ru_RU", l("&7Название мира"),
			"hu_HU", l("&7Világ neve"),
			"es_ES", l("&7Nombre del mundo")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_WORLDGUARDREGIONNAMELORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_WORLDGUARDREGIONNAMELORE",
			"en_US", l("&7Name of WorldGuard region"),
			"fr_FR", l("&7Nom de la région WorldGuard"),
			"zh_CN", l("&7WorldGuard区域名"),
			"zh_TW", l("&7WorldGuard區域名稱"),
			"ru_RU", l("&7Название региона"),
			"hu_HU", l("&7WorldGuard Régió neve"),
			"es_ES", l("&7Nombre de la región del WorldGuard")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_OBJECTSIDELORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_OBJECTSIDELORE",
			"en_US", l("&7Object side"),
			"fr_FR", l("&7Côté d'objet"),
			"zh_CN", l("&7Object side"),
			"zh_TW", l("&7物件類別"),
			"ru_RU", l("&7Сторона объекта"),
			"hu_HU", l("&7Tárgy oldal"),
			"es_ES", l("&7Lugar del objeto (SERVER o PLAYER)")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_OBJECTTYPELORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_OBJECTTYPELORE",
			"en_US", l("&7Object type"),
			"fr_FR", l("&7Type d'objet"),
			"zh_CN", l("&7"),
			"zh_TW", l("&7物件類型"),
			"ru_RU", l("&7Тип объекта"),
			"hu_HU", l("&7Tárgy típus"),
			"es_ES", l("&7Tipo de objeto")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_CONDITION_CONDITIONTYPELORE = n("GUI_SUPREMESHOPS_EDITOR_CONDITION_CONDITIONTYPELORE",
			"en_US", l("&7Condition type"),
			"fr_FR", l("&7Type de condition"),
			"zh_CN", l("&7条件类型"),
			"zh_TW", l("&7條件類型"),
			"ru_RU", l("&7Тип условия"),
			"hu_HU", l("&7Feltétel típus"),
			"es_ES", l("&7Tipo de condición")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_MODIFIER_CONDITIONSLORE = n("GUI_SUPREMESHOPS_EDITOR_MODIFIER_CONDITIONSLORE",
			"en_US", l("&7A list of conditions required for this", "&7 modifier to be applied to a shop"),
			"fr_FR", l("&7Une liste de conditions requises pour que", "&7 le modifier soit appliqué à un shop"),
			"zh_CN", l("&7A list of conditions required for this", "&7 modifier to be applied to a shop"),
			"zh_TW", l("&7一個交易額外編輯功能會應用於商店的條件列表"),
			"ru_RU", l("&7Список условий, необходимых для", "&7 этого модификатора в магазине"),
			"hu_HU", l("&7Azon feltételek listája, amelyek ahhoz szükségesek,", "&7 hogy ezt a módosítót alkalmazzák az boltban"),
			"es_ES", l("&7Una lista de condiciones requeridas para que el modificador se aplique a la tienda")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_MODIFIER_TARGETOBJECTSIDESLORE = n("GUI_SUPREMESHOPS_EDITOR_MODIFIER_TARGETOBJECTSIDESLORE",
			"en_US", l("&7What objects sides will be affected by the modifier"),
			"fr_FR", l("&7Quels côtés d'objets vont être affectés par le modifier"),
			"zh_CN", l("&7What objects sides will be affected by the modifier"),
			"zh_TW", l("&7哪一個商店類別會被交易額外功能影響"),
			"ru_RU", l("&7Какие стороны объекта будут затронуты модификатором"),
			"hu_HU", l("&7Milyen tárgyak oldalát befolyásolja a módosító"),
			"es_ES", l("&7Qué posición del objeto será afectado por el modificador")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_MODIFIER_TARGETOBJECTTYPESLORE = n("GUI_SUPREMESHOPS_EDITOR_MODIFIER_TARGETOBJECTTYPESLORE",
			"en_US", l("&7What objects types will be affected by the modifier"),
			"fr_FR", l("&7Quels types d'objets vont être affectés par le modifier"),
			"zh_CN", l("&7What objects types will be affected by the modifier"),
			"zh_TW", l("&7哪一個商店類型會被交易額外功能影響"),
			"ru_RU", l("&7Какие типы объектов будут затронуты модификатором"),
			"hu_HU", l("&7Milyen tárgyak típusát befolyásolja a módosító"),
			"es_ES", l("&7Qué tipo de objetos serán afectados por el modificador")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_MODIFIER_FORMULALORE = n("GUI_SUPREMESHOPS_EDITOR_MODIFIER_FORMULALORE",
			"en_US", l("&7Modifier formula to apply on the final amount"),
			"fr_FR", l("&7Formule de modifier à appliquer sur le montant final"),
			"zh_CN", l("&7应用于最终数量的修正公式"),
			"zh_TW", l("&7這個公式會應用於最終交易數值"),
			"ru_RU", l("&7Формула модификатора, применяемая к финальной сумме"),
			"hu_HU", l("&7A végső összegre alkalmazandó módosító képlet"),
			"es_ES", l("&7Fórmula del modificador que se aplicará al resultante final")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_SHOPCREATIONTAX_CONDITIONSLORE = n("GUI_SUPREMESHOPS_EDITOR_SHOPCREATIONTAX_CONDITIONSLORE",
			"en_US", l("&7A list of conditions required for this", "&7 tax to be applied to a shop"),
			"fr_FR", l("&7Une liste de conditions requises pour que", "&7 la taxe soit appliqué à un shop"),
			"zh_CN", l("&7商店收税", "&7所需的条件列表"),
			"zh_TW", l("&7將這個稅收應用於商店時所需的條件列表"),
			"ru_RU", l("&7Список условий, необходимых для того,", "&7 чтобы данный размер комиссии был применён"),
			"hu_HU", l("&7Az adó feltételeinek listája a boltban"),
			"es_ES", l("&7Una lista de condiciones requeridas para que el impuesto se aplique a la tienda")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_SHOPCREATIONTAX_VAULTMONEYTAXLORE = n("GUI_SUPREMESHOPS_EDITOR_SHOPCREATIONTAX_VAULTMONEYTAXLORE",
			"en_US", l("&7A Vault money tax taken"),
			"fr_FR", l("&7Une taxe d'argent Vault prise"),
			"zh_CN", l("&7A Vault money tax taken"),
			"zh_TW", l("&7一個收取稅收的金額"),
			"ru_RU", l("&7Потерянная комиссия Vault"),
			"hu_HU", l("&7A Vault pénz adó elvétele"),
			"es_ES", l("&7Se retiró dinero de tus fondos para impuestos")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADES = n("GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADES",
			"en_US", l("&7The trades of this admin shop"),
			"fr_FR", l("&7Les échanges de ce shop admin"),
			"zh_CN", l("&7The trades of this admin shop"),
			"zh_TW", l("&7這間管理員商店中的交易"),
			"ru_RU", l("&7Обмены этого админ-шопа"),
			"hu_HU", l("&7Admin bolt kereskedései"),
			"es_ES", l("&7Los comercios de esta tienda de tipo administrador")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGERS = n("GUI_SUPREMESHOPS_EDITOR_TRIGGERS",
			"en_US", l("&7The triggers of this admin shop"),
			"fr_FR", l("&7Les triggers de ce shop admin"),
			"zh_CN", l("&7管理员商店的触发器"),
			"zh_TW", l("&7這間管理員商店的觸發器"),
			"ru_RU", l("&7Триггеры этого админ-шопа"),
			"hu_HU", l("&7Admin bolt indítója"),
			"es_ES", l("&7Los triggers (o desencadenantes) de esta tienda de tipo administrador")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADENAME = n("GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADENAME",
			"en_US", l("&7The name of this trade"),
			"fr_FR", l("&7Le nom de cet échange"),
			"zh_CN", l("&7交易名"),
			"zh_TW", l("&7這筆交易的名稱"),
			"ru_RU", l("&7Название обмена"),
			"hu_HU", l("&7Ezen kereskedés neve"),
			"es_ES", l("&7El nombre de este comercio")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADESLIMIT = n("GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADESLIMIT",
			"en_US", l("&7The maximum amount of trades per player"),
			"fr_FR", l("&7Le nombre d'échanges maximum par joueur"),
			"zh_TW", l("&7每位玩家的最大交易次數"),
			"ru_RU", l("&7Лимит обменов на игрока"),
			"hu_HU", l("&7A maximális kereskedések száma per játékos"),
			"es_ES", l("&7El número máximo de comercios por jugador")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADEOBJECTS = n("GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADEOBJECTS",
			"en_US", l("&7Obects of this trade"),
			"fr_FR", l("&7Objets de cet échange"),
			"zh_CN", l("&7这个交易的物品"),
			"zh_TW", l("&7這筆交易的物件"),
			"ru_RU", l("&7Предметы обмена"),
			"hu_HU", l("&7Kereskedés tárgyai"),
			"es_ES", l("&7Objetos de este comercio")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADECONDITIONS = n("GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADECONDITIONS",
			"en_US", l("&7Conditions for this trade to be available"),
			"fr_FR", l("&7Conditions pour que cet échange soit disponible"),
			"zh_CN", l("&7可进行交易的条件"),
			"zh_TW", l("&7可進行交易的條件"),
			"ru_RU", l("&7Условия актуальности этого обмена"),
			"hu_HU", l("&7Feltételek a kereskedés elérhetőségére"),
			"es_ES", l("&7Condiciones para que este comercio esté disponible")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGER_TYPELORE = n("GUI_SUPREMESHOPS_EDITOR_TRIGGER_TYPELORE",
			"en_US", l("&7Type of trigger"),
			"fr_FR", l("&7Type de trigger"),
			"zh_CN", l("&7触发器类型"),
			"zh_TW", l("&7觸發器的類型"),
			"ru_RU", l("&7Тип триггера"),
			"hu_HU", l("&7Indító típusa"),
			"es_ES", l("&7Tipo de trigger (o desencadenante)")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGER_NAMELORE = n("GUI_SUPREMESHOPS_EDITOR_TRIGGER_NAMELORE",
			"en_US", l("&7Display name of trigger"),
			"fr_FR", l("&7Nom d'affichage de trigger"),
			"zh_CN", l("&7触发器展示名"),
			"zh_TW", l("&7觸發器的顯示名稱"),
			"ru_RU", l("&7Название триггера"),
			"hu_HU", l("&7Kijelzi az indító nevét"),
			"es_ES", l("&7Mostrar el nombre del trigger")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGER_PARTICLEPATTERNIDLORE = n("GUI_SUPREMESHOPS_EDITOR_TRIGGER_PARTICLEPATTERNIDLORE",
			"en_US", l("&7What particle pattern should display around this trigger"),
			"fr_FR", l("&7Quel pattern de particules doit être affiché autour de ce trigger"),
			"zh_CN", l("&7What particle pattern should display around this trigger"),
			"zh_TW", l("&7哪一個類型的粒子效果要在這個觸發器周圍顯示"),
			"ru_RU", l("&7Какой паттерн частиц будет отображаться вокруг этого триггера"),
			"hu_HU", l("&7Milyen részecske minta jelenjen meg, amikor rákattolsz az indítóra"),
			"es_ES", l("&7Qué patrón de partículas deberían mostrarse alrededor de este trigger (o desencadenante)")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGER_GUIIDLORE = n("GUI_SUPREMESHOPS_EDITOR_TRIGGER_GUIIDLORE",
			"en_US", l("&7What GUI should open when clicking this trigger"),
			"fr_FR", l("&7Quel GUI doit s'ouvrir lorsqu'on clique sur ce trigger"),
			"zh_CN", l("&7点击触发器时打开哪一个GUI"),
			"zh_TW", l("&7點擊這個觸發器時要打開哪個GUI"),
			"ru_RU", l("&7Какое меню будет открыто при нажатии на триггер"),
			"hu_HU", l("&7Milyen GUI nyíljon meg, amikor kattintasz erre az indítóra"),
			"es_ES", l("&7Qué GUI debería abrirse cuando se haga clic en este trigger")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGER_TRIGGERPERMISSIONLORE = n("GUI_SUPREMESHOPS_EDITOR_TRIGGER_TRIGGERPERMISSIONLORE",
			"en_US", l("&7To set if the trigger be accessible only by permission"),
			"fr_FR", l("&7À définir si le trigger doit être accessible uniquement par permission"),
			"zh_CN", l("&7设置触发器是否只能通过权限触发"),
			"zh_TW", l("&7設置觸發器是否只能通過權限觸發"),
			"ru_RU", l("&7Установить активность триггера только при наличии пермишена"),
			"hu_HU", l("&7Annak beállítása, hogy a triggert csak engedéllyel lehessen elérni"),
			"es_ES", l("&7Para seleccionar si el trigger debería ser solo accesible por permiso")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGER_TRIGGERCONDITIONSLORE = n("GUI_SUPREMESHOPS_EDITOR_TRIGGER_TRIGGERCONDITIONSLORE",
			"en_US", l("&7To set if the trigger be accessible only", "&7 under certain conditions"),
			"fr_FR", l("&7À définir si le trigger doit être accessible", "&7 uniquement sous certains conditions"),
			"zh_CN", l("&7设置触发器是否", "&7只在特定条件下可以使用"),
			"zh_TW", l("&7設置觸發器是否", "&7只能在特定條件下使用"),
			"ru_RU", l("&7Установить активацию данного тригера", "&7 ТОЛЬКО при выполнении некоторых условий"),
			"hu_HU", l("&7Hogy beállítsd, ha a trigger csak", "&7 bizonyos feltételek alatt érhető el"),
			"es_ES", l("&7Para seleccionar si el trigger debe ser accesible solo bajo ciertas condiciones")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGER_GUISIZELORE = n("GUI_SUPREMESHOPS_EDITOR_TRIGGER_GUISIZELORE",
			"en_US", l("&7Size of the GUI opened on interaction"),
			"fr_FR", l("&7Taille du GUI ouvert lors de l'interaction"),
			"zh_CN", l("&7交互时打开的GUI的大小"),
			"zh_TW", l("&7交互時開啟的GUI大小"),
			"ru_RU", l("&7Размер меню, открываемого по нажатию"),
			"hu_HU", l("&7Az interakció során megnyitott GUI mérete"),
			"es_ES", l("&7Tamaño de la GUI abierta por la interacción")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGER_BLOCKLORE = n("GUI_SUPREMESHOPS_EDITOR_TRIGGER_BLOCKLORE",
			"en_US", l("&7Location of block"),
			"fr_FR", l("&7Emplacement du bloc"),
			"zh_CN", l("&7方块位置"),
			"zh_TW", l("&7方塊位置"),
			"ru_RU", l("&7Местонахождение блока"),
			"hu_HU", l("&7Blokk helyszíne"),
			"es_ES", l("&7Localización del bloque")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGER_ENTITYUUIDLORE = n("GUI_SUPREMESHOPS_EDITOR_TRIGGER_ENTITYUUIDLORE",
			"en_US", l("&7The entity UUID"),
			"fr_FR", l("&7L'UUID de l'entité"),
			"zh_CN", l("&7实体的UUID"),
			"zh_TW", l("&7實體的UUID"),
			"ru_RU", l("&7UUID сущности"),
			"hu_HU", l("&7Entitás UUID"),
			"es_ES", l("&7La UUID de la entidad")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGER_ENTITYNAMELORE = n("GUI_SUPREMESHOPS_EDITOR_TRIGGER_ENTITYNAMELORE",
			"en_US", l("&7The entity name"),
			"fr_FR", l("&7Le nom de l'entité"),
			"zh_CN", l("&7实体名"),
			"zh_TW", l("&7實體的名稱"),
			"ru_RU", l("&7Название сущности"),
			"hu_HU", l("&7Entitás neve"),
			"es_ES", l("&7El nombre de la entidad")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGER_NPCIDLORE = n("GUI_SUPREMESHOPS_EDITOR_TRIGGER_NPCIDLORE",
			"en_US", l("&7The GCoreLegacy NPC id"),
			"fr_FR", l("&7L'id du NPC GCoreLegacy"),
			"zh_CN", l("&7GCore的 NPC id"),
			"zh_TW", l("&7Gcore的 NPC ID"),
			"ru_RU", l("&7ID NPC в GCoreLegacy"),
			"hu_HU", l("&7GCore NPC azonosító"),
			"es_ES", l("&7El GCoreLegacy NPC id")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGER_CITIZENSNPCIDLORE = n("GUI_SUPREMESHOPS_EDITOR_TRIGGER_CITIZENSNPCIDLORE",
			"en_US", l("&7The Citizens NPC id"),
			"fr_FR", l("&7L'id du NPC Citizens"),
			"zh_CN", l("&7Citizens的NPC id"),
			"zh_TW", l("&7Citizens的NPC ID"),
			"ru_RU", l("&7ID сущености в Citizens"),
			"hu_HU", l("&7Citizens NPC azonosító"),
			"es_ES", l("&7La ID del NPC dada por Citizens")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRIGGER_MYTHICSMOBIDLORE = n("GUI_SUPREMESHOPS_EDITOR_TRIGGER_MYTHICSMOBIDLORE",
			"en_US", l("&7The MythicMobs mob type id"),
			"fr_FR", l("&7L'id du type de mob MythicMobs"),
			"zh_CN", l("&7MythicMobs 怪物id"),
			"zh_TW", l("&7MythicMobs 怪物ID"),
			"ru_RU", l("&7ID сущености в MythicMobs"),
			"hu_HU", l("&7MythicMobs mob típus azonosítója"),
			"es_ES", l("&7La ID del NPC dada por Mythicmobs")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_TYPELORE = n("GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_TYPELORE",
			"en_US", l("&7Type of object"),
			"fr_FR", l("&7Type d'objet"),
			"zh_CN", l("&7object类型"),
			"zh_TW", l("&7物件類型"),
			"ru_RU", l("&7Тип объекта"),
			"hu_HU", l("&7Tárgy típusa"),
			"es_ES", l("&7Tipo de objeto")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_SIDELORE = n("GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_SIDELORE",
			"en_US", l("&7Side of the object"),
			"fr_FR", l("&7Côté de l'objet"),
			"zh_CN", l("&7Side of the object"),
			"zh_TW", l("&7物件類別"),
			"ru_RU", l("&7Сторона объекта"),
			"hu_HU", l("&7Tárgy oldala"),
			"es_ES", l("&7Bando del objeto")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_COMMANDS = n("GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_COMMANDS",
			"en_US", l("&7Commands performed on trade"),
			"fr_FR", l("&7Commandes executées lors de l'échange"),
			"zh_CN", l("&7交易时执行的指令"),
			"zh_TW", l("&7交易時執行的指令"),
			"ru_RU", l("&7Команды, используемые при обмене"),
			"hu_HU", l("&7A kereskedelemben végrehajtott parancsok"),
			"es_ES", l("&7Comandos ejecutados en el comercio")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_PREVIEWITEM = n("GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_PREVIEWITEM",
			"en_US", l("&7Item displayed in trade preview"),
			"fr_FR", l("&7Item affiché dans la prévisualisation de l'échange"),
			"zh_CN", l("&7交易预览时显示的物品"),
			"zh_TW", l("&7預覽交易時顯示的物品"),
			"ru_RU", l("&7Предмет, отображаемый в качестве превью обмена"),
			"hu_HU", l("&7A kereskedelem előnézetében megjelenő elem"),
			"es_ES", l("&7Objeto mostrado en la previsualización del comercio")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_COMMANDSEXECUTIONSIDE = n("GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_COMMANDSEXECUTIONSIDE",
			"en_US", l("&7Execute commands as player or as console"),
			"fr_FR", l("&7Exécuter les commandes en tant que joueur ou console"),
			"zh_CN", l("&7以玩家或控制台身份执行指令"),
			"zh_TW", l("&7以玩家或後台身分執行指令"),
			"ru_RU", l("&7Использовать команды со стороны сервера или игрока"),
			"hu_HU", l("&7Végrehajtja a parancsot, mint játékos, vagy mint konzol"),
			"es_ES", l("&7Ejecutar comandos como jugador o como consola")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_ITEM = n("GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_ITEM",
			"en_US", l("&7Item given/taken on trade"),
			"fr_FR", l("&7Item donné/retiré à l'échange"),
			"zh_CN", l("&7交易的物品"),
			"zh_TW", l("&7在交易時 給予/拿取 的物品"),
			"ru_RU", l("&7Отданные\\полученные предметы в ходе этого обмена"),
			"hu_HU", l("&7Adott-kapott item a kereskedésben"),
			"es_ES", l("&7Objeto dado/retirado en el comercio")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_SHOPID = n("GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_SHOPID",
			"en_US", l("&7Id of shop to trade"),
			"fr_FR", l("&7Id du shop à échanger"),
			"zh_CN", l("&7进行交易的商店ID"),
			"zh_TW", l("&7進行交易的商店ID"),
			"ru_RU", l("&7ID магазина для обмена"),
			"hu_HU", l("&7Bolt azonosítója a kereskedéshez"),
			"es_ES", l("&7ID de la tienda con el que comerciar")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_MERCHANTID = n("GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_MERCHANTID",
			"en_US", l("&7Id of merchant to trade"),
			"fr_FR", l("&7Id du marchand à échanger"),
			"zh_CN", l("&7进行交易的商人ID"),
			"zh_TW", l("&7進行交易的商人ID"),
			"ru_RU", l("&7ID торговца для обмена"),
			"hu_HU", l("&7Kereskedő azonosítója a kereskedéshez"),
			"es_ES", l("&7ID del mercader con el que comerciar")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_MONEYAMOUNT = n("GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_MONEYAMOUNT",
			"en_US", l("&7Amount of money (useless if items value is enabled)"),
			"fr_FR", l("&7Montant d'argent (inutile si la valeur des items est activée)"),
			"zh_CN", l("&7钱数(如果启用了物品价值则无效)"),
			"zh_TW", l("&7金錢數量(如果啟用了 values.yml 的功能那麼這個設置&c無效&f)"),
			"ru_RU", l("&7Количество средств (бесполезно, если есть эквивалент в предметах)"),
			"hu_HU", l("&7Pénz mennyisége (haszontalan, ha az itemek értéke engedélyezve van)"),
			"es_ES", l("&7Cantidad de dinero (inútil si el valor de ítem está activado)")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_XPLEVELAMOUNT = n("GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_XPLEVELAMOUNT",
			"en_US", l("&7Amount of XP levels"),
			"fr_FR", l("&7Nombre de niveaux d'XP"),
			"zh_CN", l("&7经验等级"),
			"zh_TW", l("&7經驗等級"),
			"ru_RU", l("&7Количество уровней"),
			"hu_HU", l("&7XP szint mennyisége"),
			"es_ES", l("&7Cantidad de niveles de XP")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_PLAYERPOINTSAMOUNT = n("GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_PLAYERPOINTSAMOUNT",
			"en_US", l("&7Amount of PlayerPoints points"),
			"fr_FR", l("&7Nombre de points PlayerPoints"),
			"zh_CN", l("&7PlayerPoints点券数"),
			"zh_TW", l("&7PlayerPoints 點數數量"),
			"ru_RU", l("&7Количество очков PlayerPoints"),
			"hu_HU", l("&7PlayerPoints pontok mennyisége"),
			"es_ES", l("&7Cantidad de PlayerPoints puntos")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_TOKENENCHANTTOKENSAMOUNT = n("GUI_SUPREMESHOPS_EDITOR_TRADEOBJECT_TOKENENCHANTTOKENSAMOUNT",
			"en_US", l("&7Amount of TokenEnchant tokens"),
			"fr_FR", l("&7Nombre de tokens TokenEnchant"),
			"zh_TW", l("&7TokenEnchant 代幣數量"),
			"ru_RU", l("&7Количество TokenEnchant токенов"),
			"hu_HU", l("&7TokenEnchant tokenek mennyisége"),
			"es_ES", l("&7Cantidad de TokenEnchant tokens")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GUI_NAMELORE = n("GUI_SUPREMESHOPS_EDITOR_GUI_NAMELORE",
			"en_US", l("&7The name of the GUI"),
			"fr_FR", l("&7Le nom du GUI"),
			"ru_RU", l("&7Размер меню"),
			"hu_HU", l("&7A GUI neve"),
			"es_ES", l("&7Nombre de la GUI")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GUI_SIZELORE = n("GUI_SUPREMESHOPS_EDITOR_GUI_SIZELORE",
			"en_US", l("&7The size of the GUI"),
			"fr_FR", l("&7La taille du GUI"),
			"zh_TW", l("&7GUI大小"),
			"hu_HU", l("&7A GUI mérete"),
			"es_ES", l("&7Tamaño de la GUI")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GUI_CONTENTLORE = n("GUI_SUPREMESHOPS_EDITOR_GUI_CONTENTLORE",
			"en_US", l("&7The content of the GUI"),
			"fr_FR", l("&7Le contenu du GUI"),
			"zh_CN", l("&7GUI内容"),
			"zh_TW", l("&7GUI內容"),
			"ru_RU", l("&7Содержание меню"),
			"hu_HU", l("&7A GUI tartalma"),
			"es_ES", l("&7Contenido de la GUI")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GUI_CONTENT_TYPELORE = n("GUI_SUPREMESHOPS_EDITOR_GUI_CONTENT_TYPELORE",
			"en_US", l("&7The type of content"),
			"fr_FR", l("&7Le type de contenu"),
			"zh_CN", l("&7内容类型"),
			"zh_TW", l("&7內容類型"),
			"ru_RU", l("&7Тип содержания"),
			"hu_HU", l("&7Tartalom típusa"),
			"es_ES", l("&7El tipo de contenido")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GUI_CONTENT_CLICKACTIONSLORE = n("GUI_SUPREMESHOPS_EDITOR_GUI_CONTENT_CLICKACTIONSLORE",
			"en_US", l("&7Action to perform on different click types"),
			"fr_FR", l("&7Action à exécuter lors de différents types de clics"),
			"zh_CN", l("&7不同点击类型执行的行为"),
			"zh_TW", l("&7針對不同點擊類型執行的操作"),
			"ru_RU", l("&7Действия, зависящие от типа нажатий"),
			"hu_HU", l("&7Különböző kattintástípusokon végrehajtható művelet"),
			"es_ES", l("&7Acción a ejecutar con distintos tipos de clic")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GUI_CONTENT_ITEMLORE = n("GUI_SUPREMESHOPS_EDITOR_GUI_CONTENT_ITEMLORE",
			"en_US", l("&7The item"),
			"fr_FR", l("&7L'item"),
			"zh_CN", l("&7物品"),
			"zh_TW", l("&7物品"),
			"ru_RU", l("&7Предмет"),
			"hu_HU", l("&7Az item"),
			"es_ES", l("&7El objeto")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GUI_CONTENT_COMMANDSLORE = n("GUI_SUPREMESHOPS_EDITOR_GUI_CONTENT_COMMANDSLORE",
			"en_US", l("&7The commands performed on click"),
			"fr_FR", l("&7Les commandes exécutées au click"),
			"zh_CN", l("&7点击执行的指令"),
			"zh_TW", l("&7點擊時執行的指令"),
			"ru_RU", l("&7Команды, применяемые при нажатии"),
			"hu_HU", l("&7Kattintással végrehajtott parancsok"),
			"es_ES", l("&7Los comandos ejecutados durante el clic")
			);

	public static final Text GUI_SUPREMESHOPS_EDITOR_GUI_ADMINSHOPSLORE = n("GUI_SUPREMESHOPS_EDITOR_GUI_ADMINSHOPSLORE",
			"en_US", l("&7Admin shops added in this GUI"),
			"fr_FR", l("&7Shops admins ajoutés dans ce GUI"),
			"zh_CN", l("&7添加到这个GUI内的管理员商店"),
			"zh_TW", l("&7在這個GUI內添加管理員商店"),
			"ru_RU", l("&7Админ-шопы, добавленные в это меню"),
			"hu_HU", l("&7Admin boltok hozzáadva ebbe a GUI-ban"),
			"es_ES", l("&7Tiendas de admin añadidas en este GUI")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORGUIS = n("GUI_SUPREMESHOPS_EDITORGUIS",
			"en_US", l("&6GUI menus"),
			"fr_FR", l("&6Menus GUI"),
			"zh_CN", l("&6GUI菜单"),
			"zh_TW", l("&7GUI 選單"),
			"ru_RU", l("&6Список меню"),
			"hu_HU", l("&6GUI menük"),
			"es_ES", l("&7Menús de GUI")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORGUISLORE = n("GUI_SUPREMESHOPS_EDITORGUISLORE",
			"en_US", l("&7Customize GUI menus"),
			"fr_FR", l("&7Personnalisez les menus GUI"),
			"zh_CN", l("&7Customize GUI menus"),
			"zh_TW", l("&7自訂義 GUI 選單"),
			"ru_RU", l("&7Кастомизировать меню"),
			"hu_HU", l("&7Szabd testre a GUI menüket"),
			"es_ES", l("&7Customizar menús de GUI")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORADMINSHOPS = n("GUI_SUPREMESHOPS_EDITORADMINSHOPS",
			"en_US", l("&6Admin shops"),
			"es_ES", l("&7Tiendas de administrador"),
			"zh_CN", l("&6管理员商店"),
			"zh_TW", l("&6管理員商店"),
			"ru_RU", l("&6Админ-шопы"),
			"hu_HU", l("&6Admin boltok")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORADMINSHOPSLORE = n("GUI_SUPREMESHOPS_EDITORADMINSHOPSLORE",
			"en_US", l("&7Create and edit the admin shops"),
			"fr_FR", l("&7Créez et éditez les shops admin"),
			"zh_CN", l("&7创建并编辑管理员商店"),
			"zh_TW", l("&7創建並編輯管理員商店"),
			"ru_RU", l("&7Создание и редактирование админ-шопов"),
			"hu_HU", l("&7Hozd létre és szerkeszd meg az admin boltot"),
			"es_ES", l("&7Crea y edita las tiendas de administrador")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORADMINCREATEITEMSVSMONEY = n("GUI_SUPREMESHOPS_EDITORADMINCREATEITEMSVSMONEY",
			"en_US", l("&6Create 'items vs money' shop"),
			"es_ES", l("&7Crea una tienda de 'ítem vs. dinero'"),
			"zh_CN", l("&6Create 'items vs money' shop"),
			"zh_TW", l("&6創建 '物品 vs 金錢' 商店"),
			"ru_RU", l("&6Создание магазина по типу 'предмет НА деньги'"),
			"hu_HU", l("&6Készítsd el az -item vs pénz- boltot")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORTRIGGERS = n("GUI_SUPREMESHOPS_EDITORTRIGGERS",
			"en_US", l("&6Triggers"),
			"es_ES", l("&6Triggers (o desencadenantes)"),
			"zh_CN", l("&6管理员商店触发器"),
			"zh_TW", l("&6觸發器"),
			"ru_RU", l("&6Триггеры"),
			"hu_HU", l("&6Eseményindítók")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORTRIGGERSLORE = n("GUI_SUPREMESHOPS_EDITORTRIGGERSLORE",
			"en_US", l("&7Create and edit the triggers"),
			"fr_FR", l("&7Créez et éditez les triggers"),
			"zh_CN", l("&7创建并编辑管理员商店编辑器"),
			"zh_TW", l("&7創建並編輯觸發器"),
			"ru_RU", l("&7Создание и редактирование триггеров"),
			"hu_HU", l("&7Készítsd el, és szerkeszd meg az eseményindítókat."),
			"es_ES", l("&7Crea y edita los triggers")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORSHOPEXISTENCECONDITIONS = n("GUI_SUPREMESHOPS_EDITORSHOPEXISTENCECONDITIONS",
			"en_US", l("&6Shop existence conditions"),
			"es_ES", l("&7Condiciones de la existencia de la tienda"),
			"zh_CN", l("&6商店存在条件"),
			"zh_TW", l("&6商店存在條件"),
			"ru_RU", l("&6Условия существования магазина"),
			"hu_HU", l("&6A bolt létezésének feltételei")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORSHOPEXISTENCECONDITIONSLORE = n("GUI_SUPREMESHOPS_EDITORSHOPEXISTENCECONDITIONSLORE",
			"en_US", l("&7Create and edit the shop existence conditions"),
			"fr_FR", l("&7Créez et éditez les conditions d'existence de shop"),
			"zh_CN", l("&7Create and edit the shop existence conditions"),
			"zh_TW", l("&7創建和編輯商店存在條件"),
			"ru_RU", l("&7Создание и редактирование условий существования магазина"),
			"hu_HU", l("&7Készítsd el, és szerkeszd meg a bolt létezésének feltételeit."),
			"es_ES", l("&7Crea y edita las condiciones de existencia de la tienda")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORSHOPDESTRUCTIONCONDITIONS = n("GUI_SUPREMESHOPS_EDITORSHOPDESTRUCTIONCONDITIONS",
			"en_US", l("&6Shop destruction conditions"),
			"es_ES", l("&6Condiciones de la destrucción de la tienda"),
			"zh_CN", l("&6Shop destruction conditions"),
			"zh_TW", l("&商店摧毀條件"),
			"hu_HU", l("&6A bolt megsemmisítésének feltételei")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORSHOPDESTRUCTIONCONDITIONSLORE = n("GUI_SUPREMESHOPS_EDITORSHOPDESTRUCTIONCONDITIONSLORE",
			"en_US", l("&7Create and edit the shop destruction conditions"),
			"fr_FR", l("&7Créez et éditez les conditions de destruction de shop"),
			"zh_CN", l("&7Create and edit the shop destruction conditions"),
			"zh_TW", l("&7創建和編輯商店摧毀條件"),
			"ru_RU", l("&7Создание и редактирование условий уничтожения магазина"),
			"hu_HU", l("&7Hozzd létre, és szerkeszd meg a bolt megsemmisítésének feltételeit."),
			"es_ES", l("&7Crea y edita las condiciones de la destrucción de la tienda")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORMERCHANTDESTRUCTIONCONDITIONS = n("GUI_SUPREMESHOPS_EDITORMERCHANTDESTRUCTIONCONDITIONS",
			"en_US", l("&6Merchant destruction conditions"),
			"es_ES", l("&7Condiciones de destrucción del mercader"),
			"hu_HU", l("&6Kereskedők megsemmisítésének feltételei")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORMERCHANTDESTRUCTIONCONDITIONSLORE = n("GUI_SUPREMESHOPS_EDITORMERCHANTDESTRUCTIONCONDITIONSLORE",
			"en_US", l("&7Create and edit the shop destruction conditions"),
			"fr_FR", l("&7Créez et éditez les conditions de destruction de shop"),
			"zh_CN", l("&7Create and edit the shop destruction conditions"),
			"zh_TW", l("&7創建和編輯商店摧毀條件"),
			"ru_RU", l("&7Создание и редактирование условий уничтожения магазина"),
			"hu_HU", l("&7Hozd létre, és készítsd el a kereskedők megsemmisítésének feltételeit."),
			"es_ES", l("&7Crea y edita las condiciones de destrucción de la tienda")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORMERCHANTEXISTENCECONDITIONS = n("GUI_SUPREMESHOPS_EDITORMERCHANTEXISTENCECONDITIONS",
			"en_US", l("&6Merchant existence conditions"),
			"es_ES", l("&7Condiciones de existencia del mercader"),
			"zh_CN", l("&6Merchant existence conditions"),
			"zh_TW", l("&6商人存在條件"),
			"ru_RU", l("&6Условия существования торговца"),
			"hu_HU", l("&6Kereskedők létezésének feltételei.")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORMERCHANTEXISTENCECONDITIONSLORE = n("GUI_SUPREMESHOPS_EDITORMERCHANTEXISTENCECONDITIONSLORE",
			"en_US", l("&7Create and edit the merchant existence conditions"),
			"fr_FR", l("&7Créez et éditez les conditions d'existence de marchand"),
			"zh_CN", l("&7Create and edit the merchant existence conditions"),
			"zh_TW", l("&7創建和編輯商人存在條件"),
			"ru_RU", l("&7Создание и редактирование условий существования торговца"),
			"hu_HU", l("&7Készítsd el és szerkeszd a kereskedők létezési feltételeit"),
			"es_ES", l("&7Crea y edita las condiciones de existencia del mercader")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORTRADEMODIFIER = n("GUI_SUPREMESHOPS_EDITORTRADEMODIFIER",
			"en_US", l("&6Trade modifiers"),
			"es_ES", l("&6Modificadores de comercio"),
			"zh_CN", l("&6交易编辑器"),
			"zh_TW", l("&6交易額外功能"),
			"ru_RU", l("&6Модификаторы обмена"),
			"hu_HU", l("&6Kereskedési módosítók")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORTRADEMODIFIERLORE = n("GUI_SUPREMESHOPS_EDITORTRADEMODIFIERLORE",
			"en_US", l("&7Create and edit the trade modifiers"),
			"fr_FR", l("&7Créez et éditez les modificateurs d'échange"),
			"zh_CN", l("&7创建并编辑交易编辑器"),
			"zh_TW", l("&7創建並編輯交易額外功能"),
			"ru_RU", l("&7Создание и редактирование модификаторов обмена"),
			"hu_HU", l("&7Készítsd el és szerkeszd meg a kereskedési módosítókat"),
			"es_ES", l("&7Crea y edita los modificadores de comercio")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORTRADEMODIFIERTAXES = n("GUI_SUPREMESHOPS_EDITORTRADEMODIFIERTAXES",
			"en_US", l("&6Trade modifiers (taxes)"),
			"es_ES", l("&6Modificadores de comercio (impuestos)"),
			"zh_CN", l("&6交易编辑器 (交易税)"),
			"zh_TW", l("&6交易額外功能 (稅率)"),
			"hu_HU", l("&6Kereskedelem módosítók (adók)")
			);

	public static final Text GUI_SUPREMESHOPS_EDITORTRADEMODIFIERTAXESLORE = n("GUI_SUPREMESHOPS_EDITORTRADEMODIFIERTAXESLORE",
			"en_US", l("&7Create and edit the trade modifiers (taxes)"),
			"fr_FR", l("&7Créez et éditez les modificateurs d'échange (taxes)"),
			"zh_CN", l("&7创建并编辑交易编辑器 (交易税)"),
			"zh_TW", l("&7創建並編輯交易額外功能 (稅率)"),
			"ru_RU", l("&7Создание и редактирование модификаторов обмена (комиссия)"),
			"hu_HU", l("&7Hozz létre, és szerkeszd a kereskedelem módosítóit.(adók)"),
			"es_ES", l("&7Crea y edita los modificadores de comercio (impuestos)")
			);

}
