package com.guillaumevdn.supremeshops;

import com.guillaumevdn.gcorelegacy.lib.Perm;

public class SSPerm {

	// root/admin
	public static final Perm SUPREMESHOPS_ROOT = new Perm(null, "supremeshops.*");
	public static final Perm SUPREMESHOPS_ADMIN = new Perm(SUPREMESHOPS_ROOT, "supremeshops.admin");

	// shop
	public static final Perm SUPREMESHOPS_EDIT_ADMIN = new Perm(SUPREMESHOPS_ROOT, "supremeshops.edit.admin");
	public static final Perm SUPREMESHOPS_SHOP_CREATE = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.create.*");
	public static final Perm SUPREMESHOPS_SHOP_CREATE_BLOCK = new Perm(SUPREMESHOPS_SHOP_CREATE, "supremeshops.shop.create.block");
	public static final Perm SUPREMESHOPS_SHOP_CREATE_SIGN = new Perm(SUPREMESHOPS_SHOP_CREATE, "supremeshops.shop.create.sign");
	public static final Perm SUPREMESHOPS_SHOP_CREATE_GUI = new Perm(SUPREMESHOPS_SHOP_CREATE, "supremeshops.shop.create.gui");
	public static final Perm SUPREMESHOPS_SHOP_CREATE_MERCHANT = new Perm(SUPREMESHOPS_SHOP_CREATE, "supremeshops.shop.create.merchant");

	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_SET_OPEN = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.set_open");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_SET_ADMIN_STOCK = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.set_admin_stock");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_SET_REMOTE = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.set_remote");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_SET_DISPLAY_ITEMS = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.set_display_items");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_SET_DISPLAY_NAME = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.set_display_name");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_SET_PARTICLE_PATTERN = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.set_particle_pattern");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_EDIT_MANAGERS = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.edit_managers");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_EDIT_TRADES_LIMIT = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.edit_trades_limit");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_EDIT_TRADE_CONDITIONS = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.edit_trade_conditions");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_DESTROY = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.destroy");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_STOP_RENTING = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.stop_renting");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_ADD_STOCK = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.add_stock");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_ADD_OBJECT = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.add_object");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_REMOVE_STOCK = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.remove_stock");
	public static final Perm SUPREMESHOPS_SHOP_MANAGEMENTPERMISSION_REMOVE_OBJECT = new Perm(SUPREMESHOPS_ROOT, "supremeshops.shop.management.remove_object");

	// merchants
	public static final Perm SUPREMESHOPS_MERCHANT_CREATE = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.create.*");
	public static final Perm SUPREMESHOPS_MERCHANT_CREATE_NPC = new Perm(SUPREMESHOPS_MERCHANT_CREATE, "supremeshops.merchant.create.npc");
	public static final Perm SUPREMESHOPS_MERCHANT_CREATE_BLOCK = new Perm(SUPREMESHOPS_MERCHANT_CREATE, "supremeshops.merchant.create.block");
	public static final Perm SUPREMESHOPS_MERCHANT_CREATE_SIGN = new Perm(SUPREMESHOPS_MERCHANT_CREATE, "supremeshops.merchant.create.sign");

	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_EDIT_SHOPS = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.edit_shops");
	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_EDIT_TRADES_LIMIT = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.edit_trades_limit");
	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_EDIT_INTERACT_CONDITIONS = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.edit_interact_conditions");
	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_EDIT_MANAGERS = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.edit_managers");
	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_SET_DISPLAY_NAME = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.set_display_name");
	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_SET_PARTICLE_PATTERN = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.set_particle_pattern");
	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_SET_SKIN = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.set_skin");
	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_SET_EQUIPMENT = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.set_equipment");
	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_SET_STATUS = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.set_status");
	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_SET_OPEN = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.set_open");
	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_SET_REMOTE = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.set_remote");
	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_STOP_RENTING = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.stop_renting");
	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_DESTROY = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.destroy");

	public static final Perm SUPREMESHOPS_MERCHANT_MANAGEMENTPERMISSION_ALLSTATUS = new Perm(SUPREMESHOPS_ROOT, "supremeshops.merchant.management.status.*");

	// create objects
	public static final Perm SUPREMESHOPS_CREATE_OBJECT = new Perm(SUPREMESHOPS_ROOT, "supremeshops.create.object.*");
	public static final Perm SUPREMESHOPS_CREATE_OBJECT_COMMANDS = new Perm(SUPREMESHOPS_CREATE_OBJECT, "supremeshops.create.object.commands");
	public static final Perm SUPREMESHOPS_CREATE_OBJECT_ITEM= new Perm(SUPREMESHOPS_CREATE_OBJECT, "supremeshops.create.object.item");
	public static final Perm SUPREMESHOPS_CREATE_OBJECT_MERCHANT = new Perm(SUPREMESHOPS_CREATE_OBJECT, "supremeshops.create.object.merchant");
	public static final Perm SUPREMESHOPS_CREATE_OBJECT_PLAYERPOINTS_POINTS = new Perm(SUPREMESHOPS_CREATE_OBJECT, "supremeshops.create.object.playerpoints_points");
	public static final Perm SUPREMESHOPS_CREATE_OBJECT_SHOP = new Perm(SUPREMESHOPS_CREATE_OBJECT, "supremeshops.create.object.shop");
	public static final Perm SUPREMESHOPS_CREATE_OBJECT_TOKENENCHANT_TOKENS = new Perm(SUPREMESHOPS_CREATE_OBJECT, "supremeshops.create.object.tokenenchant_tokens");
	public static final Perm SUPREMESHOPS_CREATE_OBJECT_VAULT_MONEY = new Perm(SUPREMESHOPS_CREATE_OBJECT, "supremeshops.create.object.vault_money");
	public static final Perm SUPREMESHOPS_CREATE_OBJECT_XP_LEVEL = new Perm(SUPREMESHOPS_CREATE_OBJECT, "supremeshops.create.object.xp_level");

	// create conditions
	public static final Perm SUPREMESHOPS_CREATE_CONDITION = new Perm(SUPREMESHOPS_ROOT, "supremeshops.create.condition.*");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_CONDITION_COUNT = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.condition_count");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_DAY_OF_WEEK = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.day_of_week");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_DAY_OF_MONTH = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.day_of_month");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_GAME_TIME = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.game_time");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_IN_WORLD = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.in_world");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_IN_WORLDGUARD_REGION = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.in_worldguard_region");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_IS_ADMIN = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.is_admin");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_PERMISSION = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.permission");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_PLACEHOLDERAPIVARIABLE = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.placeholderapi_variable");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_PLAYER_HEALTH = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.player_health");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_PLAYER_HUNGER = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.player_hunger");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_PLAYER_MONEY = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.player_money");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_PLAYERPOINTS_POINTS = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.playerpoints_points");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_PLAYER_ITEMS= new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.player_items");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_PLAYER_XP_LEVEL = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.player_xp_level");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_SHOP_COUNT = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.shop_count");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_SHOP_BLOCK_TYPE = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.shop_block_type");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_SHOP_OBJECT_COUNT = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.shop_object_cound");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_SHOP_TYPE = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.shop_type");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_TIME_OF_DAY = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.time_of_day");
	public static final Perm SUPREMESHOPS_CREATE_CONDITION_TOKENENCHANT_TOKENS = new Perm(SUPREMESHOPS_CREATE_CONDITION, "supremeshops.create.condition.tokenenchant_tokens");

	// commands
	public static final Perm SUPREMESHOPS_COMMAND = new Perm(SUPREMESHOPS_ROOT, "supremeshops.command.*");

	public static final Perm SUPREMESHOPS_COMMAND_MENU = new Perm(SUPREMESHOPS_COMMAND, "supremeshops.command.menu");
	public static final Perm SUPREMESHOPS_COMMAND_MENU_OTHERS = new Perm(SUPREMESHOPS_COMMAND_MENU, "supremeshops.command.menu.others");
	public static final Perm SUPREMESHOPS_COMMAND_MENU_SPECIFIC = new Perm(SUPREMESHOPS_COMMAND_MENU, "supremeshops.command.menu.specific");

	public static final Perm SUPREMESHOPS_COMMAND_SHOP = new Perm(SUPREMESHOPS_COMMAND, "supremeshops.command.shop.*");
	public static final Perm SUPREMESHOPS_COMMAND_SHOP_MENU = new Perm(SUPREMESHOPS_COMMAND_SHOP, "supremeshops.command.shop.menu");
	public static final Perm SUPREMESHOPS_COMMAND_SHOP_ADMINEDIT = new Perm(SUPREMESHOPS_COMMAND_SHOP, "supremeshops.command.shop.adminedit");
	public static final Perm SUPREMESHOPS_COMMAND_SHOP_LIST = new Perm(SUPREMESHOPS_COMMAND_SHOP, "supremeshops.command.shop.list");
	public static final Perm SUPREMESHOPS_COMMAND_SHOP_PURGE = new Perm(SUPREMESHOPS_COMMAND_SHOP, "supremeshops.command.shop.purge");

	public static final Perm SUPREMESHOPS_COMMAND_MERCHANT = new Perm(SUPREMESHOPS_COMMAND, "supremeshops.command.merchant.*");
	public static final Perm SUPREMESHOPS_COMMAND_MERCHANT_NPC = new Perm(SUPREMESHOPS_COMMAND_MERCHANT, "supremeshops.command.merchant.npc.*");
	public static final Perm SUPREMESHOPS_COMMAND_MERCHANT_NPC_CREATE = new Perm(SUPREMESHOPS_COMMAND_MERCHANT, "supremeshops.command.merchant.npc.create");
	public static final Perm SUPREMESHOPS_COMMAND_MERCHANT_LIST = new Perm(SUPREMESHOPS_COMMAND_MERCHANT, "supremeshops.command.merchant.list");
	public static final Perm SUPREMESHOPS_COMMAND_MERCHANT_PURGE = new Perm(SUPREMESHOPS_COMMAND_MERCHANT, "supremeshops.command.merchant.purge");
	public static final Perm SUPREMESHOPS_COMMAND_MERCHANT_ADMINEDIT = new Perm(SUPREMESHOPS_COMMAND_SHOP, "supremeshops.command.merchant.adminedit");

	public static final Perm SUPREMESHOPS_COMMAND_ADMINGEN = new Perm(SUPREMESHOPS_COMMAND, "supremeshops.command.admingen");
	public static final Perm SUPREMESHOPS_COMMAND_EDIT = new Perm(SUPREMESHOPS_COMMAND, "supremeshops.command.edit");
	public static final Perm SUPREMESHOPS_COMMAND_TRADE = new Perm(SUPREMESHOPS_COMMAND, "supremeshops.command.trade");
	public static final Perm SUPREMESHOPS_COMMAND_ITEM = new Perm(SUPREMESHOPS_COMMAND, "supremeshops.command.item.*");
	public static final Perm SUPREMESHOPS_COMMAND_ITEM_BLACKLIST = new Perm(SUPREMESHOPS_COMMAND_ITEM, "supremeshops.command.item.blacklist");
	public static final Perm SUPREMESHOPS_COMMAND_ITEM_WHITELIST = new Perm(SUPREMESHOPS_COMMAND_ITEM, "supremeshops.command.item.whitelist");
	public static final Perm SUPREMESHOPS_COMMAND_ITEM_RESETVALUE = new Perm(SUPREMESHOPS_COMMAND_ITEM, "supremeshops.command.item.resetvalue");

}
