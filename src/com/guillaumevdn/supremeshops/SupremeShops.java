package com.guillaumevdn.supremeshops;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.lib.GPlugin;
import com.guillaumevdn.gcorelegacy.lib.command.CommandArgument;
import com.guillaumevdn.gcorelegacy.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcorelegacy.lib.data.DataManager.BackEnd;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.Gson;
import com.guillaumevdn.gcorelegacy.libs.me.cybermaxke.enderbox.api.resource.ResourceExtractor;
import com.guillaumevdn.supremeshops.command.ArgAdmingen;
import com.guillaumevdn.supremeshops.command.ArgEdit;
import com.guillaumevdn.supremeshops.command.ArgItemBlacklist;
import com.guillaumevdn.supremeshops.command.ArgItemResetvalue;
import com.guillaumevdn.supremeshops.command.ArgItemWhitelist;
import com.guillaumevdn.supremeshops.command.ArgMenu;
import com.guillaumevdn.supremeshops.command.convert.ArgConvertQuantumshop;
import com.guillaumevdn.supremeshops.command.convert.ArgConvertQuickshop;
import com.guillaumevdn.supremeshops.command.convert.ArgConvertShopchest;
import com.guillaumevdn.supremeshops.command.convert.ArgFixMMOItems;
import com.guillaumevdn.supremeshops.command.merchant.ArgMerchantAdminEdit;
import com.guillaumevdn.supremeshops.command.merchant.ArgMerchantList;
import com.guillaumevdn.supremeshops.command.merchant.ArgMerchantNpcCreate;
import com.guillaumevdn.supremeshops.command.merchant.ArgMerchantPurge;
import com.guillaumevdn.supremeshops.command.merchant.CommandRootMerchant;
import com.guillaumevdn.supremeshops.command.playertrade.CommandRootPlayertrade;
import com.guillaumevdn.supremeshops.command.shop.ArgShopAdminEdit;
import com.guillaumevdn.supremeshops.command.shop.ArgShopList;
import com.guillaumevdn.supremeshops.command.shop.ArgShopPurge;
import com.guillaumevdn.supremeshops.command.shop.CommandRootShop;
import com.guillaumevdn.supremeshops.data.SSDataManager;
import com.guillaumevdn.supremeshops.integration.areashop.IntegrationAreaShop;
import com.guillaumevdn.supremeshops.integration.citizens.IntegrationCitizens;
import com.guillaumevdn.supremeshops.integration.mythicmobs.IntegrationMythicMobs;
import com.guillaumevdn.supremeshops.integration.wildstacker.IntegrationWildStacker;
import com.guillaumevdn.supremeshops.listeners.CreationListeners;
import com.guillaumevdn.supremeshops.listeners.DestructionListeners;
import com.guillaumevdn.supremeshops.listeners.ItemDiscoverListeners;
import com.guillaumevdn.supremeshops.listeners.JoinListeners;
import com.guillaumevdn.supremeshops.listeners.MerchantsListeners;
import com.guillaumevdn.supremeshops.listeners.ShopsListeners;
import com.guillaumevdn.supremeshops.listeners.TriggersListeners;
import com.guillaumevdn.supremeshops.module.ModuleManager;
import com.guillaumevdn.supremeshops.module.itemvalue.PerMinuteModifiersApplicationTask;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.task.InactiveMerchantDestroyerTask;
import com.guillaumevdn.supremeshops.task.InactiveShopDestroyerTask;
import com.guillaumevdn.supremeshops.task.InvalidMerchantDestroyerTask;
import com.guillaumevdn.supremeshops.task.InvalidShopDestroyerTask;
import com.guillaumevdn.supremeshops.task.ManagersLateWagesPayTask;
import com.guillaumevdn.supremeshops.task.ManagersWagesPayTask;
import com.guillaumevdn.supremeshops.task.RentsUpdateTask;
import com.guillaumevdn.supremeshops.util.AdapterCPConditions;
import com.guillaumevdn.supremeshops.util.AdapterTradeObject;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;

/* TODO : IDEAS

- Refactor "rentable" and "manageable" interfaces for better GUIs and "conditions/object logic"

- Create admin shops from the editor more easily (inventory opens, you place things (controls to create them are in your own inventory), and you can edit the price etc, the give/take items etc)

- Link directly admin shops triggers to blocks/entities/npcs by clicking "link shop to trigger" and right-click the trigger in-game directly

- In player trades, the other player can click this item and say "i want more of this" or "please remove this"

- Factions or Towny implementation

- Interesting stats for sellers (for example, see buy percentage that is above 10 trades at a time, above 100, etc, things like that so they can plan their stock or whatever)

- System to promote shops in GUI

- Open admin shops with their own command

- QC integration

- Bungeecoord stuff ? discord : Tiippex

- Simplify GUI creation

- Custom GUI icon for merchants (when it's displayed in lists etc)

- Add a "player stock" placeholder in objects desc in trade preview GUI ("you currently have : dAZE?dzeùldk")

- Add a "chat input" option when it comes to selecting an amount (trade preview, or restock)

 */

public class SupremeShops extends GPlugin {

	// ----------------------------------------------------------------------
	// Instance
	// ----------------------------------------------------------------------

	private static SupremeShops instance;

	/**
	 * @return the plugin instance
	 */
	public static SupremeShops inst() {
		return instance;
	}

	public SupremeShops() {
		instance = this;
	}

	// ----------------------------------------------------------------------
	// Fields
	// ----------------------------------------------------------------------

	// static fields
	public static Gson GSON = null;
	public static Gson UNPRETTY_GSON = null;

	// fields
	private ModuleManager moduleManager = new ModuleManager();
	private GeneralManager generalManager = new GeneralManager();
	private List<BukkitTask> tasks = new ArrayList<BukkitTask>();
	private boolean forceItemDiscover = false;

	public ModuleManager getModuleManager() {
		return moduleManager;
	}

	public GeneralManager getGeneralManager() {
		return generalManager;
	}

	public List<BukkitTask> getTasks() {
		return tasks;
	}

	public boolean getForceItemDiscover() {
		return forceItemDiscover;
	}

	// integration
	private IntegrationAreaShop integrationAreaShop = null;
	private IntegrationCitizens integrationCitizens = null;
	private IntegrationMythicMobs integrationMythicMobs = null;
	private IntegrationWildStacker integrationWildStacker = null;

	public IntegrationAreaShop getIntegrationAreaShop() {
		return integrationAreaShop;
	}

	public void setIntegrationAreaShop(IntegrationAreaShop integrationAreaShop) {
		this.integrationAreaShop = integrationAreaShop;
	}

	public IntegrationCitizens getIntegrationCitizens() {
		return integrationCitizens;
	}

	public void setIntegrationCitizens(IntegrationCitizens integrationCitizens) {
		this.integrationCitizens = integrationCitizens;
	}

	public IntegrationMythicMobs getIntegrationMythicMobs() {
		return integrationMythicMobs;
	}

	public void setIntegrationMythicMobs(IntegrationMythicMobs integrationMythicMobs) {
		this.integrationMythicMobs = integrationMythicMobs;
	}

	public IntegrationWildStacker getIntegrationWildStacker() {
		return integrationWildStacker;
	}

	public void setIntegrationWildStacker(IntegrationWildStacker integrationWildStacker) {
		this.integrationWildStacker = integrationWildStacker;
	}

	public void pushBoardIfNeeded(boolean async) {
		if (dataManager != null && dataManager.getBoard() != null) {
			dataManager.getBoard().pushIfNeeded(async);
		}
	}

	// advanced log
	private boolean pluginLog = true;
	private boolean pluginLogConsole = false;
	private File logFile = null;
	private SimpleDateFormat logFileDateFormat = new SimpleDateFormat("HH':'mm':'ss");
	private List<String> toLog = new ArrayList<String>();

	public void pluginLog(Shop shop, Merchant merchant, String adminShopId, Player player, String optional, String message) {
		// not enabled
		if (!pluginLog) {
			return;
		}
		String line = "";
		try {
			// build line
			if (player != null) line += "{PLAYER " + player.getName() + "} ";
			if (shop != null) {
				line += "{SHOP " + shop.getId() + (shop.getId().equals(shop.getDisplayName()) ? "" : "-" + shop.getDisplayName()) + " " + shop.getType().name() + "} ";
				line += "{" + (shop.getCurrentOwner() == null ? "ADMIN" : shop.getCurrentOwner().toStringName()) + "} ";
			}
			if (merchant != null) {
				line += "{MERCHANT " + merchant.getId() + (merchant.getId().equals(merchant.getDisplayName()) ? "" : "-" + merchant.getDisplayName()) + " " + merchant.getType().name() + "} ";
				line += "{" + (merchant.getCurrentOwner() == null ? "ADMIN" : merchant.getCurrentOwner().toStringName()) + "} ";
			}
			if (adminShopId != null) line += "{ADMIN SHOP " + adminShopId + "} ";
			if (optional != null) line += optional;
			line += message;
			// add line to file
			toLog.add("[" + logFileDateFormat.format(GCoreLegacy.inst().getCalendarInstance().getTime()) + "] " + line);
			// log in console
			if (pluginLogConsole) {
				debug(line);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			error("Couldn't log '" + line + "'");
		}
	}

	public void pluginLog(Rentable rentable, Player player, String optional, String message) {
		// not enabled
		if (!pluginLog) {
			return;
		}
		String line = "";
		try {
			// build line
			if (player != null) line += "{PLAYER " + player.getName() + "} ";
			if (rentable instanceof Shop) {
				Shop shop = (Shop) rentable;
				line += "{SHOP " + shop.getId() + (shop.getId().equals(shop.getDisplayName()) ? "" : "-" + shop.getDisplayName()) + " " + shop.getType().name() + "} ";
				line += "{" + (shop.getCurrentOwner() == null ? "ADMIN" : shop.getCurrentOwner().toStringName()) + "} ";
			}
			if (rentable instanceof Merchant) {
				Merchant merchant = (Merchant) rentable;
				line += "{MERCHANT " + merchant.getId() + (merchant.getId().equals(merchant.getDisplayName()) ? "" : "-" + merchant.getDisplayName()) + " " + merchant.getType().name() + "} ";
				line += "{" + (merchant.getCurrentOwner() == null ? "ADMIN" : merchant.getCurrentOwner().toStringName()) + "} ";
			}
			if (optional != null) line += optional;
			line += message;
			// add line to file
			toLog.add("[" + logFileDateFormat.format(GCoreLegacy.inst().getCalendarInstance().getTime()) + "] " + line);
			// log in console
			if (pluginLogConsole) {
				debug(line);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			error("Couldn't log '" + line + "'");
		}
	}

	public void pluginLog(Manageable manageable, Player player, String optional, String message) {
		// not enabled
		if (!pluginLog) {
			return;
		}
		String line = "";
		try {
			// build line
			if (player != null) line += "{PLAYER " + player.getName() + "} ";
			if (manageable instanceof Shop) {
				Shop shop = (Shop) manageable;
				line += "{SHOP " + shop.getId() + (shop.getId().equals(shop.getDisplayName()) ? "" : "-" + shop.getDisplayName()) + " " + shop.getType().name() + "} ";
				line += "{" + (shop.getCurrentOwner() == null ? "ADMIN" : shop.getCurrentOwner().toStringName()) + "} ";
			}
			if (manageable instanceof Merchant) {
				Merchant merchant = (Merchant) manageable;
				line += "{MERCHANT " + merchant.getId() + (merchant.getId().equals(merchant.getDisplayName()) ? "" : "-" + merchant.getDisplayName()) + " " + merchant.getType().name() + "} ";
				line += "{" + (merchant.getCurrentOwner() == null ? "ADMIN" : merchant.getCurrentOwner().toStringName()) + "} ";
			}
			if (optional != null) line += optional;
			line += message;
			// add line to file
			toLog.add("[" + logFileDateFormat.format(GCoreLegacy.inst().getCalendarInstance().getTime()) + "] " + line);
			// log in console
			if (pluginLogConsole) {
				debug(line);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			error("Couldn't log '" + line + "'");
		}
	}

	public void savePluginLog() {
		if (toLog.isEmpty()) return;
		try {
			// create file
			if (logFile == null) {
				logFile = new File(getDataFolder() + "/plugin_log.log");
			}
			if (!logFile.getParentFile().exists()) {
				logFile.getParentFile().mkdirs();
			}
			if (!logFile.exists()) {
				logFile.createNewFile();
			}
			// write lines
			BufferedWriter writer = new BufferedWriter(new FileWriter(logFile, true));
			for (String line : toLog) {
				writer.write(line + "\n");
			}
			writer.close();
			// clear logs
			toLog.clear();
		} catch (Throwable ignored) {
		}
	}

	private boolean tradesPluginLog = true;
	private boolean tradesPluginLogConsole = false;
	private File tradesLogFile = null;
	private List<String> tradesToLog = new ArrayList<String>();

	public void tradeLog(Shop shop, Merchant merchant, Player player, int count) {
		// not enabled
		if (!tradesPluginLog) {
			return;
		}
		String line = "";
		try {
			// build line
			if (player != null) line += "{PLAYER " + player.getName() + "} ";
			if (shop != null) {
				line += "{SHOP " + shop.getId() + (shop.getId().equals(shop.getDisplayName()) ? "" : "-" + shop.getDisplayName()) + " " + shop.getType().name() + "} ";
				line += "{" + (shop.getCurrentOwner() == null ? "ADMIN" : shop.getCurrentOwner().toStringName()) + "} ";
			}
			if (merchant != null) {
				line += "{MERCHANT " + merchant.getId() + (merchant.getId().equals(merchant.getDisplayName()) ? "" : "-" + merchant.getDisplayName()) + " " + merchant.getType().name() + "} ";
				line += "{" + (merchant.getCurrentOwner() == null ? "ADMIN" : merchant.getCurrentOwner().toStringName()) + "} ";
			}
			line += "Traded" + (count > 1 ? " x" + count : "");
			// add line to file
			toLog.add("[" + logFileDateFormat.format(GCoreLegacy.inst().getCalendarInstance().getTime()) + "] " + line);
			// log in console
			if (tradesPluginLogConsole) {
				debug(line);
			}
		} catch (Exception exception) {
			exception.printStackTrace();
			error("Couldn't log '" + line + "'");
		}
	}

	public void saveTradesPluginLog() {
		if (tradesToLog.isEmpty()) return;
		try {
			// create file
			if (tradesLogFile == null) {
				tradesLogFile = new File(getDataFolder() + "/trades_log.log");
			}
			if (!tradesLogFile.getParentFile().exists()) {
				tradesLogFile.getParentFile().mkdirs();
			}
			if (!tradesLogFile.exists()) {
				tradesLogFile.createNewFile();
			}
			// write lines
			BufferedWriter writer = new BufferedWriter(new FileWriter(tradesLogFile, true));
			for (String line : tradesToLog) {
				writer.write(line + "\n");
			}
			writer.close();
			// clear logs
			tradesToLog.clear();
		} catch (Throwable ignored) {
		}
	}

	// ------------------------------------------------------------
	// Data and configuration
	// ------------------------------------------------------------

	private SSDataManager dataManager = null;
	private YMLConfiguration configuration = null;

	@Override
	public YMLConfiguration getConfiguration() {
		return configuration;
	}

	public SSDataManager getData() {
		return dataManager;
	}

	@Override
	protected void unregisterData() {
		dataManager.disable();
	}

	@Override
	public void resetData() {
		dataManager.reset();
	}

	// ------------------------------------------------------------
	// ACTIVATION
	// ------------------------------------------------------------

	@Override
	protected boolean preEnable() {
		// spigot resource id
		// FIXME this.spigotResourceId = -1;
		// success
		return true;
	}

	@Override
	protected boolean innerReload() {
		// configuration
		this.configuration = new YMLConfiguration(this, new File(getDataFolder() + "/config.yml"), "config.yml", false, true);
		success("Loaded config.yml");

		// init load locale file
		SSLocale.MSG_SUPREMESHOPS_ADMINEDITSHOPINPUT.getLines();// just to init class and fields
		SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_ADMINSHOP_TRADECONDITIONS.getLines();// just to init class and fields
		SSLocaleMisc.MISC_SUPREMESHOPS_ADMINSHOPTRADEDEFAULTITEMLORE.getLines();// just to init class and fields
		reloadLocale(SSLocale.file);

		// settings
		pluginLog = getConfiguration().getBoolean("log_file", false);
		pluginLogConsole = getConfiguration().getBoolean("log_file_console", false);
		tradesPluginLog = getConfiguration().getBoolean("trades_log_file", false);
		tradesPluginLogConsole = getConfiguration().getBoolean("trades_log_file_console", false);
		forceItemDiscover = getConfiguration().getBoolean("force_item_discover", false);

		// modules
		moduleManager.reloadConfig();

		// data manager
		if (dataManager == null) {
			BackEnd backend = getConfiguration().getEnumValue("data.backend", BackEnd.class, BackEnd.JSON);
			if (backend == null) {
				backend = BackEnd.JSON;
			}
			this.dataManager = new SSDataManager(backend);
			dataManager.enable();
		}

		// restart display managers
		generalManager.restartDisplaysManagers();

		// success
		return true;
	}

	// ----------------------------------------------------------------------
	// Enable
	// ----------------------------------------------------------------------

	@Override
	protected boolean enable() {
		// gson
		GSON = Utils.createGsonBuilder()
				.registerTypeAdapter(TradeObject.class, new AdapterTradeObject())
				.registerTypeAdapter(CPConditions.class, new AdapterCPConditions())
				.setPrettyPrinting()
				.create();

		UNPRETTY_GSON = Utils.createGsonBuilder()
				.registerTypeAdapter(TradeObject.class, new AdapterTradeObject())
				.registerTypeAdapter(CPConditions.class, new AdapterCPConditions())
				.create();

		// extract default scripts
		File configFile = new File(getDataFolder() + "/config.yml");
		if (configFile.exists()) {
			YMLConfiguration config = new YMLConfiguration(this, configFile, "config.yml", false, true);
			if (config.getBoolean("generate_default_config", true)) {
				for (String path : Utils.asList("guis", "items", "merchants", "particle_patterns", "shops")) {
					ResourceExtractor extractor = new ResourceExtractor(SupremeShops.instance, new File(getDataFolder() + "/" + path), path, null);
					try {
						extractor.extract(false, true);
					} catch (IOException exception) {
						exception.printStackTrace();
						error("Could not extract the default " + path + " files");
					}
				}
			}
		}

		// inner reload
		innerReload();

		// integration
		registerPluginIntegration("AreaShop", IntegrationAreaShop.class);
		registerPluginIntegration("Citizens", IntegrationCitizens.class);
		registerPluginIntegration("MythicMobs", IntegrationMythicMobs.class);
		registerPluginIntegration("WildStacker", IntegrationWildStacker.class);

		// shop command
		CommandRootShop shopCommand = new CommandRootShop();
		registerCommand(shopCommand, SSPerm.SUPREMESHOPS_ADMIN);
		shopCommand.addChild(new ArgFixMMOItems());
		shopCommand.addChild(new ArgAdmingen());
		shopCommand.addChild(new ArgEdit());
		shopCommand.addChild(new ArgShopAdminEdit());
		shopCommand.addChild(new ArgShopList());
		shopCommand.addChild(new ArgShopPurge());
		shopCommand.addChild(new ArgMenu());

		CommandArgument commandItem = new CommandArgument(this, Utils.asList("item", "it"), "item-related commands", null, false);
		shopCommand.addChild(commandItem);
		commandItem.addChild(new ArgItemBlacklist());
		commandItem.addChild(new ArgItemWhitelist());
		commandItem.addChild(new ArgItemResetvalue());

		CommandArgument commandConvert = new CommandArgument(this, Utils.asList("convert", "import"), "convert data from other plugins", null, false);
		shopCommand.addChild(commandConvert);
		commandConvert.addChild(new ArgConvertQuantumshop());
		commandConvert.addChild(new ArgConvertQuickshop());
		commandConvert.addChild(new ArgConvertShopchest());

		// merchant command
		CommandRootMerchant merchantCommand = new CommandRootMerchant();
		registerCommand(merchantCommand, SSPerm.SUPREMESHOPS_ADMIN, true);
		merchantCommand.addChild(new ArgMerchantAdminEdit());
		merchantCommand.addChild(new ArgMerchantList());
		merchantCommand.addChild(new ArgMerchantPurge());
		merchantCommand.addChild(new ArgMerchantNpcCreate());

		// trade command
		registerCommand(new CommandRootPlayertrade(), SSPerm.SUPREMESHOPS_ADMIN, true);

		// events
		Bukkit.getPluginManager().registerEvents(new TriggersListeners(), this);
		Bukkit.getPluginManager().registerEvents(new CreationListeners(), this);
		Bukkit.getPluginManager().registerEvents(new DestructionListeners(), this);
		Bukkit.getPluginManager().registerEvents(new ItemDiscoverListeners(), this);
		Bukkit.getPluginManager().registerEvents(new JoinListeners(), this);
		Bukkit.getPluginManager().registerEvents(new MerchantsListeners(), this);
		Bukkit.getPluginManager().registerEvents(new ShopsListeners(), this);

		// tasks
		tasks.add(new InvalidShopDestroyerTask().runTaskTimer(this, 20L * 15L, 20L * 15L));  // every 15s, destroy invalid shops
		tasks.add(new InvalidMerchantDestroyerTask().runTaskTimer(this, 20L * 23L, 20L * 15L));  // every 15s, destroy invalid merchants
		tasks.add(new InactiveShopDestroyerTask().runTaskTimer(this, 20L * 20L, 20L * 60L));  // every minute, destroy inactive shops
		tasks.add(new InactiveMerchantDestroyerTask().runTaskTimer(this, 20L * 50L, 20L * 60L));  // every minute, destroy inactive merchants
		tasks.add(new ManagersWagesPayTask().runTaskTimer(this, 20L * 20L, 20L * 60L));  // every minute, pay managers wages
		tasks.add(new ManagersLateWagesPayTask().runTaskTimer(this, 20L * 40L, 20L * 60L));  // every minute, pay late managers wages
		tasks.add(new RentsUpdateTask().runTaskTimer(this, 20L * 60L, 20L * 50L));  // every minute, update rents
		tasks.add(new InactiveShopDestroyerTask().runTaskTimer(this, 20L * 50L, 20L * 60L));  // every minute, destroy inactive merchants
		tasks.add(new PerMinuteModifiersApplicationTask().runTaskTimerAsynchronously(this, 20L * 60L, 20L * 60L));  // every minute, apply per minute modifiers for items
		tasks.add(new BukkitRunnable() {
			@Override
			public void run() {
				savePluginLog();
				saveTradesPluginLog();
			}
		}.runTaskTimer(this, 20L, 20L));
		tasks.add(new BukkitRunnable() {
			@Override
			public void run() {
				pushBoardIfNeeded(true);
			}
		}.runTaskTimer(this, 20L * 15L, 20L * 15L));

		// success
		return true;
	}

	// ----------------------------------------------------------------------
	// Disable
	// ----------------------------------------------------------------------

	@Override
	protected void disable() {
		// cancel player trades
		generalManager.cancelPlayerTrades();
		// remove displays
		generalManager.stopDisplayManagers();
		// remove merchant npcs
		generalManager.removeMerchantsNpcs();
		// push board if needed
		pushBoardIfNeeded(false);
		// save logs
		savePluginLog();
		saveTradesPluginLog();
		// stop tasks
		for (BukkitTask task : tasks) {
			task.cancel();
		}
	}

}
