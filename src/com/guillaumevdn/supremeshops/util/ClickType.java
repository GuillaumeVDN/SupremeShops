package com.guillaumevdn.supremeshops.util;

import com.guillaumevdn.gcorelegacy.lib.messenger.Text;
import com.guillaumevdn.supremeshops.SSLocaleMisc;

public enum ClickType {

	LEFT_CLICK(SSLocaleMisc.MISC_SUPREMESHOPS_LEFTCLICK),
	RIGHT_CLICK(SSLocaleMisc.MISC_SUPREMESHOPS_RIGHTCLICK);

	private Text text;

	private ClickType(Text text) {
		this.text = text;
	}

	public Text getText() {
		return text;
	}

}
