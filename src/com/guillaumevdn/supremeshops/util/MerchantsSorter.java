package com.guillaumevdn.supremeshops.util;

import java.util.Comparator;
import java.util.List;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.messenger.Text;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;

public class MerchantsSorter {

	// base
	private List<Merchant> list;
	private SortCriteria criteria;

	public MerchantsSorter(List<Merchant> list,SortCriteria criteria) {
		this.list = list;
		this.criteria = criteria;
	}

	// get
	public List<Merchant> getList() {
		return list;
	}

	public SortCriteria getCriteria() {
		return criteria;
	}

	public List<Merchant> getSortedList() {
		// by name
		List<Merchant> sorted = Utils.asList(list);
		if (criteria.equals(SortCriteria.BY_NAME)) {
			Utils.sortList(sorted, NAME_COMPARATOR);
		}
		// by ranking (server)
		else if (criteria.equals(SortCriteria.BY_RANKING_SERVER)) {
			Utils.sortList(sorted, TRADES_SERVER_COMPARATOR);
		}
		// by ranking (seller)
		else if (criteria.equals(SortCriteria.BY_RANKING_SELLER)) {
			Utils.sortList(sorted, TRADES_SELLER_COMPARATOR);
		}
		// by unique buyers
		else if (criteria.equals(SortCriteria.BY_UNIQUE_BUYERS)) {
			Utils.sortList(sorted, BUYERS_COMPARATOR);
		}
		// sorted, return it
		return sorted;
	}

	// comparators
	public static final Comparator<Merchant> NAME_COMPARATOR = new Comparator<Merchant>() {
		@Override
		public int compare(Merchant s1, Merchant s2) {
			return s1.getDisplayName().compareTo(s2.getDisplayName());
		}
	};

	public static final Comparator<Merchant> TRADES_SERVER_COMPARATOR = new Comparator<Merchant>() {
		@Override
		public int compare(Merchant s1, Merchant s2) {
			return Integer.compare(s1.getBuyersTrades(), s2.getBuyersTrades());
		}
	};

	public static final Comparator<Merchant> TRADES_SELLER_COMPARATOR = new Comparator<Merchant>() {
		@Override
		public int compare(Merchant s1, Merchant s2) {
			// not the same owner weight
			int ownerCompare = OWNER_COMPARATOR.compare(s1.getCurrentOwner(), s2.getCurrentOwner());
			if (ownerCompare != 0) return ownerCompare;
			// same owner
			return Integer.compare(s1.getBuyersTrades(), s2.getBuyersTrades());
		}
	};

	public static final Comparator<UserInfo> OWNER_COMPARATOR = new Comparator<UserInfo>() {
		@Override
		public int compare(UserInfo o1, UserInfo o2) {
			// same owner
			if (Utils.equals(o1, o2)) {
				return 0;
			}
			// not the same owner
			else {
				// compare between two owners type (admin/player)
				if (o2 == null && o1 != null) return 1;
				if (o1 == null && o2 != null) return -1;
				// compare between two owners names
				return o1.toOfflinePlayer().getName().compareToIgnoreCase(o2.toOfflinePlayer().getName());
			}
		}
	};

	public static final Comparator<Merchant> BUYERS_COMPARATOR = new Comparator<Merchant>() {
		@Override
		public int compare(Merchant s1, Merchant s2) {
			return Integer.compare(s1.getBuyers().size(), s2.getBuyers().size());
		}
	};

	// sort criteria
	public enum SortCriteria {

		// values
		BY_NAME(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIABYNAME),
		BY_RANKING_SERVER(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIABYRANKINGSERVER),
		BY_RANKING_SELLER(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIABYRANKINGSELLER),
		BY_UNIQUE_BUYERS(SSLocaleMisc.MISC_SUPREMESHOPS_MERCHANTLISTSORTCRITERIABYUNIQUEBUYERS),
		;

		// base
		private Text name;

		private SortCriteria(Text name) {
			this.name = name;
		}

		// get
		public Text getName() {
			return name;
		}

	}

}
