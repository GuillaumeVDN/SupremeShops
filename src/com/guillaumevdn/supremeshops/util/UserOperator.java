package com.guillaumevdn.supremeshops.util;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.data.DataManager.Callback;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.SSUser;

public abstract class UserOperator {

	// base
	private UserInfo info;
	private SSUser user = null;
	private boolean offline = false;

	public UserOperator(UserInfo info) {
		this.info = info;
	}

	// methods
	public void operate() {
		// get quests
		user = SSUser.get(info);
		if (user != null) {// online
			process();
		} else {// offline, load
			SupremeShops.inst().getData().getUsers().loadUser(info, new Callback() {
				@Override
				public void callback() {
					user = SSUser.get(info);
					offline = true;
					process();
				}
			});
		}
	}

	private void process() {
		// process
		process(user);
		// unload eventually
		if (offline) {
			SupremeShops.inst().getData().getUsers().unloadUser(info);
		}
	}

	// abstract methods
	protected abstract void process(SSUser user);

}
