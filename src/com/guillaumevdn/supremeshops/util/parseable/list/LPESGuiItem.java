package com.guillaumevdn.supremeshops.util.parseable.list;

import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ConfigData;
import com.guillaumevdn.gcorelegacy.lib.parseable.ListParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.supremeshops.module.gui.SSGuiItem;

public class LPESGuiItem extends ListParseable<SSGuiItem> {

	// base
	public LPESGuiItem(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "condition", mandatory, editorSlot, editorIcon, editorDescription);
	}

	// methods
	@Override
	public SSGuiItem createElement(String elementId) {
		// create data
		ConfigData data = new ConfigData(getLastData().getPlugin(), getLastData().getSuperId(), getLastData().getConfig(), getLastData().getPath().isEmpty() ? elementId : getLastData().getPath() + "." + elementId);
		// create
		SSGuiItem element = new SSGuiItem(elementId, this, false, -1, getEditorIcon(), null);
		// load, add and return
		element.load(data);
		addElement(element);
		return element;
	}

	@Override
	public SSGuiItem loadElement(String elementId, ConfigData data) {
		// create
		SSGuiItem element = new SSGuiItem(elementId, this, false, -1, getEditorIcon(), null);
		// load, add and return
		element.load(data);
		addElement(element);
		return element;
	}

}
