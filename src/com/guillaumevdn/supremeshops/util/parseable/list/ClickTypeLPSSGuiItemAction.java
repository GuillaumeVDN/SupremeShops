package com.guillaumevdn.supremeshops.util.parseable.list;

import java.util.List;

import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ConfigData;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.list.EnumListParseable;
import com.guillaumevdn.supremeshops.module.gui.SSGuiItemAction;
import com.guillaumevdn.supremeshops.util.parseable.primitive.PPSSGuiItemAction;

public class ClickTypeLPSSGuiItemAction extends EnumListParseable<PPSSGuiItemAction, ClickType> {

	// base
	public ClickTypeLPSSGuiItemAction(String id, Parseable parent, boolean allowDefaultCase, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, allowDefaultCase, ClickType.class, "inventory click type", mandatory, editorSlot, editorIcon, editorDescription);
	}

	// methods
	@Override
	public PPSSGuiItemAction createElement(String elementId) {
		// create data
		ConfigData data = new ConfigData(getLastData().getPlugin(), getLastData().getSuperId(), getLastData().getConfig(), getLastData().getPath().isEmpty() ? elementId : getLastData().getPath() + "." + elementId);
		// create
		PPSSGuiItemAction element = new PPSSGuiItemAction(elementId, this, SSGuiItemAction.Type.NONE.toString(), false, -1, getEditorIcon(), null);
		// load, add and return
		element.load(data);
		addElement(element);
		return element;
	}

	@Override
	public PPSSGuiItemAction loadElement(String elementId, ConfigData data) {
		// create
		PPSSGuiItemAction element = new PPSSGuiItemAction(elementId, this, SSGuiItemAction.Type.NONE.toString(), false, -1, getEditorIcon(), null);
		// load, add and return
		element.load(data);
		addElement(element);
		return element;
	}

}
