package com.guillaumevdn.supremeshops.util.parseable.list;

import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ConfigData;
import com.guillaumevdn.gcorelegacy.lib.parseable.ListParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;

public class LPCondition extends ListParseable<Condition> {

	// base
	public LPCondition(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "condition", mandatory, editorSlot, editorIcon, editorDescription);
	}

	// methods
	public String getNextId() {
		int i = 1;
		for (Condition condition : getElements().values()) {
			Integer number = Utils.integerOrNull(condition.getId());
			if (number != null && number > i) {
				i = number;
			}
		}
		return "" + i;
	}

	@Override
	public Condition createElement(String elementId) {
		// create data
		ConfigData data = new ConfigData(getLastData().getPlugin(), getLastData().getSuperId(), getLastData().getConfig(), getLastData().getPath().isEmpty() ? elementId : getLastData().getPath() + "." + elementId);
		// create
		Condition element = ConditionType.DAY_OF_WEEK.createNew(elementId, this, data, false, false, -1, getEditorIcon(), null);
		// load, add and return
		element.load(data);
		addElement(element);
		return element;
	}

	@Override
	public Condition loadElement(String elementId, ConfigData data) {
		// create
		Condition element = Condition.load(elementId, this, data, false, -1, getEditorIcon(), null);
		// add and return
		if (element != null) {
			addElement(element);
		}
		return element;
	}

	// clone
	protected LPCondition() {
		super();
	}

	@Override
	public LPCondition clone() {
		return (LPCondition) super.clone();
	}

}
