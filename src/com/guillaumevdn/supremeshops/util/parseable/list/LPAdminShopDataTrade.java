package com.guillaumevdn.supremeshops.util.parseable.list;

import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ConfigData;
import com.guillaumevdn.gcorelegacy.lib.parseable.ListParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.supremeshops.module.adminshop.AdminShopDataTrade;

public class LPAdminShopDataTrade extends ListParseable<AdminShopDataTrade> {

	// base
	public LPAdminShopDataTrade(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "admin shop trade", mandatory, editorSlot, editorIcon, editorDescription);
	}

	// methods
	@Override
	public AdminShopDataTrade createElement(String elementId) {
		// create data
		ConfigData data = new ConfigData(getLastData().getPlugin(), getLastData().getSuperId(), getLastData().getConfig(), getLastData().getPath().isEmpty() ? elementId : getLastData().getPath() + "." + elementId);
		// create
		AdminShopDataTrade element = new AdminShopDataTrade(elementId, this, false, -1, getEditorIcon(), null);
		// load, add and return
		element.load(data);
		addElement(element);
		return element;
	}

	@Override
	public AdminShopDataTrade loadElement(String elementId, ConfigData data) {
		// create
		AdminShopDataTrade element = new AdminShopDataTrade(elementId, this, false, -1, getEditorIcon(), null);
		// load, add and return
		element.load(data);
		addElement(element);
		return element;
	}

}
