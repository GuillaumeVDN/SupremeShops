package com.guillaumevdn.supremeshops.util.parseable.container;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.messenger.Text;
import com.guillaumevdn.gcorelegacy.lib.parseable.ContainerParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPInteger;
import com.guillaumevdn.gcorelegacy.lib.parseable.primitive.PPText;
import com.guillaumevdn.supremeshops.SSLocaleGUI;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.trigger.Trigger;
import com.guillaumevdn.supremeshops.util.parseable.list.LPCondition;

public class CPConditions extends ContainerParseable {

	// base
	private LPCondition conditions = addComponent(new LPCondition("conditions", this, true, 0, EditorGUI.ICON_CONDITION, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_OBJECT_CONDITIONSLORE.getLines()));
	private PPInteger requiredValid = addComponent(new PPInteger("required_valid", this, "9999", 0, Integer.MAX_VALUE, false, 1, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_OBJECT_CONDITIONSREQUIREDVALIDLORE.getLines()));
	private PPInteger requiredNotValid = addComponent(new PPInteger("required_not_valid", this, "0", 0, Integer.MAX_VALUE, false, 2, EditorGUI.ICON_NUMBER, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_OBJECT_CONDITIONSREQUIREDNOTVALIDLORE.getLines()));
	private PPText generalErrorMessage = addComponent(new PPText("general_error_message", this, null, false, 3, EditorGUI.ICON_STRING_LIST, SSLocaleGUI.GUI_SUPREMESHOPS_EDITOR_OBJECT_CONDITIONSGENERALERRORMESSAGELORE.getLines()));

	public CPConditions(String id, Parseable parent, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, "conditions settings", mandatory, editorSlot, editorIcon, editorDescription);
	}

	// get
	public LPCondition getConditions() {
		return conditions;
	}

	public PPInteger getRequiredValid() {
		return requiredValid;
	}

	public Integer getRequiredValid(Player parser) {
		return requiredValid.getParsedValue(parser);
	}

	public PPInteger getRequiredNotValid() {
		return requiredNotValid;
	}

	public Integer getRequiredNotValid(Player parser) {
		return requiredNotValid.getParsedValue(parser);
	}

	public PPText getGeneralErrorMessage() {
		return generalErrorMessage;
	}

	public Text getGeneralErrorMessage(Player parser) {
		return generalErrorMessage.getParsedValue(parser);
	}

	// methods
	public boolean isValid(Player player, Shop shop, boolean sendErrorMessage) {
		// no conditions
		if (conditions.getElements().isEmpty()) {
			return true;
		}
		// check
		int size = conditions.getElements().size();
		List<Condition> valid = new ArrayList<Condition>(), invalid = new ArrayList<Condition>();
		for (Condition condition : conditions.getElements().values()) {
			// check shop
			try {
				(condition.isValid(shop, player) ? valid : invalid).add(condition);
			} catch (UnsupportedOperationException ignored) {
				// check player
				try {
					(condition.isValid(player) ? valid : invalid).add(condition);
				} catch (UnsupportedOperationException ignored2) {
					// unsupported
					--size;
				}
			}
		}
		// success
		int requiredValid = getRequiredValid(player);
		if (requiredValid > size) requiredValid = size;
		int requiredNotValid = getRequiredNotValid(player);
		if (requiredNotValid > size) requiredNotValid = size;
		if (valid.size() >= requiredValid && invalid.size() >= requiredNotValid) {
			return true;
		}
		// invalid
		else {
			if (sendErrorMessage) {
				// general error message
				Text general = generalErrorMessage.getValue() == null || generalErrorMessage.getValue().isEmpty() ? null : generalErrorMessage.getParsedValue(player);
				if (general != null) {
					general.send(player);
				}
				// all other messages
				else {
					for (Condition condition : conditions.getElements().values()) {
						Text message = condition.getErrorMessage(player);
						if (message != null) {
							message.send(player);
						}
					}
				}
			}
			return false;
		}
	}

	public boolean isValid(Player player, Trigger trigger, boolean sendErrorMessage) {
		// no conditions
		if (conditions.getElements().isEmpty()) {
			return true;
		}
		// check
		int size = conditions.getElements().size();
		List<Condition> valid = new ArrayList<Condition>(), invalid = new ArrayList<Condition>();
		for (Condition condition : conditions.getElements().values()) {
			// check trigger
			try {
				(condition.isValid(trigger, player) ? valid : invalid).add(condition);
			} catch (UnsupportedOperationException ignored) {
				// check player
				try {
					(condition.isValid(player) ? valid : invalid).add(condition);
				} catch (UnsupportedOperationException ignored2) {
					// unsupported
					--size;
				}
			}
		}
		// success
		int requiredValid = getRequiredValid(player);
		if (requiredValid > size) requiredValid = size;
		int requiredNotValid = getRequiredNotValid(player);
		if (requiredNotValid > size) requiredNotValid = size;
		if (valid.size() >= requiredValid && invalid.size() >= requiredNotValid) {
			return true;
		}
		// invalid
		else {
			if (sendErrorMessage) {
				// general error message
				Text general = generalErrorMessage.getValue() == null || generalErrorMessage.getValue().isEmpty() ? null : generalErrorMessage.getParsedValue(player);
				if (general != null) {
					general.send(player);
				}
				// all other messages
				else {
					for (Condition condition : conditions.getElements().values()) {
						Text message = condition.getErrorMessage(player);
						if (message != null) {
							message.send(player);
						}
					}
				}
			}
			return false;
		}
	}

	public boolean isValid(Player player, Merchant merchant, boolean sendErrorMessage) {
		// no conditions
		if (conditions.getElements().isEmpty()) {
			return true;
		}
		// check
		int size = conditions.getElements().size();
		List<Condition> valid = new ArrayList<Condition>(), invalid = new ArrayList<Condition>();
		for (Condition condition : conditions.getElements().values()) {
			// check merchant
			try {
				(condition.isValid(merchant, player) ? valid : invalid).add(condition);
			} catch (UnsupportedOperationException ignored) {
				// check player
				try {
					(condition.isValid(player) ? valid : invalid).add(condition);
				} catch (UnsupportedOperationException ignored2) {
					// unsupported
					--size;
				}
			}
		}
		// success
		int requiredValid = getRequiredValid(player);
		if (requiredValid > size) requiredValid = size;
		int requiredNotValid = getRequiredNotValid(player);
		if (requiredNotValid > size) requiredNotValid = size;
		if (valid.size() >= requiredValid && invalid.size() >= requiredNotValid) {
			return true;
		}
		// invalid
		else {
			if (sendErrorMessage) {
				// general error message
				Text general = generalErrorMessage.getValue() == null || generalErrorMessage.getValue().isEmpty() ? null : generalErrorMessage.getParsedValue(player);
				if (general != null) {
					general.send(player);
				}
				// all other messages
				else {
					for (Condition condition : conditions.getElements().values()) {
						Text message = condition.getErrorMessage(player);
						if (message != null) {
							message.send(player);
						}
					}
				}
			}
			return false;
		}
	}

	// clone
	protected CPConditions() {
		super();
	}

	@Override
	public CPConditions clone() {
		return (CPConditions) super.clone();
	}

}
