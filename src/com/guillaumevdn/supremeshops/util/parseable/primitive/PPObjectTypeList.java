package com.guillaumevdn.supremeshops.util.parseable.primitive;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;

import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.parseable.ParseResult;
import com.guillaumevdn.gcorelegacy.lib.parseable.Parseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.PrimitiveParseable;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorItem;
import com.guillaumevdn.gcorelegacy.lib.parseable.editor.ModifCallback;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.module.object.ObjectType;

public class PPObjectTypeList extends PrimitiveParseable<List<ObjectType>> {

	// base
	public PPObjectTypeList(String id, Parseable parent, List<String> defaultValue, boolean mandatory, int editorSlot, Mat editorIcon, List<String> editorDescription) {
		super(id, parent, defaultValue, "list of object types", mandatory, editorSlot, editorIcon, editorDescription);
	}

	// parse
	@Override
	public ParseResult<List<ObjectType>> parseValue(List<String> value, Player parsing) throws Throwable {
		if (value.isEmpty()) {
			return new ParseResult<List<ObjectType>>(new ArrayList<ObjectType>());
		}
		List<ObjectType> result = new ArrayList<ObjectType>();
		for (String val : value) {
			ObjectType parsed = ObjectType.valueOf(val);
			if (parsed != null) {
				result.add(parsed);
			} else {
				getLastData().log("couldn't parse element " + val + " for " + parsing.getName());
				return null;
			}
		}
		return new ParseResult<List<ObjectType>>(result);
	}

	// editor
	@Override
	protected void fillEditor(final EditorGUI gui, Player player, final ModifCallback onModif) {
		// current and delete
		EditorGUI.fillItemCurrent(gui, player, 20, this, onModif);
		EditorGUI.fillItemDelete(gui, player, 24, this, onModif);
		// set line icons
		if (getValue() != null) {
			for (int i = 0; i < getValue().size(); ++i) {
				final String line = getValue().get(i);
				final int index = i;
				gui.setRegularItem(new EditorItem("line_" + index, -1, Mat.PAPER, "§6" + (index + 1), GLocale.GUI_GENERIC_EDITORLISTELEMENTLORE.getLines("{current}", line)) {
					@Override
					protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
						// edit
						if (clickType.isLeftClick()) {
							// create sub GUI
							String name = Utils.getNewInventoryName(gui.getName(), "" + (index + 1));
							EditorGUI sub = new EditorGUI(getLastData().getPlugin(), gui, name, 9, GUI.SLOTS_0_TO_7) {
								private EditorGUI subThis = this;
								@Override
								protected void fill() {
									// current, raw and delete
									EditorGUI.fillItemCurrent(subThis, player, "" + (index + 1), Utils.asList(line), null, "GCoreLegacy material", isMandatory(), getEditorIcon(), 0, onModif);
									EditorGUI.fillItemRaw(subThis, player, 3, getValue().get(index), onModif, new RawChangeCallback() {
										@Override
										public void callback(EditorGUI from, Player player, String value) {
											getValue().set(index, value);
										}
									});
									EditorGUI.fillItemDelete(subThis, player, 6, onModif, new ModifCallback() {
										@Override
										protected void onModif(Object instance, EditorGUI from, Player player) {
											getValue().remove(index);
										}
									});
									// select
									setRegularItem(new EditorItem("control_item_select", 2, Mat.ENDER_CHEST, GLocale.GUI_GENERIC_EDITORENUMSELECT.getLine(), GLocale.GUI_GENERIC_EDITORENUMSELECTLORE.getLines()) {
										@Override
										protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
											// selection gui
											EditorGUI subSelection = new EditorGUI(getLastData().getPlugin(), gui, Utils.getNewInventoryName(gui.getName(), "Select"), 54, GUI.SLOTS_0_TO_44) {
												@Override
												protected void fill() {
													// add values
													for (final ObjectType val : Utils.asSortedList(ObjectType.values(), Utils.objectSorter)) {
														if (!val.hasAllRequiredPlugins()) continue;
														final String valName = val.getId();
														setRegularItem(new EditorItem("value_" + valName, -1, Mat.ENDER_CHEST, "§6" + valName, null) {
															@Override
															protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
																// replace value
																getValue().set(index, valName);
																onModif.callback(null, gui, player);
																// re-fill and open
																subThis.open(player);
															}
														});
													}
													// back item
													setPersistentItem(new EditorItem("control_item_back", 52, Mat.ARROW, GLocale.GUI_GENERIC_EDITORITEMBACK.getLine(), null) {
														@Override
														protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
															gui.open(player);
														}
													});
												}
											};
											// open it
											subSelection.open(player);
										}
									});
									// back item
									setPersistentItem(new EditorItem("control_item_back", 8, Mat.ARROW, GLocale.GUI_GENERIC_EDITORITEMBACK.getLine(), null) {
										@Override
										protected void onClick(final Player player, final ClickType clickType, final int pageIndex) {
											gui.open(player);
										}
									});
								}
							};
							// open it
							sub.open(player);
						}
						// delete
						else if (clickType.isRightClick()) {
							// delete value
							getValue().remove(index);
							onModif.callback(null, gui, player);
							// re-fill and open
							gui.open(player);
						}
					}
				});
			}
		}
		// new value
		gui.setPersistentItem(new EditorItem("control_item_add", 22, Mat.BLAZE_ROD, GLocale.GUI_GENERIC_EDITORITEMADD.getLine(), null) {
			@Override
			protected void onClick(Player player, ClickType clickType, int pageIndex) {
				// add value
				if (getValue() == null) {
					setValue(Utils.asList(ObjectType.ITEM.getId()));
				} else {
					getValue().add(ObjectType.ITEM.getId());
				}
				onModif.callback(null, gui, player);
				// re-fill and open
				gui.open(player);
			}
		});
	}

	@Override
	public int getEditorSize() {
		return 27;
	}

	@Override
	public List<Integer> getEditorRegularSlots() {
		return GUI.SLOTS_0_TO_17;
	}

	@Override
	public int getEditorBackSlot() {
		return 25;
	}

	// clone
	protected PPObjectTypeList() {
		super();
	}

	@Override
	public PPObjectTypeList clone() {
		return (PPObjectTypeList) super.clone();
	}

}
