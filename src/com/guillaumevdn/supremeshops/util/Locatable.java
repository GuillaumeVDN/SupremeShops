package com.guillaumevdn.supremeshops.util;

import org.bukkit.Location;
import org.bukkit.World;

import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;

/**
 * @author GuillaumeVDN
 */
public interface Locatable {

	World getWorld();
	BlockCoords getBlock();
	Location getLocation();

}
