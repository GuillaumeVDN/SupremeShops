package com.guillaumevdn.supremeshops.util;

import java.io.IOException;

import com.guillaumevdn.gcorelegacy.lib.parseable.editor.EditorGUI;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.TypeAdapter;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonToken;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.module.condition.Condition;
import com.guillaumevdn.supremeshops.module.condition.ConditionType;
import com.guillaumevdn.supremeshops.util.parseable.container.CPConditions;

public class AdapterCPConditions extends TypeAdapter<CPConditions> {

	@Override
	public void write(JsonWriter out, CPConditions value) throws IOException {
		if (value == null) {
			out.nullValue();
		} else {
			// begin
			out.beginObject();
			// condition
			out.name("conditions").beginObject();
			for (Condition condition : value.getConditions().getElements().values()) {
				out.name(condition.getId()).beginObject();
				out.name("type").value(condition.getType().getId());
				condition.getType().getLogic().writeJsonProperties(out, condition);
				out.endObject();
			}
			out.endObject();
			// required valid
			JsonUtils.writeJsonValue(out, "required_valid", value.getRequiredValid().getValue());
			// required not valid
			JsonUtils.writeJsonValue(out, "required_not_valid", value.getRequiredNotValid().getValue());
			// general error message
			JsonUtils.writeJsonValue(out, "general_error_message", value.getGeneralErrorMessage().getValue());
			// end
			out.endObject();
		}
	}

	@Override
	public CPConditions read(JsonReader in) throws IOException {
		if (in.peek().equals(JsonToken.NULL)) {
			in.nextNull();
			return null;
		} else {
			// begin
			in.beginObject();
			CPConditions conditions = new CPConditions("conditions", null, false, -1, EditorGUI.ICON_CONDITION, null);
			// conditions
			in.nextName();
			in.beginObject();
			while (in.peek().equals(JsonToken.NAME)) {
				String id = in.nextName();
				in.beginObject();
				in.nextName();
				ConditionType type = ConditionType.valueOf(in.nextString());
				conditions.getConditions().addElement(type.getLogic().readJsonPropertiesAndBuild(in, conditions, id));
				in.endObject();
			}
			in.endObject();
			// required valid
			conditions.getRequiredValid().setValue(JsonUtils.readJsonValue(in));
			// required not valid
			conditions.getRequiredNotValid().setValue(JsonUtils.readJsonValue(in));
			// general error message
			conditions.getGeneralErrorMessage().setValue(JsonUtils.readJsonValue(in));
			// done
			in.endObject();
			return conditions;
		}
	}

}
