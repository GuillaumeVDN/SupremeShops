package com.guillaumevdn.supremeshops.util;

import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;

public class ListItem {

	// base
	private ItemData item;
	private boolean exactMatch, durabilityCheck;

	public ListItem(ItemData item) {
		this(item, true, item.isUnbreakable());
	}

	public ListItem(ItemData item, boolean exactMatch, boolean durabilityCheck) {
		this.item = item;
		this.exactMatch = exactMatch;
		this.durabilityCheck = durabilityCheck;
	}

	// get
	public ItemData getItem() {
		return item;
	}

	public boolean getExactMatch() {
		return exactMatch;
	}

	public boolean getDurabilityCheck() {
		return durabilityCheck;
	}

	// set
	public void setItem(ItemData item) {
		this.item = item;
	}

	public void setExactMatch(boolean exactMatch) {
		this.exactMatch = exactMatch;
	}

	public void setDurabilityCheck(boolean durabilityCheck) {
		this.durabilityCheck = durabilityCheck;
	}

	// methods
	public boolean match(ItemStack stack) {
		return item.isSimilar(stack, durabilityCheck, exactMatch, false);
	}
	
	@Override
	public String toString() {
		return item.getId();
	}

}
