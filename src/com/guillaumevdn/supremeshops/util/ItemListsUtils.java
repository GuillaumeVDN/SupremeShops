package com.guillaumevdn.supremeshops.util;

import java.io.File;

import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.lib.configuration.YMLConfiguration;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.supremeshops.SupremeShops;

public class ItemListsUtils {

	public static boolean isItemBlacklisted(ItemStack item) {
		for (ListItem blacklisted : SupremeShops.inst().getModuleManager().getItemBlacklist()) {
			if (blacklisted.match(item)) {
				return true;
			}
		}
		return false;
	}

	public static boolean isItemWhitelisted(ItemStack item) {
		for (ListItem whitelisted : SupremeShops.inst().getModuleManager().getItemWhitelist()) {
			if (whitelisted.match(item)) {
				return true;
			}
		}
		return false;
	}

	public static void updateItemBlacklist(ListItem item) {
		removeItemBlacklist(item.getItem(), false);
		SupremeShops.inst().getModuleManager().getItemBlacklist().add(item);
		pushBlacklistAsync();
	}

	public static void removeItemBlacklist(ItemData item, boolean push) {
		for (int i = 0; i < SupremeShops.inst().getModuleManager().getItemBlacklist().size(); ++i) {
			if (SupremeShops.inst().getModuleManager().getItemBlacklist().get(i).getItem().equals(item)) {
				SupremeShops.inst().getModuleManager().getItemBlacklist().remove(i);
				if (push) pushBlacklistAsync();
				return;
			}
		}
	}

	public static void updateItemWhitelist(ListItem item) {
		removeItemWhitelist(item.getItem(), false);
		SupremeShops.inst().getModuleManager().getItemWhitelist().add(item);
		pushWhitelistAsync();
	}

	public static void removeItemWhitelist(ItemData item, boolean push) {
		for (int i = 0; i < SupremeShops.inst().getModuleManager().getItemWhitelist().size(); ++i) {
			if (SupremeShops.inst().getModuleManager().getItemWhitelist().get(i).getItem().equals(item)) {
				SupremeShops.inst().getModuleManager().getItemWhitelist().remove(i);
				if (push) pushWhitelistAsync();
				return;
			}
		}
	}

	public static boolean isItemAllowed(ItemStack item) {
		// blacklist
		if (SupremeShops.inst().getModuleManager().getEnableItemBlacklist()) {
			for (ListItem blacklisted : SupremeShops.inst().getModuleManager().getItemBlacklist()) {
				if (blacklisted.match(item)) {
					return false;
				}
			}
		}
		// whitelist
		if (SupremeShops.inst().getModuleManager().getEnableItemWhitelist()) {
			boolean hasOne = false;
			for (ListItem whitelisted : SupremeShops.inst().getModuleManager().getItemWhitelist()) {
				if (whitelisted.match(item)) {
					hasOne = true;
					break;
				}
			}
            return hasOne;
		}
		// seems good
		return true;
	}

	public static void pushBlacklistAsync() {
		new BukkitRunnable() {
			@Override
			public void run() {
				YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/items/blacklist.yml"), "items/blacklist.yml", true, false);
				int id = 0;
				for (ListItem item : SupremeShops.inst().getModuleManager().getItemBlacklist()) {
					config.setItem("list." + ++id, item.getItem());
					config.set("list." + id + ".exact_match", item.getExactMatch());
					config.set("list." + id + ".durability_check", item.getDurabilityCheck());
				}
				config.save();
			}
		}.runTaskAsynchronously(SupremeShops.inst());
	}

	public static void pushWhitelistAsync() {
		new BukkitRunnable() {
			@Override
			public void run() {
				YMLConfiguration config = new YMLConfiguration(SupremeShops.inst(), new File(SupremeShops.inst().getDataFolder() + "/items/whitelist.yml"), "items/whitelist.yml", true, false);
				int id = 0;
				for (ListItem item : SupremeShops.inst().getModuleManager().getItemWhitelist()) {
					config.setItem("list." + ++id, item.getItem());
					config.set("list." + id + ".exact_match", item.getExactMatch());
					config.set("list." + id + ".durability_check", item.getDurabilityCheck());
				}
				config.save();
			}
		}.runTaskAsynchronously(SupremeShops.inst());
	}

}
