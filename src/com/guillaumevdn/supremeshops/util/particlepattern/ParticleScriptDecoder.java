package com.guillaumevdn.supremeshops.util.particlepattern;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.lib.versioncompat.particle.ParticleManager;
import com.guillaumevdn.supremeshops.SupremeShops;

public class ParticleScriptDecoder {

	// base
	private File file;
	private ParticleScript script;

	public ParticleScriptDecoder(File file) {
		this.file = file;
		this.script = new ParticleScript(file.getName());
	}

	// get
	public File getFile() {
		return file;
	}

	public ParticleScript getScript() {
		return script;
	}

	// decode
	public void decode() throws Throwable {
		// read lines without comments
		List<String> lines = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new FileReader(file));
		String line;
		while ((line = reader.readLine()) != null) {
			int commentIndex = line.indexOf("//");
			if (commentIndex != -1) line = line.substring(0, commentIndex);
			if (!line.trim().isEmpty()) {
				lines.add(line);
			}
		}
		reader.close();
		// consume lines
		consume(lines, 0, 1, new HashMap<String, Double>());
	}

	protected void consume(List<String> lines, int index, int twoPoints, Map<String, Double> localVars) {
		// build line (with local variables)
		String trimmed = lines.get(index).trim();
		for (String var : localVars.keySet()) {
			int i = trimmed.toLowerCase().indexOf(var);
			if (i != -1) {
				trimmed = trimmed.substring(0, i) + localVars.get(var) + trimmed.substring(i + var.length());
			}
		}
		// not the matching hyphens
		int twoPointsCount = 0;
		for (char c : trimmed.toCharArray()) {
			if (c == ':') ++twoPointsCount;
			else break;
		}
		if (twoPointsCount != twoPoints) {
			return;
		}
		trimmed = trimmed.substring(twoPointsCount);
		// loop
		String trimmedLower = trimmed.toLowerCase();
		if (trimmedLower.startsWith("loop ")) {
			// decode params
			String var;
			double begin, end, step;
			try {
				String[] params = trimmed.substring("loop ".length()).split(",");
				var = params[0];
				begin = Double.parseDouble(params[1]);
				end = Double.parseDouble(params[2]);
				step = Double.parseDouble(params[3]);
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("On decode of script " + file.getName() + ", an error occured while parsing params of line '" + trimmed + "'");
				return;
			}
			// loop
			++index;
			for (double i = begin; i <= end; i += step) {
				Map<String, Double> newLocalVars = Utils.asMapCopy(localVars);
				newLocalVars.put(var, i);
				consume(lines, index, twoPoints + 1, newLocalVars);
			}
			// when does it end ?
			for (; index < lines.size(); ++index) {
				int tpCount = 0;
				for (char c : lines.get(index).trim().toCharArray()) {
					if (c == ':') ++tpCount;
					else break;
				}
				if (tpCount != twoPoints + 1) {
					break;
				}
			}
			// keep consuming
			if (index < lines.size()) {
				consume(lines, index, twoPoints, localVars);
			}
			return;
		}
		// display
		else if (trimmedLower.startsWith("display ")) {
			// decode params
			ParticleManager.Type effect;
			String x, y, z, count;
			try {
				String[] params = trimmed.substring("display ".length()).split(",");
				effect = Utils.valueOfOrNull(ParticleManager.Type.class, params[0]);
				x = params[1];
				y = params[2];
				z = params[3];
				count = params[4];
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("On decode of script " + file.getName() + ", an error occured while parsing params of line '" + trimmed + "'");
				return;
			}
			// add operation
			script.getOperations().add(new OperationDisplay(effect, x, y, z, count));
		}
		// set
		else if (trimmedLower.startsWith("set ")) {
			// decode params
			String variable, value;
			try {
				String[] params = trimmed.substring("set ".length()).split(",");
				variable = params[0];
				value = params[1];
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("On decode of script " + file.getName() + ", an error occured while parsing params of line '" + trimmed + "'");
				return;
			}
			// add operation
			script.getOperations().add(new OperationSet(variable, value));
		}
		// wait_ticks
		else if (trimmedLower.startsWith("wait_ticks ")) {
			// decode params
			String ticks;
			try {
				ticks = trimmed.substring("wait_ticks ".length());
			} catch (Throwable exception) {
				exception.printStackTrace();
				SupremeShops.inst().error("On decode of script " + file.getName() + ", an error occured while parsing params of line '" + trimmed + "'");
				return;
			}
			// add operation
			script.getOperations().add(new OperationWaitTicks(ticks));
		}
		// other operation
		else {
			SupremeShops.inst().error("On decode of script " + file.getName() + ", unknown operation in line '" + trimmed + "'");
			return;
		}
		// keep consuming
		if (++index < lines.size()) {
			consume(lines, index, twoPoints, localVars);
		}
	}

}
