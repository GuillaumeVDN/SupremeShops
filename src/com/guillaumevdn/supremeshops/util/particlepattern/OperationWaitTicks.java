package com.guillaumevdn.supremeshops.util.particlepattern;

import java.util.Collection;

import org.bukkit.entity.Player;

public class OperationWaitTicks implements Operation {

	// base
	private String ticks;

	public OperationWaitTicks(String ticks) {
		this.ticks = ticks;
	}

	// get
	public String getTicks() {
		return ticks;
	}

	// methods
	@Override
	public int perform(ParticleScriptExecution execution, Collection<Player> players, boolean isAsync) {
		return (int) Math.ceil(execution.parseAndCalculate(ticks));
	}

}
