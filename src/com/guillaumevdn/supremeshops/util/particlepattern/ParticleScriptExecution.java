package com.guillaumevdn.supremeshops.util.particlepattern;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.bukkit.Location;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;

public class ParticleScriptExecution {

	// base
	private ParticleScript script;
	private int operationIndex;
	private int ticksToWait;
	private Map<String, Double> variables = new HashMap<String, Double>();// lower-case key
	private Location fixedBaseLocation;

	/**
	 * Initializes a new particle script execution
	 * Same as calling {@link #ParticleScriptExecution(ParticleScript, Location)} with a null fixedBaseLocation argument
	 * @param script the particle script
	 */
	public ParticleScriptExecution(ParticleScript script) {
		this(script, null);
	}

	/**
	 * Initializes a new particle script execution
	 * @param script the particle script
	 * @param fixedBaseLocation the fixed base location ; if this is null (because the location is volatile), you'll have to override method {@link #getBaseLocation()}
	 */
	public ParticleScriptExecution(ParticleScript script, Location fixedBaseLocation) {
		this.script = script;
		this.fixedBaseLocation = fixedBaseLocation;
		reset();
	}

	// get
	public Location getBaseLocation() {
		return fixedBaseLocation;
	}

	public int getOperationIndex() {
		return operationIndex;
	}

	public int getTicksToWait() {
		return ticksToWait;
	}

	public double getVariable(String name) {
		String lower = name.toLowerCase();
		return variables.containsKey(lower) ? variables.get(lower) : 0d;
	}

	// set
	public void setOperationIndex(int operationIndex) {
		this.operationIndex = operationIndex;
	}

	public void setTicksToWait(int ticksToWait) {
		this.ticksToWait = ticksToWait;
	}

	public void setVariable(String name, double value) {
		variables.put(name.toLowerCase(), value);
	}

	// methods
	public void reset() {
		// indexes
		operationIndex = -1;
		ticksToWait = 0;
		// variables
		variables.clear();
	}

	/**
	 * @param players the players to perform the action to, or null if none (all actions might not be affected by this parameter)
	 * @param isAsync true if the execution of actions should be async
	 * @return true if this execution is over
	 */
	public boolean update(Collection<Player> players, boolean isAsync) {
		// must wait
		if (--ticksToWait > 0) {
			return false;
		}
		// get base location (and end if invalid)
		Location baseLocation = getBaseLocation();
		if (baseLocation == null) {
			return true;
		}
		// reset base location variables (it might have moved)
		setVariable("baseX", baseLocation.getX());
		setVariable("baseY", baseLocation.getY());
		setVariable("baseZ", baseLocation.getZ());
		// execute operations
		for (;;) {
			// done for this execution
			if (++operationIndex >= script.getOperations().size()) {
				return true;
			}
			// perform next operation
			if ((ticksToWait = script.getOperations().get(operationIndex).perform(this, players, isAsync)) > 0) {
				return false;// must wait
			}
		}
	}

	public double parseAndCalculate(String raw) {
		String parsed = raw;
		for (String var : variables.keySet()) {
			parsed = parsed.replaceAll("(?i)\\b" + var + "\\b", "" + variables.get(var));
		}
		try {
			return Utils.calculateExpression(parsed);
		} catch (Throwable exception) {
			SupremeShops.inst().error("Couldn't parse mathematical expression '" + parsed + "' (raw '" + raw + "') (" + exception.getMessage() + ")");
			return 1d;
		}
	}

}
