package com.guillaumevdn.supremeshops.util.particlepattern;

import org.bukkit.OfflinePlayer;

import com.guillaumevdn.gcorelegacy.lib.Perm;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;

public class ParticlePattern {

	// base
	private String id;
	private String name;
	private Perm permission;
	private ParticlePatternAvailability availability;
	private ItemData icon;
	private ParticleScript script;

	public ParticlePattern(String id, String name, Perm permission, ParticlePatternAvailability availability, ItemData icon, ParticleScript script) {
		this.id = id;
		this.name = name;
		this.permission = permission;
		this.availability = availability;
		this.icon = icon;
		this.script = script;
	}

	// get
	public String getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public Perm getPermission() {
		return permission;
	}

	public boolean hasPermission(OfflinePlayer player) {
		return permission == null || permission.has(player);
	}

	public ParticlePatternAvailability getAvailability() {
		return availability;
	}

	public ItemData getIcon() {
		return icon;
	}

	public ParticleScript getScript() {
		return script;
	}

}
