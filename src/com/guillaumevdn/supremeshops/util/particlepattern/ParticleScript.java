package com.guillaumevdn.supremeshops.util.particlepattern;

import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;

public class ParticleScript {

	// base
	private String id;
	private List<Operation> operations;
	
	public ParticleScript(String id) {
		this(id, Utils.emptyList(Operation.class));
	}

	public ParticleScript(String id, List<Operation> operations) {
		this.id = id;
		this.operations = operations;
	}

	// get
	public String getId() {
		return id;
	}

	public List<Operation> getOperations() {
		return operations;
	}

	// overriden methods
	@Override
	public String toString() {
		return getId();
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ParticleScript other = (ParticleScript) obj;
		if (id == null) {
            return other.id == null;
		} else return id.equals(other.id);
    }

}
