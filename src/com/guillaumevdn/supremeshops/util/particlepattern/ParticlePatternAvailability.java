package com.guillaumevdn.supremeshops.util.particlepattern;

public enum ParticlePatternAvailability {

	BLOCK,
	SIGN,
	ENTITY

}
