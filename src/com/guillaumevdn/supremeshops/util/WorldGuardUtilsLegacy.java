package com.guillaumevdn.supremeshops.util;

import org.bukkit.Location;
import org.bukkit.World;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.regions.ProtectedRegion;

public class WorldGuardUtilsLegacy {

	public static boolean isLocInRegion(Location location, World regionWorld, String regionId) {
		for (ProtectedRegion region : ((WorldGuardPlugin) Utils.getPlugin("WorldGuard")).getRegionManager(regionWorld).getApplicableRegions(location)) {
			if (region.getId().equals(regionId)) {
				return true;
			}
		}
		return false;
	}

}
