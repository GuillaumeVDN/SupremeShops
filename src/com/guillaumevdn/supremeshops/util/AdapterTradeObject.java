package com.guillaumevdn.supremeshops.util;

import java.io.IOException;
import java.util.UUID;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.TypeAdapter;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonToken;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;

public class AdapterTradeObject extends TypeAdapter<TradeObject> {

	@Override
	public void write(JsonWriter out, TradeObject value) throws IOException {
		if (value == null) {
			out.nullValue();
		} else {
			// begin object
			out.beginObject();
			// object type and side
			out.name("type").value(value.getType().getId());
			out.name("can_edit_side").value(value.getCanEditSide());
			out.name("side").value(value.getSide().name());
			// object properties
			value.getType().getLogic().writeJsonProperties(out, value);
			// end object
			out.endObject();
		}
	}

	@Override
	public TradeObject read(JsonReader in) throws IOException {
		if (in.peek().equals(JsonToken.NULL)) {
			in.nextNull();
			return null;
		} else {
			// begin object
			in.beginObject();
			// object type and side
			in.nextName();
			ObjectType type = ObjectType.valueOf(in.nextString());
			in.nextName();
			boolean canEditSide = in.nextBoolean();
			in.nextName();
			ObjectSide side = Utils.valueOfOrNull(ObjectSide.class, in.nextString());
			// object properties
			TradeObject object = type.getLogic().readJsonPropertiesAndBuild(in, null, UUID.randomUUID().toString().replace("-", "").substring(0, 5), side, canEditSide);
			// end object
			in.endObject();
			// return object
			return object;
		}
	}

}
