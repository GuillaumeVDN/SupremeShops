package com.guillaumevdn.supremeshops.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonReader;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonToken;
import com.guillaumevdn.gcorelegacy.libs.com.google.gson.stream.JsonWriter;

public class JsonUtils {

	public static void writeJsonValue(JsonWriter out, String name, List<String> value) throws IOException {
		// name
		out.name(name);
		// null
		if (value == null) {
			out.value("$NULL_VALUE");
		}
		// empty
		else if (value.isEmpty()) {
			out.value("$EMPTY_VALUE");
		}
		// single
		else if (value.size() == 1) {
			out.value(value.get(0));
		}
		// list
		else if (value.size() > 1) {
			out.beginArray();
			for (String val : value) {
				out.value(val);
			}
			out.endArray();
		}
	}

	public static List<String> readJsonValueIfName(String name, JsonReader in) throws IOException {
		if (in.nextName().equalsIgnoreCase(name)) {
			return readJsonValue(in);
		}
		return null;
	}

	public static List<String> readJsonValue(JsonReader in) throws IOException {
		// name
		if (in.peek().equals(JsonToken.NAME)) in.nextName();
		// list
		List<String> value;
		if (in.peek().equals(JsonToken.BEGIN_ARRAY)) {
			value = new ArrayList<String>();
			in.beginArray();
			while (!in.peek().equals(JsonToken.END_ARRAY)) {
				value.add(in.nextString());
			}
			in.endArray();
		}
		else {
			// null
			String val = in.nextString();
			if (val.equals("$NULL_VALUE")) {
				value = null;
			}
			// empty
			else if (val.equals("$EMPTY_VALUE")) {
				value = Utils.emptyList();
			}
			// single
			else {
				value = Utils.asList(val);
			}
		}
		// set value
		return value;
	}

}
