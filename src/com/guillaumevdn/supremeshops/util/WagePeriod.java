package com.guillaumevdn.supremeshops.util;

public enum WagePeriod {
	
	DAILY,
	WEEKLY,
	MONTHLY

}
