package com.guillaumevdn.supremeshops;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.bukkit.Location;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.gui.GUI;
import com.guillaumevdn.gcorelegacy.lib.gui.ItemData;
import com.guillaumevdn.gcorelegacy.lib.npc.Npc;
import com.guillaumevdn.gcorelegacy.lib.util.Pair;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.gui.manageable.management.ManageableGUI;
import com.guillaumevdn.supremeshops.gui.merchant.MerchantGUI;
import com.guillaumevdn.supremeshops.gui.particlepatternassignable.ParticlePatternAssignableGUI;
import com.guillaumevdn.supremeshops.gui.rentable.RentableGUI;
import com.guillaumevdn.supremeshops.gui.shop.ShopGUI;
import com.guillaumevdn.supremeshops.gui.shop.TradePreviewGUI;
import com.guillaumevdn.supremeshops.module.display.items.ItemsDisplayable;
import com.guillaumevdn.supremeshops.module.display.items.ItemsDisplayableManager;
import com.guillaumevdn.supremeshops.module.display.particles.DisplayParticlesManager;
import com.guillaumevdn.supremeshops.module.display.sign.SignDisplayableManager;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.manageable.MerchantManagementPermission;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchant;
import com.guillaumevdn.supremeshops.module.merchant.NpcMerchantSkin;
import com.guillaumevdn.supremeshops.module.object.ObjectType;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.object.type.ObjectItem;
import com.guillaumevdn.supremeshops.module.particlepatternassignable.ParticlePatternAssignable;
import com.guillaumevdn.supremeshops.module.playertrade.PlayerTrade;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class GeneralManager {

	// ------------------------------------------------------------------------------------------------------------------------------------------------------
	//		SHOP DISPLAYS
	// ------------------------------------------------------------------------------------------------------------------------------------------------------

	private ItemsDisplayableManager itemsDisplayableManager = null;
	private SignDisplayableManager signDisplayableManager = null;
	private DisplayParticlesManager displayParticlesManager = null;

	// get
	public ItemsDisplayableManager getItemsDisplayableManager() {
		return itemsDisplayableManager;
	}

	public SignDisplayableManager getSignDisplayableManager() {
		return signDisplayableManager;
	}

	public DisplayParticlesManager getDisplayParticlesManager() {
		return displayParticlesManager;
	}

	// methods
	public void restartDisplaysManagers() {
		// stop previous
		stopDisplayManagers();
		// restart new ones
		if (itemsDisplayableManager == null && SupremeShops.inst().getConfiguration().getBoolean("enable_items_display", true)) {
			itemsDisplayableManager = new ItemsDisplayableManager();
			itemsDisplayableManager.restart();
		}
		if (displayParticlesManager == null) {
			displayParticlesManager = new DisplayParticlesManager();
			displayParticlesManager.restartTasks();
		}
		if (signDisplayableManager == null) {
			signDisplayableManager = new SignDisplayableManager();
		}
	}

	public void stopDisplayManagers() {
		if (itemsDisplayableManager != null) {
			itemsDisplayableManager.stop();
			itemsDisplayableManager = null;
		}
		if (signDisplayableManager != null) {
			signDisplayableManager = null;
		}
		if (displayParticlesManager != null) {
			displayParticlesManager.stopTasks();
			displayParticlesManager = null;
		}
	}

	public void cleanDisplayItems(ItemsDisplayable displayable) {
		for (Item item : displayable.getItemsDisplayBase().getWorld().getEntitiesByClass(Item.class)) {
			if (item.getCustomName() != null && item.isCustomNameVisible()) {
				if (item.getItemStack() != null && item.getItemStack().getItemMeta() != null && (ItemsDisplayableManager.ITEM_MARK_BEGIN + displayable.getId()).equals(item.getItemStack().getItemMeta().getDisplayName())) {
					item.remove();
				}
			}
		}
	}

	// ------------------------------------------------------------------------------------------------------------------------------------------------------
	//		MERCHANTS
	// ------------------------------------------------------------------------------------------------------------------------------------------------------

	private Map<String, Integer> merchantsNpcsIds = new HashMap<String, Integer>();
	private Set<Integer> allMerchantsNpcsIds = new HashSet<Integer>();

	// get
	public Map<String, Integer> getMerchantsNpcsIds() {
		return merchantsNpcsIds;
	}

	public Set<Integer> getAllMerchantsNpcsIds() {
		return allMerchantsNpcsIds;
	}

	public int getMerchantNpcId(NpcMerchant merchant) {
		String merchantId = merchant.getId().toLowerCase();
		Integer npcId = merchantsNpcsIds.get(merchantId);
		if (npcId == null) {
			npcId = NpcMerchant.NPC_ID_BASE;
			while (!allMerchantsNpcsIds.add(++npcId));
			merchantsNpcsIds.put(merchantId, npcId);
		}
		return npcId;
	}

	public NpcMerchant getMerchantByNpc(Npc npc) {
		for (String merchantId : merchantsNpcsIds.keySet()) {
			if (merchantsNpcsIds.get(merchantId) == npc.getId()) {
				return (NpcMerchant) SupremeShops.inst().getData().getMerchants().getElement(merchantId);
			}
		}
		return null;
	}

	// methods
	public void spawnMerchantsNpcs() {
		for (Player player : Utils.getOnlinePlayers()) {
			spawnMerchantsNpcs(player);
		}
	}

	public void spawnMerchantsNpcs(Player player) {
		for (Merchant m : SupremeShops.inst().getData().getMerchants().getAll(Utils.asList(NpcMerchant.class), false, ElementRemotePolicy.MIGHT_BE)) {
			NpcMerchant merchant = (NpcMerchant) m;
			int npcId = getMerchantNpcId(merchant);
			Npc npc = GCoreLegacy.inst().getNpcManager().getNpc(player, npcId);
			if (npc == null) {
				Location location = merchant.getLocation();
				NpcMerchantSkin skin = SupremeShops.inst().getModuleManager().getMerchantSkin(merchant.getSkinId());
				if (location != null) {
					GCoreLegacy.inst().getNpcManager().spawnNpc(player, npcId, merchant.getDisplayName(), skin == null ? null : skin.getData(), skin == null ? null : skin.getSignature(), location, 5d, Utils.asSet(merchant.getStatus()), merchant.getNpcItems());
				}
			}
		}
	}

	public void spawnMerchantNpcs(NpcMerchant merchant) {
		for (Player player : Utils.getOnlinePlayers()) {
			int npcId = getMerchantNpcId(merchant);
			Npc npc = GCoreLegacy.inst().getNpcManager().getNpc(player, npcId);
			if (npc == null) {
				Location location = merchant.getLocation();
				NpcMerchantSkin skin = SupremeShops.inst().getModuleManager().getMerchantSkin(merchant.getSkinId());
				if (location != null) {
					GCoreLegacy.inst().getNpcManager().spawnNpc(player, npcId, merchant.getDisplayName(), skin == null ? null : skin.getData(), skin == null ? null : skin.getSignature(), location, 5d, Utils.asSet(merchant.getStatus()), merchant.getNpcItems());
				}
			}
		}
	}

	public void removeMerchantsNpcs() {
		for (Player player : Utils.getOnlinePlayers()) {
			removeMerchantsNpcs(player);
		}
	}

	public void removeMerchantsNpcs(Player player) {
		for (Merchant m : SupremeShops.inst().getData().getMerchants().getAll(Utils.asList(NpcMerchant.class), false, ElementRemotePolicy.MIGHT_BE)) {
			NpcMerchant merchant = (NpcMerchant) m;
			int npcId = getMerchantNpcId(merchant);
			Npc npc = GCoreLegacy.inst().getNpcManager().getNpc(player, npcId);
			if (npc != null) {
				GCoreLegacy.inst().getNpcManager().removeNpc(player, npc);
			}
		}
	}

	public void removeMerchantNpc(NpcMerchant merchant) {
		for (Player player : Utils.getOnlinePlayers()) {
			Npc npc = GCoreLegacy.inst().getNpcManager().getNpc(player, getMerchantNpcId(merchant));
			if (npc != null) {
				GCoreLegacy.inst().getNpcManager().removeNpc(player, npc);
			}
		}
	}

	// ------------------------------------------------------------------------------------------------------------------------------------------------------
	//		PLAYER TRADES
	// ------------------------------------------------------------------------------------------------------------------------------------------------------

	private List<PlayerTrade> playerTrades = new ArrayList<PlayerTrade>();
	private Map<Player, List<Player>> askedPlayerTrades = new HashMap<Player, List<Player>>();// key = target, value = who asked

	// get
	public List<PlayerTrade> getPlayerTrades() {
		return playerTrades;
	}

	public Map<Player, List<Player>> getAskedPlayerTrades() {
		return askedPlayerTrades;
	}

	public List<Player> getAskedPlayerTrades(Player target) {
		List<Player> whoAsked = askedPlayerTrades.get(target);
		if (whoAsked == null) askedPlayerTrades.put(target, whoAsked = new ArrayList<Player>());
		return whoAsked;
	}

	public PlayerTrade getPlayerTrade(Player player) {
		for (PlayerTrade trade : playerTrades) {
			if (player.equals(trade.getPlayer1()) || player.equals(trade.getPlayer2())) {
				return trade;
			}
		}
		return null;
	}

	// methods
	public void startTrade(Player player1, Player player2) {
		// cancel current trades
		PlayerTrade trade = getPlayerTrade(player1);
		if (trade != null) trade.cancel();
		trade = getPlayerTrade(player2);
		if (trade != null) trade.cancel();
		// start new one
		trade = new PlayerTrade(player1, player2);
		trade.start();
		playerTrades.add(trade);
	}

	public void cancelPlayerTrades() {
		for (PlayerTrade trade : Utils.asList(playerTrades)) {
			trade.cancel();
		}
		playerTrades.clear();
	}

	// ------------------------------------------------------------------------------------------------------------------------------------------------------
	//		MISC
	// ------------------------------------------------------------------------------------------------------------------------------------------------------

	private Map<Player, Pair<UserInfo, Set<ShopManagementPermission>>> copyingShopManagerPermissions = new HashMap<Player, Pair<UserInfo, Set<ShopManagementPermission>>>();
	private Map<Player, Pair<UserInfo, Set<MerchantManagementPermission>>> copyingMerchantManagerPermissions = new HashMap<Player, Pair<UserInfo, Set<MerchantManagementPermission>>>();
	private List<Player> multiRestockingAll = new ArrayList<Player>();
	private List<Player> multiWithdrawingAllBenefits = new ArrayList<Player>();
	private List<Player> multiWithdrawingAllStock = new ArrayList<Player>();

	// get
	public Pair<UserInfo, Set<ShopManagementPermission>> getCopyingShopManagerPermissions(Player player) {
		return copyingShopManagerPermissions.get(player);
	}

	public Pair<UserInfo, Set<MerchantManagementPermission>> getCopyingMerchantManagerPermissions(Player player) {
		return copyingMerchantManagerPermissions.get(player);
	}

	public List<Player> getMultiRestockingAll() {
		return multiRestockingAll;
	}

	public List<Player> getMultiWithdrawingAllBenefits() {
		return multiWithdrawingAllBenefits;
	}

	public List<Player> getMultiWithdrawingAllStock() {
		return multiWithdrawingAllStock;
	}

	// set
	public void setCopyingShopManagerPermissions(Player player, UserInfo manager, Set<ShopManagementPermission> permissions) {
		if (manager == null || permissions == null) {
			copyingShopManagerPermissions.remove(player);
		} else {
			copyingShopManagerPermissions.put(player, new Pair<UserInfo, Set<ShopManagementPermission>>(manager, permissions));
		}
	}

	public void setCopyingMerchantManagerPermissions(Player player, UserInfo manager, Set<MerchantManagementPermission> permissions) {
		if (manager == null || permissions == null) {
			copyingMerchantManagerPermissions.remove(player);
		} else {
			copyingMerchantManagerPermissions.put(player, new Pair<UserInfo, Set<MerchantManagementPermission>>(manager, permissions));
		}
	}

	// methods
	public void updateShopsWithItems(Collection<ItemData> items, Shop originalShop, Player originalPlayer) {
		// find shops
		List<Shop> shops = Utils.asList(originalShop);
		for (Shop shop : SupremeShops.inst().getData().getShops().getAll().values()) {
			objects:for (TradeObject object : shop.getObjects(null, Utils.asList(ObjectType.ITEM))) {
				for (ItemData item : items) {
					if (((ObjectItem) object).getItem().getType(originalPlayer).equals(item.getType(), false)) {
						shops.add(shop);
						break objects;
					}
				}
			}
		}
		// process shops
		for (Shop shop : shops) {
			// update display
			if (shop instanceof ItemsDisplayable) {
				SupremeShops.inst().getGeneralManager().cleanDisplayItems((ItemsDisplayable) shop);
			}
			// update GUIs
			for (GUI gui : GUI.getRegisteredGUIs()) {
				if (gui instanceof TradePreviewGUI) {
					TradePreviewGUI previewGui = (TradePreviewGUI) gui;
					if (shops.contains(previewGui.getShop())) {
						previewGui.refreshRows();
						if (SupremeShops.inst().getModuleManager().getOtherPlayerTradeSound() != null) {
							for (Player pl : previewGui.getViewers()) {
								if (!pl.equals(originalPlayer)) {
									SupremeShops.inst().getModuleManager().getOtherPlayerTradeSound().play(pl);
								}
							}
						}
					}
				}
			}
		}
	}

	public void closeRelatedGUIs(Object object, boolean onlyTradePreview) {
		// only trade preview
		if (onlyTradePreview) {
			for (GUI gui : Utils.asList(GUI.getRegisteredGUIs())) {
				if (gui instanceof TradePreviewGUI && (Utils.equals(((TradePreviewGUI) gui).getMerchant(), object) || Utils.equals(((TradePreviewGUI) gui).getShop(), object))) {
					gui.unregister();
				}
			}
		}
		// all GUIs
		else {
			if (object instanceof Shop || object instanceof Merchant || object instanceof Rentable || object instanceof Manageable || object instanceof ParticlePatternAssignable) {
				for (GUI gui : Utils.asList(GUI.getRegisteredGUIs())) {
					if ((gui instanceof ShopGUI && Utils.equals(((ShopGUI) gui).getShop(), object)) || (gui instanceof MerchantGUI && Utils.equals(((MerchantGUI) gui).getMerchant(), object)) || (gui instanceof ManageableGUI && Utils.equals(((ManageableGUI) gui).getManageable(), object)) || (gui instanceof RentableGUI && Utils.equals(((RentableGUI) gui).getRentable(), object)) || (gui instanceof ParticlePatternAssignableGUI && Utils.equals(((ParticlePatternAssignableGUI) gui).getParticlePatternAssignable(), object))) {
						gui.unregister();
					}
				}
			}
		}
	}

}
