package com.guillaumevdn.supremeshops.listeners;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryOpenEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.inventory.ItemStack;

import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.SSUser;

public class ItemDiscoverListeners implements Listener {

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void event(final InventoryOpenEvent event) {
		if (!SupremeShops.inst().getForceItemDiscover()) {
			return;
		}
		SSUser user = SSUser.get(event.getPlayer());
		if (user != null) {
			Set<Mat> materials = new HashSet<Mat>();
			for (ItemStack item : event.getInventory().getContents()) {
				Mat mat = Mat.fromItem(item);
				if (!mat.isAir()) {
					materials.add(mat);
				}
			}
			if (!materials.isEmpty()) {
				user.setMaterialsDiscovered(materials, true);
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void event(InventoryClickEvent event) {
		if (!SupremeShops.inst().getForceItemDiscover()) {
			return;
		}
		Mat mat = Mat.fromItem(event.getCurrentItem());
		if (!mat.isAir()) {
			SSUser user = SSUser.get(event.getWhoClicked());
			if (user != null) {
				user.setMaterialDiscovered(mat, true);
			}
		}
	}

	@EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
	public void event(PlayerPickupItemEvent event) {
		if (!SupremeShops.inst().getForceItemDiscover()) {
			return;
		}
		Mat mat = Mat.fromItem(event.getItem().getItemStack());
		if (!mat.isAir()) {
			SSUser user = SSUser.get(event.getPlayer());
			if (user != null) {
				user.setMaterialDiscovered(mat, true);
			}
		}
	}

}
