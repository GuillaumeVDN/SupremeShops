package com.guillaumevdn.supremeshops.listeners;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.entity.TNTPrimed;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockBurnEvent;
import org.bukkit.event.block.BlockExplodeEvent;
import org.bukkit.event.block.BlockFadeEvent;
import org.bukkit.event.block.BlockFormEvent;
import org.bukkit.event.block.BlockFromToEvent;
import org.bukkit.event.block.BlockGrowEvent;
import org.bukkit.event.block.BlockPistonExtendEvent;
import org.bukkit.event.block.BlockPistonRetractEvent;
import org.bukkit.event.block.BlockRedstoneEvent;
import org.bukkit.event.block.BlockSpreadEvent;
import org.bukkit.event.block.LeavesDecayEvent;
import org.bukkit.event.block.MoistureChangeEvent;
import org.bukkit.event.entity.EntityExplodeEvent;
import org.bukkit.event.inventory.InventoryPickupItemEvent;
import org.bukkit.event.inventory.InventoryType;

import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.PlayerShop;
import com.guillaumevdn.supremeshops.util.DestroyCause;

/**
 * @author GuillaumeVDN
 */
public class DestructionListeners implements Listener {

	// prevent shops from being destroyed

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(BlockBreakEvent event) {
		if (!attemptToDestroy(event.getPlayer(), new BlockCoords(event.getBlock()))) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(BlockBurnEvent event) {
		if (!attemptToDestroy(null, new BlockCoords(event.getBlock()))) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(BlockFadeEvent event) {
		if (!attemptToDestroy(null, new BlockCoords(event.getBlock()))) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(BlockGrowEvent event) {
		if (!attemptToDestroy(null, new BlockCoords(event.getBlock()))) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(BlockFormEvent event) {
		if (!attemptToDestroy(null, new BlockCoords(event.getBlock()))) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(BlockFromToEvent event) {
		if (!attemptToDestroy(null, new BlockCoords(event.getBlock()))) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(BlockPistonExtendEvent event) {
		// the piston itself is a shop
		if (!attemptToDestroy(null, new BlockCoords(event.getBlock()))) {
			event.setCancelled(true);
		}
		// other blocks are a shop
		else {
			for (Block block : Utils.asList(event.getBlocks())) {
				if (!attemptToDestroy(null, new BlockCoords(block))) {
					event.setCancelled(true);
					break;
				}
			}
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(BlockPistonRetractEvent event) {
		// the piston itself is a shop
		if (!attemptToDestroy(null, new BlockCoords(event.getBlock()))) {
			event.setCancelled(true);
		}
		// other blocks are a shop
		else {
			for (Block block : Utils.asList(event.getBlocks())) {
				if (!attemptToDestroy(null, new BlockCoords(block))) {
					event.setCancelled(true);
					break;
				}
			}
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(BlockRedstoneEvent event) {
		if (!attemptToDestroy(null, new BlockCoords(event.getBlock()))) {
			event.setNewCurrent(event.getOldCurrent());
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(BlockSpreadEvent event) {
		if (!attemptToDestroy(null, new BlockCoords(event.getBlock()))) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(LeavesDecayEvent event) {// This because it might be a custom leaves chest block for the shop
		if (!attemptToDestroy(null, new BlockCoords(event.getBlock()))) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(MoistureChangeEvent event) {// This because it might be a custom moisture chest block for the shop
		if (!attemptToDestroy(null, new BlockCoords(event.getBlock()))) {
			event.setCancelled(true);
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(EntityExplodeEvent event) {
		// get player (if tnt)
		TNTPrimed tnt = Utils.instanceOf(event.getEntity(), TNTPrimed.class) ? (TNTPrimed) event.getEntity() : null;
		Player player = tnt != null && Utils.instanceOf(tnt.getSource(), Player.class) ? (Player) tnt.getSource() : null;
		// check blocks
		for (Block block : Utils.asList(event.blockList())) {
			if (!attemptToDestroy(player, new BlockCoords(block))) {
				event.blockList().remove(block);
			}
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(BlockExplodeEvent event) {
		for (Block block : Utils.asList(event.blockList())) {
			if (!attemptToDestroy(null, new BlockCoords(block))) {
				event.blockList().remove(block);
			}
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(InventoryPickupItemEvent event) {// Display items being picked up
		if (event.getInventory().getType().equals(InventoryType.HOPPER) && SupremeShops.inst().getGeneralManager().getItemsDisplayableManager() != null && SupremeShops.inst().getGeneralManager().getItemsDisplayableManager().isDisplayItem(event.getItem())) {
			event.setCancelled(true);
		}
	}

	/**
	 * @return false if the event should be cancelled
	 */
	private boolean attemptToDestroy(Player player, BlockCoords coords) {
		// shop
		PlayerShop shop = SupremeShops.inst().getData().getShops().getPhysicalElementLinkedToBlock(coords);
		if (shop != null) {
			// if it's a player, then he might be allowed to destroy it
			if (player != null && SupremeShops.inst().getModuleManager().canShopBeDestroyed(player, shop, true)) {
				shop.destroy(DestroyCause.ALLOWED_BREAK, true);
			}
			// not a player, can't destroy it
			return false;
		}
		// merchant
		Merchant merchant = SupremeShops.inst().getData().getMerchants().getPhysicalElementLinkedToBlock(coords);
		if (merchant != null) {
			// if it's a player, then he might be allowed to destroy it
			if (player != null && SupremeShops.inst().getModuleManager().canMerchantBeDestroyed(player, merchant, true)) {
				merchant.destroy(DestroyCause.ALLOWED_BREAK, true);
			}
			// not a player, can't destroy it
			return false;
		}
		return true;
	}

}
