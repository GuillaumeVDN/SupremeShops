package com.guillaumevdn.supremeshops.listeners;

import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;
import org.bukkit.event.player.PlayerInteractEvent;

import com.guillaumevdn.gcorelegacy.lib.event.NpcInteractEvent;
import com.guillaumevdn.gcorelegacy.lib.npc.Npc;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.gui.SSGui;
import com.guillaumevdn.supremeshops.module.trigger.type.AdminShopTriggerBlock;
import com.guillaumevdn.supremeshops.module.trigger.type.AdminShopTriggerEntity;
import com.guillaumevdn.supremeshops.module.trigger.type.AdminShopTriggerNpc;
import com.guillaumevdn.supremeshops.module.trigger.type.AdminShopTriggerPreciseEntity;

public class TriggersListeners implements Listener {

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(PlayerInteractEvent event) {
		if (event.getClickedBlock() != null) {
			Block block = event.getClickedBlock();
			for (AdminShopTriggerBlock trigger : SupremeShops.inst().getModuleManager().getTriggersByClass(AdminShopTriggerBlock.class)) {
				Player player = event.getPlayer();
				if (block.getLocation().equals(trigger.getBlock(player))) {
					if (trigger.areTriggerConditionsValid(player, true)) {
						SSGui gui = trigger.getGui(player);
						if (gui != null) {
							gui.build(player).open(player);
						} else {
							SupremeShops.inst().warning("Couldn't find GUI with id " + trigger.getGuiId(player) + " for trigger " + trigger.getId());
						}
					}
					return;
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.HIGHEST, ignoreCancelled = true)
	public void event(PlayerInteractEntityEvent event) {
		Entity entity = event.getRightClicked();
		if (entity != null) {
			// entity triggers
			if (entity.getCustomName() != null) {
				for (AdminShopTriggerEntity trigger : SupremeShops.inst().getModuleManager().getTriggersByClass(AdminShopTriggerEntity.class)) {
					Player player = event.getPlayer();
					if (entity.getCustomName().equals(trigger.getEntityName(player))) {
						if (trigger.areTriggerConditionsValid(player, true)) {
							SSGui gui = trigger.getGui(player);
							if (gui != null) {
								gui.build(player).open(player);
							} else {
								SupremeShops.inst().warning("Couldn't find GUI with id " + trigger.getGuiId(player) + " for trigger " + trigger.getId());
							}
						}
						return;
					}
				}
			}
			// precise entity triggers
			for (AdminShopTriggerPreciseEntity trigger : SupremeShops.inst().getModuleManager().getTriggersByClass(AdminShopTriggerPreciseEntity.class)) {
				Player player = event.getPlayer();
				if (entity.getUniqueId().equals(trigger.getEntityUniqueId(player))) {
					if (trigger.areTriggerConditionsValid(player, true)) {
						SSGui gui = trigger.getGui(player);
						if (gui != null) {
							gui.build(player).open(player);
						} else {
							SupremeShops.inst().warning("Couldn't find GUI with id " + trigger.getGuiId(player) + " for trigger " + trigger.getId());
						}
					}
					return;
				}
			}
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(NpcInteractEvent event) {
		Npc npc = event.getNpc();
		for (AdminShopTriggerNpc trigger : SupremeShops.inst().getModuleManager().getTriggersByClass(AdminShopTriggerNpc.class)) {
			Player player = event.getPlayer();
			if (npc.getId() == trigger.getNpcId(player)) {
				if (trigger.areTriggerConditionsValid(player, true)) {
					SSGui gui = trigger.getGui(player);
					if (gui != null) {
						gui.build(player).open(player);
					} else {
						SupremeShops.inst().warning("Couldn't find GUI with id " + trigger.getGuiId(player) + " for trigger " + trigger.getId());
					}
				}
				return;
			}
		}
	}

}
