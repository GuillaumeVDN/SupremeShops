package com.guillaumevdn.supremeshops.listeners;

import java.util.Set;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.event.NpcAttackEvent;
import com.guillaumevdn.gcorelegacy.lib.event.NpcEvent;
import com.guillaumevdn.gcorelegacy.lib.event.NpcInteractEvent;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Pair;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.SSUser;
import com.guillaumevdn.supremeshops.gui.rentable.UnrentedGUI;
import com.guillaumevdn.supremeshops.gui.rentable.management.RentableManagementGUI;
import com.guillaumevdn.supremeshops.module.manageable.MerchantManagementPermission;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.util.ClickType;

public class MerchantsListeners implements Listener {

	// interaction with block merchants

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(PlayerInteractEvent event) {
		Block block = event.getClickedBlock();
		String action = event.getAction().toString();
		if (block != null && action.contains("CLICK_BLOCK")) {
			Merchant merchant = SupremeShops.inst().getData().getMerchants().getPhysicalElementLinkedToBlock(new BlockCoords(block));
			if (merchant != null) {
				event.setCancelled(true);
				Player player = event.getPlayer();
				ClickType click = action.contains("LEFT") ? ClickType.LEFT_CLICK : ClickType.RIGHT_CLICK;
				boolean sneak = event.getPlayer().isSneaking();
				// preview
				if (click.equals(SupremeShops.inst().getModuleManager().getPreviewClick()) && sneak == SupremeShops.inst().getModuleManager().getPreviewClickSneak()) {
					// shop is unrented
					if (merchant instanceof Rentable && !((Rentable) merchant).isRented()) {
						// edit admin
						if (SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
							new RentableManagementGUI((Rentable) merchant, player, null, -1).open(player);
							return;
						}
						// not rentable
						else if (!((Rentable) merchant).isRentable()) {
							SSLocale.MSG_SUPREMESHOPS_UNRENTABLEMERCHANT.send(player);
							return;
						} else {
							// can't rent (conditions)
							if (!((Rentable) merchant).areRentConditionsValid(player, true)) {
								return;
							}
							// streak delay
							if (((Rentable) merchant).getRentPeriodStreakLimitDelay() > 1) {
								long nextRent = SSUser.get(player).getLastRentEnd((Rentable) merchant) + Utils.getSecondsInMillis(((Rentable) merchant).getRentPeriodStreakLimitDelay() * 60);
								if (System.currentTimeMillis() < nextRent) {
									SSLocale.MSG_SUPREMESHOPS_CANTRENTMERCHANTSTREAKLIMITDELAY.send(player, "{time}", Utils.formatDurationMillis(nextRent - System.currentTimeMillis()));
									return;
								}
							}
							// open gui
							new UnrentedGUI((Rentable) merchant, null, player, null, -1).open(player);
						}
					}
					// another option
					else if (!merchant.getManagementPermissions(player).isEmpty() || (merchant.ensureOpenOrError(player) && merchant.areInteractConditionsValid(player, true)) || (merchant instanceof Rentable && SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player))) {
						merchant.buildGui(player, null, -1).open(player);
					}
				}
				// unknown click
				else {
					String previewClick = (SupremeShops.inst().getModuleManager().getPreviewClick() != null ? SupremeShops.inst().getModuleManager().getPreviewClick().getText().getLine() : "/") + (SupremeShops.inst().getModuleManager().getPreviewClickSneak() ? " + " + SSLocaleMisc.MISC_SUPREMESHOPS_SNEAK.getLine() : "");
					if (merchant.getCurrentOwner() != null ? player.equals(merchant.getCurrentOwner().toPlayer()) : SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
						SSLocale.MSG_SUPREMESHOPS_MERCHANTHELPEDIT.send(player, "{preview_click}", previewClick);
					} else {
						SSLocale.MSG_SUPREMESHOPS_MERCHANTHELP.send(player, "{preview_click}", previewClick);
					}
				}
			}
		}
	}

	// interaction/management of shops

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(NpcAttackEvent event) {
		clickEvent(event, ClickType.LEFT_CLICK);
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(NpcInteractEvent event) {
		clickEvent(event, ClickType.RIGHT_CLICK);
	}

	private void clickEvent(NpcEvent event, ClickType click) {
		Merchant merchant = SupremeShops.inst().getGeneralManager().getMerchantByNpc(event.getNpc());
		if (merchant != null) {
			// copy management permissions
			Player player = event.getPlayer();
			Pair<UserInfo, Set<MerchantManagementPermission>> copying = SupremeShops.inst().getGeneralManager().getCopyingMerchantManagerPermissions(player);
			if (copying != null) {
				if (merchant.hasManagementPermission(player, MerchantManagementPermission.EDIT_MANAGERS)) {
					merchant.changeManagementPermissions(copying.getA(), copying.getB());
					SSLocale.MSG_SUPREMESHOPS_COPIEDPERMISSIONS.send(player, "{player}", copying.getA().toOfflinePlayer().getName());
				} else {
					SSLocale.MSG_SUPREMESHOPS_MERCHANTNOTYOURS.send(player);
				}
				return;
			}
			// interaction with merchant
			else {
				boolean sneak = event.getPlayer().isSneaking();
				// preview
				if (click.equals(SupremeShops.inst().getModuleManager().getPreviewClick()) && sneak == SupremeShops.inst().getModuleManager().getPreviewClickSneak()) {
					// merchant is an unrented merchant
					if (merchant instanceof Rentable && !((Rentable) merchant).isRented()) {
						// edit admin
						if (SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
							new RentableManagementGUI((Rentable) merchant, player, null, -1).open(player);
							return;
						}
						// not rentable
						else if (!((Rentable) merchant).isRentable()) {
							SSLocale.MSG_SUPREMESHOPS_UNRENTABLEMERCHANT.send(player);
							return;
						} else {
							// can't rent (conditions)
							if (!((Rentable) merchant).areRentConditionsValid(player, true)) {
								return;
							}
							// streak delay
							if (((Rentable) merchant).getRentPeriodStreakLimitDelay() > 1) {
								long nextRent = SSUser.get(player).getLastRentEnd((Rentable) merchant) + Utils.getSecondsInMillis(((Rentable) merchant).getRentPeriodStreakLimitDelay() * 60);
								if (System.currentTimeMillis() < nextRent) {
									SSLocale.MSG_SUPREMESHOPS_CANTRENTMERCHANTSTREAKLIMITDELAY.send(player, "{time}", Utils.formatDurationMillis(nextRent - System.currentTimeMillis()));
									return;
								}
							}
							// open gui
							new UnrentedGUI((Rentable) merchant, null, player, null, -1).open(player);
						}
					}
					// another option
					else if (!merchant.getManagementPermissions(player).isEmpty() || (merchant.isOpen() && merchant.areInteractConditionsValid(player, true)) || (merchant instanceof Rentable && SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player))) {
						merchant.buildGui(player, null, -1).open(player);
					}
				}
				// unknown click
				else {
					String previewClick = (SupremeShops.inst().getModuleManager().getPreviewClick() != null ? SupremeShops.inst().getModuleManager().getPreviewClick().getText().getLine() : "/") + (SupremeShops.inst().getModuleManager().getPreviewClickSneak() ? " + " + SSLocaleMisc.MISC_SUPREMESHOPS_SNEAK.getLine() : "");
					if (merchant.getCurrentOwner() != null ? player.equals(merchant.getCurrentOwner().toPlayer()) : SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
						SSLocale.MSG_SUPREMESHOPS_MERCHANTHELPEDIT.send(player, "{preview_click}", previewClick);
					} else {
						SSLocale.MSG_SUPREMESHOPS_MERCHANTHELP.send(player, "{preview_click}", previewClick);
					}
				}
			}
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void eventLowest(PlayerInteractEvent event) {
		Block block = event.getClickedBlock();
		String action = event.getAction().toString();
		if (block != null && action.contains("CLICK_BLOCK")) {
			Merchant merchant = SupremeShops.inst().getData().getMerchants().getPhysicalElementLinkedToBlock(new BlockCoords(block));
			if (merchant != null) {
				// copy management permissions
				Player player = event.getPlayer();
				Pair<UserInfo, Set<MerchantManagementPermission>> copying = SupremeShops.inst().getGeneralManager().getCopyingMerchantManagerPermissions(player);
				if (copying != null) {
					event.setCancelled(true);
					if (merchant.hasManagementPermission(player, MerchantManagementPermission.EDIT_MANAGERS)) {
						merchant.changeManagementPermissions(copying.getA(), copying.getB());
						SSLocale.MSG_SUPREMESHOPS_COPIEDPERMISSIONS.send(player, "{player}", copying.getA().toOfflinePlayer().getName());
					} else {
						SSLocale.MSG_SUPREMESHOPS_MERCHANTNOTYOURS.send(player);
					}
					return;
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL /* normal because sneak cancel GUI delay is LOWEST*/, ignoreCancelled = true)
	public void event(PlayerToggleSneakEvent event) {
		// end copy management permissions
		Player player = event.getPlayer();
		Pair<UserInfo, Set<MerchantManagementPermission>> copying = SupremeShops.inst().getGeneralManager().getCopyingMerchantManagerPermissions(player);
		if (copying != null) {
			SupremeShops.inst().getGeneralManager().setCopyingMerchantManagerPermissions(player, null, null);
			SSLocale.MSG_SUPREMESHOPS_PERMISSIONSCOPYEND.send(player);
		}
	}

}
