package com.guillaumevdn.supremeshops.listeners;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.block.BlockFace;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.block.SignChangeEvent;

import com.guillaumevdn.gcorelegacy.GCoreLegacy;
import com.guillaumevdn.gcorelegacy.GLocale;
import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Handler;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.api.event.merchant.MerchantCreateEvent;
import com.guillaumevdn.supremeshops.api.event.shop.ShopCreateEvent;
import com.guillaumevdn.supremeshops.data.ShopBoard.ElementRemotePolicy;
import com.guillaumevdn.supremeshops.module.merchant.BlockMerchant;
import com.guillaumevdn.supremeshops.module.merchant.RentableBlockMerchant;
import com.guillaumevdn.supremeshops.module.merchant.RentableSignMerchant;
import com.guillaumevdn.supremeshops.module.merchant.SignMerchant;
import com.guillaumevdn.supremeshops.module.shop.BlockShop;
import com.guillaumevdn.supremeshops.module.shop.RentableBlockShop;
import com.guillaumevdn.supremeshops.module.shop.RentableSignShop;
import com.guillaumevdn.supremeshops.module.shop.SignShop;
import com.guillaumevdn.supremeshops.util.CreationTax;

/**
 * @author GuillaumeVDN
 */
public class CreationListeners implements Listener {

	private Map<Block, Block> awaitingAgainstSigns = new HashMap<Block, Block>();

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(BlockPlaceEvent event) {
		// placing a sign
		Block block = event.getBlock();
		Mat blockMat = Mat.fromBlock(block);
		if (blockMat.toString().contains("SIGN")) {
			// already a shop there
			Block against = event.getBlockAgainst();
			if (SupremeShops.inst().getData().getShops().getPhysicalElementLinkedToBlock(new BlockCoords(against)) != null) {
				event.setCancelled(true);
				return;
			}
			// already a merchant there
			if (SupremeShops.inst().getData().getMerchants().getPhysicalElementLinkedToBlock(new BlockCoords(against)) != null) {
				event.setCancelled(true);
				return;
			}
			// await
			awaitingAgainstSigns.put(block, against);
		}
		// chest or trapped chest
		else if (blockMat.equals(Mat.CHEST) || blockMat.equals(Mat.TRAPPED_CHEST)) {
			// there's a shop or a merchant around
			for (BlockFace blockFace : Utils.asList(BlockFace.EAST, BlockFace.WEST, BlockFace.NORTH, BlockFace.SOUTH)) {
				Block relative = block.getRelative(blockFace);
				if (Mat.fromBlock(relative).equals(blockMat) && (SupremeShops.inst().getData().getShops().getPhysicalElementLinkedToBlock(new BlockCoords(relative)) != null || SupremeShops.inst().getData().getMerchants().getPhysicalElementLinkedToBlock(new BlockCoords(relative)) != null)) {
					event.setCancelled(true);
					return;
				}
			}
		}
	}

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(SignChangeEvent event) {
		// get first line
		String line0 = event.getLine(0);
		if (line0 == null) return;
		line0 = line0.toLowerCase().trim();

		// ------------------------------------------------------------------------------------------------------------------------------------------------------
		// 												SIGN SHOPS
		// ------------------------------------------------------------------------------------------------------------------------------------------------------

		if (line0.equals(SupremeShops.inst().getConfiguration().getString("sign_input_signshop", "[signshop]").toLowerCase().trim())) {
			Block block = event.getBlock();
			// already a shop there
			if (SupremeShops.inst().getData().getShops().getPhysicalElementLinkedToBlock(new BlockCoords(block)) != null) {
				block.breakNaturally();
				return;
			}
			// already a merchant there
			if (SupremeShops.inst().getData().getMerchants().getPhysicalElementLinkedToBlock(new BlockCoords(block)) != null) {
				block.breakNaturally();
				return;
			}
			// rent sign shop
			Player player = event.getPlayer();
			String line1 = event.getLine(1) == null ? "" : event.getLine(1).trim().toLowerCase();
			if (line1.equals(SupremeShops.inst().getConfiguration().getString("sign_input_rent", "rent").toLowerCase().trim())) {
				// can't create admin
				if (!SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					event.getBlock().breakNaturally();
					return;
				}
				// create shop
				String shopId = "sign_" + block.getX() + "_" + block.getY() + "_" + block.getZ();
				final RentableSignShop shop = new RentableSignShop(shopId, new BlockCoords(block));
				// event
				ShopCreateEvent ev = new ShopCreateEvent(shop, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					block.breakNaturally();
					return;
				}
				// create
				SupremeShops.inst().getData().getShops().add(shop);
				SSLocale.MSG_SUPREMESHOPS_CREATESHOPRENT.send(player);
				SupremeShops.inst().pluginLog(shop, null, null, null, null, "Created shop");
				// update display later
				new Handler() {
					@Override
					public void execute() {
						SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(shop);
					}
				}.runSyncLater(1L);
			}
			// admin sign shop
			else if (line1.equals(SupremeShops.inst().getConfiguration().getString("sign_input_admin", "admin").toLowerCase().trim())) {
				// can't create admin
				if (!SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					event.getBlock().breakNaturally();
					return;
				}
				// create shop
				String shopId = "sign_" + block.getX() + "_" + block.getY() + "_" + block.getZ();
				final SignShop shop = new SignShop(shopId, null, new BlockCoords(block));
				// event
				ShopCreateEvent ev = new ShopCreateEvent(shop, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					block.breakNaturally();
					return;
				}
				// create
				SupremeShops.inst().getData().getShops().add(shop);
				SSLocale.MSG_SUPREMESHOPS_CREATESHOPADMIN.send(player);
				SupremeShops.inst().pluginLog(shop, null, null, null, null, "Created shop");
				// update display later
				new Handler() {
					@Override
					public void execute() {
						SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(shop);
					}
				}.runSyncLater(1L);
			}
			// regular sign shop
			else {
				// can't create
				if (!SSPerm.SUPREMESHOPS_SHOP_CREATE_SIGN.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					block.breakNaturally();
					return;
				}
				// max sign shops reached
				int amount = SupremeShops.inst().getData().getShops().getElements(new UserInfo(player), Utils.asList(SignShop.class), false, ElementRemotePolicy.MIGHT_BE).size();
				int max = SupremeShops.inst().getModuleManager().getMaxSignShops(player);
				if (amount >= max) {
					SSLocale.MSG_SUPREMESHOPS_MAXSIGNSHOPS.send(player);
					block.breakNaturally();
					return;
				}
				// max shops reached
				amount = SupremeShops.inst().getData().getShops().getElements(new UserInfo(player), null, false, ElementRemotePolicy.MIGHT_BE).size();
				max = SupremeShops.inst().getModuleManager().getMaxShops(player);
				if (amount >= max) {
					SSLocale.MSG_SUPREMESHOPS_MAXSHOPS.send(player);
					block.breakNaturally();
					return;
				}
				// create shop
				String shopId = "sign_" + block.getX() + "_" + block.getY() + "_" + block.getZ();
				final SignShop shop = new SignShop(shopId, new UserInfo(player), new BlockCoords(block));
				// isn't allowed
				if (!SupremeShops.inst().getModuleManager().canShopExist(player, shop, true)) {
					block.breakNaturally();
					return;
				}
				// can't pay taxes
				double taxMoney = 0d;
				for (CreationTax tax : SupremeShops.inst().getModuleManager().getShopCreationTaxes().values()) {
					if (tax.getConditions().isValid(player, shop, false)) {
						taxMoney += tax.getVaultMoneyTax(player);
					}
				}
				if (taxMoney > 0d && GCoreLegacy.inst().getEconomyHandler().get(player) < taxMoney) {
					block.breakNaturally();
					SSLocale.MSG_SUPREMESHOPS_NOTENOUGHMONEYCREATIONTAX.send(player, "{money}", Utils.round5(taxMoney));
					return;
				}
				// event
				ShopCreateEvent ev = new ShopCreateEvent(shop, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					block.breakNaturally();
					return;
				}
				// pay taxes
				if (taxMoney > 0d) {
					GCoreLegacy.inst().getEconomyHandler().take(player, taxMoney);
					SSLocale.MSG_SUPREMESHOPS_PAIDCREATIONTAX.send(player, "{money}", Utils.round5(taxMoney));
				}
				// create
				SupremeShops.inst().getData().getShops().add(shop);
				SSLocale.MSG_SUPREMESHOPS_CREATESHOP.send(player, "{amount}", amount + 1, "{max}", max);
				SupremeShops.inst().pluginLog(shop, null, null, null, null, "Created shop");
				// update display later
				new Handler() {
					@Override
					public void execute() {
						SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(shop);
					}
				}.runSyncLater(1L);
			}
		}

		// ------------------------------------------------------------------------------------------------------------------------------------------------------
		// 												BLOCK MERCHANTS
		// ------------------------------------------------------------------------------------------------------------------------------------------------------

		else if (line0.equals(SupremeShops.inst().getConfiguration().getString("sign_input_blockmerchant", "[merchant]").toLowerCase().trim())) {
			// not an awaiting sign
			Block block = event.getBlock();
			Block against = awaitingAgainstSigns.remove(block);
			if (against == null) {
				return;
			}
			// not a valid merchant block or can't use it
			if (!SupremeShops.inst().getModuleManager().canUseMerchantBlock(event.getPlayer(), Mat.fromBlock(against))) {
				block.breakNaturally();
				return;
			}
			// already a merchant there
			if (SupremeShops.inst().getData().getMerchants().getPhysicalElementLinkedToBlock(new BlockCoords(against)) != null) {
				block.breakNaturally();
				return;
			}
			// already a shop there
			if (SupremeShops.inst().getData().getShops().getPhysicalElementLinkedToBlock(new BlockCoords(block)) != null) {
				block.breakNaturally();
				return;
			}
			// rent block merchant
			Player player = event.getPlayer();
			String line1 = event.getLine(1) == null ? "" : event.getLine(1).replace(" ", "").trim().toLowerCase();
			if (line1.equals(SupremeShops.inst().getConfiguration().getString("sign_input_rent", "rent").toLowerCase().trim())) {
				// can't create admin
				if (!SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					block.breakNaturally();
					return;
				}
				// create
				String merchantId = "rent_block_" + against.getWorld().getName() + "_" + against.getX() + "_" + against.getY() + "_" + against.getZ();
				final RentableBlockMerchant merchant = new RentableBlockMerchant(merchantId, new BlockCoords(against), new BlockCoords(block));
				// event
				MerchantCreateEvent ev = new MerchantCreateEvent(merchant, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					block.breakNaturally();
					return;
				}
				// create
				SupremeShops.inst().getData().getMerchants().add(merchant);
				SSLocale.MSG_SUPREMESHOPS_CREATEMERCHANTRENT.send(player);
				SupremeShops.inst().pluginLog(null, merchant, null, null, null, "Created merchant");
				// update display later
				new Handler() {
					@Override
					public void execute() {
						SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(merchant);
					}
				}.runSyncLater(1L);
			}
			// admin block merchant
			else if (line1.equals(SupremeShops.inst().getConfiguration().getString("sign_input_admin", "admin").toLowerCase().trim())) {
				// can't create admin
				if (!SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					block.breakNaturally();
					return;
				}
				// create shop
				String merchantId = "block_" + against.getWorld().getName() + "_" + against.getX() + "_" + against.getY() + "_" + against.getZ();
				final BlockMerchant merchant = new BlockMerchant(merchantId, null, new BlockCoords(against), new BlockCoords(block));
				// event
				MerchantCreateEvent ev = new MerchantCreateEvent(merchant, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					block.breakNaturally();
					return;
				}
				// create
				SupremeShops.inst().getData().getMerchants().add(merchant);
				SSLocale.MSG_SUPREMESHOPS_CREATEMERCHANTADMIN.send(player);
				SupremeShops.inst().pluginLog(null, merchant, null, null, null, "Created merchant");
				// update display later
				new Handler() {
					@Override
					public void execute() {
						SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(merchant);
					}
				}.runSyncLater(1L);
			}
			// regular block shop
			else {
				// can't create
				if (!SSPerm.SUPREMESHOPS_MERCHANT_CREATE_BLOCK.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					block.breakNaturally();
					return;
				}
				// max block merchants reached
				int amount = SupremeShops.inst().getData().getMerchants().getElements(new UserInfo(player), Utils.asList(BlockMerchant.class), false, com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy.MIGHT_BE).size();
				int max = SupremeShops.inst().getModuleManager().getMaxBlockMerchants(player);
				if (amount >= max) {
					SSLocale.MSG_SUPREMESHOPS_MAXBLOCKMERCHANTS.send(player);
					block.breakNaturally();
					return;
				}
				// max merchants reached
				amount = SupremeShops.inst().getData().getMerchants().getElements(new UserInfo(player), null, false, com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy.MIGHT_BE).size();
				max = SupremeShops.inst().getModuleManager().getMaxMerchants(player);
				if (amount >= max) {
					SSLocale.MSG_SUPREMESHOPS_MAXMERCHANTS.send(player);
					block.breakNaturally();
					return;
				}
				// create
				String merchantId = "block_" + against.getWorld().getName() + "_" + against.getX() + "_" + against.getY() + "_" + against.getZ();
				final BlockMerchant merchant = new BlockMerchant(merchantId, new UserInfo(player), new BlockCoords(against), new BlockCoords(block));
				// isn't allowed
				if (!SupremeShops.inst().getModuleManager().canMerchantExist(player, merchant, true)) {
					block.breakNaturally();
					return;
				}
				// can't pay taxes
				double taxMoney = 0d;
				for (CreationTax tax : SupremeShops.inst().getModuleManager().getMerchantCreationTaxes().values()) {
					if (tax.getConditions().isValid(player, merchant, false)) {
						taxMoney += tax.getVaultMoneyTax(player);
					}
				}
				if (taxMoney > 0d && GCoreLegacy.inst().getEconomyHandler().get(player) < taxMoney) {
					block.breakNaturally();
					SSLocale.MSG_SUPREMESHOPS_NOTENOUGHMONEYCREATIONTAX.send(player, "{money}", Utils.round5(taxMoney));
					return;
				}
				// event
				MerchantCreateEvent ev = new MerchantCreateEvent(merchant, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					block.breakNaturally();
					return;
				}
				// pay taxes
				if (taxMoney > 0d) {
					GCoreLegacy.inst().getEconomyHandler().take(player, taxMoney);
					SSLocale.MSG_SUPREMESHOPS_PAIDCREATIONTAX.send(player, "{money}", Utils.round5(taxMoney));
				}
				// add
				SupremeShops.inst().getData().getMerchants().add(merchant);
				SSLocale.MSG_SUPREMESHOPS_CREATEMERCHANT.send(player, "{amount}", amount + 1, "{max}", max);
				SupremeShops.inst().pluginLog(null, merchant, null, null, null, "Created merchant");
				// update display later
				new Handler() {
					@Override
					public void execute() {
						SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(merchant);
					}
				}.runSyncLater(1L);
			}
		}

		// ------------------------------------------------------------------------------------------------------------------------------------------------------
		// 												BLOCK SHOPS
		// ------------------------------------------------------------------------------------------------------------------------------------------------------

		else if (line0.equals(SupremeShops.inst().getConfiguration().getString("sign_input_blockshop", "[shop]").toLowerCase().trim())) {
			// not an awaiting sign
			Block block = event.getBlock();
			Block against = awaitingAgainstSigns.remove(block);
			if (against == null) {
				return;
			}
			// already a shop there
			if (SupremeShops.inst().getData().getShops().getPhysicalElementLinkedToBlock(new BlockCoords(against)) != null) {
				block.breakNaturally();
				return;
			}
			// already a merchant there
			if (SupremeShops.inst().getData().getMerchants().getPhysicalElementLinkedToBlock(new BlockCoords(against)) != null) {
				block.breakNaturally();
				return;
			}
			// not a valid shop block or can't use it
			if (!SupremeShops.inst().getModuleManager().canUseShopBlock(event.getPlayer(), Mat.fromBlock(against))) {
				block.breakNaturally();
				return;
			}
			// rent block shop
			Player player = event.getPlayer();
			String line1 = event.getLine(1) == null ? "" : event.getLine(1).replace(" ", "").trim().toLowerCase();
			if (line1.equals(SupremeShops.inst().getConfiguration().getString("sign_input_rent", "rent").toLowerCase().trim())) {
				// can't create admin
				if (!SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					block.breakNaturally();
					return;
				}
				// create shop
				String shopId = "rent_block_" + against.getWorld().getName() + "_" + against.getX() + "_" + against.getY() + "_" + against.getZ();
				final RentableBlockShop shop = new RentableBlockShop(shopId, new BlockCoords(against), new BlockCoords(block));
				// event
				ShopCreateEvent ev = new ShopCreateEvent(shop, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					block.breakNaturally();
					return;
				}
				// create
				SupremeShops.inst().getData().getShops().add(shop);
				SSLocale.MSG_SUPREMESHOPS_CREATESHOPRENT.send(player);
				SupremeShops.inst().pluginLog(shop, null, null, null, null, "Created shop");
				// update display later
				new Handler() {
					@Override
					public void execute() {
						SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(shop);
					}
				}.runSyncLater(1L);
			}
			// admin block shop
			else if (line1.equals(SupremeShops.inst().getConfiguration().getString("sign_input_admin", "admin").toLowerCase().trim())) {
				// can't create admin
				if (!SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					block.breakNaturally();
					return;
				}
				// create shop
				String shopId = against.getWorld().getName() + "_" + against.getX() + "_" + against.getY() + "_" + against.getZ();
				final BlockShop shop = new BlockShop(shopId, null, new BlockCoords(against), new BlockCoords(block));
				// event
				ShopCreateEvent ev = new ShopCreateEvent(shop, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					block.breakNaturally();
					return;
				}
				// create
				SupremeShops.inst().getData().getShops().add(shop);
				SSLocale.MSG_SUPREMESHOPS_CREATESHOPADMIN.send(player);
				SupremeShops.inst().pluginLog(shop, null, null, null, null, "Created shop");
				// update display later
				new Handler() {
					@Override
					public void execute() {
						SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(shop);
					}
				}.runSyncLater(1L);
			}
			// regular block shop
			else {
				// can't create
				if (!SSPerm.SUPREMESHOPS_SHOP_CREATE_BLOCK.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					block.breakNaturally();
					return;
				}
				// max block shops reached
				int amount = SupremeShops.inst().getData().getShops().getElements(new UserInfo(player), Utils.asList(BlockShop.class), false, ElementRemotePolicy.MIGHT_BE).size();
				int max = SupremeShops.inst().getModuleManager().getMaxBlockShops(player);
				if (amount >= max) {
					SSLocale.MSG_SUPREMESHOPS_MAXBLOCKSHOPS.send(player);
					block.breakNaturally();
					return;
				}
				// max shops reached
				amount = SupremeShops.inst().getData().getShops().getElements(new UserInfo(player), null, false, ElementRemotePolicy.MIGHT_BE).size();
				max = SupremeShops.inst().getModuleManager().getMaxShops(player);
				if (amount >= max) {
					SSLocale.MSG_SUPREMESHOPS_MAXSHOPS.send(player);
					block.breakNaturally();
					return;
				}
				// create shop
				String shopId = "block_" + against.getWorld().getName() + "_" + against.getX() + "_" + against.getY() + "_" + against.getZ();
				final BlockShop shop = new BlockShop(shopId, new UserInfo(player), new BlockCoords(against), new BlockCoords(block));
				// isn't allowed
				if (!SupremeShops.inst().getModuleManager().canShopExist(player, shop, true)) {
					block.breakNaturally();
					return;
				}
				// can't pay taxes
				double taxMoney = 0d;
				for (CreationTax tax : SupremeShops.inst().getModuleManager().getShopCreationTaxes().values()) {
					if (tax.getConditions().isValid(player, shop, false)) {
						taxMoney += tax.getVaultMoneyTax(player);
					}
				}
				if (taxMoney > 0d && GCoreLegacy.inst().getEconomyHandler().get(player) < taxMoney) {
					block.breakNaturally();
					SSLocale.MSG_SUPREMESHOPS_NOTENOUGHMONEYCREATIONTAX.send(player, "{money}", Utils.round5(taxMoney));
					return;
				}
				// event
				ShopCreateEvent ev = new ShopCreateEvent(shop, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					block.breakNaturally();
					return;
				}
				// pay taxes
				if (taxMoney > 0d) {
					GCoreLegacy.inst().getEconomyHandler().take(player, taxMoney);
					SSLocale.MSG_SUPREMESHOPS_PAIDCREATIONTAX.send(player, "{money}", Utils.round5(taxMoney));
				}
				// create
				SupremeShops.inst().getData().getShops().add(shop);
				SSLocale.MSG_SUPREMESHOPS_CREATESHOP.send(player, "{amount}", amount + 1, "{max}", max);
				SupremeShops.inst().pluginLog(shop, null, null, null, null, "Created shop");
				// update display later
				new Handler() {
					@Override
					public void execute() {
						SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(shop);
					}
				}.runSyncLater(1L);
			}
		}

		// ------------------------------------------------------------------------------------------------------------------------------------------------------
		// 												SIGN MERCHANTS
		// ------------------------------------------------------------------------------------------------------------------------------------------------------

		else if (line0.equals(SupremeShops.inst().getConfiguration().getString("sign_input_signmerchant", "[signmerchant]").toLowerCase().trim())) {
			Block block = event.getBlock();
			// already a shop there
			if (SupremeShops.inst().getData().getShops().getPhysicalElementLinkedToBlock(new BlockCoords(block)) != null) {
				block.breakNaturally();
				return;
			}
			// already a merchant there
			if (SupremeShops.inst().getData().getMerchants().getPhysicalElementLinkedToBlock(new BlockCoords(block)) != null) {
				block.breakNaturally();
				return;
			}
			// rent sign merchant
			Player player = event.getPlayer();
			String line1 = event.getLine(1) == null ? "" : event.getLine(1).trim().toLowerCase();
			if (line1.equals(SupremeShops.inst().getConfiguration().getString("sign_input_rent", "rent").toLowerCase().trim())) {
				// can't create admin
				if (!SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					block.breakNaturally();
					return;
				}
				// create shop
				String merchantId = "sign_" + block.getX() + "_" + block.getY() + "_" + block.getZ();
				final RentableSignMerchant merchant = new RentableSignMerchant(merchantId, new BlockCoords(block));
				// event
				MerchantCreateEvent ev = new MerchantCreateEvent(merchant, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					block.breakNaturally();
					return;
				}
				// create
				SupremeShops.inst().getData().getMerchants().add(merchant);
				SSLocale.MSG_SUPREMESHOPS_CREATEMERCHANTRENT.send(player);
				SupremeShops.inst().pluginLog(null, merchant, null, null, null, "Created merchant");
				// update display later
				new Handler() {
					@Override
					public void execute() {
						SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(merchant);
					}
				}.runSyncLater(1L);
			}
			// admin sign merchant
			else if (line1.equals(SupremeShops.inst().getConfiguration().getString("sign_input_admin", "admin").toLowerCase().trim())) {
				// can't create admin
				if (!SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					event.getBlock().breakNaturally();
					return;
				}
				// create
				String merchantId = "sign_" + block.getX() + "_" + block.getY() + "_" + block.getZ();
				final SignMerchant merchant = new SignMerchant(merchantId, null, new BlockCoords(block));
				// event
				MerchantCreateEvent ev = new MerchantCreateEvent(merchant, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					block.breakNaturally();
					return;
				}
				// create
				SupremeShops.inst().getData().getMerchants().add(merchant);
				SSLocale.MSG_SUPREMESHOPS_CREATEMERCHANTADMIN.send(player);
				SupremeShops.inst().pluginLog(null, merchant, null, null, null, "Created shop");
				// update display later
				new Handler() {
					@Override
					public void execute() {
						SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(merchant);
					}
				}.runSyncLater(1L);
			}
			// regular sign shop
			else {
				// can't create
				if (!SSPerm.SUPREMESHOPS_MERCHANT_CREATE_SIGN.has(player)) {
					GLocale.MSG_GENERIC_NOPERMISSION.send(player, "{plugin}", SupremeShops.inst().getName());
					block.breakNaturally();
					return;
				}
				// max sign merchants reached
				int amount = SupremeShops.inst().getData().getMerchants().getElements(new UserInfo(player), Utils.asList(SignMerchant.class), false, com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy.MIGHT_BE).size();
				int max = SupremeShops.inst().getModuleManager().getMaxSignMerchants(player);
				if (amount >= max) {
					SSLocale.MSG_SUPREMESHOPS_MAXSIGNMERCHANTS.send(player);
					block.breakNaturally();
					return;
				}
				// max shops reached
				amount = SupremeShops.inst().getData().getShops().getElements(new UserInfo(player), null, false, ElementRemotePolicy.MIGHT_BE).size();
				max = SupremeShops.inst().getModuleManager().getMaxMerchants(player);
				if (amount >= max) {
					SSLocale.MSG_SUPREMESHOPS_MAXMERCHANTS.send(player);
					block.breakNaturally();
					return;
				}
				// create
				String merchantId = "sign_" + block.getX() + "_" + block.getY() + "_" + block.getZ();
				final SignMerchant merchant = new SignMerchant(merchantId, new UserInfo(player), new BlockCoords(block));
				// isn't allowed
				if (!SupremeShops.inst().getModuleManager().canMerchantExist(player, merchant, true)) {
					block.breakNaturally();
					return;
				}
				// can't pay taxes
				double taxMoney = 0d;
				for (CreationTax tax : SupremeShops.inst().getModuleManager().getMerchantCreationTaxes().values()) {
					if (tax.getConditions().isValid(player, merchant, false)) {
						taxMoney += tax.getVaultMoneyTax(player);
					}
				}
				if (taxMoney > 0d && GCoreLegacy.inst().getEconomyHandler().get(player) < taxMoney) {
					block.breakNaturally();
					SSLocale.MSG_SUPREMESHOPS_NOTENOUGHMONEYCREATIONTAX.send(player, "{money}", Utils.round5(taxMoney));
					return;
				}
				// event
				MerchantCreateEvent ev = new MerchantCreateEvent(merchant, player);
				Bukkit.getPluginManager().callEvent(ev);
				if (ev.isCancelled()) {
					block.breakNaturally();
					return;
				}
				// pay taxes
				if (taxMoney > 0d) {
					GCoreLegacy.inst().getEconomyHandler().take(player, taxMoney);
					SSLocale.MSG_SUPREMESHOPS_PAIDCREATIONTAX.send(player, "{money}", Utils.round5(taxMoney));
				}
				// create
				SupremeShops.inst().getData().getMerchants().add(merchant);
				SSLocale.MSG_SUPREMESHOPS_CREATEMERCHANT.send(player, "{amount}", amount + 1, "{max}", max);
				SupremeShops.inst().pluginLog(null, merchant, null, null, null, "Created shop");
				// update display later
				new Handler() {
					@Override
					public void execute() {
						SupremeShops.inst().getGeneralManager().getSignDisplayableManager().refreshSign(merchant);
					}
				}.runSyncLater(1L);
			}
		}
	}

}
