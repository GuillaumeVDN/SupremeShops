package com.guillaumevdn.supremeshops.listeners;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.SpongeAbsorbEvent;

import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.PlayerShop;
import com.guillaumevdn.supremeshops.util.DestroyCause;

/**
 * @author GuillaumeVDN
 */
public class DestructionSpongeListeners implements Listener {

	// prevent sponge shops from being destroyed

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(SpongeAbsorbEvent event) { // This because it might be a custom sponge chest block for the shop
		if (!attemptToDestroy(null, new BlockCoords(event.getBlock()))) {
			event.setCancelled(true);
		}
	}

	/**
	 * @return false if the event should be cancelled
	 */
	private boolean attemptToDestroy(Player player, BlockCoords coords) {
		// shop
		PlayerShop shop = SupremeShops.inst().getData().getShops().getPhysicalElementLinkedToBlock(coords);
		if (shop != null) {
			// if it's a player, then he might be allowed to destroy it
			if (player != null && SupremeShops.inst().getModuleManager().canShopBeDestroyed(player, shop, true)) {
				shop.destroy(DestroyCause.ALLOWED_BREAK, true);
			}
			// not a player, can't destroy it
			return false;
		}
		// merchant
		Merchant merchant = SupremeShops.inst().getData().getMerchants().getPhysicalElementLinkedToBlock(coords);
		if (merchant != null) {
			// if it's a player, then he might be allowed to destroy it
			if (player != null && SupremeShops.inst().getModuleManager().canMerchantBeDestroyed(player, merchant, true)) {
				merchant.destroy(DestroyCause.ALLOWED_BREAK, true);
			}
			// not a player, can't destroy it
			return false;
		}
		return true;
	}

}
