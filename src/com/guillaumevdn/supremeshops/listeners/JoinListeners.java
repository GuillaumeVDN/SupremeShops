package com.guillaumevdn.supremeshops.listeners;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import com.guillaumevdn.supremeshops.SupremeShops;

public class JoinListeners implements Listener {

	@EventHandler
	public void event(PlayerJoinEvent event) {
		SupremeShops.inst().getGeneralManager().spawnMerchantsNpcs(event.getPlayer());
	}

	@EventHandler
	public void event(PlayerQuitEvent event) {
		SupremeShops.inst().getGeneralManager().removeMerchantsNpcs(event.getPlayer());
	}

}
