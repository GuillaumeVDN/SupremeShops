package com.guillaumevdn.supremeshops.listeners;

import java.util.Set;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import com.guillaumevdn.gcorelegacy.data.UserInfo;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Pair;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SSLocaleMisc;
import com.guillaumevdn.supremeshops.SSPerm;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.data.SSUser;
import com.guillaumevdn.supremeshops.gui.rentable.UnrentedGUI;
import com.guillaumevdn.supremeshops.gui.rentable.management.RentableManagementGUI;
import com.guillaumevdn.supremeshops.gui.shop.TradePreviewGUI;
import com.guillaumevdn.supremeshops.module.manageable.ShopManagementPermission;
import com.guillaumevdn.supremeshops.module.object.ObjectSide;
import com.guillaumevdn.supremeshops.module.object.TradeObject;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.PlayerShop;
import com.guillaumevdn.supremeshops.util.ClickType;

public class ShopsListeners implements Listener {

	// management of shops

	@EventHandler(ignoreCancelled = true, priority = EventPriority.LOWEST)
	public void eventLowest(PlayerInteractEvent event) {
		Block block = event.getClickedBlock();
		String action = event.getAction().toString();
		if (block != null && action.contains("CLICK_BLOCK")) {
			PlayerShop shop = SupremeShops.inst().getData().getShops().getPhysicalElementLinkedToBlock(new BlockCoords(block));
			if (shop != null) {
				// copy management permissions
				Player player = event.getPlayer();
				Pair<UserInfo, Set<ShopManagementPermission>> copying = SupremeShops.inst().getGeneralManager().getCopyingShopManagerPermissions(player);
				if (copying != null) {
					event.setCancelled(true);
					if (shop.hasManagementPermission(player, ShopManagementPermission.EDIT_MANAGERS)) {
						shop.changeManagementPermissions(copying.getA(), copying.getB());
						SSLocale.MSG_SUPREMESHOPS_COPIEDPERMISSIONS.send(player, "{player}", copying.getA().toOfflinePlayer().getName());
					} else {
						SSLocale.MSG_SUPREMESHOPS_SHOPNOTYOURS.send(player);
					}
					return;
				}
				// restock all
				else if (SupremeShops.inst().getGeneralManager().getMultiRestockingAll().contains(player)) {
					event.setCancelled(true);
					if (shop.isCurrentOwnerAdmin()) {
						SSLocale.MSG_SUPREMESHOPS_NOSTOCKADMINSHOP.send(player);
						return;
					}
					if (shop.hasManagementPermission(player, ShopManagementPermission.ADD_STOCK)) {
						shop.restockAll(player);
						SSLocale.MSG_SUPREMESHOPS_RESTOCKEDALLMULTI.send(player);
					} else {
						SSLocale.MSG_SUPREMESHOPS_SHOPNOTYOURS.send(player);
					}
					return;
				}
				// withdraw all (benefits)
				else if (SupremeShops.inst().getGeneralManager().getMultiWithdrawingAllBenefits().contains(player)) {
					event.setCancelled(true);
					if (shop.isCurrentOwnerAdmin()) {
						SSLocale.MSG_SUPREMESHOPS_NOSTOCKADMINSHOP.send(player);
						return;
					}
					if (shop.hasManagementPermission(player, ShopManagementPermission.REMOVE_STOCK)) {
						// withdraw
						for (TradeObject object : shop.getObjects(Utils.asList(ObjectSide.TAKING), null)) {
							object.withdrawAsMuchStockAsPossible(player, shop);
						}
						SSLocale.MSG_SUPREMESHOPS_WITHDRAWNALLBENEFITSMULTI.send(player);
					} else {
						SSLocale.MSG_SUPREMESHOPS_SHOPNOTYOURS.send(player);
					}
					return;
				}
				// withdraw all (stock)
				else if (SupremeShops.inst().getGeneralManager().getMultiWithdrawingAllStock().contains(player)) {
					event.setCancelled(true);
					if (shop.isCurrentOwnerAdmin()) {
						SSLocale.MSG_SUPREMESHOPS_NOSTOCKADMINSHOP.send(player);
						return;
					}
					if (shop.hasManagementPermission(player, ShopManagementPermission.REMOVE_STOCK)) {
						// withdraw
						for (TradeObject object : shop.getObjects(Utils.asList(ObjectSide.GIVING), null)) {
							object.withdrawAsMuchStockAsPossible(player, shop);
						}
						SSLocale.MSG_SUPREMESHOPS_WITHDRAWNALLSTOCKMULTI.send(player);
					} else {
						SSLocale.MSG_SUPREMESHOPS_SHOPNOTYOURS.send(player);
					}
					return;
				}
			}
		}
	}

	@EventHandler(priority = EventPriority.NORMAL /* normal because sneak cancel GUI delay is LOWEST*/, ignoreCancelled = true)
	public void event(PlayerToggleSneakEvent event) {
		// end copy management permissions
		Player player = event.getPlayer();
		Pair<UserInfo, Set<ShopManagementPermission>> copying = SupremeShops.inst().getGeneralManager().getCopyingShopManagerPermissions(player);
		if (copying != null) {
			SupremeShops.inst().getGeneralManager().setCopyingShopManagerPermissions(player, null, null);
			SSLocale.MSG_SUPREMESHOPS_PERMISSIONSCOPYEND.send(player);
		}
		// end restocking all
		else if (SupremeShops.inst().getGeneralManager().getMultiRestockingAll().remove(player)) {
			SSLocale.MSG_SUPREMESHOPS_MULTIRESTOCKALLEND.send(player);
		}
		// end withdrawing all (benefits)
		else if (SupremeShops.inst().getGeneralManager().getMultiWithdrawingAllBenefits().remove(player)) {
			SSLocale.MSG_SUPREMESHOPS_MULTIWITHDRAWALLBENEFITSEND.send(player);
		}
		// end withdrawing all (stock)
		else if (SupremeShops.inst().getGeneralManager().getMultiWithdrawingAllStock().remove(player)) {
			SSLocale.MSG_SUPREMESHOPS_MULTIWITHDRAWALLSTOCKSEND.send(player);
		}
	}

	// interaction with shops

	@EventHandler(ignoreCancelled = true, priority = EventPriority.HIGHEST)
	public void event(PlayerInteractEvent event) {
		Block block = event.getClickedBlock();
		String action = event.getAction().toString();
		if (block != null && action.contains("CLICK_BLOCK")) {
			PlayerShop shop = SupremeShops.inst().getData().getShops().getPhysicalElementLinkedToBlock(new BlockCoords(block));
			if (shop != null) {
				event.setCancelled(true);
				Player player = event.getPlayer();
				ClickType click = action.contains("LEFT") ? ClickType.LEFT_CLICK : ClickType.RIGHT_CLICK;
				boolean sneak = event.getPlayer().isSneaking();
				// preview
				if (click.equals(SupremeShops.inst().getModuleManager().getPreviewClick()) && sneak == SupremeShops.inst().getModuleManager().getPreviewClickSneak()) {
					// shop is an unrented block shop
					if (shop instanceof Rentable && !((Rentable) shop).isRented()) {
						// edit admin
						if (SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
							new RentableManagementGUI((Rentable) shop, player, null, -1).open(player);
							return;
						}
						// not rentable
						else if (!((Rentable) shop).isRentable()) {
							SSLocale.MSG_SUPREMESHOPS_UNRENTABLESHOP.send(player);
							return;
						} else {
							// can't rent (conditions)
							if (!((Rentable) shop).areRentConditionsValid(player, true)) {
								return;
							}
							// streak delay
							if (((Rentable) shop).getRentPeriodStreakLimitDelay() > 1) {
								long nextRent = SSUser.get(player).getLastRentEnd((Rentable) shop) + Utils.getSecondsInMillis(((Rentable) shop).getRentPeriodStreakLimitDelay() * 60);
								if (System.currentTimeMillis() < nextRent) {
									SSLocale.MSG_SUPREMESHOPS_CANTRENTSHOPSTREAKLIMITDELAY.send(player, "{time}", Utils.formatDurationMillis(nextRent - System.currentTimeMillis()));
									return;
								}
							}
							// open gui
							new UnrentedGUI((Rentable) shop, null, player, null, -1).open(player);
						}
					}
					// another option
					else if (!shop.getManagementPermissions(player).isEmpty() || (shop.ensureOpenOrError(player) && shop.areTradeConditionsValid(player, true) && (! SupremeShops.inst().getForceItemDiscover() || shop.hasDiscoveredAllItems(player, true))) || (shop instanceof Rentable && SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player))) {
						new TradePreviewGUI(shop, null, 1, player, null, -1).open(player);
					}
				}
				// unknown click
				else {
					String previewClick = (SupremeShops.inst().getModuleManager().getPreviewClick() != null ? SupremeShops.inst().getModuleManager().getPreviewClick().getText().getLine() : "/") + (SupremeShops.inst().getModuleManager().getPreviewClickSneak() ? " + " + SSLocaleMisc.MISC_SUPREMESHOPS_SNEAK.getLine() : "");
					if (shop.getCurrentOwner() != null ? player.equals(shop.getCurrentOwner().toPlayer()) || (shop instanceof Rentable && SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) : SSPerm.SUPREMESHOPS_EDIT_ADMIN.has(player)) {
						SSLocale.MSG_SUPREMESHOPS_SHOPHELPEDIT.send(player, "{preview_click}", previewClick);
					} else {
						SSLocale.MSG_SUPREMESHOPS_SHOPHELP.send(player, "{preview_click}", previewClick);
					}
				}
			}
		}
	}

}
