package com.guillaumevdn.supremeshops.task;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.lib.messenger.Text;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SSLocale;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class RentsUpdateTask extends BukkitRunnable {

	private Map<Player, Long> lastStreakLimitMessage = new HashMap<>();
	private Map<Player, Long> lastPaidRentMessage = new HashMap<>();

	@Override
	public void run() {
		// get all known rentables
		List<? extends Rentable> rentables = Utils.asListMultiple(
				SupremeShops.inst().getData().getShops().getAll(Utils.asList(Rentable.class), false, com.guillaumevdn.supremeshops.data.ShopBoard.ElementRemotePolicy.MIGHT_BE),
				SupremeShops.inst().getData().getMerchants().getAll(Utils.asList(Rentable.class), false, com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy.MIGHT_BE));
		// check them
		List<Shop> shopToPush = new ArrayList<Shop>();
		List<Merchant> merchantsToPush = new ArrayList<Merchant>();
		long toleranceDelayMillis = Utils.getSecondsInMillis(SupremeShops.inst().getConfiguration().getInt("force_unrent_tolerance_delay", 1440) * 60);
		long forceUnrentDelayMessageMillis = Utils.getSecondsInMillis(SupremeShops.inst().getConfiguration().getInt("force_unrent_message_delay", 1800));
		for (Rentable rentable : rentables) {
			// isn't rented
			if (!rentable.isRented()) {
				continue;
			}
			// update rent
			rentable.checkToUpdateRent(false);
			// is awaiting force unrent
			if (rentable.isAwaitingForceUnrent()) {
				// TROP TARD, L'HUISSIER EST DEJA A LA PORTE POUR VENIR PRENDRE LES MEUBLES MON COCO
				long deadline = rentable.getLastCheckRent() + toleranceDelayMillis;
				if (System.currentTimeMillis() >= deadline) {
					rentable.changeRent(null, 0, false);
					if (rentable instanceof Shop) {
						shopToPush.add((Shop) rentable);
					} else if (rentable instanceof Merchant) {
						merchantsToPush.add((Merchant) rentable);
					}
					continue;
				}
				// notify
				else {
					Player owner = rentable.getCurrentOwner().toPlayer();
					if (owner != null) {
						Text message;
						Map<Player, Long> map;
						if (rentable.getPastPaidRents() >= rentable.getRentPeriodStreakLimit()) {
							message = SSLocale.MSG_SUPREMESHOPS_MUSTUNRENTMERCHANTMAXSTREAKLIMIT;
							map = lastStreakLimitMessage;
						} else if (rentable.getPaidRents() == 0) {
							message = SSLocale.MSG_SUPREMESHOPS_MUSTPAYMERCHANTRENT;
							map = lastPaidRentMessage;
						}
						else continue;
						long remaining = deadline - System.currentTimeMillis();
						if (remaining > 300000L) {
							Long last = map.getOrDefault(owner, 0L);
							if (System.currentTimeMillis() - last < forceUnrentDelayMessageMillis) {
								continue;
							}
							map.put(owner, System.currentTimeMillis());
						}
						message.send(owner, "{merchant}", rentable.getDisplayName(), "{time}", Utils.formatDurationMillis(remaining));
					}
				}
			}
		}
		// push
		if (!shopToPush.isEmpty()) {
			SupremeShops.inst().getData().getShops().pushAsync(shopToPush);
		}
		if (!merchantsToPush.isEmpty()) {
			SupremeShops.inst().getData().getMerchants().pushAsync(merchantsToPush);
		}
	}

}
