package com.guillaumevdn.supremeshops.task;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.DestroyCause;

public class InactiveShopDestroyerTask extends BukkitRunnable {

	@Override
	public void run() {
		// check all known shops
		Set<Shop> shopsToDestroy = new HashSet<Shop>();
		long delay = Utils.getMinutesInMillis(SupremeShops.inst().getModuleManager().getInactivityTimeDestroy());
		if (delay > 0L) {
			for (Shop shop : SupremeShops.inst().getData().getShops().getAll().values()) {
				// user was gone for a long time
				if (!shop.isCurrentOwnerAdmin() && !(shop instanceof Rentable)) {
					if (shop.getCurrentOwner() != null) {
						long last = shop.getCurrentOwner().toOfflinePlayer().getLastPlayed();
						if (System.currentTimeMillis() - last >= delay) {
							shopsToDestroy.add(shop);
						}
					}
				}
			}
		}
		// destroy shops
		Set<Merchant> moreMerchantsToPush = new HashSet<>();
		if (!shopsToDestroy.isEmpty()) {
			for (Shop shop : shopsToDestroy) {
				moreMerchantsToPush.addAll(shop.destroy(DestroyCause.INACTIVE_DESTROYER_TASK, false));
			}
			SupremeShops.inst().getData().getShops().deleteAll(shopsToDestroy);
		}
		// push remaining merchants
		if (!moreMerchantsToPush.isEmpty()) {
			SupremeShops.inst().getData().getMerchants().pullAsync(moreMerchantsToPush, null);
		}
		// log
		if (!shopsToDestroy.isEmpty()) {
			SupremeShops.inst().pluginLog(null, null, null, null, "{INACTIVE-SHOP-DESTROYER} ", "Destroyed " + shopsToDestroy.size() + " shop" + Utils.getPlural(shopsToDestroy.size()));
		}
	}

}
