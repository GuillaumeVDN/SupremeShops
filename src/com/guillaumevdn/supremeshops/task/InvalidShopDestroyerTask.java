package com.guillaumevdn.supremeshops.task;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.lib.data.DataManager.BackEnd;
import com.guillaumevdn.gcorelegacy.lib.material.Mat;
import com.guillaumevdn.gcorelegacy.lib.util.BlockCoords;
import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.BlockShop;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.module.shop.SignShop;
import com.guillaumevdn.supremeshops.util.DestroyCause;

public class InvalidShopDestroyerTask extends BukkitRunnable {

	@Override
	public void run() {
		// check all known shops
		Set<Shop> shopsToDestroy = new HashSet<Shop>();
		boolean disableBlockValidity = SupremeShops.inst().getData().getBackEnd().equals(BackEnd.MYSQL) && SupremeShops.inst().getConfiguration().getBoolean("data.sync_disable_blockvalidity", false);
		for (Shop shop : SupremeShops.inst().getData().getShops().getAll().values()) {
			// block shop
			if (shop instanceof BlockShop) {
				// no sign
				BlockCoords coords = ((BlockShop) shop).getSign();
				if (coords == null) {
					shopsToDestroy.add(shop);
					continue;
				}
				// chunk isn't loaded, check it later
				if (coords.getWorld() == null || !coords.getWorld().isChunkLoaded(coords.getX() >> 4, coords.getZ() >> 4)) {
					continue;
				}
				// sign isn't a sign or shop block isn't there
				if (!disableBlockValidity) {
					Block block = coords.toBlock();
					if (!Mat.fromBlock(block).getModernName().contains("SIGN") || Mat.fromBlock(block = ((BlockShop) shop).getBlock().toBlock()).isAir()) {
						shopsToDestroy.add(shop);
						continue;
					}
				}
			}
			// sign shop
			if (shop instanceof SignShop) {
				// no sign
				BlockCoords coords = ((SignShop) shop).getSign();
				if (coords == null) {
					shopsToDestroy.add(shop);
					continue;
				}
				// chunk isn't loaded, check it later
				if (coords.getWorld() == null || !coords.getWorld().isChunkLoaded(coords.getX() >> 4, coords.getZ() >> 4)) {
					continue;
				}
				// sign isn't a sign
				if (!disableBlockValidity) {
					Block block = coords.toBlock();
					Mat mat = Mat.fromBlock(block);
					if (!mat.getModernName().contains("SIGN")) {
						shopsToDestroy.add(shop);
						SupremeShops.inst().warning("Sign shop at " + coords + " is invalid and will be removed (" + mat + ")");
						continue;
					}
				}
			}
			// owner is online and the shop doesn't match existence conditions
			if (!shop.isCurrentOwnerAdmin() && !(shop instanceof Rentable)) {
				Player owner = shop.getCurrentOwner() != null ? shop.getCurrentOwner().toPlayer() : null;
				if (owner != null) {
					if (!SupremeShops.inst().getModuleManager().canShopExist(owner, shop, true)) {
						shopsToDestroy.add(shop);
						continue;
					}
				}
			}
		}
		// destroy shops
		Set<Merchant> moreMerchantsToPush = new HashSet<Merchant>();
		if (!shopsToDestroy.isEmpty()) {
			for (Shop shop : shopsToDestroy) {
				moreMerchantsToPush.addAll(shop.destroy(DestroyCause.INVALID_DESTROYER_TASK, false));
			}
			SupremeShops.inst().getData().getShops().deleteAll(shopsToDestroy);
		}
		// push remaining merchants
		if (!moreMerchantsToPush.isEmpty()) {
			SupremeShops.inst().getData().getMerchants().pullAsync(moreMerchantsToPush, null);
		}
		// log
		if (!shopsToDestroy.isEmpty()) {
			SupremeShops.inst().pluginLog(null, null, null, null, "{INVALID-SHOP-DESTROYER} ", "Destroyed " + shopsToDestroy.size() + " shop" + Utils.getPlural(shopsToDestroy.size()));
		}
	}

}
