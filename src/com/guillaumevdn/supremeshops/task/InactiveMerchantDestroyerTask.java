package com.guillaumevdn.supremeshops.task;

import java.util.HashSet;
import java.util.Set;

import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.rentable.Rentable;
import com.guillaumevdn.supremeshops.module.shop.Shop;
import com.guillaumevdn.supremeshops.util.DestroyCause;

public class InactiveMerchantDestroyerTask extends BukkitRunnable {

	@Override
	public void run() {
		// check all known shops
		Set<Merchant> merchantsToDestroy = new HashSet<Merchant>();
		long delay = Utils.getMinutesInMillis(SupremeShops.inst().getModuleManager().getInactivityTimeDestroy());
		if (delay > 0L) {
			for (Merchant merchant : SupremeShops.inst().getData().getMerchants().getAll().values()) {
				// user was gone for a long time and not an admin
				if (!merchant.isCurrentOwnerAdmin() && !(merchant instanceof Rentable)) {
					if (merchant.getCurrentOwner() != null) {
						long last = merchant.getCurrentOwner().toOfflinePlayer().getLastPlayed();
						if (System.currentTimeMillis() - last >= delay) {
							merchantsToDestroy.add(merchant);
						}
					}
				}
			}
		}
		// destroy merchants
		Set<Shop> shopsToDestroy = new HashSet<Shop>();
		if (!merchantsToDestroy.isEmpty()) {
			for (Merchant merchant : merchantsToDestroy) {
				shopsToDestroy.addAll(merchant.destroy(DestroyCause.INACTIVE_DESTROYER_TASK, false));
			}
			SupremeShops.inst().getData().getShops().deleteAll(shopsToDestroy);
		}
		// destroy shops
		Set<Merchant> moreMerchantsToPush = new HashSet<Merchant>();
		if (!shopsToDestroy.isEmpty()) {
			for (Shop shop : shopsToDestroy) {
				moreMerchantsToPush.addAll(shop.destroy(DestroyCause.INACTIVE_DESTROYER_TASK, false));
			}
			SupremeShops.inst().getData().getShops().deleteAll(shopsToDestroy);
		}
		// push remaining merchants
		moreMerchantsToPush.removeAll(merchantsToDestroy);
		if (!moreMerchantsToPush.isEmpty()) {
			SupremeShops.inst().getData().getMerchants().pullAsync(moreMerchantsToPush, null);
		}
		// log
		if (!merchantsToDestroy.isEmpty() || !shopsToDestroy.isEmpty()) {
			SupremeShops.inst().pluginLog(null, null, null, null, "{INACTIVE-MERCHANT-DESTROYER} ", "Destroyed " + merchantsToDestroy.size() + " merchant" + Utils.getPlural(merchantsToDestroy.size()) + " (and " + shopsToDestroy.size() + " shop" + Utils.getPlural(shopsToDestroy.size()) + ")");
		}
	}

}
