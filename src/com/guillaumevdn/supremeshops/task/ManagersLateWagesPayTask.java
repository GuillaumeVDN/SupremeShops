package com.guillaumevdn.supremeshops.task;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.scheduler.BukkitRunnable;

import com.guillaumevdn.gcorelegacy.lib.util.Utils;
import com.guillaumevdn.supremeshops.SupremeShops;
import com.guillaumevdn.supremeshops.module.manageable.Manageable;
import com.guillaumevdn.supremeshops.module.merchant.Merchant;
import com.guillaumevdn.supremeshops.module.shop.Shop;

public class ManagersLateWagesPayTask extends BukkitRunnable {

	@Override
	public void run() {
		// get all known manageables
		List<? extends Manageable> manageables = Utils.asListMultiple(
				SupremeShops.inst().getData().getShops().getAll(Utils.asList(Manageable.class), false, com.guillaumevdn.supremeshops.data.ShopBoard.ElementRemotePolicy.MIGHT_BE),
				SupremeShops.inst().getData().getMerchants().getAll(Utils.asList(Manageable.class), false, com.guillaumevdn.supremeshops.data.MerchantBoard.ElementRemotePolicy.MIGHT_BE));
		// check them
		List<Shop> shopToPush = new ArrayList<Shop>();
		List<Merchant> merchantsToPush = new ArrayList<Merchant>();
		for (Manageable manageable : manageables) {
			if (manageable.checkToPayLateWages(false) != 0) {
				if (manageable instanceof Shop) {
					shopToPush.add((Shop) manageable);
				} else if (manageable instanceof Merchant) {
					merchantsToPush.add((Merchant) manageable);
				}
			}
		}
		// push
		if (!shopToPush.isEmpty()) {
			SupremeShops.inst().getData().getShops().pushAsync(shopToPush);
		}
		if (!merchantsToPush.isEmpty()) {
			SupremeShops.inst().getData().getMerchants().pushAsync(merchantsToPush);
		}
	}

}
