// Lines starting with // are comments
// Lines starting with : are instructions ; the more : there are, the more deeper you're in the code (in a loop for example, one more : needs to be added to define which instructions are in the loop)
// Default variables are baseX, baseY, baseZ (base coordinates)

// Display emeralds around, every tick

:set cx,baseX + 0.5
:set cz,baseZ + 0.5
:set radius,0.75
:set y,baseY

:loop angle,0,720,15               // make a loop using variable angle from 0 to 720 with steps of 15
::set x,cx + radius * cos(angle)   // calculate x
::set y,y + 0.015                  // calculate y
::set z,cz + radius * sin(angle)   // calculate z
::wait_ticks 1				       // wait one tick
::display VILLAGER_HAPPY,x,y,z,1   // display

// Wait 2 seconds until restarting

:wait_ticks 40
