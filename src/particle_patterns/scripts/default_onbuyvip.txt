// Lines starting with // are comments
// Lines starting with : are instructions ; the more : there are, the more deeper you're in the code (in a loop for example, one more : needs to be added to define which instructions are in the loop)
// Default variables are baseX, baseY, baseZ (base coordinates)

// Display a few emeralds above

:loop delta,0,10,1						// make a loop using variable delta from 0 to 10 with steps of 1
::set x,baseX - 1 + rand(3)				// calculate random x
::set y,baseY + rand(2)					// calculate random y
::set z,baseZ - 1 + rand(3)				// calculate random z
::wait_ticks 1							// wait one tick
::display VILLAGER_HAPPY,x,y,z,1		// display

// Display a heart at the end

:display HEART,z,y + 0.3,z,1
